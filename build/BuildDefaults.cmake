# Commmon build settings across all Vipertooh Voice UI modules.

set(CMAKE_CXX_STANDARD 11)
include(GNUInstallDirs)

# Compilation FLAGS for 3P solutions.
if(BUILD_SENSORY_CLIENT)
add_definitions(-DBUILD_SENSORY_CLIENT)
endif(BUILD_SENSORY_CLIENT)

if(BUILD_DSPC_CLIENT)
add_definitions(-DBUILD_DSPC_CLIENT)
endif(BUILD_DSPC_CLIENT)

if(BUILD_AVS_WWE)
add_definitions(-DBUILD_AVS_WWE)
endif(BUILD_AVS_WWE)

#Compilation Flag for 32 or 64 bits
if(FLAG_32_BIT)
add_definitions(-DFLAG_32_BIT)
endif(FLAG_32_BIT)
if(FLAG_64_BIT)
add_definitions(-DFLAG_64_BIT)
endif(FLAG_64_BIT)
