# VoiceUIFramework
# About
This Directory contains the code for VoiceUI sub-system of Vipertooth
# Code organization
## build
Contains build defaults for cmake
## CMakeLists.txt
Top level cmake list that builds cross-platform, VOICE UI Framework for Viper Tooth
Along with this, it also builds a sample application that demonstrates use of voice-ui-framework
## VoiceUIFramework 
Main code base of Voice UI Framework
### TODO: Describe all the folders
## VoiceUISampleApp 
Example APP using VoiceUIFramework
# Build Flags
## BUILD_TARGET_UNIT_TESTS
When defined, Builds unit tests for target (a.k.a Vipertooth, 8017 etc)
## BUILD_HOST_SANITY_TESTS
When defined builds sanity tests (unit tests on Hosts like x86)
Valid only for Native(Host) builds, DO NOT USE for Target Builds. 
When defined a CPP compiler flag HOST_SANITY_TESTS_ENABLED  is set

