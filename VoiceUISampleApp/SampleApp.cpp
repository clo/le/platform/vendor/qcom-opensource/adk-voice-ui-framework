/* Copyright (c) 2018, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    SampleApp.cpp
 *  @brief   SampleAPP to exercise Voice UI Framework
 *
 *  DESCRIPTION
 *    SampleAPP to exercise Voice UI Framework on LE platforms
 ***************************************************************/

#include <VoiceUIManager/VoiceUIManager.h>

#include <csignal>
#include <syslog.h>
#include <iostream>
#include <memory>
#include <string>

using namespace std;
using namespace voiceUIFramework::voiceUIManager;

#if 0

static const std::string WELCOME_MESSAGE =
    "+--------------------------- Voice Ui Framework for VT -------------------------+\n"
    "|                                  Options:                                     |\n"
    //    "| Playback Volume Control:                                                      |\n"
    //    "|       Press '+' followed by Enter to increase Device Volume.                  |\n"
    //    "|       Press '-' followed by Enter to decrease Device Volume.                  |\n"
    "| Playback Mute Control:                                                        |\n"
    "|       Press 'm' followed by Enter to toggle Mute.                             |\n"
    //    "| Stop Clients:                                                                 |\n"
    //    "|       Press 's' followed by Enter to stop an ongoing interaction.             |\n"
    //    "| Start Clients:                                                                |\n"
    //    "|       Press 'r' followed by Enter to restart Keyword Detection manually       |\n"
    "| Initiate AVS Authentication:                                                  |\n"
    "|       Press 'a' followed by Enter to Initiate AVS authentication              |\n"
    "| TAP to TALK default Client:                                                   |\n"
    "|       Press 't' followed by Enter to Start Default Client interaction manually|\n"
    "| TAP to TALK LOCAL QC ASR:                                                     |\n"
    "|       Press 'l' followed by Enter to Start Local ASR interaction manually     |\n"
    "| AVS Locale:                                                                   |\n"
    "|       Press 'e' followed by Enter to Change AVS locale to en-GB this menu.    |\n"
    "| AVS Re Authorization:                                                         |\n"
    "|       Press 'z' followed by Enter to Re Authorize AVS.                        |\n"
    "| help:                                                                         |\n"
    "|       Press 'h' followed by Enter to PRINT this menu.                         |\n"
    "| Quit:                                                                         |\n"
    "|       Press 'q' followed by Enter to QUIT application.                        |\n"
    "+-------------------------------------------------------------------------------+\n";

static const char QUIT = 'q';
static const char HELP = 'h';
static const char START = 'r';
static const char STOP = 's';
static const char TAPTOTALK = 't';
static const char TAPTOTALKLOCALASR = 'l';
static const char VOLUP = '+';
static const char VOLDOWN = '-';
static const char TOGGLE_MUTE = 'm';
static const char AVS_AUTHENTICATION = 'a';
static const char AVS_LOCALE = 'e';
static const char REAUTHORIZE = 'z';
#endif

//Notification Observer Client Class to handle callbacks from Voice UI Clients
class NotificationObserverClient : public NotificationObserver {
 public:
  ~NotificationObserverClient(){};

  /* NotificationObserver class */
  //Notification containing callback received from Voice UI Clients
  virtual void NotificationReceived(VoiceUIClientID client_id, std::string event) {
    syslog(LOG_INFO, "SampeAPP::NotificationObserverClient::NotificationReceived - Client: %s - Event: %s", ClientIDToString(client_id).c_str(),
        event.c_str());
  }
};

int main(int argc, char **argv) {

  sigset_t signal_set;
  if (sigemptyset(&signal_set) < 0) {
      syslog(LOG_ERR,"ERROR: Unable to create process signal mask\n");
      std::exit(EXIT_FAILURE);
  }
  if (sigaddset(&signal_set, SIGINT) < 0) {
      syslog(LOG_ERR,"ERROR: Unable to add to process signal mask\n");
      std::exit(EXIT_FAILURE);
  }
  if (sigaddset(&signal_set, SIGTERM) < 0) {
      syslog(LOG_ERR,"ERROR: Unable to add to process signal mask\n");
      std::exit(EXIT_FAILURE);
  }
  if (sigprocmask(SIG_BLOCK, &signal_set, nullptr) < 0) {
      syslog(LOG_ERR,"ERROR: Unable to set process signal mask\n");
      std::exit(EXIT_FAILURE);
  }

  //Create Voice UI manager
  std::shared_ptr<VoiceUIManager> voice_ui_manager = std::make_shared<VoiceUIManager>();

  //Configure VoiceUIManager
  if (!voice_ui_manager->Configure()) {
    syslog(LOG_DEBUG, "VoiceUIManager fail to initialize.");
    return -1;
  }

  //Notification Observer Client class to handle callbacks from Voice UI Clients
  std::shared_ptr<NotificationObserver> notification_observer = std::make_shared<NotificationObserverClient>();
  voice_ui_manager->AddNotificationObserver(notification_observer);

  //Create AVS Solution
  voice_ui_manager->CreateClient(VoiceUIClientID::AVS_SOLUTION, false);
  //Create AVS Google
  voice_ui_manager->CreateClient(VoiceUIClientID::CORTANA_SOLUTION, true);
  //Create AVS Cortana
  voice_ui_manager->CreateClient(VoiceUIClientID::GVA_SOLUTION, true);
  //Create AVS Modular - QC ASR/NLu
  voice_ui_manager->CreateClient(VoiceUIClientID::MODULAR_SOLUTION, false);

  // Main loop waiting for signals
  bool waiting_for_signals = true;
  while (waiting_for_signals) {
      // Block until a signal arrives
      int signal_number;
      int error = sigwait(&signal_set, &signal_number);

      // Check there was no error
      if (error) {
          syslog(LOG_ERR,"Error %d while waiting for process signals\n", error);
          break;
      }

      // Check which signal it was
      switch (signal_number) {
          case SIGINT:
          case SIGTERM:
              // Exit loop, terminate gracefully
              waiting_for_signals = false;
              break;

          default:
              syslog(LOG_ERR,"ERROR: Received unexpected process signal\n");
              waiting_for_signals = false;
              break;
      }
  }

#if 0
  //Allow creation of the clients
  std::this_thread::sleep_for(std::chrono::milliseconds(5000));


//TBD (kshriniv) Implement message sending via SIGUSR1 or equivalent
  std::cout << WELCOME_MESSAGE.c_str() << std::endl;

  char c;
  bool loop = true;
  IntentManagerEvent intent;
  IntentManagerEventExtended intent_ext_;

  while (loop) {
    std::cin >> c;
    c = ::tolower(c);

    switch (c) {
      case QUIT:
        loop = false;
        syslog(LOG_INFO, "Exiting Sample APP !!!");
        break;
      case START:
        break;
      case STOP:
        break;
      case TAPTOTALK:
        syslog(LOG_INFO, "TAP to talk Default Client pressed...");
        intent = IntentManagerEvent::TAP_BUTTON_COMMAND_AVS;
        voice_ui_manager->IntentReceived(intent);
        break;
      case TAPTOTALKLOCALASR:
        syslog(LOG_INFO, "TAP to talk Local ASR pressed...");
        intent = IntentManagerEvent::TAP_BUTTON_COMMAND_QCASR;
        voice_ui_manager->IntentReceived(intent);
        break;
      case VOLUP:
        syslog(LOG_INFO, "VOLUP pressed...");
        intent = IntentManagerEvent::VOL_UP_EVENT;
        voice_ui_manager->IntentReceived(intent);
        break;
      case VOLDOWN:
        syslog(LOG_INFO, "VOLDOWN pressed...");
        intent = IntentManagerEvent::VOL_DOWN_EVENT;
        voice_ui_manager->IntentReceived(intent);
        break;
      case TOGGLE_MUTE:
        syslog(LOG_INFO, "TOGGLE_MUTE pressed...");
        intent = IntentManagerEvent::MUTE_EVENT;
        voice_ui_manager->IntentReceived(intent);
        break;
      case AVS_AUTHENTICATION:
        syslog(LOG_INFO, "AVS Authentication started...");
        intent = IntentManagerEvent::ONBOARDING_REQUESTED;
        intent_ext_.str_1 = "AVS";
        voice_ui_manager->IntentReceived(intent, intent_ext_);
        break;
      case AVS_LOCALE:
        syslog(LOG_INFO, "AVS locale change triggered ...");
        intent = IntentManagerEvent::SET_AVS_LOCALE;
        //  {"en-US"},
        //  {"en-GB"},
        //  {"de-DE"},
        //  {"en-IN"},
        //  {"en-CA"},
        //  {"ja-JP"},
        //  {"en-AU"},
        //  {"fr-FR"},
        //  {"it-IT"},
        //  {"es-ES"},
        intent_ext_.str_1 = "en-GB";
        voice_ui_manager->IntentReceived(intent, intent_ext_);
        break;
      case REAUTHORIZE:
        intent = IntentManagerEvent::RESET_AVS_AUTHENTICATION;
        voice_ui_manager->IntentReceived(intent);
        std::this_thread::sleep_for(std::chrono::milliseconds(10000));
        syslog(LOG_DEBUG, "VoiceUI needs to be restarted!");
        loop = false;
        break;
      case HELP:
        std::cout << WELCOME_MESSAGE.c_str() << std::endl;
        break;
      default:
        std::cout << "Not a valid input, please try again." << std::endl;
        break;
    }
  }
#endif

  //Shutdown Voice UI Manager
  voice_ui_manager->Shutdown();

  syslog(LOG_DEBUG, "Exiting Sample APP");
  return 0;
}
