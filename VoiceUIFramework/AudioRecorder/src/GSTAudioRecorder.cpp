/* Copyright (c) 2018-2020, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    GSTAudioRecorder.cpp
 *  @brief   brief description
 *
 *  DESCRIPTION
 *      MediaRecorder is a wrapper on AUDIO HAL provides functionality
 *      to configure audio recording session and streaming PCM samples
 ***************************************************************/
#include <AudioRecorder/GSTAudioRecorder.h>

static GstFlowReturn on_new_sample_from_sink(GstElement *elt, gpointer *data) {
  GstSample *sample;
  GstBuffer *app_buffer, *buffer;
  GstElement *source;
  GstMapInfo info;

  GSTAudioRecorder *ptr = (GSTAudioRecorder *)data;
  /* get the sample from appsink */
  sample = gst_app_sink_pull_sample(GST_APP_SINK(elt));
  buffer = gst_sample_get_buffer(sample);
  gst_buffer_map(buffer, &info, GST_MAP_READ);
  ptr->callGetBufferEvent(info.data, info.size);
  gst_buffer_unmap(buffer, &info);
  gst_sample_unref(sample);
  return GST_FLOW_OK;
}

static gboolean handle_bus_msgs(GstBus *bus, GstMessage *msg, gpointer user_data) {
  GSTAudioRecorder *ptr = (GSTAudioRecorder *)user_data;
  return ptr->handle_bus_msg(bus, msg);
}

gboolean GSTAudioRecorder::handle_bus_msg(GstBus *bus, GstMessage *msg) {
  switch (GST_MESSAGE_TYPE(msg)) {
    case GST_MESSAGE_ERROR: {
      GError *err;
      gchar *dbg;

      syslog(LOG_ERR, "GSTAudioRecorder::handle_bus_msg - GST_MESSAGE_ERROR");

      /* dump graph on error */
      GST_DEBUG_BIN_TO_DOT_FILE_WITH_TS(GST_BIN(pipeline_), GST_DEBUG_GRAPH_SHOW_ALL, "gst-play.error");

      gst_message_parse_error(msg, &err, &dbg);
      if (dbg != NULL) {
        g_printerr("ERROR debug information: %s\n", dbg);
        syslog(LOG_ERR, "GSTAudioRecorder::handle_bus_msg - ERROR debug information: %s\n", dbg);
      }
      g_clear_error(&err);
      g_free(dbg);

      /* flush any other error messages from the bus and clean up */
      gst_element_set_state(pipeline_, GST_STATE_NULL);
      record_failed_event_(-1, stream_id_);
      break;
    }
    case GST_MESSAGE_APPLICATION: {
      const GstStructure *s;

      syslog(LOG_DEBUG, "GSTAudioRecorder::handle_bus_msg - GST_MESSAGE_APPLICATION");

      s = gst_message_get_structure(msg);

      if (gst_structure_has_name(s, "StopGSTRecorder")) {
        syslog(LOG_DEBUG, "GSTAudioRecorder::handle_bus_msg - Stop GST Audio recording");
        ExecuteStopRecorder();
      }
      break;
    }
    default:
      break;
  }

  return TRUE;
}

GSTAudioRecorder::GSTAudioRecorder(gint streamid) {
  loop_ = NULL;
  source_ = NULL;
  sink_ = NULL;
  pipeline_ = NULL;
  stream_id_ = streamid;
  record_failed_event_ = NULL;
  get_buffer_event_ = NULL;
  callback_observer_ = NULL;
  log_filename_ = NULL;

  //Read VoiceUI Database
  auto voice_ui_config = voiceUIFramework::voiceUIUtils::VoiceUIConfig::getInstance();
  if (voice_ui_config) {
    //Read Voice Dump status
    voicedump_enable_ = voice_ui_config->ReadWithDefault<bool>(true, voiceUIFramework::voiceUIUtils::Keys::k_framework_voice_dump_status_);
    //Read Voice Dump Path
    voicedump_path_ = voice_ui_config->ReadWithDefault<std::string>("/data", voiceUIFramework::voiceUIUtils::Keys::k_framework_voice_dump_path_);
  } else {
    syslog(LOG_ERR, "VoiceUIConfig not Initialized");
    voicedump_enable_ = true;
    voicedump_path_ = "/data";
  }
  syslog(LOG_INFO, "GSTAudioRecorder::GSTAudioRecorder - Voice Dump Status = %d", voicedump_enable_);
}

GSTAudioRecorder::~GSTAudioRecorder() {
  syslog(LOG_ERR, "GSTAudioRecorder::~GSTAudioRecorder()- Destructor");
}

gint GSTAudioRecorder::InitRecorder(AudioProp *prop) {
  GstElement *capsfilter;
  GstCaps *filtercaps;
  GstBus *bus;

  //open file to dump voice packets
  if (voicedump_enable_) {
    std::string filepath = std::string(voicedump_path_ + "/pcmdump_gst_recorder");
    log_filename_ = fopen(filepath.c_str(), "wb");
  }

  if (!gst_init_check(NULL, NULL, NULL)) {
    syslog(LOG_ERR, "GSTAudioRecorder::StartRecognition() - gst_init_check() Fail!");
    return -1;
  }
  pipeline_ = gst_pipeline_new("audio-recorder");

  //source_ = gst_element_factory_make("qahwsrc", "recorder-source");
  source_ = gst_element_factory_make("pulsesrc", "recorder-source");

  if (!source_) {
    syslog(LOG_ERR, "GSTAudioRecorder::StartRecognition() - source_ == NULL");
    return -1;
  }

  g_object_set(G_OBJECT(source_), "blocksize", prop->bufsize,
      // g_object_set(G_OBJECT(source_), "blocksize", prop->bufsize, "ffv-state", prop->ffv_state, "ffv-ec-ref-dev", prop->ffv_ec_ref_dev, "ffv-channel", prop->ffv_channel_index,
      NULL);

  //set pulsesrc element device property as regular0
  g_object_set(G_OBJECT(source_), "device", PULSE_SRC_DEVICE, NULL);

  capsfilter = gst_element_factory_make("capsfilter", "filter");
  if (!capsfilter) {
    syslog(LOG_ERR, "GSTAudioRecorder::StartRecognition() - capsfilter == NULL");
    return -1;
  }

  sink_ = gst_element_factory_make("appsink", "sink");
  if (!sink_) {
    syslog(LOG_ERR, "GSTAudioRecorder::StartRecognition() - sink_ == NULL");
    return -1;
  }

  loop_ = g_main_loop_new(NULL, FALSE);
  bus = gst_pipeline_get_bus(GST_PIPELINE(pipeline_));
  bus_watch_ = gst_bus_add_watch(bus, handle_bus_msgs, this);
  gst_object_unref(bus);
  g_object_set(G_OBJECT(sink_), "emit-signals", TRUE, "sync", FALSE, NULL);
  g_signal_connect(sink_, "new-sample", G_CALLBACK(on_new_sample_from_sink), this);
  gst_bin_add_many(GST_BIN(pipeline_), source_, capsfilter, sink_, NULL);
  gst_element_link_many(source_, capsfilter, sink_, NULL);
  filtercaps = gst_caps_new_simple("audio/x-raw", "format", G_TYPE_STRING, prop->format, "layout", G_TYPE_STRING, "interleaved", "rate", G_TYPE_INT,
      prop->rate, "channels", G_TYPE_INT, prop->channels,
      NULL);
  g_object_set(G_OBJECT(capsfilter), "caps", filtercaps, NULL);
  gst_caps_unref(filtercaps);

  return 0;
}

gint GSTAudioRecorder::StartRecorder() {
  syslog(LOG_DEBUG, "GSTAudioRecorder::StartRecorder()");
  if (pipeline_) {
    gst_element_set_state(pipeline_, GST_STATE_PLAYING);
    if (gst_element_get_state(pipeline_, NULL, NULL, -1) == GST_STATE_CHANGE_FAILURE) {
      g_error("Failed to go into PLAYING state");
      return -1;
    }
    std::thread th1(&GSTAudioRecorder::runMainLoop, this);
    th1.detach();
    syslog(LOG_DEBUG, "**************GSTAudioRecorder::StartRecorder() - DONE");
    return 0;
  }
  return -1;
}

gint GSTAudioRecorder::StopRecorder() {
  syslog(LOG_DEBUG, "GSTAudioRecorder::StopRecorder()");

  bool retval = gst_element_post_message(GST_ELEMENT(pipeline_),
      gst_message_new_application(GST_OBJECT(pipeline_),
          gst_structure_new("StopGSTRecorder",
              "message", G_TYPE_STRING, "Rquest to Stop Recorder", NULL)));

  return retval ? 1 : 0;
}

gint GSTAudioRecorder::ExecuteStopRecorder() {
  syslog(LOG_DEBUG, "GSTAudioRecorder::ExecuteStopRecorder()");
  if (pipeline_) {
    GstState state;
    GstState pending;
    GstStateChangeReturn stateChangeRet = gst_element_get_state(pipeline_, &state, &pending, GST_CLOCK_TIME_NONE);
    if (GST_STATE_CHANGE_SUCCESS == stateChangeRet && GST_STATE_NULL == state) {
      syslog(LOG_DEBUG, "alreadyStopped");
    } else if (GST_STATE_CHANGE_ASYNC == stateChangeRet && GST_STATE_NULL == pending) {
      syslog(LOG_DEBUG, "alreadyStopping");
    } else {
      stateChangeRet = gst_element_set_state(pipeline_, GST_STATE_NULL);
      if (GST_STATE_CHANGE_FAILURE == stateChangeRet) {
        pipeline_ = NULL;
        return -1;
      } else if (GST_STATE_CHANGE_ASYNC == stateChangeRet) {
        syslog(LOG_DEBUG, "ExecuteStopRecorder Pending");
        return 0;
      } else {
      }
    }
    pipeline_ = NULL;

    //Close file containing voice packets
    if (voicedump_enable_) {
      fclose(log_filename_);
    }

    //Quit main loop
    Clear();

    syslog(LOG_DEBUG, "*************GSTAudioRecorder::StopRecorder() - DONE");
    return 0;
  }

  return -1;
}

void GSTAudioRecorder::Clear() {
  g_main_loop_quit(loop_);
}

void GSTAudioRecorder::callGetBufferEvent(guint8 *data, int size) {
  //get_buffer_event_(data, size, stream_id_);

  //syslog(LOG_DEBUG, "GSTAudioRecorder::callGetBufferEvent - Size =%d\n", size);
  //save audio in a file
  if (voicedump_enable_) {
    fwrite(data, 1, size, log_filename_);
  }

  if (callback_observer_) {
    callback_observer_->OnNewVoiceBuffer(data, size);
  }
}

void GSTAudioRecorder::SetObserver(std::shared_ptr<voiceUIFramework::audioRecorder::AudioRecorderCallBackInterface> callback_observer) {
  callback_observer_ = callback_observer;
}

void GSTAudioRecorder::runMainLoop() {
  g_main_loop_run(loop_);
  if (pipeline_) {
    gst_element_set_state(pipeline_, GST_STATE_NULL);
    gst_object_unref(GST_OBJECT(pipeline_));
    pipeline_ = NULL;
  }
}
