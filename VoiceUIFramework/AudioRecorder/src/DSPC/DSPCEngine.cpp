/*Copyright (c) 2019-2020, The Linux Foundation. All rights reserved.
*
*Redistribution and use in source and binary forms, with or without
*modification, are permitted provided that the following conditions are
*met:
*    * Redistributions of source code must retain the above copyright
*      notice, this list of conditions and the following disclaimer.
*    * Redistributions in binary form must reproduce the above
*      copyright notice, this list of conditions and the following
*      disclaimer in the documentation and/or other materials provided
*      with the distribution.
*    * Neither the name of The Linux Foundation nor the names of its
*      contributors may be used to endorse or promote products derived
*      from this software without specific prior written permission.
*
*THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
*WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
*MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
*ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
*BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
*BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
*WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
*OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
*IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    DSPCEngine.cpp
 *  @brief   brief description
 *
 *  DESCRIPTION
 *    description
 ***************************************************************/

#include <AudioRecorder/DSPC/DSPCEngine.h>

namespace voiceUIFramework {
namespace audioRecorder {

const char awbFilename[] = "/data/voice-ui-framework/qc-dspc-engine.awb";

/** The socket listener. */
static CTcpIO2 *ptcpIO_ = 0;
/** Command buffer. */
static UINT32 commandBuf[MAX_COMMAND_BUFFER_LEN];
/** Partial message offset in bytes. */
static UINT32 s_partial = 0;
/* If non-zero, length of partial message in words. */
static UINT32 s_pktLen = 0;

/**
 * @brief Handle AWE server messages.
 * @param [in] pAwe             the library instance
 */
static void NotifyFunction(void *pAwe, int count) {
  CAWELib *pAwelib = (CAWELib *)pAwe;
  SReceiveMessage *pCurrentReplyMsg = ptcpIO_->BinaryGetMessage();
  const int msglen = pCurrentReplyMsg->size();

  if (msglen != 0) {
    // This is a binary message.
    UINT32 *txPacket_Host = (UINT32 *)(pCurrentReplyMsg->GetData());
    if (txPacket_Host) {
      UINT32 first = txPacket_Host[0];
      UINT32 len = (first >> 16);
      if (s_pktLen) {
        len = s_pktLen;
      }
      if (len > MAX_COMMAND_BUFFER_LEN) {
        syslog(LOG_INFO, "DSPCEngine::NotifyFunction count=%d msglen=%d\n",
            count, msglen);
        syslog(LOG_INFO, "DSPCEngine::NotifyFunction NotifyFunction: packet 0x%08x len %d too big (max %d)\n", first,
            len, MAX_COMMAND_BUFFER_LEN);
        exit(1);
      }
      if (len * sizeof(int) > s_partial + msglen) {
        // Paxket is not complete, copy partial words.
        //              printf("Partial message bytes=%d len=%d s_partial=%d\n",
        //                  msglen, len, s_partial);
        memcpy(((char *)commandBuf) + s_partial, txPacket_Host, msglen);
        s_partial += msglen;
        s_pktLen = len;
        return;
      }
      memcpy(((char *)commandBuf) + s_partial, txPacket_Host, msglen);
      s_partial = 0;
      s_pktLen = 0;

      // AWE processes the message.
      int ret = pAwelib->SendCommand(commandBuf, MAX_COMMAND_BUFFER_LEN);
      if (ret < 0) {
        syslog(LOG_INFO, "DSPCEngine::NotifyFunction SendCommand failed with %d\n", ret);
        exit(1);
      }

      // Verify sane AWE reply.
      len = commandBuf[0] >> 16;
      if (len > MAX_COMMAND_BUFFER_LEN) {
        syslog(LOG_INFO, "DSPCEngine::NotifyFunction NotifyFunction: reply packet len %d too big (max %d)\n",
            len, MAX_COMMAND_BUFFER_LEN);
        exit(1);
      }
      ret = ptcpIO_->SendBinaryMessage("", -1, (BYTE *)commandBuf, len * sizeof(UINT32));
      if (ret < 0) {
        syslog(LOG_INFO, "DSPCEngine::NotifyFunctionNotifyFunction: SendBinaryMessage failed with %d\n", ret);
        exit(1);
      }
    } else {
      syslog(LOG_INFO, "DSPCEngine::NotifyFunctionNotifyFunction: impossible no message pointer\n");
      exit(1);
    }
  } else {
    printf("NotifyFunction: illegal zero klength message\n");
    exit(1);
  }
}

DSPCEngine::DSPCEngine()
    : awelib_{nullptr}, inCount_{0}, outCount_{0}, numInSamples_{0}, numInChannels_{0}, numOutSamples_{0}, numOutChannels_{0}, isInComplex_{0}, isOutComplex_{0}, wireId1_{1}, wireId2_{2}, layoutId_{0}, currentVRState_{false} {
  syslog(LOG_INFO, "DSPCEngine::DSPCEngine()");
}

DSPCEngine::~DSPCEngine() {
  syslog(LOG_INFO, "DSPCEngine::~DSPCEngine()");
}

int DSPCEngine::Initialize(std::vector<int> &inSamples, std::vector<int> &outSamples, unsigned int *number_of_output_channels) {
  syslog(LOG_INFO, "DSPCEngine::Initialize()");
  int error = 0;
  int blockIx;

  // Create an AWELib instance
  awelib_ = AWELibraryFactory();
  error = awelib_->CreateEx("qc_ec_ns", 1e9f, 1e9f, true);

  if (error < 0) {
    syslog(LOG_INFO, "DSPCEngine::Initialize() - Create AWE failed with %d\n", error);
    delete awelib_;
    return error;
  }
  syslog(LOG_INFO, "DSPCEngine::Initialize() - Using library '%s'\n", awelib_->GetLibraryVersion());

  // Load a layout
  {
    UINT32 pos = 0;
    FILE *fin = fopen(awbFilename, "rb");
    if (!fin) {
      syslog(LOG_INFO, "DSPCEngine::Initialize() - can't open file [%s]\n", awbFilename);
      return -1;
    }
    fseek(fin, 0, SEEK_END);
    UINT32 nFileLenInWords = ftell(fin) / sizeof(UINT32);
    fseek(fin, 0, SEEK_SET);
    UINT32 *buffer = (UINT32 *)malloc(nFileLenInWords * sizeof(UINT32));
    if (fread(buffer, sizeof(UINT32), nFileLenInWords, fin) != nFileLenInWords) {
      syslog(LOG_INFO, "DSPCEngine::Initialize() - read of [%s] failed\n", awbFilename);
      fclose(fin);
      free(buffer);
      return -1;
    }
    fclose(fin);
    error = awelib_->LoadAwbMem(buffer, nFileLenInWords, &pos);
    if (error < 0) {
      syslog(LOG_INFO, "DSPCEngine::Initialize() - LoadAwbMem on [%s] failed with %d at word offset %d\n", awbFilename, error, pos);
      fclose(fin);
      free(buffer);
      return error;
    }
    free(buffer);
    syslog(LOG_INFO, "DSPCEngine::Initialize() - LoadAwbFile on [%s] successful\n", awbFilename);
  }

  // Get pin properties
  inCount_ = 0;
  outCount_ = 0;
  numInSamples_ = 0;
  numInChannels_ = 0;
  numOutSamples_ = 0;
  numOutChannels_ = 0;
  isInComplex_ = 0;
  isOutComplex_ = 0;

  wireId1_ = 1;   // default value
  wireId2_ = 2;   // default value
  layoutId_ = 0;  // default value

  error = awelib_->PinProps(numInSamples_, numInChannels_, isInComplex_, wireId1_, numOutSamples_, numOutChannels_,
      isOutComplex_, wireId2_);

  if (error < 0) {  // Error
    syslog(LOG_INFO, "DSPCEngine::Initialize() - PinProps failed with %d\n", error);
    return error;
  } else if (error > 0) {  // We have a layout
    if (isInComplex_)
      numInChannels_ *= 2;
    if (isOutComplex_)
      numOutChannels_ *= 2;
    inCount_ = numInSamples_ * numInChannels_;
    outCount_ = numOutSamples_ * numOutChannels_;
    syslog(LOG_INFO, "DSPCEngine::Initialize() - In: %d samples, %d chans ID=%d; out: %d samples, %d chans ID=%d\n", numInSamples_, numInChannels_,
        wireId1_, numOutSamples_, numOutChannels_, wireId2_);

    //Store number of output channels defined by DSPC engine
    *number_of_output_channels = numOutChannels_;
    syslog(LOG_INFO, "DSPCEngine::Initialize() - Number of output channels for selected DSPC engine =%d", *number_of_output_channels);

  } else {  // We have no layout
    syslog(LOG_INFO, "DSPCEngine::Initialize() - PinProps found no layout\n");
  }

  awelib_->SetLayoutAddresses(wireId1_, wireId2_, layoutId_);

  // Listen for AWE server on 15002.
  UINT32 listenPort = 15002;
  ptcpIO_ = new CTcpIO2();
  ptcpIO_->SetNotifyFunction(awelib_, NotifyFunction);
  syslog(LOG_INFO, "DSPCEngine::Initialize() - Setting: NotifyFunction()");
  HRESULT hr = ptcpIO_->CreateBinaryListenerSocket(listenPort);
  if (FAILED(hr)) {
    syslog(LOG_INFO, "DSPCEngine::Initialize() Could not open socket on %d failed with %d\n",
        listenPort, hr);
    delete ptcpIO_;
    return -1;
  }

  //Create and size buffers.
  inSamples.resize(inCount_);
  outSamples.resize(outCount_);

  syslog(LOG_INFO, "DSPCEngine::Initialize() - inSamples.size() = %d - outSamples.size() = %d", inSamples.size(), outSamples.size());
  syslog(LOG_INFO, "DSPCEngine::Initialize() - inCount = %d, outCount = %d", inCount_, outCount_);

  //reset input  vectors
  memset(&inSamples[0], 0, inCount_ * sizeof(int));

  // Process the input data
  syslog(LOG_INFO, "DSPCEngine::Initialize() - Ready to process Audio");

  return 0;
}

//TODO store vectors on INITIALIZATION
int DSPCEngine::PumpAudio(std::vector<int> &in_samples, std::vector<int> &out_samples) {
  int error = 0;

  error = awelib_->PinProps(numInSamples_, numInChannels_, isInComplex_, wireId1_, numOutSamples_, numOutChannels_,
      isOutComplex_, wireId2_);

  if (awelib_ == nullptr) {
    syslog(LOG_ERR, "DSPCEngine::PumpAudio() - DSPC engine is NULL");
    return -1;
  } else {
    if (error > 0) {
      // Pump samples to DSPC Engine
      error = awelib_->PumpAudio(&in_samples[0], &out_samples[0], inCount_, outCount_);

      if (error < 0) {
        syslog(LOG_INFO, "DSPCEngine::PumpAudio() -PumpAudio failed, error = %d", error);
      }
    } else {
      syslog(LOG_INFO, "DSPCEngine::PumpAudio() - Do not PUMP - PinProp ERROR = %d", error);
    }
  }

  return error;
}

void DSPCEngine::setVRStatus(bool status) {
  if (currentVRState_ != status) {
    currentVRState_ = status;
    syslog(LOG_INFO, "DSPCEngine::setVRStatus() - %d", status);

    static Sample AWE_VRState[AWE_VRState_value_SIZE];
    AWE_VRState[0].iVal = {(unsigned int)status};

    //SET new value
    int ret = awelib_->SetValues(MAKE_ADDRESS(AWE_VRState_ID, AWE_VRState_value_OFFSET), AWE_VRState_value_SIZE, AWE_VRState);
    syslog(LOG_INFO, "DSPCEngine::setVRStatus() - setVRStatus status=%d, error = %d\n", status, ret);

    //Fetch new value
    Sample AWE_VRState_read[AWE_VRState_value_SIZE];
    int error = awelib_->FetchValues(MAKE_ADDRESS(AWE_VRState_ID, AWE_VRState_value_OFFSET),
        AWE_VRState_value_SIZE,
        AWE_VRState_read);
    syslog(LOG_INFO, "DSPCEngine::setVRStatus() - *** FetchVRStatus newValue=%d", AWE_VRState_read->iVal);
  }
}

int DSPCEngine::GetDoaDirection() {
  syslog(LOG_INFO, "DSPCEngine::GetDoaDirection()");
}

void DSPCEngine::Shutdown() {
  syslog(LOG_INFO, "DSPCEngine::Shutdown()");
  if (awelib_ != nullptr)
    delete awelib_;
  awelib_ = nullptr;

  syslog(LOG_DEBUG, "DSPCEngine::Shutdown() - DONE");
}

}  // namespace audioRecorder
}  // namespace voiceUIFramework
