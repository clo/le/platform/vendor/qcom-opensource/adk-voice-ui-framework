/* Copyright (c) 2018, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    QAHWManager.cpp
 *  @brief   Audio recorder client for Qualcomm Technologies, Inc LE Platform
 *
 *  DESCRIPTION
 *    Handles audio recorder sessions for LE platforms interacting with Audio HAl.
 ***************************************************************/

#include <AudioRecorder/QAHWManager.h>

namespace voiceUIFramework {
namespace audioRecorder {

using namespace voiceUIFramework::wakeword;

QAHWManager::QAHWManager(std::shared_ptr<WakeWordInterface> wakeword_engine, bool always_on, VoiceUIClientID client_id)
    : BaseAudioRecorder(wakeword_engine, always_on), media_recorder_{nullptr}, client_id_{client_id} {
  syslog(LOG_INFO, "+++++++QAHWManager Constructor called");
}

QAHWManager::~QAHWManager() {
  syslog(LOG_INFO, "------QAHWManager Destructor called");
}

int QAHWManager::Initialize() {
  syslog(LOG_INFO, "QAHWManager::Initialize()");
  return 0;
}

int QAHWManager::StartRecording(int channelIndex) {
  syslog(LOG_INFO, "QAHWManager::StartRecording() - Is recording? %d\n", recording_);

  int err = 0;
  if (callback_observer_) {
    if (!callback_observer_->InitializeRecognition())
      return -1;
  }
  if (!always_on_) {
    if (wakeword_engine_) {
      err = wakeword_engine_->StopRecognition();
      if (err != 0)
        return err;
    }
  }

  AudioProp prop;
  if (recording_ == false) {
    recording_ = true;

    //if media recorder already exist delete old instance.
    if (media_recorder_) {
      delete media_recorder_;
    }

    //Create media recorder using ID clientID as streamID
    media_recorder_ = new GSTAudioRecorder(ClientIDToIndex(client_id_));
    media_recorder_->SetObserver(callback_observer_);

    //Audio format: 16000 Hz - 1 channel - 16 bit - Little-endian
    prop.rate = 16000;
    prop.channels = 1;
    prop.bufsize = 320;
    prop.format = "S16LE";
    prop.ffv_state = 1;
    //        //prop.ffv_ec_ref_dev = MediaPlayer::getCurrentDeviceId();
    //        prop.ffv_ec_ref_dev = 0x200000;
    //        // TODO: GET DEVICEID
    //        if (prop.ffv_ec_ref_dev == 0x20000)
    //            prop.ffv_ec_ref_dev = 0x200000;
    //        prop.ffv_channel_index = channelIndex;

    err = media_recorder_->InitRecorder(&prop);
    if (err < 0) {
      syslog(LOG_ERR, "QAHWManager::StartRecording() - FAILED InitRecorder() = %d\n", err);
    } else {
      err = media_recorder_->StartRecorder();
      if (err < 0) {
        syslog(LOG_ERR, "QAHWManager::StartRecording() - FAILED StartRecorder() = %d\n", err);
      }
    }
  } else {
    syslog(LOG_ERR, "QAHWManager::StartRecording() - FAILED Audio recording ALREADY running");
    //err = 1;
  }
  syslog(LOG_INFO, "QAHWManager::StartRecording() - DONE! ");
  return err;
}

int QAHWManager::StopRecording() {
  int err = 0;
  syslog(LOG_INFO, "QAHWManager::StopRecording() - Is recording? %d\n", recording_);
  if (recording_) {
    recording_ = false;
    err = media_recorder_->StopRecorder();
    if (err < 0) {
      syslog(LOG_ERR, "QAHWManager::StopRecording() - FAILED StopRecognition() = %d", err);
    } else {
      if (!always_on_) {
        if (wakeword_engine_) {
          syslog(LOG_INFO, "QAHWManager::StopRecording() - always_on_ = FALSE - RESTARTING KeyWord detection");
          err = wakeword_engine_->StartRecognition();
        }
      }
    }
  } else {
    syslog(LOG_WARNING, "QAHWManager::StopRecording() - Warning: Audio recording NOT running");
  }
  syslog(LOG_INFO, "QAHWManager::StopRecording() - DONE! ");
  return err;
}

void QAHWManager::Shutdown() {
  syslog(LOG_DEBUG, "QAHWManager::Shutdown() - Audio Recorder from: %s", voiceUIUtils::ClientIDToString(client_id_).c_str());

  //delete media recorder instance
  if (media_recorder_) {
    delete media_recorder_;
  }
}

}  // namespace audioRecorder
}  // namespace voiceUIFramework
