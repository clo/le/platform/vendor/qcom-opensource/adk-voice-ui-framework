/*Copyright (c) 2019-2020, The Linux Foundation. All rights reserved.
*
*Redistribution and use in source and binary forms, with or without
*modification, are permitted provided that the following conditions are
*met:
*    * Redistributions of source code must retain the above copyright
*      notice, this list of conditions and the following disclaimer.
*    * Redistributions in binary form must reproduce the above
*      copyright notice, this list of conditions and the following
*      disclaimer in the documentation and/or other materials provided
*      with the distribution.
*    * Neither the name of The Linux Foundation nor the names of its
*      contributors may be used to endorse or promote products derived
*      from this software without specific prior written permission.
*
*THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
*WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
*MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
*ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
*BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
*BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
*WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
*OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
*IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    ThirdPartyAudioRecorder.cpp
 *  @brief   Audio recorder client for Third Party Engines
 *
 *  DESCRIPTION
 *    Handles audio recorder sessions for Third Party Engines on LE platforms
 ***************************************************************/

#include <AudioRecorder/ThirdPartyAudioRecorder.h>

namespace voiceUIFramework {
namespace audioRecorder {

using namespace voiceUIFramework::wakeword;

ThirdPartyAudioRecorder::ThirdPartyAudioRecorder(std::shared_ptr<WakeWordInterface> wakeword_engine, bool always_on, VoiceUIClientID client_id)
    : BaseAudioRecorder(wakeword_engine, always_on), client_id_{client_id}, recording_loop_{false}, voicedump_enable_{false}, number_of_mic_channels_{0}, tap_or_multiturn_session_running_{false}, foutput_{nullptr}, voiceui_dump_file_{nullptr}, f_samples_pa_int_{nullptr} {
  syslog(LOG_INFO, "+++++++ThirdPartyAudioRecorder Constructor called");

  //Read VoiceUI Database
  auto voice_ui_config = voiceUIFramework::voiceUIUtils::VoiceUIConfig::getInstance();
  if (voice_ui_config) {
    //Read number of microphones
    number_of_mic_channels_ = voice_ui_config->ReadWithDefault<int>(MIC_CHANNELS, voiceUIFramework::voiceUIUtils::Keys::k_framework_num_of_mics_);
    //Read number of EC channels
    number_of_ec_channels_ = voice_ui_config->ReadWithDefault<int>(EC_CHANNELS, voiceUIFramework::voiceUIUtils::Keys::k_framework_num_of_ec_channels_);
    //Read BUFSIZE required by thirdparty
    third_party_bufsize_ = voice_ui_config->ReadWithDefault<int>(BUFSIZE, voiceUIFramework::voiceUIUtils::Keys::k_third_party_bufsize);
    //Read Voice Dump status
    voicedump_enable_ = voice_ui_config->ReadWithDefault<bool>(true, voiceUIUtils::Keys::k_framework_voice_dump_status_);
    //Read Voice Dump Path
    voicedump_path_ = voice_ui_config->ReadWithDefault<std::string>("/data", voiceUIUtils::Keys::k_framework_voice_dump_path_);
  } else {
    syslog(LOG_ERR, "ThirdPartyAudioRecorder::Initialize() - VoiceUIConfig not Initialized");
    number_of_mic_channels_ = MIC_CHANNELS;
    number_of_ec_channels_ = EC_CHANNELS;
    third_party_bufsize_ = BUFSIZE;
    voicedump_enable_ = true;
    voicedump_path_ = "/data";
  }
  syslog(LOG_INFO, "ThirdPartyAudioRecorder::ThirdPartyAudioRecorder - Voice Dump Status = %d / Num of MICS= %d / Num of EC Chs= %d", voicedump_enable_, number_of_mic_channels_, number_of_ec_channels_);
}

ThirdPartyAudioRecorder::~ThirdPartyAudioRecorder() {
  syslog(LOG_INFO, "------ThirdPartyAudioRecorder Destructor called");
}

int ThirdPartyAudioRecorder::Initialize() {
  syslog(LOG_INFO, "ThirdPartyAudioRecorder::Initialize()");

  dspc_engine_ = std::make_shared<DSPCEngine>();

  //store of output channels that DSPC engine configuration is using
  unsigned int number_of_output_channels;
  if (dspc_engine_->Initialize(inSamples_, outSamples_, &number_of_output_channels) != 0) {
    syslog(LOG_INFO, "ThirdPartyAudioRecorder::Initialize() - DSPCEngine FAILED to Initialize.");
    return -1;
  }

  syslog(LOG_INFO, "ThirdPartyAudioRecorder::Initialize() - dspc_engine_->Initialize done! - inSamples_.size() = %d - outSamples_.size() = %d", inSamples_.size(), outSamples_.size());

  //Launch Recording Thread
  client_thread_ = std::thread([&]() {
    //Utils for audio demux
    auto thirdparty_audio_util =
        voiceUIFramework::voiceUIUtils::ThirdPartyAudioUtils::getInstance();

    //Configure number of output channels
    thirdparty_audio_util->setNumOfOutputChannels(number_of_output_channels);

    //Start audio recording using pulseAudioRecorder
    /* The sample type to use */
    static const pa_sample_spec ss = {
        .format = PA_SAMPLE_S32LE,
        .rate = 48000,
        .channels = number_of_mic_channels_ + number_of_ec_channels_};
    pa_simple* s = NULL;

    //crate channel mapping
    static pa_channel_map map;
    //populate channel mapping
    pa_channel_map_init_extend(&map, ss.channels, PA_CHANNEL_MAP_DEFAULT);

    char tss[PA_SAMPLE_SPEC_SNPRINT_MAX], tcm[PA_CHANNEL_MAP_SNPRINT_MAX];
    pa_sample_spec_snprint(tss, sizeof(tss), &ss);
    pa_channel_map_snprint(tcm, sizeof(tcm), &map);
    syslog(LOG_INFO, "ThirdPartyAudioRecorder::Recording - Starting RECORDING stream with sample specification %s and channel map %s.", tss, tcm);

    int error;
    //Total number of captured channels (MICS + EC)
    int number_of_input_channels = (number_of_mic_channels_ + number_of_ec_channels_);
    //VoiceBuffer input from Pulse Client
    uint8_t input_buf_[third_party_bufsize_ * number_of_input_channels * sizeof(int)];  //Reason of * sizeof(int) - Data from pa_simple_read comes as uint8 instead of int(32 bits).

    /* Create the recording stream */
    if (!(s = pa_simple_new(NULL, "ThirdPartyAudioRecorder", PA_STREAM_RECORD, "regular-builtin-mic-ec-ref-loopback0", "record", &ss, (const pa_channel_map*)&map, NULL, &error))) {
      syslog(LOG_INFO, "ThirdPartyAudioRecorder::Recording Thread - pa_simple_new() failed: %s\n", pa_strerror(error));
    } else {
      recording_loop_ = true;
    }

    bool files_are_opened = false;

    while (recording_loop_) {
      int error;

      /* Record some data ... */
      if (pa_simple_read(s, input_buf_, sizeof(input_buf_), &error) < 0) {
        syslog(LOG_INFO, "ThirdPartyAudioRecorder::Recording Thread  - pa_simple_read() failed: %s\n", pa_strerror(error));
        recording_loop_ = false;
      }

      //Read data as INT (WORD - 2 bytes)
      int* input_buf_int_ = (int*)&input_buf_;

      //Clear inSamples vector from previous interaction.
      inSamples_.clear();
      inSamples_.insert(inSamples_.begin(), &input_buf_int_[0], &input_buf_int_[third_party_bufsize_ * number_of_input_channels]);

      //Process Voice Buffers
      dspc_engine_->PumpAudio(inSamples_, outSamples_);

      //check if tap or multi turn session is running if not feed wakeword engine
      if (tap_or_multiturn_session_running_) {
        //convert form 48Khz_32bits to 16Khz_16bits
        std::vector<uint8_t> data = thirdparty_audio_util->Demux16khz16bitpcm(outSamples_, NUMOF_ASR_CHANNEL_);

        //save audiodump in a files
        if (voicedump_enable_) {
          if (!files_are_opened) {
            syslog(LOG_INFO, "ThirdPartyAudioRecorder::Recording Thread for TAP/MultiTurn  - Opening Voice Dumping files");
            //open files to dump voice packets
            //foutput_ = fopen("/data/pcmdump_output_dspc", "wb");
            //f_samples_pa_int_ = fopen("/data/pcmdump_pa_input_dspc", "wb");
            std::string filepath = std::string(voicedump_path_ + "/pcmdump_3P_tap_multi");
            voiceui_dump_file_ = fopen(filepath.c_str(), "wb");

            files_are_opened = true;
          }
          //write output samples
          //fwrite(input_buf_int_, sizeof(int), third_party_bufsize_ * number_of_input_channels, f_samples_pa_int_);
          //fwrite(&outSamples_[0], sizeof(int), third_party_bufsize_ * outSamples_.size(), foutput_);
          fwrite(&data[0], sizeof(uint8_t), data.size(), voiceui_dump_file_);
        }
        if (callback_observer_) {
          //Sending to voiceUi client
          callback_observer_->OnNewVoiceBuffer(&data[0], data.size());
        }
      } else {
        //Close files containing voice packets
        if (voicedump_enable_) {
          if (files_are_opened) {
            syslog(LOG_INFO, "ThirdPartyAudioRecorder::Recording Thread - Closing Voice Dumping files for TAP/MultiTurn");
            //fclose(foutput_);
            //fclose(f_samples_pa_int_);
            fclose(voiceui_dump_file_);

            files_are_opened = false;
          }
        }
        //Feed Wakework Engine with outSamples_
        if (wakeword_engine_ != nullptr) {
          bool key_word_hit = wakeword_engine_->Feed(outSamples_);
          //Set VR State
          dspc_engine_->setVRStatus(key_word_hit);
        }
      }
    }
    syslog(LOG_INFO, "ThirdPartyAudioRecorder::Recording Thread Stopped!");
  });

  syslog(LOG_INFO, "ThirdPartyAudioRecorder::Initialize() - Done.");
  return 0;
}

int ThirdPartyAudioRecorder::StartRecording(int channelIndex) {
  syslog(LOG_INFO, "ThirdPartyAudioRecorder::StartRecording() - Is recording? %d\n", recording_);

  int err = 0;

  if (recording_ == false) {
    recording_ = true;

    if (callback_observer_) {
      if (!callback_observer_->InitializeRecognition())
        return -1;
    }

    //Enable flag to indicate that TAP or multi turn session is running.
    tap_or_multiturn_session_running_ = true;
    //Indicate DSPC that ASR session is ON
    dspc_engine_->setVRStatus(tap_or_multiturn_session_running_);

  } else {
    syslog(LOG_ERR, "ThirdPartyAudioRecorder::StartRecording() - Warning: TAP/MultiTurn Audio recording ALREADY running");
    //err = 1;
  }
  syslog(LOG_INFO, "ThirdPartyAudioRecorder::StartRecording() - DONE! ");
  return err;
}

int ThirdPartyAudioRecorder::StopRecording() {
  int err = 0;
  syslog(LOG_INFO, "ThirdPartyAudioRecorder::StopRecording() - Is recording? %d\n", recording_);
  if (recording_) {
    recording_ = false;

    //Disable flag to indicate that TAP or multi turn session is running.
    tap_or_multiturn_session_running_ = false;
    //Indicate DSPC that ASR session is OFF
    dspc_engine_->setVRStatus(tap_or_multiturn_session_running_);

  } else {
    syslog(LOG_WARNING, "ThirdPartyAudioRecorder::StopRecording() - Warning: TAP/MultiTurn Audio recording NOT running");
  }
  syslog(LOG_INFO, "ThirdPartyAudioRecorder::StopRecording() - DONE! ");
  return err;
}

void ThirdPartyAudioRecorder::Shutdown() {
  syslog(LOG_DEBUG, "ThirdPartyAudioRecorder::Shutdown() - Audio Recorder from: %s", voiceUIUtils::ClientIDToString(client_id_).c_str());

  //Stop audio recorder
  recording_loop_ = false;

  //Join Client Thread to main Thread
  if (client_thread_.joinable()) {
    client_thread_.join();
  }

  //Shutdown DSPC Engine
  dspc_engine_->Shutdown();
}

}  // namespace audioRecorder
}  // namespace voiceUIFramework
