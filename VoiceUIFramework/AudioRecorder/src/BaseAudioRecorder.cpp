/* Copyright (c) 2018, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    QAHWManager.cpp
 *  @brief   Audio recorder client for Qualcomm Technologies, Inc LE Platform
 *
 *  DESCRIPTION
 *    Handles audio recorder sessions for LE platforms interacting with Audio HAl.
 ***************************************************************/

#include <AudioRecorder/BaseAudioRecorder.h>

namespace voiceUIFramework {
namespace audioRecorder {

using namespace voiceUIFramework::wakeword;

BaseAudioRecorder::BaseAudioRecorder(std::shared_ptr<WakeWordInterface> wakeword_engine, bool always_on)
    : wakeword_engine_{wakeword_engine}, recording_{false}, always_on_{always_on} {
  syslog(LOG_INFO, "+++++++BaseAudioRecorder Constructor called");
}

BaseAudioRecorder::~BaseAudioRecorder() {
  syslog(LOG_INFO, "------BaseAudioRecorder Destructor called");
}

int BaseAudioRecorder::Initialize() {
  return 0;
}

int BaseAudioRecorder::StartRecording(int channelIndex) {
  syslog(LOG_INFO, "BaseAudioRecorder::StartRecording() - Is recording? %d\n", recording_);

  int err = 0;
  if (callback_observer_) {
    if (!callback_observer_->InitializeRecognition())
      return -1;
  }
  if (!always_on_) {
    if (wakeword_engine_ != nullptr) {
      err = wakeword_engine_->StopRecognition();
      if (err != 0)
        return err;
    }
  }

  if (recording_ == false) {
    recording_ = true;
    syslog(LOG_INFO, "BaseAudioRecorder::StartRecording() - Recording...");
  } else {
    syslog(LOG_ERR, "BaseAudioRecorder::StartRecording() - FAILED Audio recording ALREADY running");
  }

  return err;
}

int BaseAudioRecorder::StopRecording() {
  int err = 0;
  syslog(LOG_INFO, "BaseAudioRecorder::StopRecording() - Is recording? %d\n", recording_);

  if (recording_) {
    recording_ = false;
    syslog(LOG_INFO, "BaseAudioRecorder::StopRecording() - Done.");

    if (!always_on_) {
      syslog(LOG_INFO, "BaseAudioRecorder::StopRecording() - always_on_ = TRUE - RESTARTING KeyWord detection");
      if (wakeword_engine_ != nullptr) {
        err = wakeword_engine_->StartRecognition();
      }
    } else {
      syslog(LOG_WARNING, "BaseAudioRecorder::StopRecording() - Warning: Audio recording NOT running");
    }
  }
  return err;
}

bool BaseAudioRecorder::IsRecording() {
  return recording_;
}

bool BaseAudioRecorder::IsAlwaysOn() {
  return always_on_;
}

void BaseAudioRecorder::SetCallBackObserver(std::shared_ptr<AudioRecorderCallBackInterface> callback_observer) {
  syslog(LOG_INFO, "BaseAudioRecorder::SetCallBackObserver()");
  callback_observer_ = callback_observer;
}

void BaseAudioRecorder::SetWakewordEngine(std::shared_ptr<voiceUIFramework::wakeword::WakeWordInterface> wakeword_engine) {
  wakeword_engine_ = wakeword_engine;
}

void BaseAudioRecorder::Shutdown() {
  syslog(LOG_INFO, "BaseAudioRecorder::Shutdown()");
}

}  // namespace audioRecorder
}  // namespace voiceUIFramework
