/* Copyright (c) 2018-2019, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    VoiceUIAudioRecorderTest.cpp
 *  @brief   brief description
 *
 *  DESCRIPTION
 *    description
 ***************************************************************/

#include <syslog.h>
#include <iostream>
#include <memory>
#include <string>
#include <thread>

#include <AudioRecorder/BaseAudioRecorder.h>
#include <VoiceUIUtils/GlobalValues.h>
#include <Wakeword/WakeWordInterface.h>
#include <AudioRecorder/QAHWManager.h>


#include "gtest/gtest.h"

using namespace std;
using namespace voiceUIFramework::voiceUIUtils;
using namespace voiceUIFramework::wakeword;
using namespace voiceUIFramework::audioRecorder;

namespace {

enum class Result {
  fail = -1,
  success = 0
};

template <typename E>
constexpr typename std::underlying_type<E>::type to_underlying(E e) noexcept {
  return static_cast<typename std::underlying_type<E>::type>(e);
}

class WakeWordMock : public WakeWordInterface {
 public:
  WakeWordMock() {
    recognizing_ = false;
  }

  ~WakeWordMock() {
    // You can do clean-up work that doesn't throw exceptions here.
  }

  //initializes wake word engine
  int Initialize() {
    syslog(LOG_INFO, "WakeWordMock::Initialize()");
    return 0;
  }
  //Starts wakeword engine
  virtual int StartRecognition() {
    syslog(LOG_DEBUG, "WakeWordMock::StartRecognition() - Expected_result = %d", expected_result_);
    if (expected_result_ == to_underlying(Result::success)) {
      recognizing_ = true;
    }
    return expected_result_;
  }
  //Stop wakeword engine
  virtual int StopRecognition() {
    syslog(LOG_DEBUG, "WakeWordMock::StopRecognition() - Expected_result = %d", expected_result_);
    if (expected_result_ == to_underlying(Result::success)) {
      recognizing_ = false;
    }
    return expected_result_;
  }
  //Stop audio capture (LAB) once keyword is detected
    virtual void StopAudioCapture(
            VoiceUIClientID client,
            StopCaptureResult capture_data) {
  }
  //shutdown wake word engine
  virtual void Shutdown() {
  }
  //keyword detected notification
  virtual void KeywordRecognized() {
  }
  //Starts wakeword engine for all clients
  virtual int StartRecognition(VoiceUIClientID client) {
    return 0;
  }
  //Stops wakeword engine for all clients
  virtual int StopRecognition(VoiceUIClientID client) {
    return 0;
  }
  //
  virtual bool AddObserver(VoiceUIClientID client, std::shared_ptr<WakeWordCallBackInterface> observer) {
    return true;
  }
  //
  virtual bool RemoveObserver(VoiceUIClientID client) {
    return true;
  }
  //
  virtual int GetDOADirection(VoiceUIClientID client_id) {
   return 0;
  }
  //
  virtual int GetChannelIndex(VoiceUIClientID client_id) {
   return 0;
  }


  //mock function to change expected result from start/stop Wakeword
  void SetExpectedResult(int expected_result) {
    expected_result_ = expected_result;
  }
  //mock function to get Recognizing State
  bool IsRecognizing() {
    syslog(LOG_DEBUG, "WakeWordMock::IsRecognizing() - Recognizing_ = %d", recognizing_);
    return recognizing_;
  }

  //mock function to restart Wakeword engine
  void ReinitializeWakeWord() {
    syslog(LOG_DEBUG, "WakeWordMock::ReinitializeWakeWord()");
    expected_result_ = to_underlying(Result::success);
    StartRecognition();
  }

 private:
  int expected_result_ = to_underlying(Result::success);
  bool recognizing_;
};

class MockAudioRecorderObserver : public AudioRecorderCallBackInterface {
 public:
  MockAudioRecorderObserver() {
    recognition_status_ = true;
  }

  ~MockAudioRecorderObserver() {
  }

  /* AudioRecorderCallBackInterface */
  void OnNewVoiceBuffer(uint8_t *buf, uint32_t size) {
    voice_packet += size;
  }
  bool InitializeRecognition() {
    return recognition_status_;
  }

  //Mock functions Implementation
  void SetRecoginitionStatus(bool status) {
    recognition_status_ = status;
  }

  //Get # of voice packets read
  int GetVoicePacketNumber() {
    syslog(LOG_DEBUG, "***** MockAudioRecorderObserver::GetVoicePacketNumber - voice_packet = %d", voice_packet);
    return voice_packet;
  }

 private:
  bool recognition_status_;
  int voice_packet = 0;
};

// The fixture for testing class VoiceUIManager.
class VoiceUIAudioRecorderTest : public ::testing::Test {
 protected:
  VoiceUIAudioRecorderTest() {
    // You can do set-up work for each test here.
  }

  virtual ~VoiceUIAudioRecorderTest() {
    // You can do clean-up work that doesn't throw exceptions here.
  }

  // If the constructor and destructor are not enough for setting up
  // and cleaning up each test, you can define the following methods:

  virtual void SetUp() {
    wake_word_mock_ = std::make_shared<WakeWordMock>();
  }

  virtual void TearDown() {
  }

  std::shared_ptr<WakeWordMock> wake_word_mock_;

  // Objects declared here can be used by all tests in the test case for VoiceUIManager.
};

// Test AudioRecorder creation for both AlwaysOn and non-AlwaysOn Voice UI Clients.
TEST_F(VoiceUIAudioRecorderTest, MethodTestCreateAudioRecorder) {
  syslog(LOG_INFO, "***** VoiceUIAudioRecorderTest::MethodTestCreateAudioRecorder - START");
  bool always_on = true;

  std::shared_ptr<AudioRecorderInterface> audio_recorder_always_on = std::make_shared<BaseAudioRecorder>(wake_word_mock_, always_on);
  ASSERT_TRUE(audio_recorder_always_on->Initialize() == to_underlying(Result::success));

  always_on = false;
  std::shared_ptr<AudioRecorderInterface> audio_recorder = std::make_shared<BaseAudioRecorder>(wake_word_mock_, always_on);
  ASSERT_TRUE(audio_recorder->Initialize() == to_underlying(Result::success));
}

//Test Audiorecorder integration with wakeword engine for non-AlwaysOn Voice UI Clients(without wakeword embedded)
TEST_F(VoiceUIAudioRecorderTest, MethodTestWakewordIntegratioNonAlwaysOn) {
  syslog(LOG_INFO, "***** VoiceUIAudioRecorderTest::MethodTestWakewordIntegratioNonAlwaysOn - START");
  bool always_on = false;
  int index_channel = 1;

  std::shared_ptr<AudioRecorderInterface> audio_recorder = std::make_shared<BaseAudioRecorder>(wake_word_mock_, always_on);
  ASSERT_TRUE(audio_recorder->Initialize() == to_underlying(Result::success));

  wake_word_mock_->StartRecognition();
  ASSERT_TRUE(wake_word_mock_->IsRecognizing());

  //Observer working fine.
  ASSERT_TRUE(audio_recorder->StartRecording(index_channel) == to_underlying(Result::success));
  ASSERT_FALSE(wake_word_mock_->IsRecognizing());
  ASSERT_TRUE(audio_recorder->IsRecording());

  ASSERT_TRUE(audio_recorder->StopRecording() == to_underlying(Result::success));
  ASSERT_TRUE(wake_word_mock_->IsRecognizing());
  ASSERT_FALSE(audio_recorder->IsRecording());

  //Observer failing to Initialize Recognition.
  ASSERT_TRUE(audio_recorder->StartRecording(index_channel) == to_underlying(Result::success));
  ASSERT_FALSE(wake_word_mock_->IsRecognizing());
  ASSERT_TRUE(audio_recorder->IsRecording());

  ASSERT_TRUE(audio_recorder->Initialize() == to_underlying(Result::success));
  ASSERT_TRUE(audio_recorder->StopRecording() == to_underlying(Result::success));
  ASSERT_TRUE(wake_word_mock_->IsRecognizing());
  ASSERT_FALSE(audio_recorder->IsRecording());
}

//Test Audiorecorder integration with wakeword engine for AlwaysOn Voice UI Clients
TEST_F(VoiceUIAudioRecorderTest, MethodTestWakewordIntegrationNonAlwaysOnFailingToRestartWakeword) {
  syslog(LOG_INFO, "***** VoiceUIAudioRecorderTest::MethodTestWakewordIntegrationNonAlwaysOnFailingToRestartWakeword - START");
  bool always_on = false;
  int index_channel = 1;

  std::shared_ptr<AudioRecorderInterface> audio_recorder = std::make_shared<BaseAudioRecorder>(wake_word_mock_, always_on);
  ASSERT_TRUE(audio_recorder->Initialize() == to_underlying(Result::success));

  wake_word_mock_->StartRecognition();
  ASSERT_TRUE(wake_word_mock_->IsRecognizing());

  ASSERT_TRUE(audio_recorder->StartRecording(index_channel) == to_underlying(Result::success));
  ASSERT_FALSE(wake_word_mock_->IsRecognizing());
  ASSERT_TRUE(audio_recorder->IsRecording());

  //waits 100ms of recording
  std::this_thread::sleep_for(std::chrono::milliseconds(100));

  //force wakeword to fail to start/stop causing Audio recording to fail.
  wake_word_mock_->SetExpectedResult(to_underlying(Result::fail));

  ASSERT_FALSE(audio_recorder->StopRecording() == to_underlying(Result::success));
  ASSERT_FALSE(wake_word_mock_->IsRecognizing());
  ASSERT_FALSE(audio_recorder->IsRecording());

  wake_word_mock_->ReinitializeWakeWord();

  ASSERT_TRUE(wake_word_mock_->IsRecognizing());
  ASSERT_FALSE(audio_recorder->IsRecording());

  ASSERT_TRUE(audio_recorder->StartRecording(index_channel) == to_underlying(Result::success));
  ASSERT_FALSE(wake_word_mock_->IsRecognizing());
  ASSERT_TRUE(audio_recorder->IsRecording());

  ASSERT_TRUE(audio_recorder->StopRecording() == to_underlying(Result::success));
  ASSERT_TRUE(wake_word_mock_->IsRecognizing());
  ASSERT_FALSE(audio_recorder->IsRecording());
}

//Only execute it on TARGET - dependencies to GST + AudioHal
//Test AudioRecorderObserver integration
TEST_F(VoiceUIAudioRecorderTest, MethodTestAudioRecorderObserver) {
  syslog(LOG_INFO, "***** VoiceUIAudioRecorderTest::MethodTestAudioRecorderObserver - START");
  bool always_on = false;
  int index_channel = 1;
  int packet_num = 0;

  std::shared_ptr<AudioRecorderInterface> audio_recorder = std::make_shared<QAHWManager>(wake_word_mock_, always_on, VoiceUIClientID::MOCK_FULL_SOLUTION);
  std::shared_ptr<MockAudioRecorderObserver> audio_recorder_observer = std::make_shared<MockAudioRecorderObserver>();
  audio_recorder->SetCallBackObserver(audio_recorder_observer);

  ASSERT_TRUE(audio_recorder->Initialize() == to_underlying(Result::success));

  wake_word_mock_->StartRecognition();
  ASSERT_TRUE(wake_word_mock_->IsRecognizing());

  ASSERT_TRUE(audio_recorder->StartRecording(index_channel) == to_underlying(Result::success));
  ASSERT_FALSE(wake_word_mock_->IsRecognizing());
  ASSERT_TRUE(audio_recorder->IsRecording());

  //wait 200ms of recording
  std::this_thread::sleep_for(std::chrono::milliseconds(200));
  //check on voice packages received - Expected != than PREVIOUS VALUE
  ASSERT_TRUE(packet_num != audio_recorder_observer->GetVoicePacketNumber());

  ASSERT_TRUE(audio_recorder->StopRecording() == to_underlying(Result::success));
  ASSERT_TRUE(wake_word_mock_->IsRecognizing());
  ASSERT_FALSE(audio_recorder->IsRecording());

  //wait 200ms of recording
  std::this_thread::sleep_for(std::chrono::milliseconds(200));

  //Simulate failure on RecorderObserver to initialize recognition
  audio_recorder_observer->SetRecoginitionStatus(false);

  packet_num = audio_recorder_observer->GetVoicePacketNumber();
  ASSERT_FALSE(audio_recorder->StartRecording(index_channel) == to_underlying(Result::success));
  ASSERT_TRUE(wake_word_mock_->IsRecognizing());
  ASSERT_FALSE(audio_recorder->IsRecording());

  //wait 200ms of recording
  std::this_thread::sleep_for(std::chrono::milliseconds(200));

  //check on voice packages received - Expected Same number
  ASSERT_TRUE(packet_num == audio_recorder_observer->GetVoicePacketNumber());

  ASSERT_TRUE(audio_recorder->StopRecording() == to_underlying(Result::success));
  ASSERT_TRUE(wake_word_mock_->IsRecognizing());
  ASSERT_FALSE(audio_recorder->IsRecording());

  //Simulate Success on RecorderObserver to initialize recognition
  audio_recorder_observer->SetRecoginitionStatus(true);

  packet_num = audio_recorder_observer->GetVoicePacketNumber();
  ASSERT_TRUE(audio_recorder->StartRecording(index_channel) == to_underlying(Result::success));
  ASSERT_FALSE(wake_word_mock_->IsRecognizing());
  ASSERT_TRUE(audio_recorder->IsRecording());

  //wait 100ms of recording
  std::this_thread::sleep_for(std::chrono::milliseconds(100));
  //check on voice packages received - Expected != than PREVIOUS VALUE
  ASSERT_TRUE(packet_num != audio_recorder_observer->GetVoicePacketNumber());

  ASSERT_TRUE(audio_recorder->StopRecording() == to_underlying(Result::success));
  ASSERT_TRUE(wake_word_mock_->IsRecognizing());
  ASSERT_FALSE(audio_recorder->IsRecording());
}

//Test Audiorecorder engine for AlwaysOn Voice UI Clients(with wakeword embedded)
TEST_F(VoiceUIAudioRecorderTest, MethodTestAlwaysOn) {
  syslog(LOG_INFO, "***** VoiceUIAudioRecorderTest::MethodTestIntegrationWithWakeWordAlwaysOn - START");
  bool always_on = true;
  int index_channel = 1;

  std::shared_ptr<AudioRecorderInterface> audio_recorder = std::make_shared<BaseAudioRecorder>(wake_word_mock_, always_on);
  ASSERT_TRUE(audio_recorder->Initialize() == to_underlying(Result::success));

  ASSERT_TRUE(audio_recorder->StartRecording(index_channel) == to_underlying(Result::success));
  ASSERT_TRUE(audio_recorder->IsRecording());

  //waits 100ms of recording
  std::this_thread::sleep_for(std::chrono::milliseconds(100));

  audio_recorder->StopRecording();
  ASSERT_FALSE(audio_recorder->IsRecording());
}

}  // namespace

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
