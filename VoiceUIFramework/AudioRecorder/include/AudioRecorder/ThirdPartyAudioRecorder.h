/*Copyright (c) 2019-2020, The Linux Foundation. All rights reserved.
*
*Redistribution and use in source and binary forms, with or without
*modification, are permitted provided that the following conditions are
*met:
*    * Redistributions of source code must retain the above copyright
*      notice, this list of conditions and the following disclaimer.
*    * Redistributions in binary form must reproduce the above
*      copyright notice, this list of conditions and the following
*      disclaimer in the documentation and/or other materials provided
*      with the distribution.
*    * Neither the name of The Linux Foundation nor the names of its
*      contributors may be used to endorse or promote products derived
*      from this software without specific prior written permission.
*
*THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
*WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
*MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
*ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
*BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
*BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
*WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
*OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
*IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    ThirdPartyAudioRecorder.h
 *  @brief   Audio recorder client for Third Party Engine
 *
 *  DESCRIPTION
 *    Handles audio recorder sessions for Third Party on LE platforms.
 ***************************************************************/

#ifndef VOICEUIFRAMEWORK_AUDIORECORDER_INCLUDE_AUDIORECORDER_THIRDPARTYAUDIORECORDER_H
#define VOICEUIFRAMEWORK_AUDIORECORDER_INCLUDE_AUDIORECORDER_THIRDPARTYAUDIORECORDER_H

#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <syslog.h>
#include <unistd.h>
#include <cstdio>
#include <fstream>
#include <iostream>
#include <memory>
#include <string>
#include <thread>
#include <vector>

#include <pulse/error.h>
#include <pulse/simple.h>

#include <AudioRecorder/BaseAudioRecorder.h>
#include <AudioRecorder/DSPC/DSPCEngine.h>
#include <VoiceUIUtils/GlobalValues.h>
#include <VoiceUIUtils/ThirdPartyAudioUtils.h>
#include <VoiceUIUtils/VoiceUIConfig.h>
#include <Wakeword/WakeWordInterface.h>

namespace voiceUIFramework {
namespace audioRecorder {

using namespace voiceUIFramework::wakeword;
using namespace voiceUIFramework::voiceUIUtils;

#define NUMOF_ASR_CHANNEL_ 3

class ThirdPartyAudioRecorder : public BaseAudioRecorder {
 public:
  ThirdPartyAudioRecorder(std::shared_ptr<WakeWordInterface> wakeword_engine, bool always_on, VoiceUIClientID client_id);

  ~ThirdPartyAudioRecorder();

  //Initialize AudioRecorder
  virtual int Initialize();
  //Start Recognition
  virtual int StartRecording(int channelIndex);
  //Stop Recognition
  virtual int StopRecording();
  //shutdown audio recorder
  virtual void Shutdown();

 protected:
 private:
  //DSPC Engine
  std::shared_ptr<DSPCEngine> dspc_engine_;
  //ClientID who owns the instance
  VoiceUIClientID client_id_;
  //Always-on audiorecording loop
  bool recording_loop_;
  //Thread to execute client solution
  std::thread client_thread_;
  //number of microphones
  int number_of_mic_channels_;
  //number of ec channels
  int number_of_ec_channels_;
  //Third Party BUFSIZE
  int third_party_bufsize_;
  //voice dump file
  bool voicedump_enable_;
  std::string voicedump_path_;
  //audio dump files
  FILE* foutput_;
  FILE* voiceui_dump_file_;
  FILE* f_samples_pa_int_;

  //TAP or multiturn Session flag
  bool tap_or_multiturn_session_running_;

  // Create and size buffers.
  std::vector<int> inSamples_;
  std::vector<int> outSamples_;
};

}  // namespace audioRecorder
}  // namespace voiceUIFramework

#endif /* VOICEUIFRAMEWORK_AUDIORECORDER_INCLUDE_AUDIORECORDER_THIRDPARTYAUDIORECORDER_H_ */
