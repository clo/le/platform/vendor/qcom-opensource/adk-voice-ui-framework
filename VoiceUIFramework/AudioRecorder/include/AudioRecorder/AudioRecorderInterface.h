/* Copyright (c) 2018, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    AudioRecorderInterface.h
 *  @brief   Audio recorder Interface
 *
 *  DESCRIPTION
 *    Handles audio interface for AudioRecorder client implementation
 ***************************************************************/

#ifndef VOICEUIFRAMEWORK_VOICEUIAUDIORECORDER_INCLUDE_AUDIORECORDER_AUDIORECORDERINTERFACE_H_
#define VOICEUIFRAMEWORK_VOICEUIAUDIORECORDER_INCLUDE_AUDIORECORDER_AUDIORECORDERINTERFACE_H_

#include <AudioRecorder/AudioRecorderCallBackInterface.h>
#include <Wakeword/WakeWordInterface.h>
#include <memory>

namespace voiceUIFramework {
namespace audioRecorder {

class AudioRecorderInterface {
 public:
  virtual ~AudioRecorderInterface() = default;
  //Initialize AudioRecorder
  virtual int Initialize() = 0;
  //Start a new Recognition using channel index from STHAL
  virtual int StartRecording(int channelIndex) = 0;
  //Stop Recognition
  virtual int StopRecording() = 0;
  //Check if Recognition is happening
  virtual bool IsRecording() = 0;
  //Check if AudioRecorder is always on
  virtual bool IsAlwaysOn() = 0;
  //Define observer to receive callbacks for recognition
  virtual void SetCallBackObserver(std::shared_ptr<voiceUIFramework::audioRecorder::AudioRecorderCallBackInterface> callback_observer) = 0;
  //Set Wakeword Engine reference
  virtual void SetWakewordEngine(std::shared_ptr<voiceUIFramework::wakeword::WakeWordInterface> wakeword_engine) = 0;
  //shutdown audio recorder
  virtual void Shutdown() = 0;
};

}  // namespace audioRecorder
}  // namespace voiceUIFramework

#endif /* VOICEUIFRAMEWORK_VOICEUIAUDIORECORDER_INCLUDE_AUDIORECORDER_AUDIORECORDERINTERFACE_H_ */
