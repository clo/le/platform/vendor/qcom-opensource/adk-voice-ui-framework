/*Copyright (c) 2019-2020, The Linux Foundation. All rights reserved.
*
*Redistribution and use in source and binary forms, with or without
*modification, are permitted provided that the following conditions are
*met:
*    * Redistributions of source code must retain the above copyright
*      notice, this list of conditions and the following disclaimer.
*    * Redistributions in binary form must reproduce the above
*      copyright notice, this list of conditions and the following
*      disclaimer in the documentation and/or other materials provided
*      with the distribution.
*    * Neither the name of The Linux Foundation nor the names of its
*      contributors may be used to endorse or promote products derived
*      from this software without specific prior written permission.
*
*THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
*WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
*MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
*ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
*BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
*BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
*WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
*OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
*IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    DSPCEngine.h
 *  @brief   Wapper for DSPC Engine
 *
 *  DESCRIPTION
 *    Wrapper DSPC Algorithm in a class to be used by DSPC Audiorecorder.
 ***************************************************************/

#ifndef VOICEUIFRAMEWORK_AUDIORECORDER_INCLUDE_AUDIORECORDER_DSPCENGINE_H_
#define VOICEUIFRAMEWORK_AUDIORECORDER_INCLUDE_AUDIORECORDER_DSPCENGINE_H_

#include <AudioRecorder/DSPC/TcpIO2.h>
#include <AudioRecorder/DSPC/awelib.h>

#include <errno.h>
#include <float.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <syslog.h>
#include <unistd.h>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <memory>
#include <string>
#include <thread>
#include <vector>

namespace voiceUIFramework {
namespace audioRecorder {

//Default number of MICS and EC channels
#define MIC_CHANNELS 4
#define EC_CHANNELS 2  //Modify this using adk.voiceui.db depending on target
#define BUFSIZE 960    //Modify this using adk.voiceui.db depending on target

//VRState address
#define AWE_VRState_classID 0xBEEF0870
#define AWE_VRState_ID 30006
#define AWE_VRState_value_OFFSET 8
#define AWE_VRState_value_MASK 0x00000100
#define AWE_VRState_value_SIZE 1

class DSPCEngine {
 public:
  DSPCEngine();
  ~DSPCEngine();

  int Initialize(std::vector<int> &inSamples, std::vector<int> &outSamples, unsigned int *number_of_output_channels);
  void Shutdown();
  int PumpAudio(std::vector<int> &in_samples, std::vector<int> &out_samples);
  void setVRStatus(bool status);
  int GetDoaDirection();

 protected:
 private:
  CAWELib *awelib_;

  //properties
  // Get pin properties
  UINT32 inCount_;
  UINT32 outCount_;
  UINT32 numInSamples_;
  UINT32 numInChannels_;
  UINT32 numOutSamples_;
  UINT32 numOutChannels_;
  UINT32 isInComplex_;
  UINT32 isOutComplex_;
  UINT32 wireId1_;   // default value
  UINT32 wireId2_;   // default value
  UINT32 layoutId_;  // default value

  bool currentVRState_;
};

}  // namespace audioRecorder
}  // namespace voiceUIFramework

#endif /* VOICEUIFRAMEWORK_AUDIORECORDER_INCLUDE_AUDIORECORDER_DSPCENGINE_H_ */
