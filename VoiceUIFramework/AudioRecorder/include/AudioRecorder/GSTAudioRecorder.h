/* Copyright (c) 2018-2020, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    GSTAudioRecorder.h
 *  @brief   brief description
 *
 *  DESCRIPTION
 *      MediaRecorder is a wrapper on AUDIO HAL provides functionality
 *      to configure audio recording session and streaming PCM samples
 ***************************************************************/

#ifndef VOICE_UI_FRAMEWORK_CODE_VOICEUIFRAMEWORK_AUDIORECORDER_INCLUDE_AUDIORECORDER_GSTAUDIORECORDER_H_
#define VOICE_UI_FRAMEWORK_CODE_VOICEUIFRAMEWORK_AUDIORECORDER_INCLUDE_AUDIORECORDER_GSTAUDIORECORDER_H_

#include <stdio.h>
#include <syslog.h>
#include <memory>

#include <AudioRecorder/AudioRecorderCallBackInterface.h>
#include <VoiceUIUtils/VoiceUIConfig.h>
#include <gst/app/gstappsink.h>
#include <gst/gst.h>
#include <stdio.h>
#include <thread>

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}

#endif

#ifndef BASEMACHINE_IPQx
#define PULSE_SRC_DEVICE "regular-builtin-mic"
#else /*Cypress*/
#define PULSE_SRC_DEVICE "low-latency0.2"
#endif

struct AudioProp {
  /* No cof audio Channels*/
  guint8 channels;
  /* Audio Sample Rate*/
  guint rate;
  /*PCM audio format*/
  gchar *format;
  /* Streaming PCM sample size*/
  gint bufsize;
  /* Device id*/
  gint device;
  /* FFV State*/
  gint ffv_state;
  /* FFV output refernce device*/
  gint ffv_ec_ref_dev;
  /* FFV channel index*/
  gint ffv_channel_index;
};

/**
 * Callback declaration of streaming PCM samples
 *
 * @param buf Buffer contains PCM samples.
 * @param size Size of buf in bytes
 * @streamid Should be zero
 * @return void.
 */

typedef void GetBufferEvent(guint8 *buf, guint size, gint streamid);

/**
 * Callback declaration of error event in raudio recording session
 *
 * @param errorType Error no
 * @streamid Should be zero
 * @return void.
 */

typedef void RecordFailedEvent(gint errorType, gint streamid);

class GSTAudioRecorder {
 private:
  /* Gstreamer data structures*/
  GMainLoop *loop_;
  GstElement *source_;
  GstElement *sink_;
  GstElement *pipeline_;
  gint stream_id_;
  guint bus_watch_;
  /* Callback pointer for record failed event*/
  RecordFailedEvent *record_failed_event_;
  /*Callback pointer for streaming PCM samples*/
  GetBufferEvent *get_buffer_event_;
  //callback for voice packets
  std::shared_ptr<voiceUIFramework::audioRecorder::AudioRecorderCallBackInterface> callback_observer_;
  //File for audio recording dump
  FILE *log_filename_;
  //enable/disable voice recording dump
  bool voicedump_enable_;
  //voice dump log file path
  std::string voicedump_path_;

 public:
  /**
     * Constructor
     *
     * @streamid Should be zero
     */

  GSTAudioRecorder(gint streamid);

  /**
     * Destructor
     */

  ~GSTAudioRecorder();
  /**
     * Initialize Audio recoding session with specified audio properties
     *
     * @param prop Audio properties
     * @return Success code or error code.
     */

  gint InitRecorder(AudioProp *prop);

  /**
     * Start Audio recoding session
     *
     * @return Success code or error code.
     */

  gint StartRecorder();

  /**
     * Request to Stop Audio recoding session
     *
     * @return Success code or error code.
     */

  gint StopRecorder();

  /**
       * Stop Audio recoding session
       *
       * @return Success code or error code.
       */

  gint ExecuteStopRecorder();

  /**
     * Clean up all resources, should be called before calling destructor
     *
     * @return void.
     */

  void Clear();
  /**
     *  Notification of a callback to execute on Audio recoding session error event
     *
     * @param eventFunc The callback to execute
     * @return void.
     */

  void callGetBufferEvent(guint8 *data, int size);
  /**
     * Gstreamer bus message handler
     * @param bus Pointer to Gst Bus
     @ @param msg Bus message
     * @return success or error code
     */

  gboolean handle_bus_msg(GstBus *bus, GstMessage *msg);
  /**
     *  Wrapper for running Glib Main loop
     * @return void.
     */

  //Set Observer to feed with voice packets
  void SetObserver(std::shared_ptr<voiceUIFramework::audioRecorder::AudioRecorderCallBackInterface> callback_observer);

  //Mian Loop
  void runMainLoop();
};

#endif /* VOICE_UI_FRAMEWORK_CODE_VOICEUIFRAMEWORK_AUDIORECORDER_INCLUDE_AUDIORECORDER_GSTAUDIORECORDER_H_ */
