/* Copyright (c) 2018, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    BaseAudioRecorder.h
 *  @brief   brief description
 *
 *  DESCRIPTION
 *    description
 ***************************************************************/

#ifndef VOICE_UI_FRAMEWORK_CODE_VOICEUIFRAMEWORK_AUDIORECORDER_INCLUDE_AUDIORECORDER_BASEAUDIORECORDER_H_
#define VOICE_UI_FRAMEWORK_CODE_VOICEUIFRAMEWORK_AUDIORECORDER_INCLUDE_AUDIORECORDER_BASEAUDIORECORDER_H_

#include <syslog.h>
#include <cstdio>
#include <iostream>
#include <memory>
#include <string>

#include <AudioRecorder/AudioRecorderInterface.h>
#include <Wakeword/WakeWordInterface.h>

namespace voiceUIFramework {
namespace audioRecorder {

using namespace voiceUIFramework::wakeword;

class BaseAudioRecorder : public AudioRecorderInterface {
 public:
  BaseAudioRecorder(std::shared_ptr<WakeWordInterface> wakeword_engine, bool always_on);

  ~BaseAudioRecorder();

  //Initialize AudioRecorder
  virtual int Initialize();
  //Start a new Recognition using channel index from STHAL
  virtual int StartRecording(int channelIndex);
  //Stop Recognition
  virtual int StopRecording();
  //Check if Recognition is happening
  virtual bool IsRecording();
  //Check if AudioRecorder is always on
  virtual bool IsAlwaysOn();
  //Define observer to receive callbacks for recognition
  virtual void SetCallBackObserver(std::shared_ptr<voiceUIFramework::audioRecorder::AudioRecorderCallBackInterface> callback_observer);
  //Set Wakeword Engine reference
  virtual void SetWakewordEngine(std::shared_ptr<voiceUIFramework::wakeword::WakeWordInterface> wakeword_engine);
  //shutdown audio recorder
  virtual void Shutdown();

 protected:
  //reference to wakeword engine
  std::shared_ptr<voiceUIFramework::wakeword::WakeWordInterface> wakeword_engine_;
  //recording status
  bool recording_;
  //Audio recorder AlwaysON status - Defines on Voice UI Solution is created.
  bool always_on_;
  //Voice UI Audiorecorder Callback Observer which will receive recording packages
  std::shared_ptr<voiceUIFramework::audioRecorder::AudioRecorderCallBackInterface> callback_observer_;
};

}  // namespace audioRecorder
}  // namespace voiceUIFramework

#endif /* VOICE_UI_FRAMEWORK_CODE_VOICEUIFRAMEWORK_AUDIORECORDER_INCLUDE_AUDIORECORDER_BASEAUDIORECORDER_H_ */
