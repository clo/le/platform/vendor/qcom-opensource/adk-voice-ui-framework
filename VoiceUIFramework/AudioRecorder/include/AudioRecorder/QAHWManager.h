/* Copyright (c) 2018, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    QAHWManager.h
 *  @brief   Audio recorder client for Qualcomm Technologies, Inc LE Platform
 *
 *  DESCRIPTION
 *    Handles audio recorder sessions for LE platforms interacting with Audio HAl.
 ***************************************************************/

#ifndef VOICEUIFRAMEWORK_AUDIORECORDER_INCLUDE_AUDIORECORDER_QAHWMANAGER_H_
#define VOICEUIFRAMEWORK_AUDIORECORDER_INCLUDE_AUDIORECORDER_QAHWMANAGER_H_

#include <syslog.h>
#include <cstdio>
#include <iostream>
#include <memory>
#include <string>

#include <AudioRecorder/BaseAudioRecorder.h>
#include <AudioRecorder/GSTAudioRecorder.h>
#include <VoiceUIUtils/GlobalValues.h>
#include <Wakeword/WakeWordInterface.h>

namespace voiceUIFramework {
namespace audioRecorder {

using namespace voiceUIFramework::wakeword;
using namespace voiceUIFramework::voiceUIUtils;

class QAHWManager : public BaseAudioRecorder {
 public:
  QAHWManager(std::shared_ptr<WakeWordInterface> wakeword_engine, bool always_on, VoiceUIClientID client_id);

  ~QAHWManager();

  //Initialize AudioRecorder
  virtual int Initialize();
  //Start Recognition
  virtual int StartRecording(int channelIndex);
  //Stop Recognition
  virtual int StopRecording();
  //shutdown audio recorder
  virtual void Shutdown();

 protected:
 private:
  //reference for MediaRecorder
  GSTAudioRecorder* media_recorder_;
  //ClientID who owns the instance
  VoiceUIClientID client_id_;
};

}  // namespace audioRecorder
}  // namespace voiceUIFramework

#endif /* VOICEUIFRAMEWORK_AUDIORECORDER_INCLUDE_AUDIORECORDER_QAHWMANAGER_H_ */
