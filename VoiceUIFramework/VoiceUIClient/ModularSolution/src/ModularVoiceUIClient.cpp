/* Copyright (c) 2018, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    ModularVoiceUIClient.cpp
 *  @brief   Modular voice UI Client base class for Modular Voice UI Client implementation
 *
 *  DESCRIPTION
 *    Base class for Modular solution Client implementation
 ***************************************************************/

#include <VoiceUIClient/ModularSolution/ModularVoiceUIClient.h>

namespace voiceUIFramework {
namespace voiceUIClient {

ModularVoiceUIClient::ModularVoiceUIClient(
    std::shared_ptr<voiceUIFramework::voiceUIClient::ClientObserver> client_observer,
    voiceUIFramework::voiceUIUtils::VoiceUIClientID client_id,
    bool wake_word_embedded)
    : BaseVoiceUIClient(client_observer) {
  base_client_manager_ = std::make_shared<ModularClientManager>(client_id, wake_word_embedded, client_observer);

  syslog(LOG_INFO, "ModularVoiceUIClient Constructor called for: %s", ClientIDToString(client_id).c_str());
}

ModularVoiceUIClient::~ModularVoiceUIClient() {
  syslog(LOG_INFO, "---ModularVoiceUIClient::~ModularVoiceUIClient");
}

bool ModularVoiceUIClient::Configure() {
  //Check if Modular Solution is enabled
  auto voice_ui_config = voiceUIFramework::voiceUIUtils::VoiceUIConfig::getInstance();
  if (voice_ui_config) {
    bool is_enable = voice_ui_config->ReadWithDefault<bool>(true, voiceUIUtils::Keys::k_modular_status_);
    if (!is_enable) {
      syslog(LOG_ERR, "ModularVoiceUIClient::Configure() - Modular Solution DISABLED (Check VoiceUI Database)");
      return false;
    }
    //base_client_manager_->SetActive(is_enable);
  } else {
    syslog(LOG_ERR, "ModularVoiceUIClient::Configure() - VoiceUIConfig not Initialized");
    //TODO ENABLE OR DISABLE CLIENT base_client_manager_->SetActive(is_enable);
  }
  return true;
}

void ModularVoiceUIClient::IntentReceived(IntentManagerEvent intent) {
  syslog(LOG_INFO, "ModularVoiceUIClient::IntentReceived - ClientID = %s", ClientIDToString(base_client_manager_->GetClientID()).c_str());
  //    base_client_manager_->IntentReceived(intent);

  //Process event on a separate thread
  std::thread intent_thread = std::thread([=] { base_client_manager_->IntentReceived(intent); });
  intent_thread.detach();
}
void ModularVoiceUIClient::IntentReceived(IntentManagerEvent intent, IntentManagerEventExtended intent_ext) {
  syslog(LOG_INFO, "ModularVoiceUIClient::IntentReceivedExtended - ClientID = %s", ClientIDToString(base_client_manager_->GetClientID()).c_str());
  //base_client_manager_->IntentReceived(intent, intent_ext);

  //Process event on a separate thread
  std::thread intent_thread = std::thread([=] { base_client_manager_->IntentReceived(intent, intent_ext); });
  intent_thread.detach();
}

}  // namespace voiceUIClient
}  // namespace voiceUIFramework
