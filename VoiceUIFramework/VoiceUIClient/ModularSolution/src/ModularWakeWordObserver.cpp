/* Copyright (c) 2018, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    ModularWakeWordObserver.cpp
 *  @brief   Implements WakeWord Callbacks for ModularClientManager.cpp
 *
 *  DESCRIPTION
 *
 ***************************************************************/

#include <VoiceUIClient/ModularSolution/ModularWakeWordObserver.h>

namespace voiceUIFramework {
namespace voiceUIClient {

ModularWakewordObserver::ModularWakewordObserver(std::shared_ptr<ASRInterface> asr_client)
    : asr_client_{asr_client} {
  syslog(LOG_INFO, "+++++++ModularWakeWordObserver Constructor called");
}

ModularWakewordObserver::~ModularWakewordObserver() {
  syslog(LOG_INFO, "------ModularWakeWordObserver Destructor called");
}

WakewordCallbackRc ModularWakewordObserver::ObserverAdded() {
  syslog(LOG_DEBUG, "ModularWakeWordObserver::ObserverAdded\n");
  //Start Recognition
  //wake_word_->StartRecognition(client_id_);
    return WakewordCallbackRc::WW_CB_OK;
}

WakewordCallbackRc ModularWakewordObserver::ObserverRemoved() {
  syslog(LOG_DEBUG, "ModularWakeWordObserver::ObserverRemoved\n");
    return WakewordCallbackRc::WW_CB_OK;
}

WakewordCallbackRc ModularWakewordObserver::StartRecognitionComplete() {
  syslog(LOG_DEBUG, "ModularWakeWordObserver::StartRecognitionComplete\n");
    return WakewordCallbackRc::WW_CB_OK;
}

WakewordCallbackRc ModularWakewordObserver::OnNewVoiceBufferReceived(
        void* buf,
        size_t size) {
  //TODO: Platform Independent size_t conversion to uint - loss less
  syslog(LOG_DEBUG, "ModularWakeWordObserver::OnNewVoiceBuffer - Received buffer size =%d\n", size);
  //Should be running always
  if (asr_client_->IsRunning()) {
    asr_client_->ProcessInput(static_cast<uint8_t*>(buf), static_cast<int>(size));
  }
  else {
    syslog(LOG_ERR, "ModularWakeWordObserver::FATAL Error, ASR is not Running\n");
    // wake_word_->StopAudioCapture(client_id_);
  }
    return WakewordCallbackRc::WW_CB_OK;
}

WakewordCallbackRc ModularWakewordObserver::OnKeywordDetected(
        voiceUIFramework::wakeword::KwdDetectionResults res) {
  syslog(LOG_DEBUG, "ModularWakeWordObserver::OnKeywordDetected\n");

  //Acquire wakelock
  int delay = WAKELOCK_MODULAR_CLIENT_TIMEOUT;
  system_wake_lock_toggle(true, WAKELOCK_MODULAR_CLIENT_NAME, delay);

  //SetClientState(ModularClientState::SVA_DETECTED); //TODO: CHECK HOW TO ENABLE THIS
  // Start Local ASR
  asr_client_->Start();
    return WakewordCallbackRc::WW_CB_OK;
}

WakewordCallbackRc ModularWakewordObserver::StopRecognitionComplete() {
  syslog(LOG_DEBUG, "ModularWakeWordObserver::StopRecognitionComplete\n");
    return WakewordCallbackRc::WW_CB_OK;
}

WakewordCallbackRc ModularWakewordObserver::StopAudioCaptureComplete() {
  syslog(LOG_DEBUG, "ModularWakeWordObserver::StopAudioCaptureComplete\n");
    return WakewordCallbackRc::WW_CB_OK;
}

WakewordCallbackRc ModularWakewordObserver::SoundTriggerDataReceived(
        voiceUIFramework::wakeword::SoundTriggerData type,
        int value) {
  syslog(LOG_DEBUG, "ModularWakeWordObserver::SoundTriggerDataReceived %s  : %d\n", SoundTriggerDataToString(type).c_str(), value);
    return WakewordCallbackRc::WW_CB_OK;
}

}  // namespace voiceUIClient
}  // namespace voiceUIFramework
