/* Copyright (c) 2018, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    ModularAudioRecorderObserver.cpp
 *  @brief   Audio recorder observer for Speech recognition
 *
 *  DESCRIPTION
 *    Audio recorder observer for Modular clients to be used with ASR engine
 ***************************************************************/

#include <VoiceUIClient/ModularSolution/ModularAudioRecorderObserver.h>

namespace voiceUIFramework {
namespace voiceUIClient {

#define OK 0;

ModularAudioRecorderObserver::ModularAudioRecorderObserver(std::shared_ptr<ASRInterface> asr_client)
    : asr_client_{asr_client} {
  syslog(LOG_INFO, "+++++++ModularAudioRecorderObserver Constructor called");
}

ModularAudioRecorderObserver::~ModularAudioRecorderObserver() {
  syslog(LOG_INFO, "------ModularAudioRecorderObserver Destructor called");
}

void ModularAudioRecorderObserver::OnNewVoiceBuffer(uint8_t* buf, uint32_t size) {
  //syslog(LOG_DEBUG, "ModularAudioRecorderObserver::OnNewVoiceBuffer - Received buffer size =%d\n", size);
  if (asr_client_ != nullptr) {
    asr_client_->ProcessInput(buf, size);
  }
}

bool ModularAudioRecorderObserver::InitializeRecognition() {
  //Nothing to be done
  return true;
}

}  // namespace voiceUIClient
}  // namespace voiceUIFramework
