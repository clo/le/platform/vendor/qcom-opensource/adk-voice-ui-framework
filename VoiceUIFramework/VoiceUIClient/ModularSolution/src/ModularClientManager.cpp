/* Copyright (c) 2018-2019, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    ModularClientManager.cpp
 *  @brief   Base Modular Client Manager for Modular Voice UI Client Manager implementation
 *
 *  DESCRIPTION
 *    Base class for Modular solution Client Manager implementation
 ***************************************************************/

#include <VoiceUIClient/ModularSolution/ModularClientManager.h>

namespace voiceUIFramework {
namespace voiceUIClient {

ModularClientManager::ModularClientManager(
    VoiceUIClientID client_id,
    bool wake_word_embedded,
    std::shared_ptr<voiceUIFramework::voiceUIClient::ClientObserver> client_observer)
    : BaseClientManager(client_id, wake_word_embedded, client_observer), state_(ModularClientState::IDLE), previous_state_(ModularClientState::IDLE), last_intent_processed_(ModularSolutionIntentID::UNKNOWN), speech_done_{false} {
}

ModularClientManager::~ModularClientManager() {
  // TODO Auto-generated destructor stub
}

bool ModularClientManager::ValidateCredentials() {
  return true;
}

bool ModularClientManager::Initialize() {
  syslog(LOG_INFO, "ModularClientManager::Initialize() - Wake_word_embedded_ = %d", wake_word_embedded_);
  if (!wake_word_embedded_) {
    // Get Single ton wakeword pointer
    wake_word_ = voiceUIFramework::wakeword::QSTHWClient::Get();  // TODO: check if we need this
    //wake_word_ = voiceUIFramework::wakeword::WakewordDummyClient::Get(); // To disable wakework
  }

  //Create Audio recorder for Client to alow interaction with push to talk button.
  audio_recorder_ = std::make_shared<QAHWManager>(wake_word_, true, client_id_);

  //Initialize ASR Provider
  asr_client_ = std::make_shared<voiceUIFramework::qtiASRClient::QTILocalASRNLU>(shared_from_this());
  if (asr_client_) {
    if (asr_client_->Initialize()) {
      if (!wake_word_embedded_) {
        // Start ASR
        //asr_client_->Start();

        // Add observer / Load Sound model only when he relevant ASR-NLU loads
        //Create and configure WakeWord Observer for Modular client
        modular_wakeword_observer_ = std::make_shared<ModularWakewordObserver>(asr_client_);
        if (!wake_word_->AddObserver(client_id_, modular_wakeword_observer_)) {
          syslog(LOG_ERR, "ModularClientManager::Initialize() - Wakeword fail to initialize");
          return false;
        }
      }
      //Create and configure AudioRecorder Observer for Modular client
      modular_audiorecorder_observer_ = std::make_shared<ModularAudioRecorderObserver>(asr_client_);
      audio_recorder_->SetCallBackObserver(modular_audiorecorder_observer_);

    } else {
      syslog(LOG_ERR, "ModularClientManager::Initialize() - Error to initialize ASR Client");
      return false;
    }
  }

  //Initialize NLU Provider
  if (nlu_client_) {
    if (nlu_client_->Initialize()) {
    } else {
      syslog(LOG_ERR, "ModularClientManager::Initialize() - Error to initialize NLU Client");
      return false;
    }
  }

  //Initialize TTS Provider
  tts_client_ = std::make_shared<voiceUIFramework::ttsEngineDummy::QTIDummyTTS>(shared_from_this());
  if (tts_client_) {
    if (tts_client_->Initialize()) {
      syslog(LOG_INFO, "ModularClientManager::Initialize() - Initialised TTS client");
    } else {
      syslog(LOG_ERR, "ModularClientManager::Initialize() - Error to initialize TTS Client");
      return false;
    }
  }

  // Create MAP of NLU Intents
  nlu_intent_map_[IntentIDToString(ModularSolutionIntentID::VOLUME)] = ModularSolutionIntentID::VOLUME;
  nlu_intent_map_[IntentIDToString(ModularSolutionIntentID::LIGHT)] = ModularSolutionIntentID::LIGHT;
  nlu_intent_map_[IntentIDToString(ModularSolutionIntentID::CONTROL)] = ModularSolutionIntentID::CONTROL;
  nlu_intent_map_[IntentIDToString(ModularSolutionIntentID::THERMOSTAT)] = ModularSolutionIntentID::THERMOSTAT;
  nlu_intent_map_[IntentIDToString(ModularSolutionIntentID::MUSIC)] = ModularSolutionIntentID::MUSIC;
  nlu_intent_map_[IntentIDToString(ModularSolutionIntentID::TELEPHONE)] = ModularSolutionIntentID::TELEPHONE;
  nlu_intent_map_[IntentIDToString(ModularSolutionIntentID::SECURITY)] = ModularSolutionIntentID::SECURITY;
  nlu_intent_map_[IntentIDToString(ModularSolutionIntentID::CONNECTION)] = ModularSolutionIntentID::CONNECTION;
  nlu_intent_map_[IntentIDToString(ModularSolutionIntentID::BLUETOOTH)] = ModularSolutionIntentID::BLUETOOTH;
  nlu_intent_map_[IntentIDToString(ModularSolutionIntentID::UNKNOWN)] = ModularSolutionIntentID::UNKNOWN;

  return true;
}

bool ModularClientManager::StartClient() {
  //TODO Initiate WAKEWORD but not used by ModularVoiceUIClient , hence start recognition in callback on addobserver
  return true;
}

bool ModularClientManager::StopClient() {
  //TODO Stop WAKEWORD
  return true;
}

void ModularClientManager::IntentReceived(IntentManagerEvent intent) {
  syslog(LOG_INFO, "ModularClientManager::IntentReceived - intent Received: %s", IntentManagerEventToString(intent).c_str());

  if (IsActive()) {
    if (intent == IntentManagerEvent::TAP_BUTTON_COMMAND || intent == IntentManagerEvent::TAP_BUTTON_COMMAND_QCASR) {
      //start recording and ASR process
      asr_client_->Start();
      if (!audio_recorder_->StartRecording(0) != 0) {
        syslog(LOG_ERR, "ModularClientManager::IntentReceived - TAP_BUTTON_COMMAND - StartRecording() Fail");
      }
    } else {
      syslog(LOG_INFO, "ModularClientManager::IntentReceived - unknown intent");
    }
  } else {
    syslog(LOG_ERR, "ModularClientManager::IntentReceived - Modular ASR CLient is not Active");
  }
}

void ModularClientManager::IntentReceived(IntentManagerEvent intent, IntentManagerEventExtended intent_ext) {
  syslog(LOG_INFO, "ModularClientManager::IntentManagerEventExtended - intent Received: %s", IntentManagerEventToString(intent).c_str());
}

void ModularClientManager::AppMessageReceived(AppMessage app_message) {
  syslog(LOG_DEBUG, "ModularClientManager::AppMessageReceived() = %s", AppMessageToString(app_message).c_str());
}

bool ModularClientManager::Shutdown() {
  syslog(LOG_INFO, "ModularClientManager::Shutdown()");

  //AudioRecorder
  if (audio_recorder_)
    audio_recorder_->Shutdown();

  //ASR client
  if (asr_client_) {
    asr_client_->Shutdown();
  }
  //NLU client
  if (nlu_client_) {
    nlu_client_->Shutdown();
  }
  //TTS client
  if (tts_client_) {
    tts_client_->Shutdown();
  }
  // TODO: wake_word_?? or
  // handled by base client manager?
  return true;
}

//broadcast NLU results
void ModularClientManager::NLUResults(std::string result) {
  SetClientState(ModularClientState::NLU_ENDED);

  //Process NLU Results
  if (!ProcessJSON(result)) {
    syslog(LOG_ERR, "ModularClientManager::NLUResults - ProcessJSON Error");
    ProcessError();
  }
}

void ModularClientManager::NLUError(std::string error) {
  SetClientState(ModularClientState::NLU_ERROR);
  syslog(LOG_ERR, "ModularClientManager::NLUError : %s", error.c_str());

  //Process error and go back to IDLE
  //TODO

  SetClientState(ModularClientState::IDLE);
}

void ModularClientManager::ASRStarted() {
  syslog(LOG_INFO, "ModularClientManager::ASRStarted");
  SetClientState(ModularClientState::SVA_DETECTED);
  SetClientState(ModularClientState::ASR_STARTED);
}

//broadcast partial ASR results
void ModularClientManager::ASRPartialResults(std::string result) {
  syslog(LOG_INFO, "ModularClientManager::ASRPartialResult: %s", result.c_str());
}

//broadcast complete ASR results
void ModularClientManager::ASRCompleteResults(std::string result) {
  syslog(LOG_INFO, "ModularClientManager::ASRCompleteResults: %s", result.c_str());
  SetClientState(ModularClientState::ASR_ENDED);

  if (!wake_word_embedded_) {
    syslog(LOG_INFO, "ModularClientManager::ASRCompleteResults::STOP LAB CAPTURE");
        wake_word_->StopAudioCapture(client_id_,
                voiceUIFramework::wakeword::StopCaptureResult::STOPCAPTURE_OK);
  }

  //stop recording from Audio Hal
  audio_recorder_->StopRecording();

  //Process ASR response
  if (asr_client_) {
    if (asr_client_->HasNLU()) {
      SetClientState(ModularClientState::NLU_STARTED);
      //Process NLU results
      NLUResults(result);
    } else {
      if (nlu_client_) {
        if (!nlu_client_->ProcessInput(result)) {
          syslog(LOG_ERR, "ModularClientManager::ASRPartialResult: NLU->ProcessInput() Error");
          ProcessError();
        }
      }
    }
  }
}

//broadcast ASR Errors
void ModularClientManager::ASRError(std::string error) {
  syslog(LOG_ERR, "ModularClientManager::ASRError : %s", error.c_str());
  SetClientState(ModularClientState::ASR_ERROR);

  if (!wake_word_embedded_) {
    syslog(LOG_INFO, "ModularClientManager::ASRError::STOP LAB CAPTURE");
        wake_word_->StopAudioCapture(client_id_,
                voiceUIFramework::wakeword::StopCaptureResult::STOPCAPTURE_OK);
  }

  //stop recording from Audio Hal
  audio_recorder_->StopRecording();

  //Process error and go back to IDLE
  //TODO

  SetClientState(ModularClientState::IDLE);
}

//broadcast TTS ready to use
void ModularClientManager::TTSInitialized() {
  syslog(LOG_INFO, "ModularClientManager::TTSInitialized");

  //TODO Playback some voice Message saying that system is available.
}

//broadcast TTS speak has begun
void ModularClientManager::TTSSpeakBegin() {
  syslog(LOG_INFO, "ModularClientManager::TTSSpeakBegin");
  SetClientState(ModularClientState::TTS_STARTED);
}

//broadcast TTS utterance has ended
void ModularClientManager::TTSSpeakEnd() {
  SetClientState(ModularClientState::TTS_ENDED);
  syslog(LOG_INFO, "ModularClientManager::TTSSpeakEnd");
}

//broadcast all TTS utterance queue is done
void ModularClientManager::TTSSpeakDone() {
  syslog(LOG_INFO, "ModularClientManager::TTSSpeakDone");
  SetClientState(ModularClientState::TTS_ENDED);

  //TTS is done go back to IDLE
  SetClientState(ModularClientState::IDLE);
}

//broadcast TTS Errors
void ModularClientManager::TTSError() {
  syslog(LOG_ERR, "ModularClientManager::TTSError");
  SetClientState(ModularClientState::TTS_ENDED);

  //Process error and go bat to IDLE
  //TODO
  SetClientState(ModularClientState::IDLE);
}

//get modular client current state
ModularClientState ModularClientManager::GetClientState() {
  return state_;
}

//set modular client state
void ModularClientManager::SetClientState(ModularClientState state) {
  syslog(LOG_INFO, "ModularClientManager::SetClientState = %s", ModularClientStateToString(state).c_str());

  //Save current State before changing it
  previous_state_ = state_;

  //notify clientObservers about current state
  if (state == ModularClientState::IDLE) {
    client_observer_->NewSolutionStatus(VoiceUIClientID::MODULAR_SOLUTION, VUISolutionStatus::IDLE);

    //Send message for IDLE LED state
    IntentManagerEvent intent = IntentManagerEvent::LED_PATTERN_IDLE;
    if (client_observer_)
      client_observer_->SendIntent(VoiceUIClientID::MODULAR_SOLUTION, intent);

    if (speech_done_) {
      //Send message for SPEECH_DONE LED state
      IntentManagerEvent intent = IntentManagerEvent::LED_PATTERN_SPEECH_DONE;
      if (client_observer_)
        client_observer_->SendIntent(VoiceUIClientID::MODULAR_SOLUTION, intent);

      //reset speech_done boolean
      speech_done_ = false;
    }
    //Release wakelock
    system_wake_lock_toggle(false, WAKELOCK_MODULAR_CLIENT_NAME, 0);

  } else if (state == ModularClientState::SVA_DETECTED) {  //TODO: CHECK this
    //Notify client_observer Listening
    client_observer_->NewSolutionStatus(VoiceUIClientID::MODULAR_SOLUTION, VUISolutionStatus::LISTENING);

    //Send message for IDLE LED state
    IntentManagerEvent intent = IntentManagerEvent::LED_PATTERN_LISTENING;
    if (client_observer_)
      client_observer_->SendIntent(VoiceUIClientID::MODULAR_SOLUTION, intent);

  } else if (state == ModularClientState::ASR_STARTED) {
    // Get DOA
    int doaAngle;

    //TODO - get from STHAL or DSPC AudioRecorder
    //Get DOA information form ST HAL
    if ((doaAngle = wake_word_->GetDOADirection(VoiceUIClientID::MODULAR_SOLUTION)) == -1) {
      syslog(LOG_INFO, "InteractionManager::onDialogUXStateChanged Unable to get DOA, Default to 0");
      doaAngle = 0;
    } else {
      syslog(LOG_INFO, "InteractionManager::onDialogUXStateChanged DOA %d", doaAngle);
    }

    //Send Direction OF Arrival Message
    IntentManagerEvent intent_2;
    intent_2 = IntentManagerEvent::DIRECTION_OF_ARRIVAL;
    IntentManagerEventExtended intent_ext;
    intent_ext.value_uint = doaAngle;
    if (client_observer_) {
      client_observer_->SendIntent(VoiceUIClientID::MODULAR_SOLUTION, intent_2, intent_ext);
    }
  } else if (state == ModularClientState::NLU_STARTED) {
    client_observer_->NewSolutionStatus(VoiceUIClientID::MODULAR_SOLUTION, VUISolutionStatus::BUSY);

    //Send message for THINKING/BUSY state
    IntentManagerEvent intent = IntentManagerEvent::LED_PATTERN_THINKING;
    if (client_observer_)
      client_observer_->SendIntent(VoiceUIClientID::MODULAR_SOLUTION, intent);

  } else if (state == ModularClientState::TTS_STARTED) {
    client_observer_->NewSolutionStatus(VoiceUIClientID::MODULAR_SOLUTION, VUISolutionStatus::SPEAKING);

    //Send message for SPEAKING state
    IntentManagerEvent intent = IntentManagerEvent::LED_PATTERN_SPEAKING;
    if (client_observer_)
      client_observer_->SendIntent(VoiceUIClientID::MODULAR_SOLUTION, intent);

    //set speech_done boolean
    speech_done_ = true;
  }
  state_ = state;

  //TODO: Check if we really need this
  if (state_ == ModularClientState::IDLE) {
    //TODO resume music if needed
  }
}

bool ModularClientManager::ProcessJSON(const std::string& jsonstr) {
  syslog(LOG_INFO, "ModularClientManager::ProcessJSON -jston_str = %s", jsonstr.c_str());
  std::cout << "LocalASR: JSON Response = " << jsonstr.c_str() << std::endl;

  //Identify if NLU response contains a TTS content
  bool domain_has_tts = false;

  Json::Value root;
  Json::Reader reader;

  bool parsingSuccessful = reader.parse(jsonstr.c_str(), root);  //parse process
  if (!parsingSuccessful) {
    std::string error_str = reader.getFormattedErrorMessages();
    syslog(LOG_ERR, "ModularClientManager::ProcessJSON - Failed to parse: %s", error_str.c_str());
    return false;
  }

  std::string intent_str = root.get("intent", IntentIDToString(ModularSolutionIntentID::UNKNOWN)).asString();
  syslog(LOG_INFO, "ModularClientManager::ProcessJSON - Intent = %s", intent_str.c_str());

  //read array of slots
  const Json::Value& slots = root["slots"];
  syslog(LOG_INFO, "ModularClientManager::ProcessJSON - Slots # = %d", slots.size());

  //check which Domain is the intent and process it.
  std::map<std::string, ModularSolutionIntentID>::iterator it = nlu_intent_map_.find(intent_str);
  if (it != nlu_intent_map_.end()) {
    switch (it->second) {
      case ModularSolutionIntentID::VOLUME:
        ProcessNLUVolumeIntent(slots);
        break;
      case ModularSolutionIntentID::LIGHT:
        domain_has_tts = true;
        ProcessNLULightIntent(slots);
        break;
      case ModularSolutionIntentID::CONTROL:
        domain_has_tts = true;
        ProcessNLUControlIntent(slots);
        break;
      case ModularSolutionIntentID::THERMOSTAT:
        domain_has_tts = true;
        ProcessNLUThermostatIntent(slots);
        break;
      case ModularSolutionIntentID::MUSIC:
        ProcessNLUMusicIntent(slots);
        break;
      case ModularSolutionIntentID::TELEPHONE:
        ProcessNLUTelephoneIntent(slots);
        break;
      case ModularSolutionIntentID::SECURITY:
        domain_has_tts = true;
        ProcessNLUSecutiryIntent(slots);
        break;
      case ModularSolutionIntentID::CONNECTION:
        ProcessNLUConnectionIntent(slots);
        break;
      case ModularSolutionIntentID::BLUETOOTH:
        ProcessNLUBluetoothIntent(slots);
        break;
      default:
        syslog(LOG_ERR, "ModularClientManager::ProcessJSON - NLU Domain not valid");
        break;
    }

    //save intent type
    last_intent_processed_ = it->second;

  } else {
    syslog(LOG_ERR, "JSON Payload is not a valid event. Payload = %s", intent_str.c_str());
    return false;
  }

  if (!domain_has_tts) {
    //If NLU response does not have a TTS, use case is DONE. Move CLient to IDLE.
    SetClientState(ModularClientState::IDLE);
  }

  //DEMO CODE to show JSON responses on DISPLAY MANAGER
  //Send JSON response from ASR/NLU to DisplayManager
  IntentManagerEvent intent = IntentManagerEvent::DISPLAY_MANAGER_NOTIFICATION;
  IntentManagerEventExtended intent_ext;
  intent_ext.notification.domain = "LocalASR";
  intent_ext.notification.message = IntentIDToString(it->second);
  intent_ext.notification.display_time = "3000";  //0  for permanently display
  intent_ext.notification.priority = "5";

  syslog(LOG_INFO, "ModularClientManager::ProcessJSON - Send JSON to DisplayManager");
  if (client_observer_)
    client_observer_->SendIntent(VoiceUIClientID::MOCK_MODULAR_SOLUTION, intent, intent_ext);
  //DEMO CODE to show JSON responses on DISPLAY MANAGER

  return true;
}

void ModularClientManager::ProcessError() {
  syslog(LOG_ERR, "ModularClientManager::ProcessError()");

  //TODO if present playback some voice feedback message
  SetClientState(ModularClientState::IDLE);
}

void ModularClientManager::ProcessNLUVolumeIntent(const Json::Value& slots) {
  syslog(LOG_INFO, "ModularClientManager::ProcessNLUVolumeIntent");

  bool valid_command = true;
  VolumeIntentID volumeState = VolumeIntentID::UNKNOWN;
  int volume_level = 0;
  TTS_Intents temp_intent = TTS_Intents::UNKNOWN;

  for (int i = 0; i < slots.size(); i++) {
    if (!VolumeIntentIDToString(VolumeIntentID::VOLUME).compare(slots[i]["name"].asString())) {
      std::string volume_level_str = slots[i]["value"].asString();
      if (volume_level_str.compare("null")) {
        if (IsNumber(volume_level_str)) {
          volume_level = std::stoi(volume_level_str);
        }
      }
    } else if (!VolumeIntentIDToString(VolumeIntentID::VOLUMESTATE).compare(slots[i]["name"].asString())) {
      if (!VolumeIntentIDToString(VolumeIntentID::ABSOLUTE).compare(slots[i]["value"].asString())) {
        volumeState = VolumeIntentID::ABSOLUTE;
      } else if (!VolumeIntentIDToString(VolumeIntentID::INCREASE).compare(slots[i]["value"].asString())) {
        volumeState = VolumeIntentID::INCREASE;

        IntentManagerEvent intent = IntentManagerEvent::INCREASE_VOLUME;

        if (client_observer_) {
          client_observer_->SendIntent(VoiceUIClientID::MODULAR_SOLUTION, intent);
          syslog(LOG_INFO, "ModularClientManager::ProcessNLUVolumeIntent Volume_Increased sent to TTS for playback");
          temp_intent = TTS_Intents::Volume_Increased;
          //removed to make behavior consistent to AVS
          //tts_client_->Speak(temp_intent);
        }
      } else if (!VolumeIntentIDToString(VolumeIntentID::DECREASE).compare(slots[i]["value"].asString())) {
        volumeState = VolumeIntentID::DECREASE;

        IntentManagerEvent intent = IntentManagerEvent::DECREASE_VOLUME;

        if (client_observer_) {
          client_observer_->SendIntent(VoiceUIClientID::MODULAR_SOLUTION, intent);
          syslog(LOG_INFO, "ModularClientManager::ProcessNLUVolumeIntent Volume_Decreased sent to TTS for playback");
          temp_intent = TTS_Intents::Volume_Decreased;
          //removed to make behavior consistent to AVS
          //tts_client_->Speak(temp_intent);
        }
      } else if (!VolumeIntentIDToString(VolumeIntentID::MUTE).compare(slots[i]["value"].asString())) {
        volumeState = VolumeIntentID::MUTE;
        IntentManagerEvent intent = IntentManagerEvent::SET_MUTE_STATE;
        IntentManagerEventExtended intent_ext;
        intent_ext.bool_state_1 = true;
        if (client_observer_) {
          client_observer_->SendIntent(VoiceUIClientID::MODULAR_SOLUTION, intent, intent_ext);
          syslog(LOG_INFO, "ModularClientManager::ProcessNLUVolumeIntent Device_muted sent to TTS for playback");
          temp_intent = TTS_Intents::Device_muted;
          //removed to make behavior consistent to AVS
          //tts_client_->Speak(temp_intent);
        }

      } else if (!VolumeIntentIDToString(VolumeIntentID::UNMUTE).compare(slots[i]["value"].asString())) {
        volumeState = VolumeIntentID::UNMUTE;

        IntentManagerEvent intent = IntentManagerEvent::SET_MUTE_STATE;
        IntentManagerEventExtended intent_ext;
        intent_ext.bool_state_1 = false;
        if (client_observer_) {
          client_observer_->SendIntent(VoiceUIClientID::MODULAR_SOLUTION, intent, intent_ext);
          syslog(LOG_INFO, "ModularClientManager::ProcessNLUVolumeIntent Device_unmuted sent to TTS for playback");
          temp_intent = TTS_Intents::Device_unmuted;
          //removed to make behavior consistent to AVS
          //tts_client_->Speak(temp_intent);
        }
      }
    } else {
      valid_command = false;
      syslog(LOG_ERR, "ModularClientManager::ProcessNLUVolumeIntent - Invalid Slot Information");
    }

    //Process Absolute Volume
    if (valid_command) {
      if (volumeState == VolumeIntentID::ABSOLUTE) {
        syslog(LOG_INFO, "ModularClientManager::ProcessNLUVolumeIntent - New volume level = %d ", volume_level);
        if (volume_level > 0 && volume_level <= 10) {
          IntentManagerEvent intent = IntentManagerEvent::SET_VOLUME_ABSOLUTE;
          IntentManagerEventExtended intent_ext;
          intent_ext.value_double_1 = (double)volume_level / 10.0;

          syslog(LOG_INFO, "ModularClientManager::ProcessNLUVolumeIntent - New volume level (double) = %f", intent_ext.value_double_1);

          if (client_observer_) {
            client_observer_->SendIntent(VoiceUIClientID::MODULAR_SOLUTION, intent, intent_ext);
            temp_intent = TTS_Intents::Volume_changed;
            //removed to make behavior consistent to AVS
            //tts_client_->Speak(temp_intent);
          }
        }
      }
    }
  }
}

void ModularClientManager::ProcessNLULightIntent(const Json::Value& slots) {
  syslog(LOG_INFO, "ModularClientManager::ProcessNLULightIntent");

  std::string room, state;
  bool valid_command = true;

  for (int i = 0; i < slots.size(); i++) {
    if (!LightIntentIDToString(LightIntentID::ROOM).compare(slots[i]["name"].asString())) {
      room = slots[i]["value"].asString();
    } else if (!LightIntentIDToString(LightIntentID::STATE).compare(slots[i]["name"].asString())) {
      state = slots[i]["value"].asString();
    } else {
      valid_command = false;
      syslog(LOG_ERR, "ModularClientManager::ProcessNLULightIntent - Invalid Slot Information");
    }
  }

  if (valid_command) {
    syslog(LOG_INFO, "ModularClientManager::ProcessNLULightIntent - Light Intent - Room = %s / State = %s", room.c_str(), state.c_str());
    //Send Light status to IntentManager
    IntentManagerEvent intent = IntentManagerEvent::SET_LIGHT_STATE;
    IntentManagerEventExtended intent_ext;
    intent_ext.ts.target = room;
    intent_ext.ts.state = state;

    TTS_Intents temp_intent = TTS_Intents::UNKNOWN;

    if (client_observer_) {
      client_observer_->SendIntent(VoiceUIClientID::MODULAR_SOLUTION, intent, intent_ext);

      if (room == "kitchen") {
        if (state == "on") {
          temp_intent = TTS_Intents::Kitchen_Lights_ON;
          syslog(LOG_INFO, "ModularClientManager::ProcessNLULightIntent - Kitchen_Lights_ON sent to TTS for playback");
        } else {
          temp_intent = TTS_Intents::Kitchen_lights_OFF;
          syslog(LOG_INFO, "ModularClientManager::ProcessNLULightIntent - Kitchen_lights_OFF sent to TTS for playback");
        }
      } else if (room == "bedroom") {
        if (state == "on") {
          temp_intent = TTS_Intents::Bedroom_Lights_ON;
          syslog(LOG_INFO, "ModularClientManager::ProcessNLULightIntent - Bedroom_Lights_ON sent to TTS for playback");
        } else {
          temp_intent = TTS_Intents::Bedroom_Lights_OFF;
          syslog(LOG_INFO, "ModularClientManager::ProcessNLULightIntent - Bedroom_Lights_OFF sent to TTS for playback");
        }
      } else if (room == "living_room") {
        if (state == "on") {
          temp_intent = TTS_Intents::Living_Room_Lights_ON;
          syslog(LOG_INFO, "ModularClientManager::ProcessNLULightIntent - Living_Room_Lights_ON sent to TTS for playback");
        } else {
          temp_intent = TTS_Intents::Living_Room_Lights_OFF;
          syslog(LOG_INFO, "ModularClientManager::ProcessNLULightIntent - Living_Room_Lights_OFF sent to TTS for playback");
        }
      } else if (room == "all") {
        if (state == "on") {
          temp_intent = TTS_Intents::Lights_ON;
          syslog(LOG_INFO, "ModularClientManager::ProcessNLULightIntent - Lights_ON sent to TTS for playback");
        } else {
          temp_intent = TTS_Intents::Lights_OFF;
          syslog(LOG_INFO, "ModularClientManager::ProcessNLULightIntent - Lights_OFF sent to TTS for playback");
        }
      }
      tts_client_->Speak(temp_intent);
    }
  }
}

void ModularClientManager::ProcessNLUControlIntent(const Json::Value& slots) {
  syslog(LOG_INFO, "ModularClientManager::ProcessNLUControlIntent");

  std::string target, state;
  bool valid_command = true;

  for (int i = 0; i < slots.size(); i++) {
    if (!ControlIntentIDToString(ControlIntentID::TARGET).compare(slots[i]["name"].asString())) {
      target = slots[i]["value"].asString();
    } else if (!ControlIntentIDToString(ControlIntentID::STATE).compare(slots[i]["name"].asString())) {
      state = slots[i]["value"].asString();
    } else {
      valid_command = false;
      syslog(LOG_ERR, "ModularClientManager::ProcessNLUControlIntent - Invalid Slot Information");
    }
  }

  if (valid_command) {
    syslog(LOG_INFO, "ModularClientManager::ProcessNLUControlIntent - Control Intent - Target = %s / State = %s", target.c_str(), state.c_str());
    //Send Control status to IntentManager
    IntentManagerEvent intent = IntentManagerEvent::SET_CONTROL_DEVICE_STATE;
    IntentManagerEventExtended intent_ext;
    intent_ext.ts.target = target;
    intent_ext.ts.state = state;
    TTS_Intents temp_intent = TTS_Intents::UNKNOWN;

    if (client_observer_) {
      client_observer_->SendIntent(VoiceUIClientID::MODULAR_SOLUTION, intent, intent_ext);

      if (target == "thermostat") {
        if (state == "on") {
          temp_intent = TTS_Intents::Thermostat_ON;
          syslog(LOG_INFO, "ModularClientManager::ProcessNLUControlIntent - Thermostat_ON sent to TTS for playback");
        } else {
          temp_intent = TTS_Intents::Thermostat_OFF;
          syslog(LOG_INFO, "ModularClientManager::ProcessNLUControlIntent - Thermostat_OFF sent to TTS for playback");
        }
      }
      tts_client_->Speak(temp_intent);
    }
  }
}

void ModularClientManager::ProcessNLUThermostatIntent(const Json::Value& slots) {
  syslog(LOG_INFO, "ModularClientManager::ProcessNLThermostatIntent");

  ThermostatIntentID difference;
  int temperature = 0;
  bool valid_command = true;

  for (int i = 0; i < slots.size(); i++) {
    if (!ThermostatIntentIDToString(ThermostatIntentID::DIFFERENCE).compare(slots[i]["name"].asString())) {
      if (!ThermostatIntentIDToString(ThermostatIntentID::ABSOLUTE).compare(slots[i]["value"].asString())) {
        difference = ThermostatIntentID::ABSOLUTE;
      } else if (!ThermostatIntentIDToString(ThermostatIntentID::INCREASE).compare(slots[i]["value"].asString())) {
        difference = ThermostatIntentID::INCREASE;
      } else if (!ThermostatIntentIDToString(ThermostatIntentID::DECREASE).compare(slots[i]["value"].asString())) {
        difference = ThermostatIntentID::DECREASE;
      }
    } else if (!ThermostatIntentIDToString(ThermostatIntentID::TEMPERATURE).compare(slots[i]["name"].asString())) {
      std::string temp_str = slots[i]["value"].asString();
      if (temp_str.compare("null")) {
        if (IsNumber(temp_str)) {
          temperature = std::stoi(temp_str);
        }
      }
    } else {
      valid_command = false;
      syslog(LOG_ERR, "ModularClientManager::ProcessNLThermostatIntent - Invalid Slot Information");
    }
  }

  IntentManagerEvent intent = IntentManagerEvent::UNKNOWN_INTENT;
  IntentManagerEventExtended intent_ext;

  TTS_Intents temp_intent = TTS_Intents::UNKNOWN;

  if (valid_command) {
    switch (difference) {
      case ThermostatIntentID::ABSOLUTE:
        syslog(LOG_INFO, "ModularClientManager::ProcessNLThermostatIntent - Set Temperature %d", temperature);
        intent = IntentManagerEvent::SET_THERMOSTAT_STATE_ABSOLUTE;
        intent_ext.value_uint = temperature;

        if (client_observer_) {
          client_observer_->SendIntent(VoiceUIClientID::MODULAR_SOLUTION, intent, intent_ext);
          temp_intent = TTS_Intents::Thermostat_temp_changed;
          syslog(LOG_INFO, "ModularClientManager::ProcessNLThermostatIntent - Thermostat_temp_changed sent to TTS for playback");
          tts_client_->Speak(temp_intent);
        }

        break;

      case ThermostatIntentID::INCREASE:
        syslog(LOG_INFO, "ModularClientManager::ProcessNLThermostatIntent - INCREASE temperature in 10 degrees ");
        intent = IntentManagerEvent::INCREASE_THERMOSTAT_TEMPERATURE;

        if (client_observer_) {
          client_observer_->SendIntent(VoiceUIClientID::MODULAR_SOLUTION, intent);
          // Todo: use TEMPERATURE_INCREASE .wav file WHEN IT IS available
          temp_intent = TTS_Intents::Thermostat_temp_changed;
          syslog(LOG_INFO, "ModularClientManager::ProcessNLThermostatIntent - Thermostat_temp_changed sent to TTS for playback");
          tts_client_->Speak(temp_intent);
        }

        break;

      case ThermostatIntentID::DECREASE:
        syslog(LOG_INFO, "ModularClientManager::ProcessNLThermostatIntent - DECREASE temperature in 10 degrees ");
        intent = IntentManagerEvent::DECREASE_THERMOSTAT_TEMPERATURE;

        if (client_observer_) {
          client_observer_->SendIntent(VoiceUIClientID::MODULAR_SOLUTION, intent);
          // Todo: use TEMPERATURE_dECREASE .wav file WHEN IT IS available
          temp_intent = TTS_Intents::Thermostat_temp_changed;
          syslog(LOG_INFO, "ModularClientManager::ProcessNLThermostatIntent - Thermostat_temp_changed sent to TTS for playback");
          tts_client_->Speak(temp_intent);
        }
        break;
    }
  }
}

void ModularClientManager::ProcessNLUMusicIntent(const Json::Value& slots) {
  syslog(LOG_INFO, "ModularClientManager::ProcessNLUMusicIntent");

  std::string action, group_target;
  bool valid_command = true;
  IntentManagerEvent intent = IntentManagerEvent::UNKNOWN_INTENT;

  for (int i = 0; i < slots.size(); i++) {
    if (!MusicIntentIDToString(MusicIntentID::ACTION).compare(slots[i]["name"].asString())) {
      action = slots[i]["value"].asString();
      if (!MusicIntentIDToString(MusicIntentID::PLAY).compare(slots[i]["value"].asString())) {
        intent = IntentManagerEvent::START_PLAYBACK;
      } else if (!MusicIntentIDToString(MusicIntentID::STOP).compare(slots[i]["value"].asString())) {
        intent = IntentManagerEvent::STOP_PLAYBACK;
      } else if (!MusicIntentIDToString(MusicIntentID::PAUSE).compare(slots[i]["value"].asString())) {
        intent = IntentManagerEvent::PAUSE_PLAYBACK;
      } else if (!MusicIntentIDToString(MusicIntentID::PREVIOUS).compare(slots[i]["value"].asString())) {
        intent = IntentManagerEvent::PLAY_PREVIOUS_TRACK;
      } else if (!MusicIntentIDToString(MusicIntentID::NEXT).compare(slots[i]["value"].asString())) {
        intent = IntentManagerEvent::PLAY_NEXT_TRACK;
      }
    } else if (!MusicIntentIDToString(MusicIntentID::MUSICROOM).compare(slots[i]["name"].asString())) {
      group_target = slots[i]["value"].asString();
    } else {
      valid_command = false;
      syslog(LOG_ERR, "ModularClientManager::ProcessNLUMusicIntent - Invalid Slot Information");
    }
  }

  if (valid_command) {
    syslog(LOG_INFO, "ModularClientManager::ProcessNLUMusicIntent - Music Intent - action = %s / room = %s", action.c_str(), group_target.c_str());
    //Send intent based on the music room

    if (group_target.empty() || (group_target.compare("default") == 0)) {
      if (client_observer_)
        client_observer_->SendIntent(VoiceUIClientID::MODULAR_SOLUTION, intent);
    } else {
      //Send Music intent to IntentManager
      IntentManagerEventExtended intent_ext;
      intent_ext.str_1 = group_target;

      if (client_observer_)
        client_observer_->SendIntent(VoiceUIClientID::MODULAR_SOLUTION, intent, intent_ext);
    }
  }
}

void ModularClientManager::ProcessNLUTelephoneIntent(const Json::Value& slots) {
  syslog(LOG_INFO, "ModularClientManager::ProcessNLUTelephoneIntent");

  std::string command;
  bool valid_command = true;

  for (int i = 0; i < slots.size(); i++) {
    if (!TelephoneIntentIDToString(TelephoneIntentID::PHONEACTION).compare(slots[i]["name"].asString())) {
      command = slots[i]["value"].asString();
    } else {
      valid_command = false;
      syslog(LOG_ERR, "ModularClientManager::ProcessNLUTelephoneIntent - Invalid Slot Information");
    }
  }

  if (valid_command) {
    syslog(LOG_INFO, "ModularClientManager::ProcessNLUTelephoneIntent - Telephone Intent - command = %s", command.c_str());
    //Send Telephone status to IntentManager
    IntentManagerEvent intent = IntentManagerEvent::HANDLE_CALL;
    IntentManagerEventExtended intent_ext;
    intent_ext.str_1 = command;

    TTS_Intents temp_intent = TTS_Intents::UNKNOWN;

    if (client_observer_) {
      client_observer_->SendIntent(VoiceUIClientID::MODULAR_SOLUTION, intent, intent_ext);

      if (command == "accept") {
        temp_intent = TTS_Intents::Incoming_call_accepted;
        syslog(LOG_INFO, "ModularClientManager::ProcessNLUTelephoneIntent - Incoming_call_accepted sent to TTS for playback");
      } else if (command == "reject") {
        temp_intent = TTS_Intents::Incoming_call_Rejected;
        syslog(LOG_INFO, "ModularClientManager::ProcessNLUTelephoneIntent - Incoming_call_Rejected sent to TTS for playback");
      } else if (command == "hang_up") {
        temp_intent = TTS_Intents::Call_Ended;
        syslog(LOG_INFO, "ModularClientManager::ProcessNLUTelephoneIntent - Call_Ended sent to TTS for playback");
      }
      tts_client_->Speak(temp_intent);
    }
  }
}

void ModularClientManager::ProcessNLUSecutiryIntent(const Json::Value& slots) {
  syslog(LOG_INFO, "ModularClientManager::ProcessNLUSecutiryIntent");

  std::string target, state;
  bool valid_command = true;

  for (int i = 0; i < slots.size(); i++) {
    if (!SecurityIntentIDToString(SecurityIntentID::DOORTARGET).compare(slots[i]["name"].asString())) {
      target = slots[i]["value"].asString();
    } else if (!SecurityIntentIDToString(SecurityIntentID::DOORSTATE).compare(slots[i]["name"].asString())) {
      state = slots[i]["value"].asString();
    } else {
      valid_command = false;
      syslog(LOG_ERR, "ModularClientManager::ProcessNLUSecutiryIntent - Invalid Slot Information");
    }
  }

  if (valid_command) {
    syslog(LOG_INFO, "ModularClientManager::ProcessNLUSecutiryIntent - Security Intent - Target = %s / State = %s", target.c_str(), state.c_str());
    //TODO Send Security status to IntentManager
    IntentManagerEvent intent = IntentManagerEvent::SET_SECURITY_STATE;
    IntentManagerEventExtended intent_ext;
    intent_ext.ts.target = target;
    intent_ext.ts.state = state;
    TTS_Intents temp_intent = TTS_Intents::UNKNOWN;

    if (client_observer_) {
      client_observer_->SendIntent(VoiceUIClientID::MODULAR_SOLUTION, intent, intent_ext);

      if (target == "front") {
        if (state == "open") {
          temp_intent = TTS_Intents::Front_Door_Opened;
          syslog(LOG_INFO, "ModularClientManager::ProcessNLUSecutiryIntent - Front_Door_Opened sent to TTS for playback");
        } else {
          temp_intent = TTS_Intents::Front_Door_Closed;
          syslog(LOG_INFO, "ModularClientManager::ProcessNLUSecutiryIntent - Front_Door_Closed sent to TTS for playback");
        }
      } else if (target == "garage") {
        if (state == "open") {
          temp_intent = TTS_Intents::Garage_Door_Opened;
          syslog(LOG_INFO, "ModularClientManager::ProcessNLUSecutiryIntent - Garage_Door_Opened sent to TTS for playback");
        } else {
          temp_intent = TTS_Intents::Garage_Door_Closed;
          syslog(LOG_INFO, "ModularClientManager::ProcessNLUSecutiryIntent - Garage_Door_Closed sent to TTS for playback");
        }
      }
      tts_client_->Speak(temp_intent);
    }
  }
}

void ModularClientManager::ProcessNLUConnectionIntent(const Json::Value& slots) {
  syslog(LOG_INFO, "ModularClientManager::ProcessNLUConnectionIntent");

  std::string target;
  bool valid_command = true;

  for (int i = 0; i < slots.size(); i++) {
    if (!ConnectionIntentIDToString(ConnectionIntentID::CONNECTIONTARGET).compare(slots[i]["name"].asString())) {
      target = slots[i]["value"].asString();
    } else {
      valid_command = false;
      syslog(LOG_ERR, "ModularClientManager::ProcessNLUConnectionIntent - Invalid Slot Information");
    }
  }

  if (valid_command) {
    syslog(LOG_INFO, "ModularClientManager::ProcessNLUConnectionIntent - Connection Intent - Target = %s", target.c_str());
    //Send Connection status to IntentManager
    IntentManagerEvent intent = IntentManagerEvent::SELECT_SOURCE;
    IntentManagerEventExtended intent_ext;
    intent_ext.str_1 = target;

    TTS_Intents temp_intent = TTS_Intents::UNKNOWN;

    if (client_observer_) {
      client_observer_->SendIntent(VoiceUIClientID::MODULAR_SOLUTION, intent, intent_ext);
      // TODO: handle voice promts when the connection related .wav files are available
    }
  }
}

void ModularClientManager::ProcessNLUBluetoothIntent(const Json::Value& slots) {
  syslog(LOG_INFO, "ModularClientManager::ProcessNLUBluetoothIntent");

  std::string target;
  bool valid_command = true;

  for (int i = 0; i < slots.size(); i++) {
    if (!BLuetoothIntentIDToString(BluetoothIntentID::BLUETOOTHACTION).compare(slots[i]["name"].asString())) {
      target = slots[i]["value"].asString();
    } else {
      valid_command = false;
      syslog(LOG_ERR, "ModularClientManager::ProcessNLUBluetoothIntent - Invalid Slot Information");
    }
  }

  if (valid_command) {
    syslog(LOG_INFO, "ModularClientManager::ProcessNLUBluetoothIntent - BluetoothIntent - Action = %s", target.c_str());
    //Send Connection status to IntentManager

    //TODO: SEND BT EVENTS TO CONNECTIVITY MANAGER
  }
}

}  // namespace voiceUIClient
}  // namespace voiceUIFramework
