/* Copyright (c) 2018, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    ModularClientManager.h
 *  @brief   Modular Voice UI Client Manager implementation
 *
 *  DESCRIPTION
 *    Handles Modular Voice UI Client Manager implementation
 ***************************************************************/

#ifndef VOICEUIFRAMEWORK_VOICEUICLIENT_MODULARSOLUTION_INCLUDE_VOICEUICLIENT_MODULARSOLUTION_MODULARCLIENTMANAGER_H_
#define VOICEUIFRAMEWORK_VOICEUICLIENT_MODULARSOLUTION_INCLUDE_VOICEUICLIENT_MODULARSOLUTION_MODULARCLIENTMANAGER_H_

#include <chrono>
#include <iostream>
#include <memory>
#include <string>
#include <thread>

#include <json/json.h>
#include <json/reader.h>
#include <json/value.h>
#include <json/writer.h>

#include <AudioRecorder/QAHWManager.h>

#ifdef MODULAR_CLIENT_ENABLED
#include <QTIASRIntegration/QTILocalASRNLU.h>
#include <QTIDummyTTS/QTIDummyTTS.h>
#endif

#include <VoiceUIClient/BaseSolution/BaseClientManager.h>
#include <VoiceUIClient/BaseSolution/ClientObserver.h>
#include <VoiceUIClient/ModularSolution/ASR/ASRCallBackInterface.h>
#include <VoiceUIClient/ModularSolution/ASR/ASRInterface.h>
#include <VoiceUIClient/ModularSolution/ModularAudioRecorderObserver.h>
#include <VoiceUIClient/ModularSolution/ModularWakeWordObserver.h>
#include <VoiceUIClient/ModularSolution/NLU/NLUCallBackInterface.h>
#include <VoiceUIClient/ModularSolution/NLU/NLUInterface.h>
#include <VoiceUIClient/ModularSolution/TTS/TTSCallBackInterface.h>
#include <VoiceUIClient/ModularSolution/TTS/TTSInterface.h>
#include <VoiceUIClient/ModularSolution/VoiceIntents.h>
#include <VoiceUIUtils/VoiceUIConfig.h>

namespace voiceUIFramework {
namespace voiceUIClient {

using voiceUIFramework::wakeword::KwdDetectionResults;

enum class ModularClientState {
  IDLE,
  SVA_DETECTED,
  ASR_STARTED,
  ASR_ENDED,
  ASR_ERROR,
  NLU_STARTED,
  NLU_ENDED,
  NLU_ERROR,
  TTS_STARTED,
  TTS_ENDED,
  UNKNOWN_STATE
};

/* Method to convert Intent to String */
inline const std::string ModularClientStateToString(ModularClientState state) {
  switch (state) {
    case ModularClientState::IDLE:
      return "Modular Client in IDLE";
    case ModularClientState::SVA_DETECTED:
      return "Modular Client in SVA_DETECTED";
    case ModularClientState::ASR_STARTED:
      return "Modular Client in ASR_STARTED";
    case ModularClientState::ASR_ENDED:
      return "Modular Client in ASR_ENDED";
    case ModularClientState::ASR_ERROR:
      return "Modular Client in ASR_ERROR";
    case ModularClientState::NLU_STARTED:
      return "Modular Client in NLU_STARTED";
    case ModularClientState::NLU_ENDED:
      return "Modular Client in NLU_ENDED";
    case ModularClientState::NLU_ERROR:
      return "Modular Client in NLU_ERROR";
    case ModularClientState::TTS_STARTED:
      return "Modular Client in TTS_STARTED";
    case ModularClientState::TTS_ENDED:
      return "Modular Client in TTS_ENDED";
    case ModularClientState::UNKNOWN_STATE:
      return "Unknown state";
  }
  return "Invalid Modular Client STATE";
}

#define QTI_LOCAL_ASR "QTI Local ASR"
#define NO_CLIENT_SELECTED "none"
#define LOCAL_FILES "local files"

class ModularClientManager : public BaseClientManager,
                             public NLUCallBackInterface,
                             public ASRCallBackInterface,
                             public TTSCallBackInterface,
                             public std::enable_shared_from_this<ModularClientManager> {
 public:
  ModularClientManager(
      VoiceUIClientID client_id,
      bool wake_word_embedded,
      std::shared_ptr<ClientObserver> client_observer);

  virtual ~ModularClientManager();

  /* BaseVoiceUIClient class */
  //Validates credentials from Voice UI Client
  virtual bool ValidateCredentials();
  //Initializes Voice UI Solution
  virtual bool Initialize();  //going to be implemented by Provider manager Class
  //Runs Voice UI Client
  virtual bool StartClient();
  //Stops Voice UI Client
  virtual bool StopClient();
  //Receive Intents from Voice UI IntentManager
  virtual void IntentReceived(IntentManagerEvent intent);
  virtual void IntentReceived(IntentManagerEvent intent, IntentManagerEventExtended intent_ext);
  //Receive App Events from Voice UI Manager
  virtual void AppMessageReceived(AppMessage app_message);
  //Shutdown Client
  virtual bool Shutdown();

  /* NLUCallBackInterface class */
  //broadcast NLU results
  virtual void NLUResults(std::string result);
  //broadcast NLU Errors
  virtual void NLUError(std::string error);

  //broadcast ASR Start
  virtual void ASRStarted();
  //broadcast partial ASR results
  virtual void ASRPartialResults(std::string result);
  //broadcast complete ASR results
  virtual void ASRCompleteResults(std::string result);
  //broadcast ASR Errors
  virtual void ASRError(std::string error);

  /* TTSCallBackInterface class */
  //broadcast TTS ready to use
  virtual void TTSInitialized();
  //broadcast TTS speak has begun
  virtual void TTSSpeakBegin();
  //broadcast TTS speak has ended
  virtual void TTSSpeakEnd();
  //broadcast all TTS speak queue is done
  virtual void TTSSpeakDone();
  //broadcast TTS Errors
  virtual void TTSError();

  //get modular client current state
  ModularClientState GetClientState();

  static constexpr unsigned int IpcSignalHash(const char* s, int off = 0) {
    return !s[off] ? 5381 : (IpcSignalHash(s, off + 1) * 33) ^ s[off];
}
 protected:
  //set modular client state
  void SetClientState(ModularClientState state);
  //ProcessJSON response from NLU
  bool ProcessJSON(const std::string& jsonstr);
  //ProcessError for ASR, NLU, TTS.
  void ProcessError();

  //Process each NLU domain responses
  void ProcessNLUVolumeIntent(const Json::Value& slots);
  void ProcessNLULightIntent(const Json::Value& slots);
  void ProcessNLUControlIntent(const Json::Value& slots);
  void ProcessNLUThermostatIntent(const Json::Value& slots);
  void ProcessNLUTelephoneIntent(const Json::Value& slots);
  void ProcessNLUMusicIntent(const Json::Value& slots);
  void ProcessNLUSecutiryIntent(const Json::Value& slots);
  void ProcessNLUConnectionIntent(const Json::Value& slots);
  void ProcessNLUBluetoothIntent(const Json::Value& slots);

  //Current and previous Client State
  ModularClientState state_;
  ModularClientState previous_state_;
  //Last intent type processed
  ModularSolutionIntentID last_intent_processed_;
  //ASR Client
  std::shared_ptr<ASRInterface> asr_client_;
  //NLU Client
  std::shared_ptr<NLUInterface> nlu_client_;
  //TTS Client
  std::shared_ptr<TTSInterface> tts_client_;
  //NLU Intents Map
  std::map<std::string, ModularSolutionIntentID> nlu_intent_map_;
  // Modular Audio recorder Callback object
  std::shared_ptr<ModularAudioRecorderObserver> modular_audiorecorder_observer_;
  //Modular Wakeword Callback object
  std::shared_ptr<ModularWakewordObserver> modular_wakeword_observer_;

  //current solutions read form VoiceUI db
  std::string selected_asr_;
  std::string selected_nlu_;
  std::string selected_tts_;

  //Speech has finished
  bool speech_done_;
};

}  // namespace voiceUIClient
}  // namespace voiceUIFramework

#endif /* VOICEUIFRAMEWORK_VOICEUICLIENT_MODULARSOLUTION_INCLUDE_VOICEUICLIENT_MODULARSOLUTION_MODULARCLIENTMANAGER_H_ */
