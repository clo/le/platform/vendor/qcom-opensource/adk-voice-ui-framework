/* Copyright (c) 2018, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    TTSInterface.h
 *  @brief   TTS interface for TTS implementation
 *
 *  DESCRIPTION
 *    Handles TTS implementation for TTS providers
 ***************************************************************/

#ifndef VOICEUIFRAMEWORK_VOICEUICLIENT_MODULARSOLUTION_INCLUDE_VOICEUICLIENT_MODULARSOLUTION_TTS_TTSINTERFACE_H_
#define VOICEUIFRAMEWORK_VOICEUICLIENT_MODULARSOLUTION_INCLUDE_VOICEUICLIENT_MODULARSOLUTION_TTS_TTSINTERFACE_H_

namespace voiceUIFramework {
namespace voiceUIClient {

enum class TTS_Intents {
  Bedroom_Lights_OFF,
  Bedroom_Lights_ON,
  Call_Ended,
  Device_muted,
  Device_unmuted,
  Front_Door_Closed,
  Front_Door_Opened,
  Garage_Door_Closed,
  Garage_Door_Opened,
  Incoming_call_accepted,
  Incoming_call_Rejected,
  Kitchen_lights_OFF,
  Kitchen_Lights_ON,
  Lights_OFF,
  Lights_ON,
  Living_Room_Lights_OFF,
  Living_Room_Lights_ON,
  Thermostat_OFF,
  Thermostat_ON,
  Thermostat_temp_changed,
  Volume_changed,
  Volume_Decreased,
  Volume_Increased,
  UNKNOWN
};

class TTSInterface {
 public:
  virtual ~TTSInterface() = default;

  //Initializes ASR engine
  virtual bool Initialize() = 0;
  //Speak
  virtual bool Speak() = 0;
  virtual bool Speak(TTS_Intents intent) = 0;
  //Stops Speaking immediately
  virtual bool StopSpeaking() = 0;
  //Verify if TTS is speaking
  virtual bool IsSpeaking() = 0;
  //Shutdown ASR Engine
  virtual void Shutdown() = 0;
};

}  // namespace voiceUIClient
}  // namespace voiceUIFramework

#endif /* VOICEUIFRAMEWORK_VOICEUICLIENT_MODULARSOLUTION_INCLUDE_VOICEUICLIENT_MODULARSOLUTION_TTS_TTSINTERFACE_H_ */
