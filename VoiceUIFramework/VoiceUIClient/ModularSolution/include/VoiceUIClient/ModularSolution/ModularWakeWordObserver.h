/* Copyright (c) 2018, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    ModularWakeWordObserver.h
 *  @brief   Implements WakeWord Callbacks for ModularClientManager.cpp
 *
 *  DESCRIPTION
 *
 ***************************************************************/

#ifndef VOICEUIFRAMEWORK_VOICEUICLIENT_MODULARSOLUTION_INCLUDE_VOICEUICLIENT_MODULARSOLUTION_MODULARWAKEWORDOBSERVER_H_
#define VOICEUIFRAMEWORK_VOICEUICLIENT_MODULARSOLUTION_INCLUDE_VOICEUICLIENT_MODULARSOLUTION_MODULARWAKEWORDOBSERVER_H_

#include <syslog.h>
#include <cstdio>
#include <iostream>
#include <memory>
#include <string>

#include <VoiceUIClient/ModularSolution/ASR/ASRInterface.h>
#include <Wakeword/WakeWordCallBackInterface.h>


#include <adk/wakelock.h>

namespace voiceUIFramework {
namespace voiceUIClient {

using namespace voiceUIFramework::wakeword;

class ModularWakewordObserver : public voiceUIFramework::wakeword::WakeWordCallBackInterface {
 public:
  ModularWakewordObserver(std::shared_ptr<ASRInterface> asr_client);
  virtual ~ModularWakewordObserver();

  /* WakeWordCallBackInterface */
  // Observer client is added Successfully (Sound model Loaded Successfully)
    WakewordCallbackRc ObserverAdded();
  // Observer client is removed Successfully (Sound model Unloaded Successfully)
    WakewordCallbackRc ObserverRemoved();
  // Recognition is scheduled, STHAL is recognising keywords of this client
    WakewordCallbackRc StartRecognitionComplete();
  // Callback to transfer LAB Data from Buffers in WakeWord to the client
    WakewordCallbackRc OnNewVoiceBufferReceived(void* buf, size_t size);
  // Callback to notify detection of keyword
    WakewordCallbackRc OnKeywordDetected(
            voiceUIFramework::wakeword::KwdDetectionResults res);
  // Callback to notify Stop Recognition
    WakewordCallbackRc StopRecognitionComplete();
  // Callback to notify Stop Audio Capture
    WakewordCallbackRc StopAudioCaptureComplete();

  // Call back to notify doa or channel Index
    WakewordCallbackRc SoundTriggerDataReceived(
            voiceUIFramework::wakeword::SoundTriggerData type,
            int value);

 private:
  //ASR Client
  std::shared_ptr<ASRInterface> asr_client_;
};

}  // namespace voiceUIClient
}  // namespace voiceUIFramework

#endif /* VOICEUIFRAMEWORK_VOICEUICLIENT_MODULARSOLUTION_INCLUDE_VOICEUICLIENT_MODULARSOLUTION_MODULARWAKEWORDOBSERVER_H_ */
