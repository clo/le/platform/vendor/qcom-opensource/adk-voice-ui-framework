/* Copyright (c) 2018, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    VoiceIntents.h
 *  @brief   brief description
 *
 *  DESCRIPTION
 *    description
 ***************************************************************/

#ifndef VOICEUIFRAMEWORK_VOICEUICLIENT_MODULARSOLUTION_INCLUDE_VOICEUICLIENT_MODULARSOLUTION_VOICEINTENTS_H_
#define VOICEUIFRAMEWORK_VOICEUICLIENT_MODULARSOLUTION_INCLUDE_VOICEUICLIENT_MODULARSOLUTION_VOICEINTENTS_H_

#include <string>

namespace voiceUIFramework {
namespace voiceUIClient {

/* An enum class for all Intents Commands processed by Modular Solution*/
enum class ModularSolutionIntentID {
  VOLUME,
  LIGHT,
  CONTROL,
  THERMOSTAT,
  MUSIC,
  TELEPHONE,
  SECURITY,
  CONNECTION,
  BLUETOOTH,
  UNKNOWN
};
/* Method to convert Intent to String */
inline const std::string IntentIDToString(ModularSolutionIntentID intent) {
  switch (intent) {
    case ModularSolutionIntentID::VOLUME:
      return "VolumeIntent";
    case ModularSolutionIntentID::LIGHT:
      return "LightIntent";
    case ModularSolutionIntentID::CONTROL:
      return "ControlIntent";
    case ModularSolutionIntentID::THERMOSTAT:
      return "TemperatureIntent";
    case ModularSolutionIntentID::MUSIC:
      return "MusicIntent";
    case ModularSolutionIntentID::TELEPHONE:
      return "PhoneIntent";
    case ModularSolutionIntentID::SECURITY:
      return "SecurityIntent";
    case ModularSolutionIntentID::CONNECTION:
      return "ConnectionIntent";
    case ModularSolutionIntentID::BLUETOOTH:
      return "BluetoothIntent";
    case ModularSolutionIntentID::UNKNOWN:
      return "UnknownIntent";
  }
  return "Unknown Intent ID";
}

/* An enum class for Volume Intent*/
enum class VolumeIntentID {
  VOLUME,
  VOLUMESTATE,
  INCREASE,
  DECREASE,
  ABSOLUTE,
  MUTE,
  UNMUTE,
  UNKNOWN
};
/* Method to convert Volume Command to String */
inline const std::string VolumeIntentIDToString(VolumeIntentID command) {
  switch (command) {
    case VolumeIntentID::VOLUME:
      return "Volume";
    case VolumeIntentID::VOLUMESTATE:
      return "VolumeState";
    case VolumeIntentID::INCREASE:
      return "increase";
    case VolumeIntentID::DECREASE:
      return "decrease";
    case VolumeIntentID::ABSOLUTE:
      return "absolute";
    case VolumeIntentID::MUTE:
      return "mute";
    case VolumeIntentID::UNMUTE:
      return "unmute";
    case VolumeIntentID::UNKNOWN:
      return "unknown";
  }
  return "Unknown Volume Parameter";
}

/* An enum class for Light commands*/
enum class LightIntentID {
  ROOM,
  STATE,
  UNKNOWN
};
/* Method to convert Light to String */
inline const std::string LightIntentIDToString(LightIntentID command) {
  switch (command) {
    case LightIntentID::ROOM:
      return "Room";
    case LightIntentID::STATE:
      return "State";
    case LightIntentID::UNKNOWN:
      return "unknown";
  }
  return "Unknown Light Parameter";
}

/* An enum class for Control Intent*/
enum class ControlIntentID {
  STATE,
  TARGET,
  UNKNOWN
};
/* Method to convert Thermostat Command to String */
inline const std::string ControlIntentIDToString(ControlIntentID command) {
  switch (command) {
    case ControlIntentID::STATE:
      return "State";
    case ControlIntentID::TARGET:
      return "Target";
    case ControlIntentID::UNKNOWN:
      return "unknown";
  }
  return "Unknown Thermostat Parameter";
}

/* An enum class for Thermostat Intent*/
enum class ThermostatIntentID {
  DIFFERENCE,
  TEMPERATURE,
  INCREASE,
  DECREASE,
  ABSOLUTE,
  UNKNOWN
};
/* Method to convert Thermostat Command to String */
inline const std::string ThermostatIntentIDToString(ThermostatIntentID command) {
  switch (command) {
    case ThermostatIntentID::DIFFERENCE:
      return "Difference";
    case ThermostatIntentID::TEMPERATURE:
      return "Temperature";
    case ThermostatIntentID::INCREASE:
      return "increase";
    case ThermostatIntentID::DECREASE:
      return "decrease";
    case ThermostatIntentID::ABSOLUTE:
      return "absolute";
    case ThermostatIntentID::UNKNOWN:
      return "unknown";
  }
  return "Unknown Thermostat Parameter";
}

/* An enum class for Music commands*/
enum class MusicIntentID {
  ACTION,
  MUSICROOM,
  PLAY,
  STOP,
  PAUSE,
  PREVIOUS,
  NEXT,
  UNKNOWN
};
/* Method to convert Music Command to String */
inline const std::string MusicIntentIDToString(MusicIntentID command) {
  switch (command) {
    case MusicIntentID::ACTION:
      return "Action";
    case MusicIntentID::MUSICROOM:
      return "MusicRoom";
    case MusicIntentID::PLAY:
      return "play";
    case MusicIntentID::STOP:
      return "stop";
    case MusicIntentID::PAUSE:
      return "pause";
    case MusicIntentID::PREVIOUS:
      return "previous";
    case MusicIntentID::NEXT:
      return "next";
    case MusicIntentID::UNKNOWN:
      return "unknown";
  }
  return "Unknown Music Parameter";
}

/* An enum class for Reference values for Telephone*/
enum class TelephoneIntentID {
  PHONEACTION,
  ACCEPT,
  REJECT,
  HANGUP,
  UNKNOWN
};
/* Method to convert Telephone Command to String */
inline const std::string TelephoneIntentIDToString(TelephoneIntentID reference) {
  switch (reference) {
    case TelephoneIntentID::PHONEACTION:
      return "PhoneAction";
    case TelephoneIntentID::ACCEPT:
      return "accept";
    case TelephoneIntentID::REJECT:
      return "reject";
    case TelephoneIntentID::HANGUP:
      return "hang_up";
    case TelephoneIntentID::UNKNOWN:
      return "unknown";
  }
  return "Unknown Telephone Parameter";
}

/* An enum class for Reference values for Security intent*/
enum class SecurityIntentID {
  DOORSTATE,
  DOORTARGET,
  OPEN,
  CLOSE,
  UNKNOWN
};
/* Method to convert Security Command to String */
inline const std::string SecurityIntentIDToString(SecurityIntentID reference) {
  switch (reference) {
    case SecurityIntentID::DOORSTATE:
      return "DoorState";
    case SecurityIntentID::DOORTARGET:
      return "DoorTarget";
    case SecurityIntentID::OPEN:
      return "open";
    case SecurityIntentID::CLOSE:
      return "close";
    case SecurityIntentID::UNKNOWN:
      return "unknown";
  }
  return "Unknown Security Parameter";
}

/* An enum class for Reference values for Connection intent*/
enum class ConnectionIntentID {
  CONNECTIONTARGET,
  HDMI,
  TV,
  OPTICAL,
  DIGITAL,
  ANALOG,
  USB,
  BLUETOOTH,
  WIFI,
  UNKNOWN
};
/* Method to convert Connection Command to String */
inline const std::string ConnectionIntentIDToString(ConnectionIntentID reference) {
  switch (reference) {
    case ConnectionIntentID::CONNECTIONTARGET:
      return "ConnectionTarget";
    case ConnectionIntentID::HDMI:
      return "hdmi";
    case ConnectionIntentID::TV:
      return "tv";
    case ConnectionIntentID::OPTICAL:
      return "optical";
    case ConnectionIntentID::DIGITAL:
      return "digital";
    case ConnectionIntentID::ANALOG:
      return "analog";
    case ConnectionIntentID::USB:
      return "usb";
    case ConnectionIntentID::BLUETOOTH:
      return "bluetooth";
    case ConnectionIntentID::WIFI:
      return "wifi";
    case ConnectionIntentID::UNKNOWN:
      return "unknown";
  }
  return "Unknown Connection Parameter";
}

/* An enum class for Bluetooth commands*/
enum class BluetoothIntentID {
  BLUETOOTHACTION,
  UNKNOWN
};
/* Method to convert Bluetooth Commands to String */
inline const std::string BLuetoothIntentIDToString(BluetoothIntentID command) {
  switch (command) {
    case BluetoothIntentID::BLUETOOTHACTION:
      return "BluetoothAction";
    case BluetoothIntentID::UNKNOWN:
      return "unknown";
  }
  return "Unknown Bluetooth Command ";
}

}  // namespace voiceUIClient
}  // namespace voiceUIFramework

#endif /* VOICEUIFRAMEWORK_VOICEUICLIENT_MODULARSOLUTION_INCLUDE_VOICEUICLIENT_MODULARSOLUTION_VOICEINTENTS_H_ */
