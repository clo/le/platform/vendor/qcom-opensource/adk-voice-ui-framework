/* Copyright (c) 2018, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    VoiceUIClientTest.cpp
 *  @brief   brief description
 *
 *  DESCRIPTION
 *    description
 ***************************************************************/

#include <syslog.h>
#include <iostream>
#include <memory>
#include <string>
#include <thread>

#include <VoiceUIClient/FullSolution/FullVoiceUIClient.h>
#include <VoiceUIClient/ModularSolution/ModularVoiceUIClient.h>

#include "gtest/gtest.h"

using namespace std;
using namespace voiceUIFramework::voiceUIClient;
using namespace voiceUIFramework::voiceUIUtils;

namespace {

class ClientObserverMock : public ClientObserver {
 public:
  ClientObserverMock()
      : last_intent_sent_(IntentManagerEvent::UNKNOWN_INTENT) {
    // You can do set-up work for each test here.
  }

  ~ClientObserverMock() {
    // You can do clean-up work that doesn't throw exceptions here.
  }

  virtual void NewSolutionStatus(VoiceUIClientID client_id, VUISolutionStatus status) {
  }
  //Callback for keyword detected
  virtual void KeywordDetected(VoiceUIClientID client_id) {
  }
  //Callback for direction of arrival (doa)
  virtual void DirectionOfArrival(VoiceUIClientID client_id, int direction) {
  }
  //Callback for volume status changed
  virtual void VolumeStatus(VoiceUIClientID client_id, int volume) {
  }
  //Callback for mute status changed
  virtual void MuteStatus(VoiceUIClientID client_id, bool state) {
  }
  //Callback for enable/disable status changed
  virtual void ActiveStatus(VoiceUIClientID client_id, bool state) {
  }
  //Sends Intent from Voice UI Client
  virtual void SendIntent(VoiceUIClientID client_id, IntentManagerEvent intent) {
    syslog(LOG_INFO, "ClientObserverMock::SendIntent -  Event: %s", IntentManagerEventToString(intent).c_str());
    if (intent != IntentManagerEvent::LED_PATTERN_IDLE && intent != IntentManagerEvent::LED_PATTERN_LISTENING && intent != IntentManagerEvent::LED_PATTERN_ONBOARDING && intent != IntentManagerEvent::LED_PATTERN_SPEAKING && intent != IntentManagerEvent::LED_PATTERN_THINKING) {
      last_intent_sent_ = intent;
    }
  }
  virtual void SendIntent(VoiceUIClientID client_id, IntentManagerEvent intent, IntentManagerEventExtended intent_ext) {
    syslog(LOG_INFO, "ClientObserverMock::SendIntentExtended -  Event: %s", IntentManagerEventToString(intent).c_str());
    if (intent != IntentManagerEvent::DISPLAY_MANAGER_NOTIFICATION) {
      last_intent_sent_ = intent;
      last_event_extended_ = intent_ext;
    }
  }
  //Request Client Restart
  virtual void RequestRestartClient(VoiceUIClientID client_id) {
  }

  IntentManagerEvent GetLastIntentSent() {
    return last_intent_sent_;
  }

  IntentManagerEventExtended GetLastEventExtended() {
    return last_event_extended_;
  }

 private:
  IntentManagerEvent last_intent_sent_;
  IntentManagerEventExtended last_event_extended_;
};

class ModularClientManagerMock : public ModularClientManager {
 public:
  ModularClientManagerMock(VoiceUIClientID client_id,
      bool wake_word_embedded,
      std::shared_ptr<ClientObserver> client_observer)
      : ModularClientManager(client_id, wake_word_embedded, client_observer) {
    // You can do set-up work for each test here.
  }
  ~ModularClientManagerMock() {
    // You can do clean-up work that doesn't throw exceptions here.
  }
  ModularClientState GetClientPreviousState() {
    syslog(LOG_INFO, "ModularClientManagerMock::GetClientPreviousState -  previous_state_: %s", ModularClientStateToString(previous_state_).c_str());
    return previous_state_;
  }
  ModularSolutionIntentID GetPreviousIntentProcessed() {
    syslog(LOG_INFO, "ModularClientManagerMock::GetPreviousIntentProcessed = %s", IntentIDToString(last_intent_processed_).c_str());
    return last_intent_processed_;
  }
};

// The fixture for testing class VoiceUIManager.
class VoiceUIClientTest : public ::testing::Test {
 protected:
  VoiceUIClientTest() {
    // You can do set-up work for each test here.
  }

  virtual ~VoiceUIClientTest() {
    // You can do clean-up work that doesn't throw exceptions here.
  }

  // If the constructor and destructor are not enough for setting up
  // and cleaning up each test, you can define the following methods:

  virtual void SetUp() {
    client_observer_mock_ = std::make_shared<ClientObserverMock>();
  }

  virtual void TearDown() {
  }

  std::shared_ptr<ClientObserverMock> client_observer_mock_;
};

TEST_F(VoiceUIClientTest, TestCreateFullClient) {
  syslog(LOG_DEBUG, "***** VoiceUIClientTest::TestCreateFullClient - START");

  bool wake_word_embedded = true;

  std::shared_ptr<BaseVoiceUIClient> client_wakeword_embeeded = std::make_shared<FullVoiceUIClient>(client_observer_mock_, VoiceUIClientID::AVS_SOLUTION, wake_word_embedded);
  ASSERT_EQ(true, client_wakeword_embeeded->Start());
  //shutdown Client to join client Thread to main Thread
  client_wakeword_embeeded->Shutdown();

  wake_word_embedded = false;
  std::shared_ptr<BaseVoiceUIClient> client = std::make_shared<FullVoiceUIClient>(client_observer_mock_, VoiceUIClientID::AVS_SOLUTION, wake_word_embedded);
  ASSERT_EQ(true, client->Start());
  //shutdown Client to join client Thread to main Thread
  client->Shutdown();
}

TEST_F(VoiceUIClientTest, TestCreateModularClient) {
  syslog(LOG_INFO, "***** VoiceUIClientTest::TestCreateModularClient - START");

  bool wake_word_embedded = true;

  std::shared_ptr<BaseVoiceUIClient> client_wakeword_embeeded = std::make_shared<ModularVoiceUIClient>(client_observer_mock_, VoiceUIClientID::MODULAR_SOLUTION, wake_word_embedded);
  ASSERT_EQ(true, client_wakeword_embeeded->Start());
  //shutdown Client to join client Thread to main Thread
  client_wakeword_embeeded->Shutdown();

  wake_word_embedded = false;
  std::shared_ptr<BaseVoiceUIClient> client = std::make_shared<ModularVoiceUIClient>(client_observer_mock_, VoiceUIClientID::MODULAR_SOLUTION, wake_word_embedded);
  ASSERT_EQ(true, client->Start());
  //shutdown Client to join client Thread to main Thread
  client->Shutdown();
}

TEST_F(VoiceUIClientTest, TestClientID) {
  syslog(LOG_INFO, "***** VoiceUIClientTest::TestClientID - START");
  bool wake_word_embedded = true;

  //Full Solution
  VoiceUIClientID clientID = VoiceUIClientID::AVS_SOLUTION;

  std::shared_ptr<BaseVoiceUIClient> full_client_wakeword_embeeded = std::make_shared<FullVoiceUIClient>(client_observer_mock_, clientID, wake_word_embedded);
  ASSERT_EQ(true, full_client_wakeword_embeeded->Start());

  ASSERT_EQ(full_client_wakeword_embeeded->GetClientID(), VoiceUIClientID::AVS_SOLUTION);

  //shutdown Client to join client Thread to main Thread
  full_client_wakeword_embeeded->Shutdown();

  //Modular Solution
  clientID = VoiceUIClientID::MODULAR_SOLUTION;

  std::shared_ptr<BaseVoiceUIClient> modular_client_wakeword_embeeded = std::make_shared<ModularVoiceUIClient>(client_observer_mock_, clientID, wake_word_embedded);
  ASSERT_EQ(true, modular_client_wakeword_embeeded->Start());

  ASSERT_EQ(modular_client_wakeword_embeeded->GetClientID(), VoiceUIClientID::MODULAR_SOLUTION);

  //shutdown Client to join client Thread to main Thread
  modular_client_wakeword_embeeded->Shutdown();
}

TEST_F(VoiceUIClientTest, TestFalseClientID) {
  syslog(LOG_DEBUG, "***** VoiceUIClientTest::TestFalseClientID - START");
  bool wake_word_embedded = true;

  //Full Solution
  VoiceUIClientID clientID = VoiceUIClientID::AVS_SOLUTION;

  std::shared_ptr<BaseVoiceUIClient> full_client_wakeword_embeeded = std::make_shared<FullVoiceUIClient>(client_observer_mock_, clientID, wake_word_embedded);
  ASSERT_EQ(true, full_client_wakeword_embeeded->Start());

  ASSERT_FALSE((full_client_wakeword_embeeded->GetClientID() == VoiceUIClientID::CORTANA_SOLUTION));

  //shutdown Client to join client Thread to main Thread
  full_client_wakeword_embeeded->Shutdown();

  //Modular Solution
  clientID = VoiceUIClientID::MODULAR_SOLUTION;

  std::shared_ptr<BaseVoiceUIClient> modular_client_wakeword_embeeded = std::make_shared<ModularVoiceUIClient>(client_observer_mock_, clientID, wake_word_embedded);
  ASSERT_EQ(true, modular_client_wakeword_embeeded->Start());

  ASSERT_FALSE((modular_client_wakeword_embeeded->GetClientID() == VoiceUIClientID::CORTANA_SOLUTION));

  //shutdown Client to join client Thread to main Thread
  modular_client_wakeword_embeeded->Shutdown();
}

TEST_F(VoiceUIClientTest, TestClientActiveState) {
  syslog(LOG_DEBUG, "***** VoiceUIClientTest::TestClientActiveState - START");
  bool wake_word_embedded = false;

  //Full Solution
  VoiceUIClientID clientID = VoiceUIClientID::AVS_SOLUTION;

  std::shared_ptr<BaseVoiceUIClient> full_client = std::make_shared<FullVoiceUIClient>(client_observer_mock_, clientID, wake_word_embedded);
  ASSERT_EQ(true, full_client->Start());

  //waits 500ms to initiate Client
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  ASSERT_EQ(true, full_client->IsActive());

  //Deactivate Client
  ASSERT_EQ(true, full_client->SetActive(false));
  ASSERT_EQ(false, full_client->IsActive());

  //Active Client
  ASSERT_EQ(true, full_client->SetActive(true));
  ASSERT_EQ(true, full_client->IsActive());

  //shutdown Client to join client Thread to main Thread
  full_client->Shutdown();

  //Modular Solution
  clientID = VoiceUIClientID::MODULAR_SOLUTION;

  std::shared_ptr<BaseVoiceUIClient> modular_client = std::make_shared<ModularVoiceUIClient>(client_observer_mock_, clientID, wake_word_embedded);
  ASSERT_EQ(true, modular_client->Start());

  //waits 500ms to initiate Client
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  ASSERT_EQ(true, modular_client->IsActive());

  //Deactivate Client
  ASSERT_EQ(true, modular_client->SetActive(false));
  ASSERT_EQ(false, modular_client->IsActive());

  //Active Client
  ASSERT_EQ(true, modular_client->SetActive(true));
  ASSERT_EQ(true, modular_client->IsActive());

  //shutdown Client to join client Thread to main Thread
  modular_client->Shutdown();
}

TEST_F(VoiceUIClientTest, TestClientMuteState) {
  syslog(LOG_DEBUG, "***** VoiceUIClientTest::TestClientMuteState - START");
  bool wake_word_embedded = false;

  //Full Solution
  VoiceUIClientID clientID = VoiceUIClientID::AVS_SOLUTION;

  std::shared_ptr<BaseVoiceUIClient> full_client = std::make_shared<FullVoiceUIClient>(client_observer_mock_, clientID, wake_word_embedded);
  ASSERT_EQ(true, full_client->Start());

  //waits 500ms to initiate Client
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  ASSERT_EQ(false, full_client->IsMute());

  //Mute Client
  full_client->SetMute(true);
  ASSERT_EQ(true, full_client->IsMute());

  //Unmute Client
  full_client->SetMute(false);
  ASSERT_EQ(false, full_client->IsMute());

  //shutdown Client to join client Thread to main Thread
  full_client->Shutdown();

  //Modular Solution
  clientID = VoiceUIClientID::MODULAR_SOLUTION;

  std::shared_ptr<BaseVoiceUIClient> modular_client = std::make_shared<ModularVoiceUIClient>(client_observer_mock_, clientID, wake_word_embedded);
  ASSERT_EQ(true, modular_client->Start());

  //waits 500ms to initiate Client
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  ASSERT_EQ(false, modular_client->IsMute());

  //Mute Client
  modular_client->SetMute(true);
  ASSERT_EQ(true, modular_client->IsMute());

  //Unmute Client
  modular_client->SetMute(false);
  ASSERT_EQ(false, modular_client->IsMute());

  //shutdown Client to join client Thread to main Thread
  modular_client->Shutdown();
}

TEST_F(VoiceUIClientTest, TestCreateFullClientManager) {
  syslog(LOG_DEBUG, "***** VoiceUIClientTest::TestCreateFullClientManager - START");

  bool wake_word_embedded = false;
  VoiceUIClientID clientID = VoiceUIClientID::AVS_SOLUTION;

  std::shared_ptr<BaseClientManager> client_manager = std::make_shared<FullClientManager>(clientID, wake_word_embedded, client_observer_mock_);

  ASSERT_EQ(true, client_manager->Initialize());

  //shutdown Client to join client Thread to main Thread
  client_manager->Shutdown();
}

TEST_F(VoiceUIClientTest, TestCreatModularClientManager) {
  syslog(LOG_DEBUG, "***** VoiceUIClientTest::TestCreatModularClientManager - START");

  bool wake_word_embedded = false;
  VoiceUIClientID clientID = VoiceUIClientID::MODULAR_SOLUTION;

  std::shared_ptr<BaseClientManager> client_manager = std::make_shared<ModularClientManager>(clientID, wake_word_embedded, client_observer_mock_);

  ASSERT_EQ(true, client_manager->Initialize());

  //shutdown Client to join client Thread to main Thread
  client_manager->Shutdown();
}

TEST_F(VoiceUIClientTest, TestClientManagerSendIntent) {
  syslog(LOG_DEBUG, "***** VoiceUIClientTest::TestClientManagerSendIntent - START");
  bool wake_word_embedded = false;

  //Full Solution
  VoiceUIClientID clientID = VoiceUIClientID::AVS_SOLUTION;

  std::shared_ptr<BaseClientManager> full_client_manager = std::make_shared<FullClientManager>(clientID, wake_word_embedded, client_observer_mock_);
  ASSERT_EQ(true, full_client_manager->Initialize());

  IntentManagerEvent intent = IntentManagerEvent::UNKNOWN_INTENT;
  ASSERT_FALSE(full_client_manager->SendIntent(intent));

  intent = IntentManagerEvent::INCREASE_VOLUME;
  ASSERT_TRUE(full_client_manager->SendIntent(intent));

  //shutdown Client to join client Thread to main Thread
  full_client_manager->Shutdown();

  //Modular Solution
  clientID = VoiceUIClientID::AVS_SOLUTION;

  std::shared_ptr<BaseClientManager> modular_client_manager = std::make_shared<FullClientManager>(clientID, wake_word_embedded, client_observer_mock_);
  ASSERT_EQ(true, modular_client_manager->Initialize());

  intent = IntentManagerEvent::UNKNOWN_INTENT;
  ASSERT_FALSE(modular_client_manager->SendIntent(intent));

  intent = IntentManagerEvent::INCREASE_VOLUME;
  ASSERT_TRUE(modular_client_manager->SendIntent(intent));

  //shutdown Client to join client Thread to main Thread
  modular_client_manager->Shutdown();
}

TEST_F(VoiceUIClientTest, TestClientManagerSendIntentExtended) {
  syslog(LOG_DEBUG, "***** VoiceUIClientTest::TestClientManagerSendIntentExtended - START");
  bool wake_word_embedded = false;

  //Full Solution
  VoiceUIClientID clientID = VoiceUIClientID::AVS_SOLUTION;

  std::shared_ptr<BaseClientManager> full_client_manager = std::make_shared<FullClientManager>(clientID, wake_word_embedded, client_observer_mock_);
  ASSERT_EQ(true, full_client_manager->Initialize());

  IntentManagerEvent intent = IntentManagerEvent::SET_VOLUME_ABSOLUTE;
  IntentManagerEventExtended intent_ext;
  intent_ext.value_uint = 55;

  ASSERT_EQ(true, full_client_manager->SendIntent(intent, intent_ext));

  //shutdown Client to join client Thread to main Thread
  full_client_manager->Shutdown();

  //Modular Solution
  clientID = VoiceUIClientID::MODULAR_SOLUTION;

  std::shared_ptr<BaseClientManager> modular_client_manager = std::make_shared<ModularClientManager>(clientID, wake_word_embedded, client_observer_mock_);
  ASSERT_EQ(true, modular_client_manager->Initialize());

  intent = IntentManagerEvent::SET_VOLUME_ABSOLUTE;
  intent_ext.value_uint = 55;

  ASSERT_EQ(true, modular_client_manager->SendIntent(intent, intent_ext));

  //shutdown Client to join client Thread to main Thread
  modular_client_manager->Shutdown();
}

//Only execute it on TARGET - dependencies to QC Local ASR+NLU
TEST_F(VoiceUIClientTest, TestASRUseCaseFlow) {
  syslog(LOG_DEBUG, "***** VoiceUIClientTest::TestASRUseCaseFlow - START");

  bool wake_word_embedded = false;
  VoiceUIClientID clientID = VoiceUIClientID::MODULAR_SOLUTION;

  std::shared_ptr<ModularClientManagerMock> client_manager = std::make_shared<ModularClientManagerMock>(clientID, wake_word_embedded, client_observer_mock_);
  ASSERT_EQ(true, client_manager->Initialize());

  //Simulate ASR Partical Results
  ASSERT_EQ(ModularClientState::IDLE, client_manager->GetClientState());
  client_manager->ASRPartialResults("Initiating voice");
  ASSERT_EQ(ModularClientState::IDLE, client_manager->GetClientPreviousState());
  ASSERT_EQ(ModularClientState::IDLE, client_manager->GetClientState());

  //Simulate ASR results for ASR+NLU engine - Use case: "Play Music in the living Room" - no TTS
  ASSERT_EQ(ModularClientState::IDLE, client_manager->GetClientState());
  client_manager->ASRPartialResults("Initiating voice transcription.");
  ASSERT_EQ(ModularClientState::IDLE, client_manager->GetClientPreviousState());
  ASSERT_EQ(ModularClientState::IDLE, client_manager->GetClientState());

  //Simulate ASR results for ASR+NLU engine - Use case: "Accept income phon ecall" - No TTS
  ASSERT_EQ(ModularClientState::IDLE, client_manager->GetClientState());
  client_manager->ASRCompleteResults("{\"intent\":\"PhoneIntent\",\"slots\":[{\"name\":\"PhoneAction\",\"value\":\"accept\"}]}");
  ASSERT_EQ(ModularClientState::NLU_ENDED, client_manager->GetClientPreviousState());
  ASSERT_EQ(ModularClientState::IDLE, client_manager->GetClientState());

  //TO DO Simulate ASR results for "Turn on Thermostat" - WITH TTS
  ASSERT_EQ(ModularClientState::IDLE, client_manager->GetClientState());
  client_manager->ASRCompleteResults("{\"intent\":\"ControlIntent\",\"slots\":[{\"name\":\"State\",\"value\":\"on\"},{\"name\":\"Target\",\"value\":\"thermostat\"}]}");
  ASSERT_EQ(ModularClientState::NLU_ENDED, client_manager->GetClientPreviousState());  // CHANGE FOR TTS_END when TTS IS AVAILABLE
  ASSERT_EQ(ModularClientState::IDLE, client_manager->GetClientState());

  //shutdown Client to join client Thread to main Thread
  client_manager->Shutdown();
}

TEST_F(VoiceUIClientTest, TestASRInvalidUseCaseFlow) {
  syslog(LOG_DEBUG, "***** VoiceUIClientTest::TestASRInvalidUseCaseFlow - START");

  bool wake_word_embedded = false;
  VoiceUIClientID clientID = VoiceUIClientID::MODULAR_SOLUTION;

  std::shared_ptr<ModularClientManagerMock> client_manager = std::make_shared<ModularClientManagerMock>(clientID, wake_word_embedded, client_observer_mock_);
  ASSERT_EQ(true, client_manager->Initialize());

  //Simulate ASR results for invalid INTENT
  ASSERT_EQ(ModularClientState::IDLE, client_manager->GetClientState());
  client_manager->ASRCompleteResults("{\"intent\":\"InvalidIntent\",\"slots\":[{\"name\":\"PhoneActionIsNull\",\"value\":\"accept_null\"}]}");
  ASSERT_EQ(ModularClientState::NLU_ENDED, client_manager->GetClientPreviousState());
  ASSERT_EQ(ModularClientState::IDLE, client_manager->GetClientState());

  //Simulate ASR results for invalid JSON format
  ASSERT_EQ(ModularClientState::IDLE, client_manager->GetClientState());
  client_manager->ASRCompleteResults("{not a valid JSON}");
  ASSERT_EQ(ModularClientState::NLU_ENDED, client_manager->GetClientPreviousState());
  ASSERT_EQ(ModularClientState::IDLE, client_manager->GetClientState());

  //shutdown Client to join client Thread to main Thread
  client_manager->Shutdown();
}

TEST_F(VoiceUIClientTest, TestASRErrorFlow) {
  syslog(LOG_DEBUG, "***** VoiceUIClientTest::TestASRErrorFlow - START");

  bool wake_word_embedded = false;
  VoiceUIClientID clientID = VoiceUIClientID::MODULAR_SOLUTION;

  std::shared_ptr<ModularClientManagerMock> client_manager = std::make_shared<ModularClientManagerMock>(clientID, wake_word_embedded, client_observer_mock_);
  ASSERT_EQ(true, client_manager->Initialize());

  //Simulate ASR ERROR callback
  ASSERT_EQ(ModularClientState::IDLE, client_manager->GetClientState());
  client_manager->ASRError("{\"Error\":\"Code_01\",\"slots\":[{\"name\":\"reason\",\"value\":\"Error processing ASR\"}]}");
  ASSERT_EQ(ModularClientState::ASR_ERROR, client_manager->GetClientPreviousState());
  ASSERT_EQ(ModularClientState::IDLE, client_manager->GetClientState());

  //shutdown Client to join client Thread to main Thread
  client_manager->Shutdown();
}

TEST_F(VoiceUIClientTest, TestQCASRUseCaseIntents) {
  syslog(LOG_DEBUG, "***** VoiceUIClientTest::TestQCASRUseCaseIntents - START");

  bool wake_word_embedded = false;
  VoiceUIClientID clientID = VoiceUIClientID::MODULAR_SOLUTION;

  std::shared_ptr<ModularClientManagerMock> client_manager = std::make_shared<ModularClientManagerMock>(clientID, wake_word_embedded, client_observer_mock_);
  ASSERT_EQ(true, client_manager->Initialize());

  //VOLUME Intent
  client_manager->ASRCompleteResults("{\"intent\":\"VolumeIntent\",\"slots\":[{\"name\":\"Volume\",\"value\":\"null\"},{\"name\":\"VolumeState\",\"value\":\"increase\"}]}");
  ASSERT_EQ(ModularSolutionIntentID::VOLUME, client_manager->GetPreviousIntentProcessed());

  //LIGHT Intent
  client_manager->ASRCompleteResults("{\"intent\":\"LightIntent\",\"slots\":[{\"name\":\"Room\",\"value\":\"living_room\"},{\"name\":\"State\",\"value\":\"off\"}]}");
  ASSERT_EQ(ModularSolutionIntentID::LIGHT, client_manager->GetPreviousIntentProcessed());

  //CONTROL Intent
  client_manager->ASRCompleteResults("{\"intent\":\"ControlIntent\",\"slots\":[{\"name\":\"State\",\"value\":\"on\"},{\"name\":\"Target\",\"value\":\"thermostat\"}]}");
  ASSERT_EQ(ModularSolutionIntentID::CONTROL, client_manager->GetPreviousIntentProcessed());

  //THERMOSTAT Intent
  client_manager->ASRCompleteResults("{\"intent\":\"TemperatureIntent\",\"slots\":[{\"name\":\"Temperature\",\"value\":30},{\"name\":\"Difference\",\"value\":\"absolute\"}]}");
  ASSERT_EQ(ModularSolutionIntentID::THERMOSTAT, client_manager->GetPreviousIntentProcessed());

  //MUSIC Intent
  client_manager->ASRCompleteResults("{\"intent\":\"MusicIntent\",\"slots\":[{\"name\":\"Action\",\"value\":\"play\"},{\"name\":\"MusicRoom\",\"value\":\"living_room\"}]}");
  ASSERT_EQ(ModularSolutionIntentID::MUSIC, client_manager->GetPreviousIntentProcessed());

  //TELEPHONE Intent
  client_manager->ASRCompleteResults("{\"intent\":\"PhoneIntent\",\"slots\":[{\"name\":\"PhoneAction\",\"value\":\"accept\"}]}");
  ASSERT_EQ(ModularSolutionIntentID::TELEPHONE, client_manager->GetPreviousIntentProcessed());

  //SECURITY Intent
  client_manager->ASRCompleteResults("{\"intent\":\"SecurityIntent\",\"slots\":[{\"name\":\"DoorState\",\"value\":\"open\"},{\"name\":\"DoorTarget\",\"value\":\"front\"}]}");
  ASSERT_EQ(ModularSolutionIntentID::SECURITY, client_manager->GetPreviousIntentProcessed());

  //CONNECTION Intent
  client_manager->ASRCompleteResults("{\"intent\":\"ConnectionIntent\",\"slots\":[{\"name\":\"ConnectionTarget\",\"value\":\"optical\"}]}");
  ASSERT_EQ(ModularSolutionIntentID::CONNECTION, client_manager->GetPreviousIntentProcessed());

  //shutdown Client to join client Thread to main Thread
  client_manager->Shutdown();
}

//Volume Use cases
TEST_F(VoiceUIClientTest, TestQCASRVolumeUseCases) {
  syslog(LOG_DEBUG, "***** VoiceUIClientTest::TestQCASRVolumeUseCases - START");

  bool wake_word_embedded = false;
  VoiceUIClientID clientID = VoiceUIClientID::MODULAR_SOLUTION;

  std::shared_ptr<ModularClientManagerMock> client_manager = std::make_shared<ModularClientManagerMock>(clientID, wake_word_embedded, client_observer_mock_);

  ASSERT_EQ(true, client_manager->Initialize());

  //VOLUME Increase
  client_manager->ASRCompleteResults("{\"intent\":\"VolumeIntent\",\"slots\":[{\"name\":\"Volume\",\"value\":\"null\"},{\"name\":\"VolumeState\",\"value\":\"increase\"}]}");
  ASSERT_EQ(ModularSolutionIntentID::VOLUME, client_manager->GetPreviousIntentProcessed());
  ASSERT_EQ(IntentManagerEvent::INCREASE_VOLUME, client_observer_mock_->GetLastIntentSent());

  //VOLUME Decrease
  client_manager->ASRCompleteResults("{\"intent\":\"VolumeIntent\",\"slots\":[{\"name\":\"Volume\",\"value\":\"null\"},{\"name\":\"VolumeState\",\"value\":\"decrease\"}]}");
  ASSERT_EQ(ModularSolutionIntentID::VOLUME, client_manager->GetPreviousIntentProcessed());
  ASSERT_EQ(IntentManagerEvent::DECREASE_VOLUME, client_observer_mock_->GetLastIntentSent());

  //VOLUME Set Absolute
  client_manager->ASRCompleteResults("{\"intent\":\"VolumeIntent\",\"slots\":[{\"name\":\"Volume\",\"value\":\"8\"},{\"name\":\"VolumeState\",\"value\":\"absolute\"}]}");
  ASSERT_EQ(ModularSolutionIntentID::VOLUME, client_manager->GetPreviousIntentProcessed());
  ASSERT_EQ(IntentManagerEvent::SET_VOLUME_ABSOLUTE, client_observer_mock_->GetLastIntentSent());
  ASSERT_EQ(0.8, client_observer_mock_->GetLastEventExtended().value_double_1);

  //VOLUME Mute
  client_manager->ASRCompleteResults("{\"intent\":\"VolumeIntent\",\"slots\":[{\"name\":\"Volume\",\"value\":\"null\"},{\"name\":\"VolumeState\",\"value\":\"mute\"}]}");
  ASSERT_EQ(ModularSolutionIntentID::VOLUME, client_manager->GetPreviousIntentProcessed());
  ASSERT_EQ(IntentManagerEvent::SET_MUTE_STATE, client_observer_mock_->GetLastIntentSent());
  ASSERT_EQ(true, client_observer_mock_->GetLastEventExtended().bool_state_1);

  //VOLUME Unmute
  client_manager->ASRCompleteResults("{\"intent\":\"VolumeIntent\",\"slots\":[{\"name\":\"Volume\",\"value\":\"null\"},{\"name\":\"VolumeState\",\"value\":\"unmute\"}]}");
  ASSERT_EQ(ModularSolutionIntentID::VOLUME, client_manager->GetPreviousIntentProcessed());
  ASSERT_EQ(IntentManagerEvent::SET_MUTE_STATE, client_observer_mock_->GetLastIntentSent());
  ASSERT_EQ(false, client_observer_mock_->GetLastEventExtended().bool_state_1);

  //shutdown Client to join client Thread to main Thread
  client_manager->Shutdown();
}

//Light Use cases
TEST_F(VoiceUIClientTest, TestQCASRLightUseCases) {
  syslog(LOG_DEBUG, "***** VoiceUIClientTest::TestQCASRLightUseCases - START");

  bool wake_word_embedded = false;
  VoiceUIClientID clientID = VoiceUIClientID::MODULAR_SOLUTION;

  std::shared_ptr<ModularClientManagerMock> client_manager = std::make_shared<ModularClientManagerMock>(clientID, wake_word_embedded, client_observer_mock_);

  ASSERT_EQ(true, client_manager->Initialize());

  //Kitchen ON
  client_manager->ASRCompleteResults("{\"intent\":\"LightIntent\",\"slots\":[{\"name\":\"Room\",\"value\":\"kitchen\"},{\"name\":\"State\",\"value\":\"on\"}]}");
  ASSERT_EQ(ModularSolutionIntentID::LIGHT, client_manager->GetPreviousIntentProcessed());
  ASSERT_EQ(IntentManagerEvent::SET_LIGHT_STATE, client_observer_mock_->GetLastIntentSent());
  ASSERT_EQ("kitchen", client_observer_mock_->GetLastEventExtended().ts.target);
  ASSERT_EQ("on", client_observer_mock_->GetLastEventExtended().ts.state);

  //Kitchen OFF
  client_manager->ASRCompleteResults("{\"intent\":\"LightIntent\",\"slots\":[{\"name\":\"Room\",\"value\":\"kitchen\"},{\"name\":\"State\",\"value\":\"off\"}]}");
  ASSERT_EQ(ModularSolutionIntentID::LIGHT, client_manager->GetPreviousIntentProcessed());
  ASSERT_EQ(IntentManagerEvent::SET_LIGHT_STATE, client_observer_mock_->GetLastIntentSent());
  ASSERT_EQ("kitchen", client_observer_mock_->GetLastEventExtended().ts.target);
  ASSERT_EQ("off", client_observer_mock_->GetLastEventExtended().ts.state);

  //Living room ON
  client_manager->ASRCompleteResults("{\"intent\":\"LightIntent\",\"slots\":[{\"name\":\"Room\",\"value\":\"living_room\"},{\"name\":\"State\",\"value\":\"on\"}]}");
  ASSERT_EQ(ModularSolutionIntentID::LIGHT, client_manager->GetPreviousIntentProcessed());
  ASSERT_EQ(IntentManagerEvent::SET_LIGHT_STATE, client_observer_mock_->GetLastIntentSent());
  ASSERT_EQ("living_room", client_observer_mock_->GetLastEventExtended().ts.target);
  ASSERT_EQ("on", client_observer_mock_->GetLastEventExtended().ts.state);

  //Living room OFF
  client_manager->ASRCompleteResults("{\"intent\":\"LightIntent\",\"slots\":[{\"name\":\"Room\",\"value\":\"living_room\"},{\"name\":\"State\",\"value\":\"off\"}]}");
  ASSERT_EQ(ModularSolutionIntentID::LIGHT, client_manager->GetPreviousIntentProcessed());
  ASSERT_EQ(IntentManagerEvent::SET_LIGHT_STATE, client_observer_mock_->GetLastIntentSent());
  ASSERT_EQ("living_room", client_observer_mock_->GetLastEventExtended().ts.target);
  ASSERT_EQ("off", client_observer_mock_->GetLastEventExtended().ts.state);

  //Bedroom ON
  client_manager->ASRCompleteResults("{\"intent\":\"LightIntent\",\"slots\":[{\"name\":\"Room\",\"value\":\"bedroom\"},{\"name\":\"State\",\"value\":\"on\"}]}");
  ASSERT_EQ(ModularSolutionIntentID::LIGHT, client_manager->GetPreviousIntentProcessed());
  ASSERT_EQ(IntentManagerEvent::SET_LIGHT_STATE, client_observer_mock_->GetLastIntentSent());
  ASSERT_EQ("bedroom", client_observer_mock_->GetLastEventExtended().ts.target);
  ASSERT_EQ("on", client_observer_mock_->GetLastEventExtended().ts.state);

  //Bedroom OFF
  client_manager->ASRCompleteResults("{\"intent\":\"LightIntent\",\"slots\":[{\"name\":\"State\",\"value\":\"off\"},{\"name\":\"Room\",\"value\":\"bedroom\"}]}");
  ASSERT_EQ(ModularSolutionIntentID::LIGHT, client_manager->GetPreviousIntentProcessed());
  ASSERT_EQ(IntentManagerEvent::SET_LIGHT_STATE, client_observer_mock_->GetLastIntentSent());
  ASSERT_EQ("bedroom", client_observer_mock_->GetLastEventExtended().ts.target);
  ASSERT_EQ("off", client_observer_mock_->GetLastEventExtended().ts.state);

  //ALL ON
  client_manager->ASRCompleteResults("{\"intent\":\"LightIntent\",\"slots\":[{\"name\":\"Room\",\"value\":\"all\"},{\"name\":\"State\",\"value\":\"on\"}]}");
  ASSERT_EQ(ModularSolutionIntentID::LIGHT, client_manager->GetPreviousIntentProcessed());
  ASSERT_EQ(IntentManagerEvent::SET_LIGHT_STATE, client_observer_mock_->GetLastIntentSent());
  ASSERT_EQ("all", client_observer_mock_->GetLastEventExtended().ts.target);
  ASSERT_EQ("on", client_observer_mock_->GetLastEventExtended().ts.state);

  //ALL OFF
  client_manager->ASRCompleteResults("{\"intent\":\"LightIntent\",\"slots\":[{\"name\":\"Room\",\"value\":\"all\"},{\"name\":\"State\",\"value\":\"off\"}]}");
  ASSERT_EQ(ModularSolutionIntentID::LIGHT, client_manager->GetPreviousIntentProcessed());
  ASSERT_EQ(IntentManagerEvent::SET_LIGHT_STATE, client_observer_mock_->GetLastIntentSent());
  ASSERT_EQ("all", client_observer_mock_->GetLastEventExtended().ts.target);
  ASSERT_EQ("off", client_observer_mock_->GetLastEventExtended().ts.state);

  //shutdown Client to join client Thread to main Thread
  client_manager->Shutdown();
}

//Control Use cases
TEST_F(VoiceUIClientTest, TestQCASRControlUseCases) {
  syslog(LOG_DEBUG, "***** VoiceUIClientTest::TestQCASRControlUseCases - START");

  bool wake_word_embedded = false;
  VoiceUIClientID clientID = VoiceUIClientID::MODULAR_SOLUTION;

  std::shared_ptr<ModularClientManagerMock> client_manager = std::make_shared<ModularClientManagerMock>(clientID, wake_word_embedded, client_observer_mock_);

  ASSERT_EQ(true, client_manager->Initialize());

  //Thermostat ON
  client_manager->ASRCompleteResults("{\"intent\":\"ControlIntent\",\"slots\":[{\"name\":\"State\",\"value\":\"on\"},{\"name\":\"Target\",\"value\":\"thermostat\"}]}");
  ASSERT_EQ(ModularSolutionIntentID::CONTROL, client_manager->GetPreviousIntentProcessed());
  ASSERT_EQ(IntentManagerEvent::SET_CONTROL_DEVICE_STATE, client_observer_mock_->GetLastIntentSent());
  ASSERT_EQ("thermostat", client_observer_mock_->GetLastEventExtended().ts.target);
  ASSERT_EQ("on", client_observer_mock_->GetLastEventExtended().ts.state);

  //Thermostat OFF
  client_manager->ASRCompleteResults("{\"intent\":\"ControlIntent\",\"slots\":[{\"name\":\"State\",\"value\":\"off\"},{\"name\":\"Target\",\"value\":\"thermostat\"}]}");
  ASSERT_EQ(ModularSolutionIntentID::CONTROL, client_manager->GetPreviousIntentProcessed());
  ASSERT_EQ(IntentManagerEvent::SET_CONTROL_DEVICE_STATE, client_observer_mock_->GetLastIntentSent());
  ASSERT_EQ("thermostat", client_observer_mock_->GetLastEventExtended().ts.target);
  ASSERT_EQ("off", client_observer_mock_->GetLastEventExtended().ts.state);

  //shutdown Client to join client Thread to main Thread
  client_manager->Shutdown();
}

//Thermostat Use cases
TEST_F(VoiceUIClientTest, TestQCASRThermostatUseCases) {
  syslog(LOG_DEBUG, "***** VoiceUIClientTest::TestQCASRThermostatUseCases - START");

  bool wake_word_embedded = false;
  VoiceUIClientID clientID = VoiceUIClientID::MODULAR_SOLUTION;

  std::shared_ptr<ModularClientManagerMock> client_manager = std::make_shared<ModularClientManagerMock>(clientID, wake_word_embedded, client_observer_mock_);

  ASSERT_EQ(true, client_manager->Initialize());

  //Thermostat Set Temperature
  client_manager->ASRCompleteResults("{\"intent\":\"TemperatureIntent\",\"slots\":[{\"name\":\"Temperature\",\"value\":30},{\"name\":\"Difference\",\"value\":\"absolute\"}]}");
  ASSERT_EQ(ModularSolutionIntentID::THERMOSTAT, client_manager->GetPreviousIntentProcessed());

  ASSERT_EQ(IntentManagerEvent::SET_THERMOSTAT_STATE_ABSOLUTE, client_observer_mock_->GetLastIntentSent());
  ASSERT_EQ(30, client_observer_mock_->GetLastEventExtended().value_uint);

  //Thermostat Decrease Temperature
  client_manager->ASRCompleteResults("{\"intent\":\"TemperatureIntent\",\"slots\":[{\"name\":\"Difference\",\"value\":\"decrease\"},{\"name\":\"Temperature\",\"value\":\"null\"}]}");
  ASSERT_EQ(ModularSolutionIntentID::THERMOSTAT, client_manager->GetPreviousIntentProcessed());
  ASSERT_EQ(IntentManagerEvent::DECREASE_THERMOSTAT_TEMPERATURE, client_observer_mock_->GetLastIntentSent());

  //Thermostat Increase Temperature
  client_manager->ASRCompleteResults("{\"intent\":\"TemperatureIntent\",\"slots\":[{\"name\":\"Difference\",\"value\":\"increase\"},{\"name\":\"Temperature\",\"value\":\"null\"}]}");
  ASSERT_EQ(ModularSolutionIntentID::THERMOSTAT, client_manager->GetPreviousIntentProcessed());
  ASSERT_EQ(IntentManagerEvent::INCREASE_THERMOSTAT_TEMPERATURE, client_observer_mock_->GetLastIntentSent());

  //shutdown Client to join client Thread to main Thread
  client_manager->Shutdown();
}

//Music Use cases
TEST_F(VoiceUIClientTest, TestQCASRMusicUseCases) {
  syslog(LOG_DEBUG, "***** VoiceUIClientTest::TestQCASRMusicUseCases - START");

  bool wake_word_embedded = false;
  VoiceUIClientID clientID = VoiceUIClientID::MODULAR_SOLUTION;

  std::shared_ptr<ModularClientManagerMock> client_manager = std::make_shared<ModularClientManagerMock>(clientID, wake_word_embedded, client_observer_mock_);

  ASSERT_EQ(true, client_manager->Initialize());

  //Play Music
  client_manager->ASRCompleteResults("{\"intent\":\"MusicIntent\",\"slots\":[{\"name\":\"Action\",\"value\":\"play\"},{\"name\":\"MusicRoom\",\"value\":\"default\"}]}");
  ASSERT_EQ(ModularSolutionIntentID::MUSIC, client_manager->GetPreviousIntentProcessed());
  ASSERT_EQ(IntentManagerEvent::START_PLAYBACK, client_observer_mock_->GetLastIntentSent());

  //Stop Music
  client_manager->ASRCompleteResults("{\"intent\":\"MusicIntent\",\"slots\":[{\"name\":\"Action\",\"value\":\"stop\"},{\"name\":\"MusicRoom\",\"value\":\"default\"}]}");
  ASSERT_EQ(ModularSolutionIntentID::MUSIC, client_manager->GetPreviousIntentProcessed());
  ASSERT_EQ(IntentManagerEvent::STOP_PLAYBACK, client_observer_mock_->GetLastIntentSent());

  //Pause Music
  client_manager->ASRCompleteResults("{\"intent\":\"MusicIntent\",\"slots\":[{\"name\":\"Action\",\"value\":\"pause\"},{\"name\":\"MusicRoom\",\"value\":\"default\"}]}");
  ASSERT_EQ(ModularSolutionIntentID::MUSIC, client_manager->GetPreviousIntentProcessed());
  ASSERT_EQ(IntentManagerEvent::PAUSE_PLAYBACK, client_observer_mock_->GetLastIntentSent());

  //Previous Music
  client_manager->ASRCompleteResults("{\"intent\":\"MusicIntent\",\"slots\":[{\"name\":\"Action\",\"value\":\"previous\"},{\"name\":\"MusicRoom\",\"value\":\"default\"}]}");
  ASSERT_EQ(ModularSolutionIntentID::MUSIC, client_manager->GetPreviousIntentProcessed());
  ASSERT_EQ(IntentManagerEvent::PLAY_PREVIOUS_TRACK, client_observer_mock_->GetLastIntentSent());

  //Next Music
  client_manager->ASRCompleteResults("{\"intent\":\"MusicIntent\",\"slots\":[{\"name\":\"Action\",\"value\":\"next\"},{\"name\":\"MusicRoom\",\"value\":\"default\"}]}");
  ASSERT_EQ(ModularSolutionIntentID::MUSIC, client_manager->GetPreviousIntentProcessed());
  ASSERT_EQ(IntentManagerEvent::PLAY_NEXT_TRACK, client_observer_mock_->GetLastIntentSent());

  //Play music in the Living room
  client_manager->ASRCompleteResults("{\"intent\":\"MusicIntent\",\"slots\":[{\"name\":\"Action\",\"value\":\"play\"},{\"name\":\"MusicRoom\",\"value\":\"living_room\"}]}");
  ASSERT_EQ(ModularSolutionIntentID::MUSIC, client_manager->GetPreviousIntentProcessed());
  ASSERT_EQ(IntentManagerEvent::START_PLAYBACK, client_observer_mock_->GetLastIntentSent());
  ASSERT_EQ("living_room", client_observer_mock_->GetLastEventExtended().str_1);

  //Set up party mode
  client_manager->ASRCompleteResults("{\"intent\":\"MusicIntent\",\"slots\":[{\"name\":\"Action\",\"value\":\"party\"},{\"name\":\"MusicRoom\",\"value\":\"default\"}]}");
  ASSERT_EQ(ModularSolutionIntentID::MUSIC, client_manager->GetPreviousIntentProcessed());

  //Set up PA mode
  client_manager->ASRCompleteResults("{\"intent\":\"MusicIntent\",\"slots\":[{\"name\":\"Action\",\"value\":\"pa\"},{\"name\":\"MusicRoom\",\"value\":\"default\"}]}");
  ASSERT_EQ(ModularSolutionIntentID::MUSIC, client_manager->GetPreviousIntentProcessed());

  //shutdown Client to join client Thread to main Thread
  client_manager->Shutdown();
}

//Telephone Use cases
//TODO CREATE INTENTS ON INTENT MANAGER AND CHECK  SPECIFIC IntentManagerEvent HERE
TEST_F(VoiceUIClientTest, TestQCASRTelephoneUseCases) {
  syslog(LOG_DEBUG, "***** VoiceUIClientTest::TestQCASRTelephoneUseCases - START");

  bool wake_word_embedded = false;
  VoiceUIClientID clientID = VoiceUIClientID::MODULAR_SOLUTION;

  std::shared_ptr<ModularClientManagerMock> client_manager = std::make_shared<ModularClientManagerMock>(clientID, wake_word_embedded, client_observer_mock_);

  ASSERT_EQ(true, client_manager->Initialize());

  //Accept Call
  client_manager->ASRCompleteResults("{\"intent\":\"PhoneIntent\",\"slots\":[{\"name\":\"PhoneAction\",\"value\":\"accept\"}]}");
  ASSERT_EQ(ModularSolutionIntentID::TELEPHONE, client_manager->GetPreviousIntentProcessed());
  ASSERT_EQ(IntentManagerEvent::HANDLE_CALL, client_observer_mock_->GetLastIntentSent());
  ASSERT_EQ("accept", client_observer_mock_->GetLastEventExtended().str_1);

  //Reject Call
  client_manager->ASRCompleteResults("{\"intent\":\"PhoneIntent\",\"slots\":[{\"name\":\"PhoneAction\",\"value\":\"reject\"}]}");
  ASSERT_EQ(ModularSolutionIntentID::TELEPHONE, client_manager->GetPreviousIntentProcessed());
  ASSERT_EQ(IntentManagerEvent::HANDLE_CALL, client_observer_mock_->GetLastIntentSent());
  ASSERT_EQ("reject", client_observer_mock_->GetLastEventExtended().str_1);

  //Hang Up Call
  client_manager->ASRCompleteResults("{\"intent\":\"PhoneIntent\",\"slots\":[{\"name\":\"PhoneAction\",\"value\":\"hang_up\"}]}");
  ASSERT_EQ(ModularSolutionIntentID::TELEPHONE, client_manager->GetPreviousIntentProcessed());
  ASSERT_EQ(IntentManagerEvent::HANDLE_CALL, client_observer_mock_->GetLastIntentSent());
  ASSERT_EQ("hang_up", client_observer_mock_->GetLastEventExtended().str_1);

  //shutdown Client to join client Thread to main Thread
  client_manager->Shutdown();
}

//Security Use cases
TEST_F(VoiceUIClientTest, TestQCASRSecurityUseCases) {
  syslog(LOG_DEBUG, "***** VoiceUIClientTest::TestQCASRSecurityUseCases - START");

  bool wake_word_embedded = false;
  VoiceUIClientID clientID = VoiceUIClientID::MODULAR_SOLUTION;

  std::shared_ptr<ModularClientManagerMock> client_manager = std::make_shared<ModularClientManagerMock>(clientID, wake_word_embedded, client_observer_mock_);

  ASSERT_EQ(true, client_manager->Initialize());

  //Front Door Open
  client_manager->ASRCompleteResults("{\"intent\":\"SecurityIntent\",\"slots\":[{\"name\":\"DoorState\",\"value\":\"open\"},{\"name\":\"DoorTarget\",\"value\":\"front\"}]}");
  ASSERT_EQ(ModularSolutionIntentID::SECURITY, client_manager->GetPreviousIntentProcessed());
  ASSERT_EQ(IntentManagerEvent::SET_SECURITY_STATE, client_observer_mock_->GetLastIntentSent());
  ASSERT_EQ("front", client_observer_mock_->GetLastEventExtended().ts.target);
  ASSERT_EQ("open", client_observer_mock_->GetLastEventExtended().ts.state);

  //Front Door Close
  client_manager->ASRCompleteResults("{\"intent\":\"SecurityIntent\",\"slots\":[{\"name\":\"DoorTarget\",\"value\":\"front\"},{\"name\":\"DoorState\",\"value\":\"close\"}]}");
  ASSERT_EQ(ModularSolutionIntentID::SECURITY, client_manager->GetPreviousIntentProcessed());
  ASSERT_EQ(IntentManagerEvent::SET_SECURITY_STATE, client_observer_mock_->GetLastIntentSent());
  ASSERT_EQ("front", client_observer_mock_->GetLastEventExtended().ts.target);
  ASSERT_EQ("close", client_observer_mock_->GetLastEventExtended().ts.state);

  //Garage Door Open
  client_manager->ASRCompleteResults("{\"intent\":\"SecurityIntent\",\"slots\":[{\"name\":\"DoorTarget\",\"value\":\"garage\"},{\"name\":\"DoorState\",\"value\":\"open\"}]}");
  ASSERT_EQ(ModularSolutionIntentID::SECURITY, client_manager->GetPreviousIntentProcessed());
  ASSERT_EQ(IntentManagerEvent::SET_SECURITY_STATE, client_observer_mock_->GetLastIntentSent());
  ASSERT_EQ("garage", client_observer_mock_->GetLastEventExtended().ts.target);
  ASSERT_EQ("open", client_observer_mock_->GetLastEventExtended().ts.state);

  //Garage Door Close
  client_manager->ASRCompleteResults("{\"intent\":\"SecurityIntent\",\"slots\":[{\"name\":\"DoorState\",\"value\":\"close\"},{\"name\":\"DoorTarget\",\"value\":\"garage\"}]}");
  ASSERT_EQ(ModularSolutionIntentID::SECURITY, client_manager->GetPreviousIntentProcessed());
  ASSERT_EQ(IntentManagerEvent::SET_SECURITY_STATE, client_observer_mock_->GetLastIntentSent());
  ASSERT_EQ("garage", client_observer_mock_->GetLastEventExtended().ts.target);
  ASSERT_EQ("close", client_observer_mock_->GetLastEventExtended().ts.state);

  //shutdown Client to join client Thread to main Thread
  client_manager->Shutdown();
}

//Connection Use cases
TEST_F(VoiceUIClientTest, TestQCASRConnectionUseCases) {
  syslog(LOG_DEBUG, "***** VoiceUIClientTest::TestQCASRConnectionUseCases - START");

  bool wake_word_embedded = false;
  VoiceUIClientID clientID = VoiceUIClientID::MODULAR_SOLUTION;

  std::shared_ptr<ModularClientManagerMock> client_manager = std::make_shared<ModularClientManagerMock>(clientID, wake_word_embedded, client_observer_mock_);

  ASSERT_EQ(true, client_manager->Initialize());

  //HDMI Input
  client_manager->ASRCompleteResults("{\"intent\":\"ConnectionIntent\",\"slots\":[{\"name\":\"ConnectionTarget\",\"value\":\"hdmi\"}]}");
  ASSERT_EQ(ModularSolutionIntentID::CONNECTION, client_manager->GetPreviousIntentProcessed());
  ASSERT_EQ(IntentManagerEvent::SELECT_SOURCE, client_observer_mock_->GetLastIntentSent());
  ASSERT_EQ("hdmi", client_observer_mock_->GetLastEventExtended().str_1);

  //TV Input
  client_manager->ASRCompleteResults("{\"intent\":\"ConnectionIntent\",\"slots\":[{\"name\":\"ConnectionTarget\",\"value\":\"tv\"}]}");
  ASSERT_EQ(ModularSolutionIntentID::CONNECTION, client_manager->GetPreviousIntentProcessed());
  ASSERT_EQ(IntentManagerEvent::SELECT_SOURCE, client_observer_mock_->GetLastIntentSent());
  ASSERT_EQ("tv", client_observer_mock_->GetLastEventExtended().str_1);

  //Optical Input
  client_manager->ASRCompleteResults("{\"intent\":\"ConnectionIntent\",\"slots\":[{\"name\":\"ConnectionTarget\",\"value\":\"optical\"}]}");
  ASSERT_EQ(ModularSolutionIntentID::CONNECTION, client_manager->GetPreviousIntentProcessed());
  ASSERT_EQ(IntentManagerEvent::SELECT_SOURCE, client_observer_mock_->GetLastIntentSent());
  ASSERT_EQ("optical", client_observer_mock_->GetLastEventExtended().str_1);

  //Digital Input
  client_manager->ASRCompleteResults("{\"intent\":\"ConnectionIntent\",\"slots\":[{\"name\":\"ConnectionTarget\",\"value\":\"digital\"}]}");
  ASSERT_EQ(ModularSolutionIntentID::CONNECTION, client_manager->GetPreviousIntentProcessed());
  ASSERT_EQ(IntentManagerEvent::SELECT_SOURCE, client_observer_mock_->GetLastIntentSent());
  ASSERT_EQ("digital", client_observer_mock_->GetLastEventExtended().str_1);

  //Analog Input
  client_manager->ASRCompleteResults("{\"intent\":\"ConnectionIntent\",\"slots\":[{\"name\":\"ConnectionTarget\",\"value\":\"analog\"}]}");
  ASSERT_EQ(ModularSolutionIntentID::CONNECTION, client_manager->GetPreviousIntentProcessed());
  ASSERT_EQ(IntentManagerEvent::SELECT_SOURCE, client_observer_mock_->GetLastIntentSent());
  ASSERT_EQ("analog", client_observer_mock_->GetLastEventExtended().str_1);

  //USB Input
  client_manager->ASRCompleteResults("{\"intent\":\"ConnectionIntent\",\"slots\":[{\"name\":\"ConnectionTarget\",\"value\":\"usb\"}]}");
  ASSERT_EQ(ModularSolutionIntentID::CONNECTION, client_manager->GetPreviousIntentProcessed());
  ASSERT_EQ(IntentManagerEvent::SELECT_SOURCE, client_observer_mock_->GetLastIntentSent());
  ASSERT_EQ("usb", client_observer_mock_->GetLastEventExtended().str_1);

  //Bluetooth Input
  client_manager->ASRCompleteResults("{\"intent\":\"ConnectionIntent\",\"slots\":[{\"name\":\"ConnectionTarget\",\"value\":\"bluetooth\"}]}");
  ASSERT_EQ(ModularSolutionIntentID::CONNECTION, client_manager->GetPreviousIntentProcessed());
  ASSERT_EQ(IntentManagerEvent::SELECT_SOURCE, client_observer_mock_->GetLastIntentSent());
  ASSERT_EQ("bluetooth", client_observer_mock_->GetLastEventExtended().str_1);

  //WiFi Input
  client_manager->ASRCompleteResults("{\"intent\":\"ConnectionIntent\",\"slots\":[{\"name\":\"ConnectionTarget\",\"value\":\"wifi\"}]}");
  ASSERT_EQ(ModularSolutionIntentID::CONNECTION, client_manager->GetPreviousIntentProcessed());
  ASSERT_EQ(IntentManagerEvent::SELECT_SOURCE, client_observer_mock_->GetLastIntentSent());
  ASSERT_EQ("wifi", client_observer_mock_->GetLastEventExtended().str_1);

  //shutdown Client to join client Thread to main Thread
  client_manager->Shutdown();
}

//client_manager->ASRCompleteResults("{\"intent\":\"ConnectionIntent\",\"slots\":[{\"name\":\"ConnectionTarget\",\"value\":\"optical\"}]}");
//ASSERT_EQ(ModularSolutionIntentID::CONNECTION,client_manager->GetPreviousIntentProcessed());

}  // namespace
   // namespace

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
