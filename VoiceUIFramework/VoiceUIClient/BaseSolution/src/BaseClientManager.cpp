/* Copyright (c) 2018-2020, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    BaseClientManager.cpp
 *  @brief   Base Client Manager for Voice UI Client Manager implementation
 *
 *  DESCRIPTION
 *    Base class for both Modular and Full solution Client Manager implementation
 ***************************************************************/

#include <VoiceUIClient/BaseSolution/BaseClientManager.h>

namespace voiceUIFramework {
namespace voiceUIClient {

BaseClientManager::BaseClientManager(
    VoiceUIClientID client_id,
    bool wake_word_embedded,
    std::shared_ptr<voiceUIFramework::voiceUIClient::ClientObserver> client_observer)
    : wake_word_embedded_{wake_word_embedded}, client_observer_{client_observer}, client_id_{client_id}, active_{false}, mute_{false} {
  syslog(LOG_INFO, "BaseClientManager Constructor called for %s", ClientIDToString(client_id).c_str());
}

BaseClientManager::~BaseClientManager() {
  syslog(LOG_INFO, "---BaseClientManager::~BaseClientManager");
}

bool BaseClientManager::IsActive() {
  std::lock_guard<std::mutex> lock(mu);
  syslog(LOG_DEBUG, "BaseClientManager::IsActive() = %d - ClientID = %s", active_, ClientIDToString(client_id_).c_str());
  return active_;
}

bool BaseClientManager::SetActive(bool state) {
  std::lock_guard<std::mutex> lock(mu);
  int result = 0;

  syslog(LOG_DEBUG, "BaseClientManager::ClientID = %s - New State = %d", ClientIDToString(client_id_).c_str(), state);

  if (active_ == state) {
    syslog(LOG_DEBUG, "BaseClientManager::Prev state %d, New state %d, Nothing to be done", active_, state);
    return true;
  }

  if (!HasWakeWordEngine() && wake_word_) {
    if (state) {
      if (wake_word_) {
        result = wake_word_->StartRecognition(client_id_);
      }
    } else {
      if (wake_word_) {
        result = wake_word_->StopRecognition(client_id_);
        //RemoveObserver Instead?
      }
    }

    if (result != 0) {
      syslog(LOG_DEBUG, "BaseClientManager::ClientID = %s - Wakeword component fail to Start/Stop ", ClientIDToString(client_id_).c_str());
      //do not return false, cause client can still be used as TAP profile
      //return false;
    }
  } else {
    //TODO - VOICE UI Solutions with their own wakeword engine
  }
  active_ = state;
  //broadcast status to Voice UI Manager
  if (client_observer_) {
    client_observer_->ActiveStatus(GetClientID(), active_);
  }

  syslog(LOG_DEBUG, "BaseClientManager::ClientID = %s -   active_= %d", ClientIDToString(client_id_).c_str(), active_);

  return true;
}

bool BaseClientManager::IsMute() {
  std::lock_guard<std::mutex> lock(mu);
  syslog(LOG_DEBUG, "BaseClientManager::IsActive() = %d", mute_);
  return mute_;
}

bool BaseClientManager::SetMute(bool state) {
  std::lock_guard<std::mutex> lock(mu);
  mute_ = state;
  return mute_;
}

bool BaseClientManager::SendIntent(IntentManagerEvent intent) {
  if (intent == IntentManagerEvent::UNKNOWN_INTENT)
    return false;

  client_observer_->SendIntent(GetClientID(), intent);
  return true;
}
bool BaseClientManager::SendIntent(IntentManagerEvent intent, IntentManagerEventExtended intent_ext) {
  if (intent == IntentManagerEvent::UNKNOWN_INTENT)
    return false;

  client_observer_->SendIntent(GetClientID(), intent, intent_ext);
  return true;
}

VoiceUIClientID BaseClientManager::GetClientID() {
  //syslog(LOG_DEBUG, "BaseClientManager::GetClientID() - %d", client_id_);
  return client_id_;
}

bool BaseClientManager::SetAudioRecorder(std::shared_ptr<AudioRecorderInterface> audio_recorder) {
  audio_recorder_ = audio_recorder;
  if (wake_word_) {
    audio_recorder_->SetWakewordEngine(wake_word_);
  }

  return true;
}

bool BaseClientManager::HasWakeWordEngine() {
  return wake_word_embedded_;
}

}  // namespace voiceUIClient
}  // namespace voiceUIFramework
