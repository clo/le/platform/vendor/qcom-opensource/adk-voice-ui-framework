/* Copyright (c) 2018, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    BaseVoiceUIClient.cpp
 *  @brief   Base client for Voice UI Client implementation
 *
 *  DESCRIPTION
 *    Base class for both Modular and Full solution Client implementation
 ***************************************************************/

#include <VoiceUIClient/BaseSolution/BaseVoiceUIClient.h>

namespace voiceUIFramework {
namespace voiceUIClient {

BaseVoiceUIClient::BaseVoiceUIClient(
    std::shared_ptr<voiceUIFramework::voiceUIClient::ClientObserver> client_observer) {
  syslog(LOG_INFO, "BaseVoiceUIClient Constructor");
}

BaseVoiceUIClient::~BaseVoiceUIClient() {
  syslog(LOG_INFO, "---BaseVoiceUIClient::~BaseVoiceUIClient");
}

bool BaseVoiceUIClient::Start() {
  syslog(LOG_INFO, "BaseVoiceUIClient::Start()");
  //Launch Client Thread
  client_thread_ = std::thread([&]() {
    if (base_client_manager_->Initialize()) {
      SetActive(true);
    } else {
      syslog(LOG_ERR, "BaseVoiceUIClient::Start() - ClientManager fail to Initialize");
      SetActive(false);
    }
  });
  return true;
}

bool BaseVoiceUIClient::Stop() {
  return true;
}

bool BaseVoiceUIClient::IsActive() {
  syslog(LOG_INFO, "BaseVoiceUIClient::IsActive() = %d", base_client_manager_->IsActive());
  return base_client_manager_->IsActive();
}
bool BaseVoiceUIClient::SetActive(bool status) {
  syslog(LOG_INFO, "BaseVoiceUIClient::SetActive = %d", status);
  return base_client_manager_->SetActive(status);
}

bool BaseVoiceUIClient::IsMute() {
  return base_client_manager_->IsMute();
}

bool BaseVoiceUIClient::SetMute(bool status) {
  return base_client_manager_->SetMute(status);
}

void BaseVoiceUIClient::Shutdown() {
  syslog(LOG_INFO, "BaseVoiceUIClient::~Shutdown");

  //Call shutdown from manager
  base_client_manager_->Shutdown();

  //Join Client Thread to main Thread
  if (client_thread_.joinable()) {
    client_thread_.join();
  }
  syslog(LOG_INFO, "BaseVoiceUIClient::~Shutdown - DONE");
}

VoiceUIClientID BaseVoiceUIClient::GetClientID() {
  syslog(LOG_DEBUG, "BaseVoiceUIClient::GetClientID() - %d", base_client_manager_->GetClientID());
  return base_client_manager_->GetClientID();
}

void BaseVoiceUIClient::AppMessageReceived(AppMessage app_message) {
  syslog(LOG_DEBUG, "BaseVoiceUIClient::AppMessageReceived() = %s - Client: %s", AppMessageToString(app_message).c_str(), ClientIDToString(base_client_manager_->GetClientID()).c_str());
  base_client_manager_->AppMessageReceived(app_message);
}

}  // namespace voiceUIClient
}  // namespace voiceUIFramework
