/* Copyright (c) 2018, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    ClientObserver.h
 *  @brief   brief description
 *
 *  DESCRIPTION
 *    description
 ***************************************************************/

#ifndef VOICEUIFRAMEWORK_VOICEUICLIENT_INCLUDE_VOICEUICLIENT_CLIENTOBSERVER_H_
#define VOICEUIFRAMEWORK_VOICEUICLIENT_INCLUDE_VOICEUICLIENT_CLIENTOBSERVER_H_

#include <IntentManager/IntentManagerEvent.h>
#include <VoiceUIUtils/GlobalValues.h>

using namespace voiceUIFramework::voiceUIUtils;
using namespace voiceUIFramework::voiceUIIntentManager;

namespace voiceUIFramework {
namespace voiceUIClient {

//Message from App or Equivalents(Non-Contextual threads etc) to clients
//For Generic Events from Apps that affect the clients
enum class AppMessage {
  INTERNET_UP,
  INTERNET_DOWN,
  UNKNOWN_MESSAGE
};

/* Method to convert Intent to String */
inline const std::string AppMessageToString(AppMessage event) {
  switch (event) {
    case AppMessage::INTERNET_UP:
      return "INTERNET UP";
    case AppMessage::INTERNET_DOWN:
      return "INTERNET DOWN";
  }
  return "Invalid/Unknown Message";
}

/* An enum class for all Intents Commands/Events handled by Voice UI Framework */
enum class VUISolutionStatus {
  IDLE,
  LISTENING,
  BUSY,
  SPEAKING,
  EXPECTING_SPEECH,
  PLAYING_MUSIC,  // TODO for AVS Client
  INVALID
};

/* Method to convert Intent to String */
inline const std::string VUISolutionStatusToString(VUISolutionStatus status) {
  switch (status) {
    case VUISolutionStatus::IDLE:
      return "Idle";
    case VUISolutionStatus::LISTENING:
      return "Listening/ASR";
    case VUISolutionStatus::BUSY:
      return "Busy/NLU";
    case VUISolutionStatus::SPEAKING:
      return "Speaking/TTS";
    case VUISolutionStatus::EXPECTING_SPEECH:
      return "Expecting Speech";
    case VUISolutionStatus::PLAYING_MUSIC:
      return "Playing Music";
    case VUISolutionStatus::INVALID:
      return "INVALID";
  }
  return "Unknown State";
}

/* Method to convert Intent to String */
inline int VUISolutionStatusToIndex(VUISolutionStatus status) {
  switch (status) {
    case VUISolutionStatus::IDLE:
      return 0;
    case VUISolutionStatus::LISTENING:
      return 1;
    case VUISolutionStatus::BUSY:
      return 2;
    case VUISolutionStatus::SPEAKING:
      return 3;
    case VUISolutionStatus::EXPECTING_SPEECH:
      return 4;
    case VUISolutionStatus::PLAYING_MUSIC:
      return 5;
    case VUISolutionStatus::INVALID:
      return 6;
  }
  return -1;
}

class ClientObserver {
 public:
  virtual ~ClientObserver() = default;
  //Callback for playback state changed
  virtual void NewSolutionStatus(VoiceUIClientID client_id, VUISolutionStatus status) = 0;
  //Callback for keyword detected
  virtual void KeywordDetected(VoiceUIClientID client_id) = 0;
  //Callback for direction of arrival (doa)
  virtual void DirectionOfArrival(VoiceUIClientID client_id, int direction) = 0;
  //Callback for volume status changed
  virtual void VolumeStatus(VoiceUIClientID client_id, int volume) = 0;
  //Callback for mute status changed
  virtual void MuteStatus(VoiceUIClientID client_id, bool state) = 0;
  //Callback for enable/disable status changed
  virtual void ActiveStatus(VoiceUIClientID client_id, bool state) = 0;
  //Send Intent from Voice UI Client
  virtual void SendIntent(VoiceUIClientID client_id, IntentManagerEvent intent) = 0;
  virtual void SendIntent(VoiceUIClientID client_id, IntentManagerEvent intent, IntentManagerEventExtended intent_ext) = 0;
  //Request Client Restart
  virtual void RequestRestartClient(VoiceUIClientID client_id) = 0;
};

}  // namespace voiceUIClient
}  // namespace voiceUIFramework

#endif /* VOICEUIFRAMEWORK_VOICEUICLIENT_INCLUDE_VOICEUICLIENT_CLIENTOBSERVER_H_ */
