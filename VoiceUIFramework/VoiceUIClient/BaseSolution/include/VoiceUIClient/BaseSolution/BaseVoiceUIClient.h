/* Copyright (c) 2018, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    BaseVoiceUIClient.h
 *  @brief   Base client for Voice UI Client implementation
 *
 *  DESCRIPTION
 *    Base class for both Modular and Full solution Client implementation
 ***************************************************************/

#ifndef VOICEUIFRAMEWORK_VOICEUICLIENT_INCLUDE_BASEVOICEUICLIENT_H_
#define VOICEUIFRAMEWORK_VOICEUICLIENT_INCLUDE_BASEVOICEUICLIENT_H_

#include <iostream>
#include <memory>
#include <string>
#include <thread>

#include <VoiceUIClient/BaseSolution/BaseClientManager.h>
#include <VoiceUIClient/BaseSolution/ClientObserver.h>
#include <Wakeword/WakeWordInterface.h>

namespace voiceUIFramework {
namespace voiceUIClient {

using namespace voiceUIFramework::wakeword;
using namespace voiceUIFramework::voiceUIUtils;

class BaseVoiceUIClient {
 public:
  BaseVoiceUIClient(
      std::shared_ptr<voiceUIFramework::voiceUIClient::ClientObserver> client_observer);
  virtual ~BaseVoiceUIClient();
  //Runs Voice UI Client
  virtual bool Start();
  //Configures Voice UI Solution before start it.
  virtual bool Configure() = 0;
  //Stops Voice UI Client
  virtual bool Stop();
  //Check if Client is active
  virtual bool IsActive();
  //Enable/Disable Client
  virtual bool SetActive(bool status);
  //Checks if Client is MUTED
  virtual bool IsMute();
  //Mute/Unmute Client
  virtual bool SetMute(bool status);
  //Receives a Intent from VoiceUIManager
  virtual void IntentReceived(IntentManagerEvent intent) = 0;  //TODO: define events/command types
  virtual void IntentReceived(IntentManagerEvent intent, IntentManagerEventExtended intent_ext) = 0;
  //Shutdown Client
  virtual void Shutdown();
  //Gets current ClientID from client
  virtual VoiceUIClientID GetClientID();
  //Receives AppMessage From VoiceUIManager
  virtual void AppMessageReceived(AppMessage app_message);

 protected:
  //reference for ClientManager
  std::shared_ptr<BaseClientManager> base_client_manager_;
  //Thread to execute client solution
  std::thread client_thread_;
};

}  // namespace voiceUIClient
}  // namespace voiceUIFramework

#endif /* VOICEUIFRAMEWORK_VOICEUICLIENT_INCLUDE_BASEVOICEUICLIENT_H_ */
