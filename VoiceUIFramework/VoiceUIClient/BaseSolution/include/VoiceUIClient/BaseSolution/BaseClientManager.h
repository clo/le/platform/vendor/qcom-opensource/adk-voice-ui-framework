/* Copyright (c) 2018, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    BaseClientManager.h
 *  @brief   Base Client Manager for Voice UI Client Manager implementation
 *
 *  DESCRIPTION
 *    Base class for both Modular and Full solution Client Manager implementation
 ***************************************************************/

#ifndef VOICEUIFRAMEWORK_VOICEUICLIENT_INCLUDE_BASECLIENTMANAGER_H_
#define VOICEUIFRAMEWORK_VOICEUICLIENT_INCLUDE_BASECLIENTMANAGER_H_

#include <syslog.h>
#include <iostream>
#include <memory>
#include <mutex>
#include <string>
#include <thread>

#include <AudioRecorder/BaseAudioRecorder.h>
#include <VoiceUIClient/BaseSolution/ClientObserver.h>
#include <Wakeword/QSTHWClient.h>
#include <Wakeword/WakewordDummyClient.h>

namespace voiceUIFramework {
namespace voiceUIClient {

using namespace voiceUIFramework::wakeword;
using namespace voiceUIFramework::audioRecorder;
using namespace voiceUIFramework::voiceUIUtils;

class BaseClientManager {
 public:
  BaseClientManager(
      VoiceUIClientID client_id,
      bool wake_word_embedded,
      std::shared_ptr<voiceUIFramework::voiceUIClient::ClientObserver> client_observer);

  virtual ~BaseClientManager();

  //Validates credentials from Voice UI Client
  virtual bool ValidateCredentials() = 0;
  //initializes Voice UI Client and SDK
  virtual bool Initialize() = 0;
  //Runs Voice UI Client
  virtual bool StartClient() = 0;
  //Stops Voice UI Client
  virtual bool StopClient() = 0;
  //Receives a Intent from VoiceUIManager
  virtual void IntentReceived(IntentManagerEvent intent) = 0;
  virtual void IntentReceived(IntentManagerEvent intent, IntentManagerEventExtended intent_ext) = 0;
  //Shutdown Client
  virtual bool Shutdown() = 0;
  //Check if Client is active
  virtual bool IsActive();
  //Enable/Disable Client
  virtual bool SetActive(bool state);
  //Checks if Client is MUTED
  virtual bool IsMute();
  //Mute/Unmute Client
  virtual bool SetMute(bool state);
  //Sends Intent to VoiceUIManager to be broadcasted to IntentManager
  virtual bool SendIntent(IntentManagerEvent intent);
  virtual bool SendIntent(IntentManagerEvent intent, IntentManagerEventExtended intent_ext);
  //Receives AppMessage from VoiceUIManager
  virtual void AppMessageReceived(AppMessage app_message) = 0;
  //Gets current ClientID from client
  VoiceUIClientID GetClientID();
  //Set AudioRecorder reference
  bool SetAudioRecorder(std::shared_ptr<AudioRecorderInterface> audio_recorder);

 protected:
  //Checks if Client has wake word engine embedded;
  bool HasWakeWordEngine();

  //wake word engine embedded
  bool wake_word_embedded_;
  //Audio recorder reference
  std::shared_ptr<AudioRecorderInterface> audio_recorder_;
  //wake word engine reference
  std::shared_ptr<WakeWordInterface> wake_word_;
  //Client Observer reference for CallBacks to Voice UI Manager
  std::shared_ptr<ClientObserver> client_observer_;
  //Current Client ID
  VoiceUIClientID client_id_;
  //Activate/Deactivate state
  bool active_;
  //Mute/Unmute state
  bool mute_;
  //Mutex to handle multiple clients(multithreading) callbacks.
  std::mutex mu;
};

}  // namespace voiceUIClient
}  // namespace voiceUIFramework

#endif /* VOICEUIFRAMEWORK_VOICEUICLIENT_INCLUDE_BASECLIENTMANAGER_H_ */
