/* Copyright (c) 2018, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    FullClientManager.h
 *  @brief   Full Voice UI Client Manager implementation
 *
 *  DESCRIPTION
 *    Handles Full Voice UI Client Manager implementation
 ***************************************************************/

#ifndef VOICEUIFRAMEWORK_VOICEUICLIENT_FULLSOLUTION_INCLUDE_VOICEUICLIENT_FULLSOLUTION_FULLCLIENTMANAGER_H_
#define VOICEUIFRAMEWORK_VOICEUICLIENT_FULLSOLUTION_INCLUDE_VOICEUICLIENT_FULLSOLUTION_FULLCLIENTMANAGER_H_

#include <chrono>
#include <iostream>
#include <memory>
#include <string>

#include <VoiceUIClient/BaseSolution/BaseClientManager.h>

namespace voiceUIFramework {
namespace voiceUIClient {

using namespace voiceUIFramework::wakeword;
using namespace voiceUIFramework::audioRecorder;

class FullClientManager : public BaseClientManager {
 public:
  FullClientManager(
      VoiceUIClientID client_id,
      bool wake_word_embedded,
      std::shared_ptr<voiceUIFramework::voiceUIClient::ClientObserver> client_observer);

  virtual ~FullClientManager();

  /* BaseVoiceUIClient class */
  //Validates credentials from Voice UI Client
  virtual bool ValidateCredentials();
  //Initializes Voice UI Solution
  virtual bool Initialize();  //going to be implemented by Provider manager Class
  //Runs Voice UI Client
  virtual bool StartClient();
  //Stops Voice UI Client
  virtual bool StopClient();
  //Receive Intents from Voice UI IntentManager
  virtual void IntentReceived(IntentManagerEvent intent);
  virtual void IntentReceived(IntentManagerEvent intent, IntentManagerEventExtended intent_ext);
  //Receive AppMessages from Voice UI manager
  virtual void AppMessageReceived(AppMessage app_message);
  //Shutdown Client
  virtual bool Shutdown();

 protected:
};

}  // namespace voiceUIClient
}  // namespace voiceUIFramework

#endif /* VOICEUIFRAMEWORK_VOICEUICLIENT_FULLSOLUTION_INCLUDE_VOICEUICLIENT_FULLSOLUTION_FULLCLIENTMANAGER_H_ */
