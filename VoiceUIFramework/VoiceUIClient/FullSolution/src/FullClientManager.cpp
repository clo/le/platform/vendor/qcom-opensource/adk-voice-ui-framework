/* Copyright (c) 2018-2020, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    FullClientManager.cpp
 *  @brief   Base Full Client Manager for Voice UI Client Manager implementation
 *
 *  DESCRIPTION
 *    Base class for Full solution Client Manager implementation
 ***************************************************************/

#include <VoiceUIClient/FullSolution/FullClientManager.h>

namespace voiceUIFramework {
namespace voiceUIClient {

FullClientManager::FullClientManager(
    VoiceUIClientID client_id,
    bool wake_word_embedded,
    std::shared_ptr<voiceUIFramework::voiceUIClient::ClientObserver> client_observer)
    : BaseClientManager(client_id, wake_word_embedded, client_observer) {
  syslog(LOG_INFO, "FullClientManager FullClientManager called for %s", ClientIDToString(client_id).c_str());
}

FullClientManager::~FullClientManager() {
  syslog(LOG_INFO, "---FullClientManager::~FullClientManager");
}

bool FullClientManager::ValidateCredentials() {
  return true;
}

bool FullClientManager::Initialize() {
  syslog(LOG_INFO, "FullClientManager::Initialize() - Wake_word_embedded_ = %d", wake_word_embedded_);
  if (!wake_word_embedded_) {
    // Todo: Implement observer
    if (wake_word_) {
      if (wake_word_->AddObserver(client_id_, nullptr) != 0) {
        syslog(LOG_ERR, "FullClientManager::Initialize() - Wakeword fail to initialize");
        return false;
      }
    }
    //Create Audio recorder for Client configure to recording On Demand only
    audio_recorder_ = std::make_shared<BaseAudioRecorder>(wake_word_, false);
  } else {
    //Create Audio recorder for Client configure to AlwaysON - Wakeword Engine is embedded on Client.
    audio_recorder_ = std::make_shared<BaseAudioRecorder>(nullptr, true);
  }

  return true;
}

bool FullClientManager::StartClient() {
  return true;
}

bool FullClientManager::StopClient() {
  return true;
}

void FullClientManager::IntentReceived(IntentManagerEvent intent) {
  syslog(LOG_INFO, "FullClientManager::IntentReceived - intent Received: %s", IntentManagerEventToString(intent).c_str());
}
void FullClientManager::IntentReceived(IntentManagerEvent intent, IntentManagerEventExtended intent_ext) {
  syslog(LOG_INFO, "FullClientManager::IntentReceivedExtended - intent Received: %s", IntentManagerEventToString(intent).c_str());
}

void FullClientManager::AppMessageReceived(AppMessage app_message) {
  syslog(LOG_DEBUG, "FullClientManager::AppMessageReceived() = %s", AppMessageToString(app_message).c_str());
  //TODO Implement handling for INTERNET UP and DOWN
}
bool FullClientManager::Shutdown() {
  syslog(LOG_INFO, "FullClientManager::Shutdown()");

  //AudioRecorder
  if (audio_recorder_)
    audio_recorder_->Shutdown();

  syslog(LOG_INFO, "FullClientManager::Shutdown() - DONE");
  return true;
}

}  // namespace voiceUIClient
}  // namespace voiceUIFramework
