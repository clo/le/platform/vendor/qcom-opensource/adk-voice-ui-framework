/* Copyright (c) 2018, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    IntentManagerInterface.h
 *  @brief   Intent manager Interface
 *
 *  DESCRIPTION
 *    Provides a interface for Intent Manager or any other component
 *    that would like to send/receive events/messages with LE platform
 ***************************************************************/

#ifndef VOICEUIFRAMEWORK_INTENTMANAGER_INCLUDE_INTENTMANAGER_INTENTMANAGERINTERFACE_H_
#define VOICEUIFRAMEWORK_INTENTMANAGER_INCLUDE_INTENTMANAGER_INTENTMANAGERINTERFACE_H_

#include <IntentManager/IntentManagerEvent.h>

namespace voiceUIFramework {
namespace voiceUIIntentManager {

class IntentManagerInterface {
 public:
  virtual ~IntentManagerInterface() = default;

  // Sends message signal to adk IPC Bus
  virtual int SendIntent(IntentManagerEvent intent) = 0;
  virtual int SendIntent(IntentManagerEvent intent, IntentManagerEventExtended intent_ext) = 0;

  // Configures IntentManager
  virtual int Configure() = 0;
  // Starts IntentManager
  virtual int Start() = 0;
  // Stops IntentManager
  virtual int Stop() = 0;
  // Shutdown IntentManager
  virtual void Shutdown() = 0;
};

} /*  namespace voiceUIIntentManager */
} /*  namespace voiceUIFramework */

#endif /* VOICEUIFRAMEWORK_INTENTMANAGER_INCLUDE_INTENTMANAGER_INTENTMANAGERINTERFACE_H_ */
