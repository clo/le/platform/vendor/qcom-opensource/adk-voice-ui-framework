/* Copyright (c) 2018, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    IntentManagerEvent.h
 *  @brief   brief description
 *
 *  DESCRIPTION
 *    description
 ***************************************************************/

#ifndef VOICEUIFRAMEWORK_VOICEUIMANAGER_INCLUDE_VOICEUIMANAGER_INTENTMANAGEREVENT_H_
#define VOICEUIFRAMEWORK_VOICEUIMANAGER_INCLUDE_VOICEUIMANAGER_INTENTMANAGEREVENT_H_

#include <string>

namespace voiceUIFramework {
namespace voiceUIIntentManager {

// Intent Nomenculature
// <ACTION>_<DOMAIN/CATEGORY_<EXTENSION>>_<COMMAND/EVENT>
// ACTIONS : Set, Increase , Decrease, UnSet
// DOMAIN/CATEGORY : light, thermostat, volume, mute
// EXTENSON : STATE,ABSOLUTE (qualifier for the domain/command)

enum class IntentManagerEvent {
  VOL_UP_EVENT,
  VOL_DOWN_EVENT,
  VOL_UPDATED_EVENT,
  MUTE_UPDATED_EVENT,
  MUTE_EVENT,
  MIC_MUTE_EVENT,
  MIC_MUTE_UPDATED_EVENT,
  TAP_BUTTON_COMMAND,
  // For DEMO Integration
  TAP_BUTTON_COMMAND_AVS,
  TAP_BUTTON_COMMAND_QCASR,
  LED_PATTERN_IDLE,
  LED_PATTERN_LISTENING,
  LED_PATTERN_THINKING,
  LED_PATTERN_SPEAKING,
  LED_PATTERN_SPEECH_DONE,
  LED_PATTERN_ONBOARDING,
  DIRECTION_OF_ARRIVAL,
  LED_PATTERN_ALERT,
  LED_PATTERN_NOTIFICATION,
  LED_PATTERN_DND,
  // Increase/Decrease System Volume
  // Increases/Decreases by "step" provided by the client
  // Or by 0.1 on a scale of 0 to 1
  INCREASE_VOLUME,
  DECREASE_VOLUME,
  // Mute Intents
  TOGGLE_MUTE_STATE,
  SET_MUTE_STATE,
  SET_VOLUME_ABSOLUTE,
  // Light Intents
  SET_LIGHT_STATE,
  // Control Device Intents
  // On,Off
  SET_CONTROL_DEVICE_STATE,
  // Temperature in degree
  SET_THERMOSTAT_STATE_ABSOLUTE,
  // Increase/ Decrease thermostat temperature by step (degrees)
  INCREASE_THERMOSTAT_TEMPERATURE,
  DECREASE_THERMOSTAT_TEMPERATURE,
  // Handle Calls
  HANDLE_CALL,
  // Security Intents
  SET_SECURITY_STATE,
  // Playback Control
  START_PLAYBACK,
  STOP_PLAYBACK,
  PAUSE_PLAYBACK,
  PLAY_NEXT_TRACK,
  PLAY_PREVIOUS_TRACK,
  // Play-Pause One Shot Control
  TOGGLE_PLAY_PAUSE,

  PLAYBACK_STARTED_EVENT,
  PLAYBACK_STOPPED_EVENT,
  PLAYBACK_PAUSED_EVENT,
  PLAYBACK_TRACK_CHANGED,

  SELECT_SOURCE,
  SOURCE_CHANGED_EVENT,

  // DISPLAY MANAGER NOTIFICATION
  DISPLAY_MANAGER_NOTIFICATION,

  // VoiceUI Database file updated
  DB_FILE_UPDATED,

  // Framework control
  ONBOARDING_ERROR,
  ONBOARDING_REQUESTED,
  ONBOARDING_SUCCESS,
  DELETE_CREDENTIAL,
  VOICEUI_STATUS,
  SET_DEFAULT_CLIENT,

  // Modular CLient
  SET_MODULAR_CLIENT_PROPS,

  // AVS
  SET_AVS_LOCALE,
  AVS_ONBOARDING_PROMPT,
  AUTHENTICATE_AVS,

  UNKNOWN_INTENT
};

// IntentManagerEventExtended is the datatype for packing complex Intents
// It wraps IntentManagerEvent along with the body
// for.e.g Intent - Set volume to 50 can be broken down to chunks
// event = SET_VOLUME_ABSOLUTE
// value = 50

struct target_state {
  std::string target;
  std::string state;
};

struct display_notification {
  std::string domain;
  std::string message;
  std::string display_time;
  std::string priority;
};

struct modular_client_props {
  std::string path;
  std::string language;
  std::string asr;
  std::string nlu;
  std::string tts;
};

// TODO(kshriniv):  Redefine Extension using type-safe union or alternative
// std::variant is an option but, supported only in c++ 17

struct IntentManagerEventExtended {
  target_state ts;
  display_notification notification;
  modular_client_props modular_properties;
  unsigned int value_uint;
  double value_double_1;
  double value_double_2;
  bool bool_state_1;
  bool bool_state_2;
  std::string str_1;
  std::string str_2;
};

/* Method to convert Intent to String */
inline const std::string IntentManagerEventToString(IntentManagerEvent event) {
  switch (event) {
    case IntentManagerEvent::VOL_UP_EVENT:
      return "VOLUME UP EVENT";
    case IntentManagerEvent::VOL_DOWN_EVENT:
      return "VOLUME DOWN EVENT";
    case IntentManagerEvent::VOL_UPDATED_EVENT:
      return "VOLUME UPDATED EVENT";
    case IntentManagerEvent::MUTE_UPDATED_EVENT:
      return "MUTE UPDATED EVENT";
    case IntentManagerEvent::MUTE_EVENT:
      return "MUTE EVENT";
    case IntentManagerEvent::MIC_MUTE_EVENT:
      return "MIC MUTE EVENT";
    case IntentManagerEvent::TAP_BUTTON_COMMAND:
      return "TAP BUTTON COMMAND";
    case IntentManagerEvent::TAP_BUTTON_COMMAND_AVS:
      return "TAP BUTTON COMMAND AVS";
    case IntentManagerEvent::TAP_BUTTON_COMMAND_QCASR:
      return "TAP BUTTON COMMAND QCASR";
    case IntentManagerEvent::LED_PATTERN_IDLE:
      return "LED pattern IDLE";
    case IntentManagerEvent::LED_PATTERN_LISTENING:
      return "LED pattern LISTENING";
    case IntentManagerEvent::LED_PATTERN_THINKING:
      return "LED pattern THINKING";
    case IntentManagerEvent::LED_PATTERN_SPEAKING:
      return "LED pattern SPEAKING";
    case IntentManagerEvent::LED_PATTERN_SPEECH_DONE:
      return "LED PATTERN SPEECH DONE";
    case IntentManagerEvent::DIRECTION_OF_ARRIVAL:
      return "DIRECTION OF ARRIVAL";
    case IntentManagerEvent::LED_PATTERN_ALERT:
      return "LED PATTERN ALERT ON";
    case IntentManagerEvent::LED_PATTERN_NOTIFICATION:
      return "LED PATTERN NOTIFICATION ON";
    case IntentManagerEvent::LED_PATTERN_DND:
      return "LED PATTERN DND ON";
    case IntentManagerEvent::LED_PATTERN_ONBOARDING:
      return "LED pattern ONBOARDING";
    case IntentManagerEvent::SET_VOLUME_ABSOLUTE:
      return "SET VOLUME ABSOLUTE";
    case IntentManagerEvent::SET_LIGHT_STATE:
      return "SET LIGHT STATE";
    case IntentManagerEvent::SET_CONTROL_DEVICE_STATE:
      return "SET CONTROL DEVICE STATE";
    case IntentManagerEvent::SET_THERMOSTAT_STATE_ABSOLUTE:
      return "SET THERMOSTAT STATE ABSOLUTE";
    case IntentManagerEvent::INCREASE_THERMOSTAT_TEMPERATURE:
      return "INCREASE THERMOSTAT TEMPERATURE";
    case IntentManagerEvent::DECREASE_THERMOSTAT_TEMPERATURE:
      return "DECREASE THERMOSTAT TEMPERATURE";
    case IntentManagerEvent::HANDLE_CALL:
      return "HANDLE CALL";
    case IntentManagerEvent::SET_SECURITY_STATE:
      return "SET SECURITY STATE";
    case IntentManagerEvent::INCREASE_VOLUME:
      return "INCREASE VOLUME";
    case IntentManagerEvent::DECREASE_VOLUME:
      return "DECREASE VOLUME";
    case IntentManagerEvent::TOGGLE_MUTE_STATE:
      return "TOGGLE MUTE STATE";
    case IntentManagerEvent::SET_MUTE_STATE:
      return "SET MUTE STATE";
    case IntentManagerEvent::START_PLAYBACK:
      return "START PLAYBACK";
    case IntentManagerEvent::STOP_PLAYBACK:
      return "STOP PLAYBACK";
    case IntentManagerEvent::PAUSE_PLAYBACK:
      return "PAUSE PLAYBACK";
    case IntentManagerEvent::PLAY_NEXT_TRACK:
      return "PLAY NEXT TRACK";
    case IntentManagerEvent::PLAY_PREVIOUS_TRACK:
      return "PLAY PREVIOUS TRACK";
    case IntentManagerEvent::TOGGLE_PLAY_PAUSE:
      return "TOGGLE PLAY PAUSE";
    case IntentManagerEvent::SELECT_SOURCE:
      return "SELECT SOURCE";
    case IntentManagerEvent::PLAYBACK_STARTED_EVENT:
      return "PLAYBACK STARTED EVENT";
    case IntentManagerEvent::PLAYBACK_STOPPED_EVENT:
      return "PLAYBACK STOPPED EVENT";
    case IntentManagerEvent::PLAYBACK_PAUSED_EVENT:
      return "PLAYBACK PAUSED EVENT";
    case IntentManagerEvent::PLAYBACK_TRACK_CHANGED:
      return "PLAYBACK TRACK CHANGED";
    case IntentManagerEvent::SOURCE_CHANGED_EVENT:
      return "SOURCE CHANGED EVENT";
    case IntentManagerEvent::DISPLAY_MANAGER_NOTIFICATION:
      return "DISPLAY MANAGER NOTIFICATION";
    case IntentManagerEvent::DB_FILE_UPDATED:
      return "DB FILE UPDATED";
    case IntentManagerEvent::ONBOARDING_REQUESTED:
      return "ONBOARDING REQUESTED";
    case IntentManagerEvent::VOICEUI_STATUS:
      return "VOICEUI STATUS";
    case IntentManagerEvent::SET_DEFAULT_CLIENT:
      return "SET DEFAULT CLIENT";
    case IntentManagerEvent::SET_MODULAR_CLIENT_PROPS:
      return "SET MODULAR CLIENT PROPS";
    case IntentManagerEvent::ONBOARDING_ERROR:
      return "ONBOARDING ERROR";
    case IntentManagerEvent::ONBOARDING_SUCCESS:
      return "ONBOARDING SUCCESS";
    case IntentManagerEvent::SET_AVS_LOCALE:
      return "SET AVS LOCALE";
    case IntentManagerEvent::AVS_ONBOARDING_PROMPT:
      return "AVS ONBOARDING PROMPT";
    case IntentManagerEvent::AUTHENTICATE_AVS:
      return "AUTHENTICATE AVS";
    case IntentManagerEvent::DELETE_CREDENTIAL:
      return "DELETE_CREDENTIAL";
    case IntentManagerEvent::UNKNOWN_INTENT:
      return "Unknown Intent";
  }
  return "Invalid Intent";
}

}  // namespace voiceUIIntentManager
}  // namespace voiceUIFramework

#endif /* VOICEUIFRAMEWORK_VOICEUIMANAGER_INCLUDE_VOICEUIMANAGER_INTENTMANAGEREVENT_H_ */
