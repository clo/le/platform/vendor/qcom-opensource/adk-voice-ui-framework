/* Copyright (c) 2018, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    IPCDefines.h
 *  @brief   brief description
 *  Abstracts ADK specific Signal names
 *  DESCRIPTION
 *    description
 ***************************************************************/
#ifndef VOICEUIFRAMEWORK_INTENTMANAGER_INCLUDE_INTENTMANAGER_IPCDEFINES_H_
#define VOICEUIFRAMEWORK_INTENTMANAGER_INCLUDE_INTENTMANAGER_IPCDEFINES_H_

namespace voiceUIFramework {
namespace voiceUIIntentManager {

// TODO(kshriniv) : change to const char

// Namespaces
#define NAMESPACE_AUDIO_MANAGER "audio"
#define NAMESPACE_LED_MANAGER "Led"
#define NAMESPACE_VOICEUI "Voiceui"
#define NAMESPACE_BUTTON_MANAGER "Button"
#define NAMESPACE_CONNECITVITY_MANAGER "connectivity"
#define NAMESPACE_DISPLAY_MANAGER "display"

// VoiceUI message signals
#define SIGNAL_VOICEUI_EVENT "event"
#define VARIANT_NAME_MESSAGE "message"
#define SIGNAL_START_ONBOARD "start_onboarding"
#define SIGNAL_VOICEUI_STATUS "voiceui_status"
#define SIGNAL_SET_DEFAULT_CLIENT "set_default_client"
#define SIGNAL_SET_MODULAR_CLIENT_PROPS "set_modular_client_props"

// Button message Signals
#define SIGNAL_BUTTON_PRESSED "pressed"
#define VARIANT_NAME_BUTTON_ID "buttonid"
#define VARIANT_VALUE_BUTTON_VOLUP "button_VOLUP"
#define VARIANT_VALUE_BUTTON_VOLDOWN "button_VOLDOWN"
#define VARIANT_VALUE_BUTTON_VOLMUTE "button_VOLMUTE"
#define VARIANT_VALUE_BUTTON_MIC_MUTE "button_MIC_MUTE"
#define VARIANT_VALUE_BUTTON_TAP "button_TAP"
// For DEMO Integration
#define SIGNAL_BUTTON_TAP_AVS "VoiceUI_TAP_AVS"
#define SIGNAL_BUTTON_TAP_QCASR "VoiceUI_TAP_QCASR"
// For DEMO Integration

// Message Signals
// Volume
#define SIGNAL_AUDIO_VOLUME_UP "volume_up"
#define SIGNAL_AUDIO_VOLUME_DOWN "volume_down"
#define SIGNAL_AUDIO_VOLUME_SET "volume_set"
// Mute
#define SIGNAL_AUDIO_MUTE_TOGGLE "mute_toggle"
#define SIGNAL_AUDIO_MUTE_SET "mute_set"

// Audio manager playback control
#define SIGNAL_AUDIO_MUTE_TOGGLE "mute_toggle"
#define SIGNAL_AUDIO_MUTE_SET "mute_set"
#define SIGNAL_AUDIO_TRACK_NEXT "track_next"
#define SIGNAL_AUDIO_TRACK_PREVIOUS "track_prev"
#define SIGNAL_AUDIO_TRACK_PLAY "track_play"
#define SIGNAL_AUDIO_TRACK_PAUSE "track_pause"
#define SIGNAL_AUDIO_TRACK_PLAY_PAUSE_TOGGLE "track_play_pause_toggle"
#define SIGNAL_AUDIO_TRACK_STOP "track_stop"
#define SIGNAL_AUDIO_PROMPT_PLAY "prompt_play"

// others
#define SIGNAL_AUDIO_SOURCE_SELECT "source_select"
#define SIGNAL_NOTIFICATION "notification"
#define SIGNAL_DB_UPDATED "voiceui_db_updated"
#define SIGNAL_AVS_AUTHENTICATE "authenticate_avs"
#define SIGNAL_SET_AVS_LOCALE "set_avs_locale"
#define SIGNAL_ONBOARDING_ERROR "onboarding_error"
#define SIGNAL_ONBOARDING_SUCCESS "onboarding_success"

// Audio Manager Message Signals
#define SIGNAL_AUDIO_VOLUME_UPDATED "volume_updated"
#define SIGNAL_AUDIO_MUTE_UPDATED "mute_updated"
#define SIGNAL_AUDIO_SOURCE_CHANGED "source_changed"
// Signal Used By Audio manager to update playback status like Playing, Paused
// or Stopped
#define SIGNAL_AUDIO_PLAYER_STATUS_UPDATE "player_status_update"

// Connectivity Manager, Zigbee Message signals
#define SIGNAL_SET_ZIGBEE_LIGHT_STATE "set_zigbee_light_state"
#define SIGNAL_SET_ZIGBEE_DEVICE "set_zigbee_device"
#define SIGNAL_SET_ZIGBEE_THERMOSTAT "set_zigbee_thermostat"
#define SIGNAL_INC_ZIGBEE_THERMOSTAT_TEMP "inc_zigbee_thermostat_temp"
#define SIGNAL_DEC_ZIGBEE_THERMOSTAT_TEMP "dec_zigbee_thermostat_temp"
#define SIGNAL_SET_ZIGBEE_SECURITY "set_zigbee_security"

// Connectivity Call signal
#define SIGNAL_HANDLE_CALL "handle_call"

// LED Message Signals
#define SIGNAL_LED_START_PATTERN "start_pattern"
#define SIGNAL_LED_STATUS_IDLE "VoiceUI_Status_IDLE"
#define SIGNAL_LED_STATUS_LISTENING "VoiceUI_Status_LISTENING"
#define SIGNAL_LED_STATUS_SPEAKING "VoiceUI_Status_SPEAKING"

#define SIGNAL_UNKNOWN "unknown_signal"

// Audio Manager expects the volume in the range of 0 to 1
// Steps in floating point
// Volume Step value can be 0.1 as Local ASR has range from 0 to 1
#define VARIANT_VALUE_STEP_VOLUME 0.1

// Possible NAMEVALUES MAP
#define VARIANT_NAME_STEP "step"
#define VARIANT_NAME_VALUE "value"
#define VARIANT_NAME_TARGET "target"
#define VARIANT_NAME_TEMPERATURE "temperature"
#define VARIANT_NAME_STATE "state"
#define VARIANT_NAME_STATUS "status"
#define VARIANT_NAME_WAKEWORD_STATUS "wakewordstatus"
#define VARIANT_NAME_PATTERN "pattern"
#define VARIANT_NAME_GROUP "group"
#define VARIANT_NAME_MUTE "mute"
#define VARIANT_NAME_SOURCE_NAME "source-name"
#define VARIANT_NAME_VOLUME_LEVEL "volume"
#define VARIANT_NAME_KEY "key"
#define VARIANT_NAME_TYPE "type"
#define VARIANT_NAME_NAME "name"
#define VARIANT_NAME_CLIENT "client"
#define VARIANT_NAME_URL "url"
#define VARIANT_NAME_CODE "code"
#define VARIANT_NAME_ERROR "code"
#define VARIANT_NAME_REATTEMPT "reattempt"

// displayManager Notification
#define VARIANT_NAME_NOTIFICATION_DOMAIN "domain"
#define VARIANT_NAME_NOTIFICATION_MESSAGE "message"
#define VARIANT_NAME_NOTIFICATION_DISPLAY_TIME "display_time"
#define VARIANT_NAME_NOTIFICATION_PRIORITY "priority"

// modular client properties
#define VARIANT_NAME_MODULAR_CLIENT_PATH "path"
#define VARIANT_NAME_MODULAR_CLIENT_LANGUAGE "language"
#define VARIANT_NAME_MODULAR_CLIENT_ASR "asr"
#define VARIANT_NAME_MODULAR_CLIENT_NLU "nlu"
#define VARIANT_NAME_MODULAR_CLIENT_TTS "tts"

}  // namespace voiceUIIntentManager
}  // namespace voiceUIFramework

#endif  // VOICEUIFRAMEWORK_INTENTMANAGER_INCLUDE_INTENTMANAGER_IPCDEFINES_H_
