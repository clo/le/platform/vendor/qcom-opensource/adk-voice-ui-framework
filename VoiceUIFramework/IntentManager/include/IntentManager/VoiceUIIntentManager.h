/* Copyright (c) 2018, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    VoiceUIIntentManager.h
 *  @brief   Intent Manager Client for LE Platform
 *
 *  DESCRIPTION
 *    Intent manager client to send/receive events/messages from platform
 ***************************************************************/

#ifndef VOICEUIFRAMEWORK_INTENTMANAGER_INCLUDE_INTENTMANAGER_VOICEUIINTENTMANAGER_H_
#define VOICEUIFRAMEWORK_INTENTMANAGER_INCLUDE_INTENTMANAGER_VOICEUIINTENTMANAGER_H_

#include <syslog.h>
#include <iostream>
#include <memory>
#include <string>
#include <thread>

#include <IntentManager/IntentManagerEvent.h>
#include <IntentManager/IntentManagerInterface.h>
#include <IntentManager/IntentObserver.h>

namespace voiceUIFramework {
namespace voiceUIIntentManager {

class VoiceUIIntentManager : public IntentManagerInterface {
 public:
  VoiceUIIntentManager();
  ~VoiceUIIntentManager();

  /* IntentManagerInterface class */
  //Sends Message Signal to ADK IPC Bus
  virtual int SendIntent(IntentManagerEvent intent);
  // Overloaded Sends Message Signal with extension to ADK IPC Bus
  virtual int SendIntent(IntentManagerEvent intent, IntentManagerEventExtended intent_ext);

  // Configures IntentManager
  virtual int Configure();
  //Starts IntentManager
  virtual int Start();
  //Stops IntentManager
  virtual int Stop();
  //Shutdown IntentManager
  virtual void Shutdown();

  //Adds a IntentObserver to be notified about intents form ADK IPC BUS
  void AddObserver(std::shared_ptr<IntentObserver> intent_observer);

 protected:
  //status of IntentManager
  bool running_;
};

}  // namespace voiceUIIntentManager
}  // namespace voiceUIFramework

#endif /* VOICEUIFRAMEWORK_INTENTMANAGER_INCLUDE_INTENTMANAGER_VOICEUIINTENTMANAGER_H_ */
