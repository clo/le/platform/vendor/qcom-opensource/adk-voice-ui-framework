/* Copyright (c) 2018, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    VoiceUiMessageService.h
 *  @brief   brief description
 *
 *  DESCRIPTION
 *    description
 ***************************************************************/

#ifndef VOICEUIFRAMEWORK_INTENTMANAGER_INCLUDE_INTENTMANAGER_VOICEUIMESSAGESERVICE_H_
#define VOICEUIFRAMEWORK_INTENTMANAGER_INCLUDE_INTENTMANAGER_VOICEUIMESSAGESERVICE_H_

#include <syslog.h>
#include <iostream>
#include <map>
#include <memory>
#include <string>
#include <tuple>

#include <IntentManager/IPCDefines.h>
#include <IntentManager/IntentManagerEvent.h>
#include <IntentManager/IntentObserver.h>
#include <VoiceUIUtils/VoiceUIConfig.h>

#include <adk/message-service/adk-message-service.h>
#include <adk/wakelock.h>

namespace voiceUIFramework {
namespace voiceUIIntentManager {

// Used to store events to send to the ADK IPC bus (dbus signals)
//using AdkMessage = adk::ipc::Service::AdkMessageSignalPayload;

class VoiceUIMessageService {
 public:
  VoiceUIMessageService();
  virtual ~VoiceUIMessageService();

  // Needed by adk::ipc::Emitter
  typedef std::shared_ptr<VoiceUIMessageService> Ptr;

  // Translate Intent to AdkEvent and send it using SendAdkEvent()
  int SendIntent(IntentManagerEvent intent);
  int SendIntent(IntentManagerEvent intent, IntentManagerEventExtended intent_ext);

  // Adds a IntentObserver to be notified about intents form ADK IPC BUS
  void AddObserver(std::shared_ptr<IntentObserver> intent_observer);

  static constexpr unsigned int IpcSignalHash(const char *s, int off = 0) {
    return !s[off] ? 5381 : (IpcSignalHash(s, off + 1) * 33) ^ s[off];
  }
  bool Start();

  adk::msg::AdkMessageService message_service_;
 private:
  // Send events to ADK IPC BUS
  void SendAdkMessage(const adk::msg::AdkMessage msg);

  std::map<IntentManagerEvent, adk::msg::AdkMessage::AdkMsgCase> intent_to_adk_message_map_;


  // ADK IntentManager namespace for IPC message
  // const std::string voiceui_namespace_ = "voiceui-intent";

  void HandleVoiceUIIntent(adk::msg::AdkMessage &msg);
  void HandleOtherAdkMessageSignal(adk::msg::AdkMessage &msg);

  typedef void (voiceUIFramework::voiceUIIntentManager::VoiceUIMessageService::*msgSignalHandler_)(adk::msg::AdkMessage &msg);

  std::map<adk::msg::AdkMessage::AdkMsgCase, msgSignalHandler_> voice_ui_messages_;
  std::map<adk::msg::AdkMessage::AdkMsgCase, msgSignalHandler_> other_adk_messages_;

  //Map from ADK signal name to IntentManager Event
  std::map<adk::msg::AdkMessage::AdkMsgCase, IntentManagerEvent> adk_message_to_intent_map_;

  // Reference for VoiceUIManager to broadcast events received from ADK-IPC
  std::shared_ptr<IntentObserver> intent_observer_;
};
}  // namespace voiceUIIntentManager
}  // namespace voiceUIFramework

#endif /* VOICEUIFRAMEWORK_INTENTMANAGER_INCLUDE_INTENTMANAGER_VOICEUIMESSAGESERVICE_H_ */
