/* Copyright (c) 2018, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    IntentObserver.h
 *  @brief   Observer to listen for new intents from IntentManager class
 *
 *  DESCRIPTION
 *    Observer to listen for new intents received by Intent manager client.
 ***************************************************************/

#ifndef VOICEUIFRAMEWORK_INTENTMANAGER_INCLUDE_INTENTMANAGER_INTENTOBSERVER_H_
#define VOICEUIFRAMEWORK_INTENTMANAGER_INCLUDE_INTENTMANAGER_INTENTOBSERVER_H_

#include <IntentManager/IntentManagerEvent.h>
#include <VoiceUIUtils/GlobalValues.h>

using namespace voiceUIFramework::voiceUIUtils;

namespace voiceUIFramework {
namespace voiceUIIntentManager {

class IntentObserver {
 public:
  virtual ~IntentObserver() = default;

  // Receive intents received by Voice UI IntentManager
  virtual void IntentReceived(IntentManagerEvent &intent) = 0;
  virtual void IntentReceived(IntentManagerEvent &intent, IntentManagerEventExtended &intent_ext) = 0;
};

} /*  namespace voiceUIIntentManager */
} /*  namespace voiceUIFramework */

#endif /* VOICEUIFRAMEWORK_INTENTMANAGER_INCLUDE_INTENTMANAGER_INTENTOBSERVER_H_ */
