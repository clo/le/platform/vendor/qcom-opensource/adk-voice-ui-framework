/* Copyright (c) 2018, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    VoiceUIIntentManagerIPC.h
 *  @brief   brief description
 *
 *  DESCRIPTION
 *    Intent manager client to send/receive events/messages from IPC BUS from LE platform
 ***************************************************************/

#ifndef VOICEUIFRAMEWORK_INTENTMANAGER_INCLUDE_INTENTMANAGER_VOICEUIINTENTMANAGERIPC_H_
#define VOICEUIFRAMEWORK_INTENTMANAGER_INCLUDE_INTENTMANAGER_VOICEUIINTENTMANAGERIPC_H_

#include "VoiceUiMessageService.h"
#include <IntentManager/VoiceUIIntentManager.h>

namespace voiceUIFramework {
namespace voiceUIIntentManager {

class VoiceUIIntentManagerIPC : public VoiceUIIntentManager {
 public:
  VoiceUIIntentManagerIPC();
  ~VoiceUIIntentManagerIPC();

  /* IntentManagerInterface class */
  //Sends Message Signal to ADK IPC Bus
  virtual int SendIntent(IntentManagerEvent intent);
  virtual int SendIntent(IntentManagerEvent intent, IntentManagerEventExtended intent_ext);
  //Configures IntentManager
  virtual int Configure();
  //Shutdown IntentManager
  virtual void Shutdown();

  //Adds a IntentObserver to be notified about intents form ADK IPC BUS
  void AddObserver(std::shared_ptr<IntentObserver> intent_observer);

 protected:
  VoiceUIMessageService voiceui_message_service_;

};

}  // namespace voiceUIIntentManager
}  // namespace voiceUIFramework

#endif /* VOICEUIFRAMEWORK_INTENTMANAGER_INCLUDE_INTENTMANAGER_VOICEUIINTENTMANAGERIPC_H_ */
