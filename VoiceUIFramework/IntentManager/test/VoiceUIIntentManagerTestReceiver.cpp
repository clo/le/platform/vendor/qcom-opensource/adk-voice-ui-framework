/* Copyright (c) 2018-2019, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    VoiceUIIntentManagerTestReceiver.cpp
 *  @brief   brief description
 *
 *  DESCRIPTION
 *    description
 ***************************************************************/

#include <IntentManager/IPCDefines.h>
#include <IntentManager/IntentManagerEvent.h>
#include <IntentManager/VoiceUIIntentManagerIPC.h>
#include <IntentManager/VoiceUiMessageService.h>
#include <atomic>
#include <chrono>
#include <condition_variable>
#include <functional>
#include <memory>
#include <mutex>
#include <string>
#include <thread>
#include <utility>
#include <vector>
#include "adk/log.h"
#include "gtest/gtest.h"

using namespace voiceUIFramework::voiceUIIntentManager;

namespace {

class IntentObserverMock : public IntentObserver {
 public:
  IntentObserverMock() {
    is_intent_received = false;
  }

  void WaitForIntent() {
    if (!is_intent_received.load()) {
      std::unique_lock<std::mutex> lck(mtx);
      cv.wait(lck);
      is_intent_received = false;
    }
  }

  virtual void IntentReceived(IntentManagerEvent &intent) {
    is_intent_received = true;
    cv.notify_one();
    std::string intentStr = IntentManagerEventToString(intent);
    ADK_LOG_INFO("IntentObserverMock::IntentReceived - Intent: %s", intentStr.c_str());
    intent_recieved = intentStr;
  }

  virtual void IntentReceived(IntentManagerEvent &intent, IntentManagerEventExtended &intent_ext) {
    is_intent_received = true;
    cv.notify_one();
    std::string intentStr = IntentManagerEventToString(intent);
    ADK_LOG_INFO("IntentObserverMock::IntentReceivedExtended - Intent: %s", intentStr.c_str());
    intent_recieved = intentStr;
    intent_ext_recieved = intent_ext;
  }

  std::string intent_recieved;
  IntentManagerEventExtended intent_ext_recieved;

 private:
  std::mutex mtx;
  std::condition_variable cv;
  std::atomic<bool> is_intent_received{false};
};

class VoiceUIIntentManagerIPCExtended : public VoiceUIIntentManagerIPC {
 public:
  VoiceUIIntentManagerIPCExtended() {
    if (Configure())
      ADK_LOG_ERROR("Message SERVICES Init Failed\n");
  }

  void SendAdkMessage(const adk::msg::AdkMessage cmd) {
    ADK_LOG_INFO("ADK Message to be sent: %s", cmd.DebugString().c_str());
    voiceui_message_service_.message_service_.Send(cmd);
  }
};

class VoiceUIIntentManagerTest : public ::testing::Test {
 public:
 protected:
  virtual void SetUp() {
    intent_observer_ = std::make_shared<IntentObserverMock>();
  }

  virtual void TearDown() {
  }

  VoiceUIIntentManagerIPCExtended extended_ipc_;
  std::shared_ptr<IntentObserverMock> intent_observer_;
};

TEST_F(VoiceUIIntentManagerTest, MethodTestReceive) {
  extended_ipc_.AddObserver(intent_observer_);

  {
    adk::msg::AdkMessage message;
    auto message_case = message.mutable_voiceui_tap_avs();
    extended_ipc_.SendAdkMessage(message);
    intent_observer_->WaitForIntent();
    std::this_thread::sleep_for(std::chrono::seconds(1));
    ASSERT_EQ(strcmp("TAP BUTTON COMMAND AVS", intent_observer_->intent_recieved.c_str()), 0);
  }

  {
    adk::msg::AdkMessage message;
    auto message_case = message.mutable_voiceui_tap_qcasr();
    extended_ipc_.SendAdkMessage(message);
    intent_observer_->WaitForIntent();
    std::this_thread::sleep_for(std::chrono::seconds(1));
    ASSERT_EQ(strcmp("TAP BUTTON COMMAND QCASR", intent_observer_->intent_recieved.c_str()), 0);
  }

  {
    adk::msg::AdkMessage message;
    auto message_case = message.mutable_voiceui_start_onboarding();
    extended_ipc_.SendAdkMessage(message);
    intent_observer_->WaitForIntent();
    std::this_thread::sleep_for(std::chrono::seconds(1));
    ASSERT_EQ(strcmp("ONBOARDING REQUESTED", intent_observer_->intent_recieved.c_str()), 0);
  }

  {
    adk::msg::AdkMessage message;
    auto message_case = message.mutable_voiceui_delete_credential();
    extended_ipc_.SendAdkMessage(message);
    intent_observer_->WaitForIntent();
    std::this_thread::sleep_for(std::chrono::seconds(1));
    ASSERT_EQ(strcmp("DELETE_CREDENTIAL", intent_observer_->intent_recieved.c_str()), 0);
  }

  {
    adk::msg::AdkMessage message;
    auto message_case = message.mutable_audio_volume_updated();
    message_case->set_volume(0.5);
    extended_ipc_.SendAdkMessage(message);
    intent_observer_->WaitForIntent();
    std::this_thread::sleep_for(std::chrono::seconds(1));
    ASSERT_EQ(strcmp("VOLUME UPDATED EVENT", intent_observer_->intent_recieved.c_str()), 0);
    ASSERT_EQ(0.5, intent_observer_->intent_ext_recieved.value_double_1);
  }

  {
    adk::msg::AdkMessage message;
    auto message_case = message.mutable_audio_mute_updated();
    message_case->set_mute(true);
    extended_ipc_.SendAdkMessage(message);
    intent_observer_->WaitForIntent();
    std::this_thread::sleep_for(std::chrono::seconds(1));
    ASSERT_EQ(strcmp("MUTE UPDATED EVENT", intent_observer_->intent_recieved.c_str()), 0);
    ASSERT_EQ(true, intent_observer_->intent_ext_recieved.bool_state_1);
  }

  {
    adk::msg::AdkMessage message;
    auto message_case = message.mutable_voiceui_status();
    message_case->set_client("AVS");
    message_case->set_status(true);
    extended_ipc_.SendAdkMessage(message);
    intent_observer_->WaitForIntent();
    std::this_thread::sleep_for(std::chrono::seconds(1));
    ASSERT_EQ(strcmp("VOICEUI STATUS", intent_observer_->intent_recieved.c_str()), 0);
    ASSERT_EQ("AVS", intent_observer_->intent_ext_recieved.str_1);
    ASSERT_EQ(true, intent_observer_->intent_ext_recieved.bool_state_1);
  }

  {
    adk::msg::AdkMessage message;
    auto message_case = message.mutable_voiceui_status();
    message_case->set_client("CORTANA");
    message_case->set_status(false);
    extended_ipc_.SendAdkMessage(message);
    intent_observer_->WaitForIntent();
    std::this_thread::sleep_for(std::chrono::seconds(1));
    ASSERT_EQ(strcmp("VOICEUI STATUS", intent_observer_->intent_recieved.c_str()), 0);
    ASSERT_EQ("CORTANA", intent_observer_->intent_ext_recieved.str_1);
    ASSERT_EQ(false, intent_observer_->intent_ext_recieved.bool_state_1);
  }

  {
    adk::msg::AdkMessage message;
    auto message_case = message.mutable_voiceui_set_default_client();
    message_case->set_client("GVA");
    extended_ipc_.SendAdkMessage(message);
    intent_observer_->WaitForIntent();
    std::this_thread::sleep_for(std::chrono::seconds(1));
    ASSERT_EQ(strcmp("SET DEFAULT CLIENT", intent_observer_->intent_recieved.c_str()), 0);
    ASSERT_EQ("GVA", intent_observer_->intent_ext_recieved.str_1);
  }

  /*{
    adk::msg::AdkMessage message;
    auto message_case = message.mutable_voiceui_avs_locale();
    message_case->set_locale("");
    extended_ipc_.SendAdkMessage(message);
    intent_observer_->WaitForIntent();
    std::this_thread::sleep_for(std::chrono::seconds(1));
    ASSERT_EQ(strcmp("SET AVS LOCALE", intent_observer_->intent_recieved.c_str()), 0);
    ASSERT_EQ("", intent_observer_->intent_ext_recieved.str_1);
  }

  {
    adk::msg::AdkMessage message;
    auto message_case = message.mutable_voiceui_set_modular_client_props();
    message_case->set_path("");
    message_case->set_language("");
    message_case->set_asr("");
    message_case->set_nlu("");
    message_case->set_tts("");
    extended_ipc_.SendAdkMessage(message);
    intent_observer_->WaitForIntent();
    std::this_thread::sleep_for(std::chrono::seconds(1));
    ASSERT_EQ(strcmp("SET MODULAR CLIENT PROPS", intent_observer_->intent_recieved.c_str()), 0);
  }

  {
    adk::msg::AdkMessage message;
    auto message_case = message.mutable_audio_mic_mute_updated();
    message_case->set_mute(true);
    extended_ipc_.SendAdkMessage(message);
    intent_observer_->WaitForIntent();
    std::this_thread::sleep_for(std::chrono::seconds(1));
    ASSERT_EQ(strcmp("MIC MUTE UPDATED EVENT", intent_observer_->intent_recieved.c_str()), 0);
    ASSERT_EQ(true, intent_observer_->intent_ext_recieved.bool_state_1);
  }*/
}

}  // namespace
