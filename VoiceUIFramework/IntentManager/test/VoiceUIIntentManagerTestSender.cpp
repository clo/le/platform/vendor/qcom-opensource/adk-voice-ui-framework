/* Copyright (c) 2018-2019, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    VoiceUIIntentManagerTestSender.cpp
 *  @brief   brief description
 *
 *  DESCRIPTION
 *    description
 ***************************************************************/

#include <atomic>
#include <condition_variable>
#include <functional>
#include <memory>
#include <mutex>
#include <string>
#include <thread>
#include <utility>
#include <vector>
#include "IntentManager/IPCDefines.h"
#include "IntentManager/IntentManagerEvent.h"
#include "IntentManager/VoiceUIIntentManagerIPC.h"
#include "adk/log.h"
#include "gtest/gtest.h"

using namespace voiceUIFramework::voiceUIIntentManager;

class VoiceUIIntentManagerIPCExtended : public VoiceUIIntentManagerIPC {
 public:
  ~VoiceUIIntentManagerIPCExtended() {
    extended_ipc_ = NULL;
  }

  static VoiceUIIntentManagerIPCExtended *GetInstance() {
    if (!extended_ipc_)
      extended_ipc_ = new VoiceUIIntentManagerIPCExtended();

    return extended_ipc_;
  }

  void WaitForAdkMessage() {
    if (!ready.load()) {
      std::unique_lock<std::mutex> lck(mtx);
      cv.wait(lck);
      ready = false;
    }
  }

  void AdkMessageHandler(adk::msg::AdkMessage &command) {
    ADK_LOG_DEBUG("AdkMessage has been received with argument: %s", command.DebugString().c_str());
    ready = true;
    cv.notify_one();
    msg = command;
  }

  void SubscribeForMessage(adk::msg::AdkMessage::AdkMsgCase message_case) {
    ADK_LOG_INFO("VoiceUIIntentManagerIPCExtended::SubscribeForMessage");
    auto it = adk::msg::msg_name_to_oneof_id.begin();

    for (; it != adk::msg::msg_name_to_oneof_id.end(); it++) {
      if (it->second == message_case)
        break;
    }

    if (it == adk::msg::msg_name_to_oneof_id.end()) {
      ADK_LOG_ERROR("Not able to find Message Case provided %d\n", message_case);
      return;
    }

    voiceui_message_service_.message_service_.Subscribe(message_case,
        std::bind(&VoiceUIIntentManagerIPCExtended::AdkMessageHandler, this, std::placeholders::_1));
  }

  adk::msg::AdkMessage msg;

 private:
  std::mutex mtx;
  std::condition_variable cv;
  std::atomic<bool> ready{false};
  static VoiceUIIntentManagerIPCExtended *extended_ipc_;

  VoiceUIIntentManagerIPCExtended() {
    if (Configure())
      ADK_LOG_ERROR("Message SERVICES Init Failed\n");
  }
};

VoiceUIIntentManagerIPCExtended *VoiceUIIntentManagerIPCExtended::extended_ipc_ = NULL;

class VoiceUIIntentManagerTest : public ::testing::Test {
 public:
 protected:
  virtual void SetUp() {
  }

  virtual void TearDown() {
  }
};

TEST_F(VoiceUIIntentManagerTest, MethodTestSendLEDIntents) {
  VoiceUIIntentManagerIPCExtended *extended_ipc = extended_ipc->GetInstance();
  // LED Intents
  extended_ipc->SubscribeForMessage(adk::msg::AdkMessage::kVoiceuiStatusIdle);
  IntentManagerEvent intent = IntentManagerEvent::LED_PATTERN_IDLE;
  extended_ipc->SendIntent(intent);
  extended_ipc->WaitForAdkMessage();
  ASSERT_EQ(AdkMessage::kVoiceuiStatusIdle, extended_ipc->msg.adk_msg_case());

  extended_ipc->SubscribeForMessage(adk::msg::AdkMessage::kVoiceuiStatusListening);
  intent = IntentManagerEvent::LED_PATTERN_LISTENING;
  extended_ipc->SendIntent(intent);
  extended_ipc->WaitForAdkMessage();
  ASSERT_EQ(AdkMessage::kVoiceuiStatusListening, extended_ipc->msg.adk_msg_case());

  extended_ipc->SubscribeForMessage(adk::msg::AdkMessage::kVoiceuiStatusThinking);
  intent = IntentManagerEvent::LED_PATTERN_THINKING;
  extended_ipc->SendIntent(intent);
  extended_ipc->WaitForAdkMessage();
  ASSERT_EQ(AdkMessage::kVoiceuiStatusThinking, extended_ipc->msg.adk_msg_case());

  extended_ipc->SubscribeForMessage(adk::msg::AdkMessage::kVoiceuiStatusSpeaking);
  intent = IntentManagerEvent::LED_PATTERN_SPEAKING;
  extended_ipc->SendIntent(intent);
  extended_ipc->WaitForAdkMessage();
  ASSERT_EQ(AdkMessage::kVoiceuiStatusSpeaking, extended_ipc->msg.adk_msg_case());

  extended_ipc->SubscribeForMessage(adk::msg::AdkMessage::kVoiceuiStatusSpeechDone);
  intent = IntentManagerEvent::LED_PATTERN_SPEECH_DONE;
  extended_ipc->SendIntent(intent);
  extended_ipc->WaitForAdkMessage();
  ASSERT_EQ(AdkMessage::kVoiceuiStatusSpeechDone, extended_ipc->msg.adk_msg_case());

  /*extended_ipc->SubscribeForMessage(adk::msg::AdkMessage::kVoiceuiStatusIdle);
  intent = IntentManagerEvent::LED_PATTERN_ONBOARDING;
  extended_ipc->SendIntent(intent);
  extended_ipc->WaitForAdkMessage();
  ASSERT_EQ(AdkMessage::kVoiceuiStatusIdle, extended_ipc->msg.adk_msg_case());*/

  extended_ipc->SubscribeForMessage(adk::msg::AdkMessage::kVoiceuiStatusAlert);
  intent = IntentManagerEvent::LED_PATTERN_ALERT;
  extended_ipc->SendIntent(intent);
  extended_ipc->WaitForAdkMessage();
  ASSERT_EQ(AdkMessage::kVoiceuiStatusAlert, extended_ipc->msg.adk_msg_case());
}

TEST_F(VoiceUIIntentManagerTest, MethodTestSendVolumeMuteIntents) {
  VoiceUIIntentManagerIPCExtended *extended_ipc = extended_ipc->GetInstance();
  // VolumeMute Intents
  extended_ipc->SubscribeForMessage(adk::msg::AdkMessage::kAudioVolumeUp);
  IntentManagerEvent intent = IntentManagerEvent::INCREASE_VOLUME;
  IntentManagerEventExtended intent_ext;
  intent_ext.value_double_2 = 0.2;
  extended_ipc->SendIntent(intent, intent_ext);
  extended_ipc->WaitForAdkMessage();
  ASSERT_EQ(AdkMessage::kAudioVolumeUp, extended_ipc->msg.adk_msg_case());
  ASSERT_EQ(0.2, extended_ipc->msg.audio_volume_up().step());

  extended_ipc->SubscribeForMessage(adk::msg::AdkMessage::kAudioVolumeDown);
  intent = IntentManagerEvent::DECREASE_VOLUME;
  intent_ext.value_double_2 = 0.2;
  extended_ipc->SendIntent(intent, intent_ext);
  extended_ipc->WaitForAdkMessage();
  ASSERT_EQ(AdkMessage::kAudioVolumeDown, extended_ipc->msg.adk_msg_case());
  ASSERT_EQ(0.2, extended_ipc->msg.audio_volume_down().step());

  extended_ipc->SubscribeForMessage(adk::msg::AdkMessage::kAudioVolumeSet);
  intent = IntentManagerEvent::SET_VOLUME_ABSOLUTE;
  intent_ext.value_double_1 = 0.45;
  extended_ipc->SendIntent(intent, intent_ext);
  extended_ipc->WaitForAdkMessage();
  ASSERT_EQ(AdkMessage::kAudioVolumeSet, extended_ipc->msg.adk_msg_case());
  ASSERT_EQ(0.45, extended_ipc->msg.audio_volume_set().value());

  extended_ipc->SubscribeForMessage(adk::msg::AdkMessage::kAudioMuteToggle);
  intent = IntentManagerEvent::TOGGLE_MUTE_STATE;
  extended_ipc->SendIntent(intent);
  extended_ipc->WaitForAdkMessage();
  ASSERT_EQ(AdkMessage::kAudioMuteToggle, extended_ipc->msg.adk_msg_case());

  extended_ipc->SubscribeForMessage(adk::msg::AdkMessage::kAudioMuteSet);
  intent = IntentManagerEvent::SET_MUTE_STATE;
  intent_ext.bool_state_1 = true;
  extended_ipc->SendIntent(intent, intent_ext);
  extended_ipc->WaitForAdkMessage();
  ASSERT_EQ(AdkMessage::kAudioMuteSet, extended_ipc->msg.adk_msg_case());
  ASSERT_EQ(true, extended_ipc->msg.audio_mute_set().mute());

  intent_ext.bool_state_1 = false;
  extended_ipc->SendIntent(intent, intent_ext);
  extended_ipc->WaitForAdkMessage();
  ASSERT_EQ(AdkMessage::kAudioMuteSet, extended_ipc->msg.adk_msg_case());
  ASSERT_EQ(false, extended_ipc->msg.audio_mute_set().mute());
}

TEST_F(VoiceUIIntentManagerTest, MethodTestSendPlaybackControlIntents) {
  VoiceUIIntentManagerIPCExtended *extended_ipc = extended_ipc->GetInstance();
  // PlaybackControl Intents
  extended_ipc->SubscribeForMessage(adk::msg::AdkMessage::kAudioTrackPlay);
  IntentManagerEvent intent = IntentManagerEvent::START_PLAYBACK;
  extended_ipc->SendIntent(intent);
  extended_ipc->WaitForAdkMessage();
  ASSERT_EQ(AdkMessage::kAudioTrackPlay, extended_ipc->msg.adk_msg_case());

  extended_ipc->SubscribeForMessage(adk::msg::AdkMessage::kAudioTrackStop);
  intent = IntentManagerEvent::STOP_PLAYBACK;
  extended_ipc->SendIntent(intent);
  extended_ipc->WaitForAdkMessage();
  ASSERT_EQ(AdkMessage::kAudioTrackStop, extended_ipc->msg.adk_msg_case());

  extended_ipc->SubscribeForMessage(adk::msg::AdkMessage::kAudioTrackNext);
  intent = IntentManagerEvent::PLAY_NEXT_TRACK;
  extended_ipc->SendIntent(intent);
  extended_ipc->WaitForAdkMessage();
  ASSERT_EQ(AdkMessage::kAudioTrackNext, extended_ipc->msg.adk_msg_case());

  extended_ipc->SubscribeForMessage(adk::msg::AdkMessage::kAudioTrackPrevious);
  intent = IntentManagerEvent::PLAY_PREVIOUS_TRACK;
  extended_ipc->SendIntent(intent);
  extended_ipc->WaitForAdkMessage();
  ASSERT_EQ(AdkMessage::kAudioTrackPrevious, extended_ipc->msg.adk_msg_case());

  extended_ipc->SubscribeForMessage(adk::msg::AdkMessage::kAudioTrackPause);
  intent = IntentManagerEvent::PAUSE_PLAYBACK;
  extended_ipc->SendIntent(intent);
  extended_ipc->WaitForAdkMessage();
  ASSERT_EQ(AdkMessage::kAudioTrackPause, extended_ipc->msg.adk_msg_case());

  extended_ipc->SubscribeForMessage(adk::msg::AdkMessage::kAudioTrackPlayPauseToggle);
  intent = IntentManagerEvent::TOGGLE_PLAY_PAUSE;
  extended_ipc->SendIntent(intent);
  extended_ipc->WaitForAdkMessage();
  ASSERT_EQ(AdkMessage::kAudioTrackPlayPauseToggle, extended_ipc->msg.adk_msg_case());
}

TEST_F(VoiceUIIntentManagerTest, MethodTestSendZigbeeIntents) {
  VoiceUIIntentManagerIPCExtended *extended_ipc = extended_ipc->GetInstance();
  // Zigbee Intents
  extended_ipc->SubscribeForMessage(adk::msg::AdkMessage::kConnectivityZigbeeSetGroupOnOffState);
  IntentManagerEvent intent = IntentManagerEvent::SET_LIGHT_STATE;
  IntentManagerEventExtended intent_ext;
  intent_ext.ts.target = "kitchen";
  intent_ext.ts.state = "off";
  extended_ipc->SendIntent(intent, intent_ext);
  extended_ipc->WaitForAdkMessage();
  ASSERT_EQ(AdkMessage::kConnectivityZigbeeSetGroupOnOffState, extended_ipc->msg.adk_msg_case());
  ASSERT_EQ(strcmp("kitchen", extended_ipc->msg.connectivity_zigbee_set_group_on_off_state().group().c_str()), 0);
  ASSERT_EQ(false, extended_ipc->msg.connectivity_zigbee_set_group_on_off_state().state());

  extended_ipc->SubscribeForMessage(adk::msg::AdkMessage::kConnectivityZigbeeSetDeviceOnOffState);
  intent = IntentManagerEvent::SET_CONTROL_DEVICE_STATE;
  intent_ext.ts.target = "thermostat";
  intent_ext.ts.state = "on";
  extended_ipc->SendIntent(intent, intent_ext);
  extended_ipc->WaitForAdkMessage();
  ASSERT_EQ(AdkMessage::kConnectivityZigbeeSetDeviceOnOffState, extended_ipc->msg.adk_msg_case());
  ASSERT_EQ(strcmp("thermostat", extended_ipc->msg.connectivity_zigbee_set_device_on_off_state().name().c_str()), 0);
  ASSERT_EQ(true, extended_ipc->msg.connectivity_zigbee_set_device_on_off_state().state());

  /*extended_ipc->SubscribeForMessage(adk::msg::AdkMessage::kConnectivityZigbeeSetDeviceTemperature);
  IntentManagerEvent intent = IntentManagerEvent::SET_THERMOSTAT_STATE_ABSOLUTE;
  IntentManagerEventExtended intent_ext;
  intent_ext.ts.target = "thermostat";
  intent_ext.ts.state = "on";
  extended_ipc->SendIntent(intent, intent_ext);
  extended_ipc->WaitForAdkMessage();
  ASSERT_EQ(AdkMessage::kConnectivityZigbeeSetDeviceTemperature, extended_ipc->msg.adk_msg_case());
  ASSERT_EQ(strcmp("thermostat", extended_ipc->msg.connectivity_zigbee_set_device_temperature().name().c_str()), 0);
  ASSERT_EQ(true, extended_ipc->msg.connectivity_zigbee_set_device_temperature().temperature());*/

  /*extended_ipc->SubscribeForMessage(adk::msg::AdkMessage::kConnectivityZigbeeIncreaseTemperature);
  IntentManagerEvent intent = IntentManagerEvent::INCREASE_THERMOSTAT_TEMPERATURE;
  IntentManagerEventExtended intent_ext;
  intent_ext.ts.target = "thermostat";
  intent_ext.ts.state = "on";
  extended_ipc->SendIntent(intent, intent_ext);
  extended_ipc->WaitForAdkMessage();
  ASSERT_EQ(AdkMessage::kConnectivityZigbeeIncreaseTemperature, extended_ipc->msg.adk_msg_case());
  ASSERT_EQ(strcmp("thermostat", extended_ipc->msg.connectivity_zigbee_set_device_on_off_state().has_name().c_str()), 0);
  ASSERT_EQ(true, extended_ipc->msg.connectivity_zigbee_set_device_on_off_state().has_state());*/

  /*extended_ipc->SubscribeForMessage(adk::msg::AdkMessage::kConnectivityZigbeeDecreaseTemperature);
  IntentManagerEvent intent = IntentManagerEvent::DECREASE_THERMOSTAT_TEMPERATURE;
  IntentManagerEventExtended intent_ext;
  intent_ext.ts.target = "thermostat";
  intent_ext.ts.state = "on";
  extended_ipc->SendIntent(intent, intent_ext);
  extended_ipc->WaitForAdkMessage();
  ASSERT_EQ(AdkMessage::kConnectivityZigbeeDecreaseTemperature, extended_ipc->msg.adk_msg_case());
  ASSERT_EQ(strcmp("thermostat", extended_ipc->msg.connectivity_zigbee_set_device_on_off_state().has_name().c_str()), 0);
  ASSERT_EQ(true, extended_ipc->msg.connectivity_zigbee_set_device_on_off_state().has_state());*/
}
