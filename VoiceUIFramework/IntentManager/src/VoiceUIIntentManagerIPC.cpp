/* Copyright (c) 2018, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    VoiceUIIntentManagerIPC.cpp
 *  @brief   Intent Manager Client for LE Platform
 *
 *  DESCRIPTION
 *    Intent manager client to send/receive events/messages from IPC BUS from LE platform
 ***************************************************************/

#include <IntentManager/VoiceUIIntentManagerIPC.h>

namespace voiceUIFramework {
namespace voiceUIIntentManager {

VoiceUIIntentManagerIPC::VoiceUIIntentManagerIPC() {
  syslog(LOG_INFO, "VoiceUIIntentManagerIPC::VoiceUIIntentManagerIPC - Built!");
}

VoiceUIIntentManagerIPC::~VoiceUIIntentManagerIPC() {
  syslog(LOG_INFO, "VoiceUIIntentManagerIPC::~VoiceUIIntentManagerIPC");
}

int VoiceUIIntentManagerIPC::SendIntent(IntentManagerEvent intent) {
  syslog(LOG_INFO, "VoiceUIIntentManager::SendIntent - Intent to be Sent: %s ", IntentManagerEventToString(intent).c_str());
  voiceui_message_service_.SendIntent(intent);

  return 0;
}

int VoiceUIIntentManagerIPC::SendIntent(IntentManagerEvent intent, IntentManagerEventExtended intent_ext) {
  syslog(LOG_INFO, "VoiceUIIntentManager::SendIntent(Extended) - Intent to be Sent: %s ", IntentManagerEventToString(intent).c_str());
  voiceui_message_service_.SendIntent(intent, intent_ext);

  return 0;
}

int VoiceUIIntentManagerIPC::Configure() {
  if (!voiceui_message_service_.Start()) {
    // Return 1 on failure ?
    return 1;
  }
  return 0;
}

void VoiceUIIntentManagerIPC::Shutdown() {
  syslog(LOG_INFO, "VoiceUIIntentManagerIPC::Shutdown() - Join ADK Connection Thread");
}

void VoiceUIIntentManagerIPC::AddObserver(std::shared_ptr<IntentObserver> intent_observer) {
  voiceui_message_service_.AddObserver(intent_observer);
}

}  // namespace voiceUIIntentManager
}  // namespace voiceUIFramework
