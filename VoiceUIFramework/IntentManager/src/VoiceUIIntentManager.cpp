/* Copyright (c) 2018, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    VoiceUIIntentManager.cpp
 *  @brief   Intent Manager Client for LE Platform
 *
 *  DESCRIPTION
 *    Intent manager client to send/receive events/messages from platform
 ***************************************************************/

#include <IntentManager/VoiceUIIntentManager.h>

namespace voiceUIFramework {
namespace voiceUIIntentManager {

VoiceUIIntentManager::VoiceUIIntentManager()
    : running_(false) {
  syslog(LOG_INFO, "VoiceUIIntentManager::VoiceUIIntentManager - Built!");
}

VoiceUIIntentManager::~VoiceUIIntentManager() {
  syslog(LOG_INFO, "VoiceUIIntentManager::~VoiceUIIntentManager");
}

int VoiceUIIntentManager::SendIntent(IntentManagerEvent intent) {
  syslog(LOG_INFO, "VoiceUIIntentManager::SendIntent - Intent to be Sent: %s ", IntentManagerEventToString(intent).c_str());
  return 0;
}

int VoiceUIIntentManager::SendIntent(IntentManagerEvent intent, IntentManagerEventExtended intent_ext) {
  syslog(LOG_INFO, "VoiceUIIntentManager::SendIntent(Extended) - Intent to be Sent: %s ", IntentManagerEventToString(intent).c_str());
  return 0;
}

int VoiceUIIntentManager::Configure() {
  syslog(LOG_INFO, "VoiceUIIntentManager::Configure()");
  return 0;
}

int VoiceUIIntentManager::Start() {
  syslog(LOG_INFO, "VoiceUIIntentManager::Start() - Not Supported");
  running_ = true;
  return 0;
}

int VoiceUIIntentManager::Stop() {
  syslog(LOG_INFO, "VoiceUIIntentManager::Stop() - Not Supported");
  running_ = false;
  return 0;
}

void VoiceUIIntentManager::Shutdown() {
  syslog(LOG_INFO, "VoiceUIIntentManager::Shutdown()");
}

void VoiceUIIntentManager::AddObserver(std::shared_ptr<IntentObserver> intent_observer) {
  syslog(LOG_INFO, "VoiceUIIntentManager::AddObserver()");
}

}  // namespace voiceUIIntentManager
}  // namespace voiceUIFramework
