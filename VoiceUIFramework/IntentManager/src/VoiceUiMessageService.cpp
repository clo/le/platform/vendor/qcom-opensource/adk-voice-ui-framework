/* Copyright (c) 2018, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    VoiceUiMessageService.cpp
 *  @brief   brief description
 *
 *  DESCRIPTION
 *    description
 ***************************************************************/

#include <IntentManager/VoiceUiMessageService.h>

namespace voiceUIFramework {
namespace voiceUIIntentManager {

VoiceUIMessageService::VoiceUIMessageService()
    : message_service_("voiceui_intent") {
  syslog(LOG_INFO, "IPCSender::IPCSender - IPCSender Built!");

  // Create MAP of Intents to IPC message signals

  // LED Intents
  intent_to_adk_message_map_[IntentManagerEvent::LED_PATTERN_IDLE] =
      AdkMessage::kVoiceuiStatusIdle;
  intent_to_adk_message_map_[IntentManagerEvent::LED_PATTERN_LISTENING] =
      AdkMessage::kVoiceuiStatusListening;
  intent_to_adk_message_map_[IntentManagerEvent::LED_PATTERN_THINKING] =
      AdkMessage::kVoiceuiStatusThinking;
  intent_to_adk_message_map_[IntentManagerEvent::LED_PATTERN_SPEAKING] =
      AdkMessage::kVoiceuiStatusSpeaking;
  intent_to_adk_message_map_[IntentManagerEvent::LED_PATTERN_SPEECH_DONE] =
      AdkMessage::kVoiceuiStatusSpeechDone;
  intent_to_adk_message_map_[IntentManagerEvent::LED_PATTERN_ONBOARDING] =
      AdkMessage::kVoiceuiStatusIdle;
  intent_to_adk_message_map_[IntentManagerEvent::LED_PATTERN_ALERT] =
      AdkMessage::kVoiceuiStatusAlert;
  intent_to_adk_message_map_[IntentManagerEvent::LED_PATTERN_NOTIFICATION] =
      AdkMessage::kVoiceuiNotification;
  intent_to_adk_message_map_[IntentManagerEvent::LED_PATTERN_DND] =
      AdkMessage::kVoiceuiStatusDoNotDisturb;

  //Direction Of Arrival
  intent_to_adk_message_map_[IntentManagerEvent::DIRECTION_OF_ARRIVAL] =
      AdkMessage::kVoiceuiStatusDirectionOfArrival;

  // Volume Intents
  intent_to_adk_message_map_[IntentManagerEvent::INCREASE_VOLUME] =
      AdkMessage::kAudioVolumeUp;
  intent_to_adk_message_map_[IntentManagerEvent::DECREASE_VOLUME] =
      AdkMessage::kAudioVolumeDown;
  intent_to_adk_message_map_[IntentManagerEvent::SET_VOLUME_ABSOLUTE] =
      AdkMessage::kAudioVolumeSet;

  // Mute Intents
  intent_to_adk_message_map_[IntentManagerEvent::TOGGLE_MUTE_STATE] =
      AdkMessage::kAudioMuteToggle;
  intent_to_adk_message_map_[IntentManagerEvent::SET_MUTE_STATE] =
      AdkMessage::kAudioMuteSet;

  // Zigbee Intents
  intent_to_adk_message_map_[IntentManagerEvent::SET_LIGHT_STATE] =
      AdkMessage::kConnectivityZigbeeSetGroupOnOffState;
  intent_to_adk_message_map_[IntentManagerEvent::SET_CONTROL_DEVICE_STATE] =
      AdkMessage::kConnectivityZigbeeSetDeviceOnOffState;
  intent_to_adk_message_map_[IntentManagerEvent::SET_THERMOSTAT_STATE_ABSOLUTE] =
      AdkMessage::kConnectivityZigbeeSetDeviceTemperature;
  intent_to_adk_message_map_
      [IntentManagerEvent::INCREASE_THERMOSTAT_TEMPERATURE] =
          AdkMessage::kConnectivityZigbeeIncreaseTemperature;
  intent_to_adk_message_map_
      [IntentManagerEvent::DECREASE_THERMOSTAT_TEMPERATURE] =
          AdkMessage::kConnectivityZigbeeDecreaseTemperature;
  intent_to_adk_message_map_[IntentManagerEvent::SET_SECURITY_STATE] =
      AdkMessage::kVoiceuiSetZigbeeSecurity;

  // Playback Control intents
  intent_to_adk_message_map_[IntentManagerEvent::START_PLAYBACK] =
      AdkMessage::kAudioTrackPlay;
  intent_to_adk_message_map_[IntentManagerEvent::STOP_PLAYBACK] =
      AdkMessage::kAudioTrackStop;
  intent_to_adk_message_map_[IntentManagerEvent::PLAY_NEXT_TRACK] =
      AdkMessage::kAudioTrackNext;
  intent_to_adk_message_map_[IntentManagerEvent::PLAY_PREVIOUS_TRACK] =
      AdkMessage::kAudioTrackPrevious;
  intent_to_adk_message_map_[IntentManagerEvent::PAUSE_PLAYBACK] =
      AdkMessage::kAudioTrackPause;
  intent_to_adk_message_map_[IntentManagerEvent::TOGGLE_PLAY_PAUSE] =
      AdkMessage::kAudioTrackPlayPauseToggle;

  // Source Select
  intent_to_adk_message_map_[IntentManagerEvent::SELECT_SOURCE] =
      AdkMessage::kAudioSourceSelect;

  // Display Manager Notification
  intent_to_adk_message_map_[IntentManagerEvent::DISPLAY_MANAGER_NOTIFICATION] =
      AdkMessage::kDisplayNotification;

  // Database file updated
  intent_to_adk_message_map_[IntentManagerEvent::DB_FILE_UPDATED] =
      AdkMessage::kVoiceuiDatabaseUpdated;

  // Authenticate AVS instructions
  intent_to_adk_message_map_[IntentManagerEvent::AUTHENTICATE_AVS] =
      AdkMessage::kVoiceuiAuthenticateAvs;

  // Report Onboarding Error
  intent_to_adk_message_map_[IntentManagerEvent::ONBOARDING_ERROR] =
      AdkMessage::kVoiceuiAvsOnboardingError;

  intent_to_adk_message_map_[IntentManagerEvent::UNKNOWN_INTENT] =
      AdkMessage::AdkMsgCase::ADK_MSG_NOT_SET;

  // Message Signals Directed to VoiceUI
  voice_ui_messages_.emplace(AdkMessage::kVoiceuiTapAvs, &voiceUIFramework::voiceUIIntentManager::VoiceUIMessageService::HandleVoiceUIIntent);
  voice_ui_messages_.emplace(AdkMessage::kVoiceuiTapQcasr, &voiceUIFramework::voiceUIIntentManager::VoiceUIMessageService::HandleVoiceUIIntent);
  voice_ui_messages_.emplace(AdkMessage::kVoiceuiStartOnboarding, &voiceUIFramework::voiceUIIntentManager::VoiceUIMessageService::HandleVoiceUIIntent);
  voice_ui_messages_.emplace(AdkMessage::kVoiceuiDeleteCredential, &voiceUIFramework::voiceUIIntentManager::VoiceUIMessageService::HandleVoiceUIIntent);
  voice_ui_messages_.emplace(AdkMessage::kVoiceuiStatus, &voiceUIFramework::voiceUIIntentManager::VoiceUIMessageService::HandleVoiceUIIntent);
  voice_ui_messages_.emplace(AdkMessage::kVoiceuiSetDefaultClient, &voiceUIFramework::voiceUIIntentManager::VoiceUIMessageService::HandleVoiceUIIntent);
  voice_ui_messages_.emplace(AdkMessage::kVoiceuiSetAvsLocale, &voiceUIFramework::voiceUIIntentManager::VoiceUIMessageService::HandleVoiceUIIntent);
  voice_ui_messages_.emplace(AdkMessage::kVoiceuiSetModularClientProps, &voiceUIFramework::voiceUIIntentManager::VoiceUIMessageService::HandleVoiceUIIntent);
  // Message Signals of Intrest from IPC Message Bus
  // TODO (kshriniv) Ascertain if there are enough messages to keep this

  other_adk_messages_.emplace(AdkMessage::kButtonPressed, &voiceUIFramework::voiceUIIntentManager::VoiceUIMessageService::HandleOtherAdkMessageSignal);
  other_adk_messages_.emplace(AdkMessage::kAudioVolumeUpdated, &voiceUIFramework::voiceUIIntentManager::VoiceUIMessageService::HandleOtherAdkMessageSignal);
  other_adk_messages_.emplace(AdkMessage::kAudioMuteUpdated, &voiceUIFramework::voiceUIIntentManager::VoiceUIMessageService::HandleOtherAdkMessageSignal);
  other_adk_messages_.emplace(AdkMessage::kAudioMicMuteUpdated, &voiceUIFramework::voiceUIIntentManager::VoiceUIMessageService::HandleOtherAdkMessageSignal);

  // Create MAP of ADK message Signals to Intents
  // TODO (kshriniv) Do we need to handle Voulme Up event from Button Manager
  // Theory is Audio Manager will handle and send an event

  adk_message_to_intent_map_[AdkMessage::kButtonPressed] = IntentManagerEvent::VOL_UP_EVENT;
  adk_message_to_intent_map_[AdkMessage::kButtonPressed] = IntentManagerEvent::VOL_DOWN_EVENT;
  adk_message_to_intent_map_[AdkMessage::kButtonPressed] = IntentManagerEvent::MUTE_EVENT;
  adk_message_to_intent_map_[AdkMessage::kButtonPressed] = IntentManagerEvent::MIC_MUTE_EVENT;
  adk_message_to_intent_map_[AdkMessage::kButtonPressed] = IntentManagerEvent::TAP_BUTTON_COMMAND;
  //For DEMO Integration
  adk_message_to_intent_map_[AdkMessage::kVoiceuiTapAvs] = IntentManagerEvent::TAP_BUTTON_COMMAND_AVS;
  adk_message_to_intent_map_[AdkMessage::kVoiceuiTapQcasr] = IntentManagerEvent::TAP_BUTTON_COMMAND_QCASR;
  adk_message_to_intent_map_[AdkMessage::kVoiceuiStartOnboarding] = IntentManagerEvent::ONBOARDING_REQUESTED;
  adk_message_to_intent_map_[AdkMessage::kVoiceuiDeleteCredential] = IntentManagerEvent::DELETE_CREDENTIAL;
  adk_message_to_intent_map_[AdkMessage::kVoiceuiStatus] = IntentManagerEvent::VOICEUI_STATUS;
  adk_message_to_intent_map_[AdkMessage::kVoiceuiSetDefaultClient] = IntentManagerEvent::SET_DEFAULT_CLIENT;
  adk_message_to_intent_map_[AdkMessage::kVoiceuiSetAvsLocale] = IntentManagerEvent::SET_AVS_LOCALE;
  adk_message_to_intent_map_[AdkMessage::kVoiceuiSetModularClientProps] = IntentManagerEvent::SET_MODULAR_CLIENT_PROPS;

  //For DEMO Integration

  // Events from Audio Manager
  adk_message_to_intent_map_[AdkMessage::kAudioVolumeUpdated] = IntentManagerEvent::VOL_UPDATED_EVENT;
  adk_message_to_intent_map_[AdkMessage::kAudioMuteUpdated] = IntentManagerEvent::MUTE_UPDATED_EVENT;
  adk_message_to_intent_map_[AdkMessage::kAudioMicMuteUpdated] = IntentManagerEvent::MIC_MUTE_UPDATED_EVENT;
}
VoiceUIMessageService::~VoiceUIMessageService() {
  syslog(LOG_INFO, "VoiceUIMessageService::~VoiceUIMessageService");
}

int VoiceUIMessageService::SendIntent(IntentManagerEvent intent) {
  syslog(LOG_INFO, "IPCSender::SendIntent - Intent to be sent: %s ",
      IntentManagerEventToString(intent).c_str());

  adk::msg::AdkMessage message;
  // TODO: Verify if we need this Interator
  auto it = intent_to_adk_message_map_.find(intent);

  if (it != intent_to_adk_message_map_.end()) {
    switch (it->first) {
      // Volume up and down where step is NOT defined by the client, can be used
      // by local ASR
      case IntentManagerEvent::INCREASE_VOLUME: {
        auto message_case = message.mutable_audio_volume_up();
        message_case->set_step(VARIANT_VALUE_STEP_VOLUME);
        break;
      }
      case IntentManagerEvent::DECREASE_VOLUME: {
        auto message_case = message.mutable_audio_volume_down();
        message_case->set_step(VARIANT_VALUE_STEP_VOLUME);
        break;
      }
      case IntentManagerEvent::INCREASE_THERMOSTAT_TEMPERATURE: {
        auto message_case = message.mutable_connectivity_zigbee_increase_temperature();
        message_case->set_name("thermostat");
        break;
      }
      case IntentManagerEvent::DECREASE_THERMOSTAT_TEMPERATURE: {
        auto message_case = message.mutable_connectivity_zigbee_decrease_temperature();
        message_case->set_name("thermostat");
        break;
      }
      case IntentManagerEvent::TOGGLE_MUTE_STATE:
        message.mutable_audio_mute_toggle();
        break;
      case IntentManagerEvent::START_PLAYBACK:
        message.mutable_audio_track_play();
        break;
      case IntentManagerEvent::STOP_PLAYBACK:
        message.mutable_audio_track_stop();
        break;
      case IntentManagerEvent::PAUSE_PLAYBACK:
        message.mutable_audio_track_pause();
        break;
      case IntentManagerEvent::PLAY_NEXT_TRACK:
        message.mutable_audio_track_next();
        break;
      case IntentManagerEvent::PLAY_PREVIOUS_TRACK:
        message.mutable_audio_track_previous();
        break;
      case IntentManagerEvent::TOGGLE_PLAY_PAUSE:
        message.mutable_audio_track_play_pause_toggle();
        break;
      case IntentManagerEvent::LED_PATTERN_IDLE:
        message.mutable_voiceui_status_idle();
        break;
      case IntentManagerEvent::LED_PATTERN_LISTENING:
        message.mutable_voiceui_status_listening();
        break;
      case IntentManagerEvent::LED_PATTERN_THINKING:
        message.mutable_voiceui_status_thinking();
        break;
      case IntentManagerEvent::LED_PATTERN_ALERT:
        message.mutable_voiceui_status_alert();
        break;
      case IntentManagerEvent::LED_PATTERN_NOTIFICATION:
        message.mutable_voiceui_notification();
        break;
      case IntentManagerEvent::LED_PATTERN_DND:
        message.mutable_voiceui_status_do_not_disturb();
        break;
      case IntentManagerEvent::LED_PATTERN_SPEAKING:
        message.mutable_voiceui_status_speaking();
        break;
      case IntentManagerEvent::LED_PATTERN_SPEECH_DONE:
        message.mutable_voiceui_status_speech_done();
        break;
      case IntentManagerEvent::LED_PATTERN_ONBOARDING:
        message.mutable_voiceui_start_onboarding();
        break;
    }
    SendAdkMessage(message);
  } else {
    syslog(LOG_ERR,
        "Intent is not a valid ADK-IPC Message, Ignoring Intent = %s",
        IntentManagerEventToString(intent).c_str());
  }
  return 0;
}

int VoiceUIMessageService::SendIntent(IntentManagerEvent intent,
    IntentManagerEventExtended intent_ext) {
  syslog(LOG_INFO, "IPCSender::SendIntentExtended - Intent to be sent: %s ",
      IntentManagerEventToString(intent).c_str());
  auto it = intent_to_adk_message_map_.find(intent);

  adk::msg::AdkMessage message;
  if (it != intent_to_adk_message_map_.end()) {
    switch (it->first) {
        // Step is defined by the client
      case IntentManagerEvent::INCREASE_VOLUME: {
        auto message_case = message.mutable_audio_volume_up();
        message_case->set_step(intent_ext.value_double_2);
        break;
      }
      case IntentManagerEvent::DECREASE_VOLUME: {
        auto message_case = message.mutable_audio_volume_down();
        message_case->set_step(intent_ext.value_double_2);
        break;
      }
      case IntentManagerEvent::SET_VOLUME_ABSOLUTE: {
        auto message_case = message.mutable_audio_volume_set();
        message_case->set_value(intent_ext.value_double_1);
        break;
      }
      case IntentManagerEvent::SET_LIGHT_STATE: {
        auto message_case = message.mutable_connectivity_zigbee_set_group_on_off_state();
        message_case->set_group(intent_ext.ts.target);
        message_case->set_state((intent_ext.ts.state == "on") ? true : false);
        break;
      }
      case IntentManagerEvent::SET_CONTROL_DEVICE_STATE: {
        auto message_case = message.mutable_connectivity_zigbee_set_device_on_off_state();
        message_case->set_name(intent_ext.ts.target);
        message_case->set_state((intent_ext.ts.state == "on") ? true : false);
        break;
      }
      case IntentManagerEvent::SET_THERMOSTAT_STATE_ABSOLUTE: {
        auto message_case = message.mutable_connectivity_zigbee_set_device_temperature();
        message_case->set_name("thermostat");
        // The change in temperature in 0.01C. This value will be concatenated to an int16
        message_case->set_temperature(intent_ext.value_uint * 100);
        break;
      }
      case IntentManagerEvent::SET_SECURITY_STATE: {
        auto message_case = message.mutable_voiceui_set_zigbee_security();
        message_case->set_target(intent_ext.ts.target);
        message_case->set_state(intent_ext.ts.state);
        break;
      }
      case IntentManagerEvent::SET_MUTE_STATE: {
        auto message_case = message.mutable_audio_mute_set();
        message_case->set_mute(intent_ext.bool_state_1);
        break;
      }
      case IntentManagerEvent::SELECT_SOURCE: {
        // If the source is TV, fetch target source name from database
        auto message_case = message.mutable_audio_source_select();

        if ((intent_ext.str_1).compare("tv") == 0) {
          auto voice_ui_config =
              voiceUIFramework::voiceUIUtils::VoiceUIConfig::getInstance();
          std::string db_source;
          if (voice_ui_config) {
            db_source = voice_ui_config->ReadWithDefault<std::string>(
                "eARC", "voiceui.intent.source.tv");
            syslog(LOG_INFO, "Source %s", db_source.c_str());
          } else {
            syslog(LOG_ERR, "VoiceUIConfig not Initialized");
          }
          message_case->set_source_name(db_source);
        } else {
          message_case->set_source_name(intent_ext.str_1);
        }
      } break;
      case IntentManagerEvent::START_PLAYBACK: {
        auto message_case = message.mutable_audio_track_play();
        message_case->set_track_play(intent_ext.str_1);
        break;
      }
      case IntentManagerEvent::DISPLAY_MANAGER_NOTIFICATION: {
        auto message_case = message.mutable_display_notification();
        message_case->set_group(adk::msg::kGroupVoiceui);
        message_case->set_name(intent_ext.notification.domain);
        message_case->set_message(intent_ext.notification.message);
        message_case->set_time_ms(intent_ext.notification.display_time);
        break;
      }
      case IntentManagerEvent::DB_FILE_UPDATED: {
        auto message_case = message.mutable_voiceui_database_updated();
        message_case->set_key(intent_ext.str_1);
        break;
      }
      case IntentManagerEvent::AUTHENTICATE_AVS: {
        auto message_case = message.mutable_voiceui_authenticate_avs();
        message_case->set_url(intent_ext.str_1);
        message_case->set_code(intent_ext.str_2);
        break;
      }
      case IntentManagerEvent::ONBOARDING_ERROR: {
        auto message_case = message.mutable_voiceui_avs_onboarding_error();
        message_case->set_client(intent_ext.str_1);
        message_case->set_error(intent_ext.str_2);
        message_case->set_reattempt(intent_ext.value_uint);
        break;
      }
      case IntentManagerEvent::DIRECTION_OF_ARRIVAL: {
        auto message_case = message.mutable_voiceui_status_direction_of_arrival();
        message_case->set_angle(intent_ext.value_uint);
        break;
      }
      default:
        syslog(LOG_ERR,
            "Intent is not a Supported ADK-IPC Message,Ignoring Intent = %s",
            IntentManagerEventToString(intent).c_str());
        break;
    }

    SendAdkMessage(message);
  } else {
    syslog(LOG_ERR,
        "Intent is not a valid ADK-IPC Message,Ignoring Intent = %s",
        IntentManagerEventToString(intent).c_str());
  }
  return 0;
}

void VoiceUIMessageService::SendAdkMessage(const adk::msg::AdkMessage cmd) {
  syslog(LOG_INFO, "ADK Message to be sent: %s", cmd.DebugString().c_str());
  // Send Message to QSAP IPC Bus
  message_service_.Send(cmd);
}
void VoiceUIMessageService::AddObserver(std::shared_ptr<IntentObserver> intent_observer) {
  intent_observer_ = intent_observer;
}

bool VoiceUIMessageService::Start() {
  if (!message_service_.Initialise()) {
    return false;
  }
  // Register for message signals for "Voiceui" namespace
  for (std::map<adk::msg::AdkMessage::AdkMsgCase, msgSignalHandler_>::iterator it = voice_ui_messages_.begin(); it != voice_ui_messages_.end(); ++it) {
    message_service_.Subscribe(it->first,
        [this](adk::msg::AdkMessage msg) {
          // process message signal
          syslog(LOG_INFO, "AdkMessage (Voiceui) has been received with argument: %s ", msg.DebugString().c_str());
          auto iter = voice_ui_messages_.find(msg.adk_msg_case());
          if (iter == voice_ui_messages_.end()) {
            // not found
          } else {
            (this->*iter->second)(msg);
          }
        });
    // Register handler for this particular signal
  }
  for (std::map<adk::msg::AdkMessage::AdkMsgCase, msgSignalHandler_>::iterator it = other_adk_messages_.begin(); it != other_adk_messages_.end(); ++it) {
    message_service_.Subscribe(it->first,
        [this](adk::msg::AdkMessage msg) {
          // process message signal
          syslog(LOG_INFO, "AdkMessage (Voiceui) has been received with argument: %s ", msg.DebugString().c_str());
          auto iter = other_adk_messages_.find(msg.adk_msg_case());
          if (iter == other_adk_messages_.end()) {
            // not found
          } else {
            (this->*iter->second)(msg);
          }
        });
    // Register handler for this particular signal
  }
  return true;
}

void VoiceUIMessageService::HandleVoiceUIIntent(adk::msg::AdkMessage &msg) {
  //Acquire VoiceUI Intent Manger lock
  int delay = WAKELOCK_INTENT_MANAGER_TIMEOUT;
  system_wake_lock_toggle(true, WAKELOCK_INTENTMANAGER_NAME, delay);

  syslog(LOG_INFO, "IPCReceiver::HandleVoiceUIIntent");

  auto dbus_arguments = adk::msg::oneof_id_to_dbus_args.at(msg.adk_msg_case());
  auto ipc_namespace = dbus_arguments.group;
  auto ipc_msg = dbus_arguments.name;

  IntentManagerEvent intent_ = IntentManagerEvent::UNKNOWN_INTENT;
  IntentManagerEventExtended intent_ext_;

  syslog(LOG_INFO, "IPCReceiver::HandleVoiceUIIntent - payload_map = %s",
      ipc_msg.c_str());

  auto it = adk_message_to_intent_map_.find(msg.adk_msg_case());
  if (it != adk_message_to_intent_map_.end()) {
    // Found that there is a matching Intent to the signal, process further
    switch (msg.adk_msg_case()) {
      // For DEMO Integration
      case AdkMessage::kVoiceuiTapAvs:
        syslog(LOG_DEBUG, "SIGNAL_BUTTON_TAP_AVS");
        intent_ = it->second;
        syslog(LOG_DEBUG, "SIGNAL_BUTTON_TAP_AVS - Intent_ = %s",
            IntentManagerEventToString(intent_).c_str());
        if (intent_observer_) {
          syslog(LOG_DEBUG, "SIGNAL_BUTTON_TAP_AVS - Sending Intent");
          intent_observer_->IntentReceived(intent_);
        } else {
          syslog(LOG_ERR, "No Intent Observer Registered");
        }
        break;
      case AdkMessage::kVoiceuiTapQcasr:
        syslog(LOG_DEBUG, "SIGNAL_BUTTON_TAP_QCASR");
        intent_ = it->second;
        syslog(LOG_DEBUG, "SIGNAL_BUTTON_TAP_QCASR - Intent_ = %s",
            IntentManagerEventToString(intent_).c_str());
        if (intent_observer_) {
          syslog(LOG_DEBUG, "SIGNAL_BUTTON_TAP_QCASR - Sending Intent");
          intent_observer_->IntentReceived(intent_);
        } else {
          syslog(LOG_ERR, "No Intent Observer Registered");
        }
        break;
        // For DEMO Integration
      case AdkMessage::kVoiceuiStartOnboarding:
        syslog(LOG_DEBUG, "SIGNAL_START_ONBOARD");
        intent_ = it->second;
        syslog(LOG_DEBUG, "SIGNAL_START_ONBOARD - Intent_ = %s",
            IntentManagerEventToString(intent_).c_str());
        intent_ext_.str_1 = msg.voiceui_start_onboarding().client();
        if (intent_observer_) {
          intent_observer_->IntentReceived(intent_, intent_ext_);
        } else {
          syslog(LOG_ERR, "No Intent Observer Registered");
        }
        break;

      case AdkMessage::kVoiceuiDeleteCredential:
        syslog(LOG_DEBUG, "SIGNAL_DELETE_CREDENTIAL");
        intent_ = it->second;
        syslog(LOG_DEBUG, "SIGNAL_DELETE_CREDENTIAL - Intent_ = %s",
            IntentManagerEventToString(intent_).c_str());
        intent_ext_.str_1 = msg.voiceui_delete_credential().client();
        if (intent_observer_) {
          intent_observer_->IntentReceived(intent_, intent_ext_);
        } else {
          syslog(LOG_ERR, "No Intent Observer Registered");
        }
        break;

      case AdkMessage::kVoiceuiStatus:
        syslog(LOG_DEBUG, "SIGNAL_VOICEUI_STATUS");
        intent_ = it->second;
        syslog(LOG_DEBUG, "SIGNAL_VOICEUI_STATUS - Intent_ = %s",
            IntentManagerEventToString(intent_).c_str());
        intent_ext_.str_1 = msg.voiceui_status().client();
        intent_ext_.bool_state_1 = msg.voiceui_status().status();
        if (intent_observer_) {
          intent_observer_->IntentReceived(intent_, intent_ext_);
        } else {
          syslog(LOG_ERR, "No Intent Observer Registered");
        }
        break;

      case AdkMessage::kVoiceuiSetDefaultClient:
        syslog(LOG_DEBUG, "SIGNAL_SET_DEFAULT_CLIENT");
        intent_ = it->second;
        syslog(LOG_DEBUG, "SIGNAL_SET_DEFAULT_CLIENT - Intent_ = %s",
            IntentManagerEventToString(intent_).c_str());
        intent_ext_.str_1 = msg.voiceui_set_default_client().client();
        if (intent_observer_) {
          intent_observer_->IntentReceived(intent_, intent_ext_);
        } else {
          syslog(LOG_ERR, "No Intent Observer Registered");
        }
        break;

      case AdkMessage::kVoiceuiSetAvsLocale:
        syslog(LOG_DEBUG, "SIGNAL_SET_AVS_LOCALE");
        intent_ = it->second;
        syslog(LOG_DEBUG, "SIGNAL_SET_AVS_LOCALE - Intent_ = %s",
            IntentManagerEventToString(intent_).c_str());
        intent_ext_.str_1 = msg.voiceui_set_avs_locale().locale();
        if (intent_observer_) {
          intent_observer_->IntentReceived(intent_, intent_ext_);
        } else {
          syslog(LOG_ERR, "No Intent Observer Registered");
        }
        break;

      case AdkMessage::kVoiceuiSetModularClientProps:
        syslog(LOG_DEBUG, "SIGNAL_SET_MODULAR_CLIENT_PROPS");
        intent_ = it->second;
        syslog(LOG_DEBUG, "SIGNAL_SET_MODULAR_CLIENT_PROPS - Intent_ = %s",
            IntentManagerEventToString(intent_).c_str());
        intent_ext_.modular_properties.path = msg.voiceui_set_modular_client_props().path();
        intent_ext_.modular_properties.language = msg.voiceui_set_modular_client_props().language();
        intent_ext_.modular_properties.asr = msg.voiceui_set_modular_client_props().asr();
        intent_ext_.modular_properties.nlu = msg.voiceui_set_modular_client_props().nlu();
        intent_ext_.modular_properties.tts = msg.voiceui_set_modular_client_props().tts();
        if (intent_observer_) {
          intent_observer_->IntentReceived(intent_, intent_ext_);
        } else {
          syslog(LOG_ERR, "No Intent Observer Registered");
        }
        break;
      default:
        syslog(LOG_ERR,
            "IPCReceiver::HandleVoiceUIIntent: There is no Handle for "
            "Message = %s",
            ipc_msg.c_str());
        break;
    }
  } else {
    syslog(LOG_ERR, "Payload is not a valid message. Message  Received = %s",
        ipc_msg.c_str());
  }
}

void VoiceUIMessageService::HandleOtherAdkMessageSignal(adk::msg::AdkMessage &msg) {
  //Acquire VoiceUI Intent Manger lock
  int delay = WAKELOCK_INTENT_MANAGER_TIMEOUT;
  system_wake_lock_toggle(true, WAKELOCK_INTENTMANAGER_NAME, delay);

  syslog(LOG_INFO, "IPCReceiver::HandleOtherAdkMessageSignal");

  auto dbus_arguments = adk::msg::oneof_id_to_dbus_args.at(msg.adk_msg_case());
  auto ipc_namespace = dbus_arguments.group;
  auto ipc_msg = dbus_arguments.name;

  IntentManagerEvent intent_ = IntentManagerEvent::UNKNOWN_INTENT;
  IntentManagerEventExtended intent_ext_;

  syslog(LOG_INFO,
      "IPCReceiver::HandleOtherAdkMessageSignal - payload_map = %s",
      ipc_msg.c_str());

  auto it = adk_message_to_intent_map_.find(msg.adk_msg_case());

  if (it != adk_message_to_intent_map_.end()) {
    // Found that there is a matching Intent to the signal, process further
    switch (msg.adk_msg_case()) {
      case AdkMessage::kAudioVolumeUpdated:
        intent_ = it->second;
        intent_ext_.value_double_1 = msg.audio_volume_updated().volume();
        if (intent_observer_) {
          intent_observer_->IntentReceived(intent_, intent_ext_);
        } else {
          syslog(LOG_ERR, "No Intent Observer Registered");
        }
        break;
      case AdkMessage::kAudioMuteUpdated:
        intent_ = it->second;
        intent_ext_.bool_state_1 = msg.audio_mute_updated().mute();
        if (intent_observer_) {
          intent_observer_->IntentReceived(intent_, intent_ext_);
        } else {
          syslog(LOG_ERR, "No Intent Observer Registered");
        }
        break;
      case AdkMessage::kAudioMicMuteUpdated:
        intent_ = it->second;
        intent_ext_.bool_state_1 = msg.audio_mic_mute_updated().mute();
        if (intent_observer_) {
          intent_observer_->IntentReceived(intent_, intent_ext_);
        } else {
          syslog(LOG_ERR, "No Intent Observer Registered");
        }
        break;
      case AdkMessage::kButtonPressed: {
        // There any many intents based out of this signal
        const char *button_value = msg.button_pressed().button().c_str();
        switch (IpcSignalHash(button_value)) {
          case IpcSignalHash(VARIANT_VALUE_BUTTON_MIC_MUTE):
            intent_ = IntentManagerEvent::MIC_MUTE_EVENT;
            break;
          case IpcSignalHash(VARIANT_VALUE_BUTTON_TAP):
            intent_ = IntentManagerEvent::TAP_BUTTON_COMMAND;
            break;
        }
        if (IntentManagerEvent::UNKNOWN_INTENT != intent_) {
          if (intent_observer_) {
            intent_observer_->IntentReceived(intent_);
          } else {
            syslog(LOG_ERR, "No Intent Observer Registered");
          }
        } else {
          syslog(LOG_ERR, "Unknown Button Pressed Signal");
        }
        break;
      }
      // For DEMO Integration
      case AdkMessage::kVoiceuiTapAvs:
        syslog(LOG_DEBUG, "SIGNAL_BUTTON_TAP_AVS");
        intent_ = it->second;
        syslog(LOG_DEBUG, "SIGNAL_BUTTON_TAP_AVS - Intent_ = %s",
            IntentManagerEventToString(intent_).c_str());
        if (intent_observer_) {
          intent_observer_->IntentReceived(intent_);
        } else {
          syslog(LOG_ERR, "No Intent Observer Registered");
        }
        break;
      case AdkMessage::kVoiceuiTapQcasr:
        intent_ = it->second;
        syslog(LOG_DEBUG, "SIGNAL_BUTTON_TAP_QCASR - Intent_ = %s",
            IntentManagerEventToString(intent_).c_str());
        if (intent_observer_) {
          intent_observer_->IntentReceived(intent_);
        } else {
          syslog(LOG_ERR, "No Intent Observer Registered");
        }
        break;
        // For DEMO Integration

      default:
        syslog(LOG_ERR,
            "IPCReceiver::HandleOtherAdkMessageSignal: There is no Handle "
            "for Message = %s",
            ipc_msg.c_str());
        break;
    }
  } else {
    syslog(LOG_ERR,
        "IPCReceiver::HandleOtherAdkMessageSignal: Payload is not a valid "
        "event. Signal Received = %s",
        ipc_msg.c_str());
  }
  syslog(LOG_ERR,
      "IPCReceiver::HandleOtherAdkMessageSignal: DONE!!! Message = %s",
      ipc_msg.c_str());
}

}  // namespace voiceUIIntentManager
}  // namespace voiceUIFramework
