/* Copyright (c) 2019-2020, The Linux Foundation. All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 * Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above
 copyright notice, this list of conditions and the following
 disclaimer in the documentation and/or other materials provided
 with the distribution.
 * Neither the name of The Linux Foundation nor the names of its
 contributors may be used to endorse or promote products derived
 from this software without specific prior written permission.

 THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file   ThirdPartyAudioUtils.cpp
 *  @brief  Audio helper methods for Third Party Audio OUT
 *
 *  DESCRIPTION
 *    Third Party Audio output helper methods
 ***************************************************************/

#include <syslog.h>
#include <cmath>

#include "VoiceUIUtils/ThirdPartyAudioUtils.h"

#define VALID_CHANNEL_MIN 1
#define VALID_CHANNEL_MAX 9
#define DEMUX_16KHZ_ADVANCE 3

namespace voiceUIFramework {
namespace voiceUIUtils {

std::shared_ptr<ThirdPartyAudioUtils> ThirdPartyAudioUtils::thirdparty_audio_utils_s_ = nullptr;
bool ThirdPartyAudioUtils::instance_ = false;

std::shared_ptr<ThirdPartyAudioUtils> ThirdPartyAudioUtils::getInstance() {
  if (!instance_) {
    thirdparty_audio_utils_s_ = std::make_shared<ThirdPartyAudioUtils>(constructor_cookie());

    if (thirdparty_audio_utils_s_) {
      instance_ = true;
      return thirdparty_audio_utils_s_;
    } else {
      syslog(LOG_INFO, "ThirdPartyAudioUtils::getInstance(): returns nullptr");
      return nullptr;
    }

  } else {
    return thirdparty_audio_utils_s_;
  }
}

std::vector<int> ThirdPartyAudioUtils::Demux(
    std::vector<int> muxed_signal,
    const int channel) {
  std::vector<int> demux_line;

  if (false == validate(muxed_signal, channel)) {
    syslog(LOG_ERR, "validation failed!");
    return demux_line;
  }

  std::vector<int>::iterator it = muxed_signal.begin();
  std::advance(it, channel - 1);

  int begin_index = channel - 1;
  while (begin_index < muxed_signal.size()) {
    demux_line.push_back(*it);

    std::advance(it, num_of_out_channels_);
    begin_index += num_of_out_channels_;
  }

  return demux_line;
}

std::vector<int> ThirdPartyAudioUtils::Demux16khz(
    std::vector<int> muxed_signal,
    const int channel) {
  std::vector<int> demuxed_16khz_signal;

  if (false == validate(muxed_signal, channel)) {
    syslog(LOG_ERR, "validation failed!");
    return demuxed_16khz_signal;
  }

  std::vector<int>::iterator it = muxed_signal.begin();
  std::advance(it, channel - 1);

  int total_advance = num_of_out_channels_ * DEMUX_16KHZ_ADVANCE;

  int begin_index = channel - 1;
  while (begin_index < muxed_signal.size()) {
    demuxed_16khz_signal.push_back(*it);

    std::advance(it, total_advance);
    begin_index += total_advance;
  }

  return demuxed_16khz_signal;
}

std::vector<uint8_t> ThirdPartyAudioUtils::Demux16khz16bitpcm(
    std::vector<int> muxed_signal,
    const int channel) {
  std::vector<uint8_t> demuxed_16khz_16bitpcm_signal;

  if (false == validate(muxed_signal, channel)) {
    syslog(LOG_ERR, "validation failed!");
    return demuxed_16khz_16bitpcm_signal;
  }

  std::vector<int>::iterator it = muxed_signal.begin();
  std::advance(it, channel - 1);

  int total_advance = num_of_out_channels_ * DEMUX_16KHZ_ADVANCE;

  int begin_index = channel - 1;
  while (begin_index < muxed_signal.size()) {
    int audio_sample = *it;

    demuxed_16khz_16bitpcm_signal.push_back((uint8_t)(audio_sample >> 16));
    demuxed_16khz_16bitpcm_signal.push_back((uint8_t)(audio_sample >> 24));

    std::advance(it, total_advance);
    begin_index += total_advance;
  }

  return demuxed_16khz_16bitpcm_signal;
}

bool ThirdPartyAudioUtils::validate(std::vector<int> muxed_signal, int channel) {
  if (channel < VALID_CHANNEL_MIN && channel > VALID_CHANNEL_MAX) {
    syslog(
        LOG_ERR,
        "ThirdPartyAudioUtils::de-multiplex channel is not in the right format");
    return false;
  }

  if (std::remainder((long)muxed_signal.size(), (long)num_of_out_channels_)
      != 0) {
    syslog(
        LOG_ERR,
        "ThirdPartyAudioUtils::de-multiplex muxed output not in right format");
    return false;
  }

  return true;
}

void ThirdPartyAudioUtils::setNumOfOutputChannels(unsigned int num_of_out_channels) {
  syslog(LOG_DEBUG, "ThirdPartyAudioUtils::setNumOfOutputChannels: New output channel count =%d", num_of_out_channels);
  num_of_out_channels_ = num_of_out_channels;
}

}  // namespace voiceUIUtils
}  // namespace voiceUIFramework
