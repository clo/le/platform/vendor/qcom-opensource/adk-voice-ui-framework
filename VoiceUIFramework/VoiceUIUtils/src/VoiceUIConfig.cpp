/* Copyright (c) 2018, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    Config.cpp
 *  @brief   brief description
 *
 *  DESCRIPTION
 *    description
 ***************************************************************/
#include "../include/VoiceUIUtils/VoiceUIConfig.h"
#include <syslog.h>

namespace voiceUIFramework {
namespace voiceUIUtils {

// Path to database file containing configuration (provided by cmake)
const char kConfigVoiceUIDatabaseFilePath[] = VOICEUI_DB_PATH;
const char *kConfigAudioMangerDatabaseFilePath = AUDIOMANGER_DB_PATH;

std::shared_ptr<VoiceUIConfig> VoiceUIConfig::voice_ui_config_ = nullptr;
bool VoiceUIConfig::instance_flag_ = false;

std::shared_ptr<VoiceUIConfig> VoiceUIConfig::getInstance() {
  if (!instance_flag_) {
    int numOfVoiceUIClients;

    voice_ui_config_ = std::make_shared<VoiceUIConfig>(constructor_cookie());
    //Connect and Load the config database

    voice_ui_config_->config_db_ = adk::config::Config::Create(kConfigVoiceUIDatabaseFilePath);

    if (voice_ui_config_->config_db_) {
      instance_flag_ = true;
      return voice_ui_config_;
    } else {
      syslog(LOG_INFO, "VoiceUIConfig::getInstance(): VoiuceUI configuration file not present");
      return nullptr;
    }

  } else {
    return voice_ui_config_;
  }
}
}  // namespace voiceUIUtils
}  // namespace voiceUIFramework
