cmake_minimum_required(VERSION 3.0 FATAL_ERROR)
project(VoiceUIUtils LANGUAGES CXX)

include(../../build/BuildDefaults.cmake)

add_subdirectory("src")

if(BUILD_TARGET_UNIT_TESTS)
    add_subdirectory("test")
endif(BUILD_TARGET_UNIT_TESTS)
