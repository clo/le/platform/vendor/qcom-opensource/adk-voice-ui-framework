/* Copyright (c) 2018, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    VoiceUIConfigTest.cpp
 *  @brief   brief description
 *  Reads the database created by voiceui-schema-db.txt and validates against the same
 *  DESCRIPTION
 *    description
 ***************************************************************/

#include <iostream>
#include <memory>
#include <string>

#include <iostream>
#include <memory>
#include <string>

#include "VoiceUIUtils/VoiceUIConfig.h"
#include "gtest/gtest.h"

using namespace std;
using voiceUIFramework::voiceUIUtils::Keys;
using voiceUIFramework::voiceUIUtils::VoiceUIConfig;

namespace {
class VoiceUIConfigTest : public ::testing::Test {
 protected:
  VoiceUIConfigTest() {
    // You can do set-up work for each test here.
  }

  virtual ~VoiceUIConfigTest() {
    // You can do clean-up work that doesn't throw exceptions here.
  }

  virtual void SetUp() {
    voice_ui_config = voiceUIFramework::voiceUIUtils::VoiceUIConfig::getInstance();
  }

  virtual void TearDown() {
  }
  // Objects declared here can be used by all tests in the test case for VoiceUIManager.
  std::shared_ptr<VoiceUIConfig> voice_ui_config;
};

TEST_F(VoiceUIConfigTest, MethodTestCheckDatabaseInstance) {
  syslog(LOG_INFO, "***** VoiceUIConfigTest::MethodTestCheckDatabaseInstance - START");
  ASSERT_NE(nullptr, voice_ui_config);
  syslog(LOG_INFO, "***** VoiceUIConfigTest::MethodTestCheckDatabaseInstance - END");
}

TEST_F(VoiceUIConfigTest, MethodTestReadDbDefault) {
  syslog(LOG_INFO, "***** VoiceUIConfigTest::MethodTestReadDbDefault - START");
  ASSERT_NE(nullptr, voice_ui_config);
  std::string db_source;
  // voiceui.intent.source.tv1 doesnt exist in the database
  db_source = voice_ui_config->ReadWithDefault<std::string>("eARC", "voiceui.intent.source.tv1");
  ASSERT_STREQ("eARC", db_source.c_str());
  syslog(LOG_INFO, "Source %s", db_source.c_str());
  syslog(LOG_INFO, "***** VoiceUIConfigTest::MethodTestReadDbDefault - END");
}

TEST_F(VoiceUIConfigTest, MethodTestReadDb) {
  syslog(LOG_INFO, "***** VoiceUIConfigTest::MethodTestReadDb - START");
  ASSERT_NE(nullptr, voice_ui_config);
  std::string return_value_string;
  int ret_value_int;
  bool ret_value_bool;

  //TODO: Fix unit tests with right config keys
  // voiceui.intent.source.tv exists in the database

  return_value_string = voice_ui_config->ReadWithDefault<std::string>("eARC", "voiceui.intent.source.tv");
  ASSERT_STREQ("ARC", return_value_string.c_str());
  syslog(LOG_INFO, "Source %s", return_value_string.c_str());

  ret_value_int = voice_ui_config->ReadWithDefault<int>(0, "voiceui.num_clients");
  ASSERT_EQ(2, ret_value_int);

  ret_value_int = voice_ui_config->ReadWithDefault<int>(0, "voiceui.ffv.default.client");
  ASSERT_EQ(1, ret_value_int);

  ret_value_bool = voice_ui_config->ReadWithDefault<bool>(false, "voiceui.ffv.status.enabled");
  ASSERT_EQ(true, ret_value_bool);

  ret_value_bool = voice_ui_config->ReadWithDefault<bool>(false, "voiceui.ffv.voice.dump.enabled");
  ASSERT_EQ(true, ret_value_bool);

  return_value_string = voice_ui_config->ReadWithDefault<std::string>("_invalid_path", "voiceui.ffv.voice.dump.path");
  ASSERT_STREQ("/data/voice-ui-framework", return_value_string.c_str());
  syslog(LOG_INFO, "Source %s", return_value_string.c_str());

  // AVS Database Settings
  return_value_string = voice_ui_config->ReadWithDefault<std::string>("abcd", voiceUIFramework::voiceUIUtils::Keys::k_avs_sound_model_);
  ASSERT_STREQ("/data/voice-ui-framework/sm3_gmm_0703_cnn_jan08_Alexa.uim", return_value_string.c_str());

  syslog(LOG_INFO, "***** VoiceUIConfigTest::MethodTestReadDb - END");
}

}  // namespace

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
