/*
 * Copyright (c) 2018-2020, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*************************************************************
 *  @file    config-keys.h
 *  @brief   This file contains the configuration keys for voiceUI
 *
 *  DESCRIPTION
 *    description
 ***************************************************************/

#ifndef VOICEUIFRAMEWORK_VOICEUIUTILS_INCLUDE_VOICEUIUTILS_CONFIG_KEYS_H_
#define VOICEUIFRAMEWORK_VOICEUIUTILS_INCLUDE_VOICEUIUTILS_CONFIG_KEYS_H_

/** @brief adk namespace*/
namespace voiceUIFramework {

/** @brief Namespace for the VoiceUI*/
namespace voiceUIUtils {

/** @brief Configuration keys for the Voice UI*/
class Keys {
 public:
  /** @brief Sets the threshold for SVA while in idle*/
  static constexpr const char* k_ffv_sva_threshold_ = "voiceui.ffv.sva.threshold";
  /** @brief Sets the threshold for SVA while playing back audio*/
  static constexpr const char* k_ffv_sva_streaming_threshold_ = "voiceui.ffv.sva.streamingthreshold";
  /** @brief Sets the threshold for SVA while voice call*/
  static constexpr const char* k_ffv_sva_call_threshold_ = "voiceui.ffv.sva.callthreshould";
  /** @brief Sets the default number of microphones */
  static constexpr const char* k_framework_num_of_mics_ = "voiceui.ffv.num.of.mics";
  /** @brief Sets the default number of EC channels*/
  static constexpr const char* k_framework_num_of_ec_channels_ = "voiceui.ffv.num.of.ec.channels";
  /** @brief Sets the default voice interface client for platform*/
  static constexpr const char* k_framework_default_client_ = "voiceui.ffv.default.client";
  /** @brief Define is Voice UI framework is enable or disable*/
  static constexpr const char* k_framework_status_ = "voiceui.ffv.status.enabled";
  /** @brief Define if voice command logging(pcm dump) is enable or disable*/
  static constexpr const char* k_framework_voice_dump_status_ = "voiceui.ffv.voice.dump.enabled";
  /** @brief Define path for voice dump files*/
  static constexpr const char* k_framework_voice_dump_path_ = "voiceui.ffv.voice.dump.path";
  /** @brief Multiple wakeword engine, client concurrency*/
  static constexpr const char* k_framework_client_concurrency_ = "voiceui.ffv.clientconcurrency";

  /** @brief Define if AVS is enable or disable*/
  static constexpr const char* k_avs_status_ = "voiceui.avs.enabled";
  /** @brief Define if AVS Wakeword is enable or disable*/
  static constexpr const char* k_avs_wakeword_status_ = "voiceui.avs.wakeword";

  /** @brief Define if AVS is enable or disable*/
  static constexpr const char* k_avs_onboarded_ = "voiceui.avs.onboarded";
  /** @brief AVS AVS Authorization code to be used by end user while on-boarding*/
  static constexpr const char* k_avs_authorization_code_ = "voiceui.avs.authcode";
  /** @brief AVS AVS Authorization url to be used by end user while on-boarding*/
  static constexpr const char* k_avs_authorization_url_ = "voiceui.avs.authurl";
  /** @brief Define local/language value for AVS*/
  static constexpr const char* k_avs_locale_ = "voiceui.avs.locale";
  /** @brief Path for AVS credential file*/
  static constexpr const char* k_avs_credential_file_ = "voiceui.avs.credentialfile";
  /** @brief AVS clientID information*/
  static constexpr const char* k_avs_client_id_ = "voiceui.avs.clientid";
  /** @brief AVS productID information*/
  static constexpr const char* k_avs_product_id_ = "voiceui.avs.productid";
  /** @brief Path for AVS codebase link authentication database file*/
  static constexpr const char* k_avs_cblauth_db_ = "voiceui.avs.cblauthdb";
  /** @brief AVS sound model for wakeword engine*/
  static constexpr const char* k_avs_sound_model_ = "voiceui.avs.soundmodel";
  /** @brief AVS thirdparty keyword detection engine status*/
  static constexpr const char* k_avs_third_party_kwd_enabled_ = "voiceui.avs.third_party_kwd_enabled";
  /** @brief AVS thirdparty keyword detection engine selection*/
  static constexpr const char* k_avs_third_party_kwd_selection_ = "voiceui.avs.third_party_kwd_selection";
  /** @brief AVS sound model for AVS wakeword engine*/
  static constexpr const char* k_avs_wwe_sound_model_ = "voiceui.avs.wwe.soundmodel";
  /** @brief AVS keyword confidence level for stage 1 SVA for wakeword engine*/
  static constexpr const char* k_avs_stage1_confidence_ = "voiceui.avs.stage1.confidence";
  /** @brief AVS keyword confidence level for stage 2 SVA for wakeword engine*/
  static constexpr const char* k_avs_stage2_confidence_ = "voiceui.avs.stage2.confidence";
  /** @brief Indicates if AVS keyword detection has a second stage running on ARM*/
  static constexpr const char* k_avs_has_arm_second_stage_ = "voiceui.avs.has_arm_stage2";
  /** @brief AVS keyword confidence level for stage 2 Amazon WWE running on ARM*/
  static constexpr const char* k_avs_stage2_avs_wwe_confidence_ = "voiceui.avs.wwe.stage2.confidence";
  /** @brief AVS timeout for Amazon WWE voice input validation*/
  static constexpr const char* k_avs_stage2_avs_wwe_timeout_ = "voiceui.avs.wwe.stage2.timeout";
  /** @brief Define BUFSIZE required by Third Party*/
  static constexpr const char* k_third_party_bufsize = "voiceui.ffv.tp.audio.block.size";
  /** @brief Sensory sound model for AVS wakeword engine*/
  static constexpr const char* k_avs_sensory_sound_model = "voiceui.avs.sensory.soundmodel";
  /** @brief Sensory keyword confidence level for AVS sound model*/
  static constexpr const char* k_avs_sensory_operating_point = "voiceui.avs.sensory.operating.point";
  /** @brief Define if Modular Client is enable or disable*/
  static constexpr const char* k_modular_status_ = "voiceui.modular.enabled";
  /** @brief Define if Modular Client Wakeword is enable or disable*/
  static constexpr const char* k_modular_wakeword_status_ = "voiceui.modular.wakeword";

  /** @brief Modular Client sound model for wakeword engine*/
  static constexpr const char* k_modular_sound_model_ = "voiceui.modular.soundmodel";
  /** @brief Modular keyword confidence level for stage 1 SVA for wakeword engine*/
  static constexpr const char* k_modular_stage1_confidence_ = "voiceui.modular.stage1.confidence";
  /** @brief Modular keyword confidence level for stage 2 SVA for wakeword engine*/
  static constexpr const char* k_modular_stage2_confidence_ = "voiceui.modular.stage2.confidence";

  /** @brief List of all languages supported by ASR client*/
  static constexpr const char* k_modular_language_list_ = "voiceui.modular.language.list";
  /** @brief Define ASR language selection*/
  static constexpr const char* k_modular_language_selected_ = "voiceui.modular.language.selected";
  /** @brief List of all ASR clients supported*/
  static constexpr const char* k_modular_asr_list_ = "voiceui.modular.asrprovider.list";
  /** @brief Define ASR client selection*/
  static constexpr const char* k_modular_asr_selected_ = "voiceui.modular.asrprovider.selected";

  /** @brief List of all NLU clients supported*/
  static constexpr const char* k_modular_nlu_list_ = "voiceui.modular.nluprovider.list";
  /** @brief Define NLU client selection*/
  static constexpr const char* k_modular_nlu_selected_ = "voiceui.modular.nluprovider.selected";

  /** @brief List of all TTS clients supported*/
  static constexpr const char* k_modular_tts_list_ = "voiceui.modular.ttsprovider.list";
  /** @brief Define TTS client selection*/
  static constexpr const char* k_modular_tts_selected_ = "voiceui.modular.ttsprovider.selected";

  /** @brief Define QTi Local ASR sound model*/
  static constexpr const char* k_modular_qtiasr_soundmodel_ = "voiceui.modular.qtiasr.soundmodel";
  /** @brief Define threshold for local ASR engine*/
  static constexpr const char* k_modular_qtiasr_threshold_ = "voiceui.modular.qtiasr.threshold";
  /** @brief Define ending count for local ASR engine*/
  static constexpr const char* k_modular_qtiasr_maxpause_ = "voiceui.modular.qtiasr.maxpause";
  /** @brief Define timeout for local ASR engine*/
  static constexpr const char* k_modular_qtiasr_timeout_ = "voiceui.modular.qtiasr.timeout";
  /** @brief Mic Mute status information from audio-manager db*/
  static constexpr const char* k_mic_mute_staus_ = "audio.manager.mic_mute";
};

}  // namespace voiceUIUtils
}  // namespace voiceUIFramework

#endif /* VOICEUIFRAMEWORK_VOICEUIUTILS_INCLUDE_VOICEUIUTILS_CONFIG_KEYS_H_ */
