/* Copyright (c) 2018-2019, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    Config.h
 *  @brief   Header file tod describe single ton class to acess config database
 *
 *  DESCRIPTION
 *
 ***************************************************************/
#ifndef CODE_VOICEUIFRAMEWORK_VOICEUIUTILS_INCLUDE_VOICEUIUTILS_VOICEUICONFIG_H_
#define CODE_VOICEUIFRAMEWORK_VOICEUIUTILS_INCLUDE_VOICEUIUTILS_VOICEUICONFIG_H_

#include <VoiceUIUtils/GlobalValues.h>
#include <VoiceUIUtils/config-keys.h>
#include <adk/config/config.h>
#include <syslog.h>
#include <iostream>
#include <memory>

using adk::config::Config;

namespace voiceUIFramework {
namespace voiceUIUtils {

// Path to database file containing configuration (provided by cmake)
extern const char* kConfigAudioMangerDatabaseFilePath;

class VoiceUIConfig {
 private:
  static bool instance_flag_;
  std::unique_ptr<Config> config_db_;
  static std::shared_ptr<VoiceUIConfig> voice_ui_config_;
  struct constructor_cookie {};

 public:
  static std::shared_ptr<VoiceUIConfig> getInstance();
  // Reads Unit
  template <typename T>
  T ReadWithDefault(const T& default_value,
      const std::string& key);

  // Reads Array
  template <typename T>
  T ReadWithDefault(const T& default_value,
      int index, const std::string& key,
      const std::string& subkey);

  // Modifies an existing key in the database
  template <typename T>
  bool Write(const T& value, const std::string& key);

  // create = true creates a new key in the database
  template <typename T>
  bool Write(const T& value, const std::string& key,
      bool create);

  // Cannot use shared_ptr with private constructor
  explicit VoiceUIConfig(constructor_cookie){};

  ~VoiceUIConfig() {
    instance_flag_ = false;
  }
};
// Template function to read a config item of type T and return its value,
// automatically returning a given default value if the read fails.
template <typename T>
T VoiceUIConfig::ReadWithDefault(const T& default_value,
    const std::string& key) {
  T result;
  if (!config_db_->Read(&result, key)) {
    syslog(LOG_ERR, "VoiceUIConfig::ReadWithDefault - Error Reading Database %s", key.c_str());
    result = default_value;
  }
  return result;
}

// Template function to write an existing config key.
template <typename T>
bool VoiceUIConfig::Write(const T& value,
    const std::string& key) {
  bool result;
  if (!(result = config_db_->Write(value, key, false))) {
    syslog(LOG_ERR, "VoiceUIConfig::Write - Error Writing Database %s", key.c_str());
  }
  return result;
}

// Template function to write an existing config key or create a new one.
template <typename T>
bool VoiceUIConfig::Write(const T& value,
    const std::string& key, bool create) {
  bool result;
  if (!(result = config_db_->Write(value, key, create))) {
    syslog(LOG_ERR, "VoiceUIConfig::Write - Error Writing Database %s", key.c_str());
  }
  return result;
}

// Overloaded version for reading config array items with an index.
template <typename T>
T VoiceUIConfig::ReadWithDefault(const T& default_value,
    int index, const std::string& key,
    const std::string& subkey) {
  T result;
  if (!config_db_->Read(&result, index, key, subkey)) {
    syslog(LOG_ERR, "VoiceUIConfig::ReadWithDefault - Error Reading Database%s", key.c_str());
    result = default_value;
  }
  return result;
}

// Template function to read a audio-manager config item of type T and return its value,
// automatically returning a given default value if the read fails.
template <typename T>
T ReadNonVoiceUIDbWithDefault(const T& default_value,
    const std::string& key) {
  std::unique_ptr<Config> config_db_ = adk::config::Config::Create(kConfigAudioMangerDatabaseFilePath);
  T result;
  if (!config_db_->Read(&result, key)) {
    syslog(LOG_ERR, "VoiceUIConfig::ReadWithDefault - Error Reading Database %s", key.c_str());
    result = default_value;
  }
  return result;
}
// TODO: (kshriniv) write and restore database when needed

}  // namespace voiceUIUtils
}  // namespace voiceUIFramework
#endif /* CODE_VOICEUIFRAMEWORK_VOICEUIUTILS_INCLUDE_VOICEUIUTILS_VOICEUICONFIG_H_ */
