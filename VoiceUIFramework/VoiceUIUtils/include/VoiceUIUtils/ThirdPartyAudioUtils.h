/* Copyright (c) 2019-2020, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    ThirdPartyAudioUtils.h
 *  @brief   Audio helper methods for Third Party Audio OUT
 *
 *  DESCRIPTION
 *    Third Party Audio output helper methods
 ***************************************************************/

#ifndef VOICEUIUTILS_INCLUDE_VOICEUIUTILS_THIRDPARTYAUDIOUTILS_H_
#define VOICEUIUTILS_INCLUDE_VOICEUIUTILS_THIRDPARTYAUDIOUTILS_H_

#include <cmath>
#include <memory>
#include <vector>

namespace voiceUIFramework {
namespace voiceUIUtils {

class ThirdPartyAudioUtils {
 private:
  static bool instance_;
  unsigned int num_of_out_channels_;
  static std::shared_ptr<ThirdPartyAudioUtils> thirdparty_audio_utils_s_;
  struct constructor_cookie {};

 public:
  static std::shared_ptr<ThirdPartyAudioUtils> getInstance();
  std::vector<int> Demux(
      std::vector<int> muxed_signal,
      const int channel_request);
  std::vector<int> Demux16khz(
      std::vector<int> muxed_signal,
      const int channel_request);
  std::vector<uint8_t> Demux16khz16bitpcm(
      std::vector<int> muxed_signal,
      const int channel_request);
  bool validate(std::vector<int> muxed_signal, int channel);
  void setNumOfOutputChannels(unsigned int num_of_cout_channels);
  explicit ThirdPartyAudioUtils(constructor_cookie){};
  ~ThirdPartyAudioUtils() {
    instance_ = false;
  }
};

}  // namespace voiceUIUtils
}  // namespace voiceUIFramework

#endif /* VOICEUIUTILS_INCLUDE_VOICEUIUTILS_THIRDPARTYAUDIOUTILS_H_ */
