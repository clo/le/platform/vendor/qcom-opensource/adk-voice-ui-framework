/* Copyright (c) 2018-2020, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    GlobalValues.h
 *  @brief   Global Voice UI Values
 *
 *  DESCRIPTION
 *    All Voice UI values and definitions used on Voice UI Framework
 ***************************************************************/

#ifndef VOICEUIFRAMEWORK_VOICEUIUTILS_INCLUDE_VOICEUIUTILS_GLOBALVALUES_H_
#define VOICEUIFRAMEWORK_VOICEUIUTILS_INCLUDE_VOICEUIUTILS_GLOBALVALUES_H_

#include <regex>
#include <string>

namespace voiceUIFramework {
namespace voiceUIUtils {

/* An enum class for all Intents Commands/Events handled by Voice UI Framework */
enum class VoiceUIClientID {
  UNKNOWN_CLIENT = 0,
  AVS_SOLUTION,
  CORTANA_SOLUTION,
  GVA_SOLUTION,
  MODULAR_SOLUTION,
  MOCK_FULL_SOLUTION,
  MOCK_MODULAR_SOLUTION
};

/* Method to convert Client ID to String */
inline const std::string ClientIDToString(VoiceUIClientID client) {
  switch (client) {
    case VoiceUIClientID::UNKNOWN_CLIENT:
      return "Unknown Client Solution";
    case VoiceUIClientID::AVS_SOLUTION:
      return "AVS";
    case VoiceUIClientID::CORTANA_SOLUTION:
      return "CORTANA";
    case VoiceUIClientID::GVA_SOLUTION:
      return "GVA";
    case VoiceUIClientID::MODULAR_SOLUTION:
      return "MODULAR";
    case VoiceUIClientID::MOCK_FULL_SOLUTION:
      return "MOCK_FULL";
    case VoiceUIClientID::MOCK_MODULAR_SOLUTION:
      return "MOCK_MODULAR";
  }
  return "Unknown Client ID";
}

/* Method to convert Client ID to Index */
inline int ClientIDToIndex(VoiceUIClientID client) {
  switch (client) {
    case VoiceUIClientID::UNKNOWN_CLIENT:
      return 0;
    case VoiceUIClientID::AVS_SOLUTION:
      return 1;
    case VoiceUIClientID::CORTANA_SOLUTION:
      return 2;
    case VoiceUIClientID::GVA_SOLUTION:
      return 3;
    case VoiceUIClientID::MODULAR_SOLUTION:
      return 4;
    case VoiceUIClientID::MOCK_FULL_SOLUTION:
      return 5;
    case VoiceUIClientID::MOCK_MODULAR_SOLUTION:
      return 6;
  }
  return -1;
}

inline VoiceUIClientID StringToClientID(std::string client) {
  if (client.compare("AVS") == 0)
    return VoiceUIClientID::AVS_SOLUTION;
  if (client.compare("CORTANA") == 0)
    return VoiceUIClientID::CORTANA_SOLUTION;
  if (client.compare("GVA") == 0)
    return VoiceUIClientID::GVA_SOLUTION;
  if (client.compare("MODULAR") == 0)
    return VoiceUIClientID::MODULAR_SOLUTION;

  return VoiceUIClientID::UNKNOWN_CLIENT;
}
//Check if a string is a valid number
inline bool IsNumber(const std::string &str) {
  return std::regex_match(str, std::regex("(\\+|-)?[0-9]*(\\.?([0-9]+))$"));
}

/* An enum class for all Intents Third party Keyword Engines supported */
enum class ThirdPartyKWDEngines {
  NO_CLIENT = -1,
  SENSORY_CLIENT,
  AVS_WWE_CLIENT
};

/* Method to convert ThirdPartyKWDEnginesID to Index */
inline int ThirdPartyKWDEnginesIDToIndex(ThirdPartyKWDEngines engine) {
  switch (engine) {
    case ThirdPartyKWDEngines ::SENSORY_CLIENT:
      return 0;
    case ThirdPartyKWDEngines ::AVS_WWE_CLIENT:
      return 1;
  }
  return -1;
}

/* VoiceUI WakeLock definitions*/
#define WAKELOCK_AVS_TIMEOUT 30000
#define WAKELOCK_AVS_CONNECTION_TIMEOUT 3000
#define WAKELOCK_INTENT_MANAGER_TIMEOUT 2000
#define WAKELOCK_AVS_MEDIAPLAYER_SETUP_TIMEOUT 5000
#define WAKELOCK_MODULAR_CLIENT_TIMEOUT 15000

#define WAKELOCK_VUI_INIT_NAME "VOICE_UI_INIT"
#define WAKELOCK_AVS_NAME "AVS"
#define WAKELOCK_INTENTMANAGER_NAME "VoiceUi_Intent_Manager"
#define WAKELOCK_AVS_CONNECTION_NAME "AVS_CONNECTION"
#define WAKELOCK_MODULAR_CLIENT_NAME "MODULAR_CLIENT"

}  // namespace voiceUIUtils
}  // namespace voiceUIFramework

#endif /* VOICEUIFRAMEWORK_VOICEUIUTILS_INCLUDE_VOICEUIUTILS_GLOBALVALUES_H_ */
