/* Copyright (c) 2018, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    VoiceUIManager.h
 *  @brief   Voice UI Manager class to handle all Voice UI Clients (Full and Modular)
 *
 *  DESCRIPTION
 *    Allow creation, configuration and management of all voice UI Clients.
 *    Also Responsible to broadcast all events received from IntentManager for voice UI Clients.
 *    Provides notification for all NotificationObserver register on it.
 ***************************************************************/

#include <VoiceUIManager/VoiceUIManager.h>

//TODO: Orignally need to around 5sec
//Decreased to make unit tests faster
//TODO: Need to use compile flags to distinguish settngs for unit tests and application
#define INTERNET_CHECK_PERIODICITY_MILLISEC 5000

namespace voiceUIFramework {
namespace voiceUIManager {

using namespace std;
using namespace voiceUIFramework::voiceUIIntentManager;
using namespace voiceUIFramework::voiceUIUtils;
using namespace voiceUIFramework::voiceUIClient;

#ifdef AVS_CLIENT_ENABLED
using namespace voiceUIFramework::avsManager;
#endif

VoiceUIManager::VoiceUIManager()
    : disable_adk_ipc_{false}, internet_monitor_thread_(), internet_check_interval_(INTERNET_CHECK_PERIODICITY_MILLISEC), initialization_lock_released_{false}, mic_mute{false} {
  //Acquire VoiceUI Initialization LOCK.
  int delay = WAKELOCK_NO_TIMEOUT;
  system_wake_lock_toggle(true, WAKELOCK_VUI_INIT_NAME, delay);

  syslog(LOG_INFO, "+++VoiceUIManager::VoiceUIManager - VoiceUIManager Built!");

  //Voice UI Intent Manager
  voice_ui_intent_manager_ = std::make_shared<VoiceUIIntentManagerIPC>();

  //Read Mic Mute status from audio-manager Database
  mic_mute = voiceUIFramework::voiceUIUtils::ReadNonVoiceUIDbWithDefault<bool>(false, voiceUIUtils::Keys::k_mic_mute_staus_);

  //Read VoiceUI Database
  auto voice_ui_config = voiceUIFramework::voiceUIUtils::VoiceUIConfig::getInstance();
  if (voice_ui_config) {
    //Read default VoiceUiClient
    int default_client_index = voice_ui_config->ReadWithDefault<int>(ClientIDToIndex(VoiceUIClientID::UNKNOWN_CLIENT), voiceUIUtils::Keys::k_framework_default_client_);
    default_client_id_ = static_cast<VoiceUIClientID>(default_client_index);
    //Read VoiceUI Status
    voice_ui_enabled_ = voice_ui_config->ReadWithDefault<bool>(true, voiceUIUtils::Keys::k_framework_status_);
  } else {
    syslog(LOG_ERR, "VoiceUIManager::VoiceUIManager() - VoiceUIConfig not Initialized");
    default_client_id_ = VoiceUIClientID::UNKNOWN_CLIENT;
    voice_ui_enabled_ = true;
  }
  syslog(LOG_INFO, "VoiceUIManager::VoiceUIManager() - Default Client = %s", ClientIDToString(default_client_id_).c_str());
  syslog(LOG_INFO, "VoiceUIManager::VoiceUIManager() - VoiceUI Status = %d", voice_ui_enabled_);
  syslog(LOG_INFO, "VoiceUIManager::VoiceUIManager() - Mic Mute Status = %d", mic_mute);
}

//Thread object is initialised in constructor,But started here
//This is to allow complete construction of VUI Manager object
void VoiceUIManager::Start() {
  syslog(LOG_INFO, "VoiceUIManager::Start()");
}

VoiceUIManager::~VoiceUIManager() {
  syslog(LOG_INFO, "---VoiceUIManager::~VoiceUIManager");
}

void VoiceUIManager::NewSolutionStatus(VoiceUIClientID client_id, VUISolutionStatus status) {
  std::lock_guard<std::mutex> lock(mu);
  syslog(LOG_INFO, "VoiceUIManager::NewSolutionStatus - Client: %s - Event: %s", ClientIDToString(client_id).c_str(),
      VUISolutionStatusToString(status).c_str());
  for (auto observer : notification_observer_list_) {
    std::string playback_notification_ = " NewSolutionStatus: " + VUISolutionStatusToString(status);
    observer->NotificationReceived(client_id, playback_notification_);

    //If AVS is connected for first time release VoiceUI Initialization wakelock
    if (!initialization_lock_released_ && client_id == VoiceUIClientID::AVS_SOLUTION && status == VUISolutionStatus::IDLE) {
      syslog(LOG_INFO, "VoiceUIManager::NewSolutionStatus - AVS in Idle after initialization - Release VoiceUI Initialization wakelock");
      //Release VoiceUI Initialization LOCK.
      system_wake_lock_toggle(false, WAKELOCK_VUI_INIT_NAME, WAKELOCK_NO_TIMEOUT);

      //Set system to suspend mode - TODO: MOVE LOGIC TO SYSTEM-MANAGER
      const std::string power_auto_sleep_filename = "/sys/power/autosleep";

      std::ofstream power_auto_sleep_file;
      power_auto_sleep_file.open(power_auto_sleep_filename);
      power_auto_sleep_file << "mem" << std::endl;

      if (power_auto_sleep_file.bad())
        syslog(LOG_INFO, "VoiceUIManager::NewSolutionStatus Failed to Set Autosleep");
      else
        syslog(LOG_INFO, "VoiceUIManager::NewSolutionStatus Success to Set Autosleep");

      power_auto_sleep_file.close();
      //Set system to suspend mode - TODO: MOVE LOGIC TO SYSTEM-MANAGER

      initialization_lock_released_ = true;
    }
  }
}

void VoiceUIManager::KeywordDetected(VoiceUIClientID client_id) {
  std::lock_guard<std::mutex> lock(mu);
  for (auto observer : notification_observer_list_) {
    std::string keyword_notification_ = " KeywordDetected";
    observer->NotificationReceived(client_id, keyword_notification_);
  }
}

void VoiceUIManager::DirectionOfArrival(VoiceUIClientID client_id, int direction) {
  std::lock_guard<std::mutex> lock(mu);
  for (auto observer : notification_observer_list_) {
    std::string doa_notification_ = " DirectionOfArrival";
    observer->NotificationReceived(client_id, doa_notification_);
  }
}

void VoiceUIManager::VolumeStatus(VoiceUIClientID client_id, int volume) {
  std::lock_guard<std::mutex> lock(mu);
  for (auto observer : notification_observer_list_) {
    std::string volume_notification_ = " VolumeStatus";
    observer->NotificationReceived(client_id, volume_notification_);
  }
}

void VoiceUIManager::MuteStatus(VoiceUIClientID client_id, bool state) {
  std::lock_guard<std::mutex> lock(mu);
  for (auto observer : notification_observer_list_) {
    std::string mute_notification_ = " MuteStatus";
    observer->NotificationReceived(client_id, mute_notification_);
  }
}

void VoiceUIManager::ActiveStatus(VoiceUIClientID client_id, bool state) {
  std::lock_guard<std::mutex> lock(mu);

  auto voice_ui_config = voiceUIFramework::voiceUIUtils::VoiceUIConfig::getInstance();
  if (voice_ui_config) {
    // update active state to the database
    switch (client_id) {
      case VoiceUIClientID::AVS_SOLUTION: {
        if (voice_ui_config->Write<bool>(state, voiceUIUtils::Keys::k_avs_wakeword_status_)) {
          // Send database Updated
          IntentManagerEvent intent;
          IntentManagerEventExtended intent_ext;
          intent = IntentManagerEvent::DB_FILE_UPDATED;
          intent_ext.str_1 = voiceUIUtils::Keys::k_avs_wakeword_status_;
          voice_ui_intent_manager_->SendIntent(intent, intent_ext);
        }
      } break;
      case VoiceUIClientID::MODULAR_SOLUTION: {
        if (voice_ui_config->Write<bool>(state, voiceUIUtils::Keys::k_modular_wakeword_status_)) {
          // Send database Updated
          IntentManagerEvent intent;
          IntentManagerEventExtended intent_ext;
          intent = IntentManagerEvent::DB_FILE_UPDATED;
          intent_ext.str_1 = voiceUIUtils::Keys::k_modular_wakeword_status_;
          voice_ui_intent_manager_->SendIntent(intent, intent_ext);
        }
      } break;
      default:
        syslog(LOG_INFO, "VoiceUIManager::ActiveStatus  - Client: %s Unsupported", ClientIDToString(client_id).c_str());
        break;
    }
  }
  for (auto observer : notification_observer_list_) {
    std::string active_notification_ = " ActiveStatus";
    observer->NotificationReceived(client_id, active_notification_);
  }
}

void VoiceUIManager::SendIntent(VoiceUIClientID client_id, IntentManagerEvent intent) {
  std::lock_guard<std::mutex> lock(mu);
  syslog(LOG_INFO, "VoiceUIManager::SendIntentCommand  - Client: %s - Intent: %s", ClientIDToString(client_id).c_str(),
      IntentManagerEventToString(intent).c_str());

  //send intent to IntentManager
  voice_ui_intent_manager_->SendIntent(intent);

  //send callback to OEM observers
  for (auto observer : notification_observer_list_) {
    observer->NotificationReceived(client_id, IntentManagerEventToString(intent).c_str());
  }
}

void VoiceUIManager::SendIntent(VoiceUIClientID client_id, IntentManagerEvent intent, IntentManagerEventExtended intent_ext) {
  std::lock_guard<std::mutex> lock(mu);
  syslog(LOG_INFO, "VoiceUIManager::SendIntentCommand(Extended)  - Client: %s - Intent: %s", ClientIDToString(client_id).c_str(),
      IntentManagerEventToString(intent).c_str());

  //send intent to IntentManager
  voice_ui_intent_manager_->SendIntent(intent, intent_ext);

  //send callback to OEM observers
  for (auto observer : notification_observer_list_) {
    observer->NotificationReceived(client_id, IntentManagerEventToString(intent).c_str());
  }
}

void VoiceUIManager::RequestRestartClient(VoiceUIClientID client_id) {
  syslog(LOG_INFO, "VoiceUIManager::RequestClientRestart - client_id: %s", ClientIDToString(client_id).c_str());

  //TODO restart on the FLY

  //  //Stop client
  //  if (!StopClient(client_id)) {
  //    syslog(LOG_ERR, "VoiceUIManager::RequestClientRestart - Fail to STOP/Delete client:", ClientIDToString(client_id).c_str());
  //  }
  //Sleep for 8 seconds
  //  std::this_thread::sleep_for(std::chrono::milliseconds(8000));
  //  syslog(LOG_INFO, "VoiceUIManager::RequestClientRestart - Restarting Client:", ClientIDToString(client_id).c_str());

  //  bool ret = false;
  //  switch (client_id) {
  //    case VoiceUIClientID::AVS_SOLUTION:
  //      //Re create and restart AVS client - Wakeword embeeded = FALSE
  //      ret = CreateClient(client_id, false);
  //      break;
  //    case VoiceUIClientID::MODULAR_SOLUTION:
  //      //Re create and restart Modular Client client - Wakeword embeeded = FALSE
  //      ret = CreateClient(client_id, false);
  //      break;
  //    default:
  //      syslog(LOG_ERR, "VoiceUIManager::RequestClientRestart  - CreateClient: %s Unsupported", ClientIDToString(client_id).c_str());
  //      break;
  //  }
  //  if (!ret) {
  //    syslog(LOG_ERR, "VoiceUIManager::RequestClientRestart - Fail to Create Client:", ClientIDToString(client_id).c_str());
  //  }
}

void VoiceUIManager::IntentReceived(IntentManagerEvent &intent) {
  syslog(LOG_INFO, "VoiceUIManager::IntentReceived - Intent: %s", IntentManagerEventToString(intent).c_str());

  //Process VOL UP and VOl DOWN events - KEYBOARD INPUT may be removed later
  if (intent == IntentManagerEvent::VOL_UP_EVENT) {
    IntentManagerEvent intent = IntentManagerEvent::INCREASE_VOLUME;
    SendIntent(VoiceUIClientID::UNKNOWN_CLIENT, intent);
  } else if (intent == IntentManagerEvent::VOL_DOWN_EVENT) {
    IntentManagerEvent intent = IntentManagerEvent::DECREASE_VOLUME;
    SendIntent(VoiceUIClientID::UNKNOWN_CLIENT, intent);
  }
  //Process VOL UP and VOl DOWN events - KEYBOARD INPUT may be removed later

  //Send Intent just to default Voice UI Client
  else if (intent == IntentManagerEvent::TAP_BUTTON_COMMAND) {
    //TODO: ADD more events that only default client should receive
    if (!mic_mute) {
      syslog(LOG_DEBUG, "VoiceUIManager::IntentReceived - Send intent for DEFAULT Client : %s", ClientIDToString(default_client_id_).c_str());
      auto default_client = client_map_.find(ClientIDToIndex(default_client_id_));
      if (default_client != client_map_.end()) {
        default_client->second->IntentReceived(intent);
      } else {
        syslog(LOG_ERR, "VoiceUIManager::IntentReceived - Default Client configured does not exist : ", ClientIDToString(default_client_id_).c_str());
      }
    } else {
      syslog(LOG_ERR, "VoiceUIManager::IntentReceived - TAP_BUTTON_COMMAND - FAIL  reason:MicMuteStatus=%d ", mic_mute);
    }
  }
  //Send Intent for all Voice UI Clients
  else {
    syslog(LOG_DEBUG, "VoiceUIManager::IntentReceived - Sending intent for ALL Clients");

    // Get an iterator pointing to beginning of map
    std::unordered_map<int, std::shared_ptr<BaseVoiceUIClient>>::iterator it = client_map_.begin();
    // Iterate over all items on map to broadcast event/command to all Voice UI Clients
    while (it != client_map_.end()) {
      if (nullptr != it->second) {
        it->second->IntentReceived(intent);
        it++;
      }
    }
  }
}

void VoiceUIManager::IntentReceived(IntentManagerEvent &intent, IntentManagerEventExtended &intent_ext) {
  syslog(LOG_INFO, "VoiceUIManager::IntentReceivedExtended - Intent: %s", IntentManagerEventToString(intent).c_str());

  //check for VoiceUI manager specific Intents
  if (intent == IntentManagerEvent::VOICEUI_STATUS) {
    syslog(LOG_DEBUG, "VoiceUIManager::IntentReceived - VOICEUI_STATUS");
    auto clientid = StringToClientID(intent_ext.str_1);
    auto status = intent_ext.bool_state_1;
    auto wakeword_status = intent_ext.bool_state_2;
    auto client = client_map_.find(ClientIDToIndex(clientid));
    if (client != client_map_.end()) {
      if (nullptr != client->second) {
        syslog(LOG_DEBUG, "VoiceUIManager::IntentReceived - client %s Enable %d", ClientIDToString(clientid).c_str(), status);
        client->second->SetActive(status);
      }
    } else {
      syslog(LOG_DEBUG, "VoiceUIManager::IntentReceived - VOICEUI_STATUS - Client Not Found");
    }
  }
  //Set VoiceUi default Client
  else if (intent == IntentManagerEvent::SET_DEFAULT_CLIENT) {
    auto clientid = StringToClientID(intent_ext.str_1);
    syslog(LOG_DEBUG, "VoiceUIManager::IntentReceived - SET_DEFAULT_CLIENT %s", ClientIDToString(clientid).c_str());
    default_client_id_ = clientid;

    auto voice_ui_config = voiceUIFramework::voiceUIUtils::VoiceUIConfig::getInstance();
    IntentManagerEvent intent;

    //Update the database
    if (voice_ui_config) {
      //write locale result
      if (voice_ui_config->Write<std::string>(intent_ext.str_1, voiceUIUtils::Keys::k_framework_default_client_)) {
        // Send database Updated
        intent = IntentManagerEvent::DB_FILE_UPDATED;
        IntentManagerEventExtended intent_ext;
        intent_ext.str_1 = voiceUIUtils::Keys::k_framework_default_client_;
        if (voice_ui_intent_manager_)
          voice_ui_intent_manager_->SendIntent(intent, intent_ext);

      } else {
        //TODO:
        ConsolePrinter::prettyPrint("Fatal Error, Unable to write to DB, Inconsistent");
      }
    } else {
      ConsolePrinter::prettyPrint("Fatal Error, Unable to access Database");
    }
  }
  //Delete Specific Client Credentials
  else if (intent == IntentManagerEvent::DELETE_CREDENTIAL) {
    auto clientid = StringToClientID(intent_ext.str_1);
    syslog(LOG_DEBUG, "VoiceUIManager::IntentReceived - DELETE_CREDENTIAL %s", ClientIDToString(clientid).c_str());
    auto client = client_map_.find(ClientIDToIndex(clientid));
    if (client != client_map_.end()) {
      if (nullptr != client->second) {
        syslog(LOG_DEBUG, "VoiceUIManager::IntentReceived - client %s DELETE_CREDENTIAL Sent", ClientIDToString(clientid).c_str());
        client->second->IntentReceived(intent, intent_ext);
      }
    } else {
      syslog(LOG_DEBUG, "VoiceUIManager::IntentReceived - DELETE_CREDENTIAL - Client Not Found");
    }
  }
  //Onboard request for a specific client
  else if (intent == IntentManagerEvent::ONBOARDING_REQUESTED) {
    auto clientid = StringToClientID(intent_ext.str_1);
    syslog(LOG_DEBUG, "VoiceUIManager::IntentReceived - ONBOARDING_REQUESTED %s", ClientIDToString(clientid).c_str());
    auto client = client_map_.find(ClientIDToIndex(clientid));
    if (client != client_map_.end()) {
      if (nullptr != client->second) {
        syslog(LOG_DEBUG, "VoiceUIManager::IntentReceived - client %s ONBOARDING_REQUESTED Sent", ClientIDToString(clientid).c_str());
        client->second->IntentReceived(intent, intent_ext);
      }
    } else {
      syslog(LOG_DEBUG, "VoiceUIManager::IntentReceived - ONBOARDING_REQUESTED- Client Not Found");
    }
  }
  //Mic Mute Update event
  else if (intent == IntentManagerEvent::MIC_MUTE_UPDATED_EVENT) {
    syslog(LOG_INFO, "VoiceUIManager::IntentReceived - MIC_MUTE_UPDATED_EVENT -  MicMuteStatus=%d", intent_ext.bool_state_1);
    mic_mute = intent_ext.bool_state_1;
  }
  //Events for all clients
  else {
    syslog(LOG_DEBUG, "VoiceUIManager::IntentReceived - Sending intent for ALL Clients");
    // Get an iterator pointing to beginning of map
    std::unordered_map<int, std::shared_ptr<BaseVoiceUIClient>>::iterator it = client_map_.begin();
    // Iterate over all items on map to broadcast event/command to all Voice UI Clients
    while (it != client_map_.end()) {
      if (nullptr != it->second) {
        it->second->IntentReceived(intent, intent_ext);
        it++;
      }
    }
  }
}

void VoiceUIManager::SendMessageToClient(VoiceUIClientID client_id, AppMessage app_message) {
  std::lock_guard<std::mutex> lock(mu);
  syslog(LOG_INFO, "VoiceUIManager::SendMessageToClient - AppMessage: %s to %s Client ", AppMessageToString(app_message).c_str(), ClientIDToString(client_id).c_str());
  auto client = client_map_.find(ClientIDToIndex(client_id));
  if (nullptr != client->second) {
    client->second->AppMessageReceived(app_message);
  }
}

void VoiceUIManager::SendMessageToClient(AppMessage app_message) {
  std::lock_guard<std::mutex> lock(mu);

  syslog(LOG_INFO, "VoiceUIManager::SendMessageToClient(All Clients) - AppMessage: %s", AppMessageToString(app_message).c_str());

  // Get an iterator pointing to beginning of map
  std::unordered_map<int, std::shared_ptr<BaseVoiceUIClient>>::iterator it = client_map_.begin();
  // Iterate over all items on map to broadcast AppMessage to all Voice UI Clients
  while (it != client_map_.end()) {
    if (nullptr != it->second) {
      it->second->AppMessageReceived(app_message);
      it++;
    }
  }
}

bool VoiceUIManager::CreateClient(VoiceUIClientID client_id, bool wake_word_embedded) {
  syslog(LOG_INFO, "VoiceUIManager::CreateClient - ID = %d - Client Name: %s", client_id, ClientIDToString(client_id).c_str());

  std::shared_ptr<BaseVoiceUIClient> client;

  switch (client_id) {
#ifdef MODULAR_CLIENT_ENABLED
    case VoiceUIClientID::MODULAR_SOLUTION: {
      client = std::make_shared<ModularVoiceUIClient>(shared_from_this(), client_id, wake_word_embedded);
    } break;
#endif
    //Create Voice Ui Client
#ifdef AVS_CLIENT_ENABLED
    case VoiceUIClientID::AVS_SOLUTION: {
      client = std::make_shared<AVSVoiceUIClient>(shared_from_this(), client_id, wake_word_embedded);
    } break;
#endif
    default:
      syslog(LOG_ERR, "VoiceUIManager::CreateClient - UnSupported Client - FAIL.");
      return false;
  }

  //Add client to MAP
  if (!client_map_.insert({ClientIDToIndex(client_id), client}).second) {
    syslog(LOG_ERR, "VoiceUIManager::CreateClient - duplicate Client - FAIL.");
    return false;
  }

  //TODO: VERIFY HOW TO START LATER WHEN ENABLE STATUS IS CHANGED
  if (voice_ui_enabled_) {
    //start Voice UI CLient
    return StartClient(client_id);
  }
}

bool VoiceUIManager::StartClient(VoiceUIClientID client_id) {
  auto client = client_map_.find(ClientIDToIndex(client_id));

  if (client != client_map_.end()) {
    if (nullptr != client->second) {
      if (client->second->IsActive()) {
        syslog(LOG_ERR, "VoiceUIManager::StartClient - Client Already Running");
        return true;
      }
      if (client->second->Configure()) {
        return client->second->Start();
      } else {
        return false;
      }
    }
  } else {
    syslog(LOG_ERR, "VoiceUIManager::StartClient - Client not found");
  }
  return false;
}

bool VoiceUIManager::StopClient(VoiceUIClientID client_id) {
  auto client = client_map_.find(ClientIDToIndex(client_id));
  if (client != client_map_.end()) {
    syslog(LOG_INFO, "VoiceUIManager::StopClient - Client: %s", ClientIDToString(client_id).c_str());
    if (nullptr != client->second) {
      client->second->Stop();
      client->second->Shutdown();
      //Delete client from client List
      client_map_.erase(client);
    }
    return true;
  } else {
    syslog(LOG_ERR, "VoiceUIManager::StartClient - Client not found.");
    return false;
  }
}

void VoiceUIManager::AddNotificationObserver(std::shared_ptr<NotificationObserver> notification_observer) {
  if (!notification_observer_list_.insert(notification_observer).second) {
    syslog(LOG_ERR, "VoiceUIManager::AddNotificationObserver - duplicate notification Observer - FAIL.");
  }
}

void VoiceUIManager::Shutdown() {
  syslog(LOG_INFO, "VoiceUIManager::Shutdown ClientList #: %d", client_map_.size());

  //Signal wait for network thread to exit
  stop_internet_monitor_thread = true;

  //Wait for network thread
  if (internet_monitor_thread_.joinable()) {
    internet_monitor_thread_.join();
  }
  // Get an iterator pointing to beginning of map
  std::unordered_map<int, std::shared_ptr<BaseVoiceUIClient>>::iterator it = client_map_.begin();

  // Iterate over all items on map to request shutdown
  while (it != client_map_.end()) {
    if (nullptr != it->second) {
      it->second->Stop();
      it->second->Shutdown();
      it++;
    }
  }

  //Shutdown IntentManager and STOP ADK-IPC thread.
  voice_ui_intent_manager_->Shutdown();

  client_map_.clear();
  syslog(LOG_INFO, "VoiceUIManager::Shutdown -  After Map Clean - ClientList #: %d", client_map_.size());
}

bool VoiceUIManager::Configure() {
  //WORKAROUND to DO NOT start ADK IPC for unit tests. TO BE REMOVED LATER
  if (!disable_adk_ipc_) {
    voice_ui_intent_manager_->Configure();
  }

  voice_ui_intent_manager_->AddObserver(shared_from_this());

  //Start VoiceUIManager
  Start();

  return true;
}

}  // namespace voiceUIManager
}  // namespace voiceUIFramework
