/* Copyright (c) 2018, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    NotificationObserver.h
 *  @brief   Observer to listen for all events and call backs form Voice UI Framework
 *           Implement this class and register it on Voice UI Manager to receive callbacks from all Voice UI Clients.
 *
 *  DESCRIPTION
 *    Allow OEM to register its own application on VOice UI Manager to listen for Voice UI Client notifications.
 ***************************************************************/

#ifndef VOICEUIFRAMEWORK_VOICEUIMANAGER_INCLUDE_NOTIFICATIONOBSERVER_H_
#define VOICEUIFRAMEWORK_VOICEUIMANAGER_INCLUDE_NOTIFICATIONOBSERVER_H_

#include <VoiceUIUtils/GlobalValues.h>

using namespace voiceUIFramework::voiceUIUtils;

namespace voiceUIFramework {
namespace voiceUIManager {

class NotificationObserver {
 public:
  virtual ~NotificationObserver() = default;

  //Notification containing callback received from Voice UI Clients
  virtual void NotificationReceived(VoiceUIClientID client_id, std::string event) = 0;
};

}  // namespace voiceUIManager
}  // namespace voiceUIFramework

#endif /* VOICEUIFRAMEWORK_VOICEUIMANAGER_INCLUDE_NOTIFICATIONOBSERVER_H_ */
