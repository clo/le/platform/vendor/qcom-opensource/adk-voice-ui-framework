/* Copyright (c) 2018, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    VoiceUIManager.h
 *  @brief   Voice UI Manager class to handle all Voice UI Clients (Full and Modular)
 *
 *  DESCRIPTION
 *    Allow creation, configuration and management of all voice UI Clients.
 *    Also Responsible to broadcast all events received from IntentManager for voice UI Clients.
 *    Provides notification for all NotificationObserver register on it.
 ***************************************************************/

#ifndef VOICEUIFRAMEWORK_VOICEUIMANAGER_INCLUDE_VOICEUIMANAGER_H_
#define VOICEUIFRAMEWORK_VOICEUIMANAGER_INCLUDE_VOICEUIMANAGER_H_

#include <iostream>
#include <memory>
#include <mutex>
#include <string>
#include <unordered_map>
#include <unordered_set>

#include <IntentManager/IntentObserver.h>
#include <IntentManager/VoiceUIIntentManagerIPC.h>
#include <VoiceUIClient/BaseSolution/ClientObserver.h>
#include <VoiceUIClient/FullSolution/FullVoiceUIClient.h>
#include <VoiceUIClient/ModularSolution/ModularVoiceUIClient.h>
#include <VoiceUIManager/NotificationObserver.h>
#include <VoiceUIUtils/VoiceUIConfig.h>

#ifdef AVS_CLIENT_ENABLED
//Voice UI Solutions Clients
#include <AVSClientIntegration/AVSVoiceUIClient.h>
#endif

namespace voiceUIFramework {
namespace voiceUIManager {

using namespace voiceUIFramework::voiceUIIntentManager;
using namespace voiceUIFramework::voiceUIClient;
using namespace voiceUIFramework::voiceUIUtils;

class VoiceUIManager : public voiceUIFramework::voiceUIClient::ClientObserver, public voiceUIFramework::voiceUIIntentManager::IntentObserver, public std::enable_shared_from_this<VoiceUIManager> {
 public:
  VoiceUIManager();
  ~VoiceUIManager();

  /* ClientObserver Interface */
  //Callback for playback state changed
  virtual void NewSolutionStatus(VoiceUIClientID client_id, VUISolutionStatus status);
  //Callback for keyword detected
  virtual void KeywordDetected(VoiceUIClientID client_id);
  //Callback for direction of arrival (doa)
  virtual void DirectionOfArrival(VoiceUIClientID client_id, int direction);
  //Callback for volume status changed
  virtual void VolumeStatus(VoiceUIClientID client_id, int volume);
  //Callback for mute status changed
  virtual void MuteStatus(VoiceUIClientID client_id, bool state);
  //Callback for enable/disable status changed
  virtual void ActiveStatus(VoiceUIClientID client_id, bool state);
  //Sends Intent from Voice UI Client
  virtual void SendIntent(VoiceUIClientID client_id, IntentManagerEvent intent);
  virtual void SendIntent(VoiceUIClientID client_id, IntentManagerEvent intent, IntentManagerEventExtended intent_ext);
  //Request Client Restart
  virtual void RequestRestartClient(VoiceUIClientID client_id);

  /* IntentObserver Interface */
  // Receives intents received by Voice UI IntentManager
  virtual void IntentReceived(IntentManagerEvent &intent);
  virtual void IntentReceived(IntentManagerEvent &intent, IntentManagerEventExtended &intent_ext);

  //Creates a new Voice UI Client Solution
  virtual bool CreateClient(VoiceUIClientID client_id, bool wake_word_embedded);
  //Stars Voice UI Client
  bool StartClient(VoiceUIClientID client_id);
  //Stops Voice UI Client
  bool StopClient(VoiceUIClientID client_id);
  //Adds a NotificationObserver to listen for callbacks from Voice UI Clients
  void AddNotificationObserver(std::shared_ptr<NotificationObserver> notification_observer);

  //Sends a message to client (from App or Equivalent thread)
  void SendMessageToClient(VoiceUIClientID client_id, AppMessage app_message);
  //Overloaded, to All Clients
  void SendMessageToClient(AppMessage app_message);

  //Shutdown Voice UI Manager
  void Shutdown();
  //Configure VoiceUIManager and set all observer
  bool Configure();

  //WORKAROUND to DO NOT start ADK-IPC for unit tests. TO BE REMOVED LATER
  bool disable_adk_ipc_;

 protected:
  //Voice UI Clients list.
  std::unordered_map<int, std::shared_ptr<BaseVoiceUIClient>> client_map_;

  //Voice UI Intent manager
  std::shared_ptr<VoiceUIIntentManagerIPC> voice_ui_intent_manager_;

 private:
  //Setup date and time
  void SetupSystemTime();
  //Configure a Voice UI Client
  bool ConfigureClient(VoiceUIClientID client_id);
  //Check if network is available
  void CheckForInternetLoop();
  //Construction dependent startup procedure
  void Start();

  //Voice UI Notification Observers.
  std::unordered_set<std::shared_ptr<NotificationObserver>> notification_observer_list_;

  //Mutex to handle multiple clients(multithreading) callbacks.
  std::mutex mu;

  VoiceUIClientID default_client_id_;

  //Voice UI Status
  bool voice_ui_enabled_;

  std::thread internet_monitor_thread_;
  bool stop_internet_monitor_thread = false;  // simple thread stopping.

 protected:
  //Interval to check for Internet availability
  int internet_check_interval_;

  //initialization lock
  bool initialization_lock_released_;

  //Mic Mute update
  bool mic_mute;
};

}  // namespace voiceUIManager
}  // namespace voiceUIFramework

#endif /* VOICEUIFRAMEWORK_VOICEUIMANAGER_INCLUDE_VOICEUIMANAGER_H_ */
