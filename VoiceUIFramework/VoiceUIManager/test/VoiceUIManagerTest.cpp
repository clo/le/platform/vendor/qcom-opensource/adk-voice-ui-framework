/* Copyright (c) 2018, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    VoiceUIManagerTest.cpp
 *  @brief   brief description
 *
 *  DESCRIPTION
 *    description
 ***************************************************************/

#include <iostream>
#include <memory>
#include <string>

#include <iostream>
#include <memory>
#include <string>

#include "VoiceUIFrameworkStub.h"
#include "gtest/gtest.h"

using namespace std;
using namespace voiceUIFramework::voiceUIFrameworkStub;

namespace {

class VoiceUIManagerExtended : public VoiceUIManager {
 public:
  VoiceUIManagerExtended() {
    // You can do set-up work for each test here.
  }

  ~VoiceUIManagerExtended() {
    // You can do clean-up work that doesn't throw exceptions here.
  }

  //Extented Function from VoiceUIManager to create Mocks
  virtual bool CreateClient(VoiceUIClientID client_id, bool wake_word_embedded) {
    syslog(LOG_INFO, "VoiceUIManagerExtended::CreateClient - Client: %s", ClientIDToString(client_id).c_str());

    //Create Voice Ui Client
    std::shared_ptr<BaseVoiceUIClient> client;

    if (client_id == VoiceUIClientID::MOCK_FULL_SOLUTION) {
      client = std::make_shared<MockFullVoiceUIClient>(shared_from_this(), client_id, wake_word_embedded);
    } else if (client_id == VoiceUIClientID::MOCK_MODULAR_SOLUTION) {
      client = std::make_shared<MockModularVoiceUIClient>(shared_from_this(), client_id, wake_word_embedded);
    } else {
      syslog(LOG_ERR, "VoiceUIManagerExtended::CreateClient - UnSupported Client - FAIL.");
      return false;
    }

    //Add client to MAP
    if (!client_map_.insert({ClientIDToIndex(client_id), client}).second) {
      syslog(LOG_ERR, "VoiceUIManagerExtended::CreateClient - duplicate Client - FAIL.");
      return false;
    }

    //start Voice UI CLient
    return StartClient(client_id);
  }

  //Helper functions for test
  int GetClientListCount() {
    return client_map_.size();
  }

  std::shared_ptr<BaseVoiceUIClient> GetClientFromClientId(VoiceUIClientID client_id) {
    auto client = client_map_.find(ClientIDToIndex(client_id));
    if (client != client_map_.end()) {
      return client->second;
    } else {
      return NULL;
    }
  }

  void ChangeInternetCheckInterval(int interval) {
    internet_check_interval_ = interval;
  }
};

// The fixture for testing class VoiceUIManager.
class VoiceUIManagerTest : public ::testing::Test {
 protected:
  VoiceUIManagerTest() {
    // You can do set-up work for each test here.
  }

  virtual ~VoiceUIManagerTest() {
    // You can do clean-up work that doesn't throw exceptions here.
  }

  std::shared_ptr<VoiceUIManagerExtended> vmgr_;
  std::shared_ptr<MockNotificationObserver> notification_observer_;

  virtual void SetUp() {
    vmgr_ = std::make_shared<VoiceUIManagerExtended>();
    notification_observer_ = std::make_shared<MockNotificationObserver>();

    //WORKAROUND to DO NOT start ADK-IPC for unit tests. TO BE REMOVED LATER
    vmgr_->disable_adk_ipc_ = true;

    //Change InternetCheck interval for UNIT tests - Form 5 sec to 300 ms.
    vmgr_->ChangeInternetCheckInterval(300);

    //Configure VoiceUIManager
    vmgr_->Configure();

    //add notification client on Voice Ui manager
    vmgr_->AddNotificationObserver(notification_observer_);
  }

  virtual void TearDown() {
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    vmgr_->Shutdown();
  }

  // Objects declared here can be used by all tests in the test case for VoiceUIManager.
};

// Test Successful creation of a Mock Full Solution Client by VoiceUIManager::CreateClient
TEST_F(VoiceUIManagerTest, MethodTestCreateFullSolutionClient) {
  syslog(LOG_INFO, "***** VoiceUIManagerTest::MethodTestCreateFullSolutionClient - START");

  auto client_id = VoiceUIClientID::MOCK_FULL_SOLUTION;
  bool wake_word_embedded = true;
  ASSERT_EQ(true, vmgr_->CreateClient(client_id, wake_word_embedded));
}

// Test Successful creation of a Mock Mofular Solution Client by VoiceUIManager::CreateClient
TEST_F(VoiceUIManagerTest, MethodTestCreateModularSolutionClient) {
  syslog(LOG_INFO, "***** VoiceUIManagerTest::MethodTestCreateModularSolutionClient - START");

  auto client_id = VoiceUIClientID::MOCK_MODULAR_SOLUTION;
  bool wake_word_embedded = true;
  ASSERT_EQ(true, vmgr_->CreateClient(client_id, wake_word_embedded));
}

// Test creation of a unknown client
TEST_F(VoiceUIManagerTest, MethodTestCreateClientUnknown) {
  syslog(LOG_INFO, "\n");
  syslog(LOG_INFO, "VoiceUIManagerTest::MethodTestCreateClientUnknown - START");

  int random_number;
  random_number = static_cast<int>(VoiceUIClientID::MOCK_FULL_SOLUTION) + 100;

  VoiceUIClientID client_id = static_cast<VoiceUIClientID>(random_number);
  bool wake_word_embedded = true;

  //Excercise base class function directly
  ASSERT_EQ(false, vmgr_->VoiceUIManager::CreateClient(client_id, wake_word_embedded));

  syslog(LOG_INFO, "\n");
  syslog(LOG_INFO, "VoiceUIManagerTest::MethodTestCreateClientUnknown - END");
}

// Test creation of duplicate client
TEST_F(VoiceUIManagerTest, MethodTestCreateDuplicatedClient) {
  syslog(LOG_INFO, "***** VoiceUIManagerTest::MethodTestCreateDuplicatedClient - START");

  auto client_id = VoiceUIClientID::MOCK_FULL_SOLUTION;
  bool wake_word_embedded = true;
  ASSERT_EQ(true, vmgr_->CreateClient(client_id, wake_word_embedded));

  //Try to create a duplicate client.
  wake_word_embedded = true;
  ASSERT_EQ(false, vmgr_->CreateClient(client_id, wake_word_embedded));
}

//Test Start Client
TEST_F(VoiceUIManagerTest, MethodTestStartClient) {
  syslog(LOG_INFO, "\n");
  syslog(LOG_INFO, "VoiceUIManagerTest::MethodTestStartClient - START");

  auto client_id = VoiceUIClientID::MOCK_FULL_SOLUTION;
  bool wake_word_embedded = true;

  //Try to start a client without creation
  ASSERT_EQ(false, vmgr_->StartClient(client_id));

  //Create a FS test client
  ASSERT_EQ(true, vmgr_->CreateClient(client_id, wake_word_embedded));
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  ASSERT_EQ(true, vmgr_->StartClient(client_id));

  syslog(LOG_INFO, "\n");
  syslog(LOG_INFO, "VoiceUIManagerTest::MethodTestStartClient - END");
}

//Test Stop Client
TEST_F(VoiceUIManagerTest, MethodTestStopClient) {
  syslog(LOG_INFO, "\n");
  syslog(LOG_INFO, "VoiceUIManagerTest::MethodTestStopClient - START");

  auto client_id = VoiceUIClientID::MOCK_FULL_SOLUTION;
  bool wake_word_embedded = true;

  //Try to start a client without creation
  ASSERT_EQ(false, vmgr_->StopClient(client_id));
  //Create a FS test client
  ASSERT_EQ(true, vmgr_->CreateClient(client_id, wake_word_embedded));
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  ASSERT_EQ(true, vmgr_->StartClient(client_id));
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  ASSERT_EQ(true, vmgr_->StopClient(client_id));

  syslog(LOG_INFO, "\n");
  syslog(LOG_INFO, "VoiceUIManagerTest::MethodTestStopClient - END");
}

// Tests that the VoiceUIManager::CreateClient() method returns true for multiple clients creation
TEST_F(VoiceUIManagerTest, MethodTestCreateMultipleClients) {
  syslog(LOG_INFO, "***** VoiceUIManagerTest::MethodTestCreateMultipleClients - START");

  auto client_id = VoiceUIClientID::MOCK_FULL_SOLUTION;
  bool wake_word_embedded = true;

  ASSERT_EQ(0, vmgr_->GetClientListCount());
  ASSERT_EQ(true, vmgr_->CreateClient(client_id, wake_word_embedded));
  ASSERT_EQ(1, vmgr_->GetClientListCount());

  client_id = VoiceUIClientID::MOCK_MODULAR_SOLUTION;
  wake_word_embedded = false;
  ASSERT_EQ(true, vmgr_->CreateClient(client_id, wake_word_embedded));
  ASSERT_EQ(2, vmgr_->GetClientListCount());

  client_id = VoiceUIClientID::MODULAR_SOLUTION;
  wake_word_embedded = false;
  ASSERT_EQ(true, vmgr_->VoiceUIManager::CreateClient(client_id, wake_word_embedded));
  ASSERT_EQ(3, vmgr_->GetClientListCount());

  client_id = VoiceUIClientID::AVS_SOLUTION;
  wake_word_embedded = false;
  ASSERT_EQ(false, vmgr_->CreateClient(client_id, wake_word_embedded));
  ASSERT_EQ(3, vmgr_->GetClientListCount());

  client_id = VoiceUIClientID::CORTANA_SOLUTION;
  wake_word_embedded = true;
  ASSERT_EQ(false, vmgr_->CreateClient(client_id, wake_word_embedded));
  ASSERT_EQ(3, vmgr_->GetClientListCount());

  client_id = VoiceUIClientID::GVA_SOLUTION;
  wake_word_embedded = false;
  ASSERT_EQ(false, vmgr_->CreateClient(client_id, wake_word_embedded));
  ASSERT_EQ(3, vmgr_->GetClientListCount());

  syslog(LOG_INFO, "\n");
  syslog(LOG_INFO, "VoiceUIManagerTest::MethodTestCreateMultipleClients - END");
}

//TODO: TestAllEvents
TEST_F(VoiceUIManagerTest, MethodTestClientObserverOnFullSolutionClient) {
  syslog(LOG_INFO, "***** VoiceUIManagerTest::MethodTestClientObserverOnFullSolutionClient - START");

  //Create a client
  auto client_id = VoiceUIClientID::MOCK_FULL_SOLUTION;
  bool wake_word_embedded = true;
  ASSERT_EQ(true, vmgr_->CreateClient(client_id, wake_word_embedded));

  std::shared_ptr<MockFullVoiceUIClient> client;
  //Allow client to initialise
  std::this_thread::sleep_for(std::chrono::milliseconds(500));

  client = std::dynamic_pointer_cast<MockFullVoiceUIClient>(vmgr_->GetClientFromClientId(client_id));
  IntentManagerEvent intent = IntentManagerEvent::VOL_UP_EVENT;
  client->SendIntentToVUIManager(intent);

  //TODO Update to googlemock
  //EXPECT_CALL(notification_observer_, NotificationReceived())              // #3
  //    .Times(AtLeast(1));
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  ASSERT_EQ(IntentManagerEventToString(IntentManagerEvent::VOL_UP_EVENT), notification_observer_->latest_event_string);

  syslog(LOG_INFO, "\n");
  syslog(LOG_INFO, "VoiceUIManagerTest::MethodTestClientObserverOnFullSolutionClient - END");
}

TEST_F(VoiceUIManagerTest, MethodTestClientObserverOnModularSolutionClient) {
  syslog(LOG_INFO, "***** VoiceUIManagerTest::MethodTestClientObserverOnModularSolutionClient - START");

  //Create a client
  auto client_id = VoiceUIClientID::MOCK_MODULAR_SOLUTION;
  bool wake_word_embedded = true;
  ASSERT_EQ(true, vmgr_->CreateClient(client_id, wake_word_embedded));

  std::shared_ptr<MockModularVoiceUIClient> client;
  //Allow client to initialise
  std::this_thread::sleep_for(std::chrono::milliseconds(500));

  client = std::dynamic_pointer_cast<MockModularVoiceUIClient>(vmgr_->GetClientFromClientId(client_id));
  IntentManagerEvent intent = IntentManagerEvent::VOL_UP_EVENT;
  client->SendIntentToVUIManager(intent);

  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  ASSERT_EQ(IntentManagerEventToString(IntentManagerEvent::VOL_UP_EVENT), notification_observer_->latest_event_string);

  syslog(LOG_INFO, "\n");
  syslog(LOG_INFO, "VoiceUIManagerTest::MethodTestClientObserverOnModularSolutionClient - END");
}

TEST_F(VoiceUIManagerTest, MethodTestIntentObserverOnFullSolutionClient) {
  syslog(LOG_INFO, "***** VoiceUIManagerTest::MethodTestIntentObserverOnFullSolutionClient - START");

  std::shared_ptr<MockFullVoiceUIClient> client;

  //Create a client
  auto client_id = VoiceUIClientID::MOCK_FULL_SOLUTION;
  bool wake_word_embedded = true;
  ASSERT_EQ(true, vmgr_->CreateClient(client_id, wake_word_embedded));
  //Allow client to initialise
  std::this_thread::sleep_for(std::chrono::milliseconds(500));

  //Mock receiving Volume down Event
  IntentManagerEvent intent = IntentManagerEvent::MIC_MUTE_EVENT;
  vmgr_->IntentReceived(intent);

  client = std::dynamic_pointer_cast<MockFullVoiceUIClient>(vmgr_->GetClientFromClientId(client_id));
  ASSERT_EQ(intent, client->latest_intent_received);

  syslog(LOG_INFO, "\n");
  syslog(LOG_INFO, "VoiceUIManagerTest::MethodTestIntentObserverOnFullSolutionClient - END");
}

TEST_F(VoiceUIManagerTest, MethodTestIntentObserverOnModularSolutionClient) {
  syslog(LOG_INFO, "***** VoiceUIManagerTest::MethodTestIntentObserverOnModularSolutionClient - START");

  std::shared_ptr<MockModularVoiceUIClient> client;

  //Create a client
  auto client_id = VoiceUIClientID::MOCK_MODULAR_SOLUTION;
  bool wake_word_embedded = true;
  ASSERT_EQ(true, vmgr_->CreateClient(client_id, wake_word_embedded));
  //Allow client to initialise
  std::this_thread::sleep_for(std::chrono::milliseconds(500));

  //Mock receiving Volume down Event
  IntentManagerEvent intent = IntentManagerEvent::MIC_MUTE_EVENT;
  vmgr_->IntentReceived(intent);

  client = std::dynamic_pointer_cast<MockModularVoiceUIClient>(vmgr_->GetClientFromClientId(client_id));
  ASSERT_EQ(intent, client->latest_intent_received);

  syslog(LOG_INFO, "\n");
  syslog(LOG_INFO, "VoiceUIManagerTest::MethodTestIntentObserverOnModularSolutionClient - END");
}

TEST_F(VoiceUIManagerTest, MethodTestIntentObserverOnMultipleClients) {
  syslog(LOG_INFO, "***** VoiceUIManagerTest::MethodTestIntentObserverOnMultipleClients - START");

  std::shared_ptr<MockFullVoiceUIClient> full_client;
  std::shared_ptr<MockModularVoiceUIClient> modular_client;
  bool wake_word_embedded = true;

  //Create  client1
  auto client_id1 = VoiceUIClientID::MOCK_FULL_SOLUTION;

  ASSERT_EQ(true, vmgr_->CreateClient(client_id1, wake_word_embedded));
  //Allow client to initialise
  std::this_thread::sleep_for(std::chrono::milliseconds(500));

  //Create Client 2
  auto client_id2 = VoiceUIClientID::MOCK_MODULAR_SOLUTION;
  ASSERT_EQ(true, vmgr_->CreateClient(client_id2, wake_word_embedded));
  //Allow client to initialise
  std::this_thread::sleep_for(std::chrono::milliseconds(500));

  //Mock receiving Volume down Event
  //TODO: Loop through all intents
  IntentManagerEvent intent = IntentManagerEvent::MIC_MUTE_EVENT;
  vmgr_->IntentReceived(intent);

  full_client = std::dynamic_pointer_cast<MockFullVoiceUIClient>(vmgr_->GetClientFromClientId(client_id1));
  ASSERT_EQ(intent, full_client->latest_intent_received);

  modular_client = std::dynamic_pointer_cast<MockModularVoiceUIClient>(vmgr_->GetClientFromClientId(client_id2));
  ASSERT_EQ(intent, modular_client->latest_intent_received);

  syslog(LOG_INFO, "\n");
  syslog(LOG_INFO, "VoiceUIManagerTest::MethodTestIntentObserverOnMultipleClients - END");
}

//Verfy that all events from client observer are received in application

TEST_F(VoiceUIManagerTest, MethodTestNotificationObserverOnVoiceUIManager) {
  syslog(LOG_INFO, "***** VoiceUIManagerTest::MethodTestNotificationObserverOnVoiceUIManager - START");

  //In Sequence

  vmgr_->NewSolutionStatus(VoiceUIClientID::MODULAR_SOLUTION, VUISolutionStatus::IDLE);
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  //Note a single space
  ASSERT_EQ(" NewSolutionStatus: " + VUISolutionStatusToString(VUISolutionStatus::IDLE), notification_observer_->latest_event_string);
  ASSERT_EQ(VoiceUIClientID::MODULAR_SOLUTION, notification_observer_->latest_client_id);

  vmgr_->NewSolutionStatus(VoiceUIClientID::MODULAR_SOLUTION, VUISolutionStatus::LISTENING);
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  ASSERT_EQ(" NewSolutionStatus: " + VUISolutionStatusToString(VUISolutionStatus::LISTENING), notification_observer_->latest_event_string);
  ASSERT_EQ(VoiceUIClientID::MODULAR_SOLUTION, notification_observer_->latest_client_id);

  vmgr_->NewSolutionStatus(VoiceUIClientID::MODULAR_SOLUTION, VUISolutionStatus::BUSY);
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  ASSERT_EQ(" NewSolutionStatus: " + VUISolutionStatusToString(VUISolutionStatus::BUSY), notification_observer_->latest_event_string);
  ASSERT_EQ(VoiceUIClientID::MODULAR_SOLUTION, notification_observer_->latest_client_id);

  vmgr_->NewSolutionStatus(VoiceUIClientID::MODULAR_SOLUTION, VUISolutionStatus::SPEAKING);
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  ASSERT_EQ(" NewSolutionStatus: " + VUISolutionStatusToString(VUISolutionStatus::SPEAKING), notification_observer_->latest_event_string);
  ASSERT_EQ(VoiceUIClientID::MODULAR_SOLUTION, notification_observer_->latest_client_id);

  vmgr_->NewSolutionStatus(VoiceUIClientID::MODULAR_SOLUTION, VUISolutionStatus::INVALID);
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  ASSERT_EQ(" NewSolutionStatus: " + VUISolutionStatusToString(VUISolutionStatus::INVALID), notification_observer_->latest_event_string);
  ASSERT_EQ(VoiceUIClientID::MODULAR_SOLUTION, notification_observer_->latest_client_id);

  vmgr_->MuteStatus(VoiceUIClientID::MODULAR_SOLUTION, true);
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  ASSERT_EQ(" MuteStatus", notification_observer_->latest_event_string);
  ASSERT_EQ(VoiceUIClientID::MODULAR_SOLUTION, notification_observer_->latest_client_id);

  vmgr_->KeywordDetected(VoiceUIClientID::MODULAR_SOLUTION);
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  ASSERT_EQ(" KeywordDetected", notification_observer_->latest_event_string);
  ASSERT_EQ(VoiceUIClientID::MODULAR_SOLUTION, notification_observer_->latest_client_id);

  vmgr_->DirectionOfArrival(VoiceUIClientID::MODULAR_SOLUTION, 65);
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  //TODO: Value of DoA
  ASSERT_EQ(" DirectionOfArrival", notification_observer_->latest_event_string);
  ASSERT_EQ(VoiceUIClientID::MODULAR_SOLUTION, notification_observer_->latest_client_id);

  vmgr_->VolumeStatus(VoiceUIClientID::MODULAR_SOLUTION, 77);
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  //TODO: Value of Volume
  ASSERT_EQ(" VolumeStatus", notification_observer_->latest_event_string);
  ASSERT_EQ(VoiceUIClientID::MODULAR_SOLUTION, notification_observer_->latest_client_id);

  vmgr_->ActiveStatus(VoiceUIClientID::MODULAR_SOLUTION, true);
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  //TODO: Value of Volume
  ASSERT_EQ(" ActiveStatus", notification_observer_->latest_event_string);
  ASSERT_EQ(VoiceUIClientID::MODULAR_SOLUTION, notification_observer_->latest_client_id);

  syslog(LOG_INFO, "***** VoiceUIManagerTest::MethodTestNotificationObserverOnVoiceUIManager - END");
}
TEST_F(VoiceUIManagerTest, MethodTestNotificationObserverMultipleOnVoiceUIManager) {
  std::shared_ptr<MockNotificationObserver> notification_observer_1_;
  std::shared_ptr<MockNotificationObserver> notification_observer_2_;

  notification_observer_1_ = std::make_shared<MockNotificationObserver>();
  vmgr_->AddNotificationObserver(notification_observer_1_);
  notification_observer_2_ = std::make_shared<MockNotificationObserver>();
  vmgr_->AddNotificationObserver(notification_observer_2_);

  syslog(LOG_INFO, "***** VoiceUIManagerTest::MethodTestNotificationObserverMultipleOnVoiceUIManager - START");

  vmgr_->NewSolutionStatus(VoiceUIClientID::MODULAR_SOLUTION, VUISolutionStatus::IDLE);
  std::this_thread::sleep_for(std::chrono::milliseconds(500));

  //Note a single space
  //notification_observer_
  ASSERT_EQ(" NewSolutionStatus: " + VUISolutionStatusToString(VUISolutionStatus::IDLE), notification_observer_->latest_event_string);
  ASSERT_EQ(VoiceUIClientID::MODULAR_SOLUTION, notification_observer_->latest_client_id);
  //notification_observer_1_
  ASSERT_EQ(" NewSolutionStatus: " + VUISolutionStatusToString(VUISolutionStatus::IDLE), notification_observer_1_->latest_event_string);
  ASSERT_EQ(VoiceUIClientID::MODULAR_SOLUTION, notification_observer_1_->latest_client_id);
  //notification_observer_2_
  ASSERT_EQ(" NewSolutionStatus: " + VUISolutionStatusToString(VUISolutionStatus::IDLE), notification_observer_2_->latest_event_string);
  ASSERT_EQ(VoiceUIClientID::MODULAR_SOLUTION, notification_observer_2_->latest_client_id);

  syslog(LOG_INFO, "***** VoiceUIManagerTest::MethodTestNotificationObserverMultipleOnVoiceUIManager - END");
}

TEST_F(VoiceUIManagerTest, MethodTestInternetStatusForMultipleClients) {
  syslog(LOG_INFO, "***** VoiceUIManagerTest::MethodTestInternetStatusForMultipleClients - START");

  std::shared_ptr<MockFullVoiceUIClient> full_client;
  std::shared_ptr<MockModularVoiceUIClient> modular_client;
  bool wake_word_embedded = true;

  //Create  client1
  auto client_id1 = VoiceUIClientID::MOCK_FULL_SOLUTION;
  ASSERT_EQ(true, vmgr_->CreateClient(client_id1, wake_word_embedded));
  //Create Client 2
  auto client_id2 = VoiceUIClientID::MOCK_MODULAR_SOLUTION;
  ASSERT_EQ(true, vmgr_->CreateClient(client_id2, wake_word_embedded));

  //Allow client to initialise and wait for internet check status
  std::this_thread::sleep_for(std::chrono::milliseconds(1000));

  full_client = std::dynamic_pointer_cast<MockFullVoiceUIClient>(vmgr_->GetClientFromClientId(client_id1));
  if (CheckForInternet()) {
    ASSERT_EQ(AppMessage::INTERNET_UP, full_client->latest_app_message_received);
  } else {
    ASSERT_EQ(AppMessage::INTERNET_DOWN, full_client->latest_app_message_received);
  }

  modular_client = std::dynamic_pointer_cast<MockModularVoiceUIClient>(vmgr_->GetClientFromClientId(client_id2));
  if (CheckForInternet()) {
    ASSERT_EQ(AppMessage::INTERNET_UP, modular_client->latest_app_message_received);
  } else {
    ASSERT_EQ(AppMessage::INTERNET_DOWN, modular_client->latest_app_message_received);
  }

  syslog(LOG_INFO, "\n");
  syslog(LOG_INFO, "VoiceUIManagerTest::MethodTestInternetStatusForMultipleClients - END");
}


// Tests that the VoiceUIManager::CreateClient() for real Clients
TEST_F(VoiceUIManagerTest, MethodTestCreateMultipleRealClients) {
  syslog(LOG_INFO, "***** VoiceUIManagerTest::MethodTestCreateMultipleRealClients - START");

  //Adding AVS as a Client
  auto client_id = VoiceUIClientID::MODULAR_SOLUTION;
  bool wake_word_embedded = false;
  ASSERT_EQ(0, vmgr_->GetClientListCount());
  ASSERT_EQ(true, vmgr_->VoiceUIManager::CreateClient(client_id, wake_word_embedded));
  ASSERT_EQ(1, vmgr_->GetClientListCount());

  //TODO: Add Modular Client, GVA, CORTANA, and others

  //Adding a MOCK client as a Client - NOT ALLOWED
  client_id = VoiceUIClientID::MOCK_FULL_SOLUTION;
  wake_word_embedded = false;
  ASSERT_EQ(1, vmgr_->GetClientListCount());
  ASSERT_EQ(false, vmgr_->VoiceUIManager::CreateClient(client_id, wake_word_embedded));
  ASSERT_EQ(1, vmgr_->GetClientListCount());

  syslog(LOG_INFO, "\n");
  syslog(LOG_INFO, "VoiceUIManagerTest::MethodTestCreateMultipleRealClients - END");
}

}  // namespace

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
