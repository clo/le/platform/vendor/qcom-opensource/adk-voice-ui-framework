/* Copyright (c) 2018, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    VoiceUIManagerStub.h
 *  @brief   Mock Classes for All dependent classes for test
 *
 ***************************************************************/

#ifndef VOICEUIFRAMEWORK_VOICEUIMANAGER_INCLUDE_VOICEUIFWSTUB_H_
#define VOICEUIFRAMEWORK_VOICEUIMANAGER_INCLUDE_VOICEUIFWSTUB_H_

#include <IntentManager/IntentObserver.h>
#include <IntentManager/VoiceUIIntentManager.h>
#include <VoiceUIClient/BaseSolution/ClientObserver.h>
#include <VoiceUIClient/FullSolution/FullVoiceUIClient.h>
#include <VoiceUIClient/ModularSolution/ModularVoiceUIClient.h>
#include <VoiceUIManager/NotificationObserver.h>
#include <VoiceUIManager/VoiceUIManager.h>
#include <VoiceUIUtils/GlobalValues.h>

using namespace std;
using namespace voiceUIFramework::voiceUIIntentManager;
using namespace voiceUIFramework::voiceUIUtils;
using namespace voiceUIFramework::voiceUIClient;
using namespace voiceUIFramework::voiceUIManager;

bool CheckForInternet() {
  CURL *curl = NULL;
  CURLcode ret;
  curl = curl_easy_init();
  if (curl) {
    curl_easy_setopt(curl, CURLOPT_URL, "www.developer.amazon.com");
    curl_easy_setopt(curl, CURLOPT_NOBODY, 1);

    ret = curl_easy_perform(curl);
    if (ret == CURLE_OK) {
      //Send
      return true;
    } else {
      return false;
    }
  }
  curl_easy_cleanup(curl);
}

namespace voiceUIFramework {
namespace voiceUIFrameworkStub {

class MockFullClientManager : public FullClientManager {
 public:
  MockFullClientManager(
      VoiceUIClientID client_id,
      bool wake_word_embedded,
      std::shared_ptr<voiceUIFramework::voiceUIClient::ClientObserver> client_observer)
      : FullClientManager(client_id, wake_word_embedded, client_observer) {
  }

  ~MockFullClientManager() {
  }

  //Override to remove dependency
  bool Initialize() {
    syslog(LOG_INFO, "MockFullClientManager::Initialize()");
    return true;
  }
};

class MockFullVoiceUIClient : public FullVoiceUIClient {
 public:
  IntentManagerEvent latest_intent_received;
  IntentManagerEvent latest_intent_received_ext;

  AppMessage latest_app_message_received;

  MockFullVoiceUIClient(
      std::shared_ptr<voiceUIFramework::voiceUIClient::ClientObserver> client_observer,
      voiceUIFramework::voiceUIUtils::VoiceUIClientID clientID,
      bool wake_word_embedded)
      : FullVoiceUIClient(client_observer, clientID, wake_word_embedded), latest_intent_received{IntentManagerEvent::UNKNOWN_INTENT}, latest_intent_received_ext{IntentManagerEvent::UNKNOWN_INTENT}, latest_app_message_received{AppMessage::UNKNOWN_MESSAGE} {
    base_client_manager_ = std::make_shared<MockFullClientManager>(clientID, wake_word_embedded, client_observer);
  }

  ~MockFullVoiceUIClient() {
  }

  //From VoiceUI Client to to Client Observer to VUI Manager to Notification observer
  bool SendIntentToVUIManager(IntentManagerEvent intent) {
    syslog(LOG_INFO, "MockFullVoiceUIClient::SendIntentToVUIManager - intent : %s", IntentManagerEventToString(intent).c_str());
    //Send Intent to test Voice UI Manager
    base_client_manager_->SendIntent(intent);
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    return true;
  }
  bool SendIntentExtToVUIManager(IntentManagerEvent intent, IntentManagerEventExtended intent_ext) {
    syslog(LOG_INFO, "MockFullVoiceUIClient::SendIntentExtToVUIManager - intent : %s", IntentManagerEventToString(intent).c_str());
    //Send Intent to test Voice UI Manager
    base_client_manager_->SendIntent(intent, intent_ext);
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    return true;
  }

  //Override function to receive intents in Mock
  void IntentReceived(IntentManagerEvent intent) {
    syslog(LOG_INFO, "MockFullVoiceUIClient::IntentReceived - intent Received: %s", IntentManagerEventToString(intent).c_str());
    latest_intent_received = intent;
  }

  void IntentReceived(IntentManagerEvent intent, IntentManagerEvent intent_ext) {
    syslog(LOG_INFO, "MockFullVoiceUIClient::IntentReceivedExtended - intent Received: %s", IntentManagerEventToString(intent).c_str());
    latest_intent_received = intent;
    latest_intent_received_ext = intent_ext;
  }

  void AppMessageReceived(AppMessage app_message) {
    syslog(LOG_INFO, "MockFullVoiceUIClient::AppMessageReceived() = %s", AppMessageToString(app_message).c_str());
    latest_app_message_received = app_message;
  }
};

class MockModularClientManager : public ModularClientManager {
 public:
  MockModularClientManager(
      VoiceUIClientID client_id,
      bool wake_word_embedded,
      std::shared_ptr<voiceUIFramework::voiceUIClient::ClientObserver> client_observer)
      : ModularClientManager(client_id, wake_word_embedded, client_observer) {
  }

  ~MockModularClientManager() {
  }

  //Override to remove dependency
  bool Initialize() {
    syslog(LOG_INFO, "MockModularClientManager::Initialize()");
    return true;
  }
};

class MockModularVoiceUIClient : public ModularVoiceUIClient {
 public:
  IntentManagerEvent latest_intent_received;
  IntentManagerEventExtended latest_intent_received_ext;
  AppMessage latest_app_message_received;

  MockModularVoiceUIClient(
      std::shared_ptr<voiceUIFramework::voiceUIClient::ClientObserver> client_observer,
      voiceUIFramework::voiceUIUtils::VoiceUIClientID clientID,
      bool wake_word_embedded)
      : ModularVoiceUIClient(client_observer, clientID, wake_word_embedded), latest_intent_received{IntentManagerEvent::UNKNOWN_INTENT}, latest_app_message_received{AppMessage::UNKNOWN_MESSAGE} {
    base_client_manager_ = std::make_shared<MockModularClientManager>(clientID, wake_word_embedded, client_observer);
  }

  ~MockModularVoiceUIClient() {
  }

  //From VoiceUI Client to to Client Observer to VUI Manager to Notification observer
  bool SendIntentToVUIManager(IntentManagerEvent intent) {
    syslog(LOG_INFO, "MockModularVoiceUIClient::SendIntentToVUIManager - intent : %s", IntentManagerEventToString(intent).c_str());
    //Send Intent to test Voice UI Manager
    base_client_manager_->SendIntent(intent);
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    return true;
  }

  bool SendIntentExtToVUIManager(IntentManagerEvent intent, IntentManagerEventExtended intent_ext) {
    syslog(LOG_INFO, "MockModularVoiceUIClient::SendIntentExtToVUIManager - intent : %s", IntentManagerEventToString(intent).c_str());
    //Send Intent to test Voice UI Manager
    base_client_manager_->SendIntent(intent, intent_ext);
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    return true;
  }

  //Override function to receive intents in Mock
  void IntentReceived(IntentManagerEvent intent) {
    syslog(LOG_INFO, "MockModularVoiceUIClient::IntentReceived - intent Received: %s", IntentManagerEventToString(intent).c_str());
    latest_intent_received = intent;
  }
  void IntentReceived(IntentManagerEvent intent, IntentManagerEventExtended intent_, IntentManagerEventExtended intent_ext_) {
    syslog(LOG_INFO, "MockModularVoiceUIClient::IntentReceived - intent Received: %s", IntentManagerEventToString(intent).c_str());
    latest_intent_received = intent;
    latest_intent_received_ext = intent_ext_;
  }

  void AppMessageReceived(AppMessage app_message) {
    syslog(LOG_INFO, "MockModularVoiceUIClient::AppMessageReceived() = %s", AppMessageToString(app_message).c_str());
    latest_app_message_received = app_message;
  }
};

class MockNotificationObserver : public NotificationObserver {
 public:
  VoiceUIClientID latest_client_id;
  std::string latest_event_string;

  ~MockNotificationObserver(){};

  //MOCK_METHOD2(NotificationReceived, void(VoiceUIClientID client_id, std::string event));

  void NotificationReceived(VoiceUIClientID client_id, std::string event) {
    syslog(LOG_INFO, "MockNotificationObserver::NotificationReceived - Client: %s - Event: %s", ClientIDToString(client_id).c_str(),
        event.c_str());
    latest_event_string = event;
    latest_client_id = client_id;
  }
};

}  // namespace voiceUIFrameworkStub
}  // namespace voiceUIFramework
#endif
