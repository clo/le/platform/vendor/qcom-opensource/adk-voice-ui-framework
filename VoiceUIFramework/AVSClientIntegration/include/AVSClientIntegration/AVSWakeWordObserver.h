/* Copyright (c) 2018, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    AVSWakeWordObserver.h
 *  @brief   Implements WakeWord Callbacks for AVS solution
 *
 *  DESCRIPTION
 *    Implements WakeWord Callbacks for AVS solution
 ***************************************************************/

#ifndef VOICEUIFRAMEWORK_AVSCLIENTINTEGRATION_INCLUDE_AVSCLIENTINTEGRATION_AVSWAKEWORDOBSERVER_H_
#define VOICEUIFRAMEWORK_AVSCLIENTINTEGRATION_INCLUDE_AVSCLIENTINTEGRATION_AVSWAKEWORDOBSERVER_H_

#include <limits.h>
#include <syslog.h>
#include <cmath>
#include <cstdio>
#include <iostream>
#include <memory>
#include <string>

#include <AIP/ASRProfile.h>
#include <AIP/AudioInputProcessor.h>
#include <AVSCommon/AVS/AudioInputStream.h>
#include <AVSCommon/SDKInterfaces/KeyWordObserverInterface.h>
#include <DefaultClient/DefaultClient.h>

#include <VoiceUIUtils/VoiceUIConfig.h>
#include <Wakeword/WakeWordCallBackInterface.h>
#include <Wakeword/WakeWordInterface.h>

#include <adk/wakelock.h>

namespace voiceUIFramework {
namespace avsManager {

using namespace alexaClientSDK;
using namespace voiceUIFramework::wakeword;

class AVSWakeWordObserver : public voiceUIFramework::wakeword::WakeWordCallBackInterface {
 public:
  AVSWakeWordObserver(std::shared_ptr<alexaClientSDK::avsCommon::avs::AudioInputStream> stream,
      std::shared_ptr<alexaClientSDK::defaultClient::DefaultClient> client, std::shared_ptr<voiceUIFramework::wakeword::WakeWordInterface> wakeword);
  virtual ~AVSWakeWordObserver();

  /* WakeWordCallBackInterface */
  // Observer client is added Successfully (Sound model Loaded Successfully)
  WakewordCallbackRc ObserverAdded();
  // Observer client is removed Successfully (Sound model Unloaded Successfully)
  WakewordCallbackRc ObserverRemoved();
  // Recognition is scheduled, STHAL is recognizing keywords of this client
  WakewordCallbackRc StartRecognitionComplete();
  // Callback to transfer LAB Data from Buffers in WakeWord to the client
  WakewordCallbackRc OnNewVoiceBufferReceived(void* buf, size_t size);
  // Callback to notify detection of keyword
  WakewordCallbackRc OnKeywordDetected(voiceUIFramework::wakeword::KwdDetectionResults res);
  // Callback to notify Stop Recognition
  WakewordCallbackRc StopRecognitionComplete();
  // Callback to notify Stop Audio Capture
  WakewordCallbackRc StopAudioCaptureComplete();

  // Call back to notify doa or channel Index
  WakewordCallbackRc SoundTriggerDataReceived(
      voiceUIFramework::wakeword::SoundTriggerData type,
      int value);

 private:
  int create_kw_lab_stream();

  std::shared_ptr<alexaClientSDK::avsCommon::avs::AudioInputStream> stream_;
  std::weak_ptr<alexaClientSDK::defaultClient::DefaultClient> client_;
  std::unique_ptr<alexaClientSDK::avsCommon::avs::AudioInputStream::Writer> audioBufferWriter_;
  alexaClientSDK::avsCommon::utils::AudioFormat compatible_audio_format_;

  //voice dump file
  FILE* log_filename_;
  bool voicedump_enable_;
  std::string voicedump_path_;
  std::shared_ptr<voiceUIFramework::wakeword::WakeWordInterface> wakeword_;
};

} /* namespace avsManager */
}  // namespace voiceUIFramework

#endif /* VOICEUIFRAMEWORK_AVSCLIENTINTEGRATION_INCLUDE_AVSCLIENTINTEGRATION_AVSWAKEWORDOBSERVER_H_ */
