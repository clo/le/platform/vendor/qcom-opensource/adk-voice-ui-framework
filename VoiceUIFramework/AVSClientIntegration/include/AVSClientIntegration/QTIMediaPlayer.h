/* Copyright (c) 2018, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    QTIMediaPlayer.h
 *  @brief
 *
 *  DESCRIPTION
 *
 ***************************************************************/

#ifndef CODE_VOICEUIFRAMEWORK_QTIMediaPlayer_INCLUDE_QTIMediaPlayer_QTIMediaPlayer_H_
#define CODE_VOICEUIFRAMEWORK_QTIMediaPlayer_INCLUDE_QTIMediaPlayer_QTIMediaPlayer_H_

#include <chrono>
#include <cmath>
#include <cstdint>
#include <cstring>
#include <functional>
#include <future>
#include <memory>
#include <string>
#include <thread>
#include <unordered_map>

#include <AVSCommon/AVS/Attachment/AttachmentReader.h>
#include <AVSCommon/AVS/SpeakerConstants/SpeakerConstants.h>
#include <AVSCommon/SDKInterfaces/Audio/EqualizerInterface.h>
#include <AVSCommon/SDKInterfaces/HTTPContentFetcherInterfaceFactoryInterface.h>
#include <AVSCommon/SDKInterfaces/SpeakerInterface.h>
#include <AVSCommon/Utils/MediaPlayer/MediaPlayerInterface.h>
#include <AVSCommon/Utils/MediaPlayer/MediaPlayerObserverInterface.h>
#include <AVSCommon/Utils/Memory/Memory.h>
#include <MediaPlayer/AttachmentReaderSource.h>
#include <MediaPlayer/ErrorTypeConversion.h>
#include <MediaPlayer/IStreamSource.h>
#include <MediaPlayer/Normalizer.h>
#include <MediaPlayer/OffsetManager.h>
#include <PlaylistParser/PlaylistParser.h>
#include <PlaylistParser/UrlContentToAttachmentConverter.h>

#include <VoiceUIUtils/GlobalValues.h>

#include <adk/mediaplayer.h>
#include <adk/wakelock.h>
#include <syslog.h>
//serialisation using Gobject queueCallback
#include <glib.h>

namespace alexaClientSDK {
namespace mediaPlayer {

typedef std::vector<avsCommon::utils::mediaPlayer::MediaPlayerObserverInterface::TagKeyValueType> VectorOfTags;

/**
 * Class that handles creation of audio pipeline and playing of audio data.
 */
class QTIMediaPlayer
    : public avsCommon::utils::RequiresShutdown,
      public avsCommon::utils::mediaPlayer::MediaPlayerInterface,
      public avsCommon::sdkInterfaces::SpeakerInterface,
      public avsCommon::sdkInterfaces::audio::EqualizerInterface,
      public playlistParser::UrlContentToAttachmentConverter::ErrorObserverInterface,
      public std::enable_shared_from_this<QTIMediaPlayer> {
 public:
  /**
     * Creates an instance of the @c MediaPlayer.
     *
     * @param contentFetcherFactory Used to create objects that can fetch remote HTTP content.
     * @param enableEqualizer Flag, indicating whether equalizer should be enabled for this instance.
     * @param type The type used to categorize the speaker for volume control.
     * @param name Readable name for the new instance.
     * @param enableLiveMode Flag, indicating if the player is in live mode.
     * @return An instance of the @c MediaPlayer if successful else a @c nullptr.
     */
  static std::shared_ptr<QTIMediaPlayer> create(
      std::shared_ptr<avsCommon::sdkInterfaces::HTTPContentFetcherInterfaceFactoryInterface> contentFetcherFactory =
          nullptr,
      bool enableEqualizer = false,
      avsCommon::sdkInterfaces::SpeakerInterface::Type type =
          avsCommon::sdkInterfaces::SpeakerInterface::Type::AVS_SPEAKER_VOLUME,
      std::string name = "",
      bool enableLiveMode = false);

  //Destructor
  ~QTIMediaPlayer();

  /// @name Overridden EqualizerInterface methods.
  /// @{
  void setEqualizerBandLevels(avsCommon::sdkInterfaces::audio::EqualizerBandLevelMap bandLevelMap) override;
  int getMinimumBandLevel() override;
  int getMaximumBandLevel() override;
  /// }@

  /// @name Overridden MediaPlayerInterface#endif methods.
  /// @{
  SourceId setSource(
      std::shared_ptr<avsCommon::avs::attachment::AttachmentReader> attachmentReader,
      const avsCommon::utils::AudioFormat* format = nullptr) override;
  SourceId setSource(std::shared_ptr<std::istream> stream, bool repeat) override;
  SourceId setSource(const std::string& url, std::chrono::milliseconds offset = std::chrono::milliseconds::zero(), bool repeat = false)
      override;

  bool play(SourceId id) override;
  bool stop(SourceId id) override;
  bool pause(SourceId id) override;
  /**
     * To resume playback after a pause, call @c resume. Calling @c play
     * will reset the pipeline and source, and will not resume playback.
     */
  bool resume(SourceId id) override;
  uint64_t getNumBytesBuffered() override;
  std::chrono::milliseconds getOffset(SourceId id) override;
  void setObserver(std::shared_ptr<avsCommon::utils::mediaPlayer::MediaPlayerObserverInterface> observer) override;
  /// @}

  /// @name Overridden SpeakerInterface methods.
  /// @{
  bool setVolume(int8_t volume) override;
  bool adjustVolume(int8_t volume) override;
  bool setMute(bool mute) override;
  bool getSpeakerSettings(avsCommon::sdkInterfaces::SpeakerInterface::SpeakerSettings* settings) override;
  avsCommon::sdkInterfaces::SpeakerInterface::Type getSpeakerType() override;
  /// @}

  /// @name Overriden UrlContentToAttachmentConverter::ErrorObserverInterface methods.
  /// @{
  void onError() override;
  /// @}

  void doShutdown() override;

  void SetNextCb(std::function<void(void)> onNextCb);

  void SetPreviousCb(std::function<void(void)> onPreviousCb);

  void SetPlayCb(std::function<void(void)> onPlayCb);

  void SetPauseCb(std::function<void(void)> onPauseCb);

  void SetOnMuteUpdatedCb(std::function<void(bool mute_state)> onMuteUpdatedCb);

  void SetOnVolumeUpdatedCb(std::function<void(double volume)> onVolumeUpdatedCb);

 private:
  /**
   * Constructor.
   *
   * @param contentFetcherFactory Used to create objects that can fetch remote HTTP content.
   * @param enableEqualizer Flag, indicating whether equalizer should be enabled for this instance.
   * @param type The type used to categorize the speaker for volume control.
   * @param name Readable name of this instance.
   * @param enableLiveMode Flag, indicating the player is in live mode
   */
  QTIMediaPlayer(
      std::shared_ptr<avsCommon::sdkInterfaces::HTTPContentFetcherInterfaceFactoryInterface> contentFetcherFactory,
      bool enableEqualizer,
      avsCommon::sdkInterfaces::SpeakerInterface::Type type,
      std::string name,
      bool enableLiveMode);

  /**
     * Initializes GStreamer and starts a main event loop on a new thread.
     *
     * @return @c SUCCESS if initialization was successful. Else @c FAILURE.
     */
  bool init(std::string type = "");

  /**
     * Stops the currently playing audio and removes the transient elements.  The transient elements
     * are appsrc and decoder.
     */
  void tearDownTransientPipelineElements(bool notifyStop);

  /**
     * Send tags that are found in the stream to the observer.
     *
     * @param vectorOfTags Vector containing tags that are found in the stream.
     */
  void sendStreamTagsToObserver(std::unique_ptr<const VectorOfTags> vectorOfTags);

  /**
   * Worker thread handler for setting the source of audio to play.
   *
   * @param reader The @c AttachmentReader with which to receive the audio to play.
   * @param promise A promise to fulfill with a @c SourceId value once the source has been set.
   * @param audioFormat The audioFormat to be used to interpret raw audio data.
   * @param repeat An optional parameter to indicate whether to play from the source in a loop.
   */
  void handleSetAttachmentReaderSource(std::shared_ptr<avsCommon::avs::attachment::AttachmentReader> reader, std::promise<SourceId>* promise, const avsCommon::utils::AudioFormat* audioFormat = nullptr, bool repeat = false);

  /**
   * Worker thread handler for setting the source of audio to play.
   *
   * @param url The url to set as the source.
   * @param offset The offset from which to start streaming from.
   * @param promise A promise to fulfill with a @c SourceId value once the source has been set.
   * @param repeat A parameter to indicate whether to play from the url source in a loop.
   */
  void handleSetUrlSource(const std::string& url, std::chrono::milliseconds offset, std::promise<SourceId>* promise, bool repeat);

  /**
     * Worker thread handler for setting the source of audio to play.
     *
     * @param stream The source from which to receive the audio to play.
     * @param repeat Whether the audio stream should be played in a loop until stopped.
     * @param promise A promise to fulfill with a @ SourceId value once the source has been set.
     */
  void handleSetIStreamSource(std::shared_ptr<std::istream> stream, bool repeat, std::promise<SourceId>* promise);
  /**
     * Worker thread handler for starting playback of the current audio source.
     *
     * @param id The @c SourceId that the caller is expecting to be handled.
     * @param promise A promise to fulfill with a @c bool value once playback has been initiated
     * (or the operation has failed).
     */

  /**
     * Internal method to update the volume according to a gstreamer bug fix
     * https://bugzilla.gnome.org/show_bug.cgi?id=793081
     * @param gstVolume a volume to be set to GStreamer
     */
  void handleSetVolumeInternal(double gstVolume);

  /**
     * Worker thread handler for setting the volume.
     *
     * @param promise The promise to be set indicating the success of the operation.
     * @param volume The absolute volume.
     */
  void handleSetVolume(std::promise<bool>* promise, int8_t volume);

  /**
     * Worker thread handler for adjusting the volume.
     *
     * @param promise The promise to be set indicating the success of the operation.
     * @param delta The volume change.
     */
  void handleAdjustVolume(std::promise<bool>* promise, int8_t delta);

  /**
     * Worker thread handler for setting the mute.
     *
     * @param promise The promise to be set indicating the success of the operation.
     * @param mute The mute setting. True for mute and false for unmute.
     */
  void handleSetMute(std::promise<bool>* promise, bool mute);

  /**
     * Worker thread handler for getting the @c SpeakerSettings.
     *
     * @param promise The promise to be set indicating the success of the operation.
     * @param settings The current @c SpeakerSettings.
     */
  void handleGetSpeakerSettings(
      std::promise<bool>* promise,
      avsCommon::sdkInterfaces::SpeakerInterface::SpeakerSettings* settings);

  /**
     * Worker thread handler for starting playback of the current audio source.
     *
     * @param id The @c SourceId that the caller is expecting to be handled.
     * @param promise A promise to fulfill with a @c bool value once playback has been initiated
     * (or the operation has failed).
     */
  void handlePlay(SourceId id, std::promise<bool>* promise);

  /**
     * Worker thread handler for stopping audio playback.
     *
     * @param id The @c SourceId that the caller is expecting to be handled.
     * @param promise A promise to fulfill with a @c bool once playback stop has been initiated
     * (or the operation has failed).
     */
  void handleStop(SourceId id, std::promise<bool>* promise);

  /**
     * Worker thread handler for pausing playback of the current audio source.
     *
     * @param id The @c SourceId that the caller is expecting to be handled.
     * @param promise A promise to fulfill with a @c bool value once playback has been paused
     * (or the operation has failed).
     */
  void handlePause(SourceId id, std::promise<bool>* promise);

  /**
     * Worker thread handler for resume playback of the current audio source.
     *
     * @param id The @c SourceId that the caller is expecting to be handled.
     * @param promise A promise to fulfill with a @c bool value once playback has been resumed
     * (or the operation has failed).
     */
  void handleResume(SourceId id, std::promise<bool>* promise);

  /**
     * Worker thread handler for getting the current playback position.
     *
     * @param id The @c SourceId that the caller is expecting to be handled.
     * @param promise A promise to fulfill with the offset once the value has been determined.
     */
  void handleGetOffset(SourceId id, std::promise<std::chrono::milliseconds>* promise);

  /**
     * Worker thread handler for setting the observer.
     *
     * @param promise A void promise to fulfill once the observer has been set.
     * @param observer The new observer.
     */
  void handleSetObserver(
      std::promise<void>* promise,
      std::shared_ptr<avsCommon::utils::mediaPlayer::MediaPlayerObserverInterface> observer);

  /**
     * Sends the playback started notification to the observer.
     */
  void sendPlaybackStarted();

  /**
     * Sends the playback finished notification to the observer.
     */
  void sendPlaybackFinished();

  /**
     * Sends the playback paused notification to the observer.
     */
  void sendPlaybackPaused();

  /**
     * Sends the playback resumed notification to the observer.
     */
  void sendPlaybackResumed();

  /**
     * Sends the playback stopped notification to the observer.
     */
  void sendPlaybackStopped();

  /**
     * Sends the playback error notification to the observer.
     *
     * @param type The error type.
     * @param error The error details.
     */
  void sendPlaybackError(
      const alexaClientSDK::avsCommon::utils::mediaPlayer::ErrorType& type,
      const std::string& error);

  /**
     * Sends the buffer underrun notification to the observer.
     */
  void sendBufferUnderrun();
  /**
     * Sends the buffer refilled notification to the observer.
     */
  void sendBufferRefilled();
  /**
     * Used to obtain seeking information about the pipeline.
     *
     * @param isSeekable A boolean indicating whether the stream is seekable.
     * @return A boolean indicating whether the operation was successful.
     */
  bool queryIsSeekable(bool* isSeekable);

  /**
     * Performs a seek to the @c seekPoint.
     *
     * @return A boolean indicating whether the seek operation was successful.
     */
  bool seek();

  /**
     * Validates that the given id matches the current source id being operated on.
     *
     * @param id The id to validate
     * @return @c true if the id matches the source being operated on and @c false otherwise.
     */
  bool validateSourceAndId(SourceId id);

  /**
   * Get the current offset of the stream.
   *
   * @return The current stream offset in milliseconds.
   */
  std::chrono::milliseconds getCurrentStreamOffset();

  /**
     * Destructs the @c m_source with proper steps.
     */
  void cleanUpSource();

  /// Used to stream urls into attachments
  std::shared_ptr<playlistParser::UrlContentToAttachmentConverter> m_urlConverter;

  /// An instance of the @c OffsetManager.
  OffsetManager m_offsetManager;

  /// Used to create objects that can fetch remote HTTP content.
  std::shared_ptr<avsCommon::sdkInterfaces::HTTPContentFetcherInterfaceFactoryInterface> m_contentFetcherFactory;

  /// Flag indicating if equalizer is enabled for this Media Player
  bool m_equalizerEnabled;

  /// The Speaker type.
  avsCommon::sdkInterfaces::SpeakerInterface::Type m_speakerType;

  /// Flag to indicate when a playback started notification has been sent to the observer.
  bool m_playbackStartedSent;

  /// Flag to indicate when a playback finished notification has been sent to the observer.
  bool m_playbackFinishedSent;

  /// Flag to indicate whether a playback is paused.
  bool m_isPaused;

  /// Flag to indicate whether a buffer underrun is occurring.
  bool m_isBufferUnderrun;

  /// @c MediaPlayerObserverInterface instance to notify when the playback state changes.
  std::shared_ptr<avsCommon::utils::mediaPlayer::MediaPlayerObserverInterface> m_playerObserver;

  /// The current source id.
  SourceId m_currentId;

  /// Flag to indicate whether a play is currently pending a callback.
  bool m_playPending;

  /// Flag to indicate whether a pause is currently pending a callback.
  bool m_pausePending;

  /// Flag to indicate whether a resume is currently pending a callback.
  bool m_resumePending;

  /// Flag to indicate whether a pause should happen immediately.
  bool m_pauseImmediately;

  // Stream offset before we teardown the pipeline
  std::chrono::milliseconds m_offsetBeforeTeardown;

  /// Flag to indicate if the player is in live mode.
  const bool m_isLiveMode;

  bool m_pauseonBuffering;

  //*********************************************************************************************************************************//

  /// ADK-MediaPlayer instance
  std::shared_ptr<adk::MediaPlayer> player;

  std::shared_ptr<avsCommon::avs::attachment::AttachmentReader> m_AttachmentReader;

  std::shared_ptr<std::istream> m_IstreamReader;

  /// Flag to indicate whether a oldstate is play (Gstreamer pipeline old state)
  bool m_oldstateisplay;

  /// Number of times reading data has been attempted since data was last successfully read.
  unsigned int m_sourceRetryCount;

  /// read the data from attachment reader, put it in a buffer and push that buffer to ADK-Mediaplayer in needdatacallback()
  void ReadDataFromAttachmentReader(size_t size_needed);

  /// read the data from istream reader, put it in a buffer and push that buffer to ADK-Mediaplayer in needdatacallback()
  void ReadDataFromIstreamReader(size_t size_needed);

  /// Flag to indicate whether the Istream is repeat
  bool m_IstreamRepeat;

  /// Flag to indicate whether the Attchmentstream is repeat
  bool m_AttachmentRepeat;

  /// to indicate AVS Mediaplayer Type
  std::string m_MediaplayerType;

  static std::string getCapsString(const avsCommon::utils::AudioFormat& audioFormat);

  /// Callback functions from ADK-mediaplayerlib
  void OnEndOfStream(void);
  void OnError_Adk(adk::MediaPlayer::ErrorType error_type,
      std::string error_msg, std::string debug_msg);

  void OnStateChanged(adk::MediaPlayer::State new_state);

  void OnBuffering(unsigned int buffering_percent);

  void OnMetadataChanged(void);

  std::unique_ptr<const VectorOfTags> collectTags(std::map<adk::MediaPlayer::Tag, std::string>& metadata);

  ////serialisation using Gobject queueCallback
  guint queueCallback(const std::function<gboolean()>* callback);

  /**
   * The worker loop to run the glib mainloop.
   */
  void workerLoop();

  /**
   * Notification of a callback to execute on the worker thread.
   *
   * @param callback The callback to execute.
   * @return Whether the callback should be called back when worker thread is once again idle.
   */
  static gboolean onCallback(const std::function<gboolean()>* callback);

  /// Main event loop.
  GMainLoop* m_mainLoop;

  // Main loop thread
  std::thread m_mainLoopThread;

  /// The context of the glib mainloop.
  GMainContext* m_workerContext;

  /// A value to indicate an unqueued callback. g_idle_add() only returns ids >= 0.
  static const guint UNQUEUED_CALLBACK = guint(0);

  std::function<void(void)> pauseCbIntoInteractionManager;
};

}  // namespace mediaPlayer
}  // namespace alexaClientSDK
   /* CODE_VOICEUIFRAMEWORK_QTIMediaPlayer_INCLUDE_QTIMediaPlayer_QTIMediaPlayer_H_ */
#endif
