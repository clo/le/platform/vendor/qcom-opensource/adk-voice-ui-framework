/* Copyright (c) 2018, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    AVSAudioRecorderObserver.h
 *  @brief   brief description
 *
 *  DESCRIPTION
 *    description
 ***************************************************************/

#ifndef VOICE_UI_FRAMEWORK_CODE_VOICEUIFRAMEWORK_AVSCLIENTINTEGRATION_INCLUDE_AVSCLIENTINTEGRATION_AVSAUDIORECORDEROBSERVER_H_
#define VOICE_UI_FRAMEWORK_CODE_VOICEUIFRAMEWORK_AVSCLIENTINTEGRATION_INCLUDE_AVSCLIENTINTEGRATION_AVSAUDIORECORDEROBSERVER_H_

#include <syslog.h>
#include <cstdio>
#include <iostream>
#include <memory>
#include <string>

#include <AIP/AudioProvider.h>
#include <AVSCommon/AVS/AudioInputStream.h>
#include <AVSCommon/SDKInterfaces/KeyWordObserverInterface.h>
#include <AVSCommon/Utils/SDS/SharedDataStream.h>
#include <DefaultClient/DefaultClient.h>

#include <AudioRecorder/AudioRecorderCallBackInterface.h>

namespace voiceUIFramework {
namespace avsManager {

using namespace alexaClientSDK;
using namespace alexaClientSDK::capabilityAgents::aip;
using namespace alexaClientSDK::avsCommon::sdkInterfaces;
using namespace alexaClientSDK::avsCommon::avs;

//using namespace voiceUIFramework::voiceUIClient;

class AVSAudioRecorderObserver : public voiceUIFramework::audioRecorder::AudioRecorderCallBackInterface {
 public:
  AVSAudioRecorderObserver(
      std::shared_ptr<avsCommon::avs::AudioInputStream> stream,
      std::shared_ptr<defaultClient::DefaultClient> client,
      capabilityAgents::aip::AudioProvider audioProvider);
  virtual ~AVSAudioRecorderObserver();

  /* AudioRecorderCallBackInterface */
  virtual void OnNewVoiceBuffer(uint8_t* buf, uint32_t size);
  virtual bool InitializeRecognition();

  int CreateBufferStream();

  static std::unique_ptr<AudioInputStream::Writer> audio_buffer_writer_;
  static std::shared_ptr<AudioInputStream> audio_buffer_;

 private:
  std::shared_ptr<avsCommon::avs::AudioInputStream> aip_stream_;
  capabilityAgents::aip::AudioProvider audio_provider_;
  std::weak_ptr<alexaClientSDK::defaultClient::DefaultClient> avs_client_;
};

}  // namespace avsManager
}  // namespace voiceUIFramework

#endif /* VOICE_UI_FRAMEWORK_CODE_VOICEUIFRAMEWORK_AVSCLIENTINTEGRATION_INCLUDE_AVSCLIENTINTEGRATION_AVSAUDIORECORDEROBSERVER_H_ */
