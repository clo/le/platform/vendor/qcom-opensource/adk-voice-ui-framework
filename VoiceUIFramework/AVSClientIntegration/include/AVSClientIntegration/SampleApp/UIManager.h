/*
 * Copyright (c) 2018, The Linux Foundation. All rights reserved.
 *
 * Not a Contribution.
 * Apache license notifications and license are retained
 * for attribution purposes only.
 */

/*
 * Copyright 2017-2018 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 *     http://aws.amazon.com/apache2.0/
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

#ifndef ALEXA_CLIENT_SDK_SAMPLE_APP_INCLUDE_AVSMANAGER_UI_MANAGER_H_
#define ALEXA_CLIENT_SDK_SAMPLE_APP_INCLUDE_AVSMANAGER_UI_MANAGER_H_

#include <AVSCommon/SDKInterfaces/AuthObserverInterface.h>
#include <AVSCommon/SDKInterfaces/Bluetooth/BluetoothDeviceObserverInterface.h>
#include <AVSCommon/SDKInterfaces/CapabilitiesObserverInterface.h>
#include <AVSCommon/SDKInterfaces/ConnectionStatusObserverInterface.h>
#include <AVSCommon/SDKInterfaces/DialogUXStateObserverInterface.h>
#include <AVSCommon/SDKInterfaces/NotificationsObserverInterface.h>
#include <AVSCommon/SDKInterfaces/SingleSettingObserverInterface.h>
#include <AVSCommon/SDKInterfaces/SpeakerInterface.h>
#include <AVSCommon/SDKInterfaces/SpeakerManagerObserverInterface.h>
#include <AVSCommon/Utils/Logger/Logger.h>
#include <AVSCommon/Utils/SDKVersion.h>
#include <AVSCommon/Utils/Threading/Executor.h>
#include <Alerts/AlertObserverInterface.h>
#include <CBLAuthDelegate/CBLAuthRequesterInterface.h>
#include <RegistrationManager/RegistrationObserverInterface.h>
#include <Settings/DeviceSettingsManager.h>
#include <Settings/SettingCallbacks.h>
#include <Settings/SettingStringConversion.h>

#include <AVSClientIntegration/SampleApp/ConsolePrinter.h>
#include <AudioRecorder/AudioRecorderInterface.h>
#include <VoiceUIClient/BaseSolution/ClientObserver.h>
#include <Wakeword/WakeWordInterface.h>

#include <adk/wakelock.h>

#include <syslog.h>
#include <iostream>
#include <sstream>

namespace voiceUIFramework {
namespace avsManager {

using namespace alexaClientSDK;
using namespace alexaClientSDK::capabilityAgents::alerts;

/**
 * This class manages the states that the user will see when interacting with the Sample Application. For now, it simply
 * prints states to the screen.
 */
class UIManager
    : public avsCommon::sdkInterfaces::DialogUXStateObserverInterface,
      public avsCommon::sdkInterfaces::AuthObserverInterface,
      public avsCommon::sdkInterfaces::CapabilitiesObserverInterface,
      public avsCommon::sdkInterfaces::ConnectionStatusObserverInterface,
      public avsCommon::sdkInterfaces::SingleSettingObserverInterface,
      public avsCommon::sdkInterfaces::SpeakerManagerObserverInterface,
      public avsCommon::sdkInterfaces::NotificationsObserverInterface,
      public avsCommon::sdkInterfaces::bluetooth::BluetoothDeviceObserverInterface,
      public authorization::cblAuthDelegate::CBLAuthRequesterInterface,
      public registrationManager::RegistrationObserverInterface,
      public AlertObserverInterface {
 public:
  using DeviceAttributes = avsCommon::sdkInterfaces::bluetooth::BluetoothDeviceObserverInterface::DeviceAttributes;

  /**
     * Constructor.
     */
  UIManager();

  void onDialogUXStateChanged(DialogUXState state) override;

  // @name ConnectionStatusObserverInterface Functions
  /// @{
  void onConnectionStatusChanged(const Status status, const ChangedReason reason) override;
  /// }

  void onSettingChanged(const std::string& key, const std::string& value) override;

  // @name SpeakerManagerObserverInterface Functions
  /// @{
  void onSpeakerSettingsChanged(
      const avsCommon::sdkInterfaces::SpeakerManagerObserverInterface::Source& source,
      const avsCommon::sdkInterfaces::SpeakerInterface::Type& type,
      const avsCommon::sdkInterfaces::SpeakerInterface::SpeakerSettings& settings) override;
  /// }

  // @name NotificationsObserverInterface Functions
  /// @{
  void onSetIndicator(avsCommon::avs::IndicatorState state) override;
  /// }

  /// @name CBLAuthRequesterInterface Functions
  /// @{
  void onRequestAuthorization(const std::string& url, const std::string& code) override;
  void onCheckingForAuthorization() override;
  /// }

  /// @name AuthObserverInterface Methods
  /// @{
  void onAuthStateChange(
      avsCommon::sdkInterfaces::AuthObserverInterface::State newState,
      avsCommon::sdkInterfaces::AuthObserverInterface::Error newError) override;
  /// }

  /// @name CapabilitiesObserverInterface Methods
  /// @{
  void onCapabilitiesStateChange(
      avsCommon::sdkInterfaces::CapabilitiesObserverInterface::State newState,
      avsCommon::sdkInterfaces::CapabilitiesObserverInterface::Error newError) override;
  /// }

  /// @name BluetoothDeviceObserverInterface Methods
  // @{
  void onActiveDeviceConnected(const DeviceAttributes& deviceAttributes) override;
  void onActiveDeviceDisconnected(const DeviceAttributes& deviceAttributes) override;
  /// }

  /// @name RegistrationObserverInterface Functions
  /// @{
  void onLogout() override;
  /// @}

  /// @name AlertObserverInterface Methods
  /// @{
  void onAlertStateChange(const std::string& alertToken, const std::string& Alerttype, AlertObserverInterface::State state, const std::string& reason) override;
  /// @}

#ifdef ENABLE_PCC
  /**
     * Prints the Phone Control screen. This gives the user the possible phone control options
     */
  void printPhoneControlScreen();

  /**
     * Prints the Call Id screen. This prompts the user to enter a caller Id.
     */
  void printCallerIdScreen();

  /**
     * Prints the Caller Id screen. This prompts the user to enter a caller Id.
     */
  void printCallIdScreen();
#endif

  /**
     * Notifies the user that the microphone is off.
     */
  void microphoneOff();

  /*
     * Prints the state that Alexa is currenty in.
     */
  void microphoneOn();

  /**
   * Prints a warning that the customer still has to manually deregister the device.
   */
  void printResetWarning();

  /**
   * Prints a confirmation message prompting the user to confirm their intent to reauthorize the device.
   */
  void printReauthorizeConfirmation();

  //QTI Adding Qualcomm Audio interfaces (ST HAL and Audio HAL)
  void addQTIInterfaces(
      std::shared_ptr<voiceUIFramework::wakeword::WakeWordInterface> wake_word,
      std::shared_ptr<voiceUIFramework::audioRecorder::AudioRecorderInterface> audio_recorder,
      std::shared_ptr<voiceUIFramework::voiceUIClient::ClientObserver> client_observer);

  //QTI restart SVA manually
  void restartSVA();

  //QTI shutdown ST HAL
  void shutdown();

  //QTI Tap to start interaction
  void tap();

  //check connectivity status
  bool checkConnectivity();

  //Starts recognition
  void setMicrophoneOn();

  //Stops recognition and sets the microphone off
  void setMicrophoneOff();

  /**
   * Prints an error message when trying to access Comms controls if Comms is not supported.
   */
  void printCommsNotSupported();

  /**
   * Configure settings notifications.
   *
   * @param Pointer to the settings manager
   * @return @true if it succeeds to configure the settings notifications; @c false otherwise.
   */
  bool configureSettingsNotifications(std::shared_ptr<settings::DeviceSettingsManager> settingsManager);

 private:
  /**
     * Prints the current state of Alexa after checking what the appropriate message to display is based on the current
     * component states. This should only be used within the internal executor.
     */
  void printState();

  /**
   * Callback function triggered when there is a notification available regarding a boolean setting.
   *
   * @param name The setting name that was affected.
   * @param enable Whether the configuration is currently enabled or disabled.
   * @param notification The type of notification.
   */
  void onBooleanSettingNotification(
      const std::string& name,
      bool enable,
      settings::SettingNotifications notification);

  /// The current dialog UX state of the SDK
  DialogUXState dialog_state_;

  /// The current CapabilitiesDelegate state.
  avsCommon::sdkInterfaces::CapabilitiesObserverInterface::State capabilities_state_;

  /// The error associated with the CapabilitiesDelegate state.
  avsCommon::sdkInterfaces::CapabilitiesObserverInterface::Error capabilities_error_;

  /// The current authorization state of the SDK.
  avsCommon::sdkInterfaces::AuthObserverInterface::State auth_state_;

  /// Counter used to make repeated messages about checking for authorization distinguishable from each other.
  int auth_check_counter_;

  /// The current connection state of the SDK.
  avsCommon::sdkInterfaces::ConnectionStatusObserverInterface::Status connection_status_;

  /// An internal executor that performs execution of callable objects passed to it sequentially but asynchronously.
  avsCommon::utils::threading::Executor executor_;

  // String that holds a failure status message to be displayed when we are in limited mode.
  std::string failure_status_;

  // Object that manages settings notifications.
  std::shared_ptr<settings::SettingCallbacks<settings::DeviceSettingsManager>> m_callbacks;

  //QTI Interfaces for: MediaPlayers, ST Hal and Audio Hal
  std::shared_ptr<voiceUIFramework::wakeword::WakeWordInterface> wakeword_engine_;
  std::shared_ptr<voiceUIFramework::audioRecorder::AudioRecorderInterface> audio_recorder_;
  std::shared_ptr<voiceUIFramework::voiceUIClient::ClientObserver> client_observer_;

  //Speech has finished
  bool speech_done_;
};

}  // namespace avsManager
}  // namespace voiceUIFramework

#endif  // ALEXA_CLIENT_SDK_SAMPLE_APP_INCLUDE_SAMPLE_APP_UI_MANAGER_H_
