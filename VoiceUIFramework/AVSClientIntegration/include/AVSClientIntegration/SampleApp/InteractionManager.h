/*
 * Copyright (c) 2018, The Linux Foundation. All rights reserved.
 *
 * Not a Contribution.
 * Apache license notifications and license are retained
 * for attribution purposes only.
 */

/*
 * Copyright 2017-2018 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 *     http://aws.amazon.com/apache2.0/
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

#ifndef ALEXA_CLIENT_SDK_SAMPLE_APP_INCLUDE_AVSMANAGER_INTERACTION_MANAGER_H_
#define ALEXA_CLIENT_SDK_SAMPLE_APP_INCLUDE_AVSMANAGER_INTERACTION_MANAGER_H_

#include <memory>

#include <AVSClientIntegration/SampleApp/GuiRenderer.h>
#include <AVSClientIntegration/SampleApp/UIManager.h>
#include <AVSCommon/SDKInterfaces/DialogUXStateObserverInterface.h>
#include <AVSCommon/SDKInterfaces/SpeakerInterface.h>
#include <AVSCommon/Utils/RequiresShutdown.h>
#include <DefaultClient/DefaultClient.h>
#include <ESP/ESPDataModifierInterface.h>
#include <ESP/ESPDataProviderInterface.h>
#include <RegistrationManager/CustomerDataManager.h>
#include <RegistrationManager/RegistrationManager.h>

namespace voiceUIFramework {
namespace avsManager {

using namespace alexaClientSDK;

/**
 * This class manages most of the user interaction by taking in commands and notifying the DefaultClient and the
 * userInterface (the view) accordingly.
 */
class InteractionManager
    : public avsCommon::sdkInterfaces::DialogUXStateObserverInterface,
      public avsCommon::utils::RequiresShutdown {
 public:
  /**
     * Constructor.
     */
  InteractionManager(
      std::shared_ptr<defaultClient::DefaultClient> client,
      // std::shared_ptr<sampleApp::PortAudioMicrophoneWrapper> micWrapper,
      std::shared_ptr<avsManager::UIManager> userInterface,
      capabilityAgents::aip::AudioProvider holdToTalkAudioProvider,
      capabilityAgents::aip::AudioProvider tapToTalkAudioProvider,
      std::shared_ptr<avsManager::GuiRenderer> guiRenderer = nullptr,
      capabilityAgents::aip::AudioProvider wakeWordAudioProvider = capabilityAgents::aip::AudioProvider::null(),
      std::shared_ptr<esp::ESPDataProviderInterface> espProvider = nullptr,
      std::shared_ptr<esp::ESPDataModifierInterface> espModifier = nullptr,
      std::shared_ptr<avsCommon::sdkInterfaces::CallManagerInterface> callManager = nullptr);

  /**
     * Should be called whenever a user presses and releases the tap button.
     */
  void tap();

  /**
     * Acts as a "stop" button. This stops whatever has foreground focus.
     */
  void stopForegroundActivity();

  /**
     * Should be called whenever a user presses 'PLAY' for playback.
     */
  void playbackPlay();

  /**
     * Should be called whenever a user presses 'PAUSE' for playback.
     */
  void playbackPause();

  /**
     * Should be called whenever a user presses 'NEXT' for playback.
     */
  void playbackNext();

  /**
     * Should be called whenever a user presses 'PREVIOUS' for playback.
     */
  void playbackPrevious();

  /**
  * Should be called whenever a user presses 'SKIP_FORWARD' for playback.
  */
  void playbackSkipForward();

  /**
  * Should be called whenever a user presses 'SKIP_BACKWARD' for playback.
  */
  void playbackSkipBackward();

  /**
  * Should be called whenever a user presses 'SHUFFLE' for playback.
  */
  void playbackShuffle();

  /**
  * Should be called whenever a user presses 'LOOP' for playback.
  */
  void playbackLoop();

  /**
  * Should be called whenever a user presses 'REPEAT' for playback.
  */
  void playbackRepeat();

  /**
  * Should be called whenever a user presses 'THUMBS_UP' for playback.
  */
  void playbackThumbsUp();

  /**
  * Should be called whenever a user presses 'THUMBS_DOWN' for playback.
  */
  void playbackThumbsDown();

  /**
     * Should be called when setting value is selected by the user.
     */
  void changeSetting(const std::string& key, const std::string& value);

  /**
     * Should be called after a user wishes to modify the volume.
     */
  void adjustVolume(avsCommon::sdkInterfaces::SpeakerInterface::Type type, int8_t delta);

  /**
     * Should be called after a user wishes to set absolute volume.
     */
  void setVolume(avsCommon::sdkInterfaces::SpeakerInterface::Type type, int8_t volume, bool SetVolumeAndNotify = true);

  /**
     * Should be called after a user wishes to set mute.
     */
  void setMute(avsCommon::sdkInterfaces::SpeakerInterface::Type type, bool mute);

  /**
   * Reset the device and remove any customer data.
   */
  void resetDevice();

  /**
  * Prompts the user to confirm the intent to re-authorize the device.
  */
  void confirmReauthorizeDevice();

  /**
     * UXDialogObserverInterface methods
     */
  void onDialogUXStateChanged(DialogUXState newState) override;

  void restartSVA();

 private:
  /// The default SDK client.
  std::weak_ptr<defaultClient::DefaultClient> avs_client_;

  /// The user interface manager.
  std::shared_ptr<avsManager::UIManager> userinterface_manager_;

  /// The gui renderer.
  std::shared_ptr<avsManager::GuiRenderer> m_guiRenderer;

  /// The hold to talk audio provider.
  capabilityAgents::aip::AudioProvider hold_to_talk_audio_provider_;

  /// The tap to talk audio provider.
  capabilityAgents::aip::AudioProvider tap_to_talk_audio_provider_;

  /// The wake word audio provider.
  capabilityAgents::aip::AudioProvider wakeword_audio_provider_;

  /// The current connection state of the SDK.
  avsCommon::sdkInterfaces::ConnectionStatusObserverInterface::Status connection_status_;

  /// Whether a hold is currently occurring.
  bool is_hold_occurring_;

  /// Whether a tap is currently occurring.
  bool is_tap_occurring_;

  /// Whether the microphone is currently turned on.
  bool is_mic_on_;

  /**
     * An internal executor that performs execution of callable objects passed to it sequentially but asynchronously.
     */
  avsCommon::utils::threading::Executor executor_;

  /// @name RequiresShutdown Functions
  /// @{
  void doShutdown() override;
  /// @}

  /// sends Gui Toggle event
  void sendGuiToggleEvent(const std::string& toggleName, avsCommon::avs::PlaybackToggle toggleType);
};

}  // namespace avsManager
}  // namespace voiceUIFramework

#endif  // ALEXA_CLIENT_SDK_SAMPLE_APP_INCLUDE_SAMPLE_APP_INTERACTION_MANAGER_H_
