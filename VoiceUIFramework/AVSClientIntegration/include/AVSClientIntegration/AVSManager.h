/* Copyright (c) 2018-2020 The Linux Foundation. All rights reserved
 * Not a Contribution
 *
 * Copyright 2017-2019 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 *     http://aws.amazon.com/apache2.0/
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

/*************************************************************
 *  @file    AVSMAnager.h
 *  @brief   AVS Voice UI Client Manager implementation
 *
 *  DESCRIPTION
 *    AVS Voice UI Client Manager using Full Solution model
 ***************************************************************/

#ifndef AVSMANAGER_INCLUDE_AVSMANAGER_QTIAVSAPP_H_
#define AVSMANAGER_INCLUDE_AVSMANAGER_QTIAVSAPP_H_

#include <arpa/inet.h>
#include <curl/curl.h>
#include <errno.h>
#include <fcntl.h>
#include <ifaddrs.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>
#include <algorithm>
#include <cctype>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <sstream>
#include <utility>
#include <vector>

#include <ACL/Transport/HTTP2TransportFactory.h>
#include <ACL/Transport/PostConnectSynchronizer.h>
#include <AVSCommon/AVS/Initialization/AlexaClientSDKInit.h>
#include <AVSCommon/Utils/Configuration/ConfigurationNode.h>
#include <AVSCommon/Utils/DeviceInfo.h>
#include <AVSCommon/Utils/LibcurlUtils/HTTPContentFetcherFactory.h>
#include <AVSCommon/Utils/LibcurlUtils/HttpPut.h>
#include <AVSCommon/Utils/LibcurlUtils/LibcurlHTTP2ConnectionFactory.h>
#include <AVSCommon/Utils/Logger/Logger.h>
#include <AVSCommon/Utils/Logger/LoggerSinkManager.h>
#include <AVSCommon/Utils/Network/InternetConnectionMonitor.h>
#include <AVSCommon/Utils/SDKVersion.h>
#include <Alerts/Storage/SQLiteAlertStorage.h>
#include <Audio/AudioFactory.h>
#include <Bluetooth/SQLiteBluetoothStorage.h>
#include <CBLAuthDelegate/CBLAuthDelegate.h>
#include <CBLAuthDelegate/SQLiteCBLAuthDelegateStorage.h>
#include <CapabilitiesDelegate/CapabilitiesDelegate.h>
#include <ContextManager/ContextManager.h>
#include <DefaultClient/EqualizerRuntimeSetup.h>
#include <EqualizerImplementations/EqualizerController.h>
#include <EqualizerImplementations/InMemoryEqualizerConfiguration.h>
#include <EqualizerImplementations/MiscDBEqualizerStorage.h>
#include <EqualizerImplementations/SDKConfigEqualizerConfiguration.h>
#include <MediaPlayer/Normalizer.h>
#include <Notifications/SQLiteNotificationsStorage.h>
#include <SQLiteStorage/SQLiteMiscStorage.h>
#include <Settings/SQLiteSettingStorage.h>
#include <Settings/Storage/SQLiteDeviceSettingStorage.h>

#ifdef ENABLE_ESP
#include <ESP/ESPDataProvider.h>
#else
#include <ESP/DummyESPDataProvider.h>
#endif

#include <AVSClientIntegration/AVSAudioRecorderObserver.h>
#include <AVSClientIntegration/AVSWakeWordObserver.h>
#include <AVSClientIntegration/QTIMediaPlayer.h>
#include <AVSClientIntegration/SampleApp/ConnectionObserver.h>
#include <AVSClientIntegration/SampleApp/ConsolePrinter.h>
#include <AVSClientIntegration/SampleApp/GuiRenderer.h>
#include <AVSClientIntegration/SampleApp/InteractionManager.h>
#include <AVSClientIntegration/SampleApp/RevokeAuthorizationObserver.h>
#include <AVSClientIntegration/SampleApp/SampleEqualizerModeController.h>
#include <AVSClientIntegration/SampleApp/UIManager.h>

#include <AudioRecorder/QAHWManager.h>
#ifdef BUILD_DSPC_CLIENT
#include <AudioRecorder/ThirdPartyAudioRecorder.h>
#endif

#ifdef BUILD_SENSORY_CLIENT
#include <Wakeword/Sensory/SensoryHandler.h>
#include <Wakeword/Sensory/SensoryWakewordEngine.h>
#endif

#ifdef BUILD_AVS_WWE
#include <Wakeword/AVS_WWE/AVSWWEWakeWord.h>
#endif

#include <VoiceUIClient/FullSolution/FullVoiceUIClient.h>
#include <VoiceUIUtils/VoiceUIConfig.h>

namespace voiceUIFramework {
namespace avsManager {

using namespace alexaClientSDK;
using namespace alexaClientSDK::avsCommon::utils::mediaPlayer;
using namespace alexaClientSDK::mediaPlayer;
using namespace voiceUIFramework::audioRecorder;
using namespace voiceUIFramework::voiceUIClient;
using namespace alexaClientSDK::settings;

class AVSManager : public FullClientManager {
 public:
  AVSManager(VoiceUIClientID client_id, bool wake_word_embedded, std::shared_ptr<voiceUIFramework::voiceUIClient::ClientObserver> client_observer);

  virtual ~AVSManager();

  /**
     * Method to create mediaPlayers for the optional music provider adapters plugged into the SDK.
     *
     * @param httpContentFetcherFactory The HTTPContentFetcherFactory to be used while creating the mediaPlayers.
     * @param equalizerRuntimeSetup Equalizer runtime setup to register equalizers
     * @param additionalSpeakers The speakerInterface to add the created mediaPlayer.
     * @return @c true if the mediaPlayer of all the registered adapters could be created @c false otherwise.
     */
  bool createMediaPlayersForAdapters(
      std::shared_ptr<avsCommon::utils::libcurlUtils::HTTPContentFetcherFactory> httpContentFetcherFactory,
      std::shared_ptr<defaultClient::EqualizerRuntimeSetup> equalizerRuntimeSetup,
      std::vector<std::shared_ptr<avsCommon::sdkInterfaces::SpeakerInterface>>& additionalSpeakers);

  /**
     * Instances of this class register ExternalMediaAdapters. Each adapter registers itself by instantiating
     * a static instance of the below class supplying their business name and creator method.
     */
  class AdapterRegistration {
   public:
    /**
         * Register an @c ExternalMediaAdapter for use by @c ExternalMediaPlayer.
         *
         * @param playerId The @c playerId identifying the @c ExternalMediaAdapter to register.
         * @param createFunction The function to use to create instances of the specified @c ExternalMediaAdapter.
         */
    AdapterRegistration(
        const std::string& playerId,
        capabilityAgents::externalMediaPlayer::ExternalMediaPlayer::AdapterCreateFunction createFunction);
  };

  /**
     * Instances of this class register MediaPlayers to be created. Each third-party adapter registers a mediaPlayer
     * for itself by instantiating a static instance of the below class supplying their business name, speaker interface
     * type and creator method.
     */
  class MediaPlayerRegistration {
   public:
    /**
         * Register a @c MediaPlayer for use by a music provider adapter.
         *
         * @param playerId The @c playerId identifying the @c ExternalMediaAdapter to register.
         * @param speakerType The SpeakerType of the mediaPlayer to be created.
         */
    MediaPlayerRegistration(
        const std::string& playerId,
        avsCommon::sdkInterfaces::SpeakerInterface::Type speakerType);
  };

  /* FullClientManager class */
  //Validates credentials from Voice UI Client
  virtual bool ValidateCredentials();
  //initializes Voice UI Client and SDK
  virtual bool Initialize();
  //Runs Voice UI Client
  virtual bool StartClient();
  //Stops Voice UI Client
  virtual bool StopClient();
  //Receive Intents from Voice UI IntentManager
  virtual void IntentReceived(IntentManagerEvent intent);
  virtual void IntentReceived(IntentManagerEvent intent, IntentManagerEventExtended intent_ext);
  // Receive messages from main application
  virtual void AppMessageReceived(AppMessage app_message);

 private:
  //Setup AVS SDK
  bool setUpSDK();

  //Verify if there is internet connectivity
  bool CheckForInternet();

  /**
   * Create an application media player.
   *
   * @param contentFetcherFactory Used to create objects that can fetch remote HTTP content.
   * @param enableEqualizer Flag indicating if equalizer should be enabled for this media player.
   * @param type The type used to categorize the speaker for volume control.
   * @param name The media player instance name used for logging purpose.
     * @param enableLiveMode Flag, indicating if the player is in live mode.
   * @return A pointer to the @c QTIMediaPlayer and to its speaker if it succeeds; otherwise, return @c
   * nullptr.
   */
  std::pair<std::shared_ptr<QTIMediaPlayer>, std::shared_ptr<avsCommon::sdkInterfaces::SpeakerInterface>>
  createApplicationMediaPlayer(
      std::shared_ptr<avsCommon::utils::libcurlUtils::HTTPContentFetcherFactory> httpContentFetcherFactory,
      bool enableEqualizer,
      avsCommon::sdkInterfaces::SpeakerInterface::Type type,
      const std::string& name,
      bool enableLiveMode = false);

  /// The map of the adapters and their mediaPlayers.
  std::unordered_map<std::string, std::shared_ptr<avsCommon::utils::mediaPlayer::MediaPlayerInterface>> external_music_provider_mediaplayers_map_;
  /// The vector of mediaPlayers for the adapters.
  std::vector<std::shared_ptr<mediaPlayer::QTIMediaPlayer>> adapter_mediaplayers_;

  //AVS MediaPlayers
  std::shared_ptr<QTIMediaPlayer> speaker_mediaplayer_;
  std::shared_ptr<QTIMediaPlayer> audioplayer_mediaplayer_;

  std::shared_ptr<QTIMediaPlayer> alerts_mediaplayer_;
  std::shared_ptr<QTIMediaPlayer> notifications_mediaplayer_;
  std::shared_ptr<QTIMediaPlayer> bluetooth_mediaplayer_;
  std::shared_ptr<QTIMediaPlayer> ringtone_mediaplayer_;

  void IntentNextTrack(void);
  void IntentPreviousTrack(void);
  void IntentPlay(void);
  void IntentPause(void);
  void IntentOnMuteUpdated(bool mute_state);
  void IntentOnVolumeUpdated(double volume);

  /// The map of the adapters and their mediaPlayers.
  std::unordered_map<std::string, std::shared_ptr<avsCommon::sdkInterfaces::SpeakerInterface>>
      external_music_provider_speakers_map_;

  /// The @c CapabilitiesDelegate used by the client.
  std::shared_ptr<alexaClientSDK::capabilitiesDelegate::CapabilitiesDelegate> capabilities_delegate_;

  //default AVS Client to be used on AVS use cases.
  std::shared_ptr<alexaClientSDK::defaultClient::DefaultClient> default_client_;
  //Interaction manager reference for keyInputs/intent interaction
  std::shared_ptr<voiceUIFramework::avsManager::InteractionManager> interaction_manager_;
  //userInterface manager reference for keyInputs/intent interaction
  std::shared_ptr<voiceUIFramework::avsManager::UIManager> userinterface_manager_;

  /// The @c GuiRender which provides an abstraction to visual rendering
  std::shared_ptr<GuiRenderer> guiRenderer_;

  /// The singleton map from @c playerId to @c SpeakerInterface::Type.
  static std::unordered_map<std::string, avsCommon::sdkInterfaces::SpeakerInterface::Type> player_to_speaker_type_map_;
  /// The singleton map from @c playerId to @c ExternalMediaAdapter creation functions.
  static capabilityAgents::externalMediaPlayer::ExternalMediaPlayer::AdapterCreationMap adapter_to_create_func_map_;

  //Reference for AVS Audio recorder Callback object
  std::shared_ptr<voiceUIFramework::avsManager::AVSAudioRecorderObserver> avs_audiorecorder_observer_;
  //Modular Wakeword Callback object
  std::shared_ptr<voiceUIFramework::avsManager::AVSWakeWordObserver> avs_wakeword_observer_;

  //AVS onboarding requested by mobile APP
  bool onboard_requested_;

  //AVS onboarding cancel
  bool onboard_cancelled_;

  //initialization lock
  bool initialization_lock_released_;

  //Thirdparty keyword detection engine status
  bool avs_3P_KWD_enabled_;

  //Thirdparty keyword detection engine selection
  int avs_3P_KWD_selection_;
};

}  // namespace avsManager
}  // namespace voiceUIFramework

#endif
