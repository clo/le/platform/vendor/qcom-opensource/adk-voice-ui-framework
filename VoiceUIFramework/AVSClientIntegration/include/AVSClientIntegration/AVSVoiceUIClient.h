/* Copyright (c) 2018, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT  SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */
/*************************************************************
 *  @file    AVSVoiceUIClient.h
 *  @brief   AVS Voice UI Client
 *
 *  DESCRIPTION
 *    AVS Voice UI Client using Full Solution model
 ***************************************************************/

#ifndef VOICEUIFRAMEWORK_AVSMANAGER__INCLUDE_AVSMANAGER_AVSVOICEUICLIENT_H_
#define VOICEUIFRAMEWORK_AVSMANAGER__INCLUDE_AVSMANAGER_AVSVOICEUICLIENT_H_

#include <iostream>
#include <memory>
#include <string>
#include <thread>

#include <AVSClientIntegration/AVSManager.h>
#include <VoiceUIClient/FullSolution/FullClientManager.h>
#include <VoiceUIClient/FullSolution/FullVoiceUIClient.h>
#include <VoiceUIUtils/GlobalValues.h>

namespace voiceUIFramework {
namespace avsManager {

using namespace alexaClientSDK;
using namespace voiceUIFramework::wakeword;
using namespace voiceUIFramework::voiceUIUtils;
using namespace voiceUIFramework::voiceUIClient;

class AVSVoiceUIClient : public FullVoiceUIClient {
 public:
  AVSVoiceUIClient(
      std::shared_ptr<voiceUIFramework::voiceUIClient::ClientObserver> client_observer,
      voiceUIFramework::voiceUIUtils::VoiceUIClientID clientID,
      bool wake_word_embedded);
  virtual ~AVSVoiceUIClient();

  /* BaseVoiceUIClient */
  //Configures Client
  virtual bool Configure();
  //Receives a Intent from VoiceUIManager
  virtual void IntentReceived(IntentManagerEvent intent);
  virtual void IntentReceived(IntentManagerEvent intent, IntentManagerEventExtended intent_ext);
  virtual bool Stop();

 private:
  //Thread to execute client solution
  std::thread client_thread_;
};

}  // namespace avsManager
}  // namespace voiceUIFramework

#endif /* VOICEUIFRAMEWORK_AVSMANAGER__INCLUDE_AVSMANAGER_AVSVOICEUICLIENT_H_ */
