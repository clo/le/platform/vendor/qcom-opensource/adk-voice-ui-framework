/* Copyright (c) 2018, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    AVSWakeWordObserver.cpp
 *  @brief   Implements WakeWord Callbacks for AVS solution
 *
 *  DESCRIPTION
 *    Implements WakeWord Callbacks for AVS solution
 ***************************************************************/

#include <AVSClientIntegration/AVSWakeWordObserver.h>

namespace voiceUIFramework {
namespace avsManager {

using namespace alexaClientSDK;

#define OK 0

/// The sample rate of microphone audio data.
static const unsigned int SAMPLE_RATE_HZ = 16000;
/// The number of audio channels.
static const unsigned int NUM_CHANNELS = 1;
/// The size of each word within the stream.
static const size_t WORD_SIZE = 2;
/// The maximum number of readers of the stream.
static const size_t MAX_READERS = 10;
/// The amount of audio data to keep in the ring buffer.
static const std::chrono::seconds AMOUNT_OF_AUDIO_DATA_IN_BUFFER = std::chrono::seconds(15);
/// The size of the ring buffer.
static const size_t BUFFER_SIZE_IN_SAMPLES = (SAMPLE_RATE_HZ)*AMOUNT_OF_AUDIO_DATA_IN_BUFFER.count();

AVSWakeWordObserver::AVSWakeWordObserver(std::shared_ptr<alexaClientSDK::avsCommon::avs::AudioInputStream> stream,
    std::shared_ptr<alexaClientSDK::defaultClient::DefaultClient> client, std::shared_ptr<voiceUIFramework::wakeword::WakeWordInterface> wakeword)
    : stream_{stream}, client_{client}, wakeword_{wakeword}, log_filename_{nullptr} {
  //Read VoiceUI Database
  auto voice_ui_config = voiceUIFramework::voiceUIUtils::VoiceUIConfig::getInstance();
  if (voice_ui_config) {
    //Read Voice Dump status
    voicedump_enable_ = voice_ui_config->ReadWithDefault<bool>(true, voiceUIUtils::Keys::k_framework_voice_dump_status_);
    //Read Voice Dump Path
    voicedump_path_ = voice_ui_config->ReadWithDefault<std::string>("/data", voiceUIUtils::Keys::k_framework_voice_dump_path_);
  } else {
    syslog(LOG_ERR, "VoiceUIConfig not Initialized");
    voicedump_enable_ = true;
    voicedump_path_ = "/data";
  }
  syslog(LOG_INFO, "Voice Dump Status = %d", voicedump_enable_);
  syslog(LOG_INFO, "Voice Dump for Local ASR Path = %s", voicedump_path_.c_str());

  syslog(LOG_INFO, "+++++++AVSWakeWordObserver Constructor called");
}

AVSWakeWordObserver::~AVSWakeWordObserver() {
  syslog(LOG_INFO, "------AVSWakeWordObserver Destructor called");
}

WakewordCallbackRc AVSWakeWordObserver::ObserverAdded() {
  syslog(LOG_DEBUG, "AVSWakeWordObserver::ObserverAdded");

  //Creating compatible audio format for AVS
  compatible_audio_format_.sampleRateHz = SAMPLE_RATE_HZ;
  compatible_audio_format_.sampleSizeInBits = WORD_SIZE * CHAR_BIT;
  compatible_audio_format_.numChannels = NUM_CHANNELS;
  compatible_audio_format_.endianness = alexaClientSDK::avsCommon::utils::AudioFormat::Endianness::LITTLE;
  compatible_audio_format_.encoding = alexaClientSDK::avsCommon::utils::AudioFormat::Encoding::LPCM;

  return WakewordCallbackRc::WW_CB_OK;
}

WakewordCallbackRc AVSWakeWordObserver::ObserverRemoved() {
  syslog(LOG_DEBUG, "AVSWakeWordObserver::ObserverRemoved");
  return WakewordCallbackRc::WW_CB_OK;
}

WakewordCallbackRc AVSWakeWordObserver::StartRecognitionComplete() {
  syslog(LOG_DEBUG, "AVSWakeWordObserver::StartRecognitionComplete");
  return WakewordCallbackRc::WW_CB_OK;
}

WakewordCallbackRc AVSWakeWordObserver::OnNewVoiceBufferReceived(void* buf, size_t size) {
  //TODO: Platform Independent size_t conversion to uint - loss less
  //syslog(LOG_DEBUG, "AVSWakeWordObserver::OnNewVoiceBuffer - Received buffer size =%d\n", (int)size);

  // write to AVS inputStream
  audioBufferWriter_->write(buf, size / 2);

  //save audio in a file
  if (voicedump_enable_) {
    if (log_filename_ != nullptr) {
      fwrite(buf, 1, size, log_filename_);
      fflush(log_filename_);
    }
  }
  return WakewordCallbackRc::WW_CB_OK;
}

WakewordCallbackRc AVSWakeWordObserver::OnKeywordDetected(voiceUIFramework::wakeword::KwdDetectionResults res) {
  syslog(LOG_DEBUG, "AVSWakeWordObserver::OnKeywordDetected - *** ALEXA WAKEWORD ***");

  //Acquire AVS lock
  int delay = WAKELOCK_AVS_TIMEOUT;
  system_wake_lock_toggle(true, WAKELOCK_AVS_NAME, delay);

  // restart Audio Stream
  if (create_kw_lab_stream() != 0) {
    syslog(LOG_ERR, "AVSWakeWordObserver::OnKeywordDetected - create_kw_lab_stream() - FAILED");
  }

  //open dump file
  if (voicedump_enable_) {
    std::string filepath = std::string(voicedump_path_ + "/voiceDumpAVS");
    syslog(LOG_INFO, "AVSWakeWordObserver::OnKeywordDetected() - Voice Dump path: %s - %s", voicedump_path_.c_str(), filepath.c_str());
    log_filename_ = fopen(filepath.c_str(), "wb");
  }

  auto temp_client = client_.lock();
  if (temp_client) {
    // Report to AVS in samples not Bytes

    alexaClientSDK::avsCommon::avs::AudioInputStream::Index aipBegin = std::round(res.keyword_start_index_ / WORD_SIZE);
    alexaClientSDK::avsCommon::avs::AudioInputStream::Index aipEnd = std::round(res.keyword_end_index_ / WORD_SIZE);

    auto startRecognition = false;

    // Recreating wakeword audio provider for new stream_
    bool wakeAlwaysReadable = false;
    bool wakeCanOverride = true;
    bool wakeCanBeOverridden = true;

    alexaClientSDK::capabilityAgents::aip::AudioProvider wakeWordAudioProvider(stream_, compatible_audio_format_,
        alexaClientSDK::capabilityAgents::aip::ASRProfile::NEAR_FIELD, wakeAlwaysReadable, wakeCanOverride, wakeCanBeOverridden);

    //Check if there is a keyword associated to detection
    if (!res.keyword.compare("")) {
      res.keyword = "ALEXA";
    }

    // TODO(ACSDK-1976): We need to take into consideration the keyword duration.
    auto startOfSpeechTimestamp = std::chrono::steady_clock::now();
    startRecognition = (temp_client->notifyOfWakeWord(
                            wakeWordAudioProvider,
                            aipBegin,
                            aipEnd,
                            res.keyword,
                            startOfSpeechTimestamp))
                           .get();

    syslog(LOG_DEBUG, "AVSWakeWordObserver::OnKeywordDetected - Keyword = %s - Indices: Start= %lld - End= %lld", res.keyword.c_str(), aipBegin, aipEnd);

    //Restart ST Hal Keyword Recognition and bring Audio input processor to IDLE state if it fails to send Recognize Event.
    if (startRecognition == false) {
      syslog(LOG_ERR, "AVSWakeWordObserver::onKeyWordDetected - startRecognition  FAILED !!!!!");
      //Stop LAB because AVS was not able to initiate use case.
      wakeword_->StopAudioCapture(VoiceUIClientID::AVS_SOLUTION,
          voiceUIFramework::wakeword::StopCaptureResult::STOPCAPTURE_FAIL);
      //Stop AVS current activity (audio input processor)
      temp_client->stopForegroundActivity();
      return WakewordCallbackRc::WW_CB_ERR;
    }
  }
  return WakewordCallbackRc::WW_CB_OK;
}

WakewordCallbackRc AVSWakeWordObserver::StopRecognitionComplete() {
  syslog(LOG_DEBUG, "AVSWakeWordObserver::StopRecognitionComplete");
  return WakewordCallbackRc::WW_CB_OK;
}

WakewordCallbackRc AVSWakeWordObserver::StopAudioCaptureComplete() {
  syslog(LOG_DEBUG, "AVSWakeWordObserver::StopAudioCaptureComplete");
  //close dumpfile
  if (voicedump_enable_) {
    if (log_filename_ != nullptr) {
      fclose(log_filename_);
      log_filename_ = nullptr;
    }
  }

  return WakewordCallbackRc::WW_CB_OK;
}

int AVSWakeWordObserver::create_kw_lab_stream() {
  int result = OK;

  /*
     * Creating the buffer (Shared Data Stream) that will hold user audio data. This is the main input into the SDK.
     */
  size_t buffer_size = alexaClientSDK::avsCommon::avs::AudioInputStream::calculateBufferSize(BUFFER_SIZE_IN_SAMPLES, WORD_SIZE, MAX_READERS);
  auto aip_buffer = std::make_shared<alexaClientSDK::avsCommon::avs::AudioInputStream::Buffer>(buffer_size);
  stream_ = alexaClientSDK::avsCommon::avs::AudioInputStream::create(
      aip_buffer, WORD_SIZE, MAX_READERS);

  if (stream_ == nullptr) {
    syslog(LOG_ERR, "AVSWakeWordObserver::ObserverAdded - create_kw_lab_stream() - assert_error: create_kw_lab_stream - m_stream == nullptr");
    result = -1;
  } else {
    audioBufferWriter_ = stream_->createWriter(alexaClientSDK::avsCommon::avs::AudioInputStream::Writer::Policy::NONBLOCKABLE, true);
    if (audioBufferWriter_ == nullptr) {
      syslog(LOG_ERR, "AVSWakeWordObserver::ObserverAdded - assert_error: create_kw_lab_stream - audioBufferWriter == nullptr");
      result = -1;
    }
  }

  return result;
}

WakewordCallbackRc AVSWakeWordObserver::SoundTriggerDataReceived(
    voiceUIFramework::wakeword::SoundTriggerData type,
    int value) {
  syslog(LOG_DEBUG, "AVSWakeWordObserver::SoundTriggerDataReceived %s  : %d\n", SoundTriggerDataToString(type).c_str(), value);
  return WakewordCallbackRc::WW_CB_OK;
}

}  // namespace avsManager
}  // namespace voiceUIFramework
