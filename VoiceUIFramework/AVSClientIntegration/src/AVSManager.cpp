/* Copyright (c) 2018-2020 The Linux Foundation. All rights reserved
 * Not a Contribution
 *
 * Copyright 2017-2019 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 *     http://aws.amazon.com/apache2.0/
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

/*************************************************************
 *  @file    AVSMAnager.cpp
 *  @brief   AVS Voice UI Client Manager implementation
 *
 *  DESCRIPTION
 *    AVS Voice UI Client Manager using Full Solution model
 ***************************************************************/

#include <AVSClientIntegration/AVSManager.h>

namespace voiceUIFramework {
namespace avsManager {

using namespace alexaClientSDK;
using namespace alexaClientSDK::avsCommon::utils::mediaPlayer;
using namespace alexaClientSDK::mediaPlayer;
using namespace alexaClientSDK::capabilityAgents::externalMediaPlayer;
using namespace alexaClientSDK::settings;

//AVS SDK Version
static const std::string AVS_VERSION = avsCommon::utils::sdkVersion::getCurrentVersion();

/// The sample rate of microphone audio data.
static const unsigned int SAMPLE_RATE_HZ = 16000;
/// The number of audio channels.
static const unsigned int NUM_CHANNELS = 1;
/// The size of each word within the stream.
static const size_t WORD_SIZE = 2;
/// The maximum number of readers of the stream.
static const size_t MAX_READERS = 10;
/// The amount of audio data to keep in the ring buffer.
static const std::chrono::seconds AMOUNT_OF_AUDIO_DATA_IN_BUFFER = std::chrono::seconds(15);
/// The size of the ring buffer.
static const size_t BUFFER_SIZE_IN_SAMPLES = (SAMPLE_RATE_HZ)*AMOUNT_OF_AUDIO_DATA_IN_BUFFER.count();

/// Key for the root node value containing configuration values for SampleApp.
static const std::string SAMPLE_APP_CONFIG_KEY("sampleApp");

/// Key for the root node value containing configuration values for Equalizer.
static const std::string EQUALIZER_CONFIG_KEY("equalizer");

/// Key for the @c firmwareVersion value under the @c SAMPLE_APP_CONFIG_KEY configuration node.
static const std::string FIRMWARE_VERSION_KEY("firmwareVersion");

/// Key for the endpoint value under the @c SAMPLE_APP_CONFIG_KEY configuration node.
static const std::string ENDPOINT_KEY("endpoint");

/// Key for setting if display cards are supported or not under the @c SAMPLE_APP_CONFIG_KEY configuration node.
static const std::string DISPLAY_CARD_KEY("displayCardsSupported");

/// Path to configuration file (from command line arguments).
std::vector<std::string> configFiles;

/// The @c m_playerToMediaPlayerMap Map of the adapter to their speaker-type and MediaPlayer creation methods.
std::unordered_map<std::string, avsCommon::sdkInterfaces::SpeakerInterface::Type> AVSManager::player_to_speaker_type_map_;

/// The singleton map from @c playerId to @c ExternalMediaAdapter creation functions.
std::unordered_map<std::string, ExternalMediaPlayer::AdapterCreateFunction> AVSManager::adapter_to_create_func_map_;

//#define HOST_ADDR "www.developer.amazon.com"

/// String to identify log entries originating from this file.
static const std::string TAG("SampleApplication");

/**
 * Create a LogEntry using this file's TAG and the specified event string.
 *
 * @param The event string for this @c LogEntry.
 */
#define LX(event) alexaClientSDK::avsCommon::utils::logger::LogEntry(TAG, event)

std::string config_path_json_;          //= "/data/AlexaClientSDKConfig.json";
std::string avs_client_id_;             // = "amzn1.application-oa2-client.1a726446b84c43c191ee3ec166f0d338";
std::string avs_product_id_;            // = "QCReference";
std::string avs_cbl_auth_delegate_db_;  // = "/data/cblAuthDelegate.db";
std::string avsJsonString_start_("{\n\"deviceInfo\":{\n   \"deviceSerialNumber\":\"");
std::string avsJsonString_client_id_("\",\n   \"clientId\":\"");
std::string avsJsonString_product_id_("\",\n   \"productId\":\"");
std::string avsJsonString_auth_delegate__("\"\n},\n\"alertsCapabilityAgent\": {\n    \"databaseFilePath\":\"/data/alerts.db\"\n},\n\"settings\":{\n    \"databaseFilePath\":\"/data/settingsca.db\",\n    \"defaultAVSClientSettings\":{\n        \"locale\":\"en-US\"\n    }\n},\n\"deviceSettings\":{\n   \"databaseFilePath\":\"/data/devicesettings.db\"\n},    \n\"notifications\":{\n    \"databaseFilePath\":\"/data/notifications.db\"\n},\n\"sampleApp\":{\n    \"displayCardsSupported\":true\n},\n\"certifiedSender\":{\n        \"databaseFilePath\":\"/data/certifiedsender.db\"\n},\n\"cblAuthDelegate\":{\n      \"databaseFilePath\":\"");
std::string avsJsonString_final_("\"\n},\n\"miscDatabase\":{\n      \"databaseFilePath\":\"/data/miscDatabase.db\"\n},\n\"logging\":{\n      \"logLevel\":\"DEBUG9\"\n}\n}");

static const int8_t INCREASE_VOLUME = 10;
static const int8_t DECREASE_VOLUME = -10;

/// GStreamer Volume Element Minimum.
static const int8_t GST_SET_VOLUME_MIN = 0;

/// GStreamer Volume Element Maximum.
static const int8_t GST_SET_VOLUME_MAX = 1;

/// AVS setVolume Minimum.
const int8_t AVS_SET_VOLUME_MIN = 0;

/// AVS setVolume Maximum.
const int8_t AVS_SET_VOLUME_MAX = 100;

//Amazon server address
#define HOST_ADDR_AMAZON "www.developer.amazon.com"

AVSManager::AVSManager(
    VoiceUIClientID client_id,
    bool wake_word_embedded,
    std::shared_ptr<voiceUIFramework::voiceUIClient::ClientObserver> client_observer)
    : FullClientManager(client_id, wake_word_embedded, client_observer), onboard_requested_{false}, onboard_cancelled_{false}, initialization_lock_released_{false} {
  syslog(LOG_INFO, "AVSManager::AVSManager() - Constructor called for: %s", ClientIDToString(client_id).c_str());

  //Read VoiceUI Database
  auto voice_ui_config = voiceUIFramework::voiceUIUtils::VoiceUIConfig::getInstance();
  if (voice_ui_config) {
    //delete code and URL result
    if (voice_ui_config->Write<std::string>("", voiceUIUtils::Keys::k_avs_authorization_code_) && voice_ui_config->Write<std::string>("", voiceUIUtils::Keys::k_avs_authorization_url_)) {
      syslog(LOG_INFO, "DB is updated  : Delete Authorization code and URL");
    } else {
      //TODO:
      syslog(LOG_ERR, "Fatal Error, Unable to write to DB, Inconsistent");
    }
    //Read JSON configuration path
    config_path_json_ = voice_ui_config->ReadWithDefault<std::string>("/data/AlexaClientSDKConfig.json", voiceUIUtils::Keys::k_avs_credential_file_);
    //Read CBL Authentication DB configuration path
    avs_cbl_auth_delegate_db_ = voice_ui_config->ReadWithDefault<std::string>("/data/cblAuthDelegate.db", voiceUIUtils::Keys::k_avs_cblauth_db_);
    //Read AVS Client ID
    avs_client_id_ = voice_ui_config->ReadWithDefault<std::string>("amzn1.application-oa2-client.1a726446b84c43c191ee3ec166f0d338", voiceUIUtils::Keys::k_avs_client_id_);
    //Read AVS Product ID
    avs_product_id_ = voice_ui_config->ReadWithDefault<std::string>("QCReference", voiceUIUtils::Keys::k_avs_product_id_);
    //Read ThirdParty voice detection engine status
    avs_3P_KWD_enabled_ = voice_ui_config->ReadWithDefault<bool>(false, voiceUIUtils::Keys::k_avs_third_party_kwd_enabled_);
    //Read ThirdParty voice detection engine selection
    avs_3P_KWD_selection_ = voice_ui_config->ReadWithDefault<int>(ThirdPartyKWDEnginesIDToIndex(ThirdPartyKWDEngines::NO_CLIENT), voiceUIUtils::Keys::k_avs_third_party_kwd_selection_);

  } else {
    syslog(LOG_ERR, "VoiceUIManager::VoiceUIManager() - VoiceUIConfig not Initialized");
    config_path_json_ = "/data/AlexaClientSDKConfig.json";
    avs_cbl_auth_delegate_db_ = "/data/cblAuthDelegate.db";
    avs_client_id_ = "amzn1.application-oa2-client.1a726446b84c43c191ee3ec166f0d338";
    avs_product_id_ = "QCReference";
    avs_3P_KWD_enabled_ = false;
    avs_3P_KWD_selection_ = ThirdPartyKWDEnginesIDToIndex(ThirdPartyKWDEngines::NO_CLIENT);
  }
  syslog(LOG_INFO, "AVSManager::AVSManager() - JSON Path = %s", config_path_json_.c_str());
  syslog(LOG_INFO, "AVSManager::AVSManager() - CBL DB Path = %s", avs_cbl_auth_delegate_db_.c_str());
  syslog(LOG_INFO, "AVSManager::AVSManager() - AVS Client ID = %s", avs_client_id_.c_str());
  syslog(LOG_INFO, "AVSManager::AVSManager() - AVS Product ID = %s", avs_product_id_.c_str());
  syslog(LOG_INFO, "AVSManager::AVSManager() - AVS Third party Keyword Detection Engine Enabled = %d", avs_3P_KWD_enabled_);
  syslog(LOG_INFO, "AVSManager::AVSManager() - AVS Third party Keyword Detection Engine Selection Index = %d", avs_3P_KWD_selection_);
}

AVSManager::~AVSManager() {
  syslog(LOG_INFO, "AVSManager::~AVSManager() - INIT");

  if (capabilities_delegate_) {
    capabilities_delegate_->shutdown();
  }

  // First clean up anything that depends on the the MediaPlayers.
  external_music_provider_mediaplayers_map_.clear();

  if (interaction_manager_) {
    interaction_manager_->shutdown();
  }

  if (userinterface_manager_) {
    userinterface_manager_->shutdown();
  }

  // Now it's safe to shut down the MediaPlayers.
  for (auto& mediaPlayer : adapter_mediaplayers_) {
    mediaPlayer->shutdown();
  }
  if (speaker_mediaplayer_) {
    speaker_mediaplayer_->shutdown();
  }
  if (audioplayer_mediaplayer_) {
    audioplayer_mediaplayer_->shutdown();
  }
  if (alerts_mediaplayer_) {
    alerts_mediaplayer_->shutdown();
  }
  if (notifications_mediaplayer_) {
    notifications_mediaplayer_->shutdown();
  }
  if (bluetooth_mediaplayer_) {
    bluetooth_mediaplayer_->shutdown();
  }
  if (ringtone_mediaplayer_) {
    ringtone_mediaplayer_->shutdown();
  }

#ifdef ENABLE_COMMS_AUDIO_PROXY
  if (m_commsMediaPlayer) {
    m_commsMediaPlayer->shutdown();
  }
#endif

  avsCommon::avs::initialization::AlexaClientSDKInit::uninitialize();

  //wakeword
  if (wake_word_ != nullptr)
    wake_word_->RemoveObserver(VoiceUIClientID::AVS_SOLUTION);

  syslog(LOG_INFO, "AVSManager::~AVSManager() - DONE");
}

AVSManager::AdapterRegistration::AdapterRegistration(const std::string& playerId, ExternalMediaPlayer::AdapterCreateFunction createFunction) {
  if (adapter_to_create_func_map_.find(playerId) != adapter_to_create_func_map_.end()) {
    std::string errorStr = "WARNING:Adapter already exists for playerId " + playerId;
    voiceUIFramework::avsManager::ConsolePrinter::simplePrint(errorStr);
  }

  adapter_to_create_func_map_[playerId] = createFunction;
}

AVSManager::MediaPlayerRegistration::MediaPlayerRegistration(
    const std::string& playerId,
    avsCommon::sdkInterfaces::SpeakerInterface::Type speakerType) {
  if (player_to_speaker_type_map_.find(playerId) != player_to_speaker_type_map_.end()) {
    std::string errorStr = "WARNING:MediaPlayer already exists for playerId " + playerId;
    voiceUIFramework::avsManager::ConsolePrinter::simplePrint(errorStr);
  }

  player_to_speaker_type_map_[playerId] = speakerType;
}

bool AVSManager::createMediaPlayersForAdapters(
    std::shared_ptr<avsCommon::utils::libcurlUtils::HTTPContentFetcherFactory> http_content_fetcher_factory,
    std::shared_ptr<defaultClient::EqualizerRuntimeSetup> equalizerRuntimeSetup,
    std::vector<std::shared_ptr<avsCommon::sdkInterfaces::SpeakerInterface>>& additionalSpeakers) {
  bool equalizerEnabled = nullptr != equalizerRuntimeSetup;
  for (auto& entry : player_to_speaker_type_map_) {
    auto mediaPlayerSpeakerPair = createApplicationMediaPlayer(
        http_content_fetcher_factory, equalizerEnabled, entry.second, entry.first + "QTIMediaPlayer", false);
    auto mediaPlayer = mediaPlayerSpeakerPair.first;
    auto speaker = mediaPlayerSpeakerPair.second;
    if (mediaPlayer) {
      external_music_provider_mediaplayers_map_[entry.first] = mediaPlayer;
      external_music_provider_speakers_map_[entry.first] = speaker;
      additionalSpeakers.push_back(speaker);
      adapter_mediaplayers_.push_back(mediaPlayer);
      if (equalizerEnabled) {
        equalizerRuntimeSetup->addEqualizer(mediaPlayer);
      }
    } else {
      std::string errorStr = "ERROR:Failed to create mediaPlayer for playerId " + entry.first;
      voiceUIFramework::avsManager::ConsolePrinter::simplePrint(errorStr);
      return false;
    }
  }

  return true;
}

bool AVSManager::Initialize() {
  syslog(LOG_INFO, "AVSManager::Initialize()");

  //Check AVS SDK Credentials
  if (!ValidateCredentials()) {
    return false;
  }

  //Create wakeword client
  if (!wake_word_embedded_) {
    // Check if device is configured to use Third Party Keyword Detection Algorithms
    if (avs_3P_KWD_enabled_) {
      ThirdPartyKWDEngines avs_3P_KWD_selection_ID = static_cast<ThirdPartyKWDEngines>(avs_3P_KWD_selection_);
      switch (avs_3P_KWD_selection_ID) {
        case ThirdPartyKWDEngines::AVS_WWE_CLIENT: {
#if defined BUILD_DSPC_CLIENT && defined BUILD_AVS_WWE
          syslog(LOG_DEBUG, "AVSManager::Initialize() - Creating AVS WWE KWD Engine");
          // Create WWE client
          wake_word_ = std::make_shared<voiceUIFramework::wakeword::avs::AVSWWEWakeWord>();
#else
          syslog(LOG_ERR, "AVSManager::Initialize() - ERROR Current Device build does not support AVS WWE or DSPC Engine.\n Exiting AVS Initialization !");
          return false;
#endif
        } break;
        case ThirdPartyKWDEngines::SENSORY_CLIENT: {
#if defined BUILD_DSPC_CLIENT && defined BUILD_SENSORY_CLIENT
          syslog(LOG_DEBUG, "AVSManager::Initialize() - Creating Sensory KWD Engine");
          // Create Sensory client
          wake_word_ = voiceUIFramework::wakeword::sensory::SensoryWakewordEngine::Create();
#else
          syslog(LOG_ERR, "AVSManager::Initialize() - ERROR Current Device build does not support Sensory or DSPC Engine.\n Exiting AVS Initialization !");
          return false;
#endif
        } break;
        default:
          syslog(LOG_ERR, "AVSManager::Initialize() - ERROR Current Device build does not support KWD Engine Selected on Qualcomm Device Configuration Tool. Engine Index Selected =%d.\n Exiting AVS Initialization !", avs_3P_KWD_selection_);
          return false;
          break;
      }
    } else {
      syslog(LOG_DEBUG, "AVSManager::Initialize() - Creating Qualcomm KWD Engine (QVA)");
      // Get Single to STHal Client pointer
      wake_word_ = voiceUIFramework::wakeword::QSTHWClient::Get();
    }

    if (wake_word_ == nullptr) {
      syslog(LOG_ERR, "AVSManager::Initialize() - ERROR During Wakeword Initialization - Exiting AVS Initialization");
      return false;
    }
  }

  //Create Audio recorder for Client configure to recording On Demand only
#if defined BUILD_DSPC_CLIENT && (defined BUILD_AVS_WWE || defined BUILD_SENSORY_CLIENT)
  // Check if device is configured to use Third Party Keyword Detection Algorithms
  if (avs_3P_KWD_enabled_) {
    // Create ThirdParty Audio Recorder for DSPC client
    audio_recorder_ = std::make_shared<ThirdPartyAudioRecorder>(wake_word_, true, client_id_);
  } else {
    syslog(LOG_ERR, "AVSManager::Initialize() - ERROR DSPC client needs to be combined with a ThirdPArty KWD Engine. Enable Third Party KWD on Qualcomm Device Configuration Tool and verify that your build has DSPC enabled.\n Exiting AVS Initialization !");
    return false;
  }
#else
  // Create Audiohal with Fluence Audio recorder client
  audio_recorder_ = std::make_shared<QAHWManager>(wake_word_, false, client_id_);
#endif
  if (audio_recorder_->Initialize() != 0)
    syslog(LOG_ERR, "AVSManager::Initialize() - ERROR During Audio Recorder Initialization -Exiting AVS Initialization !");

  //setup AVS SDK
  //TODO: EXIT APP if SDK SetUp Fails
  if (!setUpSDK()) {
    syslog(LOG_ERR, "AVSManager::Initialize() - ERROR During SDK Initialization - Exiting AVS Initialization !");
    return false;
  }

  if (audioplayer_mediaplayer_) {
    // create Intent Mapper function objects
    audioplayer_mediaplayer_->SetNextCb(std::bind(std::mem_fn(&AVSManager::IntentNextTrack), this));
    audioplayer_mediaplayer_->SetPreviousCb(std::bind(std::mem_fn(&AVSManager::IntentPreviousTrack), this));

    audioplayer_mediaplayer_->SetPlayCb(std::bind(std::mem_fn(&AVSManager::IntentPlay), this));
    audioplayer_mediaplayer_->SetPauseCb(std::bind(std::mem_fn(&AVSManager::IntentPause), this));

    audioplayer_mediaplayer_->SetOnMuteUpdatedCb(std::bind(std::mem_fn(&AVSManager::IntentOnMuteUpdated), this, std::placeholders::_1));
    audioplayer_mediaplayer_->SetOnVolumeUpdatedCb(std::bind(std::mem_fn(&AVSManager::IntentOnVolumeUpdated), this, std::placeholders::_1));
  }

  return true;
}

bool AVSManager::ValidateCredentials() {
  //AVS JSON file
  const char* conf_file = config_path_json_.c_str();
  //AVS onboard status
  bool avs_onboarded = false;

  // Read Database
  auto voice_ui_config = voiceUIFramework::voiceUIUtils::VoiceUIConfig::getInstance();
  //Read avs_onboarded
  if (voice_ui_config) {
    avs_onboarded = voice_ui_config->ReadWithDefault<bool>(false, voiceUIUtils::Keys::k_avs_onboarded_);
  } else {
    ConsolePrinter::prettyPrint("Fatal Error, Unable to access Database");
  }
  if (access(conf_file, F_OK) != -1) {
    std::ifstream alexaCfg(conf_file, std::ifstream::in);
    if (alexaCfg.peek() != EOF) {
      // file exists, proceed with sdk setup
      syslog(LOG_INFO, "AVSManager::ValidateCredentials(): Valid JSON file exists, proceed with AVS SDK setup");

      if (access(avs_cbl_auth_delegate_db_.c_str(), F_OK) != -1) {
        std::ifstream alexa_auth(avs_cbl_auth_delegate_db_, std::ifstream::in);
        if (alexa_auth.peek() != EOF) {
          // file exists, proceed with sdk setup
          syslog(LOG_INFO, "AVSManager::ValidateCredentials(): %s file exists, proceed with AVS SDK setup", avs_cbl_auth_delegate_db_.c_str());
          if (avs_onboarded) {
            syslog(LOG_INFO, "AVSManager::ValidateCredentials(): AVS already Onboarded - Credential validated");
            return true;
          }
        }
      }
    }
  }
  // No files are present, reset the database
  {
    IntentManagerEvent intent;
    //Reset the database (device might have got off-boarded from the cloud)
    if (voice_ui_config) {
      //write conboarding result
      if (voice_ui_config->Write<bool>(false, voiceUIUtils::Keys::k_avs_onboarded_)) {
        // Send database Updated
        intent = IntentManagerEvent::DB_FILE_UPDATED;
        IntentManagerEventExtended intent_ext;
        intent_ext.str_1 = voiceUIUtils::Keys::k_avs_onboarded_;
        if (client_observer_)
          client_observer_->SendIntent(VoiceUIClientID::AVS_SOLUTION, intent, intent_ext);

      } else {
        //TODO:
        ConsolePrinter::prettyPrint("Fatal Error, Unable to write to DB, Inconsistent");
      }
    } else {
      ConsolePrinter::prettyPrint("Fatal Error, Unable to access Database");
    }
  }
  syslog(LOG_INFO, "AVSManager::ValidateCredentials(): Waiting for AVS credential onboarding");

  /// play Prompt for Onboard
  IntentManagerEvent intent = IntentManagerEvent::AVS_ONBOARDING_PROMPT;
  if (client_observer_) {
    client_observer_->SendIntent(VoiceUIClientID::AVS_SOLUTION, intent);
  }

  int Onboard_prompt_count = 60;
  do {
    Onboard_prompt_count--;
    std::this_thread::sleep_for(std::chrono::milliseconds(2000));
    syslog(LOG_INFO, "AVSManager::ValidateCredentials() Waiting for Onboarding");
    if (Onboard_prompt_count == 0) {
      /// play Prompt for Onboard after every 2 minutes (60*2 = 120 seconds)
      IntentManagerEvent intent = IntentManagerEvent::AVS_ONBOARDING_PROMPT;
      if (client_observer_) {
        client_observer_->SendIntent(VoiceUIClientID::AVS_SOLUTION, intent);
      }
      Onboard_prompt_count = 60;
    }

    if (!initialization_lock_released_) {
      syslog(LOG_INFO, "AVSManager::ValidateCredentials() - AVS is IDLE after Initialization - ONBOARDED IS NEEDED - release VoiceUI Initialization wakelock.");
      //Release VoiceUI Initialization LOCK.
      system_wake_lock_toggle(false, WAKELOCK_VUI_INIT_NAME, WAKELOCK_NO_TIMEOUT);

      //Set system to suspend mode - TODO: MOVE LOGIC TO SYSTEM-MANAGER
      const std::string power_auto_sleep_filename = "/sys/power/autosleep";

      std::ofstream power_auto_sleep_file;
      power_auto_sleep_file.open(power_auto_sleep_filename);
      power_auto_sleep_file << "mem" << std::endl;

      if (power_auto_sleep_file.bad())
        syslog(LOG_INFO, "AVSManager::ValidateCredentials() - Failed to Set Autosleep");
      else
        syslog(LOG_INFO, "AVSManager::ValidateCredentials() - Success to Set Autosleep");

      power_auto_sleep_file.close();
      //Set system to suspend mode - TODO: MOVE LOGIC TO SYSTEM-MANAGER

      initialization_lock_released_ = true;
    }

  } while ((!onboard_requested_) && (!onboard_cancelled_));

  //Acquire VoiceUI AVS Lock for Onboarding.
  int delay = WAKELOCK_NO_TIMEOUT;
  system_wake_lock_toggle(true, WAKELOCK_VUI_INIT_NAME, delay);

  //Client was stopped even before the credentials from AVS were validated
  if (onboard_cancelled_) {
    syslog(LOG_INFO, "AVSManager::ValidateCredentials() AVS client is getting deactivated");
    return false;
  }

  syslog(LOG_INFO, "AVSManager::ValidateCredentials(): Creating JSON file %s", conf_file);

  //Creating JSON file using a rand() as serial number
  std::srand(time(0));
  long long unsigned int avs_serial_number, randMin = 1000000000, randMax = 9999999999;
  avs_serial_number = (std::rand() % (randMax - randMin + 1)) + randMin;

  //int avs_serial_number = std::rand();  //TO DO: replace by UNIQUE serial number from platform
  syslog(LOG_INFO, "AVSManager::ValidateCredentials(): Device Serial number: %llu and no.of digits=%d", avs_serial_number, (int)log10(avs_serial_number) + 1);

  std::string avs_serial_number_str = static_cast<std::ostringstream*>(&(std::ostringstream() << avs_serial_number))->str();
  //create final JSON string
  std::string json_content(avsJsonString_start_ + avs_serial_number_str + avsJsonString_client_id_ + avs_client_id_ + avsJsonString_product_id_ + avs_product_id_ + avsJsonString_auth_delegate__ + avs_cbl_auth_delegate_db_ + avsJsonString_final_);
  //wait
  std::this_thread::sleep_for(std::chrono::milliseconds(500));

  // create and write to conf_file
  int fdo = open(conf_file, O_CREAT | O_WRONLY, S_IRUSR | S_IWUSR);

  if (fdo > 0) {
    syslog(LOG_INFO, "AVSManager::ValidateCredentials(): writing credentials to %s \n", conf_file);

    write(fdo, json_content.c_str(), json_content.length());
    close(fdo);

    //      has_new_refreshtoken_ = true;
    printf("\nAVSManager::ValidateCredentials(): Valid JSON file created, proceed with AVS SDK setup");
  } else {
    printf("\nAVSManager::ValidateCredentials(): Valid JSON file exists, proceed with AVS SDK setup");
  }

  syslog(LOG_INFO, "AVSManager::ValidateCredentials - new JSON = %s", json_content.c_str());

  //wait to make sure that file was created
  std::this_thread::sleep_for(std::chrono::milliseconds(500));

  return true;
}

bool AVSManager::StartClient() {
  bool ret = true;

  syslog(LOG_INFO, "AVSManager::StartClient()");

  voiceUIFramework::avsManager::ConsolePrinter::simplePrint(
      "QTIAVSApp:: run() - Running Alexa APP with log level: "
      + alexaClientSDK::avsCommon::utils::logger::convertLevelToName(alexaClientSDK::avsCommon::utils::logger::Level::DEBUG9));

  return ret;
}

bool AVSManager::StopClient() {
  bool ret = true;
  onboard_cancelled_ = true;

  syslog(LOG_INFO, "AVSManager::StopClient()");

  return ret;
}

void AVSManager::IntentReceived(IntentManagerEvent intent) {
  syslog(LOG_INFO, "AVSManager::IntentReceived - intent Received: %s", IntentManagerEventToString(intent).c_str());

  if (IsActive()) {
    if (intent == IntentManagerEvent::TAP_BUTTON_COMMAND || intent == IntentManagerEvent::TAP_BUTTON_COMMAND_AVS) {
      interaction_manager_->tap();
    } else if (intent == IntentManagerEvent::VOL_UP_EVENT) {
      interaction_manager_->adjustVolume(SpeakerInterface::Type::AVS_SPEAKER_VOLUME, INCREASE_VOLUME);
      interaction_manager_->adjustVolume(SpeakerInterface::Type::AVS_ALERTS_VOLUME, INCREASE_VOLUME);
      if (mute_) {
        mute_ = false;
      }
    } else if (intent == IntentManagerEvent::VOL_DOWN_EVENT) {
      interaction_manager_->adjustVolume(SpeakerInterface::Type::AVS_SPEAKER_VOLUME, DECREASE_VOLUME);
      interaction_manager_->adjustVolume(SpeakerInterface::Type::AVS_ALERTS_VOLUME, DECREASE_VOLUME);
      if (mute_) {
        mute_ = false;
      }
    } else if (intent == IntentManagerEvent::MUTE_EVENT) {
      mute_ = !mute_;
      interaction_manager_->setMute(SpeakerInterface::Type::AVS_SPEAKER_VOLUME, mute_);
      interaction_manager_->setMute(SpeakerInterface::Type::AVS_ALERTS_VOLUME, mute_);
    } else {
      syslog(LOG_INFO, "AVSManager::IntentReceived - unknown intent/unhandled %s", IntentManagerEventToString(intent).c_str());
    }
  } else {
    syslog(LOG_ERR, "AVSManager::IntentReceived - AVS Client is not Active, Not Handling the intent %s", IntentManagerEventToString(intent).c_str());
  }
}

void AVSManager::IntentReceived(IntentManagerEvent intent, IntentManagerEventExtended intent_ext) {
  syslog(LOG_INFO, "AVSManager::IntentManagerEventExtended - intent Received: %s", IntentManagerEventToString(intent).c_str());
  if (IsActive()) {
    if (intent == IntentManagerEvent::SET_AVS_LOCALE) {
      syslog(LOG_INFO, "AVSManager::IntentReceived - AVS_LOCALE: default_client_->changeSetting() to = %s", intent_ext.str_1.c_str());
      default_client_->changeSetting("locale", intent_ext.str_1);

      auto voice_ui_config = voiceUIFramework::voiceUIUtils::VoiceUIConfig::getInstance();
      IntentManagerEvent intent;

      //Update the database
      if (voice_ui_config) {
        //write locale result
        if (voice_ui_config->Write<std::string>(intent_ext.str_1, voiceUIUtils::Keys::k_avs_locale_)) {
          // Send database Updated
          intent = IntentManagerEvent::DB_FILE_UPDATED;
          IntentManagerEventExtended intent_ext;
          intent_ext.str_1 = voiceUIUtils::Keys::k_avs_locale_;
          if (client_observer_)
            client_observer_->SendIntent(VoiceUIClientID::AVS_SOLUTION, intent, intent_ext);

        } else {
          //TODO:
          ConsolePrinter::prettyPrint("Fatal Error, Unable to write to DB, Inconsistent");
        }
      } else {
        ConsolePrinter::prettyPrint("Fatal Error, Unable to access Database");
      }

    } else if (intent == IntentManagerEvent::DELETE_CREDENTIAL) {
      syslog(LOG_INFO, "****** AVSManager::IntentReceived() - DELETE_CREDENTIAL");
      //Reset AVS Authentication
      interaction_manager_->resetDevice();

      auto voice_ui_config = voiceUIFramework::voiceUIUtils::VoiceUIConfig::getInstance();
      IntentManagerEvent intent;

      //Reset the database
      if (voice_ui_config) {
        //write conboarding result
        if (voice_ui_config->Write<bool>(false, voiceUIUtils::Keys::k_avs_onboarded_)) {
          // Send database Updated
          intent = IntentManagerEvent::DB_FILE_UPDATED;
          IntentManagerEventExtended intent_ext;
          intent_ext.str_1 = voiceUIUtils::Keys::k_avs_onboarded_;
          if (client_observer_)
            client_observer_->SendIntent(VoiceUIClientID::AVS_SOLUTION, intent, intent_ext);

        } else {
          //TODO:
          ConsolePrinter::prettyPrint("Fatal Error, Unable to write to DB, Inconsistent");
        }
      } else {
        ConsolePrinter::prettyPrint("Fatal Error, Unable to access Database");
      }

    } else {
      syslog(LOG_INFO, "AVSManager::IntentReceived - unknown/unhandled intent %s", IntentManagerEventToString(intent).c_str());
    }
  } else {
    // Check if there is internet, otherwise report error
    if (intent == IntentManagerEvent::ONBOARDING_REQUESTED) {
      // Check for Internet and Proceed
      if (CheckForInternet()) {
        onboard_requested_ = true;
        onboard_cancelled_ = false;
        syslog(LOG_INFO, "AVSManager::IntentReceived - AVS Onboarding has started");
      } else {
        IntentManagerEvent intent_iot;
        intent_iot = IntentManagerEvent::ONBOARDING_ERROR;
        IntentManagerEventExtended intent_iot_ext;
        intent_iot_ext.str_1 = "AVS";
        intent_iot_ext.str_2 = "No Internet";
        intent_iot_ext.value_uint = 0;
        if (client_observer_)
          client_observer_->SendIntent(VoiceUIClientID::AVS_SOLUTION, intent_iot, intent_iot_ext);
      }
    } else {
      syslog(LOG_ERR, "AVSManager::IntentReceived - AVS Client is not Active, Not Handling the intent %s", IntentManagerEventToString(intent).c_str());
    }
  }
}

void AVSManager::IntentNextTrack(void) {
  syslog(LOG_INFO, "AVSManager : OnNextTrack");
  if (interaction_manager_) {
    interaction_manager_->playbackNext();
  }
}

void AVSManager::IntentPreviousTrack(void) {
  syslog(LOG_INFO, "AVSManager : OnPreviousTrack");
  if (interaction_manager_) {
    interaction_manager_->playbackPrevious();
  }
}

void AVSManager::IntentPlay(void) {
  syslog(LOG_INFO, "AVSManager : OnPlay");
  if (interaction_manager_) {
    interaction_manager_->playbackPlay();
  }
}

void AVSManager::IntentPause(void) {
  syslog(LOG_INFO, "AVSManager : OnPause");
  if (interaction_manager_) {
    interaction_manager_->playbackPause();
  }
}

void AVSManager::IntentOnMuteUpdated(bool mute_state) {
  syslog(LOG_INFO, "AVSManager : OnMuteUpdated");
  if (interaction_manager_) {
    syslog(LOG_INFO, "AVSManager::OnMuteUpdated -  - mute %d", mute_state);
    interaction_manager_->setMute(SpeakerInterface::Type::AVS_SPEAKER_VOLUME, mute_state);
    interaction_manager_->setMute(SpeakerInterface::Type::AVS_ALERTS_VOLUME, mute_state);
  }
}

void AVSManager::IntentOnVolumeUpdated(double volume) {
  syslog(LOG_INFO, "AVSManager : OnVolumeUpdated");
  if (interaction_manager_) {
    syslog(LOG_INFO, "AVSManager::OnVolumeUpdated volume= %f", volume);
    auto toAVSVolume =
        Normalizer::create(GST_SET_VOLUME_MIN, GST_SET_VOLUME_MAX, AVS_SET_VOLUME_MIN, AVS_SET_VOLUME_MAX);
    if (!toAVSVolume) {
      syslog(LOG_INFO, "AVSManager::OnVolumeUpdated Failed reason : createNormalizerFailed");
      return;
    }

    double tempVolume;

    if (!toAVSVolume->normalize(volume, &tempVolume)) {
      syslog(LOG_INFO, "AVSManager::OnVolumeUpdated Failed reason : normalizeVolumeFailed");
      return;
    }

    int8_t avs_volume = static_cast<int8_t>(std::round(tempVolume));
    syslog(LOG_INFO, "AVSManager::OnVolumeUpdated - volume %d", avs_volume);
    interaction_manager_->setVolume(SpeakerInterface::Type::AVS_SPEAKER_VOLUME, avs_volume, false);
    interaction_manager_->setVolume(SpeakerInterface::Type::AVS_ALERTS_VOLUME, avs_volume, false);
  }
}

void AVSManager::AppMessageReceived(AppMessage app_message) {
  syslog(LOG_DEBUG, "AVSManager::AppMessageReceived() = %s", AppMessageToString(app_message).c_str());
}

bool AVSManager::setUpSDK() {
  syslog(LOG_INFO, "AVSManager::setUpSDK()");
  voiceUIFramework::avsManager::ConsolePrinter::simplePrint("AVSManager::setUpSDK()");

  /*
     * Set up the SDK logging system to write to the SampleApp's ConsolePrinter.  Also adjust the logging level
     * if requested.
     */
  std::shared_ptr<alexaClientSDK::avsCommon::utils::logger::Logger> consolePrinter = std::make_shared<voiceUIFramework::avsManager::ConsolePrinter>();

  //Define AVS LOG Level
  avsCommon::utils::logger::Level logLevelValue = avsCommon::utils::logger::Level::DEBUG9;
  voiceUIFramework::avsManager::ConsolePrinter::simplePrint(
      "Running app with log level: " + alexaClientSDK::avsCommon::utils::logger::convertLevelToName(logLevelValue));
  consolePrinter->setLevel(logLevelValue);
  alexaClientSDK::avsCommon::utils::logger::LoggerSinkManager::instance().initialize(consolePrinter);

  // Read Credential JSON file
  configFiles.push_back(config_path_json_);
  std::vector<std::shared_ptr<std::istream>> configJsonStreams;
  for (auto configFile : configFiles) {
    if (configFile.empty()) {
      voiceUIFramework::avsManager::ConsolePrinter::simplePrint("Config filename is empty!");
      return false;
    }

    auto configInFile = std::shared_ptr<std::ifstream>(new std::ifstream(configFile));
    if (!configInFile->good()) {
      voiceUIFramework::avsManager::ConsolePrinter::simplePrint("Failed to initialize SDK! File: " + configFile);
      syslog(LOG_ERR, "Failed to read config file %s", configFile.c_str());
      return false;
    }

    configJsonStreams.push_back(configInFile);
  }

  if (!avsCommon::avs::initialization::AlexaClientSDKInit::initialize(configJsonStreams)) {
    voiceUIFramework::avsManager::ConsolePrinter::simplePrint("Failed to initialize SDK!");
    syslog(LOG_ERR, "Failed to initialize SDK!");
    return false;
  }

  auto config = alexaClientSDK::avsCommon::utils::configuration::ConfigurationNode::getRoot();
  auto sampleAppConfig = config[SAMPLE_APP_CONFIG_KEY];
  auto http_content_fetcher_factory = std::make_shared<avsCommon::utils::libcurlUtils::HTTPContentFetcherFactory>();
  // Creating the misc DB object to be used by various components.
  std::shared_ptr<alexaClientSDK::storage::sqliteStorage::SQLiteMiscStorage> miscStorage =
      alexaClientSDK::storage::sqliteStorage::SQLiteMiscStorage::create(config);

  /*
     * Creating Equalizer specific implementations
     */
  auto equalizerConfigBranch = config[EQUALIZER_CONFIG_KEY];
  auto equalizerConfiguration = equalizer::SDKConfigEqualizerConfiguration::create(equalizerConfigBranch);
  std::shared_ptr<defaultClient::EqualizerRuntimeSetup> equalizerRuntimeSetup = nullptr;

  bool equalizerEnabled = false;
  if (equalizerConfiguration && equalizerConfiguration->isEnabled()) {
    equalizerEnabled = true;
    equalizerRuntimeSetup = std::make_shared<defaultClient::EqualizerRuntimeSetup>();
    auto equalizerStorage = equalizer::MiscDBEqualizerStorage::create(miscStorage);
    auto equalizerModeController = SampleEqualizerModeController::create();

    equalizerRuntimeSetup->setStorage(equalizerStorage);
    equalizerRuntimeSetup->setConfiguration(equalizerConfiguration);
    equalizerRuntimeSetup->setModeController(equalizerModeController);
  }

  std::shared_ptr<alexaClientSDK::avsCommon::sdkInterfaces::SpeakerInterface> speakSpeaker;
  std::tie(speaker_mediaplayer_, speakSpeaker) = createApplicationMediaPlayer(
      http_content_fetcher_factory,
      false,
      avsCommon::sdkInterfaces::SpeakerInterface::Type::AVS_SPEAKER_VOLUME,
      "SpeakMediaPlayer");
  if (!speaker_mediaplayer_ || !speakSpeaker) {
    voiceUIFramework::avsManager::ConsolePrinter::simplePrint("Failed to create media player for SpeechSynthesizer!");
    return false;
  }

  std::shared_ptr<alexaClientSDK::avsCommon::sdkInterfaces::SpeakerInterface> audioSpeaker;
  std::tie(audioplayer_mediaplayer_, audioSpeaker) = createApplicationMediaPlayer(
      http_content_fetcher_factory,
      equalizerEnabled,
      avsCommon::sdkInterfaces::SpeakerInterface::Type::AVS_SPEAKER_VOLUME,
      "AudioMediaPlayer");
  if (!audioplayer_mediaplayer_ || !audioSpeaker) {
    voiceUIFramework::avsManager::ConsolePrinter::simplePrint("Failed to create media player for AudioPlayer!");
    return false;
  }

  std::shared_ptr<alexaClientSDK::avsCommon::sdkInterfaces::SpeakerInterface> notificationsSpeaker;
  std::tie(notifications_mediaplayer_, notificationsSpeaker) = createApplicationMediaPlayer(
      http_content_fetcher_factory,
      false,
      avsCommon::sdkInterfaces::SpeakerInterface::Type::AVS_ALERTS_VOLUME,
      "NotificationsMediaPlayer");
  if (!notifications_mediaplayer_ || !notificationsSpeaker) {
    voiceUIFramework::avsManager::ConsolePrinter::simplePrint("Failed to create media player for Notifications!");
    return false;
  }

  std::shared_ptr<alexaClientSDK::avsCommon::sdkInterfaces::SpeakerInterface> bluetoothSpeaker;
  std::tie(bluetooth_mediaplayer_, bluetoothSpeaker) = createApplicationMediaPlayer(
      http_content_fetcher_factory,
      false,
      avsCommon::sdkInterfaces::SpeakerInterface::Type::AVS_SPEAKER_VOLUME,
      "BluetoothMediaPlayer");

  syslog(LOG_INFO, "AVSManager::setUpSDK() 12");
  if (!bluetooth_mediaplayer_ || !bluetoothSpeaker) {
    voiceUIFramework::avsManager::ConsolePrinter::simplePrint("Failed to create media player for Bluetooth!");
    return false;
  }

  std::shared_ptr<alexaClientSDK::avsCommon::sdkInterfaces::SpeakerInterface> ringtoneSpeaker;
  std::tie(ringtone_mediaplayer_, ringtoneSpeaker) = createApplicationMediaPlayer(
      http_content_fetcher_factory,
      false,
      avsCommon::sdkInterfaces::SpeakerInterface::Type::AVS_SPEAKER_VOLUME,
      "RingtoneMediaPlayer");
  if (!ringtone_mediaplayer_ || !ringtoneSpeaker) {
    voiceUIFramework::avsManager::ConsolePrinter::simplePrint("Failed to create media player for Ringtones!");
    return false;
  }

#ifdef ENABLE_COMMS_AUDIO_PROXY
  std::shared_ptr<alexaClientSDK::avsCommon::sdkInterfaces::SpeakerInterface> commsSpeaker;
  std::tie(m_commsMediaPlayer, commsSpeaker) = createApplicationMediaPlayer(
      httpContentFetcherFactory,
      false,
      avsCommon::sdkInterfaces::SpeakerInterface::Type::AVS_SPEAKER_VOLUME,
      "CommsMediaPlayer",
      true);
  if (!m_commsMediaPlayer || !commsSpeaker) {
    ACSDK_CRITICAL(LX("Failed to create media player for comms!"));
    return false;
  }
#endif

  std::shared_ptr<alexaClientSDK::avsCommon::sdkInterfaces::SpeakerInterface> alertsSpeaker;
  std::tie(alerts_mediaplayer_, alertsSpeaker) = createApplicationMediaPlayer(
      http_content_fetcher_factory,
      false,
      avsCommon::sdkInterfaces::SpeakerInterface::Type::AVS_ALERTS_VOLUME,
      "AlertsMediaPlayer");
  if (!alerts_mediaplayer_ || !alertsSpeaker) {
    voiceUIFramework::avsManager::ConsolePrinter::simplePrint("Failed to create media player for Alerts!");
    return false;
  }

#ifdef ENABLE_PCC
  std::shared_ptr<alexaClientSDK::avsCommon::sdkInterfaces::SpeakerInterface> phoneSpeaker;
  std::shared_ptr<ApplicationMediaPlayer> phoneMediaPlayer;
  std::tie(phoneMediaPlayer, phoneSpeaker) = createApplicationMediaPlayer(
      httpContentFetcherFactory,
      false,
      avsCommon::sdkInterfaces::SpeakerInterface::Type::AVS_SPEAKER_VOLUME,
      "PhoneMediaPlayer");

  if (!phoneMediaPlayer || !phoneSpeaker) {
    ACSDK_CRITICAL(LX("Failed to create media player for phone!"));
    return false;
  }
#endif

  std::vector<std::shared_ptr<avsCommon::sdkInterfaces::SpeakerInterface>> additionalSpeakers;

  if (!createMediaPlayersForAdapters(http_content_fetcher_factory, equalizerRuntimeSetup, additionalSpeakers)) {
    voiceUIFramework::avsManager::ConsolePrinter::simplePrint("ERROR: Could not create mediaPlayers for adapters");
    return false;
  }
  auto audioFactory = std::make_shared<alexaClientSDK::applicationUtilities::resources::audio::AudioFactory>();
  // Creating equalizers
  if (nullptr != equalizerRuntimeSetup) {
    equalizerRuntimeSetup->addEqualizer(audioplayer_mediaplayer_);
  }

  // Creating the alert storage object to be used for rendering and storing alerts.
  auto alert_storage = alexaClientSDK::capabilityAgents::alerts::storage::SQLiteAlertStorage::create(config, audioFactory->alerts());
  // Creating the message storage object to be used for storing message to be sent later.
  auto message_storage = alexaClientSDK::certifiedSender::SQLiteMessageStorage::create(config);

  /*
     * Creating notifications storage object to be used for storing notification indicators.
     */
  auto notifications_storage = alexaClientSDK::capabilityAgents::notifications::SQLiteNotificationsStorage::create(config);
  /*
   * Creating settings storage object to be used for storing <key, value> pairs of AVS Settings (DEPRECATED).
   */
  auto settings_storage = alexaClientSDK::capabilityAgents::settings::SQLiteSettingStorage::create(config);

  /*
   * Creating new device settings storage object to be used for storing AVS Settings.
   */
  auto device_settings_storage = alexaClientSDK::settings::storage::SQLiteDeviceSettingStorage::create(config);

  // Create HTTP Put handler
  std::shared_ptr<avsCommon::utils::libcurlUtils::HttpPut> httpPut =
      avsCommon::utils::libcurlUtils::HttpPut::create();

  /*
   * Creating bluetooth storage object to be used for storing uuid to mac mappings for devices.
   */
  //REMOVING BT auto bluetoothStorage = alexaClientSDK::capabilityAgents::bluetooth::SQLiteBluetoothStorage::create(config);

  /*
     * Creating the UI component that observes various components and prints to the console accordingly.
     */
  userinterface_manager_ = std::make_shared<voiceUIFramework::avsManager::UIManager>();

  //Adding QTI interfaces
  userinterface_manager_->addQTIInterfaces(wake_word_, audio_recorder_, client_observer_);

  /*
   * Creating customerDataManager which will be used by the registrationManager and all classes that extend
   * CustomerDataHandler
   */
  auto customerDataManager = std::make_shared<registrationManager::CustomerDataManager>();

#ifdef ENABLE_PCC
  auto phoneCaller = std::make_shared<alexaClientSDK::sampleApp::PhoneCaller>();
#endif

  /*
   * Creating the deviceInfo object
   */
  std::shared_ptr<avsCommon::utils::DeviceInfo> deviceInfo = avsCommon::utils::DeviceInfo::create(config);
  if (!deviceInfo) {
    ACSDK_CRITICAL(LX("Creation of DeviceInfo failed!"));
    return false;
  }

  /*
   * Creating the AuthDelegate - this component takes care of LWA and authorization of the client.
   */
  auto authDelegateStorage = authorization::cblAuthDelegate::SQLiteCBLAuthDelegateStorage::create(config);

  std::shared_ptr<avsCommon::sdkInterfaces::AuthDelegateInterface> authDelegate =
      authorization::cblAuthDelegate::CBLAuthDelegate::create(
          config, customerDataManager, std::move(authDelegateStorage), userinterface_manager_, nullptr, deviceInfo);

  if (!authDelegate) {
    ACSDK_CRITICAL(LX("Creation of AuthDelegate failed!"));
    return false;
  }

  /*
   * Creating the CapabilitiesDelegate - This component provides the client with the ability to send messages to the
   * Capabilities API.
   */
  capabilities_delegate_ = alexaClientSDK::capabilitiesDelegate::CapabilitiesDelegate::create(
      authDelegate, miscStorage, httpPut, customerDataManager, config, deviceInfo);

  if (!capabilities_delegate_) {
    voiceUIFramework::avsManager::ConsolePrinter::simplePrint("Creation of CapabilitiesDelegate failed!");
    return false;
  }

  authDelegate->addAuthObserver(userinterface_manager_);
  capabilities_delegate_->addCapabilitiesObserver(userinterface_manager_);

  // INVALID_FIRMWARE_VERSION is passed to @c getInt() as a default in case FIRMWARE_VERSION_KEY is not found.
  int firmwareVersion = static_cast<int>(avsCommon::sdkInterfaces::softwareInfo::INVALID_FIRMWARE_VERSION);
  sampleAppConfig.getInt(FIRMWARE_VERSION_KEY, &firmwareVersion, firmwareVersion);

  /*
     * Check to see if displayCards is supported on the device. The default is supported unless specified otherwise in
     * the configuration.
     */
  bool displayCardsSupported;
  config[SAMPLE_APP_CONFIG_KEY].getBool(DISPLAY_CARD_KEY, &displayCardsSupported, true);

  /*
   * Creating the InternetConnectionMonitor that will notify observers of internet connection status changes.
   */
  auto internetConnectionMonitor =
      avsCommon::utils::network::InternetConnectionMonitor::create(http_content_fetcher_factory);
  if (!internetConnectionMonitor) {
    ACSDK_CRITICAL(LX("Failed to create InternetConnectionMonitor"));
    return false;
  }

  if (speaker_mediaplayer_ && audioplayer_mediaplayer_ && alerts_mediaplayer_ && notifications_mediaplayer_ && bluetooth_mediaplayer_ && ringtone_mediaplayer_) {
    voiceUIFramework::avsManager::ConsolePrinter::simplePrint("All 6 QTIMediaPlayer(adk-mediaplayer) are valid");
  }

  /*
     * Creating the Context Manager - This component manages the context of each of the components to update to AVS.
     * It is required for each of the capability agents so that they may provide their state just before any event is
     * fired off.
     */
  auto contextManager = contextManager::ContextManager::create();
  if (!contextManager) {
    ACSDK_CRITICAL(LX("Creation of ContextManager failed."));
    return false;
  }

  /*
     * Create a factory for creating objects that handle tasks that need to be performed right after establishing
     * a connection to AVS.
     */
  auto postConnectSynchronizerFactory = acl::PostConnectSynchronizerFactory::create(contextManager);

  /*
     * Create a factory to create objects that establish a connection with AVS.
     */
  auto transportFactory = std::make_shared<acl::HTTP2TransportFactory>(
      std::make_shared<avsCommon::utils::libcurlUtils::LibcurlHTTP2ConnectionFactory>(),
      postConnectSynchronizerFactory);

  /*
     * Creating the buffer (Shared Data Stream) that will hold user audio data. This is the main input into the SDK.
     */
  size_t buffer_size = alexaClientSDK::avsCommon::avs::AudioInputStream::calculateBufferSize(BUFFER_SIZE_IN_SAMPLES, WORD_SIZE, MAX_READERS);
  auto aip_buffer = std::make_shared<alexaClientSDK::avsCommon::avs::AudioInputStream::Buffer>(buffer_size);
  std::shared_ptr<alexaClientSDK::avsCommon::avs::AudioInputStream> aip_stream = alexaClientSDK::avsCommon::avs::AudioInputStream::create(
      aip_buffer, WORD_SIZE, MAX_READERS);

  if (!aip_stream) {
    syslog(LOG_ERR, "AVSManager::setUpSDK() - Failed to create Audio Input Processor shared data stream!");
  }

  /*
  * Create the BluetoothDeviceManager to communicate with the Bluetooth stack.
  */
  //std::unique_ptr<avsCommon::sdkInterfaces::bluetooth::BluetoothDeviceManagerInterface> bluetoothDeviceManager;
  //#ifdef BLUETOOTH_BLUEZ
  //    auto eventBus = std::make_shared<avsCommon::utils::bluetooth::BluetoothEventBus>();
  //
  //#ifdef BLUETOOTH_BLUEZ_PULSEAUDIO_OVERRIDE_ENDPOINTS
  //    /*
  //     * Create PulseAudio initializer object. Subscribe to BLUETOOTH_DEVICE_MANAGER_INITIALIZED event before we create
  //     * the BT Device Manager, otherwise may miss it.
  //     */
  //    m_pulseAudioInitializer = bluetoothImplementations::blueZ::PulseAudioBluetoothInitializer::create(eventBus);
  //#endif
  //
  //    bluetoothDeviceManager = bluetoothImplementations::blueZ::BlueZBluetoothDeviceManager::create(eventBus);
  //#endif

  /*
       * Creating the DefaultClient - this component serves as an out-of-box default object that instantiates and "glues"
       * together all the modules.
       */
  default_client_ = alexaClientSDK::defaultClient::DefaultClient::create(
      deviceInfo,
      customerDataManager,
      external_music_provider_mediaplayers_map_,
      external_music_provider_speakers_map_,
      adapter_to_create_func_map_,
      speaker_mediaplayer_, audioplayer_mediaplayer_, alerts_mediaplayer_, notifications_mediaplayer_,
      bluetooth_mediaplayer_,
      ringtone_mediaplayer_,
      speakSpeaker,
      audioSpeaker,
      alertsSpeaker,
      notificationsSpeaker,
      bluetoothSpeaker,
      ringtoneSpeaker,
      additionalSpeakers,
#ifdef ENABLE_PCC
      phoneSpeaker,
      phoneCaller,
#endif
#ifdef ENABLE_COMMS_AUDIO_PROXY
      m_commsMediaPlayer,
      commsSpeaker,
      sharedDataStream,
#endif
      equalizerRuntimeSetup,
      audioFactory,
      authDelegate,
      std::move(alert_storage),
      std::move(message_storage),
      std::move(notifications_storage),
      std::move(settings_storage),
      std::move(device_settings_storage),
      nullptr,  //std::move(bluetoothStorage),
      {userinterface_manager_},
      {userinterface_manager_},
      std::move(internetConnectionMonitor),
      displayCardsSupported,
      capabilities_delegate_,
      contextManager,
      transportFactory,
      firmwareVersion,
      true,
      nullptr,
      nullptr);  // std::move(bluetoothDeviceManager)

  if (!default_client_) {
    voiceUIFramework::avsManager::ConsolePrinter::simplePrint("Failed to create default SDK client!");
    syslog(LOG_ERR, " AVSManager::setUpSDK() - Failed to create default SDK client!");
    return false;
  }

  // Add userInterfaceManager as observer of locale setting.
  default_client_->addSettingObserver("locale", userinterface_manager_);

  default_client_->addSpeakerManagerObserver(userinterface_manager_);

  //default_client_->addBluetoothDeviceObserver(userinterface_manager_);
  userinterface_manager_->configureSettingsNotifications(default_client_->getSettingsManager());

  /*
       * Add GUI Renderer as an observer if display cards are supported.
       */
  if (displayCardsSupported) {
    guiRenderer_ = std::make_shared<GuiRenderer>();
    default_client_->addTemplateRuntimeObserver(guiRenderer_);
  }

  alexaClientSDK::avsCommon::utils::AudioFormat compatible_audio_format;
  compatible_audio_format.sampleRateHz = SAMPLE_RATE_HZ;
  compatible_audio_format.sampleSizeInBits = WORD_SIZE * CHAR_BIT;
  compatible_audio_format.numChannels = NUM_CHANNELS;
  compatible_audio_format.endianness = alexaClientSDK::avsCommon::utils::AudioFormat::Endianness::LITTLE;
  compatible_audio_format.encoding = alexaClientSDK::avsCommon::utils::AudioFormat::Encoding::LPCM;

  /*
     * Creating each of the audio providers. An audio provider is a simple package of data consisting of the stream
     * of audio data, as well as metadata about the stream. For each of the three audio providers created here, the same
     * stream is used since this sample application will only have one microphone.
     */

  // Creating tap to talk audio provider
  bool tapAlwaysReadable = false;  //QTI was:true;
  bool tapCanOverride = true;
  bool tapCanBeOverridden = true;

  alexaClientSDK::capabilityAgents::aip::AudioProvider tapToTalkAudioProvider(aip_stream, compatible_audio_format,
      alexaClientSDK::capabilityAgents::aip::ASRProfile::NEAR_FIELD, tapAlwaysReadable, tapCanOverride, tapCanBeOverridden);

  // Creating hold to talk audio provider
  bool holdAlwaysReadable = false;
  bool holdCanOverride = true;
  bool holdCanBeOverridden = false;

  alexaClientSDK::capabilityAgents::aip::AudioProvider holdToTalkAudioProvider(aip_stream, compatible_audio_format,
      alexaClientSDK::capabilityAgents::aip::ASRProfile::CLOSE_TALK, holdAlwaysReadable, holdCanOverride, holdCanBeOverridden);

  // Creating wake word audio provider, if necessary
  bool wakeAlwaysReadable = false;  //QTI was:true;
  bool wakeCanOverride = true;      //QTI was:false;
  bool wakeCanBeOverridden = true;

  alexaClientSDK::capabilityAgents::aip::AudioProvider wakeWordAudioProvider(aip_stream, compatible_audio_format,
      alexaClientSDK::capabilityAgents::aip::ASRProfile::NEAR_FIELD, wakeAlwaysReadable, wakeCanOverride, wakeCanBeOverridden);

#ifdef ENABLE_ESP
  // Creating ESP connector
  std::shared_ptr<esp::ESPDataProviderInterface> espProvider = esp::ESPDataProvider::create(wakeWordAudioProvider);
  std::shared_ptr<esp::ESPDataModifierInterface> espModifier = nullptr;
#else
  // Create dummy ESP connector
  auto dummyEspProvider = std::make_shared<esp::DummyESPDataProvider>();
  std::shared_ptr<esp::ESPDataProviderInterface> espProvider = dummyEspProvider;
  std::shared_ptr<esp::ESPDataModifierInterface> espModifier = dummyEspProvider;
#endif

  //Create Observer for AudioRecoder
  avs_audiorecorder_observer_ = std::make_shared<voiceUIFramework::avsManager::AVSAudioRecorderObserver>(aip_stream, default_client_,
      tapToTalkAudioProvider);
  if (audio_recorder_)
    audio_recorder_->SetCallBackObserver(avs_audiorecorder_observer_);

  //Create and configure WakeWord Observer for AVS client
  avs_wakeword_observer_ = std::make_shared<voiceUIFramework::avsManager::AVSWakeWordObserver>(aip_stream, default_client_, wake_word_);
  if (!wake_word_ || !wake_word_->AddObserver(client_id_, avs_wakeword_observer_)) {
    syslog(LOG_ERR, "AVSManager::setUpSDK() - Wakeword fail to initialize");
    return false;
  }

  interaction_manager_ = std::make_shared<voiceUIFramework::avsManager::InteractionManager>(
      default_client_,
      userinterface_manager_,
#ifdef ENABLE_PCC
      phoneCaller,
#endif
      holdToTalkAudioProvider,
      tapToTalkAudioProvider,
      guiRenderer_,
      wakeWordAudioProvider,
      espProvider,
      espModifier);

  default_client_->addAlexaDialogStateObserver(interaction_manager_);
  default_client_->addAlertsObserver(userinterface_manager_);
  default_client_->addNotificationsObserver(userinterface_manager_);

  // Creating the revoke authorization observer.
  auto revokeObserver =
      std::make_shared<RevokeAuthorizationObserver>(default_client_->getRegistrationManager());
  default_client_->addRevokeAuthorizationObserver(revokeObserver);
  default_client_->getRegistrationManager()->addObserver(userinterface_manager_);

  capabilities_delegate_->addCapabilitiesObserver(default_client_);

  // Connect once configuration is all set.
  std::string endpoint;
  sampleAppConfig.getString(ENDPOINT_KEY, &endpoint);

  default_client_->connect(capabilities_delegate_, endpoint);

  // Send default settings set by the user to AVS.
  default_client_->sendDefaultSettings();

  syslog(LOG_INFO, "AVS Client (Version = %s) is READY!", AVS_VERSION.c_str());
  return true;
}

bool AVSManager::CheckForInternet() {
  syslog(LOG_INFO, "AVSManager::CheckForInternet()");

  CURL* curl = NULL;
  CURLcode ret;
  bool result = false;

  curl = curl_easy_init();
  if (curl) {
    //check AMAZON server
    curl_easy_setopt(curl, CURLOPT_URL, HOST_ADDR_AMAZON);
    curl_easy_setopt(curl, CURLOPT_VERBOSE, 0);

    //Do not get the Body
    //TODO: Add write call back that redirects contents to syslog if there is error
    curl_easy_setopt(curl, CURLOPT_NOBODY, 1);

    ret = curl_easy_perform(curl);
    if (ret == CURLE_OK) {
      result = true;
    }
    curl_easy_cleanup(curl);
  }
  syslog(LOG_INFO, "AVSManager::CheckForInternet() - Result = %d", result);

  return result;
}

std::pair<std::shared_ptr<QTIMediaPlayer>, std::shared_ptr<avsCommon::sdkInterfaces::SpeakerInterface>>
AVSManager::createApplicationMediaPlayer(
    std::shared_ptr<avsCommon::utils::libcurlUtils::HTTPContentFetcherFactory> http_content_fetcher_factory,
    bool enableEqualizer,
    avsCommon::sdkInterfaces::SpeakerInterface::Type type,
    const std::string& name,
    bool enableLiveMode) {
  /*
       * For the SDK, the MediaPlayer happens to also provide volume control functionality.
       * Note the externalMusicProviderMediaPlayer is not added to the set of SpeakerInterfaces as there would be
       * more actions needed for these beyond setting the volume control on the MediaPlayer.
       */
  auto mediaPlayer =
      QTIMediaPlayer::create(http_content_fetcher_factory, enableEqualizer, type, name, enableLiveMode);
  return {mediaPlayer,
      std::static_pointer_cast<alexaClientSDK::avsCommon::sdkInterfaces::SpeakerInterface>(mediaPlayer)};
}

}  // namespace avsManager
}  // namespace voiceUIFramework
