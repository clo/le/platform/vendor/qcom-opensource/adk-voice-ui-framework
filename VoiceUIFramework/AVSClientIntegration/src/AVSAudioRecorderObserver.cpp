/* Copyright (c) 2018, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    AVSAudioRecorderObserver.cpp
 *  @brief   brief description
 *
 *  DESCRIPTION
 *    description
 ***************************************************************/

#include <AVSClientIntegration/AVSAudioRecorderObserver.h>

namespace voiceUIFramework {
namespace avsManager {

using namespace alexaClientSDK;
using namespace alexaClientSDK::capabilityAgents::aip;
using namespace alexaClientSDK::avsCommon::sdkInterfaces;
using namespace alexaClientSDK::avsCommon::avs;

#define OK 0;
std::unique_ptr<AudioInputStream::Writer> AVSAudioRecorderObserver::audio_buffer_writer_;
std::shared_ptr<AudioInputStream> AVSAudioRecorderObserver::audio_buffer_;

AVSAudioRecorderObserver::AVSAudioRecorderObserver(
    std::shared_ptr<avsCommon::avs::AudioInputStream> stream,
    std::shared_ptr<defaultClient::DefaultClient> client,
    capabilityAgents::aip::AudioProvider audioProvider)
    : aip_stream_{stream}, avs_client_{client}, audio_provider_{audioProvider} {
  syslog(LOG_INFO, "+++++++AVSAudioRecorderObserver Constructor called");

  //create AVS BUFFER
  int ret = CreateBufferStream();
  if (ret < 0) {
    syslog(LOG_ERR, "AVSAudioRecorderObserver::AVSAudioRecorderObserver() - FAILED create_buffer_stream");
  }
}

AVSAudioRecorderObserver::~AVSAudioRecorderObserver() {
  syslog(LOG_INFO, "------AVSAudioRecorderObserver Destructor called");
}

void AVSAudioRecorderObserver::OnNewVoiceBuffer(uint8_t* buf, uint32_t size) {
  //syslog(LOG_DEBUG, "AVSAudioRecorderObserver::OnNewVoiceBuffer - Received buffer size =%d\n", size);
  audio_buffer_writer_->write(buf, size / 2);
}

bool AVSAudioRecorderObserver::InitializeRecognition() {
  bool ret = true;
  auto tempM_client = avs_client_.lock();
  if (tempM_client) {
    syslog(LOG_INFO, "AVSAudioRecorderObserver::InitializeRecognition - m_client->notifyOfTapToTalk ");
    printf("QAHWManager::InitializeRecognition() -  m_client->notifyOfTapToTalk\n");

    if (!(tempM_client->notifyOfTapToTalk(audio_provider_, avsCommon::sdkInterfaces::KeyWordObserverInterface::UNSPECIFIED_INDEX)).get()) {
      syslog(LOG_ERR, "AVSAudioRecorderObserver::InitializeRecognition - m_client->notifyOfTapToTalk FAIL ");
      ret = false;
    }
  } else {
    ret = false;
  }

  return ret;
}

int AVSAudioRecorderObserver::CreateBufferStream() {
  int result = OK

      if (aip_stream_ == nullptr) {
    printf("assert_error: create_AudioHal_stream - m_stream == nullptr\n");
    result = -1;
  }
  else {
    audio_buffer_ = aip_stream_;
    audio_buffer_writer_ = audio_buffer_->createWriter(AudioInputStream::Writer::Policy::NONBLOCKABLE, true);
    if (audio_buffer_writer_ == nullptr) {
      printf("assert_error: create_AudioHal_stream - audioBufferWriter == nullptr\n");
      result = -1;
    }
  }
  return result;
}

}  // namespace avsManager
}  // namespace voiceUIFramework
