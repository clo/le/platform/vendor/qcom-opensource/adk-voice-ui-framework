/* Copyright (c) 2018, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    AVSVoiceUIClient.cpp
 *  @brief   AVS Voice UI Client implementation
 *
 *  DESCRIPTION
 *    AVS Voice UI Client using Full Solution model
 ***************************************************************/

#include <AVSClientIntegration/AVSVoiceUIClient.h>

namespace voiceUIFramework {
namespace avsManager {

using namespace alexaClientSDK;

AVSVoiceUIClient::AVSVoiceUIClient(
    std::shared_ptr<voiceUIFramework::voiceUIClient::ClientObserver> client_observer,
    voiceUIFramework::voiceUIUtils::VoiceUIClientID client_id,
    bool wake_word_embedded)
    : FullVoiceUIClient(client_observer, client_id, wake_word_embedded) {
  base_client_manager_ = std::make_shared<AVSManager>(client_id, wake_word_embedded, client_observer);
  syslog(LOG_INFO, "AVSVoiceUIClient Constructor called for: %s", ClientIDToString(client_id).c_str());
}

AVSVoiceUIClient::~AVSVoiceUIClient() {
  syslog(LOG_INFO, "---AVSVoiceUIClient::~AVSVoiceUIClient");
}

bool AVSVoiceUIClient::Configure() {
  syslog(LOG_INFO, "AVSVoiceUIClient::Configure()");
  //Check if AVS Solution is enabled
  auto voice_ui_config = voiceUIFramework::voiceUIUtils::VoiceUIConfig::getInstance();
  if (voice_ui_config) {
    bool is_enable = voice_ui_config->ReadWithDefault<bool>(true, voiceUIUtils::Keys::k_avs_status_);
    //TODO ENABLE OR DISABLE CLIENT base_client_manager_->SetActive(is_enable);
    if (!is_enable) {
      syslog(LOG_ERR, "AVSVoiceUIClient::Configure() - AVS Solution DISABLED (Check VoiceUI Database)");
      return false;
    }
  } else {
    syslog(LOG_ERR, "AVSVoiceUIClient::Configure() - VoiceUIConfig not Initialized");
    //TODO ENABLE OR DISABLE CLIENT base_client_manager_->SetActive(is_enable);
  }
  return true;
}

void AVSVoiceUIClient::IntentReceived(IntentManagerEvent intent) {
  syslog(LOG_INFO, "AVSVoiceUIClient::IntentReceived - ClientID = %s", ClientIDToString(base_client_manager_->GetClientID()).c_str());
  base_client_manager_->IntentReceived(intent);
}
void AVSVoiceUIClient::IntentReceived(IntentManagerEvent intent, IntentManagerEventExtended intent_ext) {
  syslog(LOG_INFO, "AVSVoiceUIClient::IntentReceivedExtended - ClientID = %s", ClientIDToString(base_client_manager_->GetClientID()).c_str());
  base_client_manager_->IntentReceived(intent, intent_ext);
}
bool AVSVoiceUIClient::Stop() {
  syslog(LOG_INFO, "AVSVoiceUIClient::Stop()");
  base_client_manager_->StopClient();
  return true;
}

}  // namespace avsManager
}  // namespace voiceUIFramework
