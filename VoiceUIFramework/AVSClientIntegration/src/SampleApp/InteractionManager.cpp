/*
 * Copyright (c) 2018, The Linux Foundation. All rights reserved.
 *
 * Not a Contribution.
 * Apache license notifications and license are retained
 * for attribution purposes only.
 */

/*
 * Copyright 2017-2018 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 *     http://aws.amazon.com/apache2.0/
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

#include <AVSClientIntegration/SampleApp/InteractionManager.h>

namespace voiceUIFramework {
namespace avsManager {

using namespace alexaClientSDK;
using namespace alexaClientSDK::avsCommon::avs;

InteractionManager::InteractionManager(
    std::shared_ptr<defaultClient::DefaultClient> client,
    std::shared_ptr<avsManager::UIManager> userInterface,
    capabilityAgents::aip::AudioProvider holdToTalkAudioProvider,
    capabilityAgents::aip::AudioProvider tapToTalkAudioProvider,
    std::shared_ptr<avsManager::GuiRenderer> guiRenderer,
    capabilityAgents::aip::AudioProvider wakeWordAudioProvider,
    std::shared_ptr<alexaClientSDK::esp::ESPDataProviderInterface> espProvider,
    std::shared_ptr<esp::ESPDataModifierInterface> espModifier,
    std::shared_ptr<avsCommon::sdkInterfaces::CallManagerInterface> callManager)
    : RequiresShutdown{"InteractionManager"}, avs_client_{client}, userinterface_manager_{userInterface}, m_guiRenderer{guiRenderer}, hold_to_talk_audio_provider_{holdToTalkAudioProvider}, tap_to_talk_audio_provider_{tapToTalkAudioProvider}, wakeword_audio_provider_{wakeWordAudioProvider}, is_hold_occurring_{false}, is_tap_occurring_{false}, is_mic_on_{false} {
  if (wakeword_audio_provider_) {
    is_mic_on_ = true;
  }
};

void InteractionManager::changeSetting(const std::string& key, const std::string& value) {
  executor_.submit([this, key, value]() {
    auto tempM_client = avs_client_.lock();
    tempM_client->changeSetting(key, value);
  });
}

void InteractionManager::tap() {
  syslog(LOG_INFO, " InteractionManager::tap() - Sending event to UI Manager ");
  executor_.submit([this]() {
    syslog(LOG_INFO, " InteractionManager::tap()");

    //if internet connection is not UP stop current activity
    if (userinterface_manager_->checkConnectivity()) {
      syslog(LOG_INFO, " InteractionManager::tap() - Execute UIManager TAP");
      userinterface_manager_->tap();
    } else {
      syslog(LOG_INFO, " InteractionManager::tap() - Internet not available- Stop current activity");
      auto tempM_client = avs_client_.lock();
      tempM_client->stopForegroundActivity();
    }
  });
}

void InteractionManager::stopForegroundActivity() {
  executor_.submit([this]() {
    auto tempM_client = avs_client_.lock();
    tempM_client->stopForegroundActivity();
  });
}

void InteractionManager::playbackPlay() {
  executor_.submit([this]() { auto tempM_client = avs_client_.lock(); tempM_client->getPlaybackRouter()->buttonPressed(PlaybackButton::PLAY); });
}

void InteractionManager::playbackPause() {
  executor_.submit([this]() { auto tempM_client = avs_client_.lock(); tempM_client->getPlaybackRouter()->buttonPressed(PlaybackButton::PAUSE); });
}

void InteractionManager::playbackNext() {
  executor_.submit([this]() { auto tempM_client = avs_client_.lock(); tempM_client->getPlaybackRouter()->buttonPressed(PlaybackButton::NEXT); });
}

void InteractionManager::playbackPrevious() {
  executor_.submit([this]() { auto tempM_client = avs_client_.lock(); tempM_client->getPlaybackRouter()->buttonPressed(PlaybackButton::PREVIOUS); });
}

void InteractionManager::playbackSkipForward() {
  executor_.submit([this]() { auto tempM_client = avs_client_.lock(); tempM_client->getPlaybackRouter()->buttonPressed(PlaybackButton::SKIP_FORWARD); });
}

void InteractionManager::playbackSkipBackward() {
  executor_.submit([this]() { auto tempM_client = avs_client_.lock(); tempM_client->getPlaybackRouter()->buttonPressed(PlaybackButton::SKIP_BACKWARD); });
}

void InteractionManager::playbackShuffle() {
  sendGuiToggleEvent(GuiRenderer::TOGGLE_NAME_SHUFFLE, PlaybackToggle::SHUFFLE);
}

void InteractionManager::playbackLoop() {
  sendGuiToggleEvent(GuiRenderer::TOGGLE_NAME_LOOP, PlaybackToggle::LOOP);
}

void InteractionManager::playbackRepeat() {
  sendGuiToggleEvent(GuiRenderer::TOGGLE_NAME_REPEAT, PlaybackToggle::REPEAT);
}

void InteractionManager::playbackThumbsUp() {
  sendGuiToggleEvent(GuiRenderer::TOGGLE_NAME_THUMBSUP, PlaybackToggle::THUMBS_UP);
}

void InteractionManager::playbackThumbsDown() {
  sendGuiToggleEvent(GuiRenderer::TOGGLE_NAME_THUMBSDOWN, PlaybackToggle::THUMBS_DOWN);
}

void InteractionManager::sendGuiToggleEvent(const std::string& toggleName, PlaybackToggle toggleType) {
  bool action = false;
  if (m_guiRenderer) {
    action = !m_guiRenderer->getGuiToggleState(toggleName);
  }
  executor_.submit(
      [this, toggleType, action]() { auto tempM_client = avs_client_.lock(); tempM_client->getPlaybackRouter()->togglePressed(toggleType, action); });
}

void InteractionManager::adjustVolume(avsCommon::sdkInterfaces::SpeakerInterface::Type type, int8_t delta) {
  executor_.submit([this, type, delta]() {
    /*
         * Group the unmute action as part of the same affordance that caused the volume change, so we don't
         * send another event. This isn't a requirement by AVS.
         */
    auto tempM_client = avs_client_.lock();
    std::future<bool> unmuteFuture = tempM_client->getSpeakerManager()->setMute(type, false, true);
    if (!unmuteFuture.valid()) {
      return;
    }
    unmuteFuture.get();

    tempM_client = avs_client_.lock();
    std::future<bool> future = tempM_client->getSpeakerManager()->adjustVolume(type, delta);
    if (!future.valid()) {
      return;
    }
    future.get();
  });
}

void InteractionManager::setVolume(avsCommon::sdkInterfaces::SpeakerInterface::Type type, int8_t volume, bool SetVolumeAndNotify) {
  executor_.submit([this, type, volume, SetVolumeAndNotify]() {
    /*
         * Group the unmute action as part of the same affordance that caused the volume change, so we don't
         * send another event. This isn't a requirement by AVS.
         */
    auto tempM_client = avs_client_.lock();
    std::future<bool> unmuteFuture = tempM_client->getSpeakerManager()->setMute(type, false, true);
    if (!unmuteFuture.valid()) {
      return;
    }
    unmuteFuture.get();

    tempM_client = avs_client_.lock();
    std::future<bool> future;
    if (SetVolumeAndNotify) {
      syslog(LOG_INFO, " InteractionManager::setVolume() - : set the volume and notify to observer ");
      future = tempM_client->getSpeakerManager()->setVolume(type, volume);
    } else {
      syslog(LOG_INFO, " InteractionManager::setVolume() - just notify to observer(volume_updated) ");
      future = tempM_client->getSpeakerManager()->setVolume(type, volume, true);
    }
    if (!future.valid()) {
      return;
    }
    future.get();
  });
}

void InteractionManager::setMute(avsCommon::sdkInterfaces::SpeakerInterface::Type type, bool mute) {
  executor_.submit([this, type, mute]() {
    auto tempM_client = avs_client_.lock();
    std::future<bool> future = tempM_client->getSpeakerManager()->setMute(type, mute);
    future.get();
  });
}

void InteractionManager::resetDevice() {
  // This is a blocking operation. No interaction will be allowed during / after resetDevice
  auto result = executor_.submit([this]() {
    auto tempM_client = avs_client_.lock();
    tempM_client->getRegistrationManager()->logout();
    userinterface_manager_->printResetWarning();
  });
  result.wait();
}

void InteractionManager::confirmReauthorizeDevice() {
  executor_.submit([this]() { userinterface_manager_->printReauthorizeConfirmation(); });
}

void InteractionManager::onDialogUXStateChanged(DialogUXState state) {
  //TODO
}

void InteractionManager::doShutdown() {
  auto tempM_client = avs_client_.lock();
  tempM_client.reset();
}

void InteractionManager::restartSVA() {
  executor_.submit([this]() {
    userinterface_manager_->restartSVA();
  });
}

}  // namespace avsManager
}  // namespace voiceUIFramework
