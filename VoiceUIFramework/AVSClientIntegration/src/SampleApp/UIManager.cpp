/*
 * Copyright (c) 2018-2020, The Linux Foundation. All rights reserved.
 *
 * Not a Contribution.
 * Apache license notifications and license are retained
 * for attribution purposes only.
 */

/*
 * UIManager.cpp
 *
 * Copyright (c) 2017 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 *     http://aws.amazon.com/apache2.0/
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

#include <AVSClientIntegration/SampleApp/UIManager.h>
#include <VoiceUIUtils/VoiceUIConfig.h>

/// String to identify log entries originating from this file.
static const std::string TAG("UIManager");

/**
 * Create a LogEntry using this file's TAG and the specified event string.
 *
 * @param The event string for this @c LogEntry.
 */
#define LX(event) alexaClientSDK::avsCommon::utils::logger::LogEntry(TAG, event)

namespace voiceUIFramework {
namespace avsManager {

using namespace alexaClientSDK;
using namespace alexaClientSDK::avsCommon::sdkInterfaces;
using namespace alexaClientSDK::capabilityAgents::alerts;
using namespace alexaClientSDK::settings;

static const std::string ENTER_LIMITED = "Entering limited interaction mode.";

static const std::string REAUTHORIZE_CONFIRMATION =
    "+----------------------------------------------------------------------------+\n"
    "|                 Device Re-authorization Confirmation:                      |\n"
    "|                                                                            |\n"
    "| This operation will remove all your personal information, device settings, |\n"
    "| and downloaded content. Are you sure you want to reauthorize your device?  |\n"
    "|                                                                            |\n"
    "| Press 'Y' followed by Enter to reset the device.                           |\n"
    "| Press 'N' followed by Enter to cancel re-authorization.                    |\n"
    "+----------------------------------------------------------------------------+\n";

static const std::string RESET_WARNING =
    "Device was reset! Please don't forget to deregister it. For more details "
    "visit https://www.amazon.com/gp/help/customer/display.html?nodeId=201357520";

/// The name of the do not disturb confirmation setting.
static const std::string DO_NOT_DISTURB_NAME = "DoNotDisturb";

UIManager::UIManager()
    : dialog_state_{DialogUXState::IDLE}, capabilities_state_{CapabilitiesObserverInterface::State::UNINITIALIZED}, capabilities_error_{CapabilitiesObserverInterface::Error::UNINITIALIZED}, auth_state_{AuthObserverInterface::State::UNINITIALIZED}, auth_check_counter_{0}, connection_status_{avsCommon::sdkInterfaces::ConnectionStatusObserverInterface::Status::DISCONNECTED}, speech_done_{false} {
}

void UIManager::onDialogUXStateChanged(DialogUXState state) {
  executor_.submit([this, state]() {
    if (state == dialog_state_) {
      return;
    }
    dialog_state_ = state;
    printState();

    std::cout << "UIManager::onDialogUXStateChanged state = " << state << std::endl;
    /*
         * This is an optimization that stops the microphone from continuously streaming audio data into the buffer
         * when a tap to talk recognize finishes. This isn't strictly necessary, as the SDK will not consume the
         * audio data when not in the middle of an active recognition.
         */
    if (avsCommon::sdkInterfaces::DialogUXStateObserverInterface::DialogUXState::IDLE == state) {
      std::cout << "InteractionManager::onDialogUXStateChanged to IDLE " << std::endl;
      syslog(LOG_INFO, " InteractionManager::onDialogUXStateChanged to IDLE ");
      //stop recording form ST Hal
      if (wakeword_engine_)
        wakeword_engine_->StopAudioCapture(VoiceUIClientID::AVS_SOLUTION, voiceUIFramework::wakeword::StopCaptureResult::STOPCAPTURE_OK);
      //stop recording form Audio Hal
      if (audio_recorder_)
        audio_recorder_->StopRecording();

      //Notify VoiceUI Manager new Solution Status
      if (client_observer_)
        client_observer_->NewSolutionStatus(VoiceUIClientID::AVS_SOLUTION, voiceUIFramework::voiceUIClient::VUISolutionStatus::IDLE);

      //Send message for IDLE LED state
      IntentManagerEvent intent = IntentManagerEvent::LED_PATTERN_IDLE;
      if (client_observer_)
        client_observer_->SendIntent(VoiceUIClientID::AVS_SOLUTION, intent);

      if (speech_done_) {
        //Send message for SPEECH DONE state
        IntentManagerEvent intent = IntentManagerEvent::LED_PATTERN_SPEECH_DONE;
        if (client_observer_)
          client_observer_->SendIntent(VoiceUIClientID::AVS_SOLUTION, intent);

        //reset speech_done boolean
        speech_done_ = false;
      }

      //Release the wakelock
      system_wake_lock_toggle(false, WAKELOCK_AVS_NAME, 0);
    }

    if (avsCommon::sdkInterfaces::DialogUXStateObserverInterface::DialogUXState::LISTENING == state) {
      int doaAngle = 0;

      std::cout << "InteractionManager::onDialogUXStateChanged to LISTENING " << std::endl;
      syslog(LOG_INFO, "InteractionManager::onDialogUXStateChanged to LISTENING ");

      auto startOfSpeechTimestamp = std::chrono::steady_clock::now();

      //TODO - get from STHAL or DSPC AudioRecorder
      //Get DOA information form ST HAL
      if (!(doaAngle = wakeword_engine_->GetDOADirection(VoiceUIClientID::AVS_SOLUTION))) {
        syslog(LOG_INFO, "InteractionManager::onDialogUXStateChanged Unable to get DOA, Default to 0");
      }

      auto startOfSpeechTimestamp2 = std::chrono::steady_clock::now();
      double elaspedTimeMs = std::chrono::duration<double, std::milli>(startOfSpeechTimestamp2 - startOfSpeechTimestamp).count();
      syslog(LOG_DEBUG, "@@@@@@@@@@ InteractionManager::onDialogUXStateChanged() -  TIME (to read DOA) in MS =%lf", elaspedTimeMs);

      //Notify VoiceUI Manager new Solution Status
      if (client_observer_)
        client_observer_->NewSolutionStatus(VoiceUIClientID::AVS_SOLUTION, voiceUIFramework::voiceUIClient::VUISolutionStatus::LISTENING);

      //Send message for LISTENING LED state
      IntentManagerEvent intent = IntentManagerEvent::LED_PATTERN_LISTENING;
      if (client_observer_)
        client_observer_->SendIntent(VoiceUIClientID::AVS_SOLUTION, intent);

      //Send Direction OF Arrival Message
      IntentManagerEvent intent_2;
      intent_2 = IntentManagerEvent::DIRECTION_OF_ARRIVAL;
      IntentManagerEventExtended intent_ext;
      intent_ext.value_uint = doaAngle;
      if (client_observer_) {
        client_observer_->SendIntent(VoiceUIClientID::AVS_SOLUTION, intent_2, intent_ext);
      }

      auto startOfSpeechTimestamp3 = std::chrono::steady_clock::now();
      double elaspedTotalTimeMs = std::chrono::duration<double, std::milli>(startOfSpeechTimestamp3 - startOfSpeechTimestamp).count();
      syslog(LOG_DEBUG, "@@@@@@@@@@ InteractionManager::onDialogUXStateChanged() - TOTAL TIME (for ALL LED notification) in MS =%lf", elaspedTotalTimeMs);
    }

    if (avsCommon::sdkInterfaces::DialogUXStateObserverInterface::DialogUXState::SPEAKING == state) {
      std::cout << "InteractionManager::onDialogUXStateChanged to SPEAKING " << std::endl;
      syslog(LOG_INFO, " InteractionManager::onDialogUXStateChanged to SPEAKING ");

      //Notify VoiceUI Manager new Solution Status
      if (client_observer_)
        client_observer_->NewSolutionStatus(VoiceUIClientID::AVS_SOLUTION, voiceUIFramework::voiceUIClient::VUISolutionStatus::SPEAKING);

      //Send message for SPEAKING state
      IntentManagerEvent intent = IntentManagerEvent::LED_PATTERN_SPEAKING;
      if (client_observer_)
        client_observer_->SendIntent(VoiceUIClientID::AVS_SOLUTION, intent);

      //set speech_done boolean
      speech_done_ = true;
    }

    //Restart STHAL detection when in THINKING( AIP: IDLE or BUSY) state
    if (avsCommon::sdkInterfaces::DialogUXStateObserverInterface::DialogUXState::THINKING == state) {
      std::cout << "InteractionManager::onDialogUXStateChanged to THINKING " << std::endl;
      syslog(LOG_INFO, " InteractionManager::onDialogUXStateChanged to THINKING ");

      //stop recording form ST Hal
      if (wakeword_engine_)
        wakeword_engine_->StopAudioCapture(VoiceUIClientID::AVS_SOLUTION, voiceUIFramework::wakeword::StopCaptureResult::STOPCAPTURE_OK);
      //stop recording form Audio Hal
      if (audio_recorder_)
        audio_recorder_->StopRecording();

      //Notify VoiceUI Manager new Solution Status
      if (client_observer_)
        client_observer_->NewSolutionStatus(VoiceUIClientID::AVS_SOLUTION, voiceUIFramework::voiceUIClient::VUISolutionStatus::BUSY);

      //Send message for THINKING/BUSY state
      IntentManagerEvent intent = IntentManagerEvent::LED_PATTERN_THINKING;
      if (client_observer_)
        client_observer_->SendIntent(VoiceUIClientID::AVS_SOLUTION, intent);
    }

    //Start Recording on Audio Hal when in EXPECTING_SPEECH state
    if (avsCommon::sdkInterfaces::DialogUXStateObserverInterface::DialogUXState::EXPECTING == state) {
      //Renew wakelock for AVS in case of multiturn conversation
      int delay = WAKELOCK_AVS_TIMEOUT;
      system_wake_lock_toggle(true, WAKELOCK_AVS_NAME, delay);

      std::cout << "InteractionManager::onDialogUXStateChanged to EXPECTING " << std::endl;
      syslog(LOG_INFO, " InteractionManager::onDialogUXStateChanged to EXPECTING ");
      //Stop ST Hal recognition
      if (wakeword_engine_)
        wakeword_engine_->StopRecognition();

      //start recording from Audio Hal using ChannelIndex from STHAL
      if (audio_recorder_)
        audio_recorder_->StartRecording(0);

      //Notify VoiceUI Manager new Solution Status
      if (client_observer_)
        client_observer_->NewSolutionStatus(VoiceUIClientID::AVS_SOLUTION, voiceUIFramework::voiceUIClient::VUISolutionStatus::EXPECTING_SPEECH);

      //Send message for LISTENING LED state
      IntentManagerEvent intent = IntentManagerEvent::LED_PATTERN_LISTENING;
      if (client_observer_)
        client_observer_->SendIntent(VoiceUIClientID::AVS_SOLUTION, intent);
    }
  });
}

void UIManager::onConnectionStatusChanged(const Status status, const ChangedReason reason) {
  //Acquire log for AVS use case
  int delay = WAKELOCK_AVS_CONNECTION_TIMEOUT;
  system_wake_lock_toggle(true, WAKELOCK_AVS_CONNECTION_NAME, delay);

  executor_.submit([this, status]() {
    if (connection_status_ == status) {
      return;
    }
    connection_status_ = status;
    printState();

    //Notify VoiceUiManager about CONNECTED state to release VoiceUI Initialization wakelock
    if (status == Status::CONNECTED) {
      //Notify VoiceUI Manager new Solution Status
      if (client_observer_)
        client_observer_->NewSolutionStatus(VoiceUIClientID::AVS_SOLUTION, voiceUIFramework::voiceUIClient::VUISolutionStatus::IDLE);
    }
  });
}

void UIManager::onSettingChanged(const std::string& key, const std::string& value) {
  executor_.submit([key, value]() {
    std::string msg = key + " set to " + value;
    ConsolePrinter::prettyPrint(msg);
  });
}

void UIManager::onSpeakerSettingsChanged(
    const SpeakerManagerObserverInterface::Source& source,
    const SpeakerInterface::Type& type,
    const SpeakerInterface::SpeakerSettings& settings) {
  executor_.submit([source, type, settings]() {
    std::ostringstream oss;
    oss << "SOURCE:" << source << " TYPE:" << type << " VOLUME:" << static_cast<int>(settings.volume)
        << " MUTE:" << settings.mute;
    ConsolePrinter::prettyPrint(oss.str());
  });
}

void UIManager::onRequestAuthorization(const std::string& url, const std::string& code) {
  executor_.submit([this, url, code]() {
    auth_check_counter_ = 0;
    IntentManagerEvent intent;

    ConsolePrinter::prettyPrint("NOT YET AUTHORIZED");

    //Set LEDS to ONBOARDING PATTERN to indicate that onboarding procedure is needed.
    intent = IntentManagerEvent::LED_PATTERN_ONBOARDING;
    if (client_observer_)
      client_observer_->SendIntent(VoiceUIClientID::AVS_SOLUTION, intent);

    std::ostringstream oss;
    oss << "To authorize, browse to: '" << url << "' and enter the code: " << code;
    ConsolePrinter::prettyPrint(oss.str());

    auto voice_ui_config = voiceUIFramework::voiceUIUtils::VoiceUIConfig::getInstance();

    //Update the database
    if (voice_ui_config) {
      //write code and URL result
      if (voice_ui_config->Write<std::string>(code, voiceUIUtils::Keys::k_avs_authorization_code_) && voice_ui_config->Write<std::string>(url, voiceUIUtils::Keys::k_avs_authorization_url_)) {
        ConsolePrinter::prettyPrint("DB is updated with Authorization code and URL");
      } else {
        //TODO:
        ConsolePrinter::prettyPrint("Fatal Error, Unable to write to DB, Inconsistent");
      }
    } else {
      ConsolePrinter::prettyPrint("Fatal Error, Unable to access Database");
    }

    //TODO: CHECK if we still need it.
    //Send Autorization code to DisplayManager
    intent = IntentManagerEvent::DISPLAY_MANAGER_NOTIFICATION;
    IntentManagerEventExtended intent_ext;
    intent_ext.notification.domain = "AVS";
    intent_ext.notification.message = code;
    intent_ext.notification.display_time = "120000";  //0  for permanently display
    intent_ext.notification.priority = "5";

    if (client_observer_)
      client_observer_->SendIntent(VoiceUIClientID::AVS_SOLUTION, intent, intent_ext);

    // Send code to IOT sys App
    IntentManagerEvent intent_iot;
    intent_iot = IntentManagerEvent::AUTHENTICATE_AVS;
    IntentManagerEventExtended intent_iot_ext;
    intent_iot_ext.str_1 = url;
    intent_iot_ext.str_2 = code;

    if (client_observer_)
      client_observer_->SendIntent(VoiceUIClientID::AVS_SOLUTION, intent_iot, intent_iot_ext);
  });
}

void UIManager::onCheckingForAuthorization() {
  executor_.submit([this]() {
    std::ostringstream oss;
    oss << "Checking for authorization (" << ++auth_check_counter_ << ")...";
    ConsolePrinter::prettyPrint(oss.str());
  });
}

void UIManager::onAuthStateChange(AuthObserverInterface::State newState, AuthObserverInterface::Error newError) {
  executor_.submit([this, newState, newError]() {
    if (auth_state_ != newState) {
      auth_state_ = newState;
      switch (auth_state_) {
        case AuthObserverInterface::State::UNINITIALIZED:
          break;
        case AuthObserverInterface::State::REFRESHED: {
          ConsolePrinter::prettyPrint("Authorized!");

          auto voice_ui_config = voiceUIFramework::voiceUIUtils::VoiceUIConfig::getInstance();

          //Update the database
          if (voice_ui_config) {
            //delete code and URL result
            if (voice_ui_config->Write<std::string>("", voiceUIUtils::Keys::k_avs_authorization_code_) && voice_ui_config->Write<std::string>("", voiceUIUtils::Keys::k_avs_authorization_url_)) {
              syslog(LOG_INFO, "DB is updated  : Delete Authorization code and URL");
            } else {
              //TODO:
              syslog(LOG_ERR, "Fatal Error, Unable to write to DB, Inconsistent");
            }
          } else {
            syslog(LOG_ERR, "Fatal Error, Unable to access Database");
          }

          //Set LEDS to IDLE PATTERN to indicate that onboarding procedure is DONE.
          IntentManagerEvent intent = IntentManagerEvent::LED_PATTERN_IDLE;
          if (client_observer_)
            client_observer_->SendIntent(VoiceUIClientID::AVS_SOLUTION, intent);

          // Send Onboarding succes to the IOT sys
          IntentManagerEvent intent_iot;
          intent_iot = IntentManagerEvent::ONBOARDING_SUCCESS;
          IntentManagerEventExtended intent_iot_ext;
          intent_iot_ext.str_1 = "AVS";
          if (client_observer_) {
            client_observer_->SendIntent(VoiceUIClientID::AVS_SOLUTION, intent_iot, intent_iot_ext);
          }

          // Update Database
          if (voice_ui_config) {
            //write conboarding result
            if (voice_ui_config->Write<bool>(true, voiceUIUtils::Keys::k_avs_onboarded_)) {
              // Send database Updated
              IntentManagerEventExtended intent_ext;
              intent = IntentManagerEvent::DB_FILE_UPDATED;
              intent_ext.str_1 = voiceUIUtils::Keys::k_avs_onboarded_;
              if (client_observer_)
                client_observer_->SendIntent(VoiceUIClientID::AVS_SOLUTION, intent, intent_ext);
            } else {
              //TODO:
              ConsolePrinter::prettyPrint("Fatal Error, Unable to write to DB, Inconsistent");
            }
          } else {
            //TODO:
            ConsolePrinter::prettyPrint("Fatal Error, Unable to access Database");
          }

        } break;
        case AuthObserverInterface::State::EXPIRED: {
          ConsolePrinter::prettyPrint("AUTHORIZATION EXPIRED");

          auto voice_ui_config = voiceUIFramework::voiceUIUtils::VoiceUIConfig::getInstance();

          //Update the database
          if (voice_ui_config) {
            //delete code and URL result
            if (voice_ui_config->Write<std::string>("", voiceUIUtils::Keys::k_avs_authorization_code_) && voice_ui_config->Write<std::string>("", voiceUIUtils::Keys::k_avs_authorization_url_)) {
              syslog(LOG_INFO, "DB is updated  : Delete Authorization code and URL");
            } else {
              //TODO:
              syslog(LOG_ERR, "Fatal Error, Unable to write to DB, Inconsistent");
            }
          } else {
            syslog(LOG_ERR, "Fatal Error, Unable to access Database");
          }

          IntentManagerEvent intent_iot;
          intent_iot = IntentManagerEvent::ONBOARDING_ERROR;
          IntentManagerEventExtended intent_iot_ext;
          intent_iot_ext.str_1 = "AVS";
          intent_iot_ext.str_2 = "Authorization Expired";
          intent_iot_ext.value_uint = 1;
          if (client_observer_)
            client_observer_->SendIntent(VoiceUIClientID::AVS_SOLUTION, intent_iot, intent_iot_ext);
        } break;
        case AuthObserverInterface::State::UNRECOVERABLE_ERROR:

          auto voice_ui_config = voiceUIFramework::voiceUIUtils::VoiceUIConfig::getInstance();

          //Update the database
          if (voice_ui_config) {
            //delete code and URL result
            if (voice_ui_config->Write<std::string>("", voiceUIUtils::Keys::k_avs_authorization_code_) && voice_ui_config->Write<std::string>("", voiceUIUtils::Keys::k_avs_authorization_url_)) {
              syslog(LOG_INFO, "DB is updated  : Delete Authorization code and URL");
            } else {
              //TODO:
              syslog(LOG_ERR, "Fatal Error, Unable to write to DB, Inconsistent");
            }
          } else {
            syslog(LOG_ERR, "Fatal Error, Unable to access Database");
          }

          IntentManagerEvent intent_iot;
          intent_iot = IntentManagerEvent::ONBOARDING_ERROR;
          IntentManagerEventExtended intent_iot_ext;
          intent_iot_ext.str_1 = "AVS";
          intent_iot_ext.str_2 = "Unrecoverable Error";
          intent_iot_ext.value_uint = 1;
          if (client_observer_)
            client_observer_->SendIntent(VoiceUIClientID::AVS_SOLUTION, intent_iot, intent_iot_ext);

          switch (newError) {
            case AuthObserverInterface::Error::SUCCESS:
            case AuthObserverInterface::Error::UNKNOWN_ERROR:
            case AuthObserverInterface::Error::AUTHORIZATION_FAILED:
            case AuthObserverInterface::Error::UNAUTHORIZED_CLIENT:
            case AuthObserverInterface::Error::SERVER_ERROR:
            case AuthObserverInterface::Error::INVALID_REQUEST:
            case AuthObserverInterface::Error::INVALID_VALUE:
            case AuthObserverInterface::Error::UNSUPPORTED_GRANT_TYPE:
            case AuthObserverInterface::Error::AUTHORIZATION_PENDING:
            case AuthObserverInterface::Error::SLOW_DOWN:
            case AuthObserverInterface::Error::INTERNAL_ERROR:
            case AuthObserverInterface::Error::INVALID_CBL_CLIENT_ID: {
              std::ostringstream oss;
              oss << "UNRECOVERABLE AUTHORIZATION ERROR: " << newError;
              ConsolePrinter::prettyPrint({oss.str(), ENTER_LIMITED});
              break;
            }
            case AuthObserverInterface::Error::AUTHORIZATION_EXPIRED:
              ConsolePrinter::prettyPrint(
                  {"AUTHORIZATION FAILED", "RE-AUTHORIZATION REQUIRED", ENTER_LIMITED});
              break;
            case AuthObserverInterface::Error::INVALID_CODE_PAIR:
              ConsolePrinter::prettyPrint(
                  {"AUTHORIZATION CODE EXPIRED", "(RE)-AUTHORIZATION REQUIRED", ENTER_LIMITED});
              break;
          }
      }
    }
  });
}

void UIManager::onCapabilitiesStateChange(
    CapabilitiesObserverInterface::State newState,
    CapabilitiesObserverInterface::Error newError) {
  executor_.submit([this, newState, newError]() {
    if ((capabilities_state_ != newState) && (capabilities_error_ != newError)) {
      capabilities_state_ = newState;
      capabilities_error_ = newError;
      if (CapabilitiesObserverInterface::State::FATAL_ERROR == capabilities_state_) {
        std::ostringstream oss;
        oss << "UNRECOVERABLE CAPABILITIES API ERROR: " << capabilities_error_;
        ConsolePrinter::prettyPrint({oss.str(), ENTER_LIMITED});
      }
    }
  });
}

void UIManager::onSetIndicator(avsCommon::avs::IndicatorState state) {
  /* Since Alexa messaging is not supported in our platform currently,
  |* clearing or playing messages is not possible, hence commented this code *|
  syslog(LOG_INFO, "UIManager::onSetIndicator");
  IntentManagerEvent intent;
    switch (state) {
    case avsCommon::avs::IndicatorState::ON:
    syslog(LOG_INFO, "UIManager::onSetIndicator : IndicatorState::ON");
      intent = IntentManagerEvent::LED_PATTERN_NOTIFICATION;
      if (client_observer_)
        client_observer_->SendIntent(VoiceUIClientID::AVS_SOLUTION, intent);
      break;
    case avsCommon::avs::IndicatorState::OFF:
    syslog(LOG_INFO, "UIManager::onSetIndicator : IndicatorState::OFF");
      intent = IntentManagerEvent::LED_PATTERN_IDLE;
      if (client_observer_)
        client_observer_->SendIntent(VoiceUIClientID::AVS_SOLUTION, intent);
      break;
    default:
      break;
  }*/

  executor_.submit([state]() {
    std::ostringstream oss;
    oss << " UIManager::onSetIndicator NOTIFICATION INDICATOR STATE: " << state;
    ConsolePrinter::prettyPrint(oss.str());
  });
}

void UIManager::onAlertStateChange(const std::string& alertToken, const std::string& Alerttype, AlertObserverInterface::State state, const std::string& reason) {
  ACSDK_DEBUG9(LX("onAlertStateChange").d("alertToken", alertToken).d("alertTyoe", Alerttype).d("state", state).d("reason", reason));

  IntentManagerEvent intent;

  switch (state) {
    case AlertObserverInterface::State::READY:
      syslog(LOG_INFO, "UIManager::onAlertStateChange() - READY ");
      break;

    case AlertObserverInterface::State::STARTED:
      syslog(LOG_INFO, "UIManager::onAlertStateChange() - STARTED ");

      //Send message for ALARM/REMINDER STARTED
      intent = IntentManagerEvent::LED_PATTERN_ALERT;
      if (client_observer_)
        client_observer_->SendIntent(VoiceUIClientID::AVS_SOLUTION, intent);
      break;

    case AlertObserverInterface::State::SNOOZED:
      syslog(LOG_INFO, "UIManager::onAlertStateChange() - SNOOZED ");

      //Send message for IDLE LED state
      intent = IntentManagerEvent::LED_PATTERN_IDLE;
      if (client_observer_)
        client_observer_->SendIntent(VoiceUIClientID::AVS_SOLUTION, intent);
      break;

    case AlertObserverInterface::State::STOPPED:
      syslog(LOG_INFO, "UIManager::onAlertStateChange() - STOPPED ");

      //Send message for IDLE LED state
      intent = IntentManagerEvent::LED_PATTERN_IDLE;
      if (client_observer_)
        client_observer_->SendIntent(VoiceUIClientID::AVS_SOLUTION, intent);
      break;

    case AlertObserverInterface::State::COMPLETED:
      syslog(LOG_INFO, "UIManager::onAlertStateChange() - COMPLETED ");
      break;

    case AlertObserverInterface::State::ERROR:
      syslog(LOG_INFO, "UIManager::onAlertStateChange() - ERROR");
      break;

    case AlertObserverInterface::State::PAST_DUE:
      syslog(LOG_INFO, "UIManager::onAlertStateChange() - PAST_DUE ");
      break;

    case AlertObserverInterface::State::FOCUS_ENTERED_FOREGROUND:
      syslog(LOG_INFO, "UIManager::onAlertStateChange() - FOCUS_ENTERED_FOREGROUND");
      break;

    case AlertObserverInterface::State::FOCUS_ENTERED_BACKGROUND:
      syslog(LOG_INFO, "UIManager::onAlertStateChange() - FOCUS_ENTERED_BACKGROUND");
      break;
  }
}

void UIManager::setMicrophoneOn() {
  if (wakeword_engine_)
    wakeword_engine_->StartRecognition();
}

void UIManager::setMicrophoneOff() {
  if (wakeword_engine_)
    wakeword_engine_->StopRecognition();
  if (audio_recorder_)
    audio_recorder_->StopRecording();
}

void UIManager::microphoneOff() {
  executor_.submit([]() { ConsolePrinter::prettyPrint("Microphone Off!"); });
}

void UIManager::microphoneOn() {
  executor_.submit([this]() { printState(); });
}

void UIManager::onBooleanSettingNotification(
    const std::string& name,
    bool state,
    settings::SettingNotifications notification) {
  std::string msg;
  if (settings::SettingNotifications::LOCAL_CHANGE_FAILED == notification || settings::SettingNotifications::AVS_CHANGE_FAILED == notification) {
    msg = "ERROR: Failed to set " + name + ". ";
  } else {
    msg += name + " is " + std::string(state ? "ON" : "OFF");
    syslog(LOG_INFO, "UIManager::onBooleanSettingNotification %s", msg.c_str());

    if (DO_NOT_DISTURB_NAME == name && true == state) {
      IntentManagerEvent intent = IntentManagerEvent::LED_PATTERN_DND;
      if (client_observer_)
        client_observer_->SendIntent(VoiceUIClientID::AVS_SOLUTION, intent);
    }
  }

  executor_.submit([msg]() { ConsolePrinter::prettyPrint(msg); });
}

void UIManager::printResetWarning() {
  executor_.submit([]() { ConsolePrinter::prettyPrint(RESET_WARNING); });
}

void UIManager::printReauthorizeConfirmation() {
  executor_.submit([]() { ConsolePrinter::simplePrint(REAUTHORIZE_CONFIRMATION); });
}

void UIManager::printState() {
  if (connection_status_ == avsCommon::sdkInterfaces::ConnectionStatusObserverInterface::Status::DISCONNECTED) {
    ConsolePrinter::prettyPrint("Client not connected!");
  } else if (connection_status_ == avsCommon::sdkInterfaces::ConnectionStatusObserverInterface::Status::PENDING) {
    ConsolePrinter::prettyPrint("Connecting...");
  } else if (connection_status_ == avsCommon::sdkInterfaces::ConnectionStatusObserverInterface::Status::CONNECTED) {
    switch (dialog_state_) {
      case DialogUXState::IDLE:
        ConsolePrinter::prettyPrint("Alexa is currently idle!");
        return;
      case DialogUXState::LISTENING:
        ConsolePrinter::prettyPrint("Listening...");
        return;
      case DialogUXState::EXPECTING:
        ConsolePrinter::prettyPrint("Expecting Speech...");
        return;
      case DialogUXState::THINKING:
        ConsolePrinter::prettyPrint("Thinking...");
        return;
      case DialogUXState::SPEAKING:
        ConsolePrinter::prettyPrint("Speaking...");
        return;
      /*
             * This is an intermediate state after a SPEAK directive is completed. In the case of a speech burst the
             * next SPEAK could kick in or if its the last SPEAK directive ALEXA moves to the IDLE state. So we do
             * nothing for this state.
             */
      case DialogUXState::FINISHED:
        ConsolePrinter::prettyPrint("Speaking Finished ...");
        return;
    }
  }
}

void UIManager::printCommsNotSupported() {
  executor_.submit([]() { ConsolePrinter::simplePrint("Comms is not supported in this device."); });
}

void UIManager::addQTIInterfaces(
    std::shared_ptr<voiceUIFramework::wakeword::WakeWordInterface> wake_word,
    std::shared_ptr<voiceUIFramework::audioRecorder::AudioRecorderInterface> audio_recorder,
    std::shared_ptr<voiceUIFramework::voiceUIClient::ClientObserver> client_observer) {
  wakeword_engine_ = wake_word;
  audio_recorder_ = audio_recorder;
  client_observer_ = client_observer;  //Send LED events
}

void UIManager::tap() {
  syslog(LOG_INFO, " UIManager::tap() - Opening Audio HAL for recording ");
  //Stop ST Hal recognition
  //wake_word_->StopRecognition();

  //start recording from Audio Hal using ChannelIndex from STHAL
  if (audio_recorder_)
    audio_recorder_->StartRecording(0);
}

bool UIManager::checkConnectivity() {
  if (connection_status_ == Status::CONNECTED)
    return true;
  else
    return false;
}

void UIManager::restartSVA() {
  if (wakeword_engine_)
    wakeword_engine_->StartRecognition();
}

void UIManager::shutdown() {
  syslog(LOG_INFO, "UIManager::shutdown() - Stop wakeword Recognition process ");
  if (wakeword_engine_) {
    wakeword_engine_->StopRecognition();
  }
}

void UIManager::onLogout() {
  syslog(LOG_INFO, " UIManager::onLogout() - AVS LOGOUT was DONE. Restart AVS Services ");

  //Restart Client
  if (client_observer_)
    client_observer_->RequestRestartClient(VoiceUIClientID::AVS_SOLUTION);
}

bool UIManager::configureSettingsNotifications(std::shared_ptr<settings::DeviceSettingsManager> settingsManager) {
  m_callbacks = SettingCallbacks<DeviceSettingsManager>::create(settingsManager);
  if (!m_callbacks) {
    ACSDK_ERROR(LX("configureSettingsNotificationsFailed").d("reason", "createCallbacksFailed"));
    return false;
  }
  bool ok =
      m_callbacks->add<DeviceSettingsIndex::DO_NOT_DISTURB>([this](bool enable, SettingNotifications notifications) {
        onBooleanSettingNotification(DO_NOT_DISTURB_NAME, enable, notifications);
      });
  return ok;
}

void UIManager::onActiveDeviceConnected(const DeviceAttributes& deviceAttributes) {
  executor_.submit([deviceAttributes]() {
    std::ostringstream oss;
    oss << "SUPPORTED SERVICES: ";
    std::string separator = "";
    for (const auto& supportService : deviceAttributes.supportedServices) {
      oss << separator << supportService;
      separator = ", ";
    }
    ConsolePrinter::prettyPrint({"BLUETOOTH DEVICE CONNECTED", "Name: " + deviceAttributes.name, oss.str()});
  });
}

void UIManager::onActiveDeviceDisconnected(const DeviceAttributes& deviceAttributes) {
  executor_.submit([deviceAttributes]() {
    std::ostringstream oss;
    oss << "SUPPORTED SERVICES: ";
    std::string separator = "";
    for (const auto& supportedService : deviceAttributes.supportedServices) {
      oss << separator << supportedService;
      separator = ", ";
    }
    ConsolePrinter::prettyPrint({"BLUETOOTH DEVICE DISCONNECTED", "Name: " + deviceAttributes.name, oss.str()});
  });
}

}  // namespace avsManager
}  // namespace voiceUIFramework
