/*
 * Copyright (c) 2018, The Linux Foundation. All rights reserved.
 *
 * Not a Contribution.
 * Apache license notifications and license are retained
 * for attribution purposes only.
 */

/*
 * Copyright 2017-2018 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 *     http://aws.amazon.com/apache2.0/
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

#include <algorithm>
#include <iostream>

/**
 *  When using pretty print, we pad our strings in the beginning and in the end with the margin representation '#'
 *  and 7 spaces. E.g., if I pass "Hello world!" string, pretty print will look like:
 *  ############################
 *  #       Hello world!       #
 *  ############################
 */
static const size_t PADDING_LENGTH = 8;

#include <AVSClientIntegration/SampleApp/ConsolePrinter.h>

namespace voiceUIFramework {
namespace avsManager {

using namespace alexaClientSDK;

std::shared_ptr<std::mutex> ConsolePrinter::global_mutex_ = std::make_shared<std::mutex>();

ConsolePrinter::ConsolePrinter()
    : avsCommon::utils::logger::Logger(avsCommon::utils::logger::Level::UNKNOWN), mutex_(global_mutex_) {
}

void ConsolePrinter::simplePrint(const std::string& stringToPrint) {
  auto mutex = global_mutex_;
  if (!mutex) {
    return;
  }

  std::lock_guard<std::mutex> lock{*mutex};
  std::cout << stringToPrint << std::endl;
}

void ConsolePrinter::prettyPrint(std::initializer_list<std::string> lines) {
  size_t maxLength = 0;
  for (auto& line : lines) {
    maxLength = std::max(line.size(), maxLength);
  }

  const std::string line(maxLength + (2 * PADDING_LENGTH), '#');
  std::ostringstream oss;
  oss << line << std::endl;

  // Write each line starting and ending with '#'
  auto padBegin = std::string("#");
  padBegin.append(PADDING_LENGTH - 1, ' ');
  for (auto& line : lines) {
    auto padEnd = std::string("#");
    padEnd.insert(padEnd.begin(), maxLength - line.size() + (PADDING_LENGTH - 1), ' ');
    oss << padBegin << line << padEnd << std::endl;
  }

  oss << line << std::endl;
  simplePrint(oss.str());
}

void ConsolePrinter::prettyPrint(const std::string& stringToPrint) {
  prettyPrint({stringToPrint});
}

void ConsolePrinter::emit(
    avsCommon::utils::logger::Level level,
    std::chrono::system_clock::time_point time,
    const char* threadMoniker,
    const char* text) {
  std::lock_guard<std::mutex> lock{*mutex_};
  std::cout << log_formatter_.format(level, time, threadMoniker, text) << std::endl;
}

}  // namespace avsManager
}  // namespace voiceUIFramework
