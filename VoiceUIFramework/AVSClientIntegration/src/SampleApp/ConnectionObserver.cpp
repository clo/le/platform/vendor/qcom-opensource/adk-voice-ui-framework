/*
 * Copyright (c) 2018, The Linux Foundation. All rights reserved.
 *
 * Not a Contribution.
 * Apache license notifications and license are retained
 * for attribution purposes only.
 */

/*
 * Copyright 2017-2018 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 *     http://aws.amazon.com/apache2.0/
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

#include <AVSClientIntegration/SampleApp/ConnectionObserver.h>

namespace voiceUIFramework {
namespace avsManager {

using namespace alexaClientSDK;
using namespace alexaClientSDK::avsCommon::sdkInterfaces;

ConnectionObserver::ConnectionObserver()
    : auth_state_(AuthObserverInterface::State::UNINITIALIZED), connection_status_(ConnectionStatusObserverInterface::Status::DISCONNECTED) {
}

void ConnectionObserver::onAuthStateChange(AuthObserverInterface::State newState, AuthObserverInterface::Error error) {
  std::lock_guard<std::mutex> lock{mutex_};
  auth_state_ = newState;
  trigger_.notify_all();
}

void ConnectionObserver::onConnectionStatusChanged(
    const ConnectionStatusObserverInterface::Status status,
    const ConnectionStatusObserverInterface::ChangedReason reason) {
  std::lock_guard<std::mutex> lock{mutex_};
  connection_status_ = status;
  trigger_.notify_all();
}

bool ConnectionObserver::waitFor(const AuthObserverInterface::State authState, const std::chrono::seconds duration) {
  std::unique_lock<std::mutex> lock(mutex_);
  return trigger_.wait_for(lock, duration, [this, authState]() { return authState == auth_state_; });
}

bool ConnectionObserver::waitFor(
    const ConnectionStatusObserverInterface::Status connectionStatus,
    const std::chrono::seconds duration) {
  std::unique_lock<std::mutex> lock(mutex_);
  return trigger_.wait_for(
      lock, duration, [this, connectionStatus]() { return connectionStatus == connection_status_; });
}

}  // namespace avsManager
}  // namespace voiceUIFramework
