/* Copyright (c) 2019-2020, The Linux Foundation. All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 * Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above
 copyright notice, this list of conditions and the following
 disclaimer in the documentation and/or other materials provided
 with the distribution.
 * Neither the name of The Linux Foundation nor the names of its
 contributors may be used to endorse or promote products derived
 from this software without specific prior written permission.

 THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    QTIMediaPlayer.cpp
 *  @brief
 *
 *  DESCRIPTION
 *
 ***************************************************************/

#include <AVSClientIntegration/QTIMediaPlayer.h>

//TODO: need to remove extra intervals after the estimation of timeout and retrycount of ADK-Mediaplayer needdatacallback()
/// The interval to wait (in milliseconds) between successive attempts to read audio data when none is available.
static const guint RETRY_INTERVALS_MILLISECONDS[] = {0, 10, 10, 10, 20, 20, 50, 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000, 2000, 3000, 4000, 5000};
//static const guint RETRY_INTERVALS_MILLISECONDS[] = {0, 10, 10, 10, 20, 20, 50, 100};

namespace alexaClientSDK {
namespace mediaPlayer {

using namespace avsCommon::avs::attachment;
using namespace avsCommon::avs::speakerConstants;
using namespace avsCommon::sdkInterfaces;
using namespace avsCommon::utils;
using namespace avsCommon::utils::mediaPlayer;
using namespace avsCommon::utils::memory;
using namespace avsCommon::utils::configuration;

/// A counter used to increment the source id when a new source is set.
static QTIMediaPlayer::SourceId g_id{0};

/// A link to @c MediaPlayerInterface::ERROR.
static const QTIMediaPlayer::SourceId ERROR_SOURCE_ID = QTIMediaPlayer::ERROR;

/// GStreamer Volume Element Minimum.
static const int8_t GST_SET_VOLUME_MIN = 0;

/// GStreamer Volume Element Maximum.
static const int8_t GST_SET_VOLUME_MAX = 1;

/// GStreamer Volume Adjust Minimum.
static const int8_t GST_ADJUST_VOLUME_MIN = -1;

/// GStreamer Volume Adjust Maximum.
static const int8_t GST_ADJUST_VOLUME_MAX = 1;

/// Mimimum level for equalizer bands
static const int MIN_EQUALIZER_LEVEL = -24;

/// Maximum level for equalizer bands
static const int MAX_EQUALIZER_LEVEL = 12;

/// Represents the zero volume to avoid the actual 0.0 value. Used as a fix for GStreamer crashing on 0 volume for PCM.
//static const gdouble VOLUME_ZERO = 0.0000001;
static const double VOLUME_ZERO = 0.0;

std::shared_ptr<QTIMediaPlayer> QTIMediaPlayer::create(
    std::shared_ptr<avsCommon::sdkInterfaces::HTTPContentFetcherInterfaceFactoryInterface> contentFetcherFactory,
    bool enableEqualizer,
    SpeakerInterface::Type type,
    std::string name,
    bool enableLiveMode) {
  syslog(LOG_DEBUG, "QTIMediaPlayer-%s : Thread %x : create()", name.c_str(), std::this_thread::get_id());

  std::shared_ptr<QTIMediaPlayer> mediaPlayer(new QTIMediaPlayer(contentFetcherFactory, enableEqualizer, type, name, enableLiveMode));
  if (mediaPlayer->init(name)) {
    return mediaPlayer;
  } else {
    return nullptr;
  }
};

QTIMediaPlayer::~QTIMediaPlayer() {
  syslog(LOG_DEBUG, "~QTIMediaPlayerCalled-%s : Thread %x : ", m_MediaplayerType.c_str(), std::this_thread::get_id());
  cleanUpSource();

  //serialisation using Gobject queueCallback
  g_main_loop_quit(m_mainLoop);
  if (m_mainLoopThread.joinable()) {
    m_mainLoopThread.join();
  }

  g_main_loop_unref(m_mainLoop);

  g_main_context_unref(m_workerContext);
}

QTIMediaPlayer::SourceId QTIMediaPlayer::setSource(
    std::shared_ptr<avsCommon::avs::attachment::AttachmentReader> reader,
    const avsCommon::utils::AudioFormat* audioFormat) {
  syslog(LOG_DEBUG, "QTIMediaPlayer-%s : Thread %x : setSourceCalled and sourceType = AttachmentReader", m_MediaplayerType.c_str(), std::this_thread::get_id());
  std::promise<QTIMediaPlayer::SourceId> promise;
  auto future = promise.get_future();

  std::function<gboolean()> callback = [this, &reader, &promise, audioFormat]() {
    handleSetAttachmentReaderSource(std::move(reader), &promise, audioFormat);
    return false;
  };
  if (queueCallback(&callback) != UNQUEUED_CALLBACK) {
    return future.get();
  }
  return ERROR_SOURCE_ID;
}

QTIMediaPlayer::SourceId QTIMediaPlayer::setSource(std::shared_ptr<std::istream> stream, bool repeat) {
  syslog(LOG_DEBUG, "QTIMediaPlayer-%s : Thread %x : setSourceCalled and sourceType = IstreamReader", m_MediaplayerType.c_str(), std::this_thread::get_id());
  std::promise<QTIMediaPlayer::SourceId> promise;
  auto future = promise.get_future();

  std::function<gboolean()> callback = [this, &stream, repeat, &promise]() {
    handleSetIStreamSource(stream, repeat, &promise);
    return false;
  };
  if (queueCallback(&callback) != UNQUEUED_CALLBACK) {
    return future.get();
  }
  return ERROR_SOURCE_ID;
}

QTIMediaPlayer::SourceId QTIMediaPlayer::setSource(const std::string& url, std::chrono::milliseconds offset, bool repeat) {
  syslog(LOG_DEBUG, "QTIMediaPlayer-%s : Thread %x : setSourceForUrlCalled and sourceType=URL : url = %s ", m_MediaplayerType.c_str(), std::this_thread::get_id(), url.c_str());
  std::promise<QTIMediaPlayer::SourceId> promise;
  auto future = promise.get_future();

  std::function<gboolean()> callback = [this, url, offset, &promise, repeat]() {
    handleSetUrlSource(url, offset, &promise, repeat);
    return false;
  };
  if (queueCallback(&callback) != UNQUEUED_CALLBACK) {
    return future.get();
  }
  return ERROR_SOURCE_ID;
}

uint64_t QTIMediaPlayer::getNumBytesBuffered() {
  syslog(LOG_DEBUG, "QTIMediaPlayer-%s : Thread %x : getNumBytesBuffered() ", m_MediaplayerType.c_str(), std::this_thread::get_id());
  if (player) {
    syslog(LOG_DEBUG, "QTIMediaPlayer-%s : getNumBytesBuffered() : GetBufferLevel called ", m_MediaplayerType.c_str());
    return player->GetBufferLevel();
  } else {
    syslog(LOG_ERR, "QTIMediaPlayer-%s : getNumBytesBuffered() : failed reason:player is nullptr", m_MediaplayerType.c_str());
  }
  return 0;
}

bool QTIMediaPlayer::play(QTIMediaPlayer::SourceId id) {
  syslog(LOG_DEBUG, "QTIMediaPlayer-%s : Thread %x : playCalled", m_MediaplayerType.c_str(), std::this_thread::get_id());
  std::promise<bool> promise;
  auto future = promise.get_future();

  std::function<gboolean()> callback = [this, id, &promise]() {
    handlePlay(id, &promise);
    return false;
  };

  if (queueCallback(&callback) != UNQUEUED_CALLBACK) {
    return future.get();
  }
  return false;
}

bool QTIMediaPlayer::stop(QTIMediaPlayer::SourceId id) {
  syslog(LOG_DEBUG, "QTIMediaPlayer-%s : Thread %x : stopCalled", m_MediaplayerType.c_str(), std::this_thread::get_id());
  std::promise<bool> promise;
  auto future = promise.get_future();

  std::function<gboolean()> callback = [this, id, &promise]() {
    handleStop(id, &promise);
    return false;
  };
  if (queueCallback(&callback) != UNQUEUED_CALLBACK) {
    return future.get();
  }
  return false;
}

bool QTIMediaPlayer::pause(QTIMediaPlayer::SourceId id) {
  syslog(LOG_DEBUG, "QTIMediaPlayer-%s : Thread %x : pausedCalled", m_MediaplayerType.c_str(), std::this_thread::get_id());
  std::promise<bool> promise;
  auto future = promise.get_future();

  std::function<gboolean()> callback = [this, id, &promise]() {
    handlePause(id, &promise);
    return false;
  };
  if (queueCallback(&callback) != UNQUEUED_CALLBACK) {
    return future.get();
  }
  return false;
}

bool QTIMediaPlayer::resume(QTIMediaPlayer::SourceId id) {
  syslog(LOG_DEBUG, "QTIMediaPlayer-%s : Thread %x : resumeCalled", m_MediaplayerType.c_str(), std::this_thread::get_id());

  std::promise<bool> promise;
  auto future = promise.get_future();

  std::function<gboolean()> callback = [this, id, &promise]() {
    handleResume(id, &promise);
    return false;
  };
  if (queueCallback(&callback) != UNQUEUED_CALLBACK) {
    return future.get();
  }
  return false;
}

std::chrono::milliseconds QTIMediaPlayer::getOffset(QTIMediaPlayer::SourceId id) {
  syslog(LOG_DEBUG, "QTIMediaPlayer-%s : Thread %x : getOffsetCalled", m_MediaplayerType.c_str(), std::this_thread::get_id());

  std::promise<std::chrono::milliseconds> promise;
  auto future = promise.get_future();
  std::function<gboolean()> callback = [this, id, &promise]() {
    handleGetOffset(id, &promise);
    return false;
  };

  if (queueCallback(&callback) != UNQUEUED_CALLBACK) {
    return future.get();
  }
  return MEDIA_PLAYER_INVALID_OFFSET;
}

void QTIMediaPlayer::setObserver(std::shared_ptr<MediaPlayerObserverInterface> observer) {
  syslog(LOG_DEBUG, "QTIMediaPlayer-%s : Thread %x : setObserverCalled", m_MediaplayerType.c_str(), std::this_thread::get_id());
  std::promise<void> promise;
  auto future = promise.get_future();

  std::function<gboolean()> callback = [this, &promise, &observer]() {
    handleSetObserver(&promise, observer);
    return false;
  };

  if (queueCallback(&callback) != UNQUEUED_CALLBACK) {
    future.wait();
  }
}

bool QTIMediaPlayer::setVolume(int8_t volume) {
  syslog(LOG_DEBUG, "QTIMediaPlayer-%s : Thread %x : setVolumeCalled=%d ", m_MediaplayerType.c_str(), std::this_thread::get_id(), volume);

  std::promise<bool> promise;
  auto future = promise.get_future();
  std::function<gboolean()> callback = [this, &promise, volume]() {
    handleSetVolume(&promise, volume);
    return false;
  };
  if (queueCallback(&callback) != UNQUEUED_CALLBACK) {
    return future.get();
  }
  return false;
}

void QTIMediaPlayer::handleSetVolumeInternal(double gstVolume) {
  syslog(LOG_DEBUG, "QTIMediaPlayer-%s : handleSetVolumeInternal() :  Volume=%f", m_MediaplayerType.c_str(), gstVolume);
  if (gstVolume == 0) {
    //g_object_set(m_pipeline.volume, "volume", VOLUME_ZERO, NULL);

    auto res = player->SetVolume(VOLUME_ZERO);
    if (!res) {
      syslog(LOG_ERR, "QTIMediaPlayer-%s : handleSetVolumeInternal() :  SetVolume(VOLUME_ZERO) failed ", m_MediaplayerType.c_str());
    }
  } else {
    // g_object_set(m_pipeline.volume, "volume", gstVolume, NULL);
    auto res = player->SetVolume(gstVolume);
    if (!res) {
      syslog(LOG_ERR, "QTIMediaPlayer-%s : handleSetVolumeInternal() :  SetVolume(Volume) failed", m_MediaplayerType.c_str());
    }
  }
}

void QTIMediaPlayer::handleSetVolume(std::promise<bool>* promise, int8_t volume) {
  syslog(LOG_INFO, "QTIMediaPlayer-%s : handleSetVolume() : new_volume=%d", m_MediaplayerType.c_str(), volume);

  //*************** do not call setvolume if current volume is same as new volume************************
  auto toAVSVolume =
      Normalizer::create(GST_SET_VOLUME_MIN, GST_SET_VOLUME_MAX, AVS_SET_VOLUME_MIN, AVS_SET_VOLUME_MAX);
  if (!toAVSVolume) {
    syslog(LOG_ERR, "QTIMediaPlayer-%s : handleSetVolume() : failed reason:createNormalizerFailed", m_MediaplayerType.c_str());
    promise->set_value(false);
    return;
  }

  double avsVolume;
  double gstVolume1;

  gstVolume1 = player->GetVolume();
  syslog(LOG_ERR, "QTIMediaPlayer-%s : handleSetVolume() : GetVolume=%f ", m_MediaplayerType.c_str(), gstVolume1);

  if (!toAVSVolume->normalize(gstVolume1, &avsVolume)) {
    syslog(LOG_ERR, "QTIMediaPlayer-%s : handleSetVolume() : failed reason:normalizeVolumeFailed", m_MediaplayerType.c_str());
    promise->set_value(false);
    return;
  }

  // AVS Volume will be between 0 and 100.
  int8_t cur_volume = static_cast<int8_t>(std::round(avsVolume));

  if (volume == cur_volume) {
    syslog(LOG_INFO, "QTIMediaPlayer-%s : handleSetVolume() : new_volume is same as cur_volume so return", m_MediaplayerType.c_str());
    promise->set_value(true);
    return;
  }
  //*************** do not call setvolume if current volume is same as new volume************************

  syslog(LOG_INFO, "QTIMediaPlayer-%s : handleSetVolume() : new_volume!= cur_volume ", m_MediaplayerType.c_str());

  auto toGstVolume =
      Normalizer::create(AVS_SET_VOLUME_MIN, AVS_SET_VOLUME_MAX, GST_SET_VOLUME_MIN, GST_SET_VOLUME_MAX);
  if (!toGstVolume) {
    syslog(LOG_ERR, "QTIMediaPlayer-%s : handleSetVolume(): failed  reason:createNormalizerFailed", m_MediaplayerType.c_str());
    promise->set_value(false);
    return;
  }
  double gstVolume;

  if (!toGstVolume->normalize(volume, &gstVolume)) {
    syslog(LOG_ERR, "QTIMediaPlayer-%s : handleSetVolume() : failed  reason:normalizeVolumeFailed", m_MediaplayerType.c_str());
    promise->set_value(false);
    return;
  }
  handleSetVolumeInternal(gstVolume);
  promise->set_value(true);
}

bool QTIMediaPlayer::adjustVolume(int8_t delta) {
  syslog(LOG_DEBUG, "QTIMediaPlayer-%s : Thread %x : adjustVolumeCalled=%d", m_MediaplayerType.c_str(), std::this_thread::get_id(), delta);

  std::promise<bool> promise;
  auto future = promise.get_future();
  std::function<gboolean()> callback = [this, &promise, delta]() {
    handleAdjustVolume(&promise, delta);
    return false;
  };
  if (queueCallback(&callback) != UNQUEUED_CALLBACK) {
    return future.get();
  }
  return false;
}

void QTIMediaPlayer::handleAdjustVolume(std::promise<bool>* promise, int8_t delta) {
  syslog(LOG_INFO, "QTIMediaPlayer-%s : handleAdjustVolume() : delta =%d", m_MediaplayerType.c_str(), delta);
  auto toGstDeltaVolume =
      Normalizer::create(AVS_ADJUST_VOLUME_MIN, AVS_ADJUST_VOLUME_MAX, GST_ADJUST_VOLUME_MIN, GST_ADJUST_VOLUME_MAX);

  if (!toGstDeltaVolume) {
    syslog(LOG_ERR, "QTIMediaPlayer-%s : handleAdjustVolume() : failed  reason:createNormalizerFailed", m_MediaplayerType.c_str());
    promise->set_value(false);
    return;
  }

  double gstVolume;
  //g_object_get(m_pipeline.volume, "volume", &gstVolume, NULL);
  gstVolume = player->GetVolume();
  syslog(LOG_ERR, "QTIMediaPlayer-%s : handleAdjustVolume() : GetVolume=%f ", m_MediaplayerType.c_str(), gstVolume);

  double gstDelta;
  if (!toGstDeltaVolume->normalize(delta, &gstDelta)) {
    syslog(LOG_ERR, "QTIMediaPlayer-%s : handleAdjustVolume() : failed reason:normalizeVolumeFailed", m_MediaplayerType.c_str());
    promise->set_value(false);
    return;
  }

  gstVolume += gstDelta;

  // If adjustment exceeds bounds, cap at max/min.
  gstVolume = std::min(gstVolume, static_cast<double>(GST_SET_VOLUME_MAX));
  gstVolume = std::max(gstVolume, static_cast<double>(GST_SET_VOLUME_MIN));

  handleSetVolumeInternal(gstVolume);

  promise->set_value(true);
}

bool QTIMediaPlayer::setMute(bool mute) {
  syslog(LOG_DEBUG, "QTIMediaPlayer-%s : Thread %x : setMuteCalled=%d ", m_MediaplayerType.c_str(), std::this_thread::get_id(), mute);

  std::promise<bool> promise;
  auto future = promise.get_future();
  std::function<gboolean()> callback = [this, &promise, mute]() {
    handleSetMute(&promise, mute);
    return false;
  };
  if (queueCallback(&callback) != UNQUEUED_CALLBACK) {
    return future.get();
  }
  return false;
}

void QTIMediaPlayer::handleSetMute(std::promise<bool>* promise, bool mute) {
  syslog(LOG_INFO, "QTIMediaPlayer-%s : handleSetMute() : new_mute=%d", m_MediaplayerType.c_str(), mute);

  //*************** do not call setmute if current_mute value is same as new_mute value***********************
  bool cur_mute = player->GetMute();

  syslog(LOG_DEBUG, "QTIMediaPlayer-%s : handleSetMute() :  GetMute=%d", m_MediaplayerType.c_str(), cur_mute);

  if (mute == cur_mute) {
    syslog(LOG_INFO, "QTIMediaPlayer-%s : handleSetMute() : new_mute is same as cur_mute so return", m_MediaplayerType.c_str());
    promise->set_value(true);
    return;
  }
  //*************** do not call setmute if current_mute value is same as new_mute value***********************
  syslog(LOG_INFO, "QTIMediaPlayer-%s : handleSetMute() : new_mute!= cur_mute ", m_MediaplayerType.c_str());

  player->SetMute(mute);

  promise->set_value(true);
}

bool QTIMediaPlayer::getSpeakerSettings(SpeakerInterface::SpeakerSettings* settings) {
  syslog(LOG_DEBUG, "QTIMediaPlayer-%s : Thread %x : getSpeakerSettingsCalled ", m_MediaplayerType.c_str(), std::this_thread::get_id());

  std::promise<bool> promise;
  auto future = promise.get_future();
  std::function<gboolean()> callback = [this, &promise, settings]() {
    handleGetSpeakerSettings(&promise, settings);
    return false;
  };
  if (queueCallback(&callback) != UNQUEUED_CALLBACK) {
    return future.get();
  }
  return false;
}
void QTIMediaPlayer::handleGetSpeakerSettings(
    std::promise<bool>* promise,
    avsCommon::sdkInterfaces::SpeakerInterface::SpeakerSettings* settings) {
  syslog(LOG_DEBUG, "QTIMediaPlayer-%s : handleGetSpeakerSettings() ", m_MediaplayerType.c_str());
  if (!settings) {
    syslog(LOG_ERR, "QTIMediaPlayer-%s : handleGetSpeakerSettings() : failed reason:nullSettings", m_MediaplayerType.c_str());
    promise->set_value(false);
    return;
  }

  auto toAVSVolume =
      Normalizer::create(GST_SET_VOLUME_MIN, GST_SET_VOLUME_MAX, AVS_SET_VOLUME_MIN, AVS_SET_VOLUME_MAX);
  if (!toAVSVolume) {
    syslog(LOG_ERR, "QTIMediaPlayer-%s : handleGetSpeakerSettings() : failed reason:createNormalizerFailed", m_MediaplayerType.c_str());
    promise->set_value(false);
    return;
  }

  double avsVolume;
  double gstVolume;
  bool mute;
  //g_object_get(m_pipeline.volume, "volume", &gstVolume, "mute", &mute, NULL);
  gstVolume = player->GetVolume();
  mute = player->GetMute();

  syslog(LOG_ERR, "QTIMediaPlayer-%s : handleAdjustVolume() : GetVolume=%f and GetMute=%d", m_MediaplayerType.c_str(), gstVolume, mute);

  if (!toAVSVolume->normalize(gstVolume, &avsVolume)) {
    syslog(LOG_ERR, "QTIMediaPlayer-%s : handleGetSpeakerSettings() : failed reason:normalizeVolumeFailed", m_MediaplayerType.c_str());
    promise->set_value(false);
    return;
  }

  // AVS Volume will be between 0 and 100.
  settings->volume = static_cast<int8_t>(std::round(avsVolume));
  settings->mute = mute;

  promise->set_value(true);
}

SpeakerInterface::Type QTIMediaPlayer::getSpeakerType() {
  syslog(LOG_DEBUG, "QTIMediaPlayer-%s : Thread %x : getSpeakerTypeCalled() ", m_MediaplayerType.c_str(), std::this_thread::get_id());
  return m_speakerType;
}

QTIMediaPlayer::QTIMediaPlayer(
    std::shared_ptr<avsCommon::sdkInterfaces::HTTPContentFetcherInterfaceFactoryInterface> contentFetcherFactory,
    bool enableEqualizer,
    SpeakerInterface::Type type,
    std::string name,
    bool enableLiveMode)
    : RequiresShutdown{name}, m_contentFetcherFactory{contentFetcherFactory}, m_equalizerEnabled{enableEqualizer}, m_speakerType{type}, m_playbackStartedSent{false}, m_playbackFinishedSent{false}, m_isPaused{false}, m_isBufferUnderrun{false}, m_playerObserver{nullptr}, m_currentId{ERROR}, m_playPending{false}, m_pausePending{false}, m_resumePending{false}, m_pauseImmediately{false}, m_isLiveMode{enableLiveMode}, player{nullptr}, m_oldstateisplay{false}, m_sourceRetryCount{0}, m_IstreamRepeat{false}, m_pauseonBuffering{false}, m_AttachmentRepeat{false} {
}

void QTIMediaPlayer::tearDownTransientPipelineElements(bool notifyStop) {
  syslog(LOG_DEBUG, "QTIMediaPlayer-%s : Thread %x : tearDownTransientPipelineElements() ", m_MediaplayerType.c_str(), std::this_thread::get_id());
  m_offsetBeforeTeardown = getCurrentStreamOffset();
  if (notifyStop) {
    syslog(LOG_DEBUG, "QTIMediaPlayer-%s : tearDownTransientPipelineElements() : sendPlaybackStopped called", m_MediaplayerType.c_str());
    sendPlaybackStopped();
  }
  m_currentId = ERROR_SOURCE_ID;
  //TODO need to check how to call stop()
  cleanUpSource();
  m_offsetManager.clear();
  m_playPending = false;
  m_pausePending = false;
  m_resumePending = false;
  m_pauseImmediately = false;
  m_playbackStartedSent = false;
  //m_playbackFinishedSent = false;
  m_isPaused = false;
  m_isBufferUnderrun = false;
  m_oldstateisplay = false;
  m_IstreamRepeat = false;
  m_pauseonBuffering = false;
  m_AttachmentRepeat = false;
}

bool QTIMediaPlayer::init(std::string type) {
  syslog(LOG_DEBUG, "QTIMediaPlayer-%s : init() ", type.c_str());

  //To-do need to check which StreamType to choose at the time creation
  std::unique_ptr<adk::MediaPlayer> temp_player;
  if (type == "SpeakMediaPlayer") {
    temp_player = adk::MediaPlayer::Create("VoiceUI-QTI-SpeakMediaPlayer", adk::MediaPlayer::StreamType::VoiceUI);
  } else if (type == "AudioMediaPlayer") {
    temp_player = adk::MediaPlayer::Create("VoiceUI-QTI-AudioMediaPlayer", adk::MediaPlayer::StreamType::Music);
  } else if (type == "NotificationsMediaPlayer") {
    temp_player = adk::MediaPlayer::Create("VoiceUI-QTI-NotificationsMediaPlayer", adk::MediaPlayer::StreamType::Cue);
  } else if (type == "BluetoothMediaPlayer") {
    temp_player = adk::MediaPlayer::Create("VoiceUI-QTI-BluetoothMediaPlayer", adk::MediaPlayer::StreamType::Music);
  } else if (type == "RingtoneMediaPlayer") {
    temp_player = adk::MediaPlayer::Create("VoiceUI-QTI-RingtoneMediaPlayer", adk::MediaPlayer::StreamType::Tone);
  } else if (type == "AlertsMediaPlayer") {
    temp_player = adk::MediaPlayer::Create("VoiceUI-QTI-AlertsMediaPlayer", adk::MediaPlayer::StreamType::Alarm);
  } else if (type == "") {
    type = "UnitTestMediaPlayer";
    temp_player = adk::MediaPlayer::Create("VoiceUI-QTI-UnitTestMediaPlayer", adk::MediaPlayer::StreamType::Cue);
  } else {
    syslog(LOG_DEBUG, "QTIMediaPlayer-%s : init() : no Mediaplayer match found for creation", type.c_str());
  }

  if (temp_player != nullptr) {
    player = std::move(temp_player);
    m_MediaplayerType = type;
    syslog(LOG_INFO, "QTIMediaPlayer-%s : init() : created successfully", m_MediaplayerType.c_str());
  } else {
    syslog(LOG_ERR, "QTIMediaPlayer-%s : init() : failed in creation", m_MediaplayerType.c_str());
    return false;
  }

  //register callbacks for Adk-Mediaplayer
  syslog(LOG_DEBUG, "QTIMediaPlayer-%s : init() : register callbacks", m_MediaplayerType.c_str());
  auto OnEndOfStream = std::bind(std::mem_fn(&QTIMediaPlayer::OnEndOfStream), this);
  auto OnError_Adk = std::bind(std::mem_fn(&QTIMediaPlayer::OnError_Adk), this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);
  auto OnStateChanged = std::bind(std::mem_fn(&QTIMediaPlayer::OnStateChanged), this, std::placeholders::_1);
  auto OnBuffering = std::bind(std::mem_fn(&QTIMediaPlayer::OnBuffering), this, std::placeholders::_1);
  auto OnMetadataChanged = std::bind(std::mem_fn(&QTIMediaPlayer::OnMetadataChanged), this);

  player->SetOnEndOfStreamCb(OnEndOfStream);
  player->SetOnErrorCb(OnError_Adk);
  player->SetOnStateChangedCb(OnStateChanged);
  player->SetBufferingCb(OnBuffering);
  player->SetOnMetadataChangedCb(OnMetadataChanged);

  //serialisation using Gobject queueCallback
  m_workerContext = g_main_context_new();
  if (!m_workerContext) {
    syslog(LOG_DEBUG, "QTIMediaPlayer-%s : init() :initPlayerFailed : reason:nullWorkerContext ", m_MediaplayerType.c_str());
    return false;
  }
  if (!(m_mainLoop = g_main_loop_new(m_workerContext, false))) {
    syslog(LOG_DEBUG, "QTIMediaPlayer-%s : init() :initPlayerFailed : reason:gstMainLoopNewFailed ", m_MediaplayerType.c_str());
    return false;
  };

  //Configure Speaker mediaplayer to use tlenght of 40ms.
  std::chrono::milliseconds tlenght_ms(40);
  if (type == "SpeakMediaPlayer") {
    player->SetPreferredSinkLatencyInMilliSeconds(tlenght_ms);
    syslog(LOG_DEBUG, "QTIMediaPlayer-%s : SetPreferredSinkLatencyInMilliSeconds() to 40 ms - DONE!!!", m_MediaplayerType.c_str());
  }

  m_mainLoopThread = std::thread(&QTIMediaPlayer::workerLoop, this);
  return true;
}

bool QTIMediaPlayer::queryIsSeekable(bool* isSeekable) {
  syslog(LOG_DEBUG, "QTIMediaPlayer-%s : queryIsSeekable() ", m_MediaplayerType.c_str());
  if (player) {
    syslog(LOG_WARNING, "QTIMediaPlayer-%s : queryIsSeekable() : GetCurrentState called", m_MediaplayerType.c_str());

    auto ret = player->GetCurrentState();

    // Getting the state failed.   (GST_STATE_CHANGE_FAILURE)
    if (std::get<1>(ret) == adk::MediaPlayer::Result::Failure) {
      syslog(LOG_INFO, "QTIMediaPlayer-%s : queryIsSeekable() : failed reason:gstElementGetStateFailed", m_MediaplayerType.c_str());
      return false;
    }
    // Getting the state was not successful (GST_STATE_CHANGE_ASYNC or GST_STATE_CHANGE_NO_PREROLL).
    if (std::get<1>(ret) == adk::MediaPlayer::Result::Async) {
      syslog(LOG_INFO, "QTIMediaPlayer-%s : queryIsSeekable() : failed reason:gstElementGetStateFailed:stateChangeAsync", m_MediaplayerType.c_str());
      return false;
    }

    if ((std::get<0>(ret) != adk::MediaPlayer::State::Playing) && (std::get<0>(ret) != adk::MediaPlayer::State::Paused)) {
      syslog(LOG_ERR, "QTIMediaPlayer-%s : queryIsSeekable() : failed reason:notPlayingOrPaused", m_MediaplayerType.c_str());
      *isSeekable = false;
      return true;
    }
    *isSeekable = true;
    return true;
    // TODO: ACSDK-1778: Investigate why gst_query_parse_seeking() is not working.
    // If it's usable again, use gst_query_parse_seeking() instead of gst_app_src_get_stream_type()
    /*
    bool res = player->IsSeekable();
    if (res) {
      *isSeekable = true;
      return true;
    } else {
      syslog(LOG_ERR, "QTIMediaPlayer-%s : queryIsSeekable() : stream is not seekable()", m_MediaplayerType.c_str());
      return false;
    }
    */
  } else {
    syslog(LOG_ERR, "QTIMediaPlayer-%s : queryIsSeekable() : failed reason: player is nullptr", m_MediaplayerType.c_str());
    return false;
  }
}

bool QTIMediaPlayer::seek() {
  bool seekSuccessful = true;
  syslog(LOG_INFO, "QTIMediaPlayer-%s : seek() ", m_MediaplayerType.c_str());
  if (!m_offsetManager.isSeekable() || !m_offsetManager.isSeekPointSet()) {
    syslog(LOG_DEBUG, "QTIMediaPlayer-%s : seekFailed reason:Stream is not seekable or seekPointSet is not set ", m_MediaplayerType.c_str());
    seekSuccessful = false;
  } else {
    if (player) {
      syslog(LOG_INFO, "QTIMediaPlayer-%s : seek() : player->Seek() called", m_MediaplayerType.c_str());
      auto res = player->Seek(std::chrono::milliseconds(m_offsetManager.getSeekPoint()));
      if (res == adk::MediaPlayer::Failure) {
        syslog(LOG_INFO, "QTIMediaPlayer-%s : seek() : player->Seek() failed reson: adk-MediaPlayer Failure ", m_MediaplayerType.c_str());
        seekSuccessful = false;
      } else {
        //std::cout << "Seek point = "
        //          << std::chrono::duration_cast<std::chrono::nanoseconds>(m_offsetManager.getSeekPoint()).count()
        //          << std::endl;
      }
    } else {
      syslog(LOG_ERR, "QTIMediaPlayer-%s : seek() : failed reason: player is nullptr", m_MediaplayerType.c_str());
      seekSuccessful = false;
    }
  }
  m_offsetManager.clear();
  return seekSuccessful;
}

void QTIMediaPlayer::onError() {
  syslog(LOG_INFO, "QTIMediaPlayer-%s : Thread %x : onError", m_MediaplayerType.c_str(), std::this_thread::get_id());
  //std::cout << "QTIMediaPlayer-" << m_MediaplayerType << " onError() : streamingError " << std::endl;

  std::thread t([&] {
    sendPlaybackError(ErrorType::MEDIA_ERROR_INTERNAL_DEVICE_ERROR, "streamingError");
  });
  t.detach();
}

void QTIMediaPlayer::doShutdown() {
  syslog(LOG_DEBUG, "QTIMediaPlayer-%s : Thread %x : doShutdown", m_MediaplayerType.c_str(), std::this_thread::get_id());
  if (m_urlConverter) {
    m_urlConverter->shutdown();
  }
  m_urlConverter.reset();
  m_playerObserver.reset();
}

void QTIMediaPlayer::ReadDataFromAttachmentReader(size_t size_needed) {
  syslog(LOG_DEBUG, "QTIMediaPlayer-%s : ReadDataFromAttachmentReader() : size_needed=%lu", m_MediaplayerType.c_str(), size_needed);

  if (m_AttachmentReader == nullptr) {
    syslog(LOG_ERR, "QTIMediaPlayer-%s : ReadDataFromAttachmentReader() :  failed reason:AttachmentReader is Null ", m_MediaplayerType.c_str());
    return;
  }
  std::vector<uint8_t> buffer(size_needed);

  auto status = AttachmentReader::ReadStatus::OK;
  auto size_filled = m_AttachmentReader->read(buffer.data(), buffer.size(), &status, std::chrono::milliseconds(1));

  if (size_filled > 0 && size_filled < size_needed) {
    buffer.resize(size_filled);
  }

  switch (status) {
    case AttachmentReader::ReadStatus::CLOSED:
      if (0 == size_filled) {
        syslog(LOG_INFO, "QTIMediaPlayer-%s : ReadDataFromAttachmentReader() : AttachmentReader is CLOSED", m_MediaplayerType.c_str());
        break;
      }
    // Fall through if some data was read.
    case AttachmentReader::ReadStatus::OK:
    case AttachmentReader::ReadStatus::OK_WOULDBLOCK:
    // Fall through to retry reading later.
    case AttachmentReader::ReadStatus::OK_TIMEDOUT:
      if (size_filled > 0) {
        syslog(LOG_INFO, "QTIMediaPlayer-%s : ReadDataFromAttachmentReader() : PushBuffer called and size_filled=%zu", m_MediaplayerType.c_str(), size_filled);
        m_sourceRetryCount = 0;
        auto res = player->PushBuffer(buffer.data(), size_filled);

        if (!res) {
          syslog(LOG_INFO, "QTIMediaPlayer-%s : ReadDataFromAttachmentReader() : PushBuffer failed", m_MediaplayerType.c_str());
          break;
        }
      } else {
        //free vector memory
        buffer.clear();
        buffer.shrink_to_fit();

        if (m_sourceRetryCount < (sizeof(RETRY_INTERVALS_MILLISECONDS) / sizeof(RETRY_INTERVALS_MILLISECONDS[0]))) {
          auto interval = RETRY_INTERVALS_MILLISECONDS[m_sourceRetryCount];
          syslog(LOG_INFO, "QTIMediaPlayer-%s : ReadDataFromAttachmentReader() : Wait=%ums and Retry Again RetryCount=%u", m_MediaplayerType.c_str(), interval, m_sourceRetryCount);
          m_sourceRetryCount++;
          std::this_thread::sleep_for(std::chrono::milliseconds(interval));
          ReadDataFromAttachmentReader(size_needed);
        }
      }
      return;
    case AttachmentReader::ReadStatus::OK_OVERRUN_RESET:  // gstreamer requires stable stream.
    case AttachmentReader::ReadStatus::ERROR_OVERRUN:
    case AttachmentReader::ReadStatus::ERROR_BYTES_LESS_THAN_WORD_SIZE:
    case AttachmentReader::ReadStatus::ERROR_INTERNAL:
      auto error = static_cast<int>(status);
      syslog(LOG_ERR, "QTIMediaPlayer-%s : ReadDataFromAttachmentReader() failed : reason:readFailed error=%d", m_MediaplayerType.c_str(), error);
      break;
  }

  if (!m_AttachmentRepeat) {
    syslog(LOG_INFO, "QTIMediaPlayer-%s : ReadDataFromAttachmentReader() : PushEndOfStream called", m_MediaplayerType.c_str());
    m_sourceRetryCount = 0;
    //free vector memory
    buffer.clear();
    buffer.shrink_to_fit();

    auto res2 = player->PushEndOfStream();
    if (!res2) {
      syslog(LOG_INFO, "QTIMediaPlayer-%s : ReadDataFromAttachmentReader() : PushEndOfStream failed", m_MediaplayerType.c_str());
    }
    return;
  }

  m_AttachmentReader->seek(0);
  //free vector memory
  buffer.clear();
  buffer.shrink_to_fit();

  if (m_sourceRetryCount < (sizeof(RETRY_INTERVALS_MILLISECONDS) / sizeof(RETRY_INTERVALS_MILLISECONDS[0]))) {
    auto interval = RETRY_INTERVALS_MILLISECONDS[m_sourceRetryCount];
    syslog(LOG_INFO, "QTIMediaPlayer-%s : ReadDataFromAttachmentReader() : Wait=%ums and Retry Again RetryCount=%u", m_MediaplayerType.c_str(), interval, m_sourceRetryCount);
    m_sourceRetryCount++;
    std::this_thread::sleep_for(std::chrono::milliseconds(interval));
    ReadDataFromAttachmentReader(size_needed);
  }
  return;
}

void QTIMediaPlayer::ReadDataFromIstreamReader(size_t size_needed) {
  syslog(LOG_DEBUG, "QTIMediaPlayer-%s : ReadDataFromIstreamReader() : size_needed=%lu", m_MediaplayerType.c_str(), size_needed);
  if (m_IstreamReader == nullptr) {
    syslog(LOG_ERR, "QTIMediaPlayer-%s :  ReadDataFromIstreamReader() failed reason:IstreamReader is Null", m_MediaplayerType.c_str());
    return;
  }

  if (m_IstreamRepeat && m_IstreamReader->eof()) {
    m_IstreamReader->clear();
    m_IstreamReader->seekg(0);
  }

  std::vector<uint8_t> buffer(size_needed);

  m_IstreamReader->read(reinterpret_cast<std::istream::char_type*>(buffer.data()), buffer.size());

  unsigned long size_filled;

  if (m_IstreamReader->bad()) {
    size_filled = 0;
    syslog(LOG_WARNING, "QTIMediaPlayer-%s : ReadDataFromIstreamReader() : Read failed reason:m_IstreamReader->bad() ", m_MediaplayerType.c_str());
  } else {
    size_filled = m_IstreamReader->gcount();
    //TODO(c_pnemag): add log for pos and eof
  }

  if (size_filled > 0) {
    if (size_filled < size_needed) {
      buffer.resize(size_filled);
    }

    syslog(LOG_INFO, "QTIMediaPlayer-%s : ReadDataFromIstreamReader() : PushBuffer called and size_filled=%zu bytes", m_MediaplayerType.c_str(), size_filled);
    m_sourceRetryCount = 0;
    auto res = player->PushBuffer(buffer.data(), size_filled);

    if (!res) {
      syslog(LOG_INFO, "QTIMediaPlayer-%s : ReadDataFromIstreamReader() : PushBuffer failed", m_MediaplayerType.c_str());
      return;
    } else {
      return;
    }
  }

  if (m_IstreamReader->bad() || (!m_IstreamRepeat && m_IstreamReader->eof())) {
    syslog(LOG_INFO, "QTIMediaPlayer-%s : ReadDataFromIstreamReader() : PushEndOfStream called ", m_MediaplayerType.c_str());
    m_sourceRetryCount = 0;
    //free vector memory
    buffer.clear();
    buffer.shrink_to_fit();
    auto res2 = player->PushEndOfStream();
    if (!res2) {
      syslog(LOG_INFO, "QTIMediaPlayer-%s : ReadDataFromIstreamReader() : PushEndOfStream fail ", m_MediaplayerType.c_str());
    }
    return;
  }

  //free vector memory
  buffer.clear();
  buffer.shrink_to_fit();

  if (m_sourceRetryCount < sizeof(RETRY_INTERVALS_MILLISECONDS) / sizeof(RETRY_INTERVALS_MILLISECONDS[0])) {
    auto interval = RETRY_INTERVALS_MILLISECONDS[m_sourceRetryCount];
    syslog(LOG_INFO, "QTIMediaPlayer-%s : ReadDataFromIstreamReader() : Wait=%u ms and Retry Again RetryCount=%u", m_MediaplayerType.c_str(), interval, m_sourceRetryCount);
    m_sourceRetryCount++;
    std::this_thread::sleep_for(std::chrono::milliseconds(interval));
    ReadDataFromIstreamReader(size_needed);
  }
  return;
}
void QTIMediaPlayer::handleSetAttachmentReaderSource(
    std::shared_ptr<AttachmentReader> reader,
    std::promise<QTIMediaPlayer::SourceId>* promise,
    const avsCommon::utils::AudioFormat* audioFormat,
    bool repeat) {
  syslog(LOG_INFO, "QTIMediaPlayer-%s : handleSetAttachmentReaderSource() ", m_MediaplayerType.c_str());

  tearDownTransientPipelineElements(true);

  m_AttachmentReader = std::move(reader);
  m_AttachmentRepeat = repeat;
  adk::MediaPlayer::MediaInfo info;

  if (audioFormat) {
    syslog(LOG_DEBUG, "QTIMediaPlayer-%s : handleSetAttachmentReaderSource() : getCapsStringcalled", m_MediaplayerType.c_str());

    std::string caps = getCapsString(*audioFormat);
    info = {
        .format = caps};

    if (player) {
      syslog(LOG_DEBUG, "QTIMediaPlayer-%s : handleSetAttachmentReaderSource() :SetSource called", m_MediaplayerType.c_str());

      auto setsource_res = player->SetSource(info, [&](size_t size_needed) {
        syslog(LOG_DEBUG, "QTIMediaPlayer-%s : Thread %x : handleSetAttachmentReaderSource() : onNeedDatacb() ", m_MediaplayerType.c_str(),std::this_thread::get_id());
        m_sourceRetryCount = 0;
        ReadDataFromAttachmentReader(size_needed);
        return; }, [&](uint16_t offset) {
    	  	  	  if (m_AttachmentReader) {
    	syslog(LOG_DEBUG, "QTIMediaPlayer-%s : Thread %x : handleSetAttachmentReaderSource() : onSeekDatacb() offset=%d", m_MediaplayerType.c_str(),std::this_thread::get_id(), offset);
    	  	  		m_AttachmentReader->seek(offset);
    	  	  	  } else {
    	  	  syslog(LOG_ERR, "QTIMediaPlayer-%s : handleSetAttachmentReaderSource() : onSeekDatacb failed : reason:AttachmentReader is nullReader", m_MediaplayerType.c_str());
    	  	  	  } }, [&]() {
    	  	  		syslog(LOG_DEBUG, "QTIMediaPlayer-%s : Thread %x : handleSetAttachmentReaderSource()) : onEnoughdatacb() ", m_MediaplayerType.c_str(),std::this_thread::get_id());
    	  	  		m_sourceRetryCount = 0; });

      if (setsource_res) {
        m_currentId = ++g_id;
        m_offsetManager.setIsSeekable(true);
        promise->set_value(m_currentId);
      } else {
        syslog(LOG_ERR, "QTIMediaPlayer-%s : handleSetAttachmentReaderSource() : SetSource failed ", m_MediaplayerType.c_str());
        promise->set_value(ERROR_SOURCE_ID);
      }
    } else {
      syslog(LOG_ERR, "QTIMediaPlayer-%s : handleSetAttachmentReaderSource() : failed reason:player is nullptr", m_MediaplayerType.c_str());
      promise->set_value(ERROR_SOURCE_ID);
    }
  } else {
    if (player) {
      syslog(LOG_DEBUG, "QTIMediaPlayer-%s : handleSetAttachmentReaderSource() : SetSource called ", m_MediaplayerType.c_str());

      auto setsource_res = player->SetSource([&](size_t size_needed) {
      syslog(LOG_DEBUG, "QTIMediaPlayer-%s : Thread %x : handleSetAttachmentReaderSource() : onNeedDatacb() ", m_MediaplayerType.c_str(),std::this_thread::get_id());
      m_sourceRetryCount = 0;
        ReadDataFromAttachmentReader(size_needed);
        return; }, [&](uint16_t offset) {
	  syslog(LOG_DEBUG, "QTIMediaPlayer-%s : Thread %x : handleSetAttachmentReaderSource() : onSeekDatacb()  offset=%d", m_MediaplayerType.c_str(),std::this_thread::get_id(),offset);
	  	  if (m_AttachmentReader) {
	  		m_AttachmentReader->seek(offset);
	  	  } else {

	  syslog(LOG_ERR, "QTIMediaPlayer-%s : handleSetAttachmentReaderSource() : onSeekDatacb() : failed reason:AttachmentReader is nullReader", m_MediaplayerType.c_str());
	  	  } }, [&]() {
	  	  		syslog(LOG_DEBUG, "QTIMediaPlayer-%s : Thread %x : handleSetAttachmentReaderSource() : onEnoughdatacb() ", m_MediaplayerType.c_str(),std::this_thread::get_id());
	  	  		m_sourceRetryCount = 0; });

      if (setsource_res) {
        m_currentId = ++g_id;
        m_offsetManager.setIsSeekable(true);

        if (m_urlConverter) {
          if (m_urlConverter->getDesiredStreamingPoint() == std::chrono::milliseconds::zero()) {
            syslog(LOG_DEBUG, "QTIMediaPlayer-%s : handleSetAttachmentReaderSource() : getDesiredStreamingPoint is zero", m_MediaplayerType.c_str());
          } else {
            syslog(LOG_DEBUG, "QTIMediaPlayer-%s : handleSetAttachmentReaderSource() : setSeekPoint of offsetManager", m_MediaplayerType.c_str());
            m_offsetManager.setSeekPoint(
                m_urlConverter->getDesiredStreamingPoint() - m_urlConverter->getStartStreamingPoint());

            if (m_offsetManager.isSeekable() && m_offsetManager.isSeekPointSet()) {
              syslog(LOG_INFO, "QTIMediaPlayer-%s : handleSetAttachmentReaderSource() : seek() called ", m_MediaplayerType.c_str());
              if (!seek()) {
                std::string error = "seekFailed";
                syslog(LOG_ERR, "QTIMediaPlayer-%s :handleSetAttachmentReaderSource()  : seekFailed", m_MediaplayerType.c_str());
                sendPlaybackError(ErrorType::MEDIA_ERROR_INTERNAL_DEVICE_ERROR, error);
              };
            }
          }
        }

        promise->set_value(m_currentId);
      } else {
        syslog(LOG_ERR, "QTIMediaPlayer-%s :handleSetARSource: SetSource failed ", m_MediaplayerType.c_str());
        promise->set_value(ERROR_SOURCE_ID);
      }
    } else {
      syslog(LOG_ERR, "QTIMediaPlayer-%s : handleSetAttachmentReaderSource() : failed reason:player is nullptr", m_MediaplayerType.c_str());
      promise->set_value(ERROR_SOURCE_ID);
    }
  }
}

void QTIMediaPlayer::handleSetIStreamSource(
    std::shared_ptr<std::istream> stream,
    bool repeat,
    std::promise<QTIMediaPlayer::SourceId>* promise) {
  tearDownTransientPipelineElements(true);

  syslog(LOG_INFO, "QTIMediaPlayer-%s : handleSetIStreamSource() ", m_MediaplayerType.c_str());
  m_IstreamReader = std::move(stream);
  m_IstreamRepeat = repeat;

  if (player) {
    syslog(LOG_DEBUG, "QTIMediaPlayer-%s : handleSetIStreamSource() : SetSource called ", m_MediaplayerType.c_str());

    auto setsource_res = player->SetSource([&](size_t size_needed) {
       syslog(LOG_DEBUG, "QTIMediaPlayer-%s : Thread %x : handleSetIStreamSource() : onNeedDatacb() ", m_MediaplayerType.c_str(),std::this_thread::get_id());
    	m_sourceRetryCount = 0;
      ReadDataFromIstreamReader(size_needed);
      return; }, [&](uint16_t offset) {
	  syslog(LOG_DEBUG, "QTIMediaPlayer-%s : Thread %x : handleSetIStreamSource(): onSeekDatacb() offset=%d",m_MediaplayerType.c_str(),std::this_thread::get_id(),offset);
	  m_IstreamReader->clear();
	  m_IstreamReader->seekg(offset); }, [&]() {
	  		syslog(LOG_DEBUG, "QTIMediaPlayer-%s : Thread %x : handleSetIStreamSource() : onEnoughdatacb() ", m_MediaplayerType.c_str(),std::this_thread::get_id());
	  		m_sourceRetryCount = 0; });

    if (setsource_res) {
      m_currentId = ++g_id;
      promise->set_value(m_currentId);
    } else {
      syslog(LOG_ERR, "QTIMediaPlayer-%s : handleSetIStreamSource(): SetSource failed ", m_MediaplayerType.c_str());
      promise->set_value(ERROR_SOURCE_ID);
    }
  } else {
    syslog(LOG_ERR, "QTIMediaPlayer-%s : handleSetIStreamSource(): failed reason: player is nullptr", m_MediaplayerType.c_str());
    promise->set_value(ERROR_SOURCE_ID);
  }
}

void QTIMediaPlayer::handleSetUrlSource(
    const std::string& url,
    std::chrono::milliseconds offset,
    std::promise<SourceId>* promise,
    bool repeat) {
  syslog(LOG_INFO, "QTIMediaPlayer-%s : handleSetUrlSource() ", m_MediaplayerType.c_str());

  tearDownTransientPipelineElements(true);

  m_urlConverter = alexaClientSDK::playlistParser::UrlContentToAttachmentConverter::create(
      m_contentFetcherFactory, url, shared_from_this(), offset);
  if (!m_urlConverter) {
    syslog(LOG_ERR, "QTIMediaPlayer-%s : handleSetUrlSource() : failed reason:badUrlConverter", m_MediaplayerType.c_str());
    promise->set_value(ERROR_SOURCE_ID);
    return;
  }

  auto attachment = m_urlConverter->getAttachment();
  if (!attachment) {
    syslog(LOG_ERR, "QTIMediaPlayer-%s : handleSetUrlSource() : failed reason:badAttachmentReceived", m_MediaplayerType.c_str());
    promise->set_value(ERROR_SOURCE_ID);
    return;
  }

  std::shared_ptr<avsCommon::avs::attachment::AttachmentReader> reader =
      attachment->createReader(sds::ReaderPolicy::NONBLOCKING);
  if (!reader) {
    syslog(LOG_ERR, "QTIMediaPlayer-%s : handleSetUrlSource() : failed reason:failedToCreateAttachmentReader", m_MediaplayerType.c_str());
    promise->set_value(ERROR_SOURCE_ID);
    return;
  }

  syslog(LOG_DEBUG, "QTIMediaPlayer-%s : handleSetUrlSource() : handleSetAttachmentReaderSource() called", m_MediaplayerType.c_str());

  handleSetAttachmentReaderSource(reader, promise, nullptr, repeat);
}

void QTIMediaPlayer::handlePlay(SourceId id, std::promise<bool>* promise) {
  //Acquire lock for 5 seconds to setup and start playing
  int delay = WAKELOCK_AVS_MEDIAPLAYER_SETUP_TIMEOUT;
  system_wake_lock_toggle(true, m_MediaplayerType, delay);

  syslog(LOG_INFO, "QTIMediaPlayer-%s : handlePlay() :  idPassed=%lu and currentId=%lu", m_MediaplayerType.c_str(), id, m_currentId);
  if (!validateSourceAndId(id)) {
    syslog(LOG_ERR, "QTIMediaPlayer-%s : handlePlay() : validateSourceAndId failed", m_MediaplayerType.c_str());
    promise->set_value(false);
    return;
  }

  if (player) {
    syslog(LOG_WARNING, "QTIMediaPlayer-%s : handlePlay() : GetCurrentState called", m_MediaplayerType.c_str());

    auto ret = player->GetCurrentState();

    // Getting the Getstate failed return value.   (GST_STATE_CHANGE_FAILURE)
    if (std::get<1>(ret) == adk::MediaPlayer::Result::Failure) {
      syslog(LOG_INFO, "QTIMediaPlayer-%s : handlePlay() : failed reason:gstElementGetStateFailed", m_MediaplayerType.c_str());
      promise->set_value(false);
      return;
    }

    if (std::get<0>(ret) == adk::MediaPlayer::State::Playing) {
      syslog(LOG_INFO, "QTIMediaPlayer-%s : handlePlay() : failed reason:alreadyPlaying", m_MediaplayerType.c_str());
      promise->set_value(false);
      return;
    }

    if (m_playPending) {
      syslog(LOG_INFO, "QTIMediaPlayer-%s : handlePlay(): failed reason:playCurrentlyPending", m_MediaplayerType.c_str());
      promise->set_value(false);
      return;
    }

    m_playbackFinishedSent = false;
    m_playbackStartedSent = false;
    m_playPending = true;
    m_pauseImmediately = false;
    promise->set_value(true);
//TODO: to be handled when Adk-mediaplayer gives queryBufferPercent API
#if 0
    /*
     * If the pipeline is completely buffered, then go straight to PLAY otherwise,
     * set pipeline to PAUSED state to attempt buffering.  The pipeline will be set to PLAY upon receiving buffer
     * percent = 100.
     */
    GstState startingState = GST_STATE_PAUSED;
    if (!m_isLiveMode) {
        /*
         * If the pipeline is completely buffered, then go straight to PLAY otherwise,
         * set pipeline to PAUSED state to attempt buffering.  The pipeline will be set to PLAY upon receiving buffer
         * percent = 100.
         */

        gint percent = 0;
        if ((GST_STATE_PAUSED == curState) && (queryBufferPercent(&percent))) {
            if (100 == percent) {
                startingState = GST_STATE_PLAYING;
            }
        }
    } else {
        startingState = GST_STATE_PLAYING;
    }
#endif

    syslog(LOG_DEBUG, "QTIMediaPlayer-%s : handlePlay() : play Called", m_MediaplayerType.c_str());
    auto res = player->Play();

    switch (res) {
      case adk::MediaPlayer::Failure: {
        const std::string errorMessage{"reason=gstElementSetStateFailure"};
        syslog(LOG_ERR, "QTIMediaPlayer-%s : handlePlay() : failed reason=adk-MediaPlayer Failure", m_MediaplayerType.c_str());
        sendPlaybackError(ErrorType::MEDIA_ERROR_INTERNAL_DEVICE_ERROR, errorMessage);
      }
        return;
      default:
        syslog(LOG_DEBUG, "QTIMediaPlayer-%s : handlePlay() : play success", m_MediaplayerType.c_str());
        if (m_urlConverter) {
          if (m_urlConverter->getDesiredStreamingPoint() == std::chrono::milliseconds::zero()) {
            syslog(LOG_DEBUG, "QTIMediaPlayer-%s : handlePlay() : getDesiredStreamingPoint is zero", m_MediaplayerType.c_str());
            return;
          }
          syslog(LOG_DEBUG, "QTIMediaPlayer-%s : handlePlay() : setSeekPoint of offsetManager", m_MediaplayerType.c_str());
          m_offsetManager.setSeekPoint(
              m_urlConverter->getDesiredStreamingPoint() - m_urlConverter->getStartStreamingPoint());
        }
        // Allow sending callbacks to be handled on the bus message
        return;
    }
  } else {
    syslog(LOG_ERR, "QTIMediaPlayer-%s : handlePlay() : failed reason: player is nullptr", m_MediaplayerType.c_str());
    promise->set_value(false);
    return;
  }
}

void QTIMediaPlayer::handleStop(QTIMediaPlayer::SourceId id, std::promise<bool>* promise) {
  syslog(LOG_INFO, "QTIMediaPlayer-%s : handleStop() : idPassed=%lu and currentId=%lu", m_MediaplayerType.c_str(), id, m_currentId);
  if (!validateSourceAndId(id)) {
    syslog(LOG_ERR, "QTIMediaPlayer-%s : handleStop() : validateSourceAndId failed", m_MediaplayerType.c_str());
    promise->set_value(false);
    return;
  }

  if (player) {
    syslog(LOG_WARNING, "QTIMediaPlayer-%s : handleStop() : GetCurrentState called", m_MediaplayerType.c_str());

    auto ret = player->GetCurrentState();

    // Getting the Getstate failed return value.   (GST_STATE_CHANGE_FAILURE)
    if (std::get<1>(ret) == adk::MediaPlayer::Result::Failure) {
      syslog(LOG_INFO, "QTIMediaPlayer-%s : handleStop() : failed reason:gstElementGetStateFailed", m_MediaplayerType.c_str());
      promise->set_value(false);
      return;
    }

    if (std::get<0>(ret) == adk::MediaPlayer::State::Stopped) {
      syslog(LOG_WARNING, "QTIMediaPlayer-%s : handleStop() : failed reason:alreadyStopped", m_MediaplayerType.c_str());

      //Return true if ALERTS Mediaplayer.
      if (!m_MediaplayerType.compare("AlertsMediaPlayer")) {
        syslog(LOG_INFO, "QTIMediaPlayer-%s : handleStop() : Send PlaybackStop if Alert MediaPlayer", m_MediaplayerType.c_str());
        promise->set_value(true);
        sendPlaybackStopped();
      } else {
        promise->set_value(false);
      }
      return;
    }

    //TODO
    //if the pending state is STOP, then current STOP action will not be performed.
    // Ddk-mediaplayer should handle this case as they did not provide intermediate state details
#if 0
    if (pending == adk::MediaPlayer::State::Stopped) {
    	        ACSDK_ERROR(LX("handleStopFailed").d("reason", "alreadyStopping"));
    	        promise->set_value(false);
    	        return;
    }
#endif
    syslog(LOG_DEBUG, "QTIMediaPlayer-%s : handleStop() : stop Called", m_MediaplayerType.c_str());
    auto res = player->Stop();

    if (res == adk::MediaPlayer::Failure) {
      syslog(LOG_ERR, "QTIMediaPlayer-%s : handleStop() : stop failed reason:adk-MediaPlayer Failure", m_MediaplayerType.c_str());
      promise->set_value(false);
    } else {
      /*
               * Based on GStreamer docs, a gst_element_set_state call to change the state to GST_STATE_NULL will never
               * return GST_STATE_CHANGE_ASYNC.
               */
      promise->set_value(true);
      if (m_playPending) {
        sendPlaybackStarted();
      } else if (m_resumePending) {
        sendPlaybackResumed();
      }
      sendPlaybackStopped();
    }
  } else {
    syslog(LOG_ERR, "QTIMediaPlayer-%s : handleStop() : failed reason:player is nullptr", m_MediaplayerType.c_str());
    promise->set_value(false);
  }
}

void QTIMediaPlayer::handlePause(QTIMediaPlayer::SourceId id, std::promise<bool>* promise) {
  syslog(LOG_INFO, "QTIMediaPlayer-%s : handlePause() : idPassed=%lu and currentId=%lu", m_MediaplayerType.c_str(), id, m_currentId);
  if (!validateSourceAndId(id)) {
    syslog(LOG_ERR, "QTIMediaPlayer-%s : handlePause() : validateSourceAndId failed", m_MediaplayerType.c_str());
    promise->set_value(false);
    return;
  }
  if (player) {
    syslog(LOG_WARNING, "QTIMediaPlayer-%s : handlePause() : GetCurrentState called", m_MediaplayerType.c_str());

    auto ret = player->GetCurrentState();

    // Getting the Getstate failed return value.   (GST_STATE_CHANGE_FAILURE)
    if (std::get<1>(ret) == adk::MediaPlayer::Result::Failure) {
      syslog(LOG_INFO, "QTIMediaPlayer-%s : handlePause() : failed reason:gstElementGetStateFailed", m_MediaplayerType.c_str());
      promise->set_value(false);
      return;
    }
    /*
         * If a play() or resume() call is pending, we want to try pausing immediately to avoid blips in audio.
         */
    if (m_playPending || m_resumePending) {
      if (m_pausePending) {
        syslog(LOG_INFO, "QTIMediaPlayer-%s : handlePause() : failed reason:pauseCurrentlyPending", m_MediaplayerType.c_str());
        promise->set_value(false);
        return;
      }

      syslog(LOG_DEBUG, "QTIMediaPlayer-%s : handlePause() : Pause Called", m_MediaplayerType.c_str());
      auto res = player->Pause();

      if (res == adk::MediaPlayer::Failure) {
        syslog(LOG_ERR, "QTIMediaPlayer-%s : handlePause() : Pause failed reason:adk-MediaPlayer Failure", m_MediaplayerType.c_str());
        promise->set_value(false);
      } else {
        m_pauseImmediately = true;
        promise->set_value(true);
      }
      return;
    }

    if (std::get<0>(ret) != adk::MediaPlayer::State::Playing) {
      syslog(LOG_WARNING, "QTIMediaPlayer-%s : handlePause() : failed reason:noAudioPlaying", m_MediaplayerType.c_str());
      promise->set_value(false);
      return;
    }

    if (m_pausePending) {
      syslog(LOG_INFO, "QTIMediaPlayer-%s : handlePause() : failed reason:pauseCurrentlyPending", m_MediaplayerType.c_str());
      promise->set_value(false);
      return;
    }

    syslog(LOG_DEBUG, "QTIMediaPlayer-%s : handlePause() : Pause called", m_MediaplayerType.c_str());
    auto res = player->Pause();

    if (res == adk::MediaPlayer::Failure) {
      syslog(LOG_ERR, "QTIMediaPlayer-%s : handlePause() : Pause failed reason: adk-MediaPlayer Failure", m_MediaplayerType.c_str());
      promise->set_value(false);

    } else {
      m_pausePending = true;
      promise->set_value(true);
    }
  } else {
    syslog(LOG_ERR, "QTIMediaPlayer-%s : handlePause() : failed reason:player is nullptr", m_MediaplayerType.c_str());
    promise->set_value(false);
  }
}

void QTIMediaPlayer::handleResume(QTIMediaPlayer::SourceId id, std::promise<bool>* promise) {
  //Acquire lock for 5 seconds to setup and start playing
  int delay = WAKELOCK_AVS_MEDIAPLAYER_SETUP_TIMEOUT;
  system_wake_lock_toggle(true, m_MediaplayerType, delay);

  syslog(LOG_INFO, "QTIMediaPlayer-%s : handleResume() : idPassed=%lu and currentId=%lu", m_MediaplayerType.c_str(), id, m_currentId);
  if (!validateSourceAndId(id)) {
    syslog(LOG_ERR, "QTIMediaPlayer-%s : handleResume() : validateSourceAndId failed", m_MediaplayerType.c_str());
    promise->set_value(false);
    return;
  }
  if (player) {
    syslog(LOG_WARNING, "QTIMediaPlayer-%s : handleResume() : GetCurrentState called", m_MediaplayerType.c_str());

    auto ret = player->GetCurrentState();

    // Getting the Getstate failed return value.   (GST_STATE_CHANGE_FAILURE)
    if (std::get<1>(ret) == adk::MediaPlayer::Result::Failure) {
      syslog(LOG_INFO, "QTIMediaPlayer-%s : handleResume() : failed reason:gstElementGetStateFailed", m_MediaplayerType.c_str());
      promise->set_value(false);
      return;
    }

    if (std::get<0>(ret) == adk::MediaPlayer::State::Playing) {
      syslog(LOG_WARNING, "QTIMediaPlayer-%s : handleResume() : failed reason:alreadyPlaying", m_MediaplayerType.c_str());
      promise->set_value(false);
      return;
    }

    // Only unpause if currently paused.
    if (std::get<0>(ret) != adk::MediaPlayer::State::Paused) {
      syslog(LOG_WARNING, "QTIMediaPlayer-%s : handleResume() : failed reason:notCurrentlyPaused", m_MediaplayerType.c_str());
      promise->set_value(false);
      return;
    }

    if (m_resumePending) {
      syslog(LOG_INFO, "QTIMediaPlayer-%s : handleResume() : failed reason:resumeCurrentlyPending", m_MediaplayerType.c_str());
      promise->set_value(false);
      return;
    }

    // TODO(c_pnemag): ACSDK-1778: Verify if we need to check percent buffer here and decide whether to continue pausing to refill
    // buffer or go straight to play

    syslog(LOG_DEBUG, "QTIMediaPlayer-%s : handleResume() : play(Resume) called", m_MediaplayerType.c_str());
    auto res = player->Play();

    if (res == adk::MediaPlayer::Failure) {
      syslog(LOG_ERR, "QTIMediaPlayer-%s : handleResume() : play(Resume) failed reason:adk-MediaPlayer Failure", m_MediaplayerType.c_str());
      promise->set_value(false);

    } else {
      // if state change from pause to play success(RESUME)
      m_resumePending = true;
      m_pauseImmediately = false;
      promise->set_value(true);
    }
  } else {
    syslog(LOG_ERR, "QTIMediaPlayer-%s : handleResume() : failed reason:player is nullptr", m_MediaplayerType.c_str());
    promise->set_value(false);
  }
}

void QTIMediaPlayer::handleGetOffset(SourceId id, std::promise<std::chrono::milliseconds>* promise) {
  syslog(LOG_INFO, "QTIMediaPlayer-%s : handleGetOffset() : idPassed=%lu and currentId=%lu", m_MediaplayerType.c_str(), id, m_currentId);

  if (!validateSourceAndId(id)) {
    syslog(LOG_ERR, "QTIMediaPlayer-%s : handleGetOffset() : validateSourceAndId fail", m_MediaplayerType.c_str());
    promise->set_value(m_offsetBeforeTeardown);
    return;
  }

  promise->set_value(getCurrentStreamOffset());
}

std::chrono::milliseconds QTIMediaPlayer::getCurrentStreamOffset() {
  syslog(LOG_WARNING, "QTIMediaPlayer-%s : getCurrentStreamOffsetCalled() ", m_MediaplayerType.c_str());

  auto offsetInMilliseconds = MEDIA_PLAYER_INVALID_OFFSET;

  if (player) {
    syslog(LOG_WARNING, "QTIMediaPlayer-%s : getCurrentStreamOffsetCalled() : GetCurrentState called", m_MediaplayerType.c_str());

    auto ret = player->GetCurrentState();

    if (std::get<1>(ret) == adk::MediaPlayer::Result::Failure) {
      // Getting the state failed.   (GST_STATE_CHANGE_FAILURE)
      syslog(LOG_INFO, "QTIMediaPlayer-%s : getCurrentStreamOffsetCalled() : failed reason:gstElementGetStateFailed", m_MediaplayerType.c_str());
    } else if (std::get<1>(ret) != adk::MediaPlayer::Result::Success) {
      // Getting the state was not successful (GST_STATE_CHANGE_ASYNC or GST_STATE_CHANGE_NO_PREROLL).
      syslog(LOG_INFO, "QTIMediaPlayer-%s : getCurrentStreamOffsetCalled() : failed reason:gstElementGetStateFailed:AsyncOrNoPreRoll", m_MediaplayerType.c_str());
    } else if ((std::get<0>(ret) != adk::MediaPlayer::State::Playing) && (std::get<0>(ret) != adk::MediaPlayer::State::Paused)) {
      // Invalid State.
      syslog(LOG_ERR, "QTIMediaPlayer-%s : getCurrentStreamOffsetCalled() : failed reason:invalidPipelineState", m_MediaplayerType.c_str());
    } else {
      // Query succeeded.
      syslog(LOG_DEBUG, "QTIMediaPlayer-%s : getCurrentStreamOffsetCalled() : GetPosition called", m_MediaplayerType.c_str());
      std::chrono::nanoseconds res = player->GetPosition();

      if (res != std::chrono::nanoseconds(0)) {
        syslog(LOG_DEBUG, "QTIMediaPlayer-%s : getCurrentStreamOffsetCalled() : GetPosition success", m_MediaplayerType.c_str());
        std::chrono::milliseconds startStreamingPoint = std::chrono::milliseconds::zero();
        if (m_urlConverter) {
          startStreamingPoint = m_urlConverter->getStartStreamingPoint();
        }
        offsetInMilliseconds =
            startStreamingPoint + std::chrono::duration_cast<std::chrono::milliseconds>(res);
      } else {
        syslog(LOG_ERR, "QTIMediaPlayer-%s : getCurrentStreamOffsetCalled() : GetPosition failed reason:position is zero", m_MediaplayerType.c_str());
      }
    }
  } else {
    syslog(LOG_ERR, "QTIMediaPlayer-%s : getCurrentStreamOffsetCalled() : failed reason:player is nullptr", m_MediaplayerType.c_str());
  }

  return offsetInMilliseconds;
}

void QTIMediaPlayer::handleSetObserver(
    std::promise<void>* promise,
    std::shared_ptr<avsCommon::utils::mediaPlayer::MediaPlayerObserverInterface> observer) {
  syslog(LOG_INFO, "QTIMediaPlayer-%s : handleSetObserver() ", m_MediaplayerType.c_str());
  m_playerObserver = observer;
  promise->set_value();
}

void QTIMediaPlayer::sendPlaybackStarted() {
  int delay = WAKELOCK_NO_TIMEOUT;
  system_wake_lock_toggle(true, m_MediaplayerType, delay);

  if (!m_playbackStartedSent) {
    m_playbackStartedSent = true;
    m_playPending = false;
    if (m_playerObserver) {
      syslog(LOG_INFO, "QTIMediaPlayer-%s : Thread %x : sendPlaybackStarted() :  onPlaybackStarted called : currentId=%lu", m_MediaplayerType.c_str(), std::this_thread::get_id(), m_currentId);
      m_playerObserver->onPlaybackStarted(m_currentId);
    }
  }
}

void QTIMediaPlayer::sendPlaybackFinished() {
  system_wake_lock_toggle(false, m_MediaplayerType, WAKELOCK_NO_TIMEOUT);
  syslog(LOG_INFO, "QTIMediaPlayer-%s : sendPlaybackFinished() ", m_MediaplayerType.c_str());

  if (m_currentId == ERROR_SOURCE_ID) {
    syslog(LOG_ERR, "QTIMediaPlayer-%s: sendPlaybackFinished() : failed reason:invalidsourceid", m_MediaplayerType.c_str());
    return;
  }
  m_isPaused = false;
  m_playbackStartedSent = false;

  uint64_t temp_id = m_currentId;

  tearDownTransientPipelineElements(false);
  if (m_urlConverter) {
    m_urlConverter->shutdown();
  }
  m_urlConverter.reset();

  if (!m_playbackFinishedSent) {
    m_playbackFinishedSent = true;
    if (m_playerObserver) {
      syslog(LOG_INFO, "QTIMediaPlayer-%s : Thread %x : sendPlaybackFinished() : OnPlaybackFinished called: currentId=%lu", m_MediaplayerType.c_str(), std::this_thread::get_id(), temp_id);
      m_playerObserver->onPlaybackFinished(temp_id);
    }
  }
  m_playbackStartedSent = false;
}

void QTIMediaPlayer::sendPlaybackPaused() {
  system_wake_lock_toggle(false, m_MediaplayerType, WAKELOCK_NO_TIMEOUT);

  m_pausePending = false;
  m_pauseonBuffering = false;
  m_isPaused = true;
  if (m_playerObserver) {
    syslog(LOG_INFO, "QTIMediaPlayer-%s : Thread %x : sendPlaybackPaused() : OnPlaybackPaused called : currentId=%lu", m_MediaplayerType.c_str(), std::this_thread::get_id(), m_currentId);
    m_playerObserver->onPlaybackPaused(m_currentId);
  }
}

void QTIMediaPlayer::sendPlaybackResumed() {
  int delay = WAKELOCK_NO_TIMEOUT;
  system_wake_lock_toggle(true, m_MediaplayerType, delay);

  m_resumePending = false;
  m_isPaused = false;
  if (m_playerObserver) {
    syslog(LOG_INFO, "QTIMediaPlayer-%s : Thread %x : sendPlaybackResumed() : OnPlaybackResumed called: currentId=%lu", m_MediaplayerType.c_str(), std::this_thread::get_id(), m_currentId);
    m_playerObserver->onPlaybackResumed(m_currentId);
  }
}

void QTIMediaPlayer::sendPlaybackStopped() {
  system_wake_lock_toggle(false, m_MediaplayerType, WAKELOCK_NO_TIMEOUT);

  syslog(LOG_INFO, "QTIMediaPlayer-%s : sendPlaybackStopped() ", m_MediaplayerType.c_str());
  if (m_currentId == ERROR_SOURCE_ID) {
    return;
  }
  uint64_t temp_id = m_currentId;
  tearDownTransientPipelineElements(false);
  if (m_urlConverter) {
    m_urlConverter->shutdown();
  }
  m_urlConverter.reset();

  if (m_playerObserver) {
    syslog(LOG_INFO, "QTIMediaPlayer-%s : Thread %x : sendPlaybackStopped() : OnPlaybackStopped called: currentId=%lu", m_MediaplayerType.c_str(), std::this_thread::get_id(), temp_id);
    m_playerObserver->onPlaybackStopped(temp_id);
  }
}

void QTIMediaPlayer::sendPlaybackError(const ErrorType& type, const std::string& error) {
  system_wake_lock_toggle(false, m_MediaplayerType, WAKELOCK_NO_TIMEOUT);

  syslog(LOG_INFO, "QTIMediaPlayer-%s : sendPlaybackError() ", m_MediaplayerType.c_str());
  if (m_currentId == ERROR_SOURCE_ID) {
    return;
  }

  m_playPending = false;
  m_pausePending = false;
  m_resumePending = false;
  m_pauseImmediately = false;
  m_pauseonBuffering = false;

  uint64_t temp_id = m_currentId;

  tearDownTransientPipelineElements(false);
  if (m_urlConverter) {
    m_urlConverter->shutdown();
  }
  m_urlConverter.reset();

  if (m_playerObserver) {
    syslog(LOG_INFO, "QTIMediaPlayer-%s : Thread %x :sendPlaybackError() : OnPlaybackError called: currentId=%lu", m_MediaplayerType.c_str(), std::this_thread::get_id(), temp_id);
    m_playerObserver->onPlaybackError(temp_id, type, error);
  }
}

void QTIMediaPlayer::sendBufferUnderrun() {
  m_pauseonBuffering = false;
  if (m_playerObserver) {
    syslog(LOG_INFO, "QTIMediaPlayer-%s : Thread %x : sendBufferUnderrun() : onBufferUnderrun called : currentId=%lu", m_MediaplayerType.c_str(), std::this_thread::get_id(), m_currentId);
    m_playerObserver->onBufferUnderrun(m_currentId);
  }
}

void QTIMediaPlayer::sendBufferRefilled() {
  if (m_playerObserver) {
    syslog(LOG_INFO, "QTIMediaPlayer-%s : Thread %x : sendBufferRefilled() : onBufferRefilled called : currentId=%lu", m_MediaplayerType.c_str(), std::this_thread::get_id(), m_currentId);
    m_playerObserver->onBufferRefilled(m_currentId);
  }
}

bool QTIMediaPlayer::validateSourceAndId(SourceId id) {
  syslog(LOG_DEBUG, "QTIMediaPlayer-%s : Thread %x : validateSourceAndId() ", m_MediaplayerType.c_str(), std::this_thread::get_id());
  if (id != m_currentId) {
    //syslog(LOG_ERR, "QTIMediaPlayer-%s : validateSourceAndId() : failed reason:sourceIdMismatch", m_MediaplayerType.c_str());
    return false;
  }
  return true;
}

void QTIMediaPlayer::cleanUpSource() {
  syslog(LOG_DEBUG, "QTIMediaPlayer-%s : Thread %x : cleanUpSource()", m_MediaplayerType.c_str(), std::this_thread::get_id());

  if (player) {
    syslog(LOG_ERR, "QTIMediaPlayer-%s : cleanUpSource() : stop called", m_MediaplayerType.c_str());
    auto res = player->Stop();
    if (res == adk::MediaPlayer::Failure) {
      syslog(LOG_ERR, "QTIMediaPlayer-%s : cleanUpSource() : stop failed reason:adk-MediaPlayer Failure", m_MediaplayerType.c_str());
    }
  } else {
    syslog(LOG_ERR, "QTIMediaPlayer-%s : cleanUpSource() : failed reason:player is nullptr", m_MediaplayerType.c_str());
  }
}

//Extra adk-mediaplayer functions

std::string QTIMediaPlayer::getCapsString(const avsCommon::utils::AudioFormat& audioFormat) {
  syslog(LOG_DEBUG, "QTIMediaPlayer : getCapsString()");

  std::stringstream caps;
  switch (audioFormat.encoding) {
    case AudioFormat::Encoding::LPCM:
      caps << "audio/x-raw";
      break;
    case AudioFormat::Encoding::OPUS:
      syslog(LOG_ERR, "QTIMediaPlayer :getCapsString() :  does not handle OPUS data");
      caps << " ";
      break;
  }

  switch (audioFormat.endianness) {
    case AudioFormat::Endianness::LITTLE:
      audioFormat.dataSigned ? caps << ",format=S" << audioFormat.sampleSizeInBits << "LE"
                             : caps << ",format=U" << audioFormat.sampleSizeInBits << "LE";
      break;
    case AudioFormat::Endianness::BIG:
      audioFormat.dataSigned ? caps << ",format=S" << audioFormat.sampleSizeInBits << "BE"
                             : caps << ",format=U" << audioFormat.sampleSizeInBits << "BE";
      break;
  }

  switch (audioFormat.layout) {
    case AudioFormat::Layout::INTERLEAVED:
      caps << ",layout=interleaved";
      break;
    case AudioFormat::Layout::NON_INTERLEAVED:
      caps << ",layout=non-interleaved";
      break;
  }

  caps << ",channels=" << audioFormat.numChannels;
  caps << ",rate=" << audioFormat.sampleRateHz;

  return caps.str();
}

void QTIMediaPlayer::OnEndOfStream(void) {
  syslog(LOG_DEBUG, "QTIMediaPlayer-%s : Thread %x : OnEndOfStream() ", m_MediaplayerType.c_str(), std::this_thread::get_id());

  //TODO handle later
#if 0
  if (!m_source->handleEndOfStream()) {
      const std::string errorMessage{"reason=sourceHandleEndOfStreamFailed"};
      ACSDK_ERROR(LX("handleBusMessageFailed").m(errorMessage));
      sendPlaybackError(ErrorType::MEDIA_ERROR_INTERNAL_DEVICE_ERROR, errorMessage);
      break;
  }
#endif
  // Continue playback if there is additional data.
  if (m_IstreamRepeat) {
    m_IstreamReader->clear();
    m_IstreamReader->seekg(0);

    syslog(LOG_DEBUG, "QTIMediaPlayer-%s : OnEndOfStream() : Has additional_data : stop called", m_MediaplayerType.c_str());
    auto res = player->Stop();
    if (res == adk::MediaPlayer::Failure) {
      const std::string errorMessage{"reason=setPipelineToNullFailed"};
      syslog(LOG_DEBUG, "QTIMediaPlayer-%s : OnEndOfStream() : Has additional_data : sendPlaybackError called", m_MediaplayerType.c_str());
      sendPlaybackError(ErrorType::MEDIA_ERROR_INTERNAL_DEVICE_ERROR, errorMessage);
      return;
    }

    syslog(LOG_DEBUG, "QTIMediaPlayer-%s : OnEndOfStream() : Has additional_data : play called", m_MediaplayerType.c_str());
    auto res1 = player->Play();
    if (res1 == adk::MediaPlayer::Failure) {
      const std::string errorMessage{"reason=setPipelineToPlayingFailed"};
      syslog(LOG_DEBUG, "QTIMediaPlayer-%s : OnEndOfStream() : Has additional_data : sendPlaybackError called", m_MediaplayerType.c_str());
      sendPlaybackError(ErrorType::MEDIA_ERROR_INTERNAL_DEVICE_ERROR, errorMessage);
      return;
    }
  }
  sendPlaybackFinished();
}

void QTIMediaPlayer::OnError_Adk(adk::MediaPlayer::ErrorType error_type,
    std::string error_msg, std::string debug_msg) {
  //std::cout << "QTIMediaPlayer- " << m_MediaplayerType << " OnError_Adk error_msg= " << error_msg << std::endl;
  //std::cout << "QTIMediaPlayer- " << m_MediaplayerType << " OnError_Adk debug_msg= " << debug_msg << std::endl;

  syslog(LOG_DEBUG, "QTIMediaPlayer-%s : Thread %x : OnError_Adk() ", m_MediaplayerType.c_str(), std::this_thread::get_id());

  syslog(LOG_INFO, "QTIMediaPlayer-%s : OnError_Adk() error_type=%d", m_MediaplayerType.c_str(), error_type);
  syslog(LOG_INFO, "QTIMediaPlayer-%s : OnError_Adk() error_msg=%s", m_MediaplayerType.c_str(), error_msg.c_str());
  syslog(LOG_INFO, "QTIMediaPlayer-%s : OnError_Adk() debug_msg=%s", m_MediaplayerType.c_str(), debug_msg.c_str());

#if 0
  bool isPlaybackRemote = m_source ? m_source->isPlaybackRemote() : false;
  sendPlaybackError(gerrorToErrorType(error, isPlaybackRemote), error->message);
#endif
  if (error_type == adk::MediaPlayer::ErrorType::GENERAL) {
    syslog(LOG_DEBUG, "QTIMediaPlayer-%s : OnError_Adk : sendPlaybackError called", m_MediaplayerType.c_str());
    sendPlaybackError(ErrorType::MEDIA_ERROR_INTERNAL_DEVICE_ERROR, error_msg);
  }
}

// original
void QTIMediaPlayer::OnStateChanged(adk::MediaPlayer::State new_state) {
  adk::MediaPlayer::State current_state = new_state;

  syslog(LOG_DEBUG, "QTIMediaPlayer-%s : Thread %x : OnStateChanged() ", m_MediaplayerType.c_str(), std::this_thread::get_id());

  if (current_state == adk::MediaPlayer::State::Playing) {
    m_oldstateisplay = true;
    syslog(LOG_INFO, "QTIMediaPlayer-%s : OnStateChanged() : State=Playing ", m_MediaplayerType.c_str());

    if (!m_playbackStartedSent) {
      sendPlaybackStarted();
    } else {
      if (m_isBufferUnderrun) {
        sendBufferRefilled();
        m_isBufferUnderrun = false;
      }
      if (m_isPaused) {
        sendPlaybackResumed();
      }
    }

  } else if (current_state == adk::MediaPlayer::State::Paused) {
    syslog(LOG_INFO, "QTIMediaPlayer-%s : OnStateChanged() : State=Paused", m_MediaplayerType.c_str());
    /*
         * Pause occurred immediately after a play/resume, so it's possible that the play/resume
         * was never enacted by MediaPlayer. If there are pending play/resume at the time of the pause,
         * notify the observers that the calls were still received.
         */
    if (m_pauseImmediately) {
      if (m_playPending) {
        sendPlaybackStarted();
      } else if (m_resumePending) {
        sendPlaybackResumed();
      }
      sendPlaybackPaused();
    }
    //Handled in ADK-MP
#if 0
    else if (GST_STATE_PLAYING == pendingState) {
        // GStreamer seeks should be performed when the pipeline is in the PAUSED or PLAYING state only,
        // so just before we make our first upwards state change from PAUSED to PLAYING, we perform the
        // seek.
        if (m_offsetManager.isSeekable() && m_offsetManager.isSeekPointSet()) {
            if (!seek()) {
                std::string error = "seekFailed";
                ACSDK_ERROR(LX(error));
                sendPlaybackError(ErrorType::MEDIA_ERROR_INTERNAL_DEVICE_ERROR, error);
            };
        }
    }
#endif
    else if (m_oldstateisplay) {
      // State change from PLAYING -> PAUSED.
      if (m_isBufferUnderrun) {
        sendBufferUnderrun();
      } else if (!m_isPaused) {
        if (m_pausePending || m_pauseonBuffering) {
          sendPlaybackPaused();
        } else {
          syslog(LOG_DEBUG, "QTIMediaPlayer-%s : OnStateChanged() : State=Paused : Pause is done externally(not cloud) : Call Pause via interaction Manager", m_MediaplayerType.c_str());
          if (pauseCbIntoInteractionManager != nullptr)
            pauseCbIntoInteractionManager();
        }
      }
    }
    m_oldstateisplay = false;

  } else if (current_state == adk::MediaPlayer::State::Stopped) {
    syslog(LOG_DEBUG, "QTIMediaPlayer-%s : OnStateChanged() : State=Stopped ", m_MediaplayerType.c_str());
    sendPlaybackStopped();
  }
}

void QTIMediaPlayer::OnBuffering(unsigned int buffering_percent) {
  syslog(LOG_DEBUG, "QTIMediaPlayer-%s : Thread %x : OnBuffering() ", m_MediaplayerType.c_str(), std::this_thread::get_id());
  if (buffering_percent < 100) {
    if (player) {
      //call pause only if it is playing
      if (m_oldstateisplay) {
        syslog(LOG_DEBUG, "QTIMediaPlayer-%s : OnBuffering() : OnBufferunderrun : Pause called", m_MediaplayerType.c_str());
        auto res = player->Pause();

        if (res == adk::MediaPlayer::Failure) {
          syslog(LOG_ERR, "QTIMediaPlayer-%s : OnBuffering() : OnBufferunderrun : pause failed reason:adk-MediaPlayer Failure", m_MediaplayerType.c_str());
          std::string error = "pausingOnBufferUnderrunFailed";
          sendPlaybackError(ErrorType::MEDIA_ERROR_INTERNAL_DEVICE_ERROR, error);
          return;
        } else {
          m_pauseonBuffering = true;
        }
      }
    } else {
      syslog(LOG_ERR, "QTIMediaPlayer-%s : OnBuffering() : OnBufferunderrun : failed reason:player is nullptr", m_MediaplayerType.c_str());
      return;
    }

    // Only enter bufferUnderrun after playback has started.
    if (m_playbackStartedSent) {
      m_isBufferUnderrun = true;
    }
  } else {
    syslog(LOG_DEBUG, "QTIMediaPlayer-%s : OnBuffering() : OnBufferRefilled", m_MediaplayerType.c_str());
    if (m_pauseImmediately) {
      // To avoid starting to play if a pause() was called immediately after calling a play()
      return;
    }

    bool isSeekable = false;

    if (queryIsSeekable(&isSeekable)) {
      m_offsetManager.setIsSeekable(isSeekable);
    }

    if (m_offsetManager.isSeekable() && m_offsetManager.isSeekPointSet()) {
      syslog(LOG_DEBUG, "QTIMediaPlayer-%s : OnBuffering() : OnBufferRefilled  : seek called", m_MediaplayerType.c_str());
      seek();
    } else {
      if (player) {
        syslog(LOG_DEBUG, "QTIMediaPlayer-%s : OnBuffering() : OnBufferRefilled : Play(Resume) called", m_MediaplayerType.c_str());
        auto res = player->Play();

        if (res == adk::MediaPlayer::Failure) {
          syslog(LOG_ERR, "QTIMediaPlayer-%s : OnBuffering() : OnBufferRefilled : Play(Resume) failed reason:adk-MediaPlayer Failure", m_MediaplayerType.c_str());
          std::string error = "resumingOnBufferRefilledFailed";
          sendPlaybackError(ErrorType::MEDIA_ERROR_INTERNAL_DEVICE_ERROR, error);
        }
      } else {
        syslog(LOG_ERR, "QTIMediaPlayer-%s :  OnBuffering() : OnBufferRefilled : failed reason:player is nullptr", m_MediaplayerType.c_str());
      }
    }
  }
}

void QTIMediaPlayer::OnMetadataChanged(void) {
  if (player) {
    std::map<adk::MediaPlayer::Tag, std::string> metadata = player->GetMetadata();

    auto vectorOfTags = collectTags(metadata);

    sendStreamTagsToObserver(std::move(vectorOfTags));

  } else {
    syslog(LOG_ERR, "QTIMediaPlayer-%s : Thread %x : OnMetadataChanged() : failed reason:player is nullptr ", m_MediaplayerType.c_str(), std::this_thread::get_id());
  }
}

void QTIMediaPlayer::SetNextCb(std::function<void(void)> onNextCb) {
  if (player) {
    player->SetNextCb(onNextCb);
  }
}

void QTIMediaPlayer::SetPreviousCb(std::function<void(void)> onPreviousCb) {
  if (player) {
    player->SetPreviousCb(onPreviousCb);
  }
}

void QTIMediaPlayer::SetPlayCb(std::function<void(void)> onPlayCb) {
  if (player) {
    player->SetPlayCb(onPlayCb);
  }
}

void QTIMediaPlayer::SetPauseCb(std::function<void(void)> onPauseCb) {
  if (player) {
    pauseCbIntoInteractionManager = onPauseCb;
    player->SetPauseCb(onPauseCb);
  }
}

void QTIMediaPlayer::SetOnMuteUpdatedCb(std::function<void(bool mute_state)> onMuteUpdatedCb) {
  if (player) {
    player->SetOnMuteUpdatedCb(onMuteUpdatedCb);
  }
}

void QTIMediaPlayer::SetOnVolumeUpdatedCb(std::function<void(double volume)> onVolumeUpdatedCb) {
  if (player) {
    player->SetOnVolumeUpdatedCb(onVolumeUpdatedCb);
  }
}

void QTIMediaPlayer::sendStreamTagsToObserver(std::unique_ptr<const VectorOfTags> vectorOfTags) {
  syslog(LOG_INFO, "QTIMediaPlayer-%s : sendStreamTagsToObserver() ", m_MediaplayerType.c_str());
  if (m_playerObserver) {
    m_playerObserver->onTags(m_currentId, std::move(vectorOfTags));
  }
}

std::unique_ptr<const VectorOfTags> QTIMediaPlayer::collectTags(std::map<adk::MediaPlayer::Tag, std::string>& metadata) {
  VectorOfTags vectorOfTags;

  for (auto it : metadata) {
    MediaPlayerObserverInterface::TagKeyValueType tagKeyValueType;

    if (it.first == adk::MediaPlayer::Tag::Album) {
      tagKeyValueType.key = "album";
      tagKeyValueType.value = it.second;
      tagKeyValueType.type = MediaPlayerObserverInterface::TagType::STRING;
    } else if (it.first == adk::MediaPlayer::Tag::Artist) {
      tagKeyValueType.key = "artist";
      tagKeyValueType.value = it.second;
      tagKeyValueType.type = MediaPlayerObserverInterface::TagType::STRING;
    } else if (it.first == adk::MediaPlayer::Tag::AudioBitrate) {
      tagKeyValueType.key = "bitrate";
      tagKeyValueType.value = it.second;
      tagKeyValueType.type = MediaPlayerObserverInterface::TagType::UINT;
    } else if (it.first == adk::MediaPlayer::Tag::Genre) {
      tagKeyValueType.key = "genre";
      tagKeyValueType.value = it.second;
      tagKeyValueType.type = MediaPlayerObserverInterface::TagType::STRING;
    } else if (it.first == adk::MediaPlayer::Tag::Title) {
      tagKeyValueType.key = "title";
      tagKeyValueType.value = it.second;
      tagKeyValueType.type = MediaPlayerObserverInterface::TagType::STRING;
    } else if (it.first == adk::MediaPlayer::Tag::Url) {
      tagKeyValueType.key = "location";
      tagKeyValueType.value = it.second;
      tagKeyValueType.type = MediaPlayerObserverInterface::TagType::STRING;
    } else {
      /* Ignore Unknown tag, Ignore GST_VALUE_HOLDS_BUFFER and other types*/
      continue;
    }

    vectorOfTags.push_back(tagKeyValueType);
  }
  return make_unique<const VectorOfTags>(vectorOfTags);
}

void QTIMediaPlayer::setEqualizerBandLevels(audio::EqualizerBandLevelMap bandLevelMap) {
  //    if (!m_equalizerEnabled) {
  //        return;
  //    }
  //    std::promise<void> promise;
  //    auto future = promise.get_future();
  //    std::function<gboolean()> callback = [this, &promise, bandLevelMap]() {
  //        auto it = bandLevelMap.find(audio::EqualizerBand::BASS);
  //        if (bandLevelMap.end() != it) {
  //            g_object_set(
  //                G_OBJECT(m_pipeline.equalizer),
  //                GSTREAMER_BASS_BAND_NAME,
  //                static_cast<gdouble>(clampEqualizerLevel(it->second)),
  //                NULL);
  //        }
  //        it = bandLevelMap.find(audio::EqualizerBand::MIDRANGE);
  //        if (bandLevelMap.end() != it) {
  //            g_object_set(
  //                G_OBJECT(m_pipeline.equalizer),
  //                GSTREAMER_MIDRANGE_BAND_NAME,
  //                static_cast<gdouble>(clampEqualizerLevel(it->second)),
  //                NULL);
  //        }
  //        it = bandLevelMap.find(audio::EqualizerBand::TREBLE);
  //        if (bandLevelMap.end() != it) {
  //            g_object_set(
  //                G_OBJECT(m_pipeline.equalizer),
  //                GSTREAMER_TREBLE_BAND_NAME,
  //                static_cast<gdouble>(clampEqualizerLevel(it->second)),
  //                NULL);
  //        }
  //        promise.set_value();
  //        return false;
  //    };
  //    if (queueCallback(&callback) != UNQUEUED_CALLBACK) {
  //        future.get();
  //    }
}

int QTIMediaPlayer::getMinimumBandLevel() {
  return MIN_EQUALIZER_LEVEL;
}

int QTIMediaPlayer::getMaximumBandLevel() {
  return MAX_EQUALIZER_LEVEL;
}

////serialisation using Gobject queueCallback
void QTIMediaPlayer::workerLoop() {
  g_main_context_push_thread_default(m_workerContext);

  g_main_loop_run(m_mainLoop);

  g_main_context_pop_thread_default(m_workerContext);
}

guint QTIMediaPlayer::queueCallback(const std::function<gboolean()>* callback) {
  if (isShutdown()) {
    return UNQUEUED_CALLBACK;
  }
  auto source = g_idle_source_new();
  g_source_set_callback(
      source, reinterpret_cast<GSourceFunc>(&onCallback), const_cast<std::function<gboolean()>*>(callback), nullptr);
  auto sourceId = g_source_attach(source, m_workerContext);
  g_source_unref(source);
  return sourceId;
}

gboolean QTIMediaPlayer::onCallback(const std::function<gboolean()>* callback) {
  return (*callback)();
}

}  // namespace mediaPlayer
}  // namespace alexaClientSDK
