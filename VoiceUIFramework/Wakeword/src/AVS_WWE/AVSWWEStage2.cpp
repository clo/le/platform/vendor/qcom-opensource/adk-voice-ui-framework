/*Copyright (c) 2019, The Linux Foundation. All rights reserved.
*
*Redistribution and use in source and binary forms, with or without
*modification, are permitted provided that the following conditions are
*met:
*    * Redistributions of source code must retain the above copyright
*      notice, this list of conditions and the following disclaimer.
*    * Redistributions in binary form must reproduce the above
*      copyright notice, this list of conditions and the following
*      disclaimer in the documentation and/or other materials provided
*      with the distribution.
*    * Neither the name of The Linux Foundation nor the names of its
*      contributors may be used to endorse or promote products derived
*      from this software without specific prior written permission.
*
*THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
*WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
*MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
*ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
*BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
*BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
*WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
*OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
*IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    AVSWWEStage2.cpp
 *  @brief   Interface to handle the adaptation of voice samples to amazon WWE
 *
 *  DESCRIPTION
 *    Amazon Wakeword engine client as Stage 2 after SVA Stage1
 ***************************************************************/

#include <Wakeword/AVS_WWE/AVSWWEStage2.h>
#include <chrono>
#include <ctime>
#include <ratio>

namespace voiceUIFramework {
namespace wakeword {
namespace avs {

KeywordSessionStatus AVSWWEStage2::keyword_session_status_ = KeywordSessionStatus::IDLE;
std::vector<uint8_t> AVSWWEStage2::internal_lab_buffer_;
std::shared_ptr<WakeWordCallBackInterface> AVSWWEStage2::observer_client_ = nullptr;
std::shared_ptr<WakeWordInterface> AVSWWEStage2::wakeword_stage1_ = nullptr;
voiceUIFramework::wakeword::KwdDetectionResults AVSWWEStage2::res_;
long long AVSWWEStage2::total_words_last_interaction_ = 0;
int AVSWWEStage2::detection_count_ = 0;

AVSWWEStage2::AVSWWEStage2()
    : keyword_decoder_{nullptr}, samples_per_frame_{0}, stage2_threshold_{500}, voice_samples_{nullptr}, total_words_read_{0} {
  syslog(LOG_DEBUG, "+++AVSWWEStage2::AVSWWEStage2()");

  //Load AVS WWE threshold
  auto voice_ui_config = voiceUIFramework::voiceUIUtils::VoiceUIConfig::getInstance();
  if (voice_ui_config) {
    stage2_threshold_ = voice_ui_config->ReadWithDefault<int>(500, voiceUIUtils::Keys::k_avs_stage2_avs_wwe_confidence_);
    stage2_timeout_ = voice_ui_config->ReadWithDefault<int>(1500, voiceUIUtils::Keys::k_avs_stage2_avs_wwe_timeout_);
    sound_model_ = voice_ui_config->ReadWithDefault<std::string>("/data/voice-ui-framework/D.en-US.alexa.bin", voiceUIUtils::Keys::k_avs_wwe_sound_model_);
  } else {
    syslog(LOG_ERR, "AVSWWEStage2::AVSWWEStage2() - VoiceUIConfig not Initialized");
    stage2_threshold_ = 500;
    stage2_timeout_ = 1500;
    sound_model_ = "/data/voice-ui-framework/sm3_gmm_0703_cnn_jan08_Alexa.uim";
  }
}

AVSWWEStage2::~AVSWWEStage2() {
  syslog(LOG_DEBUG, "---AVSWWEStage2::~AVSWWEStage2()");
  PryonLiteError err = PryonLiteDecoder_Destroy(&keyword_decoder_);
}

bool AVSWWEStage2::Initialize() {
  syslog(LOG_DEBUG, "AVSWWEStage2::Initialize() - Sound model = %s / Threshold = %d / Timeout = %d", sound_model_.c_str(), stage2_threshold_, stage2_timeout_);

  PryonLiteDecoderConfig config = PryonLiteDecoderConfig_Default;
  config.detectThreshold = stage2_threshold_;

  //    if (voiceActivityDetectionCallback) {
  //  syslog(LOG_DEBUG, "AVSWWEStage2::Initialize() - userRequestedVoiceActivityDetectionEnabled");
  //  config.useVad = true;
  //  config.vadCallback = vadCallback;
  //    }

  if (sound_model_.empty()) {
    // Using the keyword model that is compiled in.
    syslog(LOG_ERR, "AVSWWEStage2::Initialize() - embeddedModelFileNotAvailable");
    return false;
  } else {
    if (!loadModelIntoMemory(&m_modelMem, sound_model_)) {
      syslog(LOG_ERR, "AVSWWEStage2::Initialize() - failedToLoadModelIntoMemory");
      return false;
    }
    config.model = m_modelMem.data();
    config.sizeofModel = m_modelMem.size();
  }
  config.resultCallback = detectionCallback;
  PryonLiteSessionInfo sessionInfo;

  // Query for the size of instance memory required by the decoder
  PryonLiteModelAttributes modelAttributes;
  PryonLiteError error = PryonLite_GetModelAttributes(config.model, config.sizeofModel, &modelAttributes);
  if (error) {
    syslog(LOG_ERR, "AVSWWEStage2::Initialize() - getModelAttributesFailed ErrorCode = %d", error);
    return false;
  }
  m_decoderMem.resize(modelAttributes.requiredDecoderMem);
  config.decoderMem = m_decoderMem.data();
  config.sizeofDecoderMem = modelAttributes.requiredDecoderMem;

  error = PryonLiteDecoder_Initialize(&config, &sessionInfo, &keyword_decoder_);

  if (error) {
    syslog(LOG_ERR, "AVSWWEStage2::Initialize() - Unable to initialize PryonLite decoder ErrorCode = %d", error);
    return false;
  }
  syslog(LOG_DEBUG, "AVSWWEStage2::Initialize() - engineVersion = %s / modelVersion = %s / requiredSamplesPerFramePerPush = %d", sessionInfo.engineAttributes.engineVersion, sessionInfo.modelAttributes.modelVersion, sessionInfo.samplesPerFrame);
  samples_per_frame_ = sessionInfo.samplesPerFrame;

  return true;
}

std::chrono::_V2::steady_clock::time_point start_sva_detected;
bool AVSWWEStage2::start(voiceUIFramework::wakeword::KwdDetectionResults res) {
  syslog(LOG_DEBUG, "AVSWWEStage2::start() KWD Detection #%d - START_KWD_Indice = %d / END_KWD_Indice = %d", res.detection_count_, res.keyword_start_index_, res.keyword_end_index_);

  //save keyword results from Stage 1
  res_ = res;

  //clear internal LAB bufer
  internal_lab_buffer_.clear();

  //initialize dump file
  voice_samples_ = fopen("/data/avs_wwe_dump", "wb");

  //Set keywordSessionStatus to processing data on Stage 2
  keyword_session_status_ = KeywordSessionStatus::PROCESSING;

  start_sva_detected = std::chrono::steady_clock::now();

  //start a thread with timeout for Stage 2
  //Process event on a separate thread
  std::thread timeout_thread = std::thread([=] {
    //log time elapsed - TO BE REMOVED LATER
    syslog(LOG_DEBUG, "@@@@@@@@@@AVSWWEStage2 - timeout_thread- Timeout for Stage 2 validation has started =%d", stage2_timeout_);
    std::this_thread::sleep_for(std::chrono::milliseconds(stage2_timeout_));
    //log time elapsed - TO BE REMOVED LATER

    if (keyword_session_status_ == KeywordSessionStatus::PROCESSING) {
      syslog(LOG_DEBUG, "AVSWWEStage2 - timeout_thread - Timeout for Stage 2 validation has expired - Keyword not found, restarting Stage 1");
      //Stop Keyword Detection Stage 1 as Stage 2 did not have a hit
      wakeword_stage1_->StopAudioCapture(VoiceUIClientID::AVS_SOLUTION, voiceUIFramework::wakeword::StopCaptureResult::STOPCAPTURE_FAIL);
    }
  });
  timeout_thread.detach();
  return true;
}

bool AVSWWEStage2::stop() {
  syslog(LOG_DEBUG, "AVSWWEStage2::stop() - keyword_session_status_ = %d", keyword_session_status_);

  if (keyword_session_status_ != KeywordSessionStatus::IDLE) {  //== KeywordSessionStatus::PROCESSING || keyword_session_status_ == KeywordSessionStatus::KEYWORD_FOUND) {
    syslog(LOG_DEBUG, "AVSWWEStage2::stop() - Set session to IDLE and close files");
    //Set keywordSessionStatus to IDLE as current usecase is done.
    keyword_session_status_ = KeywordSessionStatus::IDLE;
    //close dump files
    fclose(voice_samples_);

    total_words_last_interaction_ = total_words_read_;
  }

  return true;
}

bool AVSWWEStage2::ConfigureStage2Callbacks(std::shared_ptr<WakeWordCallBackInterface> observer_client, std::shared_ptr<WakeWordInterface> wakeword_stage1) {
  syslog(LOG_DEBUG, "AVSWWEStage2::ConfigureStage2Callbacks()");

  if (observer_client == nullptr || wakeword_stage1 == nullptr) {
    syslog(LOG_ERR, "AVSWWEStage2::ConfigureStage2Callbacks() - Failed.");
    return false;
  }
  observer_client_ = observer_client;
  wakeword_stage1_ = wakeword_stage1;
  return true;
}

bool AVSWWEStage2::loadModelIntoMemory(std::vector<char>* modelMem, const std::string& modelFilePath) {
  syslog(LOG_DEBUG, "AVSWWEStage2::loadModelIntoMemory() - Sound model = %s ", modelFilePath.c_str());
  if (!modelMem) {
    syslog(LOG_ERR, "AVSWWEStage2::loadModelIntoMemory() - loadModelIntoMemoryFailed - nullInMemoryModel");
    return false;
  }
  std::ifstream inFile(modelFilePath, std::ios::binary | std::ios::ate);
  if (!inFile.good()) {
    syslog(LOG_ERR, "AVSWWEStage2::loadModelIntoMemory() - loadModelIntoMemoryFailed - failedToCreateFileStreamWithGivenFilePath = %s", modelFilePath.c_str());
    return false;
  }
  auto size = inFile.tellg();
  inFile.seekg(0, std::ios::beg);
  modelMem->resize(size);
  if (!inFile.read(modelMem->data(), modelMem->size())) {
    syslog(LOG_ERR, "AVSWWEStage2::loadModelIntoMemory() - failedToReadFromFile");
    return false;
  }
  return true;
}

void AVSWWEStage2::SendAudioToClient(void* buf, const size_t bytes) {
  syslog(LOG_DEBUG, "+++ AVSWWEStage2::SendAudioToClient() - Thread %x - Bytes size = %d", std::this_thread::get_id(), bytes);

  //syslog(LOG_DEBUG, "AVSWWEStage2::SendAudioToClient() - Bytes size = %d", bytes);
  // Send audio data to VoiceUIClient
  observer_client_->OnNewVoiceBufferReceived(buf, bytes);

  syslog(LOG_DEBUG, "--- AVSWWEStage2::SendAudioToClient() - Thread %x", std::this_thread::get_id());
}

void AVSWWEStage2::PumdAudio(void* buf, const size_t bytes) {
  // syslog(LOG_DEBUG, "AVSWWEStage2::PumdAudio() - Bytes size = %d", size);

  syslog(LOG_DEBUG, "AVSWWEStage2::PumdAudio() - Thread %x - Bytes size = %d", std::this_thread::get_id(), bytes);

  //if not IDLE save AVS WWE dump on a file
  if (keyword_session_status_ != KeywordSessionStatus::IDLE) {
    fwrite(buf, 1, bytes, voice_samples_);
    fflush(voice_samples_);
  }

  //pump audio to AVS WWE engine looking for keyword.
  if (keyword_session_status_ == KeywordSessionStatus::PROCESSING) {
    int total_size_short = bytes / 2;
    int samples_to_read_short = total_size_short;
    int num = 0;

    //Save data on internal lab buffer
    internal_lab_buffer_.insert(internal_lab_buffer_.end(), (uint8_t*)buf, (uint8_t*)buf + bytes);

    syslog(LOG_DEBUG, "AVSWWEStage2::PumdAudio() - total_size_short = %d / samples_short_to_read = %d", total_size_short, samples_to_read_short);

    while (samples_to_read_short > 0) {
      int size_to_read = std::min(samples_per_frame_, samples_to_read_short);
      PryonLiteError err = PryonLiteDecoder_PushAudioSamples(keyword_decoder_, (short*)buf + (total_size_short - samples_to_read_short), size_to_read);
      //update total Word read
      total_words_read_ += size_to_read;

      if (err != PRYON_LITE_ERROR_OK) {
        syslog(LOG_ERR, "AVSWWEStage2::PumdAudio() - ERROR !!! ");
      }
      samples_to_read_short -= size_to_read;
      num++;
    }
  }  //keyword found on AVS WWE, so send voice buffers direct to cloud
  else if (keyword_session_status_ == KeywordSessionStatus::KEYWORD_FOUND) {
    SendAudioToClient(buf, bytes);
  } else {
    syslog(LOG_ERR, "AVSWWEStage2::PumdAudio() - Invalid KeywordSessionState. State = %d", keyword_session_status_);
  }
}

// keyword detection callback
void AVSWWEStage2::detectionCallback(PryonLiteDecoderHandle handle, const PryonLiteResult* result) {
  syslog(LOG_DEBUG, "AVSWWEStage2::detectionCallback() - Thread %x", std::this_thread::get_id());

  detection_count_++;

  syslog(LOG_DEBUG, "AVSWWEStage2::detectionCallback()- Detected keyword %s - Detection counter (SVA)=%d / Detection counter (AVS WWE)=%d", result->keyword, res_.detection_count_, detection_count_);
  syslog(LOG_DEBUG, "AVSWWEStage2::detectionCallback()- SVA INFO: START_KWD_Indice = %d / END_KWD_Indice = %d / Confidence = %d", res_.keyword_start_index_, res_.keyword_end_index_, res_.confidence_level_);
  syslog(LOG_DEBUG, "AVSWWEStage2::detectionCallback()- WWE INFO: total_words_last_interaction_ = %lld / START_KWD_Indice = %lld / END_KWD_Indice = %lld / Confidence = %d", total_words_last_interaction_, (result->beginSampleIndex - total_words_last_interaction_), (result->endSampleIndex - total_words_last_interaction_), result->confidence);

  //Set keywordSessionStatus to KEYWORD_FOUND.
  keyword_session_status_ = KeywordSessionStatus::KEYWORD_FOUND;

  //TODO: VERIFY if ALEXA_STOP should be a use case
  //get keyword detected by AVS WWE
  res_.keyword = result->keyword;
  //update keyword indices
  res_.keyword_start_index_ = (result->beginSampleIndex - total_words_last_interaction_) * 2;  // Word Size of stream = 2
  res_.keyword_end_index_ = (result->endSampleIndex - total_words_last_interaction_) * 2;      // Word Size of stream = 2

  auto startOfSpeechTimestamp2 = std::chrono::steady_clock::now();
  // Signal Keyword detection to the clients
  if (observer_client_->OnKeywordDetected(res_) == WakewordCallbackRc::WW_CB_ERR) {
    syslog(LOG_ERR, "AVSWWEStage2::detectionCallback() - Use case could not be initiated on AVS cloud - CANCELLING USE CASE");
    //Set keywordSessionStatus to IDLE as current usecase is done.
    keyword_session_status_ = KeywordSessionStatus::REJECTED_BY_CLOUD;
  } else {
    //log time elapsed - TO BE REMOVED LATER
    auto startOfSpeechTimestamp3 = std::chrono::steady_clock::now();
    double elaspedTimeMs = std::chrono::duration<double, std::milli>(startOfSpeechTimestamp3 - startOfSpeechTimestamp2).count();
    syslog(LOG_DEBUG, "@@@@@@@@@@ AVSWWEStage2::detectionCallback() - TIME (avs notification) in MS =%lf", elaspedTimeMs);
    double elaspedTotalTimeMs = std::chrono::duration<double, std::milli>(startOfSpeechTimestamp3 - start_sva_detected).count();
    syslog(LOG_DEBUG, "@@@@@@@@@@ AVSWWEStage2::detectionCallback() - TOTAL TIME (validate keyword) in MS =%lf", elaspedTotalTimeMs);
    //log time elapsed - TO BE REMOVED LATER

    //send internal LAB buffer to client.
    SendAudioToClient(internal_lab_buffer_.data(), internal_lab_buffer_.size());
  }
}

// VAD event callback
void AVSWWEStage2::vadCallback(PryonLiteDecoderHandle handle, const PryonLiteVadEvent* vadEvent) {
  syslog(LOG_DEBUG, "AVSWWEStage2::vadCallback() - VAD state %d", (int)vadEvent->vadState);
}

}  // namespace avs
}  // namespace wakeword
}  // namespace voiceUIFramework
