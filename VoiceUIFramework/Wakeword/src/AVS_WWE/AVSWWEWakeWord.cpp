/*Copyright (c) 2019-2020, The Linux Foundation. All rights reserved.
*
*Redistribution and use in source and binary forms, with or without
*modification, are permitted provided that the following conditions are
*met:
*    * Redistributions of source code must retain the above copyright
*      notice, this list of conditions and the following disclaimer.
*    * Redistributions in binary form must reproduce the above
*      copyright notice, this list of conditions and the following
*      disclaimer in the documentation and/or other materials provided
*      with the distribution.
*    * Neither the name of The Linux Foundation nor the names of its
*      contributors may be used to endorse or promote products derived
*      from this software without specific prior written permission.
*
*THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
*WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
*MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
*ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
*BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
*BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
*WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
*OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
*IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    AVSWWEWakeWord.cpp
 *  @brief   Interface to handle the adaptation of voice samples to amazon WWE
 *
 *  DESCRIPTION
 *    Amazon Wakeword engine used as only stage.
 ***************************************************************/

#include <Wakeword/AVS_WWE/AVSWWEWakeWord.h>

namespace voiceUIFramework {
namespace wakeword {
namespace avs {

//Initialization
WWEKeywordSessionStatus AVSWWEWakeWord::keyword_session_status_ = WWEKeywordSessionStatus::IDLE;
int AVSWWEWakeWord::detection_count_ = 0;
std::shared_ptr<WakeWordCallBackInterface> AVSWWEWakeWord::audio_capture_callback_ = nullptr;
FILE* AVSWWEWakeWord::voice_samples_ = nullptr;
std::vector<uint8_t> AVSWWEWakeWord::internal_buffer_(BUFF_SIZE, 0);
std::vector<uint8_t>::iterator AVSWWEWakeWord::iter_buffer = internal_buffer_.begin();

//DSPC output channels used for AVS WWE
#define DSPC_WWE_CHANNEL_1 1
#define DSPC_ASR_CHANNEL_2 2

AVSWWEWakeWord::AVSWWEWakeWord()
    : keyword_hit_session_{false}, enable_keyword_recognition_{false}, keyword_decoder_{nullptr}, samples_per_frame_{0}, total_words_read_{0} {
  syslog(LOG_DEBUG, "+++AVSWWEWakeWord::AVSWWEWakeWord()");

  //Load AVS WWE threshold
  auto voice_ui_config = voiceUIFramework::voiceUIUtils::VoiceUIConfig::getInstance();
  if (voice_ui_config) {
    wwe_threshold_ = voice_ui_config->ReadWithDefault<int>(500, voiceUIUtils::Keys::k_avs_stage2_avs_wwe_confidence_);
    sound_model_ = voice_ui_config->ReadWithDefault<std::string>("/data/voice-ui-framework/D.en-US.alexa.bin", voiceUIUtils::Keys::k_avs_wwe_sound_model_);
  } else {
    syslog(LOG_ERR, "AVSWWEWakeWord::AVSWWEWakeWord() - VoiceUIConfig not Initialized");
    wwe_threshold_ = 500;
    sound_model_ = "/data/voice-ui-framework/D.en-US.alexa.bin";
  }
  syslog(LOG_DEBUG, "AVSWWEWakeWord::AVSWWEWakeWord() - Sound model = %s / Threshold = %d", sound_model_.c_str(), wwe_threshold_);
}

AVSWWEWakeWord::~AVSWWEWakeWord() {
  syslog(LOG_DEBUG, "---AVSWWEWakeWord::~AVSWWEWakeWord()");
}

bool AVSWWEWakeWord::AddObserver(VoiceUIClientID client_id, std::shared_ptr<WakeWordCallBackInterface> observer) {
  int index = ClientIDToIndex(client_id);

  syslog(LOG_INFO, "Thread %x    ++AVSWWEWakeWord::AddObserver - Client: %s",
      std::this_thread::get_id(), ClientIDToString(client_id).c_str());

  if (nullptr == observer) {
    syslog(LOG_ERR, "AVSWWEWakeWord::AddObserver - No Observer object",
        ClientIDToString(client_id).c_str());
    return false;
  }

  //Save observer to send data once keyword is detected
  audio_capture_callback_ = observer;

  //Reset Internal Buffer for WWE
  std::fill(internal_buffer_.begin(), internal_buffer_.end(), 0);

  PryonLiteDecoderConfig config = PryonLiteDecoderConfig_Default;
  config.detectThreshold = wwe_threshold_;

  //    if (voiceActivityDetectionCallback) {
  //  syslog(LOG_DEBUG, "AVSWWEWakeWord::AddObserver() - userRequestedVoiceActivityDetectionEnabled");
  //  config.useVad = true;
  //  config.vadCallback = vadCallback;
  //    }

  if (sound_model_.empty()) {
    // Using the keyword model that is compiled in.
    syslog(LOG_ERR, "AVSWWEWakeWord::AddObserver() - embeddedModelFileNotAvailable");
    return false;
  } else {
    if (!loadModelIntoMemory(&m_modelMem_, sound_model_)) {
      syslog(LOG_ERR, "AVSWWEWakeWord::AddObserver() - failedToLoadModelIntoMemory");
      return false;
    }
    config.model = m_modelMem_.data();
    config.sizeofModel = m_modelMem_.size();
  }
  config.resultCallback = detectionCallback;
  PryonLiteSessionInfo sessionInfo;

  // Query for the size of instance memory required by the decoder
  PryonLiteModelAttributes modelAttributes;
  PryonLiteError error = PryonLite_GetModelAttributes(config.model, config.sizeofModel, &modelAttributes);
  if (error) {
    syslog(LOG_ERR, "AVSWWEWakeWord::AddObserver() - getModelAttributesFailed ErrorCode = %d", error);
    return false;
  }
  m_decoderMem_.resize(modelAttributes.requiredDecoderMem);
  config.decoderMem = m_decoderMem_.data();
  config.sizeofDecoderMem = modelAttributes.requiredDecoderMem;

  error = PryonLiteDecoder_Initialize(&config, &sessionInfo, &keyword_decoder_);

  if (error) {
    syslog(LOG_ERR, "AVSWWEWakeWord::AddObserver() - Unable to initialize PryonLite decoder ErrorCode = %d", error);
    return false;
  }
  syslog(LOG_DEBUG, "AVSWWEWakeWord::AddObserver() - engineVersion = %s / modelVersion = %s / requiredSamplesPerFramePerPush = %d", sessionInfo.engineAttributes.engineVersion, sessionInfo.modelAttributes.modelVersion, sessionInfo.samplesPerFrame);
  samples_per_frame_ = sessionInfo.samplesPerFrame;

  //notify Observer that AddObserver is complete.
  observer->ObserverAdded();

  return true;
}

int AVSWWEWakeWord::StartRecognition(VoiceUIClientID client_id) {
  syslog(LOG_DEBUG, "AVSWWEWakeWord::StartRecognition() - keyword_session_status_ = %d", keyword_session_status_);

  //check if client is has been shutdown
  if (keyword_session_status_ == WWEKeywordSessionStatus::SHUTDOWN) {
    syslog(LOG_ERR, "AVSWWEWakeWord::StartRecognition() - Client was already shutdown");
    return -1;
  }

  //clear internal LAB buffer
  std::fill(internal_buffer_.begin(), internal_buffer_.end(), 0);

  //reset iterator
  iter_buffer = internal_buffer_.begin();

  //Set WWEKeywordSessionStatus to processing data on Stage 2
  keyword_session_status_ = WWEKeywordSessionStatus::PROCESSING;

  return 0;
}

int AVSWWEWakeWord::StartRecognition() {
  syslog(LOG_DEBUG, "AVSWWEWakeWord::StartRecognition() - All clients");
  return StartRecognition(VoiceUIClientID::AVS_SOLUTION);
}

//keyword detected notification
void AVSWWEWakeWord::KeywordRecognized() {
  syslog(LOG_DEBUG, "AVSWWEWakeWord::KeywordRecognized() - Not implemented.");
}

int AVSWWEWakeWord::StopRecognition(VoiceUIClientID client_id) {
  syslog(LOG_DEBUG, "AVSWWEWakeWord::StopRecognition() - keyword_session_status_ = %d", keyword_session_status_);

  //check if client is has been shutdown
  if (keyword_session_status_ == WWEKeywordSessionStatus::SHUTDOWN) {
    syslog(LOG_ERR, "AVSWWEWakeWord::StopRecognition() - Client was already shutdown");
    return -1;
  }
  if (keyword_session_status_ != WWEKeywordSessionStatus::IDLE) {
    syslog(LOG_DEBUG, "AVSWWEWakeWord::StopRecognition() - Set session to IDLE and close files");
    //Set WWEKeywordSessionStatus to IDLE as current usecase is done.
    keyword_session_status_ = WWEKeywordSessionStatus::IDLE;
    //close dump files
    if (voice_samples_ != nullptr) {
      fclose(voice_samples_);
      voice_samples_ = nullptr;
    }
  }
  syslog(LOG_DEBUG, "AVSWWEWakeWord::StopRecognition() - keyword_session_status_ = %d - DONE", keyword_session_status_);
  return 0;
}
int AVSWWEWakeWord::StopRecognition() {
  syslog(LOG_DEBUG, "AVSWWEWakeWord::StopRecognition() - All clients");
  return StopRecognition(VoiceUIClientID::AVS_SOLUTION);
}

bool AVSWWEWakeWord::Feed(const std::vector<int> in) {
  //  syslog(LOG_DEBUG, "AVSWWEWakeWord::Feed() - Thread %x - Input Package Size = %d", std::this_thread::get_id(), in.size());

  bool key_word_hit = false;

  //pointer to audio utils to demux signal received from DSPC
  auto thirdparty_audio_util =
      voiceUIFramework::voiceUIUtils::ThirdPartyAudioUtils::getInstance();

  //syslog(LOG_DEBUG, "AVSWWEWakeWord::Feed() - Thread %x - WWE Package Size = %d", std::this_thread::get_id(), dspc_output_1_audio_channel.size());

  if (keyword_decoder_ == nullptr) {
    syslog(LOG_ERR, "AVSWWEWakeWord::Feed() - AVS WWE not initialized properly");
    return false;
  }

  //pump audio to AVS WWE engine looking for keyword.
  if (keyword_session_status_ == WWEKeywordSessionStatus::PROCESSING) {
    //Extract only 1st channel from DSPC output and feed it to AVS WWE
    std::vector<uint8_t> dspc_output_1_audio_channel;
    if (thirdparty_audio_util) {
      dspc_output_1_audio_channel =
          thirdparty_audio_util->Demux16khz16bitpcm(in,
              DSPC_WWE_CHANNEL_1);
    }

    int total_size_short = dspc_output_1_audio_channel.size() / 2;
    int remaining_samples_to_read_short = total_size_short;

    int buffer_position_tmp = std::distance(internal_buffer_.begin(), iter_buffer);

    //Feed WWE Engine respecting FEED size required per frame by WWE (samples_per_frame_)
    while (remaining_samples_to_read_short > 0) {
      int size_to_read_short = std::min(samples_per_frame_, remaining_samples_to_read_short);
      int begin_copy_index_short = total_size_short - remaining_samples_to_read_short;
      PryonLiteError err = PryonLiteDecoder_PushAudioSamples(keyword_decoder_, (short*)(&dspc_output_1_audio_channel[0] + (begin_copy_index_short)), size_to_read_short);

      int buffer_position = std::distance(internal_buffer_.begin(), iter_buffer);

      //Verify if internal_buffer has overflow
      if ((buffer_position + size_to_read_short * 2) > BUFF_SIZE) {
        //copy first part of input bytes until buffer is full
        int samples_to_fill_buffer = BUFF_SIZE - buffer_position;
        std::copy_n(dspc_output_1_audio_channel.begin() + begin_copy_index_short, samples_to_fill_buffer, iter_buffer);

        //reset buffer position
        iter_buffer = internal_buffer_.begin();
        //syslog(LOG_DEBUG, "AVSWWEWakeWord::Feed() - Internal buffer has been reseted");
        int buffer_position_new = std::distance(internal_buffer_.begin(), iter_buffer);

        //copy seconds part of input bytes starting on begin of internal buffer
        std::copy_n(dspc_output_1_audio_channel.begin() + begin_copy_index_short * 2 + samples_to_fill_buffer, size_to_read_short * 2 - samples_to_fill_buffer, iter_buffer);
        std::advance(iter_buffer, size_to_read_short * 2 - samples_to_fill_buffer);
      } else {
        //copy input bytes to internal buffer
        std::copy_n(dspc_output_1_audio_channel.begin() + begin_copy_index_short * 2, size_to_read_short * 2, iter_buffer);
        std::advance(iter_buffer, size_to_read_short * 2);
        int buffer_position_new_2 = std::distance(internal_buffer_.begin(), iter_buffer);
      }
      //Adjust remaining samples to be read
      remaining_samples_to_read_short -= size_to_read_short;

      if (err != PRYON_LITE_ERROR_OK) {
        syslog(LOG_ERR, "AVSWWEWakeWord::PumdAudio() - ERROR !!! - Check if # of bytes feed to WWe matches specified on WWE sound model ");
      }
    }
  }  //keyword found on AVS WWE, from now on send voice buffers direct to cloud as use case has already been initiated
  else if (keyword_session_status_ == WWEKeywordSessionStatus::KEYWORD_FOUND) {
    key_word_hit = true;

    //Extract only 2st channel from DSPC output (ASR data) and feed to Client
    std::vector<uint8_t> dspc_output_2_audio_channel;
    if (thirdparty_audio_util) {
      dspc_output_2_audio_channel =
          thirdparty_audio_util->Demux16khz16bitpcm(in, DSPC_ASR_CHANNEL_2);
    }

    //Send data to AVS Cloud
    audio_capture_callback_->OnNewVoiceBufferReceived((uint8_t*)&dspc_output_2_audio_channel[0], dspc_output_2_audio_channel.size());
  } else {
    syslog(LOG_ERR, "AVSWWEWakeWord::PumdAudio() - Invalid KeywordSessionState. State = %d", keyword_session_status_);
  }
  return key_word_hit;
}

//Stops audio capture (LAB) - use case is done.
void AVSWWEWakeWord::StopAudioCapture(
    VoiceUIClientID client_id,
    StopCaptureResult stop_capture) {
  syslog(LOG_DEBUG, "Thread %x    ++AVSWWEWakeWord::StopAudioCapture()",
      std::this_thread::get_id());
  syslog(LOG_INFO, "AVSWWEWakeWord::StopAudioCapture - Client: %s",
      ClientIDToString(client_id).c_str());

  switch (stop_capture) {
    case StopCaptureResult::STOPCAPTURE_OK: {
      //if (keyword_session_status_ != WWEKeywordSessionStatus::IDLE) {
      syslog(LOG_DEBUG, "AVSWWEWakeWord::StopRecognition() - Set session to IDLE and close files");
      //Set WWEKeywordSessionStatus to IDLE as current usecase is done.
      keyword_session_status_ = WWEKeywordSessionStatus::PROCESSING;

      //close dump files
      if (voice_samples_ != nullptr) {
        fclose(voice_samples_);
        voice_samples_ = nullptr;
      }
      // }
      break;
    }
    default:
      syslog(LOG_INFO,
          "AVSWWEWakeWord::StopAudioCapture StopCapture error received highly unlikely but lets get the error for now. ERROR: %s",
          StopCaptureResultToString(stop_capture));
  }
}

// Remove observer
bool AVSWWEWakeWord::RemoveObserver(VoiceUIClientID client) {
  syslog(LOG_DEBUG, "AVSWWEWakeWord::RemoveObserver()");
  //AVS WWE client
  keyword_session_status_ = WWEKeywordSessionStatus::SHUTDOWN;

  PryonLiteError status = PryonLiteDecoder_Destroy(&keyword_decoder_);
  if (status != PRYON_LITE_ERROR_OK) {
    syslog(LOG_DEBUG, "AVSWWEWakeWord::RemoveObserver() - Error to destroy WWE client ");
    return false;
  }

  return true;
}

//Gets direction of arrival notification
int AVSWWEWakeWord::GetDOADirection(VoiceUIClientID client) {
  syslog(LOG_DEBUG, "AVSWWEWakeWord::GetDOADirection() - Not implemented.");
  return 0;
}

//Gets Channel Index to be used during multi-turn conversation using Audio Recorder interface.
int AVSWWEWakeWord::GetChannelIndex(VoiceUIClientID client) {
  syslog(LOG_DEBUG, "AVSWWEWakeWord::GetDOADirection() - Not implemented.");
  return 0;
}

// keyword detection callback
void AVSWWEWakeWord::detectionCallback(PryonLiteDecoderHandle handle, const PryonLiteResult* result) {
  syslog(LOG_DEBUG, "AVSWWEWakeWord::detectionCallback() - Thread %x", std::this_thread::get_id());

  keyword_session_status_ = WWEKeywordSessionStatus::IDLE;

  detection_count_++;

  syslog(LOG_DEBUG, "AVSWWEWakeWord::detectionCallback()- Detected keyword %s - Detection counter= %d", result->keyword, detection_count_);
  syslog(LOG_DEBUG, "AVSWWEWakeWord::detectionCallback()- WWE INFO:  / START_KWD_Indice = %lld / END_KWD_Indice = %lld / Confidence = %d", (result->beginSampleIndex), (result->endSampleIndex), result->confidence);

  //initialize log file and store voice dump
  if (voice_samples_ != nullptr) {
    fclose(voice_samples_);
    voice_samples_ = nullptr;
  }
  voice_samples_ = fopen("/data/avs_wwe_dump", "wb");

  voiceUIFramework::wakeword::KwdDetectionResults res;

  //get keyword detected by AVS WWE
  res.keyword = result->keyword;
  res.detection_count_ = detection_count_;
  res.confidence_level_ = result->confidence;
  //update keyword indices - Start-index is equal to PREROOLL and End_index is WWE endSampleIndex - start Sample Index
  res.keyword_start_index_ = PREROOL;
  res.keyword_end_index_ = res.keyword_start_index_ + (result->endSampleIndex - result->beginSampleIndex) * 2;  // Word Size of stream = 2
  syslog(LOG_DEBUG, "AVSWWEWakeWord::detectionCallback()- AVS Keyword Indices:  / START_KWD_Indice = %d / END_KWD_Indice = %d", res.keyword_start_index_, res.keyword_end_index_);

  auto startOfSpeechTimestamp2 = std::chrono::steady_clock::now();
  // Signal Keyword detection to client and process client response
  if (audio_capture_callback_->OnKeywordDetected(res) == WakewordCallbackRc::WW_CB_ERR) {
    syslog(LOG_ERR, "AVSWWEWakeWord::detectionCallback() - Use case could not be initiated on AVS cloud - CANCELLING USE CASE");
    //Set WWEKeywordSessionStatus to IDLE as current usecase is done.
    keyword_session_status_ = WWEKeywordSessionStatus::PROCESSING;
  } else {
    //Set WWEKeywordSessionStatus to KEYWORD_FOUND.
    keyword_session_status_ = WWEKeywordSessionStatus::KEYWORD_FOUND;
    syslog(LOG_DEBUG, "AVSWWEWakeWord::detectionCallback() - WWEKeywordSessionStatus = KEYWORD_FOUND -internal_buffer_.size() = %d ", internal_buffer_.size());

    //adjust start index considering that AVS wants a 500ms prerool data sent to cloud before keyword.
    int start_buffer_pos = (((result->beginSampleIndex) * 2) % BUFF_SIZE) - PREROOL;  // Word Size of stream = 2
    int final_buffer_pos = ((result->endSampleIndex) * 2) % BUFF_SIZE;                // Word Size of stream = 2

    syslog(LOG_DEBUG, "AVSWWEWakeWord::detectionCallback() - WWEKeywordSessionStatus - internal_buffer_START_pos = %d, internal_buffer_FINAL_pos = %d", start_buffer_pos, final_buffer_pos);

    //handles preroll start index less then startIndex (buffer overflow)
    if (start_buffer_pos < 0) {
      start_buffer_pos = BUFF_SIZE + start_buffer_pos;  // start_buffer_pos is negative
    }
    syslog(LOG_DEBUG, "AVSWWEWakeWord::detectionCallback() - WWEKeywordSessionStatus - After considering PREROOL: internal_buffer_START_pos = %d, internal_buffer_FINAL_pos = %d", start_buffer_pos, final_buffer_pos);

    if (final_buffer_pos < start_buffer_pos) {
      fwrite(&internal_buffer_[start_buffer_pos], 1, BUFF_SIZE - start_buffer_pos, voice_samples_);
      fwrite(&internal_buffer_[0], 1, final_buffer_pos, voice_samples_);
      //send internal circular buffer to client.
      audio_capture_callback_->OnNewVoiceBufferReceived(&internal_buffer_[start_buffer_pos], BUFF_SIZE - start_buffer_pos);
      audio_capture_callback_->OnNewVoiceBufferReceived(&internal_buffer_[0], final_buffer_pos);
    } else {
      fwrite(&internal_buffer_[start_buffer_pos], 1, final_buffer_pos - start_buffer_pos, voice_samples_);
      //send internal circular buffer to client.
      audio_capture_callback_->OnNewVoiceBufferReceived(&internal_buffer_[start_buffer_pos], final_buffer_pos - start_buffer_pos);
    }
  }
}

// VAD event callback
void AVSWWEWakeWord::vadCallback(PryonLiteDecoderHandle handle, const PryonLiteVadEvent* vadEvent) {
  syslog(LOG_DEBUG, "AVSWWEWakeWord::vadCallback() - VAD state %d", (int)vadEvent->vadState);
}

bool AVSWWEWakeWord::loadModelIntoMemory(std::vector<char>* modelMem, const std::string& modelFilePath) {
  syslog(LOG_DEBUG, "AVSWWEWakeWord::loadModelIntoMemory() - Sound model = %s ", modelFilePath.c_str());
  if (!modelMem) {
    syslog(LOG_ERR, "AVSWWEWakeWord::loadModelIntoMemory() - loadModelIntoMemoryFailed - nullInMemoryModel");
    return false;
  }
  std::ifstream inFile(modelFilePath, std::ios::binary | std::ios::ate);
  if (!inFile.good()) {
    syslog(LOG_ERR, "AVSWWEWakeWord::loadModelIntoMemory() - loadModelIntoMemoryFailed - failedToCreateFileStreamWithGivenFilePath = %s", modelFilePath.c_str());
    return false;
  }
  auto size = inFile.tellg();
  inFile.seekg(0, std::ios::beg);
  modelMem->resize(size);
  if (!inFile.read(modelMem->data(), modelMem->size())) {
    syslog(LOG_ERR, "AVSWWEWakeWord::loadModelIntoMemory() - failedToReadFromFile");
    return false;
  }
  return true;
}

}  // namespace avs
}  // namespace wakeword
}  // namespace voiceUIFramework
