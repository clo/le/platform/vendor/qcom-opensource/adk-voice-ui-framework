/* Copyright (c) 2018, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    WakeWordUtils.cpp
 *  @brief   
 *
 *  DESCRIPTION
 *
 ***************************************************************/
#include <Wakeword/WakeWordUtils.h>
#include <errno.h>
#include <stdlib.h>
#include <syslog.h>

#define MAX_SET_PARAM_KEYS 4

/* set/get parameter keys */
#define PA_QST_PARAMETER_CUSTOM_CHANNEL_MIXING "st_custom_channel_mixing"
#define PA_QST_PARAMETER_SESSION_PAUSE "st_session_pause"
#define PA_QST_PARAMETER_BAD_MIC_CHANNEL_INDEX "st_bad_mic_channel_index"
#define PA_QST_PARAMETER_EC_REF_DEVICE "st_ec_ref_device"

static const char *set_param_key_array[] = {
    PA_QST_PARAMETER_CUSTOM_CHANNEL_MIXING,
    PA_QST_PARAMETER_SESSION_PAUSE,
    PA_QST_PARAMETER_BAD_MIC_CHANNEL_INDEX,
    PA_QST_PARAMETER_EC_REF_DEVICE};

/* SVA vendor uuid */
const sound_trigger_uuid_t qc_uuid =
    {0x68ab2d40, 0xe860, 0x11e3, 0x95ef, {0x00, 0x02, 0xa5, 0xd5, 0xc5, 0x1b}};

int string_to_uuid(const char *str, sound_trigger_uuid_t *uuid) {
  int tmp[10];

  if (str == NULL || uuid == NULL) {
    return -EINVAL;
  }

  if (sscanf(str, "%08x-%04x-%04x-%04x-%02x%02x%02x%02x%02x%02x",
          tmp, tmp + 1, tmp + 2, tmp + 3, tmp + 4, tmp + 5, tmp + 6,
          tmp + 7, tmp + 8, tmp + 9)
      < 10) {
    return -EINVAL;
  }
  uuid->timeLow = (uint32_t)tmp[0];
  uuid->timeMid = (uint16_t)tmp[1];
  uuid->timeHiAndVersion = (uint16_t)tmp[2];
  uuid->clockSeq = (uint16_t)tmp[3];
  uuid->node[0] = (uint8_t)tmp[4];
  uuid->node[1] = (uint8_t)tmp[5];
  uuid->node[2] = (uint8_t)tmp[6];
  uuid->node[3] = (uint8_t)tmp[7];
  uuid->node[4] = (uint8_t)tmp[8];
  uuid->node[5] = (uint8_t)tmp[9];

  return 0;
}

const char *handle_set_parameters(const char *kv_pair) {
  char *id, *test_r, *temp_str;
  int i;
  syslog(LOG_INFO, "Received key value pair: %s\n", kv_pair);
  temp_str = strdup(kv_pair);

  id = strtok_r(temp_str, "= ", &test_r);
  if (!id) {
    syslog(LOG_INFO, "%s: incorrect key value pair", __func__);
    return NULL;
  }

  for (i = 0; i < MAX_SET_PARAM_KEYS; i++) {
    if (!strncmp(id, set_param_key_array[i], strlen(set_param_key_array[i])))
      return temp_str;
  }
  return NULL;
}
