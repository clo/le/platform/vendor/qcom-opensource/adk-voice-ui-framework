/*Copyright (c) 2019-2020, The Linux Foundation. All rights reserved.
*
*Redistribution and use in source and binary forms, with or without
*modification, are permitted provided that the following conditions are
*met:
*    * Redistributions of source code must retain the above copyright
*      notice, this list of conditions and the following disclaimer.
*    * Redistributions in binary form must reproduce the above
*      copyright notice, this list of conditions and the following
*      disclaimer in the documentation and/or other materials provided
*      with the distribution.
*    * Neither the name of The Linux Foundation nor the names of its
*      contributors may be used to endorse or promote products derived
*      from this software without specific prior written permission.
*
*THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
*WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
*MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
*ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
*BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
*BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
*WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
*OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
*IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    SensoryWakewordEngine.cpp
 *  @brief   Interface to handle the adaptation of voice samples to sensory
 *
 *  DESCRIPTION
 *    Engine between Sensory and Voice UI Framework for DSPC
 ***************************************************************/

#include <Wakeword/Sensory/SensoryHandler.h>

#include <syslog.h>

#define TRUE true
#define FALSE false
#define SENSORY_PREROLL 8000
#define SENSORY_VAD_CHUNK_SIZE 512

static const unsigned int SENSORY_SAMPLE_RATE_HZ = 16000;
static const unsigned int SENSORY_HISTORY_BUFFER_IN_SEC = 5;
static const double QUIET_NAN =
    std::numeric_limits<double>::quiet_NaN();

namespace voiceUIFramework {
namespace wakeword {
namespace sensory {

static std::string GetSensoryDetails(SnsrSession session, SnsrRC result) {
  std::string message;
  if (session) {
    message = snsrErrorDetail(session);
  } else {
    message = snsrRCMessage(result);
  }
  if (message.empty()) {
    message = "Unrecognized error";
  }
  return message;
}

std::shared_ptr<SensoryHandler> SensoryHandler::Create(
    const std::string& modelFilePath,
    int myInstanceId, int operatingPoint) {
  if (modelFilePath.empty()) {
    syslog(LOG_ERR, "SensoryHandler::Create(): no model file path provided");
    return nullptr;
  }

  std::shared_ptr<SensoryHandler> sensory = std::shared_ptr<SensoryHandler>(
      new SensoryHandler(myInstanceId));
  if (sensory == nullptr || !sensory->Init(modelFilePath, operatingPoint)) {
    syslog(LOG_ERR, "SensoryHandler::Create() Error during initialization");
    return nullptr;
  }

  return sensory;
}

int SensoryHandler::GetInstanceId() {
  return my_instance_id_;
}

bool SensoryHandler::IsKeyWordHit(
    std::vector<uint8_t> audio_channel,
    voiceUIFramework::wakeword::KwdDetectionResults& result,
    std::vector<uint8_t>& labdata) {
  DisableKeyWordHitWinnerFound();

  SnsrRC rc = FeedToSensory(audio_channel);
  if (rc == SNSR_RC_STOP) {
    syslog(
        LOG_INFO, "SensoryHandler::IsKeyWordHit() keyword has been detected. Callback has been issued");
  }
  snsrClearRC(sensory_session_);

  labdata = std::move(lab_data_);

  if (keyword_hit_winner_found_) {
    result.keyword_start_index_ = keyword_hit_begin_index_;
    result.keyword_end_index_ = keyword_hit_end_index_;
    return TRUE;
  }

  return FALSE;
}

void SensoryHandler::FlushBuffers() {
  snsrRelease(sensory_session_);
  if (Init(modelFile_, operating_point_)) {
    syslog(LOG_INFO, "SensoryHandler::FlushBuffers() - Initialization success %d", my_instance_id_);
  } else {
    syslog(LOG_ERR, "SensoryHandler::FlushBuffers() - Initialization failed %d", my_instance_id_);
  }
}

SensoryHandler::SensoryHandler(int myInstanceId)
    : my_instance_id_{myInstanceId}, keyword_hit_begin_index_{QUIET_NAN}, keyword_hit_end_index_{QUIET_NAN}, keyword_hit_winner_found_{false}, sensory_session_{nullptr}, lab_capture_dump_{nullptr}, modelFile_{""}, operating_point_{6} {
}

SnsrRC SensoryHandler::FeedToSensory(std::vector<uint8_t> audioChannel) {
  SnsrStream audioStream = snsrStreamFromMemory(&audioChannel[0],
      sizeof(uint8_t) * audioChannel.size(), SNSR_ST_MODE_READ);

  snsrSetStream(sensory_session_, SNSR_SOURCE_AUDIO_PCM, audioStream);

  snsrRun(sensory_session_);

  return snsrRC(sensory_session_);
}

SensoryHandler::~SensoryHandler() {
  syslog(LOG_INFO, "SensoryWakewordEngine::~SensoryHandler");
  snsrRelease(sensory_session_);
  syslog(LOG_INFO, "SensoryWakewordEngine::~SensoryHandler - DONE!");
}

bool SensoryHandler::Init(const std::string& modelFilePath, int operatingPoint) {
  SnsrRC result = snsrNew(&sensory_session_);

  operating_point_ = operatingPoint;

  if (result != SNSR_RC_OK) {
    syslog(LOG_ERR, "SensoryHandler::Init() snsrNew::Cannot create a new sensory session - Error = %d", result);
    return FALSE;
  }

  result = snsrLoad(sensory_session_,
      snsrStreamFromFileName(modelFilePath.c_str(), "r"));
  if (result != SNSR_RC_OK) {
    syslog(
        LOG_ERR, "SensoryHandler::Init() snsrLoad::Loading the sensory model failed %s \n",
        GetSensoryDetails(sensory_session_, result).c_str());
    return FALSE;
  }

  modelFile_ = modelFilePath;

  return RunTimeSettings(&sensory_session_);
}

bool SensoryHandler::RunTimeSettings(SnsrSession* sensory_session) {
  SnsrRC result = snsrRequire(*sensory_session, SNSR_TASK_TYPE,
      SNSR_PHRASESPOT);
  if (result != SNSR_RC_OK) {
    syslog(
        LOG_ERR, "SensoryHandler::Init() snsrRequire::task failed %s \n",
        GetSensoryDetails(*sensory_session, result).c_str());
    return FALSE;
  }

  result = snsrSetHandler(sensory_session_, SNSR_RESULT_EVENT,
      snsrCallback(KeyWordDetectedCallback, nullptr,
          reinterpret_cast<void*>(this)));
  if (result != SNSR_RC_OK) {
    syslog(
        LOG_ERR, "SensoryHandler::Init() setupRuntimeSettings::snsrSetHandler failed %s",
        GetSensoryDetails(sensory_session_, result).c_str());
    return FALSE;
  }

  result = snsrSetInt(sensory_session_, SNSR_AUDIO_STREAM_SIZE,
      SENSORY_HISTORY_BUFFER_IN_SEC * SENSORY_SAMPLE_RATE_HZ);
  if (result != SNSR_RC_OK) {
    syslog(
        LOG_ERR, "SensoryHandler::Init() setupRuntimeSettings::SNSR_AUDIO_STREAM_SIZE set failed %s",
        GetSensoryDetails(sensory_session_, result).c_str());
    return FALSE;
  }

  result = snsrSetInt(sensory_session_, SNSR_AUTO_FLUSH, 0);
  if (result != SNSR_RC_OK) {
    syslog(
        LOG_ERR, "SensoryHandler::Init() setupRuntimeSettings::disable Audio pipeline flush failed %s",
        GetSensoryDetails(sensory_session_, result).c_str());
    return FALSE;
  }

  result = snsrSetInt(sensory_session_, SNSR_OPERATING_POINT,
      operating_point_);
  if (result != SNSR_RC_OK) {
    syslog(
        LOG_ERR, "SensoryHandler::Init() setupRuntimeSettings::cannot set Sensory operating point %s",
        GetSensoryDetails(sensory_session_, result).c_str());
    return FALSE;
  }

  return TRUE;
}

SnsrRC SensoryHandler::KeyWordDetectedCallback(
    SnsrSession s,
    const char* key,
    void* userData) {
  SensoryHandler* engine = static_cast<SensoryHandler*>(userData);
  if (nullptr == engine) {
    syslog(LOG_ERR, "SensoryHandler:KeyWordDetectedCallback() sensory engine returned null un-expectedly");
    return SNSR_RC_ERROR;
  }

  SnsrRC result = engine->ProcessKeywordHitResult();

  return result;
}

SnsrRC SensoryHandler::ProcessKeywordHitResult() {
  SnsrRC result = SNSR_RC_OK;
  if (!keyword_hit_winner_found_) {
    syslog(LOG_INFO, "SensoryHandler::ProcessKeywordHitResult() Keyword hit winner has been found. Process result");

    double begin, end;
    result = FetchKeywordIndicesFromSensory(&begin, &end);
    if (result != SNSR_RC_OK) {
      syslog(
          LOG_ERR,
          "SensoryHandler::ProcessKeywordHitResult() ProcessKeywordHitResult reason FetchKeywordIndicesFromSensory failed %s",
          GetSensoryDetails(sensory_session_, result).c_str());
      return result;
    }

    result = CaptureLookAheadBufferAdjustKeywordIndices(begin, end);
    if (result != SNSR_RC_OK) {
      syslog(
          LOG_ERR,
          "SensoryHandler::ProcessKeywordHitResult() ProcessKeywordHitResult reason CaptureLookAheadBufferAdjustKeywordIndices failed %s",
          GetSensoryDetails(sensory_session_, result).c_str());
      return result;
    }
    keyword_hit_winner_found_ = true;
  } else {
    syslog(LOG_INFO, "SensoryHandler::ProcessKeywordHitResult() Keyword hit winner has been already done. IGNORE");
    return result;
  }

  return result;
}

SnsrRC SensoryHandler::FetchKeywordIndicesFromSensory(
    double* begin,
    double* end) {
  SnsrRC result = snsrGetDouble(sensory_session_, SNSR_RES_BEGIN_SAMPLE,
      begin);
  if (result != SNSR_RC_OK) {
    syslog(
        LOG_ERR, "SensoryHandler::FetchKeywordIndicesFromSensory() keyWordDetectedCallbackFailed reason invalidBeginIndex %s",
        GetSensoryDetails(sensory_session_, result).c_str());
    return result;
  }

  result = snsrGetDouble(sensory_session_, SNSR_RES_END_SAMPLE, end);
  if (result != SNSR_RC_OK) {
    syslog(
        LOG_ERR, "SensoryHandler::FetchKeywordIndicesFromSensory() keyWordDetectedCallbackFailed reason invalidEndIndex %s",
        GetSensoryDetails(sensory_session_, result).c_str());
    return result;
  }

  syslog(
      LOG_INFO,
      "SensoryHandler::FetchKeywordIndicesFromSensory() Original Keyword indices available are here begin = %f, end = %f",
      *begin, *end);

  return result;
}

SnsrRC SensoryHandler::CaptureLookAheadBufferAdjustKeywordIndices(
    double begin,
    double end) {
  double audiostream_begin = begin - SENSORY_PREROLL;
  SnsrRC result = snsrSetDouble(sensory_session_, SNSR_AUDIO_STREAM_FROM,
      audiostream_begin);
  if (result != SNSR_RC_OK) {
    syslog(
        LOG_ERR,
        "SensoryHandler::CaptureLookAheadBufferAdjustKeywordIndices() CaptureLookAheadBuffer reason snsrSetDouble SNSR_AUDIO_STREAM_FROM failed %f",
        audiostream_begin);
    return result;
  }

  double audiostream_end;
  result = snsrGetDouble(sensory_session_, SNSR_AUDIO_STREAM_LAST,
      &audiostream_end);
  if (result != SNSR_RC_OK) {
    syslog(
        LOG_ERR,
        "SensoryHandler::CaptureLookAheadBufferAdjustKeywordIndices() CaptureLookAheadBuffer reason snsrGetDouble SNSR_AUDIO_STREAM_LAST failed");
    return result;
  }

  result = snsrSetDouble(sensory_session_, SNSR_AUDIO_STREAM_TO,
      audiostream_end);
  if (result != SNSR_RC_OK) {
    syslog(
        LOG_ERR,
        "SensoryHandler::CaptureLookAheadBufferAdjustKeywordIndices() CaptureLookAheadBuffer reason snsrSetDouble SNSR_AUDIO_STREAM_TO failed %f",
        audiostream_end);
    return result;
  }

  SnsrStream capture_lab_stream = nullptr;
  result = snsrGetStream(sensory_session_, SNSR_AUDIO_STREAM,
      &capture_lab_stream);
  if (result == SNSR_RC_OK) {
    syslog(LOG_INFO,
        "SensoryHandler::CaptureLookAheadBufferAdjustKeywordIndices() capture in a sensory dump file. This will contain the phrase and the buffer available ");

    uint8_t samples[SENSORY_VAD_CHUNK_SIZE];
    size_t read;  // @suppress("Type cannot be resolved")
    lab_capture_dump_ = fopen("/data/sensorydump", "w");
    do {
      read = snsrStreamRead(capture_lab_stream, samples, sizeof(*samples),  // @suppress("Invalid arguments")
          SENSORY_VAD_CHUNK_SIZE);
      fwrite(samples, sizeof(uint8_t), read, lab_capture_dump_);          // @suppress("Invalid arguments")
      std::copy(samples, samples + read, std::back_inserter(lab_data_));  // @suppress("Invalid arguments")

    } while (read == SENSORY_VAD_CHUNK_SIZE);
    fclose(lab_capture_dump_);

    keyword_hit_end_index_ = SENSORY_PREROLL + end - begin;
    keyword_hit_begin_index_ = SENSORY_PREROLL;

    syslog(
        LOG_INFO, "SensoryHandler::CaptureLookAheadBufferAdjustKeywordIndices() FINAL Indices are here begin = %f, end = %f",
        keyword_hit_begin_index_, keyword_hit_end_index_);
  }

  return result;
}

void SensoryHandler::DisableKeyWordHitWinnerFound() {
  keyword_hit_winner_found_ = false;
  keyword_hit_begin_index_ = QUIET_NAN;
  keyword_hit_end_index_ = QUIET_NAN;
}

}  // namespace sensory
}  // namespace wakeword
}  // namespace voiceUIFramework
