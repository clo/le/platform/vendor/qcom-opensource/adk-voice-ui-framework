/*Copyright (c) 2019, The Linux Foundation. All rights reserved.
*
*Redistribution and use in source and binary forms, with or without
*modification, are permitted provided that the following conditions are
*met:
*    * Redistributions of source code must retain the above copyright
*      notice, this list of conditions and the following disclaimer.
*    * Redistributions in binary form must reproduce the above
*      copyright notice, this list of conditions and the following
*      disclaimer in the documentation and/or other materials provided
*      with the distribution.
*    * Neither the name of The Linux Foundation nor the names of its
*      contributors may be used to endorse or promote products derived
*      from this software without specific prior written permission.
*
*THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
*WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
*MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
*ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
*BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
*BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
*WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
*OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
*IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    SensoryWakewordEngine.cpp
 *  @brief   Interface to handle the adaptation of voice samples to sensory
 *
 *  DESCRIPTION
 *    Engine between Sensory and Voice UI Framework for DSPC
 ***************************************************************/

#include <VoiceUIUtils/ThirdPartyAudioUtils.h>
#include <Wakeword/Sensory/SensoryWakewordEngine.h>

#include <syslog.h>

#define DSPC_SENSORY_NOOP -1
#define DSPC_SENSORY_SUCCESS 0
#define DSPC_SENSORY_CHANNEL_1 1
#define DSPC_SENSORY_CHANNEL_2 2
#define DSPC_ASR_CHANNEL 3

#define TRUE true
#define FALSE false

namespace voiceUIFramework {
namespace wakeword {
namespace sensory {

std::shared_ptr<SensoryWakewordEngine> SensoryWakewordEngine::Create() {
  auto sensory_multiple_instance_delegate =
      SensoryMultipleInstanceDelegate::Create();
  if (nullptr == sensory_multiple_instance_delegate) {
    return nullptr;
  }

  auto sensory = std::shared_ptr<SensoryWakewordEngine>(
      new SensoryWakewordEngine(sensory_multiple_instance_delegate));

  syslog(LOG_INFO, "SensoryWakewordEngine::Create() SensoryWakewordEngine::Create() - Done.");
  return sensory;
}

SensoryWakewordEngine::SensoryWakewordEngine(
    std::shared_ptr<SensoryMultipleInstanceDelegate> sensoryMultipleInstanceDelegate)
    : sensory_multiple_instance_delegate_{
          sensoryMultipleInstanceDelegate}
    , audio_capture_callback_{nullptr}
    , enable_keyword_recognition_{false}
    , keyword_hit_session_{false} {
}

SensoryWakewordEngine::~SensoryWakewordEngine() {
  syslog(LOG_INFO, "SensoryWakewordEngine::~SensoryWakewordEngine");
}

bool SensoryWakewordEngine::Feed(const std::vector<int> in) {
  auto thirdparty_audio_util =
      voiceUIFramework::voiceUIUtils::ThirdPartyAudioUtils::getInstance();

  WakewordCallbackRc wakeword_rc = WakewordCallbackRc::WW_CB_OK;
  bool is_keyword_hit = false;
  bool is_asr_setup_ready = false;
  if (thirdparty_audio_util) {
    std::vector<uint8_t> localasr_audio_channel =
        thirdparty_audio_util->Demux16khz16bitpcm(in,
            DSPC_ASR_CHANNEL);
    std::lock_guard<std::mutex> lock(enable_keyword_recognition_mutex_);
    if (enable_keyword_recognition_) {
      voiceUIFramework::wakeword::KwdDetectionResults kwd_result;
      std::vector<uint8_t> lab_data;
      is_keyword_hit = sensory_multiple_instance_delegate_->IsKeyWordHit(
          in, kwd_result, lab_data);

      SetKeywordSessionStatus(is_keyword_hit);

      if (is_keyword_hit) {
        wakeword_rc = SetupAudioDataForASR(kwd_result);
        if (WakewordCallbackRc::WW_CB_OK == wakeword_rc) {
          wakeword_rc = FeedAudioDataToASR(lab_data);
          if (WakewordCallbackRc::WW_CB_OK == wakeword_rc) {
            is_asr_setup_ready = true;
          } else {
            SetKeywordSessionStatus(false);
          }
        }
      }
    } else {
      FeedAudioDataToASR(localasr_audio_channel);
      return true;
    }
  }

  if (is_keyword_hit && is_asr_setup_ready) {
    DisableKeywordRecognition();
  }
  return is_keyword_hit;
}

int SensoryWakewordEngine::StartRecognition(VoiceUIClientID client_id) {
  return StartRecognition();
}

int SensoryWakewordEngine::StartRecognition() {
  EnableKeywordRecognition();
  SetKeywordSessionStatus(false);
  return DSPC_SENSORY_SUCCESS;
}

int SensoryWakewordEngine::StopRecognition(VoiceUIClientID client_id) {
  DisableKeywordRecognition();
  return DSPC_SENSORY_SUCCESS;
}

int SensoryWakewordEngine::StopRecognition() {
  DisableKeywordRecognition();
  return DSPC_SENSORY_SUCCESS;
}

void SensoryWakewordEngine::StopAudioCapture(
    VoiceUIClientID client_id,
    StopCaptureResult capture_result) {
  switch (capture_result) {
    case StopCaptureResult::STOPCAPTURE_OK: {
      StartRecognition();
      break;
    }
    default:
      syslog(LOG_INFO,
          "SensoryWakewordEngine::StopAudioCapture() StopCapture error received highly unlikely but lets get the error for now. In Sensory's case this will be factored during the call. So we can ignore %s",
          StopCaptureResultToString(capture_result));
  }
}

bool SensoryWakewordEngine::AddObserver(
    VoiceUIClientID client,
    std::shared_ptr<WakeWordCallBackInterface> observer) {
  if (nullptr == observer) {
    return FALSE;
  }
  std::lock_guard<std::mutex> lock(audio_capture_callback_mutex_);
  audio_capture_callback_ = observer;

  observer->ObserverAdded();

  return TRUE;
}

bool SensoryWakewordEngine::RemoveObserver(VoiceUIClientID client) {
  std::lock_guard<std::mutex> lock(audio_capture_callback_mutex_);
  audio_capture_callback_.reset();
  return TRUE;
}

void SensoryWakewordEngine::KeywordRecognized() {
  syslog(LOG_ERR, "SensoryWakewordEngine::KeywordRecognized() No implementation provided");
}

int SensoryWakewordEngine::GetDOADirection(VoiceUIClientID client_id) {
  syslog(LOG_ERR, "SensoryWakewordEngine::GetDOADirection() No implementation provided");
  return DSPC_SENSORY_NOOP;
}

int SensoryWakewordEngine::GetChannelIndex(VoiceUIClientID client_id) {
  syslog(LOG_ERR, "SensoryWakewordEngine::GetChannelIndex() No implementation provided");
  return DSPC_SENSORY_NOOP;
}

void SensoryWakewordEngine::EnableKeywordRecognition() {
  std::lock_guard<std::mutex> lock(enable_keyword_recognition_mutex_);
  enable_keyword_recognition_ = true;
}

void SensoryWakewordEngine::DisableKeywordRecognition() {
  std::lock_guard<std::mutex> lock(enable_keyword_recognition_mutex_);
  enable_keyword_recognition_ = false;
}

void SensoryWakewordEngine::SetKeywordSessionStatus(bool keywordHit) {
  std::lock_guard<std::mutex> lock(keyword_hit_session_mutex_);
  keyword_hit_session_ = keywordHit;
}

WakewordCallbackRc SensoryWakewordEngine::SetupAudioDataForASR(
    voiceUIFramework::wakeword::KwdDetectionResults &kwd_result) {
  std::lock_guard<std::mutex> lock(audio_capture_callback_mutex_);
  if (audio_capture_callback_) {
    return audio_capture_callback_->OnKeywordDetected(kwd_result);
  }
  return WakewordCallbackRc::WW_CB_ERR;
}

WakewordCallbackRc SensoryWakewordEngine::FeedAudioDataToASR(
    std::vector<uint8_t> audioASR) {
  std::lock_guard<std::mutex> lock(keyword_hit_session_mutex_);
  if (keyword_hit_session_) {
    std::lock_guard<std::mutex> lock(audio_capture_callback_mutex_);
    if (audio_capture_callback_) {
      return audio_capture_callback_->OnNewVoiceBufferReceived(
          &audioASR[0],
          audioASR.size());
    }
  }
  return WakewordCallbackRc::WW_CB_ERR;
}

}  // namespace sensory
}  // namespace wakeword
}  // namespace voiceUIFramework
