/*Copyright (c) 2019-2020, The Linux Foundation. All rights reserved.
*
*Redistribution and use in source and binary forms, with or without
*modification, are permitted provided that the following conditions are
*met:
*    * Redistributions of source code must retain the above copyright
*      notice, this list of conditions and the following disclaimer.
*    * Redistributions in binary form must reproduce the above
*      copyright notice, this list of conditions and the following
*      disclaimer in the documentation and/or other materials provided
*      with the distribution.
*    * Neither the name of The Linux Foundation nor the names of its
*      contributors may be used to endorse or promote products derived
*      from this software without specific prior written permission.
*
*THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
*WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
*MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
*ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
*BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
*BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
*WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
*OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
*IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    SensoryMultipleInstanceDelegate.cpp
 *  @brief   Interface to handle the adaptation of voice samples to sensory
 *
 *  DESCRIPTION
 *    Component that abstracts the number of instances for Sensory
 ***************************************************************/

#include <VoiceUIUtils/ThirdPartyAudioUtils.h>
#include <Wakeword/Sensory/SensoryMultipleInstanceDelegate.h>
#include <syslog.h>

#define TRUE true
#define FALSE false
#define MAX_AUDIO_BUFFERS 1
#define DSPC_SENSORY_CHANNEL_1 1
#define DSPC_SENSORY_CHANNEL_2 2
#define SENSORY_OPERATING_POINT 6
#define DEFAULT_SOUND_MODEL "/data/voice-ui-framework/thfft_alexa_enus_THF6.0_1mbi.snsr"

namespace voiceUIFramework {
namespace wakeword {
namespace sensory {

std::shared_ptr<SensoryMultipleInstanceDelegate> SensoryMultipleInstanceDelegate::Create() {
  //Read VoiceUI Database
  auto voice_ui_config = voiceUIFramework::voiceUIUtils::VoiceUIConfig::getInstance();
  int sensory_operating_point;
  std::string sensory_soundmodel;
  if (voice_ui_config) {
    //Sensory Operating point
    sensory_operating_point = voice_ui_config->ReadWithDefault<int>(SENSORY_OPERATING_POINT, voiceUIFramework::voiceUIUtils::Keys::k_avs_sensory_operating_point);
    //Sensory sound model
    sensory_soundmodel = voice_ui_config->ReadWithDefault<std::string>(DEFAULT_SOUND_MODEL, voiceUIFramework::voiceUIUtils::Keys::k_avs_sensory_sound_model);
  } else {
    syslog(LOG_ERR, "SensoryMultipleInstanceDelegate::Create() - VoiceUIConfig not Initialized");
    sensory_operating_point = SENSORY_OPERATING_POINT;
    sensory_soundmodel = DEFAULT_SOUND_MODEL;
  }
  syslog(LOG_INFO, "SensoryMultipleInstanceDelegate::Create - Sensory info:  Operating Point = %d / Sound Model = %s", sensory_operating_point, sensory_soundmodel.c_str());

  std::shared_ptr<SensoryHandler> sensory_handler_1 = SensoryHandler::Create(
      sensory_soundmodel, 1, sensory_operating_point);
  std::shared_ptr<SensoryHandler> sensory_handler_2 = SensoryHandler::Create(
      sensory_soundmodel, 2, sensory_operating_point);
  if (nullptr == sensory_handler_1 || nullptr == sensory_handler_2) {
    syslog(LOG_ERR,
        "SensoryMultipleInstanceDelegate::Create() Delegate cannot be created. Sensory Handlers cannot be created");
    return nullptr;
  }

  syslog(LOG_INFO, "SensoryMultipleInstanceDelegate:::Create() Sensory handlers created!");

  std::shared_ptr<SensoryMultipleInstanceDelegate> sensory_multiple_delegate_instance =
      std::shared_ptr<SensoryMultipleInstanceDelegate>(
          new SensoryMultipleInstanceDelegate(sensory_handler_1,
              sensory_handler_2));
  if (nullptr == sensory_multiple_delegate_instance) {
    syslog(LOG_ERR, "SensoryMultipleInstanceDelegate:::Create() Delegate cannot be created");
    return nullptr;
  }

  sensory_multiple_delegate_instance->Init();

  return sensory_multiple_delegate_instance;
}

SensoryMultipleInstanceDelegate::~SensoryMultipleInstanceDelegate() {
  syslog(LOG_INFO, "SensoryWakewordEngine::~SensoryMultipleInstanceDelegate");
  shutting_down_ = true;
  if (sensory_handler_instance_1_thread_.joinable()) {
    sensory_handler_instance_1_thread_.join();
  }
  if (sensory_handler_instance_2_thread_.joinable()) {
    sensory_handler_instance_2_thread_.join();
  }
  syslog(LOG_INFO, "SensoryWakewordEngine::~SensoryMultipleInstanceDelegate - DONE!");
}

void SensoryMultipleInstanceDelegate::ResetStateForKeywordRecognition() {
  std::lock_guard<std::mutex> lock(is_keyword_hit_mutex_);
  is_keyword_hit_ = false;
}

void SensoryMultipleInstanceDelegate::Init() {
  shutting_down_ = false;
  sensory_handler_instance_1_thread_ = std::thread(
      &SensoryMultipleInstanceDelegate::Loop, this,
      sensory_handler_1_.get(),
      &audio_buffer_sensory_instance_1_available_mutex_,
      &audio_buffer_sensory_instance_1_available_condition_variable_,
      &sensory_instance_1_audio_queue_);
  sensory_handler_instance_2_thread_ = std::thread(
      &SensoryMultipleInstanceDelegate::Loop, this,
      sensory_handler_2_.get(),
      &audio_buffer_sensory_instance_2_available_mutex_,
      &audio_buffer_sensory_instance_2_available_condition_variable_,
      &sensory_instance_2_audio_queue_);
}

bool SensoryMultipleInstanceDelegate::IsKeyWordHit(
    std::vector<int> muxAudioData,
    voiceUIFramework::wakeword::KwdDetectionResults& result,
    std::vector<uint8_t>& labdata) {
  is_keyword_hit_ = FALSE;

  auto thirdparty_audio_util =
      voiceUIFramework::voiceUIUtils::ThirdPartyAudioUtils::getInstance();

  audiopayload sensory_1_audio_payload;
  std::promise<bool> sensory_1_promise;
  {
    std::unique_lock<std::mutex> lock(
        audio_buffer_sensory_instance_1_available_mutex_);
    std::vector<uint8_t> demuxed_16bit16khz_channel_1 =
        thirdparty_audio_util->Demux16khz16bitpcm(muxAudioData,
            DSPC_SENSORY_CHANNEL_1);

    sensory_1_audio_payload.audio = std::move(demuxed_16bit16khz_channel_1);
    sensory_1_audio_payload.promise = &sensory_1_promise;

    sensory_instance_1_audio_queue_.push(sensory_1_audio_payload);
  }

  audiopayload sensory_2_audio_payload;
  std::promise<bool> sensory_2_promise;
  {
    std::unique_lock<std::mutex> lock(
        audio_buffer_sensory_instance_2_available_mutex_);
    std::vector<uint8_t> demuxed_16bit16khz_channel_2 =
        thirdparty_audio_util->Demux16khz16bitpcm(muxAudioData,
            DSPC_SENSORY_CHANNEL_2);

    sensory_2_audio_payload.audio = std::move(demuxed_16bit16khz_channel_2);
    sensory_2_audio_payload.promise = &sensory_2_promise;

    sensory_instance_2_audio_queue_.push(sensory_2_audio_payload);
  }

  audio_buffer_sensory_instance_1_available_condition_variable_.notify_one();
  audio_buffer_sensory_instance_2_available_condition_variable_.notify_one();

  sensory_1_promise.get_future().get()
      && sensory_2_promise.get_future().get();

  {
    std::lock_guard<std::mutex> lock(is_keyword_hit_mutex_);
    if (is_keyword_hit_) {
      syslog(LOG_INFO, "SensoryMultipleInstanceDelegate::IsKeyWordHit() keyword has been detected. Final Packaging");
      result.keyword_start_index_ =
          kwd_detection_result_.keyword_start_index_;
      result.keyword_end_index_ =
          kwd_detection_result_.keyword_end_index_;
      labdata = std::move(lab_data_);

      //clean both Sensory engines internal buffers.
      syslog(LOG_INFO, "SensoryMultipleInstanceDelegate::IsKeyWordHit() - Cleaning Sensory engines (internal buffers)");
      Flush();
    }
  }

  return is_keyword_hit_;
}

void SensoryMultipleInstanceDelegate::Flush() {
  sensory_handler_1_->FlushBuffers();
  sensory_handler_2_->FlushBuffers();
}

void SensoryMultipleInstanceDelegate::OnKeywordHit(
    int instanceId,
    std::vector<uint8_t>& labData,
    KwdDetectionResults& result) {
  syslog(LOG_INFO,
      "SensoryMultipleInstanceDelegate::OnKeywordHit() Hit has been determined by instance %d. Waiting to acquire lock",
      instanceId);
  std::lock_guard<std::mutex> lock(is_keyword_hit_mutex_);
  syslog(LOG_INFO, "SensoryMultipleInstanceDelegate::OnKeywordHit() Lock acquired by instance %d", instanceId);
  if (is_keyword_hit_ == false) {
    is_keyword_hit_ = true;

    kwd_detection_result_.keyword_start_index_ =
        result.keyword_start_index_;
    kwd_detection_result_.keyword_end_index_ = result.keyword_end_index_;

    lab_data_ = std::move(labData);

    syslog(LOG_INFO,
        "SensoryMultipleInstanceDelegate::OnKeywordHit() Instance Id %d results will be used begin = %d, end = %d",
        instanceId, kwd_detection_result_.keyword_start_index_,
        kwd_detection_result_.keyword_end_index_);

  } else {
    syslog(LOG_INFO, "SensoryMultipleInstanceDelegate::OnKeywordHit() Winner already processed. Ignore this instance %d",
        instanceId);
  }
}

void SensoryMultipleInstanceDelegate::Loop(
    SensoryHandler* sensory_instance,
    std::mutex* audio_buffer_sensory_available,
    std::condition_variable* audio_buffer_sensory_available_condition_variable,
    std::queue<audiopayload>* audio_queue) {
  while (!shutting_down_) {
    std::unique_lock<std::mutex> lock(*audio_buffer_sensory_available);
    audio_buffer_sensory_available_condition_variable->wait(lock,
        [=] { return !audio_queue->empty(); });

    if (audio_queue->size() == 0) {
      syslog(LOG_INFO,
          "SensoryMultipleInstanceDelegate::Loop() Incorrect wake up. Nothing to process. Continue and block");
    }

    if (audio_queue->size() != MAX_AUDIO_BUFFERS) {
      syslog(LOG_INFO,
          "SensoryMultipleInstanceDelegate::Loop( More audio buffers received than expected. We pick the first, size = %d",
          audio_queue->size());
    }

    audiopayload audio_payload = audio_queue->front();
    std::promise<bool>* promise = audio_payload.promise;
    std::vector<uint8_t> audio_block = audio_payload.audio;

    std::vector<uint8_t> lab_data;
    KwdDetectionResults results;
    bool is_keyword_hit = sensory_instance->IsKeyWordHit(audio_block,
        results, lab_data);

    if (is_keyword_hit) {
      syslog(LOG_INFO,
          "SensoryMultipleInstanceDelegate::Loop( Keyword hit has been detected by sensory instance %d",
          sensory_instance->GetInstanceId());
      OnKeywordHit(sensory_instance->GetInstanceId(), lab_data, results);
    }
    audio_queue->pop();

    promise->set_value(true);
  }
}

SensoryMultipleInstanceDelegate::SensoryMultipleInstanceDelegate(
    std::shared_ptr<SensoryHandler> sensory_handler_1,
    std::shared_ptr<SensoryHandler> sensory_handler_2)
    : sensory_handler_1_{sensory_handler_1}, sensory_handler_2_{sensory_handler_2}, is_keyword_hit_{false}, shutting_down_{false} {
}

}  // namespace sensory
}  // namespace wakeword
}  // namespace voiceUIFramework
