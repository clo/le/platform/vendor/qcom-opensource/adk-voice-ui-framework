/* Copyright (c) 2018, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    WakewordDummyClient.cpp
 *  @brief   Dummy Wakeword client
 *
 *  DESCRIPTION
 *    Dummy WakeWord Client
 ***************************************************************/

#include <VoiceUIUtils/GlobalValues.h>
#include <Wakeword/WakeWordCallBackInterface.h>
#include <Wakeword/WakewordDummyClient.h>

namespace voiceUIFramework {
namespace wakeword {

// Static Members Declaration
std::weak_ptr<WakewordDummyClient> WakewordDummyClient::wakeword_dummy_client_;

std::shared_ptr<WakewordDummyClient> WakewordDummyClient::Get() {
  std::shared_ptr<WakewordDummyClient> wakeword_dummy_client = wakeword_dummy_client_.lock();
  if (!wakeword_dummy_client) {
    wakeword_dummy_client.reset(new WakewordDummyClient());
    wakeword_dummy_client_ = wakeword_dummy_client;
    //TODO:Initialize to fetch database settings instead of having that in constructor
  }
  return std::shared_ptr<WakewordDummyClient>(wakeword_dummy_client_);
}
//
// Constructor
//

WakewordDummyClient::WakewordDummyClient()
    : target_doa_index_{0}, target_channel_index{0}, keyword_start_index_{0}, keyword_end_index_{0}, detection_count_{0}, recognizing_{false} {
  syslog(LOG_DEBUG, "+++++++WakewordDummyClient Constructor called");
}

//
// Destructor
//

WakewordDummyClient::~WakewordDummyClient() {
  syslog(LOG_DEBUG, "+++++++WakewordDummyClient Destructor called");
}

int WakewordDummyClient::Initialize() {
  syslog(LOG_DEBUG, "WakewordDummyClient::Initialize()");
  return 0;
}

int WakewordDummyClient::StartRecognition() {
  syslog(LOG_DEBUG, "WakewordDummyClient::StartRecognition()");
  int err = 0;
  if (!recognizing_) {
    recognizing_ = true;
  }
  //TODO handle when it fails
  //    else {
  //        err = 1;
  //    }
  return err;
}

int WakewordDummyClient::StartRecognition(VoiceUIClientID client_id) {
}

int WakewordDummyClient::StopRecognition() {
  syslog(LOG_DEBUG, "WakewordDummyClient::StopRecognition()");
  int err = 0;
  if (recognizing_) {
    recognizing_ = false;
  }
  //TODO handle when it fails
  //    else {
  //        err = 1;
  //    }
  return err;
}

int WakewordDummyClient::StopRecognition(VoiceUIClientID client_id) {
}

void WakewordDummyClient::StopAudioCapture(
    VoiceUIClientID client_id,
    StopCaptureResult capture_result) {
  //TODO must  StartRecognition again
}

void WakewordDummyClient::KeywordRecognized() {
}

bool WakewordDummyClient::AddObserver(VoiceUIClientID client, std::shared_ptr<WakeWordCallBackInterface> observer) {
  return true;
}
//Removes the client as wakeword observer
bool WakewordDummyClient::RemoveObserver(VoiceUIClientID client) {
  return true;
}

bool WakewordDummyClient::Feed(const std::vector<int> in) {
  return true;
}

void WakewordDummyClient::ReinitializeWakeWord() {
}

int WakewordDummyClient::GetDOADirection(VoiceUIClientID client_id) {
  return 0;
}

int WakewordDummyClient::GetChannelIndex(VoiceUIClientID client_id) {
  return 0;
}

}  // namespace wakeword
}  // namespace voiceUIFramework
