/* Copyright (c) 2018, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    QSTHWClient.cpp
 *  @brief   Wakeword client for STHAL
 *
 *  DESCRIPTION
 *    WakeWord Client based on Snapdragon Voice Activation using ST HAL
 ***************************************************************/

#include <VoiceUIUtils/VoiceUIConfig.h>
#include <Wakeword/QSTHWClient.h>
#include <Wakeword/WakeWordCallBackInterface.h>

#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <string>

namespace voiceUIFramework {
namespace wakeword {

// Static Members Declaration
std::weak_ptr<QSTHWClient> QSTHWClient::qsthwClient_ptr_;
std::map<std::string, std::string> QSTHWClient::global_wakeword_params;
std::mutex getlck;

#define SM_MINOR_VERSION 1
#define SM_MINOR_VERSION_2 2

// Private Structures - Local
struct pa_qst_source_tracking_param {
  int target_angle_L16[2];
  int interf_angle_L16[2];
  char polarActivityGUI[360];
};

struct pa_qst_target_channel_index_param {
  int target_chan_idx;
};

typedef union {
  struct pa_qst_source_tracking_param st_params;
  struct pa_qst_target_channel_index_param ch_index_params;
} pa_qst_get_param_payload_t;

// Paratmeterized Constructors for the client/session data
pa_qst_session_data::pa_qst_session_data(
    VoiceUIClientID client, std::shared_ptr<WakeWordCallBackInterface> observer)
    : observer_client(observer), counter(0), clientid(client), num_kws(1), ses_handle(0), loaded(false), started(false), stopcapture(false), pa_qst_event(NULL), rc_config(NULL), target_channel_idx(-1), target_doa_idx(-1), request_stop_in_progress_{false} {
  syslog(LOG_DEBUG,
      "Thread %x    +++++++pa_qst_session_data Constructor called ",
      std::this_thread::get_id());
  sm_file_path = get_soundmodel_path();

  if (clientid == VoiceUIClientID::AVS_SOLUTION) {
    //Check if AVS keyword has a Second Stage on ARM
    auto voice_ui_config = voiceUIFramework::voiceUIUtils::VoiceUIConfig::getInstance();
    has_second_stage_ = voice_ui_config->ReadWithDefault<bool>(true, voiceUIUtils::Keys::k_avs_has_arm_second_stage_);

    // Get all the configurable paramters from the database
    kw_buffer_duration_in_ms = get_kw_buffer_duration_in_ms();
    // We need confidence level info(2nd stage SVA Confidence Levels and
    // hist-buffer info( for Keyword Indices)
    generic_payload_size = sizeof(struct opaque_param_header) + sizeof(struct confidence_levels_info) + sizeof(struct opaque_param_header) + sizeof(struct hist_buffer_info);
    // Configure both stage 1 and stage 2 confidence levels
    stage1_cnf_level = get_first_stage_confidence_level();
    stage2_cnf_level = get_second_stage_confidence_level();

  } else {
    // For all other clients
    kw_buffer_duration_in_ms = 0;
    generic_payload_size = 0;
    stage1_cnf_level = get_first_stage_confidence_level();
    stage2_cnf_level = 0;

    //Set Keyword Second Stage on ARM FALSE for other clients
    has_second_stage_ = false;
  }
  vendor_uuid = qc_uuid;
}

void pa_qst_session_data::log_session_data() {
  syslog(LOG_DEBUG, "Thread %x    ++pa_qst_session_data log_session_data \n",
      std::this_thread::get_id());
  syslog(LOG_DEBUG, "               num_kws  %d\n", num_kws);
  syslog(LOG_DEBUG, "               ses_handle  %d\n", ses_handle);
  syslog(LOG_DEBUG, "               observer  %x\n", observer_client.get());
  syslog(LOG_DEBUG, "               clientid  %x\n", clientid);
  syslog(LOG_DEBUG, "               sm_file_path  %s\n", sm_file_path.c_str());
  syslog(LOG_DEBUG, "               kw_buffer_duration_in_ms  %d\n",
      kw_buffer_duration_in_ms);
  syslog(LOG_DEBUG, "               Sound model loaded  %d\n", loaded);
  syslog(LOG_DEBUG, "               Session Started  %d\n", started);
  syslog(LOG_DEBUG, "               Session stopcapture  %d\n", stopcapture);
  syslog(LOG_DEBUG, "               Session DOA index  %d\n", target_doa_idx);
  syslog(LOG_DEBUG, "               Session Channel Index  %d\n",
      target_channel_idx);
  syslog(LOG_DEBUG, "               stage 1 confidence  %d\n",
      stage1_cnf_level);
  syslog(LOG_DEBUG, "               stage 2 confidence  %d\n",
      stage2_cnf_level);
  syslog(LOG_DEBUG, "             --pa_qst_session_data log_session_data \n");
}

//
// SingleTon Getter Function
//

std::shared_ptr<QSTHWClient> QSTHWClient::Get() {
  getlck.lock();
  std::shared_ptr<QSTHWClient> qsthwClient_ptr = qsthwClient_ptr_.lock();
  if (!qsthwClient_ptr) {
    qsthwClient_ptr.reset(new QSTHWClient());
    qsthwClient_ptr_ = qsthwClient_ptr;
    // TODO:Initialize to fetch database settings instead of having that in
    // constructor
  }
  getlck.unlock();
  return std::shared_ptr<QSTHWClient>(qsthwClient_ptr_);
}
//
// Constructor
//

QSTHWClient::QSTHWClient()
    : pa_qst_handle(nullptr) {
  syslog(LOG_DEBUG, "Thread %x    +++++++QSTHWClient Constructor called ",
      std::this_thread::get_id());
  // Load the database with keys required for prospective use of wakeword engine
  // by the clients
  LoadWakeWordDatabase();
}

//
// Destructor
//

QSTHWClient::~QSTHWClient() {
  // TODO: Debug case where client calls add_observer but doesnt call remove
  // observer

  syslog(LOG_DEBUG, "Thread %x------QSTHWClient Destructor called size %d ",
      std::this_thread::get_id(), client_data_map_.size());
  // If observer clients die without calling remove obeserver
  // deinitialise STHAL
  if (client_data_map_.empty()) {
    syslog(LOG_DEBUG,
        "Thread %x------QSTHWClient Destructor Client Datamap is empty ",
        std::this_thread::get_id());
  }
  for (auto &iter : client_data_map_) {
    syslog(LOG_INFO,
        "Thread %x ------QSTHWClient Destructor client_data_map_ here %d",
        std::this_thread::get_id(), iter.first);
    SessionDataPtr session = std::move(client_data_map_.at(iter.first));
    if (session->started) {
      syslog(LOG_INFO,
          "Thread %x ------QSTHWClient Destructor Stop Regonition %d",
          std::this_thread::get_id(), iter.first);
      pa_qst_stop_recognition(pa_qst_handle, session->ses_handle);
    }
    if (session->loaded) {
      syslog(LOG_INFO,
          "Thread %x ------QSTHWClient Destructor Unload Sound Model %d",
          std::this_thread::get_id(), iter.first);
      pa_qst_unload_sound_model(pa_qst_handle, session->ses_handle);
    }
  }

  if (pa_qst_handle != nullptr) {
    syslog(LOG_INFO, "Thread %x ------QSTHWClient Destructor Deinit STHAL",
        std::this_thread::get_id());
    pa_qst_deinit(pa_qst_handle);
  }
}

//
// Private Member Functions
//

// Initialises the STHAL when there is atleast one client using the wakeword
// Returns
// 0 when already intilized or initialisation is successfull
// negative when intialisation fails
// pa_process_initialize Initilises STHAL (should be called only once)

int QSTHWClient::pa_process_initialize() {
  syslog(LOG_DEBUG, "Thread %x    ++QSTHWClient::pa_process_initialize()",
      std::this_thread::get_id());
  int status = 0;

  if (pa_qst_handle != nullptr) {
    // Reinitialise STHAL, voice-ui-framework had shut down abruptly or
    // deintialisation was not dont properly
    syslog(LOG_DEBUG,
        "QSTHWClient::pa_process_initialize() Already initialised -- "
        "deintialising");
    if (status = pa_qst_deinit(pa_qst_handle)) {
      return status;
    }
    // Continue processing initialisation when deinitilalisation is successfull
  }

  pa_qst_handle = pa_qst_init(PA_QST_MODULE_ID_PRIMARY);

  if (nullptr == pa_qst_handle) {
    syslog(LOG_ERR, "QSTHWClient:pa_qst_init() failed\n");
    status = -EINVAL;
  }

  // Fetch global params if any
  // Apply
  //_qst_set_parameters(pa_qst_handle, global_ses_handle, param);
  syslog(LOG_DEBUG, "--QSTHWClient::pa_process_initialize() -- Exit");

  return status;
}

// Deinitialises STHAL
// Returns 0 on success
int QSTHWClient::pa_process_deinitialize() {
  syslog(LOG_DEBUG, "Thread %x    ++QSTHWClient::pa_process_deinitialize()",
      std::this_thread::get_id());
  int status = 0;

  if (pa_qst_handle == nullptr) {
    syslog(LOG_DEBUG, "pa_qst_deinit(): Already Deinitialised\n");
    return status;
  }

  status = pa_qst_deinit(pa_qst_handle);
  if (status) {
    syslog(LOG_ERR, "pa_qst_deinit() failed\n");
    status = -EINVAL;
  } else {
    pa_qst_handle = nullptr;
  }
  syslog(LOG_DEBUG, "--QSTHWClient::pa_process_deinitialize()");
  return status;
}

// Opens the sound model file and loads it
bool QSTHWClient::pa_process_add_observer(SessionDataPtr &session_data) {
  bool ret_code = false;
  int sm_data_size = 0;
  int opaque_data_size = 0;

  bool capture_requested = true;
  struct keyword_buffer_config kb_config;

  syslog(LOG_DEBUG, "Thread %x    ++QSTHWClient::pa_process_add_observer()",
      std::this_thread::get_id());

  auto fp = fopen(session_data->sm_file_path.c_str(), "rb");
  if (fp == NULL) {
    syslog(LOG_INFO,
        "QSTHWClient::pa_process_add_observer:Could not open sound model "
        "file : %s\n",
        session_data->sm_file_path.c_str());
    return ret_code;
  }
  /* Get the sound mode size i.e. file size */
  fseek(fp, 0, SEEK_END);
  sm_data_size = ftell(fp);
  fseek(fp, 0, SEEK_SET);

  auto sound_model_size =
      sizeof(struct sound_trigger_phrase_sound_model) + sm_data_size;
  auto sound_model =
      (struct sound_trigger_phrase_sound_model *)calloc(1, sound_model_size);
  if (sound_model == NULL) {
    syslog(LOG_ERR,
        "QSTHWClient::pa_process_add_observer:Could not allocate memory for "
        "sound model");
    if (fp)
      fclose(fp);
    return ret_code;
  }

  sound_model->common.type = SOUND_MODEL_TYPE_KEYPHRASE;
  sound_model->common.data_size = sm_data_size;
  sound_model->common.data_offset = sizeof(*sound_model);
  sound_model->num_phrases = session_data->num_kws;
  for (auto i = 0; i < session_data->num_kws; i++) {
    sound_model->phrases[i].num_users = 0;
    sound_model->phrases[i].recognition_mode = RECOGNITION_MODE_VOICE_TRIGGER;
  }

  int bytes_read = fread((char *)sound_model + sound_model->common.data_offset,
      1, sm_data_size, fp);
  syslog(LOG_INFO,
      "QSTHWClient::pa_process_add_observer: bytes from the file %d\n",
      bytes_read);
  if (bytes_read != sm_data_size) {
    syslog(LOG_INFO,
        "QSTHWClient::pa_process_add_observer: Something wrong while "
        "reading data from file: bytes_read %d file_size %d",
        bytes_read, sm_data_size);
    if (fp)
      fclose(fp);
    if (sound_model)
      free(sound_model);
    return ret_code;
  }

  memcpy(&sound_model->common.vendor_uuid, &session_data->vendor_uuid,
      sizeof(sound_trigger_uuid_t));
  syslog(LOG_INFO,
      "QSTHWClient::pa_process_add_observer:sound model data_size %d "
      "data_offset %d\n",
      sm_data_size, sound_model->common.data_offset);

  // Returns non-zerovalue on failure
  auto status = pa_qst_load_sound_model(pa_qst_handle, &sound_model->common,
      NULL, NULL, &session_data->ses_handle);
  if (status) {
    syslog(LOG_ERR,
        "QSTHWClient::pa_process_add_observer:load_sound_model failed\n");
    if (fp)
      fclose(fp);
    if (sound_model)
      free(sound_model);
    return ret_code;
  }
  session_data->loaded = true;

  opaque_data_size = session_data->generic_payload_size;

  syslog(LOG_DEBUG, "opaque data size %d", opaque_data_size);

  // Any other case do not specify  opaque data size

  auto rc_config_size =
      sizeof(struct sound_trigger_recognition_config) + opaque_data_size;
  session_data->rc_config =
      (struct sound_trigger_recognition_config *)calloc(1, rc_config_size);
  if (session_data->rc_config == NULL) {
    syslog(LOG_INFO,
        "Could not allocate memory for sm data recognition config");
    status = pa_qst_unload_sound_model(pa_qst_handle, session_data->ses_handle);
    if (status) {
      syslog(LOG_ERR, "unload_sound_model failed\n");
    }
    session_data->loaded = false;
    //Close the file ptr and free up sound model, post loading the sound model
    if (fp)
      fclose(fp);
    if (sound_model)
      free(sound_model);
    return ret_code;
  }

  // Prepare RC config to be passed in pa_start_recognition
  auto rc_config = session_data->rc_config;
  rc_config->capture_handle = AUDIO_IO_HANDLE_NONE;
  rc_config->capture_device = AUDIO_DEVICE_NONE;
  rc_config->capture_requested = capture_requested;
  rc_config->num_phrases = session_data->num_kws;

  // ADD to fix crash due to non initialized members.
  rc_config->data_size = opaque_data_size;
  rc_config->data_offset =
      opaque_data_size ? sizeof(struct sound_trigger_recognition_config) : 0;

  if (session_data->generic_payload_size != 0) {
    uint8_t *opaque_ptr = NULL;
    int i = 0;
    struct opaque_param_header *param_hdr = NULL;
    struct hist_buffer_info *hist_buf = NULL;
    opaque_ptr = (uint8_t *)rc_config + rc_config->data_offset;
    param_hdr = (struct opaque_param_header *)opaque_ptr;

    // we not have the config structure for keyword indices
    // Assuming that Keyword indices are enabled by default
    // Configure alexa pre-roll duration to 500ms
    param_hdr->key_id = PARAM_KEY_CONFIDENCE_LEVELS;
    param_hdr->payload_size = sizeof(struct confidence_levels_info);

    auto conf_levels =
        (struct confidence_levels_info *)(opaque_ptr + sizeof(struct opaque_param_header));
    conf_levels->version = SM_MINOR_VERSION;
    conf_levels->num_sound_models = 1;
    auto sm_levels =
        (struct sound_model_conf_levels *)&conf_levels->conf_levels[i];
    sm_levels->sm_id = SM_ID_SVA_GMM;
    sm_levels->num_kw_levels = session_data->num_kws;
    for (auto j = 0; j < sm_levels->num_kw_levels; j++) {
      sm_levels->kw_levels[j].kw_level = session_data->stage1_cnf_level;
      // For Voice Print, Ignore
      sm_levels->kw_levels[j].num_user_levels = 0;
    }
    // CNN stitched Sound models

    //CONFIGURE IF SECOND STAGE IS AVAILABE OR NOT TODO:: generic_payload_size needs to be ZERO for stage 1 only
    if (!session_data->has_second_stage_) {
      conf_levels->num_sound_models++;
      i++;
      sm_levels = (struct sound_model_conf_levels *)&conf_levels->conf_levels[i];
      sm_levels->sm_id = SM_ID_SVA_CNN;
      sm_levels->num_kw_levels = session_data->num_kws;
      sm_levels->kw_levels[0].kw_level = session_data->stage2_cnf_level;
    }

    opaque_ptr += sizeof(struct opaque_param_header) + sizeof(struct confidence_levels_info);

    param_hdr = (struct opaque_param_header *)opaque_ptr;
    param_hdr->key_id = PARAM_KEY_HISTORY_BUFFER_CONFIG;
    param_hdr->payload_size = sizeof(struct hist_buffer_info);
    hist_buf = (struct hist_buffer_info *)(opaque_ptr + sizeof(struct opaque_param_header));
    hist_buf->version = SM_MINOR_VERSION_2;
    hist_buf->hist_buffer_duration_msec =
        session_data->kw_buffer_duration_in_ms;

    // One second of pre-roll data TBD: User config
    hist_buf->pre_roll_duration_msec = 500;
  }

  for (auto i = 0; i < session_data->num_kws; i++) {
    rc_config->phrases[i].id = i;
    rc_config->phrases[i].confidence_level = session_data->stage1_cnf_level;
    rc_config->phrases[i].num_levels = 0;
    rc_config->phrases[i].recognition_modes = RECOGNITION_MODE_VOICE_TRIGGER;
  }
  syslog(LOG_INFO,
      "QSTHWClient::pa_process_add_observer:session params %p, %d\n",
      rc_config, session_data->ses_handle);
  ret_code = true;

  // Signal addition to the oserver-client for follow up steps
  session_data->observer_client->ObserverAdded();

  syslog(LOG_DEBUG, "--QSTHWClient::pa_process_add_observer()");
  session_data->log_session_data();

  //Close the file ptr and free up sound model, post loading the sound model
  if (fp)
    fclose(fp);
  if (sound_model)
    free(sound_model);
  return ret_code;
}
void QSTHWClient::pa_process_detection_event(SessionDataPtr &session_data,
    KwdDetectionResults &kwd_result) {
  // Scope of these declarations is only this function
  enum det_event_keys { KWD_CONFIDENCE_LEVEL = 0x0,
    KWD_INDEX,
    KWD_MAX };

  int i, j, k, user_id;
  void *payload;
  char *payload_8;
  uint32_t *payload_32;
  uint32_t version, key, key_version, key_size;
  struct pa_qst_phrase_recognition_event *event = session_data->pa_qst_event;
  struct sound_trigger_phrase_recognition_event phrase_event;

  syslog(LOG_DEBUG, "Thread %x    ++QSTHWClient::pa_process_detection_event()",
      std::this_thread::get_id());
  syslog(LOG_INFO,
      "QSTHWClient::pa_process_detection_event():offset %d, size %d\n",
      event->phrase_event.common.data_offset,
      event->phrase_event.common.data_size);
  size_t payload_size = event->phrase_event.common.data_size;
  payload = calloc(1, payload_size);
  if (!payload) {
    syslog(LOG_ERR,
        "QSTHWClient::pa_process_detection_event():Memory allocation for "
        "payload failed\n");
    return;
  }
  memcpy(payload, ((char *)event) + event->phrase_event.common.data_offset,
      event->phrase_event.common.data_size);
  payload_8 = (char *)payload;
  payload_32 = (uint32_t *)payload;
  syslog(
      LOG_INFO,
      "QSTHWClient::pa_process_detection_event():detection event status %d\n",
      event->phrase_event.common.status);
  syslog(LOG_INFO,
      "QSTHWClient::pa_process_detection_event():: detection event "
      "timestamp %ld\n",
      event->timestamp);

  // Populate default kwd_result
  kwd_result.confidence_level_ = -1;
  kwd_result.keyword_start_index_ = -1;
  kwd_result.keyword_end_index_ = -1;
  kwd_result.detection_count_ = ++session_data->counter;
  kwd_result.keyword = "";

  syslog(LOG_INFO,
      "QSTHWClient::pa_process_detection_event():: %s detection count %d ", ClientIDToString(session_data->clientid).c_str(),
      kwd_result.detection_count_);

  phrase_event = event->phrase_event;
  if (session_data->generic_payload_size != 0) {
    // Case VT Alexa SM running on  ADSP
    // LSM/GCS usecase: Generic detection payload with key_id based fields
    size_t count = 0;
    struct opaque_param_header *param_hdr = NULL;
    while (count < payload_size) {
      param_hdr = (struct opaque_param_header *)(payload_8);
      switch (param_hdr->key_id) {
        case PARAM_KEY_CONFIDENCE_LEVELS: {
          if (param_hdr->payload_size != sizeof(struct confidence_levels_info)) {
            if (payload)
              free(payload);
            return;
          }
          payload_8 += sizeof(struct opaque_param_header);
          auto conf_levels = (struct confidence_levels_info *)(payload_8);
          for (i = 0; i < conf_levels->num_sound_models; i++) {
            if (conf_levels->conf_levels[i].sm_id == SM_ID_SVA_GMM) {
              for (j = 0; j < conf_levels->conf_levels[i].num_kw_levels; j++) {
                syslog(LOG_INFO, "%s: kw [%d] level %d\n", __func__, j,
                    conf_levels->conf_levels[i].kw_levels[j].kw_level);
                kwd_result.confidence_level_ =
                    conf_levels->conf_levels[i].kw_levels[j].kw_level;

                for (k = 0;
                     k < conf_levels->conf_levels[i].kw_levels[j].num_user_levels;
                     k++) {
                  syslog(LOG_INFO, "%s: user [%d] level %d\n", __func__,
                      conf_levels->conf_levels[i]
                          .kw_levels[j]
                          .user_levels[k]
                          .user_id,
                      conf_levels->conf_levels[i]
                          .kw_levels[j]
                          .user_levels[k]
                          .level);
                }
              }
            }
            // Note that confidence level reported by ADSP on detection event is just one combined for stage1 and stage2
            // Just to maintain partiy with test app, the below code is present
            if (conf_levels->conf_levels[i].sm_id == SM_ID_SVA_CNN) {
              syslog(LOG_INFO, "%s: cnn kw level %d\n", __func__,
                  conf_levels->conf_levels[i].kw_levels[0].kw_level);
            }

            if (conf_levels->conf_levels[i].sm_id == SM_ID_SVA_VOP) {
              syslog(LOG_INFO, "%s: vop user level %d\n", __func__,
                  conf_levels->conf_levels[i]
                      .kw_levels[0]
                      .user_levels[0]
                      .level);
            }
          }
          payload_8 += sizeof(struct confidence_levels_info);
          count += sizeof(struct opaque_param_header) + sizeof(struct confidence_levels_info);
        } break;

        case PARAM_KEY_KEYWORD_INDICES: {
          struct keyword_indices_info *kw_indices;
          if (param_hdr->payload_size != sizeof(struct keyword_indices_info)) {
            syslog(LOG_INFO, "%s: Invalid payload size of keyword indices\n",
                __func__);
            if (payload)
              free(payload);
            return;
          }
          payload_8 += sizeof(struct opaque_param_header);
          kw_indices = (struct keyword_indices_info *)(payload_8);
          syslog(LOG_INFO,
              "%s: kw_indces version %d, start index %d, end index %d\n",
              __func__, kw_indices->version, kw_indices->start_index,
              kw_indices->end_index);

          // Populate Indices to send back to client
          kwd_result.keyword_start_index_ = kw_indices->start_index;
          kwd_result.keyword_end_index_ = kw_indices->end_index;

          payload_8 += sizeof(struct keyword_indices_info);
          count += sizeof(struct opaque_param_header) + sizeof(struct keyword_indices_info);
        } break;

        case PARAM_KEY_TIMESTAMP: {
          if (param_hdr->payload_size != sizeof(struct timestamp_info)) {
            syslog(LOG_INFO, "%s: Invalid payload size of timestamp info\n",
                __func__);
            if (payload)
              free(payload);
            return;
          }
          payload_8 += sizeof(struct opaque_param_header);
          auto ts_info = (struct timestamp_info *)(payload_8);
          syslog(LOG_INFO,
              "%s: timestamp info version %d, 1st stage ts %ld, 2nd stage "
              "ts %ld\n",
              __func__, ts_info->version,
              ts_info->first_stage_det_event_timestamp,
              ts_info->second_stage_det_event_timestamp);

          payload_8 += sizeof(struct timestamp_info);
          count += sizeof(struct opaque_param_header) + sizeof(struct timestamp_info);
        } break;
        default: {
          syslog(LOG_INFO, "%s: Invalid param id %d\n", __func__,
              param_hdr->key_id);
          if (payload)
            free(payload);
          return;
        }
      }
    }
  } else {
    // Case APQ 8017 Alexa SM running on ADSP
    // Note that we would not get keyword indices in this case
    for (i = 0; i < phrase_event.num_phrases; i++) {
      syslog(LOG_INFO, "%s: [%d] kw_id %d level %d\n", __func__, i,
          phrase_event.phrase_extras[i].id, ((char *)payload)[i]);
      syslog(LOG_INFO, "%s: Num levels %d\n", __func__,
          phrase_event.phrase_extras[i].num_levels);
      for (j = 0; j < phrase_event.phrase_extras[i].num_levels; j++) {
        user_id = phrase_event.phrase_extras[i].levels[j].user_id;
        syslog(LOG_INFO, "%s: [%d] user_id %d level %d\n", __func__, i, user_id,
            ((char *)payload)[user_id]);
      }
    }
  }
  if (payload) {
    free(payload);
  }

  syslog(LOG_DEBUG, "--QSTHWClient::pa_process_detection_event()");
}

bool QSTHWClient::Feed(const std::vector<int> in) {
  syslog(LOG_ERR, "No implementation");
  return true;
}

// Capture Lab data
void QSTHWClient::pa_capture_lab_data(SessionDataPtr &session_data) {
  int ret = 0;
  void *buffer;
  unsigned int period_size;
  size_t bytes, written;
  std::string capture_file;
  size_t cur_bytes_read = 0;
  FILE *fp;
  struct sound_trigger_phrase_recognition_event phrase_event =
      session_data->pa_qst_event->phrase_event;
  pa_qst_ses_handle_t ses_handle = phrase_event.common.model;

  //     Get the audio_config
  audio_config_t *audio_config = &phrase_event.common.audio_config;

  uint32_t sample_rate = audio_config->sample_rate;
  uint32_t channels =
      audio_channel_count_from_in_mask(audio_config->channel_mask);
  audio_format_t format = audio_config->format;
  size_t samp_sz = audio_bytes_per_sample(format);
  struct stat st = {0};

  syslog(LOG_DEBUG, "Thread %x    ++QSTHWClient::pa_capture_lab_data()",
      std::this_thread::get_id());

  bytes = pa_qst_get_buffer_size(pa_qst_handle, ses_handle);
  if (bytes <= 0) {
    syslog(LOG_ERR, "Invalid buffer size returned\n");
    return;
  }
  // Calculate the buffer size as per the audio config and duration of capture
  // set (total bytes to read)
  /* total bytes to read = bytes to read per sec * duration where
   * audio configuration passed as part of recognition event is
   * used to obtain bytes per sec.
   */

  syslog(LOG_INFO, "rate %d, channels %d, samp sz %d, duration %d\n",
      sample_rate, channels, samp_sz);

  buffer = calloc(1, bytes);
  if (buffer == NULL) {
    syslog(LOG_INFO, "Could not allocate memory for capture buffer\n");
    return;
  }

  // while all bytes are read capture and send lab data to obsever client
  while (session_data->stopcapture != true) {
    size_t bytes_read = pa_qst_read_buffer(pa_qst_handle, ses_handle, (unsigned char *)buffer,
        bytes);

    if (bytes_read > 0) {
      //WWE Stage 2 on ARM
      if (session_data->has_second_stage_) {
        session_data->wwe_stage2_client_->PumdAudio(buffer, bytes);
      } else {
        // Pass the buffer to the observer client
        // Note Observer client copies the buffer (doesnt free/propagate it)
        session_data->observer_client->OnNewVoiceBufferReceived(buffer, bytes);
      }
      cur_bytes_read += bytes;
      memset(buffer, 0, bytes);
    }
  }

  syslog(LOG_INFO, "bytes read %d\n", cur_bytes_read);
  pa_qst_stop_buffering(pa_qst_handle, ses_handle);
  free(buffer);

  syslog(LOG_DEBUG, "--QSTHWClient::pa_capture_lab_data()");
}

// Paramters pointers to observer client session data and pa_qst_event
void QSTHWClient::pa_event_handler_thread(int index) {
  uint8_t *buffer;
  size_t size;

  auto pQsthwClient = QSTHWClient::Get();
  SessionDataPtr &session_data = pQsthwClient->client_data_map_.at(index);

  syslog(LOG_DEBUG, "Thread %x    ++QSTHWClient::pa_event_handler_thread()",
      std::this_thread::get_id());
  if (!session_data.get()) {
    syslog(LOG_INFO, "Error: context is null\n");
    return;
  }

  // Process the detection event
  pa_qst_ses_handle_t ses_handle = session_data->ses_handle;
  syslog(LOG_INFO, "[%d] session params %p\n", ses_handle,
      session_data->rc_config);
  if (session_data->pa_qst_event) {
    KwdDetectionResults res;
    pa_process_detection_event(session_data, res);

    if (session_data->has_second_stage_) {
      //start WWE second Stage on ARM session
      session_data->wwe_stage2_client_->start(res);
    } else {
      // Signal Keyword detection to the clients
      session_data->observer_client->OnKeywordDetected(res);
    }

    if (session_data->pa_qst_event->phrase_event.common.capture_available) {
      syslog(LOG_INFO, "capture LAB data\n");
      pa_capture_lab_data(session_data);
    }
    free(session_data->pa_qst_event);
    session_data->pa_qst_event = NULL;
  }

  // Start recognition on the current client
  if (!pQsthwClient->pa_process_start_recognition(
          pQsthwClient->client_data_map_.at(index))) {
    syslog(LOG_ERR,
        "QSTHWClient::StartRecognition - Unable to Start Recognition");
    return;
  }

  // Restart recogition on all the clients stopped
  auto it = pQsthwClient->global_wakeword_params.find(voiceUIUtils::Keys::k_framework_client_concurrency_);
  if ((it == pQsthwClient->global_wakeword_params.end()) || (it->second == "false")) {
    // If no concurrency, wait for pa_event_handler_thread to get finished
    // TODO: Analyse need of  wakeword_mutex_here as we are using client_data_

    syslog(LOG_INFO,
        "QSTHWClient::pa_event_handler_thread - Restarting  recognition on "
        "all other clients(orignally started");
    for (auto &iter : pQsthwClient->client_data_map_) {
      // Skip the current client
      if (iter.first == index) {
        continue;
      }
      // Restart Recognition on Clients that were ever started
      if (pQsthwClient->client_data_map_.at(iter.first)->started) {
        if (!pQsthwClient->pa_process_start_recognition(
                pQsthwClient->client_data_map_.at(iter.first))) {
          syslog(LOG_ERR,
              "QSTHWClient::StartRecognition - Unable to Start Recognition");
          return;
        }
      }
    }
  }

  syslog(LOG_DEBUG, "--QSTHWClient::pa_event_handler_thread()");
  return;
}

void QSTHWClient::pa_event_callback(
    struct sound_trigger_recognition_event *event, void *sessionHndl) {
  // This is the call back from STHAL
  // Find the observer client associated with this session handle

  // auto pQsthwClient = (QSTHWClient *)sessionHndl;
  auto pQsthwClient = QSTHWClient::Get();
  struct pa_qst_phrase_recognition_event *pa_qst_event;

  VoiceUIClientID clientid;

  syslog(LOG_DEBUG, "Thread %x    ++QSTHWClient::pa_event_callback()",
      std::this_thread::get_id());

  if (!pQsthwClient->GetClientIdFromHdl(event->model, clientid)) {
    syslog(LOG_ERR,
        "QSTHWClient::pa_event_callback No session data for handle %d\n",
        event->model);
    return;
  }

  auto index = ClientIDToIndex(clientid);

  syslog(LOG_INFO, "QSTHWClient::pa_event_callback - Index %d Client: %s",
      index, ClientIDToString(clientid).c_str());

  auto client_data_ = pQsthwClient->client_data_map_.find(index);
  if (client_data_ == pQsthwClient->client_data_map_.end()) {
    syslog(LOG_ERR,
        "QSTHWClient::pa_event_callback - %s Client not added(sound model "
        "is not loaded)",
        ClientIDToString(clientid).c_str());
    return;
  }

  // Section Auto Lock; this lock prevents multiple callbacks from STHAL
  std::lock_guard<std::mutex> lock(
      pQsthwClient->client_data_map_.at(index)->pa_event_mutex_);

  // Ignore the event if there is a pending event getting processed
  if (pQsthwClient->client_data_map_.at(index)->pa_qst_event != NULL) {
    syslog(LOG_ERR, "QSTHWClient::pa_event_callback - Client: %s is busy",
        ClientIDToString(clientid).c_str());
    return;
  }

  // capture the pa_qst_event
  if (pQsthwClient->client_data_map_.at(index)
          ->pa_event_cb_thread_.joinable()) {
    pQsthwClient->client_data_map_.at(index)->pa_event_cb_thread_.join();
    syslog(LOG_INFO,
        "QSTHWClient::pa_event_callback - Joining a previous callback "
        "thread of %s",
        ClientIDToString(clientid).c_str());
  }

  // Before parsign the event on one client, stop recognition on all clients
  // active based on k_framework_client_concurrency_

  auto it = pQsthwClient->global_wakeword_params.find(voiceUIUtils::Keys::k_framework_client_concurrency_);
  if (it == pQsthwClient->global_wakeword_params.end() || it->second == "false") {
    syslog(LOG_INFO,
        "QSTHWClient::pa_event_callback - Stopping recognition on all other "
        "clients(orignally started");

    // TODO: Use enum iterators instead of using the class names

    // For the all the client every started
    // If Wakeword concurrency is disabled
    // Stop recognition when there is a KWD detection on other client

    for (auto &iter : pQsthwClient->client_data_map_) {
      // Restart Recognition on Clients that were ever started

      // Skip sending stop to current client
      if (iter.first == index)
        continue;

      if (clientid != pQsthwClient->client_data_map_.at(iter.first)->clientid) {
        if (pQsthwClient->client_data_map_.at(iter.first)->started) {
          if (!pQsthwClient->pa_process_stop_recognition(
                  pQsthwClient->client_data_map_.at(iter.first))) {
            syslog(
                LOG_INFO,
                "QSTHWClient::StartRecognition - Unable to Start Recognition");
            return;
          }
        }
      }
    }
  }

  // Parse the event received
  pa_qst_event = (struct pa_qst_phrase_recognition_event *)event;
  auto event_timestamp = pa_qst_event->timestamp;
  syslog(LOG_INFO, "[%d] Event detection timestamp %llu\n", event->model,
      event_timestamp);
  auto event_size = sizeof(struct pa_qst_phrase_recognition_event) + pa_qst_event->phrase_event.common.data_size;

  pQsthwClient->client_data_map_.at(index)->pa_qst_event =
      (struct pa_qst_phrase_recognition_event *)calloc(1, event_size);
  if (pQsthwClient->client_data_map_.at(index)->pa_qst_event == NULL) {
    syslog(LOG_INFO, "Could not allocate memory for sm data recognition event");
    return;
  }

  memcpy(pQsthwClient->client_data_map_.at(index)->pa_qst_event, pa_qst_event,
      sizeof(struct pa_qst_phrase_recognition_event));
  memcpy((char *)pQsthwClient->client_data_map_.at(index)->pa_qst_event + pQsthwClient->client_data_map_.at(index)->pa_qst_event->phrase_event.common.data_offset,
      (char *)pa_qst_event + pa_qst_event->phrase_event.common.data_offset,
      pa_qst_event->phrase_event.common.data_size);

  // THREAD OUT TO pa_event_handler_thread to capture lab data

  pQsthwClient->client_data_map_.at(index)->pa_event_cb_thread_ =
      std::move(std::thread([=]() {
        syslog(
            LOG_INFO,
            "QSTHWClient::pa_event_callback - Thread out to handle pa_event");
        pQsthwClient->pa_event_handler_thread(index);
      }));

  // Handle failure of creation of threads

  syslog(LOG_DEBUG, "--QSTHWClient::pa_event_callback()");
}

bool QSTHWClient::pa_process_start_recognition(SessionDataPtr &session_data) {
  syslog(LOG_DEBUG,
      "Thread %x    ++QSTHWClient::pa_process_start_recognition()",
      std::this_thread::get_id());
  syslog(LOG_INFO, "session state %d\n", session_data->started);

  // Reset
  session_data->stopcapture = false;

  // Register a PA Event Callback
  auto status = pa_qst_start_recognition(
      pa_qst_handle, session_data->ses_handle, session_data->rc_config,
      pa_event_callback, (void *)this);

  if (status) {
    syslog(LOG_ERR, "pa_qst_start_recognition start_recognition failed\n");
    return false;
  }

  syslog(LOG_DEBUG, "--QSTHWClient::pa_process_start_recognition()");
  return true;
}

bool QSTHWClient::pa_process_get_data(SoundTriggerData type,
    SessionDataPtr &session_data) {
  syslog(LOG_DEBUG, "Thread %x    ++QSTHWClient::pa_process_get_data()",
      std::this_thread::get_id());

  pa_qst_get_param_payload_t payload;
  size_t payload_size = sizeof(pa_qst_get_param_payload_t);
  size_t param_data_size;

  auto status =
      pa_qst_get_param_data(pa_qst_handle, session_data->ses_handle,
          SoundTriggerDataToString(type).c_str(),
          (void *)&payload, payload_size, &param_data_size);

  if (status < 0) {
    syslog(LOG_ERR,
        "QSTHWClient::pa_process_get_data():: Failed to Get Parameter %s",
        SoundTriggerDataToString(type).c_str());
    return false;
  } else {
    switch (type) {
      case SoundTriggerData::DOA: {
        struct pa_qst_source_tracking_param st_params = payload.st_params;
        if (param_data_size != sizeof(struct pa_qst_source_tracking_param)) {
          syslog(LOG_DEBUG, "%s: ERROR. Invalid param data size returned %d\n",
              __func__, param_data_size);
          return false;
        }
        syslog(LOG_DEBUG, "%s: target angle boundaries [%d, %d]\n", __func__,
            st_params.target_angle_L16[0], st_params.target_angle_L16[1]);
        syslog(LOG_DEBUG, "%s: interference angle boundaries [%d, %d]\n",
            __func__, st_params.interf_angle_L16[0],
            st_params.interf_angle_L16[1]);

        session_data->target_doa_idx = st_params.target_angle_L16[0];
      } break;
      case SoundTriggerData::CHANNEL_INDEX: {
        syslog(LOG_DEBUG, "%s: processing channel index params\n", __func__);
        struct pa_qst_target_channel_index_param ch_index_params =
            payload.ch_index_params;
        if (param_data_size != sizeof(struct pa_qst_target_channel_index_param)) {
          syslog(LOG_DEBUG, "%s: ERROR. Invalid param data size returned %d\n",
              __func__, param_data_size);
          return false;
        }
        syslog(LOG_DEBUG, "%s: target channel index - [%d]\n", __func__,
            ch_index_params.target_chan_idx);

        session_data->target_channel_idx = ch_index_params.target_chan_idx;
      } break;
      default:
        break;
    }
  }
  syslog(LOG_DEBUG, "--QSTHWClient::pa_process_get_data()");
  return true;
}

bool QSTHWClient::pa_process_stop_recognition(SessionDataPtr &session_data) {
  syslog(LOG_DEBUG, "Thread %x    ++QSTHWClient::pa_process_stop_recognition()",
      std::this_thread::get_id());
  syslog(LOG_INFO, "session state %d\n", session_data->started);

  auto status =
      pa_qst_stop_recognition(pa_qst_handle, session_data->ses_handle);
  if (status) {
    syslog(LOG_ERR, "stop_recognition failed\n");
    return false;
  }
  session_data->observer_client->StopRecognitionComplete();
  syslog(LOG_DEBUG, "--QSTHWClient::pa_process_stop_recognition()");
  return true;
}

// Removes the sound model corresponding to the pa session data
// Stops recognition
// Unloads the corresponding sound model
// Notifies the observer client on its removal
// Returns always true TODO: false cases?

bool QSTHWClient::pa_process_remove_observer(SessionDataPtr &session_data) {
  syslog(LOG_DEBUG, "Thread %x    ++QSTHWClient::pa_process_remove_observer()",
      std::this_thread::get_id());
  if (session_data->started) {
    auto status =
        pa_qst_stop_recognition(pa_qst_handle, session_data->ses_handle);
    if (status)
      syslog(LOG_ERR, "stop_recognition failed");
    session_data->started = false;
  }
  if (session_data->loaded) {
    auto status =
        pa_qst_unload_sound_model(pa_qst_handle, session_data->ses_handle);
    if (status)
      syslog(LOG_ERR, "unload_sound_model failed\n");
    session_data->loaded = false;
  }

  // Free up the rc_config on removing the observer
  if (session_data->rc_config) {
    free(session_data->rc_config);
    session_data->rc_config = NULL;
  }
  // Notify the observer client on removal
  session_data->observer_client->ObserverRemoved();
  syslog(LOG_DEBUG, "--QSTHWClient::pa_process_remove_observer()");
  return true;
}

bool QSTHWClient::pa_process_set_global_params(std::string kv_pair) {
  syslog(LOG_INFO, "%xQSTHWClient::pa_process_set_global_params",
      std::this_thread::get_id());

  const char *param = handle_set_parameters(kv_pair.c_str());
  if (param == NULL)
    return false;
  syslog(LOG_INFO, "global param to set %s\n", param);
  pa_qst_ses_handle_t global_ses_handle = 0;
  auto status = pa_qst_set_parameters(pa_qst_handle, global_ses_handle, param);
  if (param)
    free((void *)param);
  if (status) {
    syslog(LOG_ERR, "QSTHWClient::pa_process_set_global_params Failed");
    return false;
  }
  syslog(LOG_DEBUG, "--QSTHWClient::pa_process_set_global_params()");
  return true;
}

// Database Getter Functions

// Central function, that refreshes reading from adk-config database
// In any case, (database present or not), populate default values
// For paramters that impact the wakeword

void QSTHWClient::LoadWakeWordDatabase() {
  // global_wakeword_params
  // Read multi-client-concurrency
  // This has to be picked up from qsap-config; Dummy For now
  auto voice_ui_config = voiceUIFramework::voiceUIUtils::VoiceUIConfig::getInstance();

  syslog(LOG_INFO, "Thread %x    ++QSTHWClient::LoadWakeWordDatabase",
      std::this_thread::get_id());
  QSTHWClient::global_wakeword_params.insert(
      std::pair<std::string, std::string>(voiceUIUtils::Keys::k_framework_client_concurrency_, "true"));

  QSTHWClient::global_wakeword_params.insert(
      std::pair<std::string, std::string>(
          voiceUIUtils::Keys::k_avs_sound_model_,
          voice_ui_config ? voice_ui_config->ReadWithDefault<std::string>("/data/voice-ui-framework/alexa_gmm_VT_up40.uim", voiceUIUtils::Keys::k_avs_sound_model_) : "/data/voice-ui-framework/alexa_gmm_VT_up40.uim"));

  // First Stage Confidence Level - AVS
  QSTHWClient::global_wakeword_params.insert(
      std::pair<std::string, std::string>(
          voiceUIUtils::Keys::k_avs_stage1_confidence_,
          voice_ui_config ? voice_ui_config->ReadWithDefault<std::string>("5", voiceUIUtils::Keys::k_avs_stage1_confidence_) : "5"));

  // Second Stage confidence level - AVS
  QSTHWClient::global_wakeword_params.insert(
      std::pair<std::string, std::string>(
          voiceUIUtils::Keys::k_avs_stage2_confidence_,
          voice_ui_config ? voice_ui_config->ReadWithDefault<std::string>("40", voiceUIUtils::Keys::k_avs_stage2_confidence_) : "40"));

  QSTHWClient::global_wakeword_params.insert(
      std::pair<std::string, std::string>(
          voiceUIUtils::Keys::k_modular_sound_model_,
          voice_ui_config ? voice_ui_config->ReadWithDefault<std::string>("/data/voice-ui-framework/HeySnapdragon.uim", voiceUIUtils::Keys::k_modular_sound_model_) : "/data/voice-ui-framework/HeySnapdragon.uim"));

  // First Stage Confidence Level - Modular
  QSTHWClient::global_wakeword_params.insert(
      std::pair<std::string, std::string>(
          voiceUIUtils::Keys::k_modular_stage1_confidence_,
          voice_ui_config ? voice_ui_config->ReadWithDefault<std::string>("2", voiceUIUtils::Keys::k_modular_stage1_confidence_) : "2"));

  // Second Stage confidence level - Modular
  QSTHWClient::global_wakeword_params.insert(
      std::pair<std::string, std::string>(
          voiceUIUtils::Keys::k_modular_stage2_confidence_,
          voice_ui_config ? voice_ui_config->ReadWithDefault<std::string>("42", voiceUIUtils::Keys::k_modular_stage2_confidence_) : "42"));

  //Print Database
  for (auto it = QSTHWClient::global_wakeword_params.begin(); it != QSTHWClient::global_wakeword_params.end(); ++it) {
    syslog(LOG_INFO, "QSTHWClient::LoadWakeWordDatabase: %s = %s \n", it->first.c_str(), it->second.c_str());
  }

  syslog(LOG_INFO, "--QSTHWClient::LoadWakeWordDatabase");
}

std::string pa_qst_session_data::get_soundmodel_path() {
  syslog(LOG_INFO, "Thread %x    ++pa_qst_session_data::get_soundmodel_path",
      std::this_thread::get_id());
  if (clientid == VoiceUIClientID::AVS_SOLUTION) {
    auto it =
        QSTHWClient::global_wakeword_params.find(voiceUIUtils::Keys::k_avs_sound_model_);
    return it->second;
  }
  if (clientid == VoiceUIClientID::MODULAR_SOLUTION) {
    auto it =
        QSTHWClient::global_wakeword_params.find(voiceUIUtils::Keys::k_modular_sound_model_);
    return it->second;
  }
  syslog(LOG_INFO, "--pa_qst_session_data::get_soundmodel_path");
  return std::string();
}

int pa_qst_session_data::get_kw_buffer_duration_in_ms() {
  syslog(LOG_INFO,
      "Thread %x    ++pa_qst_session_data::get_kw_buffer_duration_in_ms",
      std::this_thread::get_id());
  // Set Keyword buffer as per the client
  if (clientid == VoiceUIClientID::AVS_SOLUTION) {
    return 2000;
  }
  if (clientid == VoiceUIClientID::MODULAR_SOLUTION) {
    return 0;
  }
  syslog(LOG_INFO, "--pa_qst_session_data::get_kw_buffer_duration_in_ms");
  return 0;
}

int pa_qst_session_data::get_first_stage_confidence_level() {
  syslog(LOG_INFO,
      "Thread %x    ++pa_qst_session_data::get_first_stage_confidence_level",
      std::this_thread::get_id());
  // Set Keyword buffer as per the client
  if (clientid == VoiceUIClientID::AVS_SOLUTION) {
    auto it = QSTHWClient::global_wakeword_params.find(
        voiceUIUtils::Keys::k_avs_stage1_confidence_);
    if (it != QSTHWClient::global_wakeword_params.end())
      return std::stoi(it->second);
  } else if (clientid == VoiceUIClientID::MODULAR_SOLUTION) {
    auto it = QSTHWClient::global_wakeword_params.find(
        voiceUIUtils::Keys::k_modular_stage1_confidence_);
    if (it != QSTHWClient::global_wakeword_params.end())
      return std::stoi(it->second);
  }
  return 0;
}

int pa_qst_session_data::get_second_stage_confidence_level() {
  syslog(
      LOG_INFO,
      "Thread %x    ++pa_qst_session_data::get_second_stage_confidence_level",
      std::this_thread::get_id());
  // Set Keyword buffer as per the client
  if (clientid == VoiceUIClientID::AVS_SOLUTION) {
    auto it = QSTHWClient::global_wakeword_params.find(
        voiceUIUtils::Keys::k_avs_stage2_confidence_);
    if (it != QSTHWClient::global_wakeword_params.end())
      return std::stoi(it->second);
  }
  return 0;
}

//
// Public Member Functions
//

// Pick the sound model from default path/config database
bool QSTHWClient::AddObserver(
    VoiceUIClientID client_id,
    std::shared_ptr<WakeWordCallBackInterface> observer) {
  std::lock_guard<std::mutex> lock(wakeword_mutex_);

  bool ret = false;
  int index = ClientIDToIndex(client_id);

  syslog(LOG_INFO, "Thread %x    ++QSTHWClient::AddObserver - Client: %s",
      std::this_thread::get_id(), ClientIDToString(client_id).c_str());

  if (nullptr == observer) {
    syslog(LOG_INFO, "QSTHWClient::AddObserver - No Observer object",
        ClientIDToString(client_id).c_str());
    return ret;
  }

  // Initialize pa_qst for the first observer

  auto client_data_ = client_data_map_.find(index);
  if (client_data_ != client_data_map_.end()) {
    syslog(LOG_INFO, "QSTHWClient::AddObserver - %s Duplicate observer",
        ClientIDToString(client_id).c_str());
    return ret;
  }

  switch (client_id) {
    case VoiceUIClientID::MODULAR_SOLUTION:
    case VoiceUIClientID::AVS_SOLUTION: {
      // If the client data map is empty, pst is to be intilised
      if (client_data_map_.empty()) {
        if (pa_process_initialize()) {
          syslog(
              LOG_INFO,
              "QSTHWClient::AddObserver - Unable to initialise sound trigger",
              ClientIDToString(client_id).c_str());
          return ret;
        }
      }
      syslog(LOG_INFO, "QSTHWClient::AddObserver - %s Client Supported Index %d",
          ClientIDToString(client_id).c_str(), index);

      client_data_map_[index] =
          SessionDataPtr(new pa_qst_session_data_t(client_id, observer));

      // Returns std::future, TODO: Add code to handle futures
      auto future_func = std::async(std::launch::async, [&, index]() {
        if (!pa_process_add_observer(client_data_map_.at(index))) {
          syslog(LOG_ERR,
              "QSTHWClient::AddObserver - Unable to load Sound model");
          // Free the unique ptr
          client_data_map_.at(index).reset();
          client_data_map_.erase(index);
        }
      });
      // TODO: Uncomment to make this synchornous
      // if (api_syncronous_)
      // ret = future_func.get();
      // else
      if (client_data_map_[index] != nullptr) {
        if (client_data_map_[index]->has_second_stage_) {
          //Create WWE Second Stage on ARM
#ifdef BUILD_AVS_WWE
          client_data_map_[index]->wwe_stage2_client_ = std::make_shared<voiceUIFramework::wakeword::avs::AVSWWEStage2>();
          if (!client_data_map_[index]->wwe_stage2_client_->Initialize()) {
            syslog(LOG_ERR, "QSTHWClient::AddObserver - Unable to Initialize Stage 2 on ARM");
            return false;
          }
          if (!client_data_map_[index]->wwe_stage2_client_->ConfigureStage2Callbacks(observer, shared_from_this())) {
            syslog(LOG_ERR, "QSTHWClient::AddObserver - Unable to configure callbacks for Stage 2");
            return false;
          }
#else
          syslog(LOG_ERR,
              "QSTHWClient::AddObserver - AVS WWE Engine solution not compiled - Fail to initialize AVS WWE");
          // Free the unique ptr
          client_data_map_.at(index).reset();
          client_data_map_.erase(index);
          return false;
#endif
        }
      }

      ret = true;
    } break;
    default: {
      syslog(LOG_INFO, "QSTHWClient::AddObserver - %s Client Not Supported",
          ClientIDToString(client_id).c_str());
    } break;
  }

  syslog(LOG_INFO, "---QSTHWClient::AddObserver - Client: %s Size %d",
      ClientIDToString(client_id).c_str(), client_data_map_.size());
  return ret;
}

bool QSTHWClient::GetClientIdFromHdl(pa_qst_ses_handle_t hdl,
    VoiceUIClientID &clientid) {
  syslog(LOG_INFO, "Thread %x    ++--QSTHWClient::GetClientIdFromHdl",
      std::this_thread::get_id());
  for (auto &x : client_data_map_) {
    if (client_data_map_.at(x.first)) {
      if (client_data_map_.at(x.first)->ses_handle == hdl) {
        clientid = x.second->clientid;
        syslog(LOG_INFO, "GetClientIdFromHdl:: %d handle client id  %s", hdl,
            ClientIDToString(clientid).c_str());
        return true;
      }
    }
  }
  syslog(LOG_INFO, "GetClientIdFromHdl:: %d handle not found ", hdl);

  return false;
}

int QSTHWClient::StartRecognition(VoiceUIClientID client_id) {
  std::lock_guard<std::mutex> lock(wakeword_mutex_);

  syslog(LOG_DEBUG, "Thread %x    ++QSTHWClient::StartRecognition - Client: %s",
      std::this_thread::get_id(), ClientIDToString(client_id).c_str());
  int err = 0;

  syslog(LOG_INFO, "QSTHWClient::StartRecognition - Client: %s",
      ClientIDToString(client_id).c_str());

  if (client_data_map_.empty()) {
    syslog(LOG_INFO, "QSTHWClient::StartRecognition - No Clients Added");
    err = 1;
    return err;
  }

  auto index = ClientIDToIndex(client_id);
  auto client_data_ = client_data_map_.find(index);
  if (client_data_ == client_data_map_.end()) {
    syslog(LOG_INFO,
        "QSTHWClient::StartRecognition - Client %s not added(sound model is "
        "not loaded)",
        ClientIDToString(client_id).c_str());
    err = 1;
    return err;
  }

  // Check if startrecognition is already issued
  // If yes, return 2, already started
  if (client_data_map_.at(index)->started) {
    syslog(LOG_INFO,
        "QSTHWClient::StartRecognition - %s Client Already Recognising ",
        ClientIDToString(client_id).c_str());
    client_data_map_.at(index)->log_session_data();
    return err;
  }

  // Returns std::future, TODO: Add code to handle futures
  auto future_func = std::async(std::launch::async, [&, index]() {
    if (!pa_process_start_recognition(client_data_map_.at(index))) {
      syslog(LOG_INFO,
          "QSTHWClient::AddObserver - Unable to Start Recognition");
      client_data_map_.at(index)->started = false;
      // TODO: Add Negative call backs to observer client
      // Fore.g. STHAL Failure
      return;
    }
    client_data_map_.at(index)->started = true;
    // Signal Start of recognition to the client
    client_data_map_.at(index)->observer_client->StartRecognitionComplete();
  });
  syslog(LOG_DEBUG, "--QSTHWClient::StartRecognition - Client: %s",
      ClientIDToString(client_id).c_str());

  return err;
}

int QSTHWClient::StartRecognition() {
  std::lock_guard<std::mutex> lock(wakeword_mutex_);
  syslog(LOG_DEBUG,
      "Thread %x    ++QSTHWClient::StartRecognition() for all clients",
      std::this_thread::get_id());

  for (auto &iter : client_data_map_) {
    auto index = iter.first;
    std::async(std::launch::async, [&, index]() {
      if (!pa_process_start_recognition(client_data_map_.at(index))) {
        client_data_map_.at(index)->started = false;
        // TODO: Add Negative call backs to observer client
        // Fore.g. STHAL Failure
        return;
      }
      client_data_map_.at(index)->started = true;
      // Signal Start of recognition to the client
      client_data_map_.at(index)
          ->observer_client->StartRecognitionComplete();
    });
  }
  syslog(LOG_DEBUG, "--QSTHWClient::StartRecognition() for all clients");
  return 0;
}

void QSTHWClient::KeywordRecognized() {
  //TODO: REMOVE
}

// TODO:
void QSTHWClient::ReinitializeWakeWord() {
  syslog(LOG_DEBUG,
      "Thread %x    ++--QSTHWClient::ReinitializeWakeWord() for all clients",
      std::this_thread::get_id());
}

// This could be  synchronous call
int QSTHWClient::GetDOADirection(VoiceUIClientID client_id) {
  std::lock_guard<std::mutex> lock(wakeword_mutex_);

  syslog(LOG_DEBUG, "++QSTHWClient::GetDOADirection()");
  auto index = ClientIDToIndex(client_id);

  syslog(LOG_INFO, "QSTHWClient::GetDOADirection - Client: %s",
      ClientIDToString(client_id).c_str());

  auto client_data_ = client_data_map_.find(index);
  if (client_data_ == client_data_map_.end()) {
    syslog(LOG_INFO,
        "QSTHWClient::GetDOADirection - %s Client not added(sound model is "
        "not loaded)",
        ClientIDToString(client_id).c_str());
    return -1;
  }

  //Get the target DOA
  if (!pa_process_get_data(SoundTriggerData::DOA, client_data_map_.at(index))) {
    syslog(LOG_INFO, "QSTHWClient::GetDOADirection - Unable to Get DOA");
  }
  // client_data_map_.at(index)->observer_client->SoundTriggerDataReceived(
  // SoundTriggerData::DOA, client_data_map_.at(index)->target_doa_idx);
  syslog(LOG_DEBUG, "--QSTHWClient::GetDOADirection()");

  return client_data_map_.at(index)->target_doa_idx;
}

// This could be  synchronous call
int QSTHWClient::GetChannelIndex(VoiceUIClientID client_id) {
  std::lock_guard<std::mutex> lock(wakeword_mutex_);

  syslog(LOG_DEBUG, "++QSTHWClient::GetChannelIndex()");
  int err = 0;
  auto index = ClientIDToIndex(client_id);

  syslog(LOG_INFO, "QSTHWClient::GetChannelIndex - Client: %s",
      ClientIDToString(client_id).c_str());

  auto client_data_ = client_data_map_.find(index);
  if (client_data_ == client_data_map_.end()) {
    syslog(LOG_INFO,
        "QSTHWClient::GetChannelIndex - %s Client not added(sound model is "
        "not loaded)",
        ClientIDToString(client_id).c_str());
    err = 1;
    return err;
  }

  // Returns std::future, TODO: Add code to handle futures
  auto future_func = std::async(std::launch::async, [&, index]() {
    if (!pa_process_get_data(SoundTriggerData::CHANNEL_INDEX,
            client_data_map_.at(index))) {
      syslog(LOG_INFO,
          "QSTHWClient::GetChannelIndex - Unable to Get ChannelIndex");
      // TODO:
      // failure case
      return;
    }
    client_data_map_.at(index)->observer_client->SoundTriggerDataReceived(
        SoundTriggerData::DOA, client_data_map_.at(index)->target_channel_idx);
  });
  syslog(LOG_DEBUG, "--QSTHWClient::GetChannelIndex()");
  return err;
}

int QSTHWClient::StopRecognition(VoiceUIClientID client_id) {
  std::lock_guard<std::mutex> lock(wakeword_mutex_);

  syslog(LOG_DEBUG, "Thread %x    ++QSTHWClient::StopRecognition()",
      std::this_thread::get_id());
  int err = 0;

  syslog(LOG_INFO, "QSTHWClient::StopRecognition - Client: %s",
      ClientIDToString(client_id).c_str());

  if (client_data_map_.empty()) {
    syslog(LOG_INFO, "QSTHWClient::StopRecognition - No Clients Added");
    err = 1;
    return err;
  }

  auto index = ClientIDToIndex(client_id);
  auto client_data_ = client_data_map_.find(index);
  if (client_data_ == client_data_map_.end()) {
    syslog(LOG_INFO,
        "QSTHWClient::StopRecognition - %s Client not added(sound model is "
        "not loaded)",
        ClientIDToString(client_id).c_str());
    err = 1;
    return err;
  }

  // Check if client had already stopped or never started?
  if (!client_data_map_.at(index)->started) {
    syslog(LOG_INFO,
        "QSTHWClient::StopRecognition - %s Client Already Stopped/Never "
        "Started",
        ClientIDToString(client_id).c_str());
    client_data_map_.at(index)->log_session_data();
    return err;
  }

  // Stop Ongoing Labcapture
  if (client_data_map_.at(index)->pa_qst_event) {
    // Signal Capture thread to exit
    client_data_map_.at(index)->stopcapture = true;
    // Wait for Capture thread
    if (client_data_map_.at(index)->pa_event_cb_thread_.joinable()) {
      client_data_map_.at(index)->pa_event_cb_thread_.join();
      syslog(LOG_INFO,
          "QSTHWClient::StopRecognition - Joining a previous callback "
          "thread of %s",
          ClientIDToString(client_id).c_str());
    }
  }

  // Returns std::future, TODO: Add code to handle futures
  auto future_func = std::async(std::launch::async, [&, index]() {
    if (!pa_process_stop_recognition(client_data_map_.at(index))) {
      syslog(LOG_INFO,
          "QSTHWClient::StopRecognition - Unable to Stop Recognition");
      client_data_map_.at(index)->started = true;
      // TODO: Add Negative call backs to observer client
      // Fore.g. STHAL Failure
      return;
    }
    client_data_map_.at(index)->started = false;
    // If capture data is on, wait for thread to exit
  });
  syslog(LOG_DEBUG, "--QSTHWClient::StopRecognition()");
  return err;
}

int QSTHWClient::StopRecognition() {
  std::lock_guard<std::mutex> lock(wakeword_mutex_);

  syslog(LOG_DEBUG,
      "Thread %x    ++QSTHWClient::StopRecognition() for all clients",
      std::this_thread::get_id());
  for (auto &iter : client_data_map_) {
    if (!client_data_map_.at(iter.first)->started) {
      continue;
    }
    // Stop Ongoing Labcapture
    if (client_data_map_.at(iter.first)->pa_qst_event) {
      // Signal Capture thread to exit
      client_data_map_.at(iter.first)->stopcapture = true;
      // Wait for Capture thread
      if (client_data_map_.at(iter.first)->pa_event_cb_thread_.joinable()) {
        client_data_map_.at(iter.first)->pa_event_cb_thread_.join();
      }
    }
    auto index = iter.first;
    std::async(std::launch::async, [&, index]() {
      if (!pa_process_stop_recognition(client_data_map_.at(index))) {
        syslog(LOG_INFO,
            "QSTHWClient::StopRecognition - Unable to Stop Recognition");
        client_data_map_.at(iter.first)->started = true;
        // TODO: Add Negative call backs to observer client
        // Fore.g. STHAL Failure
        return;
      }
      client_data_map_.at(index)->started = false;
    });
  }
  syslog(LOG_DEBUG, "--QSTHWClient::StopRecognition() for all clients");
  return 0;
}

void QSTHWClient::StopAudioCapture(
    VoiceUIClientID client_id,
    StopCaptureResult capture_result) {
  std::lock_guard<std::mutex> lock(wakeword_mutex_);
  syslog(LOG_DEBUG, "Thread %x    ++QSTHWClient::StopAudioCapture()",
      std::this_thread::get_id());
  syslog(LOG_INFO, "QSTHWClient::StopAudioCapture - Client: %s",
      ClientIDToString(client_id).c_str());

  if (client_data_map_.empty()) {
    syslog(LOG_INFO, "QSTHWClient::StopAudioCapture - No Clients Added");
    return;
  }
  auto index = ClientIDToIndex(client_id);
  syslog(LOG_INFO, "QSTHWClient::StopAudioCapture - Client ID: %d", ClientIDToIndex(client_id));

  auto client_data_ = client_data_map_.find(index);
  if (client_data_ == client_data_map_.end()) {
    syslog(LOG_INFO,
        "QSTHWClient::StopAudioCapture - %s Client not added(sound model is "
        "not loaded)",
        ClientIDToString(client_id).c_str());
    return;
  }

  if (client_data_map_.at(index)->request_stop_in_progress_) {
    syslog(LOG_DEBUG, "Thread %x    ++QSTHWClient::StopAudioCapture() - STOP already being processed IGNORE request",
        std::this_thread::get_id());
    return;
  }

  // Check if client had already stopped or never started?
  if (!client_data_map_.at(index)->started) {
    syslog(LOG_INFO,
        "QSTHWClient::StopAudioCapture - %s Client Already Stopped/Never "
        "Started",
        ClientIDToString(client_id).c_str());
    client_data_map_.at(index)->log_session_data();
    return;
  }
  // No KWD
  if (!client_data_map_.at(index)->pa_qst_event) {
    syslog(LOG_INFO,
        "QSTHWClient::StopAudioCapture - %s No Keyword detected, No LAB "
        "Capture",
        ClientIDToString(client_id).c_str());
    client_data_map_.at(index)->log_session_data();
    return;
  }

  //initiate STOP LAB request
  client_data_map_.at(index)->request_stop_in_progress_ = true;
  // Signal Capture thread to exit
  std::thread t([&, index, client_id] {
    client_data_map_.at(index)->stopcapture = true;
    // Wait for Capture thread
    if (client_data_map_.at(index)->pa_event_cb_thread_.joinable()) {
      syslog(LOG_DEBUG, "Thread %x - QSTHWClient::StopAudioCapture() - INSIDE THREAD TO STOP LAB",
          std::this_thread::get_id());

      client_data_map_.at(index)->pa_event_cb_thread_.join();
      syslog(LOG_INFO,
          "QSTHWClient::StopAudioCapture - Joining a previous callback "
          "thread of %s (ID=%d)",
          ClientIDToString(client_id).c_str(), ClientIDToIndex(client_id));
    }
    // Notify client that audio capture is complete
    client_data_map_.at(index)->observer_client->StopAudioCaptureComplete();

    if (client_data_map_.at(index)->has_second_stage_) {
      //Stop 3P Stage2 WWE session
      client_data_map_.at(index)->wwe_stage2_client_->stop();
    }
    //initiate STOP LAB request executed
    client_data_map_.at(index)->request_stop_in_progress_ = false;
  });
  t.detach();

  syslog(LOG_DEBUG, "Thread %x    --QSTHWClient::StopAudioCapture()",
      std::this_thread::get_id());
  return;
}

bool QSTHWClient::RemoveObserver(VoiceUIClientID client_id) {
  std::lock_guard<std::mutex> lock(wakeword_mutex_);

  bool ret = false;
  auto index = ClientIDToIndex(client_id);

  syslog(LOG_INFO, "Thread %x    ++QSTHWClient::RemoveObserver - Client: %s",
      std::this_thread::get_id(), ClientIDToString(client_id).c_str());

  auto client_data_ = client_data_map_.find(index);
  if (client_data_ == client_data_map_.end()) {
    syslog(LOG_INFO,
        "QSTHWClient::RemoveObserver - %s Client not added as observer",
        ClientIDToString(client_id).c_str());
    return ret;
  }

  // Returns std::future, TODO: Add code to handle futures
  auto future_func = std::async(std::launch::async, [&, index]() {
    if (!pa_process_remove_observer(client_data_map_.at(index))) {
      syslog(LOG_INFO,
          "QSTHWClient::RemoveObserver - Unable to unload Sound model");
    }
    // Free the unique ptr
    client_data_map_.at(index).reset();
    client_data_map_.erase(index);

    // PLAN B ???

    if (client_data_map_.empty()) {
      syslog(LOG_INFO, "QSTHWClient::RemoveObserver - No active clients");
      // No need to handle the return code
      pa_process_deinitialize();
    }

    // TODO: TELL CLIENT THAT STHAL CAN FAIL
  });

  // TODO: Uncomment to make this synchornous
  // if (api_syncronous_)
  // ret = future_func.get();
  // else
  syslog(LOG_INFO, "--QSTHWClient::RemoveObserver - Client: %s",
      ClientIDToString(client_id).c_str());
  ret = true;
  return ret;
}

// Synchronous
// Called only on destruction of wakeword object
// Is this required?
// int QSTHWClient::RemoveAllObservers() {
//  syslog(LOG_DEBUG, "Thread %x    ++QSTHWClient::RemoveAllObservers()",
//  std::this_thread::get_id());

//  for (auto iter = client_data_map_.begin() : client_data_map_.end()) {
//        pa_process_remove_observer(client_data_map_.at(iter->first)));
// Free the unique ptr
//        client_data_map_.at(index).reset();
//        client_data_map_.erase(index);
//  }

//  //No need to handle the return code
//  pa_process_deinitialize();

//  syslog(LOG_DEBUG, "--QSTHWClient::StartRecognition() for all clients");
//  return 0;
//}
}  // namespace wakeword
}  // namespace voiceUIFramework
