/*Copyright (c) 2019-2020, The Linux Foundation. All rights reserved.
*
*Redistribution and use in source and binary forms, with or without
*modification, are permitted provided that the following conditions are
*met:
*    * Redistributions of source code must retain the above copyright
*      notice, this list of conditions and the following disclaimer.
*    * Redistributions in binary form must reproduce the above
*      copyright notice, this list of conditions and the following
*      disclaimer in the documentation and/or other materials provided
*      with the distribution.
*    * Neither the name of The Linux Foundation nor the names of its
*      contributors may be used to endorse or promote products derived
*      from this software without specific prior written permission.
*
*THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
*WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
*MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
*ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
*BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
*BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
*WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
*OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
*IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    SensoryKWDWakewordAdaptor.h
 *  @brief   Interface to handle the adaptation of voice samples to sensory
 *
 *  DESCRIPTION
 *    Handles call back containing keyword detected and voice packages
 ***************************************************************/

#ifndef WAKEWORD_INCLUDE_WAKEWORD_SENSORY_SENSORYHANDLER_H_
#define WAKEWORD_INCLUDE_WAKEWORD_SENSORY_SENSORYHANDLER_H_

#include <stdio.h>
#include <algorithm>
#include <condition_variable>
#include <cstdio>
#include <future>
#include <limits>
#include <list>
#include <memory>
#include <mutex>
#include <vector>

#include <Wakeword/WakeWordCallBackInterface.h>
#include "snsr.h"

namespace voiceUIFramework {
namespace wakeword {
namespace sensory {

class SensoryHandler : public std::enable_shared_from_this<SensoryHandler> {
 public:
  ~SensoryHandler();

  /**
     * Interface to Sensory Library
     *
     * @param - model File path
     * @param - number of sensory instances
     *
     */
  static std::shared_ptr<SensoryHandler> Create(
      const std::string& modelFilePath,
      int myInstanceId, int operatingPoint);

  bool IsKeyWordHit(
      std::vector<uint8_t> audioChannelData,
      voiceUIFramework::wakeword::KwdDetectionResults& result,
      std::vector<uint8_t>& labdata);

  int GetInstanceId();

  void FlushBuffers();

 private:
  SensoryHandler(int myInstanceId);

  /**
     * The callback that Sensory will issue to notify of keyword detections.
     *
     * @param s The @c Sensory session handle.
     * @param key The name of the callback setting.
     * @param userData A pointer to the user data to pass along to the callback.
     * @return @c SNSR_RC_OK if everything was processed properly, and a different error code otherwise.
     */
  static SnsrRC KeyWordDetectedCallback(
      SnsrSession s,
      const char* key,
      void* userData);

  /**
     * Initializes the stream reader, sets up the Sensory engine, and kicks off a thread to begin processing data from
     * the stream. This function should only be called once with each new @c AVSSensoryHandler.
     *
     * @param modelFilePath The path to the model file.
     * @return @c true if the engine was initialized properly and @c false otherwise.
     */
  bool Init(const std::string& modelFilePath, int operatingPoint);

  bool RunTimeSettings(SnsrSession* sensory_session);

  SnsrRC FeedToSensory(std::vector<uint8_t> audioChannel);

  void DisableKeyWordHitWinnerFound();

  SnsrRC ProcessKeywordHitResult();

  SnsrRC FetchKeywordIndicesFromSensory(double* begin, double* end);

  SnsrRC CaptureLookAheadBufferAdjustKeywordIndices(double begin, double end);

  int my_instance_id_;

  int operating_point_;

  double keyword_hit_begin_index_;
  double keyword_hit_end_index_;

  bool keyword_hit_winner_found_;

  SnsrSession sensory_session_;

  FILE* lab_capture_dump_;

  std::string modelFile_;

  std::vector<uint8_t> lab_data_;
};

}  // namespace sensory
}  // namespace wakeword
}  // namespace voiceUIFramework

#endif /* WAKEWORD_INCLUDE_WAKEWORD_SENSORY_SENSORYHANDLER_H_ */
