/*Copyright (c) 2019-2020, The Linux Foundation. All rights reserved.
*
*Redistribution and use in source and binary forms, with or without
*modification, are permitted provided that the following conditions are
*met:
*    * Redistributions of source code must retain the above copyright
*      notice, this list of conditions and the following disclaimer.
*    * Redistributions in binary form must reproduce the above
*      copyright notice, this list of conditions and the following
*      disclaimer in the documentation and/or other materials provided
*      with the distribution.
*    * Neither the name of The Linux Foundation nor the names of its
*      contributors may be used to endorse or promote products derived
*      from this software without specific prior written permission.
*
*THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
*WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
*MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
*ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
*BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
*BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
*WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
*OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
*IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    SensoryMultipleInstanceDelegate.h
 *  @brief   Interface to handle the adaptation of voice samples to sensory
 *
 *  DESCRIPTION
 *    Handles call back containing keyword detected and voice packages
 ***************************************************************/

#ifndef WAKEWORD_INCLUDE_WAKEWORD_SENSORY_SENSORYMULTIPLEINSTANCEDELEGATE_H_
#define WAKEWORD_INCLUDE_WAKEWORD_SENSORY_SENSORYMULTIPLEINSTANCEDELEGATE_H_

#include <bitset>
#include <condition_variable>
#include <future>
#include <limits>
#include <list>
#include <memory>
#include <mutex>
#include <queue>

#include <VoiceUIUtils/VoiceUIConfig.h>
#include <Wakeword/Sensory/SensoryHandler.h>

#define NUM_OF_SENSORY_INSTANCES 2

namespace voiceUIFramework {
namespace wakeword {
namespace sensory {

typedef struct task {
  std::vector<uint8_t> audio;
  std::promise<bool>* promise;
} audiopayload;

class SensoryMultipleInstanceDelegate : public std::enable_shared_from_this<SensoryMultipleInstanceDelegate> {
 public:
  ~SensoryMultipleInstanceDelegate();
  static std::shared_ptr<SensoryMultipleInstanceDelegate> Create();
  bool IsKeyWordHit(
      std::vector<int> muxAudioData,
      voiceUIFramework::wakeword::KwdDetectionResults& result,
      std::vector<uint8_t>& labdata);
  void ResetStateForKeywordRecognition();

 private:
  /* We could have created a vector or a list of these but for the use-case
     * its a over-kill. DSPC has 2 Audio channels to be fed to sensory
     * its a hard-requirement
     */
  SensoryMultipleInstanceDelegate(
      std::shared_ptr<SensoryHandler> sensory_handler_1,
      std::shared_ptr<SensoryHandler> sensory_handler_2);

  void Init();

  void OnKeywordHit(
      int instanceId,
      std::vector<uint8_t>& labData,
      KwdDetectionResults& result);
  void Loop(
      SensoryHandler* sensoryInstance,
      std::mutex* audio_buffer_sensory_available,
      std::condition_variable* audio_buffer_sensory_available_condition_variable,
      std::queue<audiopayload>* audio_queue);

  void Flush();

  std::shared_ptr<SensoryHandler> sensory_handler_1_;
  std::shared_ptr<SensoryHandler> sensory_handler_2_;

  bool is_keyword_hit_;
  mutable std::mutex is_keyword_hit_mutex_;

  std::atomic<bool> shutting_down_;

  std::thread sensory_handler_instance_1_thread_;
  std::thread sensory_handler_instance_2_thread_;

  std::vector<uint8_t> lab_data_;
  KwdDetectionResults kwd_detection_result_;

  std::queue<audiopayload> sensory_instance_1_audio_queue_;
  std::mutex audio_buffer_sensory_instance_1_available_mutex_;
  std::condition_variable audio_buffer_sensory_instance_1_available_condition_variable_;

  std::queue<audiopayload> sensory_instance_2_audio_queue_;
  std::mutex audio_buffer_sensory_instance_2_available_mutex_;
  std::condition_variable audio_buffer_sensory_instance_2_available_condition_variable_;
};

}  // namespace sensory
}  // namespace wakeword
}  // namespace voiceUIFramework

#endif /* WAKEWORD_INCLUDE_WAKEWORD_SENSORY_SENSORYMULTIPLEINSTANCEDELEGATE_H_ */
