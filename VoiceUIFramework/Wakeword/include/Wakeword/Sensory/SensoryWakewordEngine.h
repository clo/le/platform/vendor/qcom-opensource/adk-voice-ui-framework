/*Copyright (c) 2019, The Linux Foundation. All rights reserved.
*
*Redistribution and use in source and binary forms, with or without
*modification, are permitted provided that the following conditions are
*met:
*    * Redistributions of source code must retain the above copyright
*      notice, this list of conditions and the following disclaimer.
*    * Redistributions in binary form must reproduce the above
*      copyright notice, this list of conditions and the following
*      disclaimer in the documentation and/or other materials provided
*      with the distribution.
*    * Neither the name of The Linux Foundation nor the names of its
*      contributors may be used to endorse or promote products derived
*      from this software without specific prior written permission.
*
*THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
*WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
*MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
*ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
*BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
*BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
*WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
*OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
*IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    SensoryKWDWakewordAdaptor.h
 *  @brief   Interface to handle the adaptation of voice samples to sensory
 *
 *  DESCRIPTION
 *    Handles call back containing keyword detected and voice packages
 ***************************************************************/

#ifndef WAKEWORD_INCLUDE_WAKEWORD_SENSORY_SENSORYWAKEWORDENGINE_H_
#define WAKEWORD_INCLUDE_WAKEWORD_SENSORY_SENSORYWAKEWORDENGINE_H_

#include <memory>
#include <mutex>

#include <AIP/AudioProvider.h>
#include <AVSCommon/AVS/AudioInputStream.h>
#include <AVSCommon/SDKInterfaces/KeyWordObserverInterface.h>
#include <AVSCommon/Utils/SDS/SharedDataStream.h>
#include <DefaultClient/DefaultClient.h>

#include <Wakeword/WakeWordCallBackInterface.h>
#include <Wakeword/WakeWordInterface.h>

#include "SensoryMultipleInstanceDelegate.h"

namespace voiceUIFramework {
namespace wakeword {
namespace sensory {

using namespace alexaClientSDK;
using namespace alexaClientSDK::capabilityAgents::aip;
using namespace alexaClientSDK::avsCommon::sdkInterfaces;
using namespace alexaClientSDK::avsCommon::avs;

class SensoryWakewordEngine : public voiceUIFramework::wakeword::WakeWordInterface,
                              public std::enable_shared_from_this<SensoryWakewordEngine> {
 public:
  ~SensoryWakewordEngine();

  /**
     * Adds a stream to the SensoryKWDWakewordAdaptor. This is necessary for
     * maintaining the shared ring buffer for the AVS. Essentially this stream
     * points to the ring buffer which needs to be filled in whenever there is
     * a KWD hit
     *
     * @param - stream to be populated whenever there is a hit
     * @param - default client
     *
     */
  static std::shared_ptr<SensoryWakewordEngine> Create();

  bool Feed(const std::vector<int> in) final;
  int StartRecognition(VoiceUIClientID client_id) final;
  int StartRecognition() final;
  int StopRecognition(VoiceUIClientID client_id) final;
  int StopRecognition() final;
  void StopAudioCapture(
      VoiceUIClientID client_id,
      StopCaptureResult capture_data) final;
  void KeywordRecognized() final;
  bool AddObserver(
      VoiceUIClientID client,
      std::shared_ptr<WakeWordCallBackInterface> observer) final;
  bool RemoveObserver(VoiceUIClientID client) final;
  int GetDOADirection(VoiceUIClientID client_id) final;
  int GetChannelIndex(VoiceUIClientID client_id) final;

 private:
  SensoryWakewordEngine(
      std::shared_ptr<SensoryMultipleInstanceDelegate> sensoryMultipleInstanceDelegate);

  void EnableKeywordRecognition();
  void DisableKeywordRecognition();
  void SetKeywordSessionStatus(bool keywordHit);
  WakewordCallbackRc FeedAudioDataToASR(std::vector<uint8_t> audioASR);
  WakewordCallbackRc SetupAudioDataForASR(
      voiceUIFramework::wakeword::KwdDetectionResults& kwdResult);

  std::shared_ptr<SensoryMultipleInstanceDelegate> sensory_multiple_instance_delegate_;

  std::shared_ptr<WakeWordCallBackInterface> audio_capture_callback_;
  mutable std::mutex audio_capture_callback_mutex_;

  bool enable_keyword_recognition_;
  mutable std::mutex enable_keyword_recognition_mutex_;

  bool keyword_hit_session_;
  mutable std::mutex keyword_hit_session_mutex_;
};

}  // namespace sensory
}  // namespace wakeword
}  // namespace voiceUIFramework

#endif /* WAKEWORD_INCLUDE_WAKEWORD_SENSORY_SENSORYWAKEWORDENGINE_H_ */
