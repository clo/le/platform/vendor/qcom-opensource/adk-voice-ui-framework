/* Copyright (c) 2018, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    WakeWordUtils.h
 *  @brief   contains util functions required by wake word
 *  Some of them are c -functions
 *
 *  DESCRIPTION
 *   
 ***************************************************************/

#ifndef VOICEUIFRAMEWORK_WAKEWORD_INCLUDE_WAKEWORDUTILS_H_
#define VOICEUIFRAMEWORK_WAKEWORD_INCLUDE_WAKEWORDUTILS_H_

#include <fstream>
#include <mutex>
#ifndef BASEMACHINE_IPQx
#include "mm-audio/pulseaudio/qti_soundtrigger/pa_qti_soundtrigger.h"
#else /*Cypress*/
#include "pa-qti-soundtrigger/pa_qti_soundtrigger.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

struct keyword_buffer_config {
  int version;
  uint32_t kb_duration;
}__attribute__((packed));

/* Definitions from STHAL opaque_header.h to send packed opaque data */
#define ST_MAX_SOUND_MODELS 10
#define ST_MAX_KEYWORDS 10
#define ST_MAX_USERS 10

enum param_key {
    PARAM_KEY_CONFIDENCE_LEVELS,
    PARAM_KEY_HISTORY_BUFFER_CONFIG,
    PARAM_KEY_KEYWORD_INDICES,
    PARAM_KEY_TIMESTAMP,
};

enum sound_model_id {
    SM_ID_NONE = 0x0000,
    SM_ID_SVA_GMM = 0x0001,
    SM_ID_SVA_CNN = 0x0002,
    SM_ID_SVA_VOP = 0x0004,
    SM_ID_SVA_END = 0x00F0,
    SM_ID_CUSTOM_START = 0x0100,
    SM_ID_CUSTOM_END = 0xF000,
};

struct opaque_param_header {
    enum param_key key_id;
    uint32_t payload_size;
}__attribute__((packed));

struct user_levels {
    uint32_t user_id;
    uint8_t level;
}__attribute__((packed));

struct keyword_levels {
    uint8_t kw_level;
    uint32_t num_user_levels;
    struct user_levels user_levels[ST_MAX_USERS];
}__attribute__((packed));

struct sound_model_conf_levels {
    enum sound_model_id sm_id;
    uint32_t num_kw_levels;
    struct keyword_levels kw_levels[ST_MAX_KEYWORDS];
}__attribute__((packed));

struct confidence_levels_info {
    uint32_t version;
    uint32_t num_sound_models;
    struct sound_model_conf_levels conf_levels[ST_MAX_SOUND_MODELS];
}__attribute__((packed));

struct hist_buffer_info {
    uint32_t version;
    uint32_t hist_buffer_duration_msec;
    uint32_t pre_roll_duration_msec;
}__attribute__((packed));

struct keyword_indices_info {
    uint32_t version;
    uint32_t start_index; /* in bytes */
    uint32_t end_index; /* in bytes */
}__attribute__((packed));

struct timestamp_info {
    uint32_t version;
    long first_stage_det_event_timestamp; /* in nanoseconds */
    long second_stage_det_event_timestamp; /* in nanoseconds */
}__attribute__((packed));

extern const sound_trigger_uuid_t qc_uuid;
int string_to_uuid(const char *str, sound_trigger_uuid_t *uuid);
const char *handle_set_parameters(const char *kv_pair);


#ifdef __cplusplus
}
#endif
#endif  //VOICEUIFRAMEWORK_WAKEWORD_INCLUDE_WAKEWORDUTILS_H_
