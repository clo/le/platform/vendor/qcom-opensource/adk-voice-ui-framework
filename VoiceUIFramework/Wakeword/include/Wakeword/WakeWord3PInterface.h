/* Copyright (c) 2019, The Linux Foundation. All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 * Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above
 copyright notice, this list of conditions and the following
 disclaimer in the documentation and/or other materials provided
 with the distribution.
 * Neither the name of The Linux Foundation nor the names of its
 contributors may be used to endorse or promote products derived
 from this software without specific prior written permission.

 THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    WakeWord3PInterface.h
 *  @brief   Interface to handle 3P wakeword engines running on ARM as Stage 2 for SVA
 *
 *  DESCRIPTION
 *    Interface to handles 3P wakeword engines on ARM as a option for seconds stage from SVA
 ***************************************************************/

#ifndef VOICEUIFRAMEWORK_WAKEWORD_INCLUDE_WAKEWORD3PINTERFACE_H_
#define VOICEUIFRAMEWORK_WAKEWORD_INCLUDE_WAKEWORD3PINTERFACE_H_

#include <VoiceUIUtils/GlobalValues.h>
#include <Wakeword/WakeWordCallBackInterface.h>
#include <Wakeword/WakeWordInterface.h>

using voiceUIFramework::voiceUIUtils::VoiceUIClientID;

namespace voiceUIFramework {
namespace wakeword {

class WakeWord3PInterface {
 public:
  virtual ~WakeWord3PInterface() = default;

  //Initialize 3P Wakeword Engine
  virtual bool Initialize() = 0;
  //Start session on 3P wakeword engine
  virtual bool start(voiceUIFramework::wakeword::KwdDetectionResults res) = 0;
  //Stop session on 3P wakeword engine
  virtual bool stop() = 0;
  //Send audio to 3P wakeword engine to validate if keyword is there
  virtual void PumdAudio(void* buf, const size_t bytes) = 0;
  //Configure callbacks for Wakeword Stage 1 Engine (SVA) and aldo callback object to interact with VoiceUI Client
  virtual bool ConfigureStage2Callbacks(std::shared_ptr<WakeWordCallBackInterface> observer_client, std::shared_ptr<WakeWordInterface> wakeword_stage1) = 0;
};

}  // namespace wakeword
}  // namespace voiceUIFramework

#endif /* VOICEUIFRAMEWORK_WAKEWORD_INCLUDE_WAKEWORD3PINTERFACE_H_ */
