/* Copyright (c) 2018, The Linux Foundation. All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 * Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above
 copyright notice, this list of conditions and the following
 disclaimer in the documentation and/or other materials provided
 with the distribution.
 * Neither the name of The Linux Foundation nor the names of its
 contributors may be used to endorse or promote products derived
 from this software without specific prior written permission.

 THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    WakeWordCallBackInterface.h
 *  @brief   Interface to handle wakeword call back
 *
 *  DESCRIPTION
 *    Handles call back containing keyword detected and voice packages
 ***************************************************************/

#ifndef VOICEUIFRAMEWORK_WAKEWORD_INCLUDE_WAKEWORDCALLBACKINTERFACE_H_
#define VOICEUIFRAMEWORK_WAKEWORD_INCLUDE_WAKEWORDCALLBACKINTERFACE_H_

#include <VoiceUIUtils/GlobalValues.h>

using voiceUIFramework::voiceUIUtils::VoiceUIClientID;

#define PA_QST_PARAMETER_DIRECTION_OF_ARRIVAL "st_direction_of_arrival"
#define PA_QST_PARAMETER_CHANNEL_INDEX "st_channel_index"
#define WW_CALLBACK_OK "ww_ok"
#define WW_CALLBACK_ERR "ww_err"

namespace voiceUIFramework {
namespace wakeword {

typedef struct {
  //keyword start index
  int keyword_start_index_;
  //keyword end index
  int keyword_end_index_;
  //keyword detection count
  int detection_count_;
  // Confidence level stage 1 & 2 combined
  int confidence_level_;
  //string keyword detected
  std::string keyword;
} KwdDetectionResults;

enum class SoundTriggerData {
  DOA,
  CHANNEL_INDEX,
};

enum class WakewordCallbackRc {
  WW_CB_OK,
  WW_CB_ERR
};

inline const std::string WakewordCallbackRcToString(WakewordCallbackRc event) {
  switch (event) {
    case WakewordCallbackRc::WW_CB_OK:
      return WW_CALLBACK_OK;
    case WakewordCallbackRc::WW_CB_ERR:
      return WW_CALLBACK_ERR;
  }
  return std::string("Invalid");
}

inline const std::string SoundTriggerDataToString(SoundTriggerData event) {
  switch (event) {
    case SoundTriggerData::DOA:
      return PA_QST_PARAMETER_DIRECTION_OF_ARRIVAL;
    case SoundTriggerData::CHANNEL_INDEX:
      return PA_QST_PARAMETER_CHANNEL_INDEX;
  }
  return std::string("Invalid");
}

class WakeWordCallBackInterface {
 public:
  virtual ~WakeWordCallBackInterface() = default;

  // Observer client is added Successfully (Sound model Loaded Successfully)
  virtual WakewordCallbackRc ObserverAdded() = 0;
  // Observer client is removed Successfully (Sound model Unloaded Successfully)
  virtual WakewordCallbackRc ObserverRemoved() = 0;
  // Reconigtion is scheduled, STHAL is recognising keywords of this client
  virtual WakewordCallbackRc StartRecognitionComplete() = 0;
  // Callback to transfer LAB Data from Buffers in WakeWord to the client
  virtual WakewordCallbackRc OnNewVoiceBufferReceived(
      void* buf,
      size_t size) = 0;
  // Callback to notify detection of keyword
  virtual WakewordCallbackRc OnKeywordDetected(KwdDetectionResults res) = 0;
  // Callback to notify Stop Recognition
  virtual WakewordCallbackRc StopRecognitionComplete() = 0;
  // Callback to notify Stop Audio Capture
  virtual WakewordCallbackRc StopAudioCaptureComplete() = 0;

  // Call back to notify doa or channel Index
  // Can be over loaded to get multiple values
  virtual WakewordCallbackRc SoundTriggerDataReceived(
      SoundTriggerData type,
      int value) = 0;
};

}  // namespace wakeword
}  // namespace voiceUIFramework

#endif /* VOICEUIFRAMEWORK_WAKEWORD_INCLUDE_WAKEWORDCALLBACKINTERFACE_H_ */
