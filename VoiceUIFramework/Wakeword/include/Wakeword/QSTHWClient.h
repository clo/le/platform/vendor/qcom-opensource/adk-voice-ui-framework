/* Copyright (c) 2018, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    QSTHWClient.h
 *  @brief   Wakeword singleton class client for STHAL
 *
 *  DESCRIPTION
 *    WakeWord Client based on Snapdragon Voice Activation using ST HAL
 ***************************************************************/

#ifndef VOICEUIFRAMEWORK_WAKEWORD_INCLUDE_QSTHWCLIENT_H_
#define VOICEUIFRAMEWORK_WAKEWORD_INCLUDE_QSTHWCLIENT_H_

#include <syslog.h>
#include <future>
#include <iostream>
#include <map>
#include <memory>
#include <string>
#include <unordered_map>

#ifdef BUILD_AVS_WWE
#include <Wakeword/AVS_WWE/AVSWWEStage2.h>
#endif
#include <Wakeword/WakeWord3PInterface.h>
#include <Wakeword/WakeWordInterface.h>
#include <Wakeword/WakeWordUtils.h>
#ifndef BASEMACHINE_IPQx
#include "mm-audio/pulseaudio/qti_soundtrigger/pa_qti_soundtrigger.h"
#else /*Cypress*/
#include "pa-qti-soundtrigger/pa_qti_soundtrigger.h"
#endif

namespace voiceUIFramework {
namespace wakeword {

#define PA_QST_MODULE_ID_PRIMARY "soundtrigger.primary"

typedef void pa_qst_handle_t;
using voiceUIFramework::wakeword::KwdDetectionResults;
using voiceUIFramework::wakeword::SoundTriggerData;

// pa_qst_session_data defines the data per client/session
typedef struct pa_qst_session_data {
  //Client id of the voice-ui-framework client
  VoiceUIClientID clientid;
  sound_trigger_uuid_t vendor_uuid;
  std::string sm_file_path;
  pa_qst_ses_handle_t ses_handle;
  unsigned int num_kws;

  // Record the observer actions on the wakeword session
  bool loaded;
  bool started;
  // LAB data capture variable
  // STHAL data is captured till stopcapture is true
  bool stopcapture;
  //Stop LAB capture is being processed
  bool request_stop_in_progress_;
  //Second Stage on ARM available for sessions
  bool has_second_stage_;

  //wakeword observer client
  std::shared_ptr<WakeWordCallBackInterface> observer_client;
  //Amazon WWE client to run as Stage 2
  std::shared_ptr<WakeWord3PInterface> wwe_stage2_client_;

  unsigned int counter;
  struct sound_trigger_recognition_config *rc_config;
  struct pa_qst_phrase_recognition_event *pa_qst_event;
  // thread object that handles a pa_event_callback
  std::thread pa_event_cb_thread_;
  // Handles concurrency on callbacks
  std::mutex pa_event_mutex_;

  int kw_buffer_duration_in_ms;
  //Private Database Getter functions
  int get_kw_buffer_duration_in_ms();
  int get_first_stage_confidence_level();
  int get_second_stage_confidence_level();
  std::string get_soundmodel_path();

  //STHAL Data
  int target_doa_idx;
  int target_channel_idx;

  //Generic Payload
  int generic_payload_size;
  unsigned int stage1_cnf_level;
  unsigned int stage2_cnf_level;

  pa_qst_session_data(VoiceUIClientID client, std::shared_ptr<WakeWordCallBackInterface> observer);
  pa_qst_session_data() = delete;
  //log session data
  void log_session_data();
} pa_qst_session_data_t;

typedef std::unique_ptr<pa_qst_session_data_t> SessionDataPtr;

class QSTHWClient : public WakeWordInterface, public std::enable_shared_from_this<QSTHWClient> {
 public:
  ~QSTHWClient();

  // Singleton creation or assign for wakeword reference.
  static std::shared_ptr<QSTHWClient> Get();

  // Add Observer
  bool AddObserver(VoiceUIClientID client, std::shared_ptr<WakeWordCallBackInterface> observer);
  //Starts wakeword engine
  virtual int StartRecognition(VoiceUIClientID client_id);
  virtual int StartRecognition();
  //keyword detected notification
  virtual void KeywordRecognized();
  //Stops wakeword engine
  virtual int StopRecognition();
  virtual int StopRecognition(VoiceUIClientID client_id);
  virtual bool Feed(const std::vector<int> in);

  //Stops audio capture (LAB) once keyword is detected
  virtual void StopAudioCapture(
      VoiceUIClientID client_id,
      StopCaptureResult stop_capture);
  // Remove observer
  virtual bool RemoveObserver(VoiceUIClientID client);

  //shutdown wake word engine
  //virtual void Shutdown();

  //Restarts QSTHWClient
  void ReinitializeWakeWord();
  //Gets direction of arrival notification
  int GetDOADirection(VoiceUIClientID client);
  //Gets Channel Index to be used during multi-turn conversation using Audio Recorder interface.
  int GetChannelIndex(VoiceUIClientID client);

  // transient call back common to all sessions for the call back
  //
  static void pa_event_callback(struct sound_trigger_recognition_event *event, void *sessionHndl);

  // session specific thread call back
  void pa_event_handler_thread(int index);

  // Map stores the global (applied as whole on STHAL irrespective of client) wakeword params from the database
  // Applied at Initiaize to set paramters on STHAL
  // TODO; Do we need these parameters  for each client-specific session?
  // If yes, we need to expose these on the wake word Interface
  // Examples
  // PA_QST_PARAMETER_CUSTOM_CHANNEL_MIXING,
  // PA_QST_PARAMETER_SESSION_PAUSE,
  // PA_QST_PARAMETER_BAD_MIC_CHANNEL_INDEX,
  // PA_QST_PARAMETER_EC_REF_DEVICE

  static std::map<std::string, std::string> global_wakeword_params;

  // Map stores the client specific wakeword params from the database
  // Applied at Initiaize to set paramters on STHAL
  // TODO; Do we need these parameters  for each active session?
  // If yes, we need to expose these on the wake word Interface
  // Example
  // SOUND_MODEL_PATH etc

  std::map<std::string, std::string> client_wakeword_params;

 private:
  QSTHWClient();

  // singleton reference for QSTHWClient
  static std::weak_ptr<QSTHWClient> qsthwClient_ptr_;

  // std::future to manage sequential access to pa_qti_sound_trigger
  // TODO  std::future pa_access_future;
  // Internal private functions to access pa_pti_soundtrigger library
  // Private functions abstracted to call APIs of pa-qti-soundtrigger

  int pa_process_initialize();
  void pa_process_detection_event(SessionDataPtr &session_data, KwdDetectionResults &res);
  void pa_capture_lab_data(SessionDataPtr &session_data);
  bool pa_process_add_observer(SessionDataPtr &session_data);
  int pa_process_deinitialize();
  bool pa_process_remove_observer(SessionDataPtr &session_data);
  bool pa_process_start_recognition(SessionDataPtr &session_data);
  bool pa_process_stop_recognition(SessionDataPtr &session_data);
  bool pa_process_get_data(SoundTriggerData type, SessionDataPtr &session_data);
  bool pa_process_set_global_params(std::string kv_pair);

  // Callfunctions
  // Utility function to get session data pointer from pa handle
  bool GetClientIdFromHdl(pa_qst_ses_handle_t hdl, VoiceUIClientID &clientid);

  void LoadWakeWordDatabase();
  bool GetVoiceDumpEnabled();
  std::string GetVoiceDumpPath();
  //int RemoveAllObservers();

  //PA related private members
  const pa_qst_handle_t *pa_qst_handle;
  // Maps a client to client specific qst data
  std::unordered_map<int, SessionDataPtr> client_data_map_;

  // TODO: Synchronise incoming observerclient threads and call back threads
  std::mutex wakeword_mutex_;
};

}  // namespace wakeword
}  // namespace voiceUIFramework

#endif /* VOICEUIFRAMEWORK_WAKEWORD_INCLUDE_QSTHWCLIENT_H_ */
