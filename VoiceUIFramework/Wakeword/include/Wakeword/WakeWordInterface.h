/* Copyright (c) 2018, The Linux Foundation. All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 * Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above
 copyright notice, this list of conditions and the following
 disclaimer in the documentation and/or other materials provided
 with the distribution.
 * Neither the name of The Linux Foundation nor the names of its
 contributors may be used to endorse or promote products derived
 from this software without specific prior written permission.

 THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    WakeWordInterface.h
 *  @brief   WakeWord Interface for hotword engines
 *
 *  DESCRIPTION
 *     WakeWord Interface for hotword engines implementation
 ***************************************************************/

#ifndef VOICEUIFRAMEWORK_WAKEWORD_INCLUDE_WAKEWORDINTERFACE_H_
#define VOICEUIFRAMEWORK_WAKEWORD_INCLUDE_WAKEWORDINTERFACE_H_

#include <VoiceUIUtils/GlobalValues.h>
#include <Wakeword/WakeWordCallBackInterface.h>

using voiceUIFramework::voiceUIUtils::VoiceUIClientID;
using voiceUIFramework::wakeword::WakeWordCallBackInterface;

#define WW_STOPCAPTURE_OK "wakeword_stopcapture_ok"
#define WW_STOPCAPTURE_FAIL "wakeword_stopcapture_fail"

namespace voiceUIFramework {
namespace wakeword {

enum class StopCaptureResult {
  STOPCAPTURE_OK,
  STOPCAPTURE_FAIL
};

inline const std::string StopCaptureResultToString(StopCaptureResult event) {
  switch (event) {
    case StopCaptureResult::STOPCAPTURE_OK:
      return WW_STOPCAPTURE_OK;
    case StopCaptureResult::STOPCAPTURE_FAIL:
      return WW_STOPCAPTURE_FAIL;
  }
  return std::string("Invalid");
}

class WakeWordInterface {
 public:
  virtual ~WakeWordInterface() = default;
  //Starts wakeword engine for a particular client
  virtual int StartRecognition(VoiceUIClientID client_id) = 0;
  //Starts wakeword engine for all clients
  virtual int StartRecognition() = 0;
  //Stop wakeword engine for a particular client
  virtual int StopRecognition(VoiceUIClientID client_id) = 0;
  //Stop wakeword engine for all clients
  virtual int StopRecognition() = 0;
  //Stop audio capture (LAB) once keyword is detected
  //Synchronous call doesnt go to pulse Audio
  virtual void StopAudioCapture(
      VoiceUIClientID client_id,
      StopCaptureResult capture_data) = 0;

  virtual bool Feed(const std::vector<int> in) = 0;

  //keyword detected notification
  virtual void KeywordRecognized() = 0;
  //add observer
  virtual bool AddObserver(
      VoiceUIClientID client,
      std::shared_ptr<WakeWordCallBackInterface> observer) = 0;
  //Removes the client as wakeword observer
  virtual bool RemoveObserver(VoiceUIClientID client) = 0;
  // Get DOA, Blocking call returns doa on success else -1
  virtual int GetDOADirection(VoiceUIClientID client_id) = 0;
  // Get Channel index, Blocking call returns channel index on success else -1
  virtual int GetChannelIndex(VoiceUIClientID client_id) = 0;
};
}  // namespace wakeword
}  // namespace voiceUIFramework

#endif /* VOICEUIFRAMEWORK_WAKEWORD_INCLUDE_WAKEWORDINTERFACE_H_ */
