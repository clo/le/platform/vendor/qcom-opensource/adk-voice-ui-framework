/* Copyright (c) 2018, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    WakewordDummyClient.h
 *  @brief   Dummy Wakeword client
 *
 *  DESCRIPTION
 *    Dummy WakeWord Client
 ***************************************************************/

#ifndef VOICEUIFRAMEWORK_WAKEWORD_INCLUDE_WAKEWORDDUMMYCLIENT_H_
#define VOICEUIFRAMEWORK_WAKEWORD_INCLUDE_WAKEWORDDUMMYCLIENT_H_

#include <syslog.h>
#include <iostream>
#include <memory>
#include <string>

#include <Wakeword/WakeWordInterface.h>

namespace voiceUIFramework {
namespace wakeword {

class WakewordDummyClient : public WakeWordInterface {
 public:
  ~WakewordDummyClient();

  // Singleton creation or assign for wakeword reference.
  static std::shared_ptr<WakewordDummyClient> Get();
  //initializes wake word engine
  virtual int Initialize();

  /* WakeWordInterface */
  //Starts wakeword engine
  virtual int StartRecognition();
  virtual int StartRecognition(VoiceUIClientID client_id);
  //Stops wakeword engine
  virtual int StopRecognition();
  virtual int StopRecognition(VoiceUIClientID client_id);
  //keyword detected notification
  virtual void KeywordRecognized();
  //Stops audio capture (LAB) once keyword is detected
  virtual void StopAudioCapture(
      VoiceUIClientID client_id,
      StopCaptureResult stop_capture);
  // Add Observer
  bool AddObserver(VoiceUIClientID client, std::shared_ptr<WakeWordCallBackInterface> observer);
  // Remove observer
  virtual bool RemoveObserver(VoiceUIClientID client);

  virtual bool Feed(const std::vector<int> in);

  //shutdown wake word engine
  //virtual void Shutdown();

  //Restarts WakewordDummyClient
  void ReinitializeWakeWord();
  //Gets direction of arrival notification
  int GetDOADirection(VoiceUIClientID client);
  //Gets Channel Index to be used during multi-turn conversation using Audio Recorder interface.
  int GetChannelIndex(VoiceUIClientID client);

 private:
  WakewordDummyClient();

  //singleton reference for QSTHWClient
  static std::weak_ptr<WakewordDummyClient> wakeword_dummy_client_;

  //direction of arrival index
  int target_doa_index_;
  //Channel index for multi-turn conversation interaction
  int target_channel_index;
  //keyword start index
  int keyword_start_index_;
  //keyword end index
  int keyword_end_index_;
  //keyword detection count
  int detection_count_;
  //QSTHW Client processing state
  bool recognizing_;
};

}  // namespace wakeword
}  // namespace voiceUIFramework

#endif /* VOICEUIFRAMEWORK_WAKEWORD_INCLUDE_WAKEWORDDUMMYCLIENT_H_ */
