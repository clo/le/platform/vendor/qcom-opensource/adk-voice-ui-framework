/*Copyright (c) 2019, The Linux Foundation. All rights reserved.
*
*Redistribution and use in source and binary forms, with or without
*modification, are permitted provided that the following conditions are
*met:
*    * Redistributions of source code must retain the above copyright
*      notice, this list of conditions and the following disclaimer.
*    * Redistributions in binary form must reproduce the above
*      copyright notice, this list of conditions and the following
*      disclaimer in the documentation and/or other materials provided
*      with the distribution.
*    * Neither the name of The Linux Foundation nor the names of its
*      contributors may be used to endorse or promote products derived
*      from this software without specific prior written permission.
*
*THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
*WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
*MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
*ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
*BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
*BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
*WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
*OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
*IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    AVSWWEStage2.h
 *  @brief   Interface to handle the adaptation of voice samples to amazon WWE
 *
 *  DESCRIPTION
 *    Amazon Wakeword engine client as Stage 2 after SVA Stage1
 ***************************************************************/

#ifndef WAKEWORD_INCLUDE_WAKEWORD_SENSORY_AVSWWESTAGE2_H_
#define WAKEWORD_INCLUDE_WAKEWORD_SENSORY_AVSWWESTAGE2_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <unistd.h>
#include <cstdio>
#include <fstream>
#include <iostream>
#include <memory>
#include <thread>
#include <unordered_map>
#include <vector>

#include <VoiceUIUtils/VoiceUIConfig.h>
#include <Wakeword/AVS_WWE/pryon_lite.h>
#include <Wakeword/WakeWord3PInterface.h>
//#include <Wakeword/WakeWordCallBackInterface.h>
//#include <Wakeword/WakeWordInterface.h>

namespace voiceUIFramework {
namespace wakeword {
namespace avs {

enum class KeywordSessionStatus {
  IDLE = 0,
  PROCESSING,
  KEYWORD_FOUND,
  REJECTED_BY_CLOUD
};

class AVSWWEStage2 : public voiceUIFramework::wakeword::WakeWord3PInterface {
 public:
  AVSWWEStage2();
  ~AVSWWEStage2();

  virtual bool Initialize();
  virtual bool start(voiceUIFramework::wakeword::KwdDetectionResults res);
  virtual bool stop();
  virtual void PumdAudio(void* buf, const size_t bytes);
  virtual /*static*/ bool ConfigureStage2Callbacks(std::shared_ptr<WakeWordCallBackInterface> observer_client, std::shared_ptr<WakeWordInterface> wakeword_stage1);

 private:
  bool loadModelIntoMemory(std::vector<char>* modelMem, const std::string& modelFilePath);
  static void SendAudioToClient(void* buf, const size_t bytes);
  static void detectionCallback(PryonLiteDecoderHandle handle, const PryonLiteResult* result);
  static void vadCallback(PryonLiteDecoderHandle handle, const PryonLiteVadEvent* vadEvent);

  /// The PryonLite decoder.
  PryonLiteDecoderHandle keyword_decoder_;
  /// The memory allocated to hold m_decoder.
  std::vector<char> m_decoderMem;
  /// The memory allocated to hold the PryonLite model data.
  std::vector<char> m_modelMem;
  ///Samples per frame for Pump audio
  int samples_per_frame_;
  //Threshold for Alexa keyword
  int stage2_threshold_;
  //Timeout for Keyword validation
  int stage2_timeout_;
  //AVS WWE sound model
  std::string sound_model_;
  //keep track of total words read
  long long total_words_read_;
  //detection count for Stage 2
  static int detection_count_;
  //total words after Last Interaction
  static long long total_words_last_interaction_;

  static std::shared_ptr<WakeWordCallBackInterface> observer_client_;
  static std::shared_ptr<WakeWordInterface> wakeword_stage1_;
  static KeywordSessionStatus keyword_session_status_;
  //static bool keyword_hit_session_;

  //audio dump files
  FILE* voice_samples_;
  //Internal lab buffer while Stage 2 is verified
  static std::vector<uint8_t> internal_lab_buffer_;
  //keyword results from Stage 1
  static voiceUIFramework::wakeword::KwdDetectionResults res_;
};

}  // namespace avs
}  // namespace wakeword
}  // namespace voiceUIFramework

#endif /* WAKEWORD_INCLUDE_WAKEWORD_SENSORY_AVSSTAGE2_ */
