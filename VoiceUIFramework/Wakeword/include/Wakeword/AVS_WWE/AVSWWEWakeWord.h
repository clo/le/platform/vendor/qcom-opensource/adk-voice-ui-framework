/*Copyright (c) 2019-2020, The Linux Foundation. All rights reserved.
*
*Redistribution and use in source and binary forms, with or without
*modification, are permitted provided that the following conditions are
*met:
*    * Redistributions of source code must retain the above copyright
*      notice, this list of conditions and the following disclaimer.
*    * Redistributions in binary form must reproduce the above
*      copyright notice, this list of conditions and the following
*      disclaimer in the documentation and/or other materials provided
*      with the distribution.
*    * Neither the name of The Linux Foundation nor the names of its
*      contributors may be used to endorse or promote products derived
*      from this software without specific prior written permission.
*
*THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
*WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
*MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
*ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
*BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
*BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
*WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
*OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
*IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    AVSWWEWakeWord.h
 *  @brief   Interface to handle the adaptation of voice samples to amazon WWE
 *
 *  DESCRIPTION
 *    Amazon Wakeword engine used as only stage.
 ***************************************************************/

#ifndef WAKEWORD_INCLUDE_WAKEWORD_AVS_WWE_AVSWWEWAKEWORD_H_
#define WAKEWORD_INCLUDE_WAKEWORD_AVS_WWE_AVSWWEWAKEWORD_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <unistd.h>
#include <algorithm>
#include <chrono>
#include <cstdio>
#include <ctime>
#include <fstream>
#include <iostream>
#include <memory>
#include <ratio>
#include <thread>
#include <unordered_map>
#include <vector>

#include <VoiceUIUtils/ThirdPartyAudioUtils.h>
#include <VoiceUIUtils/VoiceUIConfig.h>
#include <Wakeword/AVS_WWE/pryon_lite.h>
#include <Wakeword/WakeWordInterface.h>
#include <Wakeword/WakeWordUtils.h>

namespace voiceUIFramework {
namespace wakeword {
namespace avs {

#define BUFF_SIZE 96000  // 16000 Hz * 16 bit * 3 sec
#define PREROOL 16000    // 500ms

enum class WWEKeywordSessionStatus {
  IDLE = 0,
  PROCESSING,
  KEYWORD_FOUND,
  SHUTDOWN
};

class AVSWWEWakeWord : public voiceUIFramework::wakeword::WakeWordInterface,
                       public std::enable_shared_from_this<AVSWWEWakeWord> {
 public:
  AVSWWEWakeWord();
  ~AVSWWEWakeWord();

  /* WakeWordInterface class */
  // Add Observer
  bool AddObserver(VoiceUIClientID client, std::shared_ptr<WakeWordCallBackInterface> observer);
  //Starts wakeword engine
  virtual int StartRecognition(VoiceUIClientID client_id);
  virtual int StartRecognition();
  //keyword detected notification
  virtual void KeywordRecognized();
  //Stops wakeword engine
  virtual int StopRecognition();
  virtual int StopRecognition(VoiceUIClientID client_id);
  virtual bool Feed(const std::vector<int> in);

  //Stops audio capture (LAB) once keyword is detected
  virtual void StopAudioCapture(
      VoiceUIClientID client_id,
      StopCaptureResult stop_capture);
  // Remove observer
  virtual bool RemoveObserver(VoiceUIClientID client);

  //Gets direction of arrival notification
  int GetDOADirection(VoiceUIClientID client);
  //Gets Channel Index to be used during multi-turn conversation using Audio Recorder interface.
  int GetChannelIndex(VoiceUIClientID client);

 private:
  static void detectionCallback(PryonLiteDecoderHandle handle, const PryonLiteResult* result);
  static void vadCallback(PryonLiteDecoderHandle handle, const PryonLiteVadEvent* vadEvent);
  bool loadModelIntoMemory(std::vector<char>* modelMem, const std::string& modelFilePath);

  bool enable_keyword_recognition_;
  bool keyword_hit_session_;
  static std::shared_ptr<WakeWordCallBackInterface> audio_capture_callback_;
  static WWEKeywordSessionStatus keyword_session_status_;
  //audio dump files
  static FILE* voice_samples_;
  static FILE* voice_samples_2;
  //detection count for WWE
  static int detection_count_;

  //internal buffer for WWE
  static std::vector<uint8_t> internal_buffer_;
  static std::vector<uint8_t>::iterator iter_buffer;

  //parameters for WWE engine
  int wwe_threshold_;
  std::string sound_model_;

  /// The PryonLite decoder.
  PryonLiteDecoderHandle keyword_decoder_;
  /// The memory allocated to hold m_decoder.
  std::vector<char> m_decoderMem_;
  /// The memory allocated to hold the PryonLite model data.
  std::vector<char> m_modelMem_;
  ///Samples per frame for Pump audio
  int samples_per_frame_;
  //keep track of total words read
  long long total_words_read_;
};

}  // namespace avs
}  // namespace wakeword
}  // namespace voiceUIFramework

#endif /* WAKEWORD_INCLUDE_WAKEWORD_AVS_WWE_AVSWWEWAKEWORD_H_ */
