/* Copyright (c) 2018, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    VoiceUIWakewordTest.cpp
 *  @brief   brief description
 *
 *  DESCRIPTION
 *    description
 ***************************************************************/

#include <chrono>
#include <iostream>
#include <memory>
#include <string>
#include <thread>
#include <utility>
#include <vector>

#include <VoiceUIUtils/GlobalValues.h>
#include <Wakeword/QSTHWClient.h>
#include <Wakeword/WakeWordCallBackInterface.h>

#include "gtest/gtest.h"

using namespace std;
using voiceUIFramework::voiceUIUtils::VoiceUIClientID;
using voiceUIFramework::wakeword::KwdDetectionResults;
using voiceUIFramework::wakeword::QSTHWClient;
using voiceUIFramework::wakeword::WakeWordInterface;
using voiceUIFramework::wakeword::SoundTriggerData;


namespace {

class MockWakeWordObserver : public WakeWordCallBackInterface {
 public:
  MockWakeWordObserver();
  virtual void ObserverAdded();
  virtual void ObserverRemoved();
  virtual void StartRecognitionComplete();
  virtual void OnNewVoiceBufferReceived(void* buf, size_t size);
  virtual void OnKeywordDetected(KwdDetectionResults res);
  virtual void StopRecognitionComplete();
  virtual void SoundTriggerDataReceived(SoundTriggerData type, int value);

  bool ObserverAddedCalled;
  bool ObserverRemovedCalled;
  bool StartRecognitionCompleteCalled;
  bool OnNewVoiceBufferReceivedCalled;
  bool OnKeywordDetectedCalled;
  bool StopRecognitionCompleteCalled;
  bool SoundTriggerDataReceivedCalled;
};

MockWakeWordObserver::MockWakeWordObserver()
    : ObserverAddedCalled{false}, ObserverRemovedCalled{false}, StartRecognitionCompleteCalled{false}, OnNewVoiceBufferReceivedCalled{false}, OnKeywordDetectedCalled{false}, StopRecognitionCompleteCalled{false}, SoundTriggerDataReceivedCalled{false} {
}

void MockWakeWordObserver::ObserverAdded() {
  syslog(LOG_INFO, "***** MockWakeWordObserver::ObserverAdded");
  ObserverAddedCalled = true;
}

void MockWakeWordObserver::ObserverRemoved() {
  syslog(LOG_INFO, "***** MockWakeWordObserver::ObserverRemoved");
  ObserverRemovedCalled = true;
}

void MockWakeWordObserver::StartRecognitionComplete() {
  syslog(LOG_INFO, "***** MockWakeWordObserver::StartRecognitionComplete");
  StartRecognitionCompleteCalled = true;
}

void MockWakeWordObserver::OnNewVoiceBufferReceived(void* buf, size_t size) {
  syslog(LOG_INFO, "***** MockWakeWordObserver::OnNewVoiceBufferReceived");
  cout << "***** MockWakeWordObserver::OnNewVoiceBufferReceived";
  OnNewVoiceBufferReceivedCalled = true;
}

void MockWakeWordObserver::OnKeywordDetected(KwdDetectionResults res) {
  syslog(LOG_INFO, "***** MockWakeWordObserver::OnKeywordDetected");
  cout << "***** MockWakeWordObserver::OnKeywordDetected";
  OnKeywordDetectedCalled = true;
}
void MockWakeWordObserver::StopRecognitionComplete() {
  syslog(LOG_INFO, "***** MockWakeWordObserver::StopRecognitionComplete");
  StopRecognitionCompleteCalled = true;
}

void MockWakeWordObserver::SoundTriggerDataReceived(SoundTriggerData type, int value) {
  syslog(LOG_DEBUG, "***** MockWakeWordObserver::SoundTriggerDataReceived %s  : %d\n", SoundTriggerDataToString(type).c_str(), value);
  SoundTriggerDataReceivedCalled = true;
}

// The fixture for testing class VoiceUIWakeword.
class VoiceUIWakewordTest : public ::testing::Test {
 protected:
  VoiceUIWakewordTest() {
    // You can do set-up work for each test here.
  }

  virtual ~VoiceUIWakewordTest() {
    // You can do clean-up work that doesn't throw exceptions here.
  }

  std::shared_ptr<WakeWordInterface> wake_word_;

  virtual void SetUp() {
    wake_word_ = voiceUIFramework::wakeword::QSTHWClient::Get();
  }

  virtual void TearDown() {
  }
};

//Test access to single ton class
TEST_F(VoiceUIWakewordTest, MethodTestWakewordCreate) {
  syslog(LOG_INFO, "***** VoiceUIWakewordTest::MethodTestWakewordCreate - START");
  ASSERT_NE(nullptr, wake_word_);
  syslog(LOG_INFO, "***** VoiceUIWakewordTest::MethodTestWakewordCreate - END");
}

//Test add observer
TEST_F(VoiceUIWakewordTest, MethodTestWakewordAddObserverSingle) {
  ASSERT_NE(nullptr, wake_word_);
  auto wake_word_observer_avs_ = std::make_shared<MockWakeWordObserver>();
  ASSERT_EQ(true, wake_word_->AddObserver(VoiceUIClientID::AVS_SOLUTION, wake_word_observer_avs_));
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  ASSERT_EQ(true, wake_word_observer_avs_->ObserverAddedCalled);
}

TEST_F(VoiceUIWakewordTest, MethodTestWakewordAddObserverMultiple) {
  ASSERT_NE(nullptr, wake_word_);
  auto wake_word_observer_avs_ = std::make_shared<MockWakeWordObserver>();
  ASSERT_EQ(true, wake_word_->AddObserver(VoiceUIClientID::AVS_SOLUTION, wake_word_observer_avs_));
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  ASSERT_EQ(true, wake_word_observer_avs_->ObserverAddedCalled);

  auto wake_word_observer_qc_ = std::make_shared<MockWakeWordObserver>();
  ASSERT_EQ(true, wake_word_->AddObserver(VoiceUIClientID::MODULAR_SOLUTION, wake_word_observer_qc_));
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  ASSERT_EQ(true, wake_word_observer_qc_->ObserverAddedCalled);
}
TEST_F(VoiceUIWakewordTest, MethodTestWakewordAddObserverDuplicate) {
  ASSERT_NE(nullptr, wake_word_);
  auto wake_word_observer_avs_ = std::make_shared<MockWakeWordObserver>();
  ASSERT_EQ(true, wake_word_->AddObserver(VoiceUIClientID::AVS_SOLUTION, wake_word_observer_avs_));
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  ASSERT_EQ(true, wake_word_observer_avs_->ObserverAddedCalled);
  //Reset
  wake_word_observer_avs_->ObserverAddedCalled = false;

  auto wake_word_observer_avs_duplicate_ = std::make_shared<MockWakeWordObserver>();
  ASSERT_EQ(false, wake_word_->AddObserver(VoiceUIClientID::AVS_SOLUTION, wake_word_observer_avs_duplicate_));
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  ASSERT_EQ(false, wake_word_observer_avs_->ObserverAddedCalled);
}

TEST_F(VoiceUIWakewordTest, MethodTestWakewordAddObserverInvalidClients) {
  ASSERT_NE(nullptr, wake_word_);
  auto wake_word_observer_dummy_ = std::make_shared<MockWakeWordObserver>();
  ASSERT_EQ(false, wake_word_->AddObserver(VoiceUIClientID::MODULAR_SOLUTION, wake_word_observer_dummy_));
  ASSERT_EQ(false, wake_word_observer_dummy_->ObserverAddedCalled);
  ASSERT_EQ(false, wake_word_->AddObserver(VoiceUIClientID::CORTANA_SOLUTION, wake_word_observer_dummy_));
  ASSERT_EQ(false, wake_word_observer_dummy_->ObserverAddedCalled);
  ASSERT_EQ(false, wake_word_->AddObserver(VoiceUIClientID::GVA_SOLUTION, wake_word_observer_dummy_));
  ASSERT_EQ(false, wake_word_observer_dummy_->ObserverAddedCalled);
  ASSERT_EQ(false, wake_word_->AddObserver(VoiceUIClientID::MOCK_FULL_SOLUTION, wake_word_observer_dummy_));
  ASSERT_EQ(false, wake_word_observer_dummy_->ObserverAddedCalled);
  ASSERT_EQ(false, wake_word_->AddObserver(VoiceUIClientID::MOCK_MODULAR_SOLUTION, wake_word_observer_dummy_));
  ASSERT_EQ(false, wake_word_observer_dummy_->ObserverAddedCalled);
}

TEST_F(VoiceUIWakewordTest, MethodTestWakewordAddObserverWithNoObserverObject) {
  ASSERT_NE(nullptr, wake_word_);
  ASSERT_EQ(false, wake_word_->AddObserver(VoiceUIClientID::AVS_SOLUTION, nullptr));
  ASSERT_EQ(false, wake_word_->AddObserver(VoiceUIClientID::MODULAR_SOLUTION, nullptr));
}

TEST_F(VoiceUIWakewordTest, MethodTestWakewordRemoveObserver) {
  ASSERT_NE(nullptr, wake_word_);
  ASSERT_EQ(false, wake_word_->RemoveObserver(VoiceUIClientID::AVS_SOLUTION));
  ASSERT_EQ(false, wake_word_->RemoveObserver(VoiceUIClientID::MODULAR_SOLUTION));
}

TEST_F(VoiceUIWakewordTest, MethodTestWakewordAddRemoveObserverSingle) {
  ASSERT_NE(nullptr, wake_word_);
  auto wake_word_observer_avs_ = std::make_shared<MockWakeWordObserver>();
  ASSERT_EQ(true, wake_word_->AddObserver(VoiceUIClientID::AVS_SOLUTION, wake_word_observer_avs_));
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  ASSERT_EQ(true, wake_word_observer_avs_->ObserverAddedCalled);
  ASSERT_EQ(true, wake_word_->RemoveObserver(VoiceUIClientID::AVS_SOLUTION));
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  ASSERT_EQ(true, wake_word_observer_avs_->ObserverRemovedCalled);
}

TEST_F(VoiceUIWakewordTest, MethodTestWakewordAddMultipleRemoveObserverSingle) {
  ASSERT_NE(nullptr, wake_word_);
  auto wake_word_observer_avs_ = std::make_shared<MockWakeWordObserver>();
  ASSERT_EQ(true, wake_word_->AddObserver(VoiceUIClientID::AVS_SOLUTION, wake_word_observer_avs_));
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  ASSERT_EQ(true, wake_word_observer_avs_->ObserverAddedCalled);

  auto wake_word_observer_qc_ = std::make_shared<MockWakeWordObserver>();
  ASSERT_EQ(true, wake_word_->AddObserver(VoiceUIClientID::MODULAR_SOLUTION, wake_word_observer_qc_));
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  ASSERT_EQ(true, wake_word_observer_qc_->ObserverAddedCalled);

  ASSERT_EQ(true, wake_word_->RemoveObserver(VoiceUIClientID::AVS_SOLUTION));
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  ASSERT_EQ(true, wake_word_observer_avs_->ObserverRemovedCalled);
  ASSERT_EQ(false, wake_word_observer_qc_->ObserverRemovedCalled);
}
TEST_F(VoiceUIWakewordTest, MethodTestWakewordAddMultipleRemoveObserverMultiple) {
  ASSERT_NE(nullptr, wake_word_);
  auto wake_word_observer_avs_ = std::make_shared<MockWakeWordObserver>();
  ASSERT_EQ(true, wake_word_->AddObserver(VoiceUIClientID::AVS_SOLUTION, wake_word_observer_avs_));
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  ASSERT_EQ(true, wake_word_observer_avs_->ObserverAddedCalled);

  auto wake_word_observer_qc_ = std::make_shared<MockWakeWordObserver>();
  ASSERT_EQ(true, wake_word_->AddObserver(VoiceUIClientID::MODULAR_SOLUTION, wake_word_observer_qc_));
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  ASSERT_EQ(true, wake_word_observer_qc_->ObserverAddedCalled);

  ASSERT_EQ(true, wake_word_->RemoveObserver(VoiceUIClientID::AVS_SOLUTION));
  ASSERT_EQ(true, wake_word_->RemoveObserver(VoiceUIClientID::MODULAR_SOLUTION));
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  ASSERT_EQ(true, wake_word_observer_avs_->ObserverRemovedCalled);
  ASSERT_EQ(true, wake_word_observer_qc_->ObserverRemovedCalled);
}
//TODO: Multi threaded test cases
TEST_F(VoiceUIWakewordTest, MethodTestWakewordStartRecognitionSingle) {
  ASSERT_NE(nullptr, wake_word_);
  auto wake_word_observer_avs_ = std::make_shared<MockWakeWordObserver>();
  ASSERT_EQ(true, wake_word_->AddObserver(VoiceUIClientID::AVS_SOLUTION, wake_word_observer_avs_));
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  ASSERT_EQ(true, wake_word_observer_avs_->ObserverAddedCalled);

  ASSERT_EQ(0, wake_word_->StartRecognition(VoiceUIClientID::AVS_SOLUTION));
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  ASSERT_EQ(true, wake_word_observer_avs_->StartRecognitionCompleteCalled);

  ASSERT_EQ(true, wake_word_->RemoveObserver(VoiceUIClientID::AVS_SOLUTION));
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  ASSERT_EQ(true, wake_word_observer_avs_->ObserverRemovedCalled);
}

TEST_F(VoiceUIWakewordTest, MethodTestWakewordStartRecognitionSingleNoObserverAdded) {
  ASSERT_NE(nullptr, wake_word_);

  auto wake_word_observer_avs_ = std::make_shared<MockWakeWordObserver>();
  //error 1
  ASSERT_EQ(1, wake_word_->StartRecognition(VoiceUIClientID::AVS_SOLUTION));
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  ASSERT_EQ(false, wake_word_observer_avs_->StartRecognitionCompleteCalled);
}

// Start Recognition after observer is removed
TEST_F(VoiceUIWakewordTest, MethodTestWakewordStartRecognitionSingleAfterObsRemoved) {
  ASSERT_NE(nullptr, wake_word_);

  auto wake_word_observer_avs_ = std::make_shared<MockWakeWordObserver>();

  ASSERT_EQ(true, wake_word_->AddObserver(VoiceUIClientID::AVS_SOLUTION, wake_word_observer_avs_));
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  ASSERT_EQ(true, wake_word_observer_avs_->ObserverAddedCalled);

  ASSERT_EQ(true, wake_word_->RemoveObserver(VoiceUIClientID::AVS_SOLUTION));
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  ASSERT_EQ(true, wake_word_observer_avs_->ObserverRemovedCalled);

  //error 1
  wake_word_observer_avs_ = std::make_shared<MockWakeWordObserver>();
  ASSERT_EQ(1, wake_word_->StartRecognition(VoiceUIClientID::AVS_SOLUTION));
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  ASSERT_EQ(false, wake_word_observer_avs_->StartRecognitionCompleteCalled);
}

TEST_F(VoiceUIWakewordTest, MethodTestWakewordStartRecognitionSingleWhenAlreadyRecognizing) {
  ASSERT_NE(nullptr, wake_word_);
  auto wake_word_observer_avs_ = std::make_shared<MockWakeWordObserver>();
  ASSERT_EQ(true, wake_word_->AddObserver(VoiceUIClientID::AVS_SOLUTION, wake_word_observer_avs_));
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  ASSERT_EQ(true, wake_word_observer_avs_->ObserverAddedCalled);

  //Error code 0 + callback
  ASSERT_EQ(0, wake_word_->StartRecognition(VoiceUIClientID::AVS_SOLUTION));
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  ASSERT_EQ(true, wake_word_observer_avs_->StartRecognitionCompleteCalled);
  wake_word_observer_avs_->StartRecognitionCompleteCalled = false;

  //Errocode 0 + no callback
  ASSERT_EQ(0, wake_word_->StartRecognition(VoiceUIClientID::AVS_SOLUTION));
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  ASSERT_EQ(false, wake_word_observer_avs_->StartRecognitionCompleteCalled);

  ASSERT_EQ(true, wake_word_->RemoveObserver(VoiceUIClientID::AVS_SOLUTION));
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  ASSERT_EQ(true, wake_word_observer_avs_->ObserverRemovedCalled);
}

// Stop Recognition Tests
TEST_F(VoiceUIWakewordTest, MethodTestWakewordStopRecognitionSingle) {
  ASSERT_NE(nullptr, wake_word_);
  auto wake_word_observer_avs_ = std::make_shared<MockWakeWordObserver>();
  ASSERT_EQ(true, wake_word_->AddObserver(VoiceUIClientID::AVS_SOLUTION, wake_word_observer_avs_));
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  ASSERT_EQ(true, wake_word_observer_avs_->ObserverAddedCalled);

  ASSERT_EQ(0, wake_word_->StartRecognition(VoiceUIClientID::AVS_SOLUTION));
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  ASSERT_EQ(true, wake_word_observer_avs_->StartRecognitionCompleteCalled);

  ASSERT_EQ(0, wake_word_->StopRecognition(VoiceUIClientID::AVS_SOLUTION));
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  ASSERT_EQ(true, wake_word_observer_avs_->StopRecognitionCompleteCalled);

  ASSERT_EQ(true, wake_word_->RemoveObserver(VoiceUIClientID::AVS_SOLUTION));
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  ASSERT_EQ(true, wake_word_observer_avs_->ObserverRemovedCalled);
}

TEST_F(VoiceUIWakewordTest, MethodTestWakewordStopRecognitionSingleNoObserverAdded) {
  ASSERT_NE(nullptr, wake_word_);

  auto wake_word_observer_avs_ = std::make_shared<MockWakeWordObserver>();
  //error 1
  ASSERT_EQ(1, wake_word_->StopRecognition(VoiceUIClientID::AVS_SOLUTION));
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  ASSERT_EQ(false, wake_word_observer_avs_->StopRecognitionCompleteCalled);
}

// Start Recognition after observer is removed
TEST_F(VoiceUIWakewordTest, MethodTestWakewordStopRecognitionSingleAfterObsRemoved) {
  ASSERT_NE(nullptr, wake_word_);

  auto wake_word_observer_avs_ = std::make_shared<MockWakeWordObserver>();

  ASSERT_EQ(true, wake_word_->AddObserver(VoiceUIClientID::AVS_SOLUTION, wake_word_observer_avs_));
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  ASSERT_EQ(true, wake_word_observer_avs_->ObserverAddedCalled);

  ASSERT_EQ(true, wake_word_->RemoveObserver(VoiceUIClientID::AVS_SOLUTION));
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  ASSERT_EQ(true, wake_word_observer_avs_->ObserverRemovedCalled);

  //error 1
  wake_word_observer_avs_ = std::make_shared<MockWakeWordObserver>();
  ASSERT_EQ(1, wake_word_->StopRecognition(VoiceUIClientID::AVS_SOLUTION));
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  ASSERT_EQ(false, wake_word_observer_avs_->StopRecognitionCompleteCalled);
}

TEST_F(VoiceUIWakewordTest, MethodTestWakewordStopRecognitionSingleWhenAlreadyStopped) {
  ASSERT_NE(nullptr, wake_word_);
  auto wake_word_observer_avs_ = std::make_shared<MockWakeWordObserver>();
  ASSERT_EQ(true, wake_word_->AddObserver(VoiceUIClientID::AVS_SOLUTION, wake_word_observer_avs_));
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  ASSERT_EQ(true, wake_word_observer_avs_->ObserverAddedCalled);

  ASSERT_EQ(0, wake_word_->StartRecognition(VoiceUIClientID::AVS_SOLUTION));
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  ASSERT_EQ(true, wake_word_observer_avs_->StartRecognitionCompleteCalled);

  //Error code 0 + callback
  ASSERT_EQ(0, wake_word_->StopRecognition(VoiceUIClientID::AVS_SOLUTION));
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  ASSERT_EQ(true, wake_word_observer_avs_->StopRecognitionCompleteCalled);
  wake_word_observer_avs_->StopRecognitionCompleteCalled = false;

  //Errocode 0 + no callback
  ASSERT_EQ(0, wake_word_->StopRecognition(VoiceUIClientID::AVS_SOLUTION));
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  ASSERT_EQ(false, wake_word_observer_avs_->StopRecognitionCompleteCalled);

  ASSERT_EQ(true, wake_word_->RemoveObserver(VoiceUIClientID::AVS_SOLUTION));
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  ASSERT_EQ(true, wake_word_observer_avs_->ObserverRemovedCalled);
}

//Stop Audio Capture Tests
TEST_F(VoiceUIWakewordTest, MethodTestWakewordStopAudioCaptureSingle) {
  ASSERT_NE(nullptr, wake_word_);
  auto wake_word_observer_avs_ = std::make_shared<MockWakeWordObserver>();

  wake_word_->StopAudioCapture(VoiceUIClientID::AVS_SOLUTION);

  ASSERT_EQ(true, wake_word_->AddObserver(VoiceUIClientID::AVS_SOLUTION, wake_word_observer_avs_));
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  ASSERT_EQ(true, wake_word_observer_avs_->ObserverAddedCalled);

  ASSERT_EQ(0, wake_word_->StartRecognition(VoiceUIClientID::AVS_SOLUTION));
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  ASSERT_EQ(true, wake_word_observer_avs_->StartRecognitionCompleteCalled);

  wake_word_->StopAudioCapture(VoiceUIClientID::AVS_SOLUTION);

  ASSERT_EQ(0, wake_word_->StopRecognition(VoiceUIClientID::AVS_SOLUTION));
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  ASSERT_EQ(true, wake_word_observer_avs_->StopRecognitionCompleteCalled);

  wake_word_->StopAudioCapture(VoiceUIClientID::AVS_SOLUTION);

  ASSERT_EQ(true, wake_word_->RemoveObserver(VoiceUIClientID::AVS_SOLUTION));
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  ASSERT_EQ(true, wake_word_observer_avs_->ObserverRemovedCalled);

  wake_word_->StopAudioCapture(VoiceUIClientID::AVS_SOLUTION);
}

// Not automated : Needs Manual Input
TEST_F(VoiceUIWakewordTest, MethodTestWakewordSnapDragonTestManual) {
  syslog(LOG_INFO, "***** VoiceUIWakewordTest::MethodTestWakewordSnapDragonTestManual - START");
  ASSERT_NE(nullptr, wake_word_);
  auto wake_word_observer_qc_ = std::make_shared<MockWakeWordObserver>();
  ASSERT_EQ(true, wake_word_->AddObserver(VoiceUIClientID::MODULAR_SOLUTION, wake_word_observer_qc_));
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  ASSERT_EQ(true, wake_word_observer_qc_->ObserverAddedCalled);

  ASSERT_EQ(0, wake_word_->StartRecognition(VoiceUIClientID::MODULAR_SOLUTION));
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  ASSERT_EQ(true, wake_word_observer_qc_->StartRecognitionCompleteCalled);

  cout << "Start Speaking... Sanpdragon is listening(Issue commands in 10 seconds) ";

  char c;
  bool loop = true;
  bool innerLoopForKwd = true;
  while (loop) {
    while (innerLoopForKwd) {
      //check for Keyword Detection every 500ms
      std::this_thread::sleep_for(std::chrono::milliseconds(500));
      if (wake_word_observer_qc_->OnKeywordDetectedCalled) {
        break;
      }
    }

    cout << "LAB Capture for 5 sec";
    std::this_thread::sleep_for(std::chrono::milliseconds(5000));

    wake_word_->GetDOADirection(VoiceUIClientID::MODULAR_SOLUTION);
    wake_word_->GetChannelIndex(VoiceUIClientID::MODULAR_SOLUTION);

    //Stop Audio Capture
    wake_word_->StopAudioCapture(VoiceUIClientID::MODULAR_SOLUTION);

    cout << "Playing back LAB DATA  Every 10 sec\n";
    //TODO:
    system("paplay --raw --rate 16000 --channels 1 --format s16le -v /data/voice-ui-framework/SVA/lab_capture_file_01");

    cout << "Press y to continue or n to exit";
    cin >> c;
    if (c == 'y')
      continue;
    else
      break;
  }

  ASSERT_EQ(0, wake_word_->StopRecognition(VoiceUIClientID::MODULAR_SOLUTION));
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  ASSERT_EQ(true, wake_word_observer_qc_->StopRecognitionCompleteCalled);

  ASSERT_EQ(true, wake_word_->RemoveObserver(VoiceUIClientID::MODULAR_SOLUTION));
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  ASSERT_EQ(true, wake_word_observer_qc_->ObserverRemovedCalled);
  syslog(LOG_INFO, "***** VoiceUIWakewordTest::MethodTestWakewordSnapDragonTestManual - END");
}

// Not automated : Needs Manual Input
TEST_F(VoiceUIWakewordTest, MethodTestWakewordAlexaTestManual) {
  syslog(LOG_INFO, "***** VoiceUIWakewordTest::MethodTestWakewordAlexaTestManual - START");
  ASSERT_NE(nullptr, wake_word_);
  auto wake_word_observer_avs_ = std::make_shared<MockWakeWordObserver>();
  ASSERT_EQ(true, wake_word_->AddObserver(VoiceUIClientID::AVS_SOLUTION, wake_word_observer_avs_));
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  ASSERT_EQ(true, wake_word_observer_avs_->ObserverAddedCalled);

  ASSERT_EQ(0, wake_word_->StartRecognition(VoiceUIClientID::AVS_SOLUTION));
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  ASSERT_EQ(true, wake_word_observer_avs_->StartRecognitionCompleteCalled);

  cout << "Start Speaking... Alexa is listening(Issue commands in 10 seconds) ";

  char c;
  bool loop = true;
  bool innerLoopForKwd = true;
  while (loop) {
    while (innerLoopForKwd) {
      //check for Keyword Detection every 500ms
      std::this_thread::sleep_for(std::chrono::milliseconds(500));
      if (wake_word_observer_avs_->OnKeywordDetectedCalled) {
        break;
      }
    }

    cout << "LAB Capture for 5 sec";

    std::this_thread::sleep_for(std::chrono::milliseconds(5000));

    wake_word_->GetDOADirection(VoiceUIClientID::AVS_SOLUTION);
    wake_word_->GetChannelIndex(VoiceUIClientID::AVS_SOLUTION);

    //Stop Audio Capture
    wake_word_->StopAudioCapture(VoiceUIClientID::AVS_SOLUTION);

    cout << "Playing back LAB DATA  Every 10 sec\n";
    //TODO:
    system("paplay --raw --rate 16000 --channels 1 --format s16le -v /data/voice-ui-framework/SVA/lab_capture_file_01");

    cout << "Press y to continue or n to exit";
    cin >> c;
    if (c == 'y')
      continue;
    else
      break;
  }

  ASSERT_EQ(0, wake_word_->StopRecognition(VoiceUIClientID::AVS_SOLUTION));
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  ASSERT_EQ(true, wake_word_observer_avs_->StopRecognitionCompleteCalled);

  ASSERT_EQ(true, wake_word_->RemoveObserver(VoiceUIClientID::AVS_SOLUTION));
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  ASSERT_EQ(true, wake_word_observer_avs_->ObserverRemovedCalled);
  syslog(LOG_INFO, "***** VoiceUIWakewordTest::MethodTestWakewordAlexaTestManual - END");
}

// Not automated : Needs Manual Input
TEST_F(VoiceUIWakewordTest, MethodTestMultiWakewordTestManual) {
  syslog(LOG_INFO, "***** VoiceUIWakewordTest::MethodTestMultiWakewordTestManual - START");
  ASSERT_NE(nullptr, wake_word_);
  auto wake_word_observer_avs_ = std::make_shared<MockWakeWordObserver>();
  auto wake_word_observer_qc_ = std::make_shared<MockWakeWordObserver>();

  ASSERT_EQ(true, wake_word_->AddObserver(VoiceUIClientID::AVS_SOLUTION, wake_word_observer_avs_));
  ASSERT_EQ(true, wake_word_->AddObserver(VoiceUIClientID::MODULAR_SOLUTION, wake_word_observer_qc_));

  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  ASSERT_EQ(true, wake_word_observer_avs_->ObserverAddedCalled);
  ASSERT_EQ(true, wake_word_observer_qc_->ObserverAddedCalled);

  ASSERT_EQ(0, wake_word_->StartRecognition(VoiceUIClientID::MODULAR_SOLUTION));
  ASSERT_EQ(0, wake_word_->StartRecognition(VoiceUIClientID::AVS_SOLUTION));
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  ASSERT_EQ(true, wake_word_observer_avs_->StartRecognitionCompleteCalled);
  ASSERT_EQ(true, wake_word_observer_qc_->StartRecognitionCompleteCalled);

  cout << "Start Speaking... Both Alexa and SnapDragon are listening.... ";

  char c;
  bool loop = true;
  bool innerLoopForKwd = true;
  while (loop) {
    while (innerLoopForKwd) {
      //check for Keyword Detection every 500ms
      std::this_thread::sleep_for(std::chrono::milliseconds(500));
      if (wake_word_observer_avs_->OnKeywordDetectedCalled) {
        cout << "ALEXA Keyword Detected!!!";
        break;
      }
      if (wake_word_observer_qc_->OnKeywordDetectedCalled) {
        cout << "SNAPDRAGON Keyword Detected!!!";
        break;
      }
    }

    cout << "LAB Capture for 5 sec";
    std::this_thread::sleep_for(std::chrono::milliseconds(5000));

    //Stop Audio Capture
    if (wake_word_observer_qc_->OnKeywordDetectedCalled) {
      wake_word_observer_qc_->OnKeywordDetectedCalled = false;
      wake_word_->StopAudioCapture(VoiceUIClientID::MODULAR_SOLUTION);
      
    } else {
      wake_word_observer_avs_->OnKeywordDetectedCalled = false;
      wake_word_->StopAudioCapture(VoiceUIClientID::AVS_SOLUTION);
      
    }

    cout << "Playing back LAB DATA  Every 10 sec\n";
    //TODO:
    system("paplay --raw --rate 16000 --channels 1 --format s16le -v /data/voice-ui-framework/SVA/lab_capture_file_01");

    cout << "Press y to continue or n to exit";
    cin >> c;
    if (c == 'y')
      continue;
    else
      break;
  }

  ASSERT_EQ(0, wake_word_->StopRecognition(VoiceUIClientID::MODULAR_SOLUTION));
  ASSERT_EQ(0, wake_word_->StopRecognition(VoiceUIClientID::AVS_SOLUTION));
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  ASSERT_EQ(true, wake_word_observer_avs_->StopRecognitionCompleteCalled);
  ASSERT_EQ(true, wake_word_observer_qc_->StopRecognitionCompleteCalled);

  ASSERT_EQ(true, wake_word_->RemoveObserver(VoiceUIClientID::MODULAR_SOLUTION));
  ASSERT_EQ(true, wake_word_->RemoveObserver(VoiceUIClientID::AVS_SOLUTION));
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  ASSERT_EQ(true, wake_word_observer_avs_->ObserverRemovedCalled);
  ASSERT_EQ(true, wake_word_observer_qc_->ObserverRemovedCalled);

  syslog(LOG_INFO, "***** VoiceUIWakewordTest::MethodTestMultiWakewordTestManual - END");
}
// Not automated : Needs Manual Input
TEST_F(VoiceUIWakewordTest, MethodTestMultiWakewordDOAManual) {
  syslog(LOG_INFO, "***** VoiceUIWakewordTest::MethodTestMultiWakewordDOA - START");
  ASSERT_NE(nullptr, wake_word_);
  auto wake_word_observer_avs_ = std::make_shared<MockWakeWordObserver>();
  auto wake_word_observer_qc_ = std::make_shared<MockWakeWordObserver>();

  ASSERT_EQ(true, wake_word_->AddObserver(VoiceUIClientID::AVS_SOLUTION, wake_word_observer_avs_));
  ASSERT_EQ(true, wake_word_->AddObserver(VoiceUIClientID::MODULAR_SOLUTION, wake_word_observer_qc_));

  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  ASSERT_EQ(true, wake_word_observer_avs_->ObserverAddedCalled);
  ASSERT_EQ(true, wake_word_observer_qc_->ObserverAddedCalled);

  ASSERT_EQ(0, wake_word_->StartRecognition(VoiceUIClientID::MODULAR_SOLUTION));
  ASSERT_EQ(0, wake_word_->StartRecognition(VoiceUIClientID::AVS_SOLUTION));
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  ASSERT_EQ(true, wake_word_observer_avs_->StartRecognitionCompleteCalled);
  ASSERT_EQ(true, wake_word_observer_qc_->StartRecognitionCompleteCalled);

  cout << "Start Speaking... Both Alexa and SnapDragon are listening.... ";

  char c;
  bool loop = true;
  bool innerLoopForKwd = true;
  while (loop) {
    while (innerLoopForKwd) {
      //check for Keyword Detection every 500ms
      cout << "ALEXA  KEYWORD DOA BEFORE KWD %d\n" << wake_word_->GetDOADirection(VoiceUIClientID::AVS_SOLUTION);
      cout << "SNAPDRAGON  KEYWORD DOA BEFORE KWD %d\n" << wake_word_->GetDOADirection(VoiceUIClientID::MODULAR_SOLUTION);

      std::this_thread::sleep_for(std::chrono::milliseconds(500));
      if (wake_word_observer_avs_->OnKeywordDetectedCalled) {
        cout << "ALEXA Keyword Detected!!!";
        cout << "ALEXA  KEYWORD DOA At KWD or LAB CAPTURE %d\n" << wake_word_->GetDOADirection(VoiceUIClientID::AVS_SOLUTION);
        break;
      }
      if (wake_word_observer_qc_->OnKeywordDetectedCalled) {
        cout << "SNAPDRAGON Keyword Detected!!!";
        cout << "SNAPDRAGON  KEYWORD DOA AT KWD or LAB CAPTURE %d\n" << wake_word_->GetDOADirection(VoiceUIClientID::MODULAR_SOLUTION);
        break;
      }
    }

    cout << "LAB Capture for 5 sec";
    cout << "ALEXA  KEYWORD DOA LAB CAPTURE %d\n" << wake_word_->GetDOADirection(VoiceUIClientID::AVS_SOLUTION);
    cout << "SNAPDRAGON  KEYWORD DOA LAB CAPTURE %d\n" << wake_word_->GetDOADirection(VoiceUIClientID::MODULAR_SOLUTION);
    std::this_thread::sleep_for(std::chrono::milliseconds(2000));
    cout << "ALEXA  KEYWORD DOA LAB CAPTURE %d\n" << wake_word_->GetDOADirection(VoiceUIClientID::AVS_SOLUTION);
    cout << "SNAPDRAGON  KEYWORD DOA LAB CAPTURE %d\n" << wake_word_->GetDOADirection(VoiceUIClientID::MODULAR_SOLUTION);
    std::this_thread::sleep_for(std::chrono::milliseconds(2000));

    //Stop Audio Capture
    if (wake_word_observer_qc_->OnKeywordDetectedCalled) {
      wake_word_observer_qc_->OnKeywordDetectedCalled = false;
      wake_word_->StopAudioCapture(VoiceUIClientID::MODULAR_SOLUTION);
      cout << "ALEXA  KEYWORD DOA AFTER LAB CAPTURE %d" << wake_word_->GetDOADirection(VoiceUIClientID::MODULAR_SOLUTION);

    } else {
      wake_word_observer_avs_->OnKeywordDetectedCalled = false;
      wake_word_->StopAudioCapture(VoiceUIClientID::AVS_SOLUTION);
      cout << "ALEXA  KEYWORD DOA AFTER LAB CAPTURE %d" << wake_word_->GetDOADirection(VoiceUIClientID::AVS_SOLUTION);
    }

    cout << "Playing back LAB DATA  Every 10 sec\n";
    //TODO:
    system("paplay --raw --rate 16000 --channels 1 --format s16le -v /data/voice-ui-framework/SVA/lab_capture_file_01");

    cout << "Press y to continue or n to exit";
    cin >> c;
    if (c == 'y')
      continue;
    else
      break;
  }

  ASSERT_EQ(0, wake_word_->StopRecognition(VoiceUIClientID::MODULAR_SOLUTION));
  ASSERT_EQ(0, wake_word_->StopRecognition(VoiceUIClientID::AVS_SOLUTION));
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  ASSERT_EQ(true, wake_word_observer_avs_->StopRecognitionCompleteCalled);
  ASSERT_EQ(true, wake_word_observer_qc_->StopRecognitionCompleteCalled);

  ASSERT_EQ(true, wake_word_->RemoveObserver(VoiceUIClientID::MODULAR_SOLUTION));
  ASSERT_EQ(true, wake_word_->RemoveObserver(VoiceUIClientID::AVS_SOLUTION));
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  ASSERT_EQ(true, wake_word_observer_avs_->ObserverRemovedCalled);
  ASSERT_EQ(true, wake_word_observer_qc_->ObserverRemovedCalled);

  syslog(LOG_INFO, "***** VoiceUIWakewordTest::MethodTestMultiWakewordDOAManual - END");
}

/*
TEST_F(VoiceUIWakewordTest, MethodTestWakewordAddObserver) {
  syslog(LOG_INFO, "***** VoiceUIWakewordTest::MethodTestWakewordAddObserver - START");

  auto wake_word_observer_qc_ = std::make_shared<MockWakeWordObserver>();

  //Positive cases
  ASSERT_EQ(true, wake_word_->StartRecognition(VoiceUIClientID::AVS_SOLUTION));
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  ASSERT_EQ(true, wake_word_observer_avs_->StartRecognitionCompleteCalled);

  ASSERT_EQ(true, wake_word_->StartRecognition(VoiceUIClientID::MODULAR_SOLUTION));
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  ASSERT_EQ(true, wake_word_observer_qc_->StartRecognitionCompleteCalled);

  ASSERT_EQ(true, wake_word_->RemoveObserver(VoiceUIClientID::AVS_SOLUTION));
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  ASSERT_EQ(true, wake_word_observer_avs_->ObserverRemovedCalled);

  ASSERT_EQ(true, wake_word_->RemoveObserver(VoiceUIClientID::MODULAR_SOLUTION));
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  ASSERT_EQ(true, wake_word_observer_qc_->ObserverRemovedCalled);

  syslog(LOG_INFO, "***** VoiceUIWakewordTest::MethodTestWakewordAddObserver - END");
}
*/
}  // namespace

int main(int argc, char** argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
