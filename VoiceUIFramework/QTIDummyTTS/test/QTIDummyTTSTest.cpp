/* Copyright (c) 2018, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    QTIDummyTTSTest.cpp
 *  @brief   brief description
 *
 *  DESCRIPTION
 *    description
 ***************************************************************/
#include <QTIDummyTTS/QTIDummyTTS.h>
#include <condition_variable>
#include <iostream>
#include <mutex>
#include "gtest/gtest.h"


namespace voiceUIFramework {
namespace voiceUIClient {

class TTSCallBack : public TTSCallBackInterface {
 public:
  bool IsTTSInitialized = false;
  bool IsTTSSpeakBegin = false;
  bool IsTTSSpeakDone = false;
  bool IsTTSError = false;

  std::mutex mutex;
  std::unique_lock<std::mutex> lock;
  std::condition_variable cond;

  TTSCallBack() {
  }

  bool WaitForObserver() {
    cond.wait(lock);
    return true;
  }

  //broadcast TTS ready to use
  void TTSInitialized() {
    lock = std::unique_lock<std::mutex>(mutex);
    IsTTSInitialized = true;
    syslog(LOG_INFO, "TTSInitialized");
    cond.notify_one();
  }

  //broadcast TTS speak has begun
  void TTSSpeakBegin() {
    IsTTSSpeakBegin = true;
    syslog(LOG_INFO, "TTSSpeakBegin");
    cond.notify_one();
  }

  //broadcast TTS utterance has ended (currently not using )
  void TTSSpeakEnd() {
    syslog(LOG_INFO, "TTSSpeakEnd");
    cond.notify_one();
  }

  //broadcast all TTS utterance queue is done
  void TTSSpeakDone() {
    IsTTSSpeakDone = true;
    syslog(LOG_INFO, "TTSSpeakDone");
    cond.notify_one();
  }

  //broadcast TTS Errors
  void TTSError() {
    IsTTSError = true;
    syslog(LOG_ERR, "TTSError");
    cond.notify_one();
  }

  ~TTSCallBack() {
  }
};
}  // namespace voiceUIClient
}  // namespace voiceUIFramework

namespace voiceUIFramework {
namespace ttsEngineDummy {

// The fixture for testing class QTIDummyTTS.
class QTIDummyTTSTest : public ::testing::Test {
 protected:
  QTIDummyTTSTest() {
    // You can do set-up work for each test here.
  }

  virtual ~QTIDummyTTSTest() {
    // You can do clean-up work that doesn't throw exceptions here.
  }
  std::shared_ptr<TTSInterface> tts_client_;
  std::shared_ptr<TTSCallBack> tts_callback_;

  // If the constructor and destructor are not enough for setting up
  // and cleaning up each test, you can define the following methods:

  virtual void SetUp() {
    tts_callback_ = std::make_shared<voiceUIFramework::voiceUIClient::TTSCallBack>();
    tts_client_ = std::make_shared<voiceUIFramework::ttsEngineDummy::QTIDummyTTS>(tts_callback_);
  }

  virtual void TearDown() {
  }
};

TEST_F(QTIDummyTTSTest, Initializetest) {
  tts_client_->Initialize();
  EXPECT_TRUE(tts_callback_->IsTTSInitialized);
  tts_callback_->IsTTSInitialized = false;
}

TEST_F(QTIDummyTTSTest, Play_UNKNOWN_Intent) {
  tts_client_->Initialize();
  EXPECT_TRUE(tts_callback_->IsTTSInitialized);

  TTS_Intents temp_intent = TTS_Intents::UNKNOWN;

  EXPECT_FALSE(tts_client_->Speak(temp_intent));
  EXPECT_FALSE(tts_callback_->IsTTSSpeakBegin);
  EXPECT_FALSE(tts_callback_->IsTTSSpeakDone);
  tts_callback_->IsTTSInitialized = false;
}

TEST_F(QTIDummyTTSTest, Play_Light_Intents) {
  tts_client_->Initialize();
  EXPECT_TRUE(tts_callback_->IsTTSInitialized);

  TTS_Intents temp_intent = TTS_Intents::UNKNOWN;

  temp_intent = TTS_Intents::Bedroom_Lights_OFF;
  EXPECT_TRUE(tts_client_->Speak(temp_intent));
  EXPECT_TRUE(tts_callback_->WaitForObserver());
  EXPECT_TRUE(tts_callback_->IsTTSSpeakBegin);
  EXPECT_TRUE(tts_callback_->WaitForObserver());
  EXPECT_TRUE(tts_callback_->IsTTSSpeakDone);
  tts_callback_->IsTTSSpeakBegin = false;
  tts_callback_->IsTTSSpeakDone = false;
  tts_callback_->IsTTSInitialized = false;

  temp_intent = TTS_Intents::Bedroom_Lights_ON;
  EXPECT_TRUE(tts_client_->Speak(temp_intent));
  EXPECT_TRUE(tts_callback_->WaitForObserver());
  EXPECT_TRUE(tts_callback_->IsTTSSpeakBegin);
  EXPECT_TRUE(tts_callback_->WaitForObserver());
  EXPECT_TRUE(tts_callback_->IsTTSSpeakDone);
  tts_callback_->IsTTSSpeakBegin = false;
  tts_callback_->IsTTSSpeakDone = false;
  tts_callback_->IsTTSInitialized = false;

  temp_intent = TTS_Intents::Kitchen_lights_OFF;
  EXPECT_TRUE(tts_client_->Speak(temp_intent));
  EXPECT_TRUE(tts_callback_->WaitForObserver());
  EXPECT_TRUE(tts_callback_->IsTTSSpeakBegin);
  EXPECT_TRUE(tts_callback_->WaitForObserver());
  EXPECT_TRUE(tts_callback_->IsTTSSpeakDone);
  tts_callback_->IsTTSSpeakBegin = false;
  tts_callback_->IsTTSSpeakDone = false;
  tts_callback_->IsTTSInitialized = false;

  temp_intent = TTS_Intents::Kitchen_Lights_ON;
  EXPECT_TRUE(tts_client_->Speak(temp_intent));
  EXPECT_TRUE(tts_callback_->WaitForObserver());
  EXPECT_TRUE(tts_callback_->IsTTSSpeakBegin);
  EXPECT_TRUE(tts_callback_->WaitForObserver());
  EXPECT_TRUE(tts_callback_->IsTTSSpeakDone);
  tts_callback_->IsTTSSpeakBegin = false;
  tts_callback_->IsTTSSpeakDone = false;
  tts_callback_->IsTTSInitialized = false;

  temp_intent = TTS_Intents::Living_Room_Lights_OFF;
  EXPECT_TRUE(tts_client_->Speak(temp_intent));
  EXPECT_TRUE(tts_callback_->WaitForObserver());
  EXPECT_TRUE(tts_callback_->IsTTSSpeakBegin);
  EXPECT_TRUE(tts_callback_->WaitForObserver());
  EXPECT_TRUE(tts_callback_->IsTTSSpeakDone);
  tts_callback_->IsTTSSpeakBegin = false;
  tts_callback_->IsTTSSpeakDone = false;
  tts_callback_->IsTTSInitialized = false;

  temp_intent = TTS_Intents::Living_Room_Lights_ON;
  EXPECT_TRUE(tts_client_->Speak(temp_intent));
  EXPECT_TRUE(tts_callback_->WaitForObserver());
  EXPECT_TRUE(tts_callback_->IsTTSSpeakBegin);
  EXPECT_TRUE(tts_callback_->WaitForObserver());
  EXPECT_TRUE(tts_callback_->IsTTSSpeakDone);
  tts_callback_->IsTTSSpeakBegin = false;
  tts_callback_->IsTTSSpeakDone = false;
  tts_callback_->IsTTSInitialized = false;

  temp_intent = TTS_Intents::Lights_OFF;
  EXPECT_TRUE(tts_client_->Speak(temp_intent));
  EXPECT_TRUE(tts_callback_->WaitForObserver());
  EXPECT_TRUE(tts_callback_->IsTTSSpeakBegin);
  EXPECT_TRUE(tts_callback_->WaitForObserver());
  EXPECT_TRUE(tts_callback_->IsTTSSpeakDone);
  tts_callback_->IsTTSSpeakBegin = false;
  tts_callback_->IsTTSSpeakDone = false;
  tts_callback_->IsTTSInitialized = false;

  temp_intent = TTS_Intents::Lights_ON;
  EXPECT_TRUE(tts_client_->Speak(temp_intent));
  EXPECT_TRUE(tts_callback_->WaitForObserver());
  EXPECT_TRUE(tts_callback_->IsTTSSpeakBegin);
  EXPECT_TRUE(tts_callback_->WaitForObserver());
  EXPECT_TRUE(tts_callback_->IsTTSSpeakDone);
  tts_callback_->IsTTSSpeakBegin = false;
  tts_callback_->IsTTSSpeakDone = false;
  tts_callback_->IsTTSInitialized = false;
}

TEST_F(QTIDummyTTSTest, Play_Voulme_and_Mute_Intents) {
  tts_client_->Initialize();
  EXPECT_TRUE(tts_callback_->IsTTSInitialized);

  TTS_Intents temp_intent = TTS_Intents::UNKNOWN;

  temp_intent = TTS_Intents::Device_muted;
  EXPECT_TRUE(tts_client_->Speak(temp_intent));
  EXPECT_TRUE(tts_callback_->WaitForObserver());
  EXPECT_TRUE(tts_callback_->IsTTSSpeakBegin);
  EXPECT_TRUE(tts_callback_->WaitForObserver());
  EXPECT_TRUE(tts_callback_->IsTTSSpeakDone);
  tts_callback_->IsTTSSpeakBegin = false;
  tts_callback_->IsTTSSpeakDone = false;
  tts_callback_->IsTTSInitialized = false;

  temp_intent = TTS_Intents::Device_unmuted;
  EXPECT_TRUE(tts_client_->Speak(temp_intent));
  EXPECT_TRUE(tts_callback_->WaitForObserver());
  EXPECT_TRUE(tts_callback_->IsTTSSpeakBegin);
  EXPECT_TRUE(tts_callback_->WaitForObserver());
  EXPECT_TRUE(tts_callback_->IsTTSSpeakDone);
  tts_callback_->IsTTSSpeakBegin = false;
  tts_callback_->IsTTSSpeakDone = false;
  tts_callback_->IsTTSInitialized = false;

  temp_intent = TTS_Intents::Volume_changed;
  EXPECT_TRUE(tts_client_->Speak(temp_intent));
  EXPECT_TRUE(tts_callback_->WaitForObserver());
  EXPECT_TRUE(tts_callback_->IsTTSSpeakBegin);
  EXPECT_TRUE(tts_callback_->WaitForObserver());
  EXPECT_TRUE(tts_callback_->IsTTSSpeakDone);
  tts_callback_->IsTTSSpeakBegin = false;
  tts_callback_->IsTTSSpeakDone = false;
  tts_callback_->IsTTSInitialized = false;

  temp_intent = TTS_Intents::Volume_Decreased;
  EXPECT_TRUE(tts_client_->Speak(temp_intent));
  EXPECT_TRUE(tts_callback_->WaitForObserver());
  EXPECT_TRUE(tts_callback_->IsTTSSpeakBegin);
  EXPECT_TRUE(tts_callback_->WaitForObserver());
  EXPECT_TRUE(tts_callback_->IsTTSSpeakDone);
  tts_callback_->IsTTSSpeakBegin = false;
  tts_callback_->IsTTSSpeakDone = false;
  tts_callback_->IsTTSInitialized = false;

  temp_intent = TTS_Intents::Volume_Increased;
  EXPECT_TRUE(tts_client_->Speak(temp_intent));
  EXPECT_TRUE(tts_callback_->WaitForObserver());
  EXPECT_TRUE(tts_callback_->IsTTSSpeakBegin);
  EXPECT_TRUE(tts_callback_->WaitForObserver());
  EXPECT_TRUE(tts_callback_->IsTTSSpeakDone);
  tts_callback_->IsTTSSpeakBegin = false;
  tts_callback_->IsTTSSpeakDone = false;
  tts_callback_->IsTTSInitialized = false;
}

TEST_F(QTIDummyTTSTest, Play_Security_Door_Intents) {
  tts_client_->Initialize();
  EXPECT_TRUE(tts_callback_->IsTTSInitialized);

  TTS_Intents temp_intent = TTS_Intents::UNKNOWN;

  temp_intent = TTS_Intents::Front_Door_Closed;
  EXPECT_TRUE(tts_client_->Speak(temp_intent));
  EXPECT_TRUE(tts_callback_->WaitForObserver());
  EXPECT_TRUE(tts_callback_->IsTTSSpeakBegin);
  EXPECT_TRUE(tts_callback_->WaitForObserver());
  EXPECT_TRUE(tts_callback_->IsTTSSpeakDone);
  tts_callback_->IsTTSSpeakBegin = false;
  tts_callback_->IsTTSSpeakDone = false;
  tts_callback_->IsTTSInitialized = false;

  temp_intent = TTS_Intents::Front_Door_Opened;
  EXPECT_TRUE(tts_client_->Speak(temp_intent));
  EXPECT_TRUE(tts_callback_->WaitForObserver());
  EXPECT_TRUE(tts_callback_->IsTTSSpeakBegin);
  EXPECT_TRUE(tts_callback_->WaitForObserver());
  EXPECT_TRUE(tts_callback_->IsTTSSpeakDone);
  tts_callback_->IsTTSSpeakBegin = false;
  tts_callback_->IsTTSSpeakDone = false;
  tts_callback_->IsTTSInitialized = false;

  temp_intent = TTS_Intents::Garage_Door_Closed;
  EXPECT_TRUE(tts_client_->Speak(temp_intent));
  EXPECT_TRUE(tts_callback_->WaitForObserver());
  EXPECT_TRUE(tts_callback_->IsTTSSpeakBegin);
  EXPECT_TRUE(tts_callback_->WaitForObserver());
  EXPECT_TRUE(tts_callback_->IsTTSSpeakDone);
  tts_callback_->IsTTSSpeakBegin = false;
  tts_callback_->IsTTSSpeakDone = false;
  tts_callback_->IsTTSInitialized = false;

  temp_intent = TTS_Intents::Garage_Door_Opened;
  EXPECT_TRUE(tts_client_->Speak(temp_intent));
  EXPECT_TRUE(tts_callback_->WaitForObserver());
  EXPECT_TRUE(tts_callback_->IsTTSSpeakBegin);
  EXPECT_TRUE(tts_callback_->WaitForObserver());
  EXPECT_TRUE(tts_callback_->IsTTSSpeakDone);
  tts_callback_->IsTTSSpeakBegin = false;
  tts_callback_->IsTTSSpeakDone = false;
  tts_callback_->IsTTSInitialized = false;
}

TEST_F(QTIDummyTTSTest, Play_Telephone_Intents) {
  tts_client_->Initialize();
  EXPECT_TRUE(tts_callback_->IsTTSInitialized);

  TTS_Intents temp_intent = TTS_Intents::UNKNOWN;

  temp_intent = TTS_Intents::Call_Ended;
  EXPECT_TRUE(tts_client_->Speak(temp_intent));
  EXPECT_TRUE(tts_callback_->WaitForObserver());
  EXPECT_TRUE(tts_callback_->IsTTSSpeakBegin);
  EXPECT_TRUE(tts_callback_->WaitForObserver());
  EXPECT_TRUE(tts_callback_->IsTTSSpeakDone);
  tts_callback_->IsTTSSpeakBegin = false;
  tts_callback_->IsTTSSpeakDone = false;
  tts_callback_->IsTTSInitialized = false;

  temp_intent = TTS_Intents::Incoming_call_accepted;
  EXPECT_TRUE(tts_client_->Speak(temp_intent));
  EXPECT_TRUE(tts_callback_->WaitForObserver());
  EXPECT_TRUE(tts_callback_->IsTTSSpeakBegin);
  EXPECT_TRUE(tts_callback_->WaitForObserver());
  EXPECT_TRUE(tts_callback_->IsTTSSpeakDone);
  tts_callback_->IsTTSSpeakBegin = false;
  tts_callback_->IsTTSSpeakDone = false;
  tts_callback_->IsTTSInitialized = false;

  temp_intent = TTS_Intents::Incoming_call_Rejected;
  EXPECT_TRUE(tts_client_->Speak(temp_intent));
  EXPECT_TRUE(tts_callback_->WaitForObserver());
  EXPECT_TRUE(tts_callback_->IsTTSSpeakBegin);
  EXPECT_TRUE(tts_callback_->WaitForObserver());
  EXPECT_TRUE(tts_callback_->IsTTSSpeakDone);
  tts_callback_->IsTTSSpeakBegin = false;
  tts_callback_->IsTTSSpeakDone = false;
  tts_callback_->IsTTSInitialized = false;
}

TEST_F(QTIDummyTTSTest, Play_Set_Control_device_state_Intents) {
  tts_client_->Initialize();
  EXPECT_TRUE(tts_callback_->IsTTSInitialized);

  TTS_Intents temp_intent = TTS_Intents::UNKNOWN;

  temp_intent = TTS_Intents::Thermostat_OFF;
  EXPECT_TRUE(tts_client_->Speak(temp_intent));
  EXPECT_TRUE(tts_callback_->WaitForObserver());
  EXPECT_TRUE(tts_callback_->IsTTSSpeakBegin);
  EXPECT_TRUE(tts_callback_->WaitForObserver());
  EXPECT_TRUE(tts_callback_->IsTTSSpeakDone);
  tts_callback_->IsTTSSpeakBegin = false;
  tts_callback_->IsTTSSpeakDone = false;
  tts_callback_->IsTTSInitialized = false;

  temp_intent = TTS_Intents::Thermostat_ON;
  EXPECT_TRUE(tts_client_->Speak(temp_intent));
  EXPECT_TRUE(tts_callback_->WaitForObserver());
  EXPECT_TRUE(tts_callback_->IsTTSSpeakBegin);
  EXPECT_TRUE(tts_callback_->WaitForObserver());
  EXPECT_TRUE(tts_callback_->IsTTSSpeakDone);
  tts_callback_->IsTTSSpeakBegin = false;
  tts_callback_->IsTTSSpeakDone = false;
  tts_callback_->IsTTSInitialized = false;
}

TEST_F(QTIDummyTTSTest, Play_Thermostat_changed__Intents) {
  tts_client_->Initialize();
  EXPECT_TRUE(tts_callback_->IsTTSInitialized);

  TTS_Intents temp_intent = TTS_Intents::UNKNOWN;

  temp_intent = TTS_Intents::Thermostat_temp_changed;
  EXPECT_TRUE(tts_client_->Speak(temp_intent));
  EXPECT_TRUE(tts_callback_->WaitForObserver());
  EXPECT_TRUE(tts_callback_->IsTTSSpeakBegin);
  EXPECT_TRUE(tts_callback_->WaitForObserver());
  EXPECT_TRUE(tts_callback_->IsTTSSpeakDone);
  tts_callback_->IsTTSSpeakBegin = false;
  tts_callback_->IsTTSSpeakDone = false;
  tts_callback_->IsTTSInitialized = false;
}

}  // namespace ttsEngineDummy
}  // namespace voiceUIFramework

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
