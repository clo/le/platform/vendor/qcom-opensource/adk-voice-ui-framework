/* Copyright (c) 2018, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    QTIDummyTTS.cpp
 *  @brief   Dummy TTS  that plays prompt files
 *
 *  DESCRIPTION
 *
 ***************************************************************/
#include <QTIDummyTTS/QTIDummyTTS.h>
#include <iostream>

std::string Bedroom_Lights_OFF = "file:///data/voice-ui-framework/Bedroom_Lights_OFF.mp3";
std::string Bedroom_Lights_ON = "file:///data/voice-ui-framework/Bedroom_Lights_ON.mp3";
std::string Call_Ended = "file:///data/voice-ui-framework/Call_Ended.mp3";
std::string Device_muted = "file:///data/voice-ui-framework/Device_has_been_muted.mp3";
std::string Device_unmuted = "file:///data/voice-ui-framework/Device_has_been_unmuted.mp3";
std::string Front_Door_Closed = "file:///data/voice-ui-framework/Front_Door_Closed.mp3";
std::string Front_Door_Opened = "file:///data/voice-ui-framework/Front_Door_Opened.mp3";
std::string Garage_Door_Closed = "file:///data/voice-ui-framework/Garage_Door_Closed.mp3";
std::string Garage_Door_Opened = "file:///data/voice-ui-framework/Garage_Door_Opened.mp3";
std::string Incoming_call_accepted = "file:///data/voice-ui-framework/Incoming_call_accepted.mp3";
std::string Incoming_call_Rejected = "file:///data/voice-ui-framework/Incoming_call_Rejected.mp3";
std::string Kitchen_lights_OFF = "file:///data/voice-ui-framework/Kitchen_lights_OFF.mp3";
std::string Kitchen_Lights_ON = "file:///data/voice-ui-framework/Kitchen_Lights_ON.mp3";
std::string Lights_OFF = "file:///data/voice-ui-framework/Lights_OFF.mp3";
std::string Lights_ON = "file:///data/voice-ui-framework/Lights_ON.mp3";
std::string Living_Room_Lights_OFF = "file:///data/voice-ui-framework/Living_Room_Lights_OFF.mp3";
std::string Living_Room_Lights_ON = "file:///data/voice-ui-framework/Living_Room_Lights_ON.mp3";
std::string Thermostat_OFF = "file:///data/voice-ui-framework/Thermostat_OFF.mp3";
std::string Thermostat_ON = "file:///data/voice-ui-framework/Thermostat_ON.mp3";
std::string Thermostat_temp_changed = "file:///data/voice-ui-framework/Thermostat_temperature_has_changed.mp3";
std::string Volume_changed = "file:///data/voice-ui-framework/Volume_changed.mp3";
std::string Volume_Decreased = "file:///data/voice-ui-framework/Volume_Decreased.mp3";
std::string Volume_Increased = "file:///data/voice-ui-framework/Volume_Increased.mp3";

namespace voiceUIFramework {
namespace ttsEngineDummy {

QTIDummyTTS::QTIDummyTTS() {
  syslog(LOG_DEBUG, "+++QTIDummyTTS::QTIDummyTTS - QTIDummyTTS Built!");
}

QTIDummyTTS::QTIDummyTTS(std::shared_ptr<voiceUIFramework::voiceUIClient::TTSCallBackInterface> tts_callback)
    : tts_callback_{tts_callback} {
  syslog(LOG_DEBUG, "+++QTIDummyTTS::QTIDummyTTS - QTIDummyTTS Built!");
}

bool QTIDummyTTS::Initialize() {
  syslog(LOG_DEBUG, "QTIDummyTTS::Initialize");

  player = adk::MediaPlayer::Create("VoiceUI-QTIDummyTTS", adk::MediaPlayer::StreamType::Cue);

  if (!player) {
    syslog(LOG_ERR, "QTIDummyTTS::Initialize - Error to create Media player for TTS");
    return false;
  }

  syslog(LOG_DEBUG, "QTIDummyTTS::Initialize - Media player for TTS is Created");

  //register callbacks for Mediaplayer
  auto onEndOfStream = std::bind(std::mem_fn(&QTIDummyTTS::OnEndOfStream), this);
  auto onError = std::bind(std::mem_fn(&QTIDummyTTS::OnError), this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);
  auto OnStateChanged = std::bind(std::mem_fn(&QTIDummyTTS::OnStateChanged), this, std::placeholders::_1);

  player->SetOnEndOfStreamCb(onEndOfStream);
  player->SetOnErrorCb(onError);
  player->SetOnStateChangedCb(OnStateChanged);

  //Notify ModularManager that TTS has Initialized
  tts_callback_->TTSInitialized();

  return true;
}

bool QTIDummyTTS::Speak() {
  syslog(LOG_DEBUG, "QTIDummyTTS::Speak");
  return true;
}

bool QTIDummyTTS::Speak(TTS_Intents intent) {
  syslog(LOG_DEBUG, "QTIDummyTTS::Speak(TTS_Intents &intent)");

  if (!player) {
    syslog(LOG_DEBUG, "QTIDummyTTS::Speak(TTS_Intents &intent) - ADK- Media player for TTS is nullptr");
    return false;
  }

  bool seturi_res = false;

  switch (intent) {
    case TTS_Intents::Bedroom_Lights_OFF:
      seturi_res = player->SetSource(Bedroom_Lights_OFF);
      break;
    case TTS_Intents::Bedroom_Lights_ON:
      seturi_res = player->SetSource(Bedroom_Lights_ON);
      break;
    case TTS_Intents::Call_Ended:
      seturi_res = player->SetSource(Call_Ended);
      break;
    case TTS_Intents::Device_muted:
      seturi_res = player->SetSource(Device_muted);
      break;
    case TTS_Intents::Device_unmuted:
      seturi_res = player->SetSource(Device_unmuted);
      break;
    case TTS_Intents::Front_Door_Closed:
      seturi_res = player->SetSource(Front_Door_Closed);
      break;
    case TTS_Intents::Front_Door_Opened:
      seturi_res = player->SetSource(Front_Door_Opened);
      break;
    case TTS_Intents::Garage_Door_Closed:
      seturi_res = player->SetSource(Garage_Door_Closed);
      break;
    case TTS_Intents::Garage_Door_Opened:
      seturi_res = player->SetSource(Garage_Door_Opened);
      break;
    case TTS_Intents::Incoming_call_accepted:
      seturi_res = player->SetSource(Incoming_call_accepted);
      break;
    case TTS_Intents::Incoming_call_Rejected:
      seturi_res = player->SetSource(Incoming_call_Rejected);
      break;
    case TTS_Intents::Kitchen_lights_OFF:
      seturi_res = player->SetSource(Kitchen_lights_OFF);
      break;
    case TTS_Intents::Kitchen_Lights_ON:
      seturi_res = player->SetSource(Kitchen_Lights_ON);
      break;
    case TTS_Intents::Lights_OFF:
      seturi_res = player->SetSource(Lights_OFF);
      break;
    case TTS_Intents::Lights_ON:
      seturi_res = player->SetSource(Lights_ON);
      break;
    case TTS_Intents::Living_Room_Lights_OFF:
      seturi_res = player->SetSource(Living_Room_Lights_OFF);
      break;
    case TTS_Intents::Living_Room_Lights_ON:
      seturi_res = player->SetSource(Living_Room_Lights_ON);
      break;
    case TTS_Intents::Thermostat_OFF:
      seturi_res = player->SetSource(Thermostat_OFF);
      break;
    case TTS_Intents::Thermostat_ON:
      seturi_res = player->SetSource(Thermostat_ON);
      break;
    case TTS_Intents::Thermostat_temp_changed:
      seturi_res = player->SetSource(Thermostat_temp_changed);
      break;
    case TTS_Intents::Volume_changed:
      seturi_res = player->SetSource(Volume_changed);
      break;
    case TTS_Intents::Volume_Decreased:
      seturi_res = player->SetSource(Volume_Decreased);
      break;
    case TTS_Intents::Volume_Increased:
      seturi_res = player->SetSource(Volume_Increased);
      break;
    case TTS_Intents::UNKNOWN:
      seturi_res = player->SetSource("");
      break;
  }

  if (seturi_res) {
    syslog(LOG_DEBUG, "QTIDummyTTS::Speak(TTS_Intents &intent) - Seturi(source) done : Start playing the intent");
    auto res = player->Play();

    if (res == adk::MediaPlayer::Failure) {
      syslog(LOG_ERR, "QTIDummyTTS::Speak(TTS_Intents &intent)  :ADK-MP - play() failed");
      return false;
    }

  } else {
    syslog(LOG_WARNING, "QTIDummyTTS::Speak(TTS_Intents &intent) - player->SetUri - Failed to  SetURI res:No matching intents found");
    return false;
  }

  return true;
}

bool QTIDummyTTS::StopSpeaking() {
  syslog(LOG_DEBUG, "QTIDummyTTS::StopSpeaking");

  //Do Nothing
  return true;
}

bool QTIDummyTTS::IsSpeaking() {
  syslog(LOG_DEBUG, "QTIDummyTTS::IsSpeaking");
  //Do Nothing
  return true;
}

void QTIDummyTTS::Shutdown() {
  syslog(LOG_DEBUG, "QTIDummyTTS::Shutdown");
  //delete the media player instance
}

QTIDummyTTS::~QTIDummyTTS() {
  syslog(LOG_DEBUG, "---QTIDummyTTS::~QTIDummyTTS");
}

void QTIDummyTTS::OnEndOfStream(void) {
  syslog(LOG_INFO, "QTIDummyTTS::OnEndOfStream");
  syslog(LOG_DEBUG, "QTIDummyTTS : OnEndOfStream() : stop Called");

  auto res = player->Stop();

  if (res == adk::MediaPlayer::Failure) {
    syslog(LOG_ERR, "QTIDummyTTS : OnEndOfStream() : stop failed reason:adk-MediaPlayer Failure");

  } else {
    syslog(LOG_DEBUG, "QTIDummyTTS : OnEndOfStream() : stop success");
  }

}

void QTIDummyTTS::OnError(adk::MediaPlayer::ErrorType error_type,
    std::string error_msg, std::string debug_msg) {
  std::cout << "QTIDummyTTS::OnError_Adk error_type= " << error_type << std::endl;
  std::cout << "QTIDummyTTS::OnError_Adk error_msg= " << error_msg << std::endl;

  syslog(LOG_INFO, "QTIDummyTTS :ADK-MP::OnError_Adk error_type=%d", error_type);
  syslog(LOG_INFO, "QTIDummyTTS :ADK-MP::OnError_Adk error_msg=%s", error_msg.c_str());
  syslog(LOG_INFO, "QTIDummyTTS :ADK-MP::OnError_Adk debug_msg=%s", debug_msg.c_str());

  //Notify ModularManager that TTS has failed
  if (error_type == adk::MediaPlayer::ErrorType::GENERAL) {
    tts_callback_->TTSError();
  }
}

void QTIDummyTTS::OnStateChanged(adk::MediaPlayer::State new_state) {
  syslog(LOG_DEBUG, "QTIDummyTTS::OnStateChanged");

  if (new_state == adk::MediaPlayer::State::Playing) {
    syslog(LOG_DEBUG, "QTIDummyTTS :ADK-MP::OnStateChanged : State::Playing");

    //Notify ModularManager that TTS has started
    tts_callback_->TTSSpeakBegin();
  } else if (new_state == adk::MediaPlayer::State::Stopped) {
    syslog(LOG_DEBUG, "QTIDummyTTS :ADK-MP::OnStateChanged : State::Stopped");
    //Notify ModularManager that TTS has ended
    tts_callback_->TTSSpeakDone();
  }
}

}  // namespace ttsEngineDummy
}  // namespace voiceUIFramework
