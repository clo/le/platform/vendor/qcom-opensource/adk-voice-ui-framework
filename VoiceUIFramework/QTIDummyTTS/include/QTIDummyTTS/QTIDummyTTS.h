/* Copyright (c) 2018, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    QTIDummyTTS.h
 *  @brief   Dummy TTS  that plays prompt files
 *
 *  DESCRIPTION
 *
 ***************************************************************/

#ifndef CODE_VOICEUIFRAMEWORK_QTIDummyTTS_INCLUDE_QTIDummyTTS_QTIDUMMYTTS_H_
#define CODE_VOICEUIFRAMEWORK_QTIDummyTTS_INCLUDE_QTIDummyTTS_QTIDUMMYTTS_H_

#include <VoiceUIClient/ModularSolution/TTS/TTSCallBackInterface.h>
#include <VoiceUIClient/ModularSolution/TTS/TTSInterface.h>
#include <adk/mediaplayer.h>
#include <syslog.h>
#include <functional>

namespace voiceUIFramework {
namespace ttsEngineDummy {

using namespace voiceUIFramework::voiceUIClient;

class QTIDummyTTS : public TTSInterface {
 public:
  //Adk-Mediaplayer instance
  std::unique_ptr<adk::MediaPlayer> player;
  QTIDummyTTS();
  QTIDummyTTS(std::shared_ptr<voiceUIFramework::voiceUIClient::TTSCallBackInterface> tts_callback);
  //Initializes TTS engine
  bool Initialize();
  //Speak
  bool Speak();
  //Speak intents
  bool Speak(TTS_Intents intent);
  //Stops Speaking immediately
  bool StopSpeaking();
  //Verify if TTS is speaking
  bool IsSpeaking();
  //Shutdown TTS Engine
  void Shutdown();
  //Destructor
  ~QTIDummyTTS();

 private:
  //functions to be used as callbacks from mediaplayer
  void OnEndOfStream(void);
  void OnError(adk::MediaPlayer::ErrorType error_type,
      std::string error_msg, std::string debug_msg);

  void OnStateChanged(adk::MediaPlayer::State new_state);
  std::shared_ptr<voiceUIFramework::voiceUIClient::TTSCallBackInterface> tts_callback_;
};

}  // namespace ttsEngineDummy
}  // namespace voiceUIFramework

#endif /* CODE_VOICEUIFRAMEWORK_QTIDummyTTS_INCLUDE_QTIDummyTTS_QTIDUMMYTTS_H_ */
