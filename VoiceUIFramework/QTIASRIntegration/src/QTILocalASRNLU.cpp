/* Copyright (c) 2018, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    QTILocalASRNLU.cpp
 *  @brief   Qualcomm Technologies, Inc local ASR / NLU Component
 *
 *  DESCRIPTION
 *    Qualcomm Technologies, Inc local ASR / NLU Component to be used on VoiceUI Modular Client
 ***************************************************************/

#include <QTIASRIntegration/QTILocalASRNLU.h>

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <cstring>
#include <thread>

#define VERBOSE
#define QC_KW

#define WORST_SCORE (-1e9)
#define FLOAT_SCORE(x) (((x) == WORST_SCORE) ? WORST_SCORE : (((float)x) / (1 << 16)))

#ifndef _WINDOWS
#ifndef _Q6_FIXES_
#include <getopt.h>
#include <unistd.h>
#endif
#endif

namespace voiceUIFramework {
namespace qtiASRClient {

// "RIFF"
#define WAV_IDENTIFIER_SIZE 4

// WAV format
#define WAV_HEADER_SIZE 44

#define FRAME_SAMPLES_10MS 160

enum {
  RESULT_SUCCESS = 0,
  RESULT_FAILURE,
  RESULT_ERROR_OPEN,
  RESULT_ERROR_READ,
  RESULT_ERROR_TOO_SHORT,
  RESULT_ERROR_TOO_BIG,
  RESULT_MAX
} RESULT_CODE;

///Function to Check
#define CHECK(x)                                                      \
  do {                                                                \
    int ret = x;                                                      \
    if (ret != ASR_SUCCESS) {                                         \
      printf("%s:%d " #x " returned %d \n", __FILE__, __LINE__, ret); \
      exit(EXIT_FAILURE);                                             \
    }                                                                 \
  } while (0)

struct wave_fmt {
  uint16_t wFormatTag;       // 2    Format code
  uint16_t nChannels;        // 2    Nc
  uint32_t nSamplesPerSec;   // 4    F
  uint32_t nAvgBytesPerSec;  // 4    F*M*Nc
  uint16_t nBlockAlign;      // 2    M*Nc
  uint16_t wBitsPerSample;   // 2    8*M (float data) or 16 (log-PCM data)

  // optional
  uint16_t cbSize;               // 2    Size of the extension: 22
  uint16_t wValidBitsPerSample;  // 2   at most 8*M
  uint32_t dwChannelMask;        //  4   Speaker position mask
  uint8_t SubFormat[16];         // 16   GUID (first two bytes are the data format code)
};

QTILocalASRNLU::QTILocalASRNLU(std::shared_ptr<voiceUIFramework::voiceUIClient::ASRCallBackInterface> asr_callback)
    : model_buffer_{nullptr}, asr_{nullptr}, asr_callback_{asr_callback}, asr_running_{false}, result_struct_size_{0}, get_struct_filled_size_{0}, show_input_sequence{false}, process_whole_file{false}, log_filename_{nullptr} {
  syslog(LOG_INFO, "+++++++QTILocalASRNLU Constructor called");

  //Read VoiceUI Database
  auto voice_ui_config = voiceUIFramework::voiceUIUtils::VoiceUIConfig::getInstance();
  if (voice_ui_config) {
    //Read Voice Dump status
    voicedump_enable_ = voice_ui_config->ReadWithDefault<bool>(true, voiceUIUtils::Keys::k_framework_voice_dump_status_);
    //Read Voice Dump Path
    voicedump_path_ = voice_ui_config->ReadWithDefault<std::string>("/data", voiceUIUtils::Keys::k_framework_voice_dump_path_);
    //Read ASR sound model
    asr_model_file_ = voice_ui_config->ReadWithDefault<std::string>("/data/voice-ui-framework/model.float.1.0_1.3.0.asr", voiceUIUtils::Keys::k_modular_qtiasr_soundmodel_);
    //ASR parameters
    asr_threshold_ = voice_ui_config->ReadWithDefault<int>(82, voiceUIUtils::Keys::k_modular_qtiasr_threshold_);
    asr_timeout_ = voice_ui_config->ReadWithDefault<int>(10000, voiceUIUtils::Keys::k_modular_qtiasr_timeout_);
    asr_max_pause_ = voice_ui_config->ReadWithDefault<int>(1000, voiceUIUtils::Keys::k_modular_qtiasr_maxpause_);

  } else {
    syslog(LOG_ERR, "VoiceUIConfig not Initialized");
    voicedump_enable_ = true;
    voicedump_path_ = "/data";
    asr_model_file_ = "/data/voice-ui-framework/model.float.1.0_1.3.0.asr";
    asr_threshold_ = 82;
    asr_timeout_ = 10000;
    asr_max_pause_ = 1000;
  }
  syslog(LOG_INFO, "Voice Dump Status = %d", voicedump_enable_);
  syslog(LOG_INFO, "Voice Dump for Local ASR Path = %s", voicedump_path_.c_str());
  syslog(LOG_INFO, "QTILocalASRNLU::QTILocalASRNLU() - ASR model File = %s", asr_model_file_.c_str());
  syslog(LOG_INFO, "QTILocalASRNLU::QTILocalASRNLU() - ASR Threshold = %d", asr_threshold_);
  syslog(LOG_INFO, "QTILocalASRNLU::QTILocalASRNLU() - ASR Timeout = %d", asr_timeout_);
  syslog(LOG_INFO, "QTILocalASRNLU::QTILocalASRNLU() - ASR Max Pause = %d", asr_max_pause_);
}

QTILocalASRNLU::~QTILocalASRNLU() {
  syslog(LOG_INFO, "------QTILocalASRNLU Destructor called");
}

bool QTILocalASRNLU::Initialize() {
  syslog(LOG_INFO, "QTILocalASRNLU::Initialize()");

  int result = 0;
  int opt;
  int repeat_count = 1;
  int thresholdCfgSet = 0, timeoutCfgSet = 0, maxPauseCfgSet = 0;
  bool show_version = true;

#ifdef STACK_PROFILING
  int64_t *rsp_tmp = NULL;
  int32_t max_stack = 0;
  FILE *fpStackLog = NULL;
#endif

#ifdef STACK_PROFILING
  // stack check before init function
#ifdef _MSC_VER
  // x86
  __asm {
            mov rsp_tmp, esp;
  }
#elif defined __ARMCC_VERSION
  // ARM
  __asm {
            MOV rsp_tmp, __current_sp();
  }
#elif defined __GNUC__
  // gcc 64 bit
  __asm__(
      "mov %%rsp, %0"
      : "=r"(rsp_tmp));
#endif
  printf("Address of current stack pointer is 0x%x\n", rsp_tmp);
  for (int i = 1; i < MAX_STACK_SIZE_WORDS; i++) {
    rsp_tmp[-i] = STACK_WATERMARK;
  }
#endif

  asr_memory_config_t memcfg;
  uint32_t memcfg_struct_size;
  memset(&memcfg, 0, sizeof(memcfg));
  memcfg_struct_size = sizeof(asr_memory_config_t);
  asr_config_t listenconfig;
  uint32_t listenconfig_struct_size;
  listenconfig_struct_size = sizeof(asr_config_t);
  listenconfig.sample_rate = 16000;
  listenconfig.frame_samples = 160;
  CHECK(asr_get_req(ASR_ID_MEM_CONFIG, (int8_t *)&memcfg, memcfg_struct_size,
      ASR_ID_CONFIG, (int8_t *)&listenconfig, listenconfig_struct_size));

  // load model
  int size;
  model_buffer_ = (char *)load_model(asr_model_file_.c_str(), &size);

  if (model_buffer_ == nullptr) {
    return false;
  }

  asr_ = 0;
  //Check Haven License
  //CHECK(asr_init(&asr_, 0, 0, 0, (int8_t *)model_buffer_, size));
  int ret = asr_init(&asr_, 0, 0, 0, (int8_t *)model_buffer_, size);
  if (ret != ASR_SUCCESS) {
    syslog(LOG_INFO, "QTILocalASRNLU::Initialize() - Haven License has Failed. Local ASR disabled");
    return false;
  }
  syslog(LOG_INFO, "QTILocalASRNLU::Initialize() - Haven License has been verified.");

  asr_version_param_t version = {0};
  uint32_t version_struct_filled_size = 0;

  CHECK(asr_get(asr_, ASR_ID_VERSION_PARAM, (int8_t *)&version, sizeof(version), &version_struct_filled_size));
  //printf("# Version ID is: 0x%llx \n", (long long)version.version);
#ifdef STACK_PROFILING
  {
    int i;
    for (i = (1 - MAX_STACK_SIZE_WORDS); i < 0; i++) {
      if (rsp_tmp[i] != STACK_WATERMARK) {
        break;
      }
    }
    max_stack = 1 - i;
    printf("Init function used %d 32-bit words of stack.\n", max_stack);
  }
#endif

  memset(&scratchparam_, 0, sizeof(scratchparam_));
  scratchparam_.scratch_size = memcfg.lib_scratch_mem_size;
  scratchparam_.scratch_ptr = (int8_t *)malloc(memcfg.lib_scratch_mem_size);
  CHECK(asr_set(asr_, ASR_ID_SCRATCH_PARAM, (int8_t *)&scratchparam_, sizeof(scratchparam_)));

  //set ASR parameters
  setThreshold(asr_threshold_);
  setMaxpause(asr_max_pause_);
  setTimeout(asr_timeout_);

  if (show_version) {
    // Get version information and print it
    // display Version information
    banner((uint64_t)version.version);
  }

#ifdef STACK_PROFILING
  printf("static mem size=%d bytes\n", memcfg.lib_static_mem_size);
  printf("scratch mem size=%d bytes\n", memcfg.lib_scratch_mem_size);
#endif

#ifdef STACK_PROFILING
// stack check before process function
#ifdef _MSC_VER
  // x86
  __asm {
        mov rsp_tmp, esp;
  }
#elif defined __ARMCC_VERSION
  // ARM
  __asm {
        MOV rsp_tmp, __current_sp();
  }
#elif defined __GNUC__
  // gcc 64 bit
  __asm__(
      "mov %%rsp, %0"
      : "=r"(rsp_tmp));
#endif
  for (int i = 1; i < MAX_STACK_SIZE_WORDS; i++) {
    rsp_tmp[-i] = STACK_WATERMARK;
  }

#endif

  syslog(LOG_INFO, "QTILocalASRNLU::Initialize() - Done!");
  return true;
}

void QTILocalASRNLU::setThreshold(uint32_t threshold) {
  syslog(LOG_INFO, "QTILocalASRNLU::setThreshold() - value = %d", threshold);

  asr_threshold_param_t cfg;
  cfg.threshold = threshold;
  CHECK(asr_set(asr_, ASR_ID_THRESHOLD_PARAM, (int8_t *)&cfg, sizeof(cfg)));
  syslog(LOG_INFO, "QTILocalASRNLU::setThreshold() - Set threshold is %d\n", cfg.threshold);

  uint32_t get_struct_filled_size = 0;
  asr_threshold_param_t cfg_get = {0};
  CHECK(asr_get(asr_, ASR_ID_THRESHOLD_PARAM, (int8_t *)&cfg_get, sizeof(cfg_get), &get_struct_filled_size));
  syslog(LOG_INFO, "QTILocalASRNLU::setThreshold() - Get threshold is %d\n", cfg_get.threshold);
}

void QTILocalASRNLU::setTimeout(uint32_t detection_timeout) {
  syslog(LOG_INFO, "QTILocalASRNLU::setTimeout() - value = %d", detection_timeout);

  asr_timeout_param_t cfg;
  cfg.detection_timeout = detection_timeout;
  CHECK(asr_set(asr_, ASR_ID_TIMEOUT_PARAM, (int8_t *)&cfg, sizeof(cfg)));
  syslog(LOG_INFO, "QTILocalASRNLU::setTimeout() is set to %d ms\n", detection_timeout);
}

void QTILocalASRNLU::setMaxpause(uint32_t max_pause_duration) {
  syslog(LOG_INFO, "QTILocalASRNLU::setMaxpause() - value = %d", max_pause_duration);

  asr_max_pause_duration_param_t cfg;
  cfg.max_pause_duration = max_pause_duration;
  CHECK(asr_set(asr_, ASR_ID_MAX_PAUSE_PARAM, (int8_t *)&cfg, sizeof(cfg)));
  syslog(LOG_INFO, "QTILocalASRNLU::setMaxpause() - is set to %d ms\n", max_pause_duration);
}

void QTILocalASRNLU::banner(uint64_t gVer) {
  syslog(LOG_INFO, "QTILocalASRNLU::banner()");
  uint32_t ver_external_major, ver_external_minor, ver_major, ver_minor, ver_revision;

  ver_external_major = (uint32_t)(((gVer)&0xFF00000000000000) >> 56);
  ver_external_minor = (uint32_t)(((gVer)&0x00FF000000000000) >> 48);
  ver_major = (uint32_t)(((gVer)&0x0000FF0000000000) >> 40);
  ver_minor = (uint32_t)(((gVer)&0x000000FF00000000) >> 32);
  ver_revision = (uint32_t)(((gVer)&0x00000000FF000000) >> 24);

  /*....execute.... */
  syslog(LOG_INFO, "--------------------------------------------------------------------");
  syslog(LOG_INFO, ">>>>>>>>>>>>>>>>>>           ASR Csim            <<<<<<<<<<<<<<<<<<<");
  syslog(LOG_INFO, ">>>>>>>>>>>>>>>>>>      Version: %d.%d_%d.%d.%d       <<<<<<<<<<<<<<<<<<<", (int)ver_external_major, (int)ver_external_minor, (int)ver_major, (int)ver_minor, (int)ver_revision);
  syslog(LOG_INFO, "--------------------------------------------------------------------");
}

void *QTILocalASRNLU::load_model(std::string model_file, int *size) {
  syslog(LOG_INFO, "QTILocalASRNLU::load_model() - File = %s", model_file.c_str());

  FILE *fp = fopen(model_file.c_str(), "rb");
  if (fp == NULL) {
    syslog(LOG_ERR, "QTILocalASRNLU::load_model() - Model file open error! name=%s", model_file.c_str());
    return nullptr;
  }

  fseek(fp, 0, SEEK_END);
  *size = ftell(fp);
  fseek(fp, 0, SEEK_SET);

#ifdef QC_KW
  void *buffer = NULL;
  if ((buffer = malloc(*size))) {
    if (fread(buffer, 1, *size, fp) != (size_t)*size) {
      syslog(LOG_ERR, "Error in reading model file");
      return nullptr;
    }
  } else {
    syslog(LOG_ERR, "Error in allocating buffer at line %d \n", __LINE__);
    return nullptr;
  }
#else
  void *buffer = malloc(*size);
  if (fread(buffer, 1, *size, fp) != (size_t)*size) {
    +fprintf(stderr, "Error in reading model file\n");
    return nullptr;
  }
#endif
  fclose(fp);

#ifdef VERBOSE
  syslog(LOG_INFO, "Model file size=%d\n", *size);
#endif
  return buffer;
}

int QTILocalASRNLU::process_an_audio_file(std::string audio_file) {
  syslog(LOG_INFO, "QTILocalASRNLU::process_an_audio_file - %s", audio_file.c_str());
  // load audio file
  FILE *audio_fp = fopen(audio_file.c_str(), "rb");
  if (audio_fp == NULL) {
    syslog(LOG_INFO, "Input audio file cannot be opened: %s", audio_file.c_str());
    return RESULT_ERROR_OPEN;
  }

  struct wave_fmt fmt;
  long pcm_offset = get_pcm_offset(audio_fp, &fmt);

  fseek(audio_fp, 0, SEEK_END);

  long file_size = ftell(audio_fp);
  fseek(audio_fp, pcm_offset, SEEK_END);

  long pcm_samples = (file_size - pcm_offset) / sizeof(short);

  fseek(audio_fp, pcm_offset, SEEK_SET);

  int FRAME_SIZE = 320;  // block_size; // 10ms

  const int SHIFT_TO_FRAME = 2;  // need 2 frame_shift to make a 30ms frame
  long feature_count = pcm_samples / FRAME_SAMPLES_10MS - SHIFT_TO_FRAME;
  const long min_frame_size = 30;  //[smm] min_frame_size field in config_keyword_customization.ini
  if (feature_count < min_frame_size) {
    syslog(LOG_ERR, "%s is too short\n", audio_file.c_str());
    fclose(audio_fp);
    return RESULT_ERROR_TOO_SHORT;
  }

  long pcm_buffer_size = FRAME_SIZE;

  short *pcm = (short *)malloc(pcm_buffer_size * sizeof(short));
  if (!pcm) {
    syslog(LOG_ERR, "Failed to allocate %ld bytes for pcm data\n", pcm_samples);
    fclose(audio_fp);
    return RESULT_ERROR_TOO_BIG;
  }

  long pcm_remained = pcm_samples;
  int loop_count = 0;

  while (pcm_remained >= FRAME_SIZE) {
    syslog(LOG_INFO, "QTILocalASRNLU::process_an_audio_file - %s WHILE", audio_file.c_str());
    // read pcm data
    if (fread(pcm, sizeof(short), pcm_buffer_size, audio_fp) != (size_t)pcm_buffer_size) {
      syslog(LOG_ERR, "Error in reading audio file");
      fclose(audio_fp);
      free(pcm);
      return RESULT_ERROR_READ;
    }
    pcm_remained -= pcm_buffer_size;
    loop_count += 1;

    ASR_RESULT ret = asr_process(asr_, 0, (int8_t *)pcm, 0, pcm_buffer_size * sizeof(short), 0, 0);

    CHECK(asr_get(asr_, ASR_ID_STATUS_PARAM, (int8_t *)&result_, result_struct_size_, &get_struct_filled_size_));

    if (ret == ASR_DETECTED) {
      syslog(LOG_INFO, "QTILocalASRNLU::process_an_audio_file - %s DETECTED", audio_file.c_str());
      if (!process_whole_file)
        break;
      //      asr_set(asr, ASR_ID_RESET_ALL_PARAM, 0, 0);
    } else if (ret == ASR_TIMEOUT) {
      syslog(LOG_INFO, "QTILocalASRNLU::process_an_audio_file - %s TIMEOUT", audio_file.c_str());
      if (!process_whole_file)
        break;
      //asr_set(asr, ASR_ID_RESET_ALL_PARAM, 0, 0);
    }
  }

  fclose(audio_fp);
  free(pcm);
  if (strcmp(result_.intent, "NULL") == 0) {
    syslog(LOG_INFO, "QTILocalASRNLU::process_an_audio_file() - Confidence = %f - OUT_OF_DOMAIN", FLOAT_SCORE(result_.confidence));
  } else {
    syslog(LOG_INFO, "QTILocalASRNLU::process_an_audio_file() - Confidence = %f - Result = %s", FLOAT_SCORE(result_.confidence), result_.intent);
  }
  syslog(LOG_INFO, "QTILocalASRNLU::process_an_audio_file - %s 10", audio_file.c_str());
  return RESULT_SUCCESS;
}

long QTILocalASRNLU::get_pcm_offset(FILE *fp, struct wave_fmt *fmt) {
  fseek(fp, 0, SEEK_SET);
  struct {
    char id[4];
    int32_t size;
  } chunk_header;

  if ((fread(&chunk_header, sizeof(chunk_header), 1, fp) != 1)
      || strncmp(chunk_header.id, "RIFF", 4) != 0) {
    return 0;
  }

  char wave[4];
  if ((fread(&wave, sizeof(wave), 1, fp) != 1)
      || strncmp(wave, "WAVE", 4) != 0) {
    printf("WAVE id mismatch !!!\n");
    return -2;
  }

  if ((fread(&chunk_header, sizeof(chunk_header), 1, fp) != 1)
      || strncmp(chunk_header.id, "fmt ", 4) != 0) {
    printf("'fmt ' id mismatch !!!\n");
    return -3;
  }

  if (fread(fmt, chunk_header.size, 1, fp) != 1) {
    printf("Error in read wave format!!!\n");
    return -4;
  }

  if (fread(&chunk_header, sizeof(chunk_header), 1, fp) != 1) {
    printf("Error in read fact/data id !!!\n");
    return -5;
  }

  if ((strncmp(chunk_header.id, "fact", 4) == 0)
      && (fseek(fp, chunk_header.size, SEEK_CUR) != 0
             || fread(&chunk_header, sizeof(chunk_header), 1, fp) != 1)) {
    printf("Error in skip fact header!!!\n");
    return -6;
  }

  if (strncmp(chunk_header.id, "data", 4) != 0) {
    printf("'data' id mismatch !!!\n");
    return -7;
  }

  long result = ftell(fp);
  fseek(fp, 0, SEEK_SET);

  return result;
}

void QTILocalASRNLU::Start() {
  syslog(LOG_INFO, "QTILocalASRNLU::Start()");

  if (!asr_running_) {
    asr_running_ = true;

    //Notify Modular client about ASR start
    asr_callback_->ASRStarted();

    //open dump file
    if (voicedump_enable_) {
      std::string filepath = std::string(voicedump_path_ + "/voiceDumpLocalASR");
      syslog(LOG_INFO, "QTILocalASRNLU::Start() - Voice Dump path: %s - %s", voicedump_path_.c_str(), filepath.c_str());
      log_filename_ = fopen(filepath.c_str(), "wb");
    }

    //Reset ASR Engine
    CHECK(asr_set(asr_, ASR_ID_RESET_ALL_PARAM, 0, 0));

    //Reset ASR results
    get_struct_filled_size_ = 0;
    memset(&result_, 0, sizeof(result_));
    result_struct_size_ = sizeof(result_);

  } else {
    syslog(LOG_ERR, "QTILocalASRNLU::Start() - Local ASR is ALREADY running");
  }
  syslog(LOG_INFO, "******** QTILocalASRNLU::Start() - DONE");
}

void QTILocalASRNLU::Stop() {
  syslog(LOG_INFO, "QTILocalASRNLU::Stop()");

  if (asr_running_) {
    asr_running_ = false;

    //Reset ASR Engine
    asr_set(asr_, ASR_ID_RESET_ALL_PARAM, 0, 0);

    //close dumpfile
    if (voicedump_enable_) {
      fclose(log_filename_);
    }
  } else {
    syslog(LOG_ERR, "QTILocalASRNLU::Stop() - Local ASR is NOT running");
  }
  syslog(LOG_INFO, "******** QTILocalASRNLU::Stop() - DONE");
}

bool QTILocalASRNLU::ProcessInput(uint8_t *buf, uint size) {
  bool result = true;

  if (asr_running_) {
    ASR_RESULT ret = asr_process(asr_, 0, (int8_t *)buf, 0, size * sizeof(int8_t), 0, 0);

    //save audio in a file
    if (voicedump_enable_) {
      fwrite(buf, 1, size, log_filename_);
      fflush(log_filename_);
    }

    //Check ASR Response
    if (ret == ASR_DETECTED) {
      CHECK(asr_get(asr_, ASR_ID_STATUS_PARAM, (int8_t *)&result_, result_struct_size_, &get_struct_filled_size_));

      syslog(LOG_INFO, "QTILocalASRNLU::ProcessInput Finished: Confidence = %f - Result = %s", FLOAT_SCORE(result_.confidence), result_.intent);

      //Stop ASR recognition
      Stop();
      //Send callback to modular client with ASR results
      asr_callback_->ASRCompleteResults(result_.intent);

    } else if (ret == ASR_TIMEOUT) {
      CHECK(asr_get(asr_, ASR_ID_STATUS_PARAM, (int8_t *)&result_, result_struct_size_, &get_struct_filled_size_));

      syslog(LOG_INFO, "QTILocalASRNLU::ProcessInput Finished: Confidence = %f - OUT_OF_DOMAIN", FLOAT_SCORE(result_.confidence));

      //Stop ASR recognition
      Stop();
      //Send callback to modular client with ASR results
      asr_callback_->ASRError("ASR TIMEOUT");
    }
  } else {
    syslog(LOG_INFO, "QTILocalASRNLU::ProcessInput: asr_running_ == FALSE - Ignore input");
  }

  return result;
}

bool QTILocalASRNLU::IsRunning() {
  return asr_running_;
}

bool QTILocalASRNLU::HasNLU() {
  return true;
}

void QTILocalASRNLU::SetLanguage() {
  syslog(LOG_INFO, "QTILocalASRNLU::SetLanguage()");

  //TODO
}

void QTILocalASRNLU::Shutdown() {
  syslog(LOG_INFO, "QTILocalASRNLU::Shutdown()");

  if (scratchparam_.scratch_ptr)
    free(scratchparam_.scratch_ptr);

  scratchparam_.scratch_ptr = NULL;

  free(model_buffer_);
}

}  // namespace qtiASRClient
}  // namespace voiceUIFramework
