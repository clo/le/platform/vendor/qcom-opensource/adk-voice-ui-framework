/* Copyright (c) 2018, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*************************************************************
 *  @file    QTILocalASRNLU.h
 *  @brief   Qualcomm Technologies, Inc local ASR / NLU Component
 *
 *  DESCRIPTION
 *    Qualcomm Technologies, Inc local ASR / NLU Component to be used on VoiceUI Modular Client
 ***************************************************************/

#ifndef VOICEUIFRAMEWORK_QTIASRINTEGRATION_INCLUDE_QTIASRINTEGRATION_QTILOCALASRNLU_H_
#define VOICEUIFRAMEWORK_QTIASRINTEGRATION_INCLUDE_QTIASRINTEGRATION_QTILOCALASRNLU_H_

#include <syslog.h>
#include <cstdio>
#include <iostream>
#include <memory>
#include <string>
#include "asr_api.h"

#include <VoiceUIClient/ModularSolution/ASR/ASRCallBackInterface.h>
#include <VoiceUIClient/ModularSolution/ASR/ASRInterface.h>
#include <VoiceUIUtils/VoiceUIConfig.h>

namespace voiceUIFramework {
namespace qtiASRClient {

class QTILocalASRNLU : public voiceUIFramework::voiceUIClient::ASRInterface, public std::enable_shared_from_this<QTILocalASRNLU> {
 public:
  QTILocalASRNLU(std::shared_ptr<voiceUIFramework::voiceUIClient::ASRCallBackInterface> asr_callback);
  virtual ~QTILocalASRNLU();

  //Initializes ASR engine
  virtual bool Initialize();
  //Starts ASR engine / If cloud engine connects to cloud
  virtual void Start();
  //Stops ASR engine / If cloud engine disconnects from cloud
  virtual void Stop();
  //Process audio input
  virtual bool ProcessInput(uint8_t* buf, uint size);
  //verify if ASR is running
  virtual bool IsRunning();
  //Verify if ASR has NLU embedded
  virtual bool HasNLU();
  //Set ASR language
  virtual void SetLanguage();
  //Shutdown ASR Engine
  virtual void Shutdown();

  //set QC ASR Threshold  - range 0-99 - default 0
  void setThreshold(uint32_t threshold);
  //set QC ASR Timeout - value in ms - default 10000ms
  void setTimeout(uint32_t detection_timeout);
  //set QC ASR Maxpause -  value in ms - default  500ms
  void setMaxpause(uint32_t max_pause_duration);

 protected:
  //Print ASR model version
  void banner(uint64_t gVer);
  //Load ASR model from a file to memory (read all of it) as a blackbox and return the pointer with size.
  void* load_model(std::string model_file, int* size);
  // * Recognize an audio file in PCM or WAV format.
  int process_an_audio_file(std::string audio_file);
  //
  long get_pcm_offset(FILE* fp, struct wave_fmt* fmt);

  asr_scratch_param_t scratchparam_;
  char* model_buffer_;
  asr_lib_t* asr_;
  bool show_input_sequence;
  bool process_whole_file;

  //ASR Results
  asr_status_param_t result_;
  uint32_t result_struct_size_;
  uint32_t get_struct_filled_size_;

  std::shared_ptr<voiceUIFramework::voiceUIClient::ASRCallBackInterface> asr_callback_;
  bool asr_running_;
  FILE* log_filename_;

  //voice dump file
  bool voicedump_enable_;
  std::string voicedump_path_;
  //ASR sound Model
  std::string asr_model_file_;

  //ASR parameters
  //threshold
  uint32_t asr_threshold_;
  //timeout
  uint32_t asr_timeout_;
  //max_pause
  uint32_t asr_max_pause_;
};

}  // namespace qtiASRClient
}  // namespace voiceUIFramework

#endif /* VOICEUIFRAMEWORK_QTIASRINTEGRATION_INCLUDE_QTIASRINTEGRATION_QTILOCALASRNLU_H_ */
